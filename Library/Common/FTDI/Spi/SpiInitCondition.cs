﻿using System.Runtime.InteropServices;

namespace Ftdi.Spi
{
   /// <summary>
   ///  Initial state of pins
   ///  
   ///  Specify the initial state(H/L) of each pins at the beginning of transfer.
   /// </summary>
   [StructLayout(LayoutKind.Sequential)]
   public struct SpiInitCondition
   {
      /// Start with trueH, falseL.
      public LogicLevel ClockPin;

      /// Start with trueH, falseL.
      public LogicLevel DataOutPin;

      /// Chipselect Active trueL, falseH
      public LogicPolarity ChipSelectPinPol;

      /// Specify the pin by FTDI.SPI.chipSelect type.
      public SpiChipSelect dwChipSelectPin;
   };
}