﻿using System.Runtime.InteropServices;

namespace Ftdi.Spi
{
   /// <summary>
   /// Wait data write
   /// 
   /// Wait for data write structure
   /// </summary>
   [StructLayout(LayoutKind.Sequential)]
   public struct SpiWaitDataWrite
   {
      public bool bWaitDataWriteComplete;

      public SpiGpio dwWaitDataWritePin;

      public bool bDataWriteCompleteState;

      public int dwDataWriteTimeoutmSecs;
   };
}