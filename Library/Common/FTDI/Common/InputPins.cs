using System.Runtime.InteropServices;

namespace Ftdi
{
   /// <summary>
   ///  GPIO pin status
   ///  
   ///  Caution The original FTCJTAG.h had bug on the name of each member function.
   ///  The name bPin4LowHighState, is wrong name. It should be bPin3LowHighState.
   ///  (Tha bPinxLowHighState where x must be zero origin, instead of one origin ).
   ///  Below definition corrected this mistake. 
   /// </summary>
   [StructLayout(LayoutKind.Sequential)]
   public struct InputPins
   {
      /// <summary>
      /// H or L
      /// </summary>
      public LogicLevel Pin0;

      /// <summary>
      /// H or L
      /// </summary>
      public LogicLevel Pin1;

      /// <summary>
      /// H or L
      /// </summary>
      public LogicLevel Pin2;

      /// <summary>
      /// H or L
      /// </summary>
      public LogicLevel Pin3;
   };
}