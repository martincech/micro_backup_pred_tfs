﻿namespace Ftdi
{
   /// <summary>
   /// the Logic Porality of physical pin
   /// </summary>
   public enum LogicPolarity
   {
      /// <summary>
      /// Logic is positive logic
      /// </summary>
      ActiveH = 0,
      /// <summary>
      /// Logic is negative logic
      /// </summary>
      ActiveL = 1 
   };
}