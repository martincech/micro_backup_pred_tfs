﻿//******************************************************************************
//
//   FtxxGpio.cs        Ftxx Gpio
//   Version 1.0        (c) Veit Electronics
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using FTD2XX_NET;

namespace Ftdi
{
   /// <summary>
   /// Performs GPIO on arbitrary FTDI device
   /// </summary>
   public class FtxxGpio
   {
      /// <summary>
      /// Represents a pin of FTDI device
      /// </summary>
      public class Pin
      {
         #region Private fields

         private readonly FtxxGpio gpio; // parent
         private readonly int pinId; // Pin ID 0 - xx

         #endregion

         /// <summary>
         /// Pin's constructor
         /// </summary>
         /// <param name="gpio">Parent FTDI device the pin belongs to</param>
         /// <param name="pinId">Sequence number of the pin</param>
         public Pin(FtxxGpio gpio, int pinId)
         {
            if (gpio == null) throw new ArgumentNullException("gpio");
            if (gpio.pins[pinId] != null)
            {
               // Allow construction only if Gpio doesn't contain the pin already
               return;
            }
            this.gpio = gpio;
            this.pinId = pinId;
         } // Pin

         /// <summary>
         /// Sets pin direction to output
         /// </summary>
         public void OutputSet()
         {
            gpio.OutputSet(pinId);
         }

         /// <summary>
         /// Sets pin value to 1
         /// </summary>
         public void Set()
         {
            gpio.Set(pinId);
         }
         
         /// <summary>
         /// Sets pin value to 0
         /// </summary>
         public void Clear()
         {
            gpio.Clear(pinId);
         }
      }

      #region Private fields & consts

      private static readonly string[] SUFFIX = {" A", " B", " C", " D"};
      private static readonly int DEVICE_COUNT_MAX = SUFFIX.Length;
      private const int PIN_PER_DEVICE_COUNT = 8;
      private static readonly int PIN_COUNT_MAX = PIN_PER_DEVICE_COUNT*DEVICE_COUNT_MAX;

      private readonly Ftdi[] device = new Ftdi[DEVICE_COUNT_MAX];
      private readonly Pin[] pins = new Pin[PIN_COUNT_MAX];

      private static readonly string[] PIN_NAMES =
      {
         "ADBUS0", "ADBUS1", "ADBUS2", "ADBUS3", "ADBUS4", "ADBUS5", "ADBUS6", "ADBUS7",
         "BDBUS0", "BDBUS1", "BDBUS2", "BDBUS3", "BDBUS4", "BDBUS5", "BDBUS6", "BDBUS7",
         "CDBUS0", "CDBUS1", "CDBUS2", "CDBUS3", "CDBUS4", "CDBUS5", "CDBUS6", "CDBUS7",
         "DDBUS0", "DDBUS1", "DDBUS2", "DDBUS3", "DDBUS4", "DDBUS5", "DDBUS6", "DDBUS7"
      };

      private readonly Dictionary<string, Pin> pinsByName = new Dictionary<string, Pin>();

      private readonly byte[] directionMask = {0x00, 0x00, 0x00, 0x00}; // current directions of pins
      private readonly byte[] data = {0x00, 0x00, 0x00, 0x00}; // current output value on pins

      #endregion

// ReSharper disable InconsistentNaming
      public readonly Pin ADBUS0;
      public readonly Pin ADBUS1;
      public readonly Pin ADBUS2;
      public readonly Pin ADBUS3;
      public readonly Pin ADBUS4;
      public readonly Pin ADBUS5;
      public readonly Pin ADBUS6;
      public readonly Pin ADBUS7;

      public readonly Pin BDBUS0;
      public readonly Pin BDBUS1;
      public readonly Pin BDBUS2;
      public readonly Pin BDBUS3;
      public readonly Pin BDBUS4;
      public readonly Pin BDBUS5;
      public readonly Pin BDBUS6;
      public readonly Pin BDBUS7;

      public readonly Pin CDBUS0;
      public readonly Pin CDBUS1;
      public readonly Pin CDBUS2;
      public readonly Pin CDBUS3;
      public readonly Pin CDBUS4;
      public readonly Pin CDBUS5;
      public readonly Pin CDBUS6;
      public readonly Pin CDBUS7;

      public readonly Pin DDBUS0;
      public readonly Pin DDBUS1;
      public readonly Pin DDBUS2;
      public readonly Pin DDBUS3;
      public readonly Pin DDBUS4;
      public readonly Pin DDBUS5;
      public readonly Pin DDBUS6;
      public readonly Pin DDBUS7;
// ReSharper restore InconsistentNaming

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="deviceName">Device's name as stored in its EEPROM</param>
      public FtxxGpio(string deviceName)
      {
         // Single channel devices
         device[0] = DeviceGet(deviceName);
         if (device[0] == null)
         {
            // it is not a single channel device
            // Try to open multi channel devices (Name + suffix A, B, C, D)
            for (var i = 0; i < DEVICE_COUNT_MAX; i++)
            {
               device[i] = DeviceGet(deviceName + SUFFIX[i]);

               if (device[i] != null) continue;
               if (i < 2) // first or second device not opened properly, something is wrong
               {
                  throw new Exception("Unable open " + deviceName + " FTDI device.");
               }
               break;
            }
         }
         // Create all pins, not every pin is going to be used on real device
         for (var i = 0; i < PIN_COUNT_MAX; i++)
         {
            pins[i] = new Pin(this, i);
            pinsByName.Add(PIN_NAMES[i], pins[i]);
         }

         ADBUS0 = pins[0];
         ADBUS1 = pins[1];
         ADBUS2 = pins[2];
         ADBUS3 = pins[3];
         ADBUS4 = pins[4];
         ADBUS5 = pins[5];
         ADBUS6 = pins[6];
         ADBUS7 = pins[7];

         BDBUS0 = pins[8];
         BDBUS1 = pins[9];
         BDBUS2 = pins[10];
         BDBUS3 = pins[11];
         BDBUS4 = pins[12];
         BDBUS5 = pins[13];
         BDBUS6 = pins[14];
         BDBUS7 = pins[15];

         CDBUS0 = pins[16];
         CDBUS1 = pins[17];
         CDBUS2 = pins[18];
         CDBUS3 = pins[19];
         CDBUS4 = pins[20];
         CDBUS5 = pins[21];
         CDBUS6 = pins[22];
         CDBUS7 = pins[23];

         DDBUS0 = pins[24];
         DDBUS1 = pins[25];
         DDBUS2 = pins[26];
         DDBUS3 = pins[27];
         DDBUS4 = pins[28];
         DDBUS5 = pins[29];
         DDBUS6 = pins[30];
         DDBUS7 = pins[31];
      }

      /// <summary>
      /// Provides access to individual pins by their names
      /// </summary>
      public Pin this[string name]
      {
         get { return pinsByName[name]; }
      }

      /// <summary>
      /// Finalize
      /// </summary>
      ~FtxxGpio()
      {
         foreach (var dev in device.Where(dev => dev != null))
         {
            dev.Close();
         }
      }

      /// <summary>
      /// Sets pin direction to output
      /// </summary>
      private void OutputSet(int pinId)
      {
         var deviceId = DeviceId(ref pinId);
         directionMask[deviceId] |= (byte) (1 << pinId);
         if (device[deviceId] == null)
         {
            throw new Exception("Unable set direction of pin for FTDI device.");
         }
         if (device[deviceId].SetBitMode(directionMask[deviceId], FTDI.FT_BIT_MODES.FT_BIT_MODE_ASYNC_BITBANG) !=
             FTDI.FT_STATUS.FT_OK)
         {
            throw new Exception("Unable set direction of pin for FTDI device.");
         }
      }

      /// <summary>
      /// Sets pin value to 1
      /// </summary>
      private void Set(int pinId)
      {
         var deviceId = DeviceId(ref pinId);
         data[deviceId] |= (byte) (1 << pinId);

         WriteByte(device[deviceId], data[deviceId]);
      }

      /// <summary>
      /// Sets pin value to 0
      /// </summary>
      private void Clear(int pinId)
      {
         var deviceId = DeviceId(ref pinId);
         data[deviceId] &= ((byte) ~(1 << pinId));

         WriteByte(device[deviceId], data[deviceId]);
      }

      /// <summary>
      /// Get device ID from pinID and correct pinID
      /// </summary>
      /// <param name="pinId"></param>
      /// <returns></returns>
      private static int DeviceId(ref int pinId)
      {
         var deviceId = pinId / PIN_PER_DEVICE_COUNT;
         pinId = pinId % PIN_PER_DEVICE_COUNT;
         return deviceId;
      }

      /// <summary>
      /// Tries to open FTDI device by its name
      /// </summary>
      private static Ftdi DeviceGet(string name)
      {
         var identifier = 0;
         var device = new Ftdi();
         if (device.Locate(name, ref identifier) != FTDI.FT_STATUS.FT_OK)
         {
            return null;
         }
         if (device.Open(identifier) != FTDI.FT_STATUS.FT_OK)
         {
            return null;
         }
         if (device.SetBitMode(0x00, FTDI.FT_BIT_MODES.FT_BIT_MODE_RESET) != FTDI.FT_STATUS.FT_OK)
         {
            return null;
         }
         return device;
      }

      /// <summary>
      /// Writes a byte value to FTDI device
      /// </summary>
      private static void WriteByte(FTDI device, byte data)
      {
         uint written = 0;
         if (device == null)
         {
            throw new Exception("Unable write to FTDI device.");
         }
         if (device.Write(new[] {data}, 1, ref written) != FTDI.FT_STATUS.FT_OK)
         {
            throw new Exception("Unable write to FTDI device.");
         }
         if (written != 1)
         {
            throw new Exception("Unable write to FTDI device.");
         }
      }
   }
}