namespace Ftdi.Jtag
{
   /// <summary>
   /// JTAG tap after read/write
   ///  
   ///  These enum represents the internal state of JTAG TAP inside device.
   /// </summary>
   public enum JtagTapState
   {
      /// <summary>
      /// Test logic is reset
      /// </summary>
      TestLogicState = 1,

      /// <summary>
      /// Keep testing.
      /// </summary>
      RunTestIdelState,

      /// <summary>
      /// Stay on Pause state of Data path
      /// </summary>
      PauseTestDataRegisterState,

      /// <summary>
      /// Stay on Pause state of Instruciton Path
      /// </summary>
      PauseInstructionRegisterState,

      /// <summary>
      /// Stay on Shift state of Data path
      /// </summary>
      ShiftTestDataRegisterState,

      /// <summary>
      /// Stay on Shift state of Instruciton Path
      /// </summary>
      ShiftInstructionRegisterState
   };
}