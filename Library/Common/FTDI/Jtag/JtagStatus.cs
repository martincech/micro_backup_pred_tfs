namespace Ftdi.Jtag
{
   /// <summary>
   ///  Error code of FTDI function.
   ///  
   ///  The returned value of FTDI DLL function is coded here. The name is changed 
   ///  from FULL C Macro style to nearly C# style, but not perfect.
   /// </summary>
   public enum JtagStatus
   {
      Success = 0, // OK
      InvalidHandle = 1, // InvalidHANDLE
      DeviceNotFound = 2, //DeviceNotFOUND
      DeviceNotOpened = 3, //DeviceNotOPENED
      IoError = 4, //IOERROR
      InsufficientResources = 5, // INSUFFICIENTRESOURCES

      FailedtoCompleteCommand = 20, // cannot change, error code mapped from FT2232c classes
      FailedtoSynchronizeDeviceMpsse = 21, // cannot change, error code mapped from FT2232c classes
      InvalidDeviceNameIndex = 22, // cannot change, error code mapped from FT2232c classes
      NullDeviceNameBufferPointer = 23, // cannot change, error code mapped from FT2232c classes 
      DeviceNameBufferTooSmall = 24, // cannot change, error code mapped from FT2232c classes
      InvalidDeviceName = 25, // cannot change, error code mapped from FT2232c classes
      InvalidLocationId = 26, // cannot change, error code mapped from FT2232c classes
      DeviceInUse = 27, // cannot change, error code mapped from FT2232c classes
      TooManyDevices = 28, // cannot change, error code mapped from FT2232c classes
      InvalidFrequencyValue = 29,
      NullInputOutputBufferPointer = 30,
      InvalidNumberBits = 31,
      NullWriteDataBufferPointer = 32,
      InvalidNumberByte = 33,
      NumberByteTooSmall = 34,
      InvalidTapControllerState = 35,
      NullReadDataBufferPointer = 36,
      NullDllVersionBufferPointer = 37,
      DllVersionBufferTooSmall = 38,
      NullLanguageCodeBufferPointer = 39,
      NullErrorMessageBufferPointer = 40,
      ErrorMessageBufferTooSmall = 41,
      InvalidLanguageCode = 42
   };
}