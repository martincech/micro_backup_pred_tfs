﻿using System.Threading;

namespace FtxxGpio.Tests.Program
{
   class Program
   {
      const string DEVICE_NAME = "VEIT USB Reader";

      static void Main(string[] args)
      {
         var gpio = new Ftdi.FtxxGpio(DEVICE_NAME);
         var myFirstPin = gpio.ADBUS0;
         var mySecondPin = gpio.BDBUS3;

         myFirstPin.OutputSet();
         mySecondPin.OutputSet();
         while (true)
         {
            myFirstPin.Set();
            Thread.Sleep(500);
            myFirstPin.Clear();
            Thread.Sleep(500);
            mySecondPin.Set();
            Thread.Sleep(500);
            mySecondPin.Clear();
            Thread.Sleep(500);
         }
      }
   }
}
