﻿using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Usb.HID;

namespace Usb.Tests.Hid
{
   [TestClass]
   public class HidWatcherTests
   {
      private UsbDeviceManager hidWatcher;

      [TestInitialize]
      public void Init()
      {
         hidWatcher = UsbDeviceManager.Instance;
      }

      [TestCleanup]
      public void Clean()
      {
         
      }

      [TestMethod]
      public void FastDisposeTest()
      {
         hidWatcher.Dispose();
      }

      [TestMethod]
      public void Watcher_WatchingStartedSomeDevicesConnected()
      {
         Assert.IsTrue(hidWatcher.ConnectedDevices.Any());
      }


      [TestMethod]
      public void Watcher_Contains_Bat2()
      {
         Assert.IsTrue(hidWatcher.ConnectedDevices.Any(device => device is HidDevice && device.VendorID == TestConstants.VID && device.ProductID == TestConstants.PID));
      }
   }
}
