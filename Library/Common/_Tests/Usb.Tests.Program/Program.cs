﻿using System;
using Usb.UMS;

namespace Usb.Test.Program
{
   internal class Program
   {
      private static void Main(string[] args)
      {
         var watcher = UsbDeviceManager.Instance;

         watcher.DeviceConnected +=
            (sender, device) =>
               Console.WriteLine("Device connected event: {0}!",
                  device is UmsDevice ? "UMS" : device is HID.HidDevice ? "HID" : "FTDI");
         watcher.DeviceDisconnected +=
            (sender, device) =>
               Console.WriteLine("Device disconnected event: {0}!",
                  device is UmsDevice ? "UMS" : device is HID.HidDevice ? "HID" : "FTDI");
         Console.WriteLine("Waiting for device changes");

         Console.ReadKey();
      }
   }
}
