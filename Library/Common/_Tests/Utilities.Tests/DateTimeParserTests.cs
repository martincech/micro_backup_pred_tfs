﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Utilities.Tests
{
    [TestClass]
    public class DateTimeParserTests
    {
        private Dictionary<string, DateTime> dateList = new Dictionary<string, DateTime>() 
        {
            {"13/12/2014",new DateTime(2014,12,13)},
            {  "1.2.2014",new DateTime(2014,2,1)},
            { "1:2:14",new DateTime(2014,2,1)},
            { "12/13/2014",new DateTime(2014,12,13)},
            {"2014.12.13",new DateTime(2014,12,13)},
            {"2014_13_12",new DateTime(2014,12,13)},
            {"1.feb.2006",new DateTime(2006,2,1)},
            {"feb,1.2013",new DateTime(2013,2,1)},
            {"2012,feb,27",new DateTime(2012,2,27)},
            {"2010,1,feb",new DateTime(2010,2,1)},
            {"16,1,feb",new DateTime(2016,2,1)},
            {"01,12,feb",new DateTime(2001,2,12)},
            {"feb,1,10",new DateTime(2010,2,1)},
            {"1,feb,10",new DateTime(2010,2,1)},
        };

        private Dictionary<string, DateTime> timeList = new Dictionary<string, DateTime>() 
        {
             {"10:11:12",new DateTime(1,1,1,10,11,12)},
             {"1:21:56",new DateTime(1,1,1,1,21,56)},
             {"16:2:3",new DateTime(1,1,1,16,2,3)},
             {"1:2:3AM",new DateTime(1,1,1,1,2,3)},
             {"1:2:3PM",new DateTime(1,1,1,13,2,3)},
             {"1:2:3am",new DateTime(1,1,1,1,2,3)},
             {"1:2:3pm",new DateTime(1,1,1,13,2,3)},
             {"12:2:3pm",new DateTime(1,1,1,12,2,3)},
             {"12:2:3am",new DateTime(1,1,1,0,2,3)},
        };

        [TestMethod]
        public void ParseDate_Ok()
        {
            foreach (var date in dateList)
            {
                DateTime parsedDate = DateTimeParser.ParseDate(date.Key);
                Assert.AreEqual(date.Value.Year, parsedDate.Year);
                Assert.AreEqual(date.Value.Month, parsedDate.Month);
                Assert.AreEqual(date.Value.Day, parsedDate.Day);
            }
        }

        [TestMethod]
        public void ParseTime_Ok()
        {
            foreach (var date in timeList)
            {
                DateTime parsedTime = DateTimeParser.ParseTime(date.Key);
                Assert.AreEqual(date.Value.Hour, parsedTime.Hour);
                Assert.AreEqual(date.Value.Minute, parsedTime.Minute);
                Assert.AreEqual(date.Value.Second, parsedTime.Second);
            }
        }
    }
}
