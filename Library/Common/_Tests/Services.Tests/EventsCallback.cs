using System;

namespace Services.Tests
{
   public class EventsCallback : ITestServiceEvents
   {
      #region Implementation of ITestServiceEvents

      public event EventHandler Event1Invoked;
      public event EventHandler Event2Invoked;
      public event EventHandler Event3Invoked;

      public void OnEvent1()
      {
         OnEvent1Invoked();
      }

      public void OnEvent2(int number)
      {
         OnEvent2Invoked();
      }

      public void OnEvent3(int number, string text)
      {
         OnEvent3Invoked();
      }

      #endregion

      #region Event invokers

      protected virtual void OnEvent1Invoked()
      {
         var handler = Event1Invoked;
         if (handler != null) handler(this, EventArgs.Empty);
      }

      protected virtual void OnEvent2Invoked()
      {
         var handler = Event2Invoked;
         if (handler != null) handler(this, EventArgs.Empty);
      }

      protected virtual void OnEvent3Invoked()
      {
         var handler = Event3Invoked;
         if (handler != null) handler(this, EventArgs.Empty);
      }

      #endregion

   }
}