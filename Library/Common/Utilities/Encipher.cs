﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Utilities
{
    /// <summary>
    /// Ecrypt data
    /// </summary>
    public class Encipher
    {
        const int BUFFER_LENGHT = 4096;
      
        /// <summary>
        /// Encrypt a byte array into a byte array using a key and an IV 
        /// </summary>
        /// <param name="clearData">data to encrypt</param>
        /// <param name="Key">encrypt key</param>
        /// <param name="IV">initialization vector</param>
        /// <returns>encrypted data</returns>
        public static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms,
               alg.CreateEncryptor(), CryptoStreamMode.Write);

            cs.Write(clearData, 0, clearData.Length);
            cs.Close();
            byte[] encryptedData = ms.ToArray();
            return encryptedData;
        }

        /// <summary>
        /// Encrypt a string into a encrypted string using a password
        /// </summary>
        /// <param name="clearText">text to encrypt</param>
        /// <param name="password">password used to encrypt</param>
        /// <returns>encrypted text</returns>
        public static string Encrypt(string clearText, string password)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            PasswordDeriveBytes pdb = GetDeriveBytes(password);
            byte[] encryptedData = Encrypt(clearBytes,
               pdb.GetBytes(32), pdb.GetBytes(16));
            return Convert.ToBase64String(encryptedData);
        }

        /// <summary>
        /// Encrypt a byte array into a byte array using password
        /// </summary>
        /// <param name="clearData">encrypt data</param>
        /// <param name="password">password used to encrypt</param>
        /// <returns></returns>
        public static byte[] Encrypt(byte[] clearData, string password)
        {
            PasswordDeriveBytes pdb = GetDeriveBytes(password);
            return Encrypt(clearData, pdb.GetBytes(32), pdb.GetBytes(16));
        }

        /// <summary>
        /// Encrypt a file into using password
        /// </summary>
        /// <param name="fileIn">file to encrypt</param>
        /// <param name="fileOut">encrypted file</param>
        /// <param name="password">pasword used to encrypt</param>
        public static void Encrypt(string fileIn,
           string fileOut, string password)
        {
            FileStream fsIn = new FileStream(fileIn,
               FileMode.Open, FileAccess.Read);
            FileStream fsOut = new FileStream(fileOut,
               FileMode.OpenOrCreate, FileAccess.Write);

            PasswordDeriveBytes pdb = GetDeriveBytes(password);

            Rijndael alg = Rijndael.Create();
            alg.Key = pdb.GetBytes(32);
            alg.IV = pdb.GetBytes(16);

            CryptoStream cs = new CryptoStream(fsOut,
               alg.CreateEncryptor(), CryptoStreamMode.Write);


            byte[] buffer = new byte[BUFFER_LENGHT];
            int bytesRead;

            do
            {
                bytesRead = fsIn.Read(buffer, 0, BUFFER_LENGHT);
                cs.Write(buffer, 0, bytesRead);
            } while (bytesRead != 0);
            cs.Close();
            fsIn.Close();
        }

        /// <summary>
        /// Decrypt data
        /// </summary>
        /// <param name="cipherData">data to decrypt</param>
        /// <param name="Key">decrypt key</param>
        /// <param name="IV">initialization vector</param>
        /// <returns>decrypted data</returns>
        public static byte[] Decrypt(byte[] cipherData,
           byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;

            CryptoStream cs = new CryptoStream(ms,
               alg.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(cipherData, 0, cipherData.Length);
            cs.Close();
            byte[] decryptedData = ms.ToArray();

            return decryptedData;
        }

        /// <summary>
        /// Decrypt string
        /// </summary>
        /// <param name="cipherText">string to decrypt</param>
        /// <param name="password">pasword used to encrypt</param>
        /// <returns>decrypted string</returns>
        public static string Decrypt(string cipherText, string password)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes pdb = GetDeriveBytes(password);

            byte[] decryptedData = Decrypt(cipherBytes,
               pdb.GetBytes(32), pdb.GetBytes(16));


            return Encoding.Unicode.GetString(decryptedData);
        }

        /// <summary>
        /// Decrypt data using password
        /// </summary>
        /// <param name="cipherData">data to decrypt</param>
        /// <param name="password">passwird used for encrypt</param>
        /// <returns>decrypted data</returns>
        public static byte[] Decrypt(byte[] cipherData, string password)
        {
            PasswordDeriveBytes pdb = GetDeriveBytes(password);
            return Decrypt(cipherData, pdb.GetBytes(32), pdb.GetBytes(16));
        }

        /// <summary>
        /// Decrypt file useing password
        /// </summary>
        /// <param name="fileIn">ecrypted file</param>
        /// <param name="fileOut">decrypted file</param>
        /// <param name="password">pasword used for encrypt</param>
        public static void Decrypt(string fileIn,
           string fileOut, string password)
        {
            FileStream fsIn = new FileStream(fileIn,
               FileMode.Open, FileAccess.Read);
            FileStream fsOut = new FileStream(fileOut,
               FileMode.OpenOrCreate, FileAccess.Write);

            PasswordDeriveBytes pdb = GetDeriveBytes(password);
            Rijndael alg = Rijndael.Create();

            alg.Key = pdb.GetBytes(32);
            alg.IV = pdb.GetBytes(16);

            CryptoStream cs = new CryptoStream(fsOut,
               alg.CreateDecryptor(), CryptoStreamMode.Write);

            byte[] buffer = new byte[BUFFER_LENGHT];
            int bytesRead;

            do
            {
                bytesRead = fsIn.Read(buffer, 0, BUFFER_LENGHT);
                cs.Write(buffer, 0, bytesRead);
            } while (bytesRead != 0);

            cs.Close();
            fsIn.Close();
        }

        private static PasswordDeriveBytes GetDeriveBytes(string password)
        {
            return new PasswordDeriveBytes(password,
               new byte[]
            {
               0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,
               0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
            });
        }
    }
}
