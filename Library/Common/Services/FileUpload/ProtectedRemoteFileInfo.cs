using System.ServiceModel;

namespace Services.FileUpload
{
   [MessageContract]
   public class ProtectedRemoteFileInfo : RemoteFileInfo
   {
      [MessageHeader(MustUnderstand = true)] public string UserName;

      [MessageHeader(MustUnderstand = true)] public string Password;
   }
}