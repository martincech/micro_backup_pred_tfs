﻿using System;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Services.Client
{
   /// <summary>
   /// Extension methods for <see cref="ChannelFactory{TChannel}"/> and <see cref="DuplexChannelFactory{TChannel}"/>
   /// </summary>
   public static class ChannelFactoryExtensions
   {
      /// <summary>
      /// Create client, do action and close client.
      /// </summary>
      /// <typeparam name="I">Service contract to be used for action</typeparam>
      /// <param name="factory">Factory to extend (creates channel)</param>
      /// <param name="action">Action to be executed on channel</param>
      public static void RemoteAction<I>(this IChannelFactory<I> factory, Action<I> action)
      {
         var commander = factory.CreateChannel();
         DoRemoteAction(factory, commander, action);
      }

      /// <summary>
      /// Create client, do action and close client.
      /// </summary>
      /// <typeparam name="I">Service contract to be used for action</typeparam>
      /// <param name="factory">Factory to extend (creates channel)</param>
      /// <param name="context">Object to be used as callback channel</param>
      /// <param name="action">Action to be executed on channel</param>
      public static void RemoteAction<I>(this DuplexChannelFactory<I> factory, object context, Action<I> action)
      {
         var commander = factory.CreateChannel(new InstanceContext(context));
         DoRemoteAction(factory, commander, action);
      }

      #region Helpers

      private static I CreateChannel<I>(this IChannelFactory<I> factory)
      {
         if (factory is ChannelFactory<I>)
         {
            return (factory as ChannelFactory<I>).CreateChannel();
         }
         var m = factory.GetType()
            .GetMethod("CreateChannel", BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance,
               null, new Type[] {}, null);
         if (m != null)
         {
            return (I) m.Invoke(factory, new object[] {});
         }
         return default(I);
      }

      private static void DoRemoteAction<I>(this IChannelFactory<I> factory, I commander, Action<I> action)
      {
         try
         {
// ReSharper disable once CompareNonConstrainedGenericWithNull
            if (commander == null || commander.Equals(default(I)))
            {
               throw new InvalidOperationException(string.Format(
                  "Factory of {0} must implement method CreateChannel()", factory.GetType()));
            }

            action(commander);
            ((ICommunicationObject) commander).Close();
         }
         catch (InvalidOperationException ex)
         {
            throw;
         }
         catch (CommunicationException)
         {
// ReSharper disable once PossibleNullReferenceException
            ((ICommunicationObject)commander).Abort();
         }
         catch (TimeoutException)
         {
// ReSharper disable once PossibleNullReferenceException
            ((ICommunicationObject)commander).Abort();
         }
         catch (Exception)
         {
// ReSharper disable once PossibleNullReferenceException
            ((ICommunicationObject)commander).Abort();
            throw;
         }
      }

      #endregion

   }
}