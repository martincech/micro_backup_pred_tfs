﻿namespace GUI
{
   public interface ITextBox
   {
      string Text { get; set; }
      void AppendText(string text);
   }
}
