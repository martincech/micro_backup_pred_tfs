﻿using System.Diagnostics;
using Usb.Platform.Windows;

namespace Usb
{
   internal sealed class UsbDeviceLoader
   {
      public static readonly UsbDeviceManager Instance;

      static UsbDeviceLoader()
      {
         foreach (var usbDeviceManager in new UsbDeviceManager[]
         {
            new WinUsbDeviceManager(),
         })
         {
            Debug.Assert(usbDeviceManager is IPlatformSupport);
            if (!(usbDeviceManager as IPlatformSupport).IsSupported) continue;
            Instance = usbDeviceManager;
            break;
         }
      }

   }
}
