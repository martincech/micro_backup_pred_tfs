﻿namespace Usb.Enums
{
   public enum Handshake
   {
      None,
      XOnXOff,
      RequestToSend,
      RequestToSendXOnXOff,
      Hardware
   }
}
