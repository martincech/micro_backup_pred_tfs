﻿using System;

namespace Usb.FTDI.Platform.Unsupported
{
// ReSharper disable once InconsistentNaming
   internal class UnsupportedFTDIManager : UsbManager<FTDIDevice>
   {
      public override bool IsSupported
      {
         get { return true; }
      }

      protected override object[] Refresh()
      {
         return new object[0];
      }

      protected override bool TryCreateDevice(object key, out FTDIDevice device, out object creationState)
      {
         throw new NotImplementedException();
      }

      protected override void CompleteDevice(object key, FTDIDevice device, object creationState)
      {
         throw new NotImplementedException();
      }
   }
}