﻿using System;
using System.IO;

namespace Usb.FTDI
{
// ReSharper disable once InconsistentNaming
   public abstract class FTDIDevice : UsbDevice
   {
      /// <summary>
      /// Makes a connection to the USB HID class device, or throws an exception if the connection cannot be made.
      /// </summary>
      /// <returns>The stream to use to communicate with the device.</returns>
      public abstract FTDIStream Open();

#if DEBUG
      public virtual FTDIStream Open(Stream testStream)
      {
         throw new NotImplementedException();
      }
#endif
   }
}
