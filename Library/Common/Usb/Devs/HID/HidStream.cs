﻿#region License

/* Copyright 2012-2013 James F. Bellinger <http://www.zer7.com/software/hidsharp>

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. */

#endregion

using System;
using System.IO;
using System.Runtime.InteropServices;

#pragma warning disable 420

namespace Usb.HID
{
   /// <summary>
   /// Communicates with a USB HID class device.
   /// </summary>
   [ComVisible(true), Guid("0C263D05-0D58-4c6c-AEA7-EB9E0C5338A2")]
   public abstract class HidStream : UsbStream
   {
      protected enum HidReports
      {
         BasicReport,
         FeatureReport
      }
      /// <exclude />
      public override void Flush()
      {
      }

      /// <summary>
      /// Sends a Get Feature setup request.
      /// </summary>
      /// <param name="buffer">The buffer to fill. Place the Report ID in the first byte.</param>
      public virtual void GetFeature(byte[] buffer)
      {
         Throw.If.Null(buffer, "buffer");
         GetFeature(buffer, 0, buffer.Length);
      }

      /// <summary>
      /// Sends a Get Feature setup request.
      /// </summary>
      /// <param name="buffer">The buffer to fill. Place the Report ID in the byte at index <paramref name="offset"/>.</param>
      /// <param name="offset">The index in the buffer to begin filling with data.</param>
      /// <param name="count">The number of bytes in the feature request.</param>
      public abstract void GetFeature(byte[] buffer, int offset, int count);

      /// <summary>
      /// Reads HID Input Reports.
      /// </summary>
      /// <returns>The data read.</returns>
      public byte[] Read()
      {
         var buffer = new byte[Device.MaxInputReportLength];
         var bytes = Read(buffer);
         Array.Resize(ref buffer, bytes);
         return buffer;
      }

      /// <summary>
      /// Reads HID Input Reports.
      /// </summary>
      /// <param name="buffer">The buffer to place the reports into.</param>
      /// <returns>The number of bytes read.</returns>
      public virtual int Read(byte[] buffer)
      {
         Throw.If.Null(buffer, "buffer");
         return Read(buffer, 0, buffer.Length);
      }



      /// <exclude />
      public override long Seek(long offset, SeekOrigin origin)
      {
         throw new NotSupportedException();
      }

      /// <summary>
      /// Sends a Set Feature setup request.
      /// </summary>
      /// <param name="buffer">The buffer of data to send. Place the Report ID in the first byte.</param>
      public void SetFeature(byte[] buffer)
      {
         Throw.If.Null(buffer, "buffer");
         SetFeature(buffer, 0, buffer.Length);
      }

      /// <summary>
      /// Sends a Set Feature setup request.
      /// </summary>
      /// <param name="buffer">The buffer of data to send. Place the Report ID in the byte at index <paramref name="offset"/>.</param>
      /// <param name="offset">The index in the buffer to start the write from.</param>
      /// <param name="count">The number of bytes in the feature request.</param>
      public abstract void SetFeature(byte[] buffer, int offset, int count);

      /// <exclude />
      public override void SetLength(long value)
      {
         throw new NotSupportedException();
      }

      /// <summary>
      /// Writes an HID Output Report to the device.
      /// </summary>
      /// <param name="buffer">The buffer containing the report. Place the Report ID in the first byte.</param>
      public void Write(byte[] buffer)
      {
         Throw.If.Null(buffer, "buffer");
         Write(buffer, 0, buffer.Length);
      }

      /// <exclude />
      public override bool CanRead
      {
         get { return true; }
      }

      /// <exclude />
      public override bool CanSeek
      {
         get { return false; }
      }

      /// <exclude />
      public override bool CanWrite
      {
         get { return true; }
      }

      /// <exclude />
      public override bool CanTimeout
      {
         get { return true; }
      }

      /// <summary>
      /// Gets the <see cref="HidDevice"/> associated with this stream.
      /// </summary>
      public abstract HidDevice Device { get; }

      /// <exclude />
      public override long Length
      {
         get { throw new NotSupportedException(); }
      }

      /// <exclude />
      public override long Position
      {
         get { throw new NotSupportedException(); }
         set { throw new NotSupportedException(); }
      }
   }
}