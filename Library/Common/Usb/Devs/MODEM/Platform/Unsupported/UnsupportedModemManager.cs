﻿using System;

namespace Usb.MODEM.Platform.Unsupported
{
   internal class UnsupportedModemManager : UsbManager<ModemDevice>
   {
      public override bool IsSupported
      {
         get { return true; }
      }

      protected override object[] Refresh()
      {
         return new object[0];
      }

      protected override bool TryCreateDevice(object key, out ModemDevice device, out object creationState)
      {
         throw new NotImplementedException();
      }

      protected override void CompleteDevice(object key, ModemDevice device, object creationState)
      {
         throw new NotImplementedException();
      }
   }
}
