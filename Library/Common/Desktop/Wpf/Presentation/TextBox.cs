﻿using System;
using System.Windows;
using System.Windows.Controls;
using GUI;

namespace Desktop.Wpf.Presentation
{
   public class TextBox : System.Windows.Controls.TextBox, ITextBox, IView
   {

      protected override void OnInitialized(EventArgs e)
      {
         base.OnInitialized(e);
         VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
         HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
      }

      protected override void OnTextChanged(TextChangedEventArgs e)
      {
         base.OnTextChanged(e);
         CaretIndex = Text.Length;
         ScrollToEnd();
      }

      public new string Text {
         get { return base.Text; }
         set { DispatcherHelper.RunAsync(() => { base.Text = value; }); }
      }

      public new void AppendText(string text)
      {
         DispatcherHelper.RunAsync(() => {base.AppendText(text); });
      }

      #region Implementation of IView

      /// <summary>
      /// Show this view
      /// </summary>
      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      /// <summary>
      /// Hide this view
      /// </summary>
      public void Hide()
      {
         Visibility = Visibility.Hidden;
      }

      #endregion
   }
}
