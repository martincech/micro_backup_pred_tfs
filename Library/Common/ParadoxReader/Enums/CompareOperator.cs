﻿namespace ParadoxReader.Enums
{
   public enum CompareOperator
   {
      Less,
      LessOrEqual,
      Equal,
      GreaterOrEqual,
      Greater,
      NotEqual
   }
}
