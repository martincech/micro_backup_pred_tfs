﻿using System.Linq;

namespace Utilities.Repository
{
   public interface IRepository<T> where T : class
   {
      bool Add(T o);
      bool Update(T o);
      bool Delete(T o);

      IQueryable<T> All();
   }
}
