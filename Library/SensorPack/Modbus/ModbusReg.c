//******************************************************************************
//
//   ModbusReg.c      Modbus register map and access functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusReg.h"
#include "ModbusSensorDataGroup.h"
#include "ModbusWeightDataGroup.h"
#include <string.h>


TModbusVersion ModbusVersion = { 1, 0};


// Locals :


//------------------------------------------------------------------------------
//  Register init
//------------------------------------------------------------------------------
void ModbusRegsInit( void)
// init modbus registers
{
}

//------------------------------------------------------------------------------
//  Check correct read register number
//------------------------------------------------------------------------------
TYesNo ModbusRegReadIsCorrectNum( EModbusRegNum R)
// checks read register number and return YES/NO whether its number is correct or not
{
   if(         
      // Sensor data
      (R < MODBUS_REG_SENSOR_DATA_TEMPERATURE ||
       R > MODBUS_REG_SENSOR_DATA_AMONIA) && 
      // Weight data
      (R < MODBUS_REG_WEIGHT_DATA_SAMPLES_RATE ||
       R > MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_128)

     ){
     return NO;
   }
   return YES;
}

word ModbusRegRead( EModbusRegNum R)
// read register value
{
   if( R <= MODBUS_REG_SENSOR_DATA_AMONIA){
       return ModbusRegReadSensorData( R); 
   }
   if( R <= MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_128){
       return ModbusRegReadWeightData( R); 
   }
   return 0;
}

//------------------------------------------------------------------------------
//  Check correct write register number
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteIsCorrectNum( EModbusRegNum R)
// checks write register number and return YES/NO whether its number is correct or not
{
   if(         
1
     ){
     return NO;
   }
   return YES;
}

TYesNo ModbusRegWrite( EModbusRegNum R, word D)
// write register value
{
   return NO;
}




