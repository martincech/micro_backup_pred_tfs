//*****************************************************************************
//
//   FramSpi.h   Win32 FRAM memory fake SPI interface
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FramSpi_H__
   #define __FramSpi_H__
   
#include "Fram/FramFake.h"
   
#define spiFramInit()
// Initialize bus

#define spiFramAttach()
// Attach SPI bus - activate chipselect

#define spiFramRelease()
// Release SPI bus - deactivate chipselect

#define spiFramByteRead()          _FramByteRead()
// Returns byte from SPI

#define spiFramByteWrite( Value)   _FramByteWrite( Value)
// Write byte to SPI

#endif
