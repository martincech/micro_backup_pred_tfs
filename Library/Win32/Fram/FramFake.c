//*****************************************************************************
//
//    FramFake.c   Fake FRAM services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "FramFake.h"

static dword _Address;
static byte  _Array[ FRAM_SIZE];

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void FramInit()
// Initialization
{
   _Address = 0;
} // FramInit

//-----------------------------------------------------------------------------
// Check for presence
//-----------------------------------------------------------------------------

TYesNo FramIsPresent( void)
// Returns YES if memory is present
{
   return( YES);
} // FramIsPresent

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

byte FramByteRead( dword Address)
// Returns single byte from <Address>
{
byte Value;

   FramBlockReadStart( Address);       // block start
   Value = FramBlockReadData();        // write data byte
   FramBlockReadStop();                // stop block read
   return( Value);
} // FramByteRead

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void FramByteWrite( dword Address, byte Value)
// Write <Value> at <Address>
{
   FramPageWriteStart( Address);       // start page write
   FramPageWriteData( Value);          // write data byte
   FramPageWritePerform();             // save data
} // FramByteWrite

//-----------------------------------------------------------------------------
// Page read
//-----------------------------------------------------------------------------

void FramBlockReadStart( dword Address)
// Block read start
{
   _Address = Address;
} // FramBlockReadStart

//-----------------------------------------------------------------------------
// Zapis Stranky do pameti
//-----------------------------------------------------------------------------

void FramPageWriteStart( dword Address)
// Start page write at <Address>
{
   _Address = Address;
} // FramPageWriteStart

//-----------------------------------------------------------------------------
// Sleep
//-----------------------------------------------------------------------------

void FramSleep( void)
// Enter sleep mode
{
} // FramSleep

//-----------------------------------------------------------------------------
// Wakeup
//-----------------------------------------------------------------------------

void FramWakeup( void)
// Wakeup from sleep mode
{
} // FramWakeup

//-----------------------------------------------------------------------------
// Fake SPI
//-----------------------------------------------------------------------------

byte _FramByteRead( void)
// Fake SPI Read byte
{
   return( _Array[ _Address++]);
} // _FlashByteRead

void _FramByteWrite( byte Value)
// Fake SPI write byte <Value>
{
   _Array[ _Address++] = Value;
} // _FlashByteWrite

byte *_FramArray( void)
// Returns array address
{
   return( _Array);
} // _FramArray   
