//*****************************************************************************
//
//    FramFake.h   Fake FRAM services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FramFake_H__
   #define __FramFake_H__

#include "Fram/Fram.h"

#ifdef __cplusplus
   extern "C" {
#endif
//-----------------------------------------------------------------------------
// Fake SPI
//-----------------------------------------------------------------------------
   
byte _FramByteRead( void);
// Fake SPI Read byte

void _FramByteWrite( byte Value);
// Fake SPI write byte <Value>

byte *_FramArray( void);
// Returns array address

//-----------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
