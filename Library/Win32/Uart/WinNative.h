//******************************************************************************
//
//   WinNative.h  Uart native communication
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef WINNATIVE_H
   #define WINNATIVE_H

#ifndef WINUART_H
   #include "Uart/WinUart.h"
#endif

#ifndef __UartNative_H__
   #include "Uart/NativeDef.h"
#endif

#ifndef CRTDUMP_H
   #include "Crt/CrtDump.h"
#endif

//------------------------------------------------------------------------------
//  WinNative
//------------------------------------------------------------------------------

class WinNative : public WinUart
{
public :
   WinNative();
   // Constructor

   ~WinNative();
   // Destructor

   void setLogger( CrtDump *logger);
   // Set data visualisation <logger>

   void setRxTimeoutShow( TYesNo enable);
   // Enable Rx timeout logging

   TYesNo send( TNativeAddress address, const void *data, int size);
   // Send <data> with <size> to <address>
   
   TYesNo receive( TNativeAddress *address, void *data, int *size);
   // Receive <data> with <size> from <address>

protected :
   TNativeHeaderCrc headerCrc( TNativeFrame *frame);
   word dataCrc( TNativeFrame *frame);

   CrtDump *_logger;
   TYesNo   _rxTimeoutShow;
}; // WinNative

#endif
