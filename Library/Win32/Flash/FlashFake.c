//*****************************************************************************
//
//   FlashFake.c   Fake Flash memory
//   Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "FlashFake.h"
#include <string.h>

static dword _Address;
static byte  _Array[ FLASH_SIZE];

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void FlashInit()
// Initialization
{
   _Address = 0;
} // FlashInit

//-----------------------------------------------------------------------------
// Wait
//-----------------------------------------------------------------------------

TYesNo FlashWaitForReady( void)
// Wait for ready
{
   return( YES);
} // FlashWaitForReady

//-----------------------------------------------------------------------------
// Identification
//-----------------------------------------------------------------------------

void FlashIdentification( TFlashDevice *Device)
// Read <Device> identification
{
   memset( Device, 0, sizeof( TFlashDevice));
} // FlashIdentification	

//-----------------------------------------------------------------------------
// Start read
//-----------------------------------------------------------------------------

void FlashBlockReadStart( dword Address)
// Start sequential read from flash array
{
   _Address = Address;
} // FlashBlockReadStart

//-----------------------------------------------------------------------------
// Start write
//-----------------------------------------------------------------------------

void FlashBlockWriteStart( dword Address)
// Start block write
{
   _Address = Address;
} // FlashBlockWriteStart

//-----------------------------------------------------------------------------
// Erase
//-----------------------------------------------------------------------------

void FlashBlockErase( dword Address)
// Erase block at <Address>
{
   // align to page boundary :
   Address /= FLASH_ERASE_SIZE;
   Address *= FLASH_ERASE_SIZE;
   memset( &_Array[ Address], 0xFF, FLASH_ERASE_SIZE);
} // FlashBlockErase

//-----------------------------------------------------------------------------
// Erase all
//-----------------------------------------------------------------------------

TYesNo FlashEraseAll( void)
// Erase all device
{
   memset( _Array, 0xFF, FLASH_SIZE);
   return( YES);
} // FlashEraseAll	

//-----------------------------------------------------------------------------
// Fake SPI
//-----------------------------------------------------------------------------

byte _FlashByteRead( void)
// Fake SPI Read byte
{
   return( _Array[ _Address++]);
} // _FlashByteRead

void _FlashByteWrite( byte Value)
// Fake SPI write byte <Value>
{
   _Array[ _Address++] = Value;
} // _FlashByteWrite

byte *_FlashArray( void)
// Returns array address
{
   return( _Array);
} // _FlashArray   
