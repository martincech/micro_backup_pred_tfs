//*****************************************************************************
//
//   FlashSpi.h  Win32 Flash memory fake SPI interface
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FlashSpi_H__
   #define __FlashSpi_H__

#include "Flash/FlashFake.h"
   
#define spiFlashInit()              
// Initialize bus

#define spiFlashAttach()            
// Attach SPI bus - activate chipselect

#define spiFlashRelease()           
// Release SPI bus - deactivate chipselect

#define spiFlashByteRead()          _FlashByteRead()
// Returns byte from SPI

#define spiFlashByteWrite( Value)   _FlashByteWrite( Value)
// Write byte to SPI

#endif
