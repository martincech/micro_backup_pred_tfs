//*****************************************************************************
//
//    IRtc.c       Kinetis Real Time Clock
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Rtc/Rtc.h"
#include "System/System.h"
#include "Cpu/Cpu.h"
#include "Hardware.h"

static inline TYesNo IsIncrementingEnabled( void)     {return (RTC->SR & RTC_SR_TCE_MASK) ? YES : NO;}
static inline TYesNo IsTimeOverflow( void)   {return (RTC->SR & RTC_SR_TOF_MASK) ? YES : NO;}
static inline TYesNo IsTimeInvalid( void)     {return (RTC->SR & RTC_SR_TIF_MASK) ? YES : NO;}
static inline TYesNo IsRtcAlive( void)  {return IsIncrementingEnabled();}

static inline void Reset( void)          {RTC->CR |= RTC_CR_SWR_MASK; RTC->CR = 0;}   
static inline void ConfigureModule(dword Value) {RTC->CR = Value;}
#define CAPACITOR_8PF       RTC_CR_SC8P_MASK
#define OSCILLATOR_ENABLE   RTC_CR_OSCE_MASK

static inline void OscillatorWaitForStabilization( void) {SysDelay(10);}
static inline void EnableIncrementing( TYesNo Enable) {RTC->SR = (Enable ? RTC_SR_TCE_MASK : 0);}
static inline void EnableOverflowAndInvalidInterrupts( void)   {RTC->IER = RTC_IER_TOIE_MASK | RTC_IER_TIIE_MASK;}
static inline void DisableRtcInterrupts( void) { RTC->IER = 0;}

static inline dword GetTime( void)        {return RTC->TSR;}
static inline void SetTime( dword Value)  {EnableIncrementing(NO); RTC->TSR = Value; EnableIncrementing(YES);}

void __irq RTC_IRQHandler( void);
// ISR

static TYesNo Invalid;

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void RtcInit( void)
// Initialize bus and RTC
{
   RtcClockEnable();
   EnableOverflowAndInvalidInterrupts();
   CpuIrqAttach(RTC_IRQn, 5, RTC_IRQHandler);
   CpuIrqEnable(RTC_IRQn);
} // RtcInit

//-----------------------------------------------------------------------------
// Load
//-----------------------------------------------------------------------------

UDateTimeGauge RtcLoad( void)
// Returns RTC time
{
   return GetTime();
} // RtcLoad

//-----------------------------------------------------------------------------
// Save
//-----------------------------------------------------------------------------

void RtcSave( UDateTimeGauge DateTime)
// Set RTC time by <Time>
{
   Invalid = NO;
   SetTime(DateTime);
   EnableOverflowAndInvalidInterrupts();
} // RtcSave

//-----------------------------------------------------------------------------
// Invalid
//-----------------------------------------------------------------------------

TYesNo RtcInvalid( void)
// is invalid time?
{
   return Invalid;
} // RtcInvalid

//******************************************************************************

//-----------------------------------------------------------------------------
// ISR
//-----------------------------------------------------------------------------

void __irq RTC_IRQHandler( void)
// ISR
{
   Invalid = YES;
   Reset();
   ConfigureModule(CAPACITOR_8PF | OSCILLATOR_ENABLE);
   OscillatorWaitForStabilization();
   DisableRtcInterrupts();
} // Handler