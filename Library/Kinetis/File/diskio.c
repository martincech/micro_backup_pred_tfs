/**HEADER********************************************************************
* 
* Copyright (c) 2010 Freescale Semiconductor;
* All Rights Reserved
*
*************************************************************************** 
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************
*
* $FileName: diskio.c$
* $Version : 
* $Date    : 
*
* Comments: This file implements low level disk interface module
*
*   
*
*END************************************************************************/

#include "diskio.h"		/* Common include file for FatFs and disk I/O layer */
#include "usb_host_msd_ufi.h"
#include "khci.h"
#include "hidef.h"

#define printf (void)

/**************************************************************************
   Global variables
**************************************************************************/
volatile DEVICE_STRUCT   mass_device = { 0 };   /* mass storage device struct */

/**************************************************************************
*   Local variables
**************************************************************************/
static volatile boolean       bCallBack = FALSE;
static volatile USB_STATUS    bStatus = USB_OK;
/* Command object used in SCSI commands */
static COMMAND_OBJECT_STRUCT  pCmd; 

static byte DiskStatus;


static TYesNo CmdTestUnitReady( void);
static TYesNo CmdWrite10( dword sector, void *buff, byte SectorCount);
static TYesNo CmdRead10( dword sector, void *buff, byte SectorCount);
static TYesNo CmdReadCapacity(dword *LastBlock, dword *BlockLength);
static TYesNo CmdInquiry( void);
static TYesNo CmdGetMaxLun( byte *LunCount);
static TYesNo CmdRequestSense( byte *SenseKey, byte *SenseKeyAdd);


 static   dword SectorSize;
 static   dword SectorCount;
 static  dword BlockSize;





static DRESULT disk_flush( void);











/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : callback_bulk_pipe
* Returned Value : 
* Comments       : Initialize Disk Drive (always successful) 
*
*END*--------------------------------------------------------------------*/
DSTATUS disk_initialize 
	(
	  /* [IN] Physical drive number (0) */
		uint_8 drv		
	)
{
  static CBW_STRUCT_PTR cbw_ptr = NULL; 
  static CSW_STRUCT_PTR csw_ptr = NULL;
    
  /* Allocate memory for Command Block Wrapper and Command Status Wrapper */
  
  if(NULL == cbw_ptr)
  {
    cbw_ptr = (CBW_STRUCT_PTR) malloc(sizeof(CBW_STRUCT));      
  }
  
   pCmd.CBW_PTR = cbw_ptr;
  
  if(NULL == pCmd.CBW_PTR)
   {
     return RES_NOT_ENOUGH_CORE;
   }
  
  if(NULL == csw_ptr)
  {
    csw_ptr = (CSW_STRUCT_PTR) malloc(sizeof(CSW_STRUCT));
  }
  
  pCmd.CSW_PTR = csw_ptr;
  
  if(NULL == pCmd.CSW_PTR)
  {
      return RES_NOT_ENOUGH_CORE;    
  }
     
  /* Store the address of CBW and CSW */
  cbw_ptr = pCmd.CBW_PTR;
  csw_ptr = pCmd.CSW_PTR;
  

  memset(pCmd.CSW_PTR, 0, sizeof(CSW_STRUCT));
  memset(pCmd.CBW_PTR, 0, sizeof(CBW_STRUCT));
  memset(&pCmd, 0, sizeof(COMMAND_OBJECT_STRUCT));

  /* Init SCSI command object */
  pCmd.CBW_PTR  = cbw_ptr;
  pCmd.CSW_PTR  = csw_ptr;
  pCmd.LUN      = drv;
  pCmd.CALL_PTR = (pointer)&mass_device.class_intf;
  pCmd.CALLBACK = callback_bulk_pipe;
  
   DiskStatus = STA_NOINIT;

byte SenseKey;
byte SenseKeyAdditional;
byte MaxLun;
byte Trial;

// nejde verbatim 8gb
// mount proběhne, ale write nejde
/*byte Buffer[512];
   if(!CmdGetMaxLun(&MaxLun)) {
      return RES_ERROR;
   }
   if(!CmdInquiry()) {
      return RES_ERROR;
   }

   if(!CmdRead10(0, Buffer, 512)) {
      return RES_ERROR;
   }

   //CmdReadCapacity(&SectorCount, &SectorSize);



   for(Trial = 0 ; Trial < 3 ; Trial++) {
      if(!CmdTestUnitReady()) {
         continue;
      }
      break;
   }


   if(!CmdGetMaxLun(&MaxLun)) {
      return RES_ERROR;
   }
   if(!CmdInquiry()) {
      return RES_ERROR;
   }
   if(!CmdRead10(0, Buffer, 512)) {
      return RES_ERROR;
   }

   if(!CmdRequestSense(&SenseKey, &SenseKeyAdditional)) {
      return RES_ERROR;
   }

   for(Trial = 0 ; Trial < 3 ; Trial++) {
      if(!CmdTestUnitReady()) {
         continue;
      }
      if(!CmdReadCapacity(&SectorCount, &SectorSize)) {
         return RES_ERROR;
      }
      break;
   }

   if(Trial == 3) {
      return RES_ERROR;
   }*/


// nejde verbatim 8gb
// prvni mount neproběhne (testunitready), druhy ano, ale write nejde
   if(!CmdInquiry()) {
      return RES_ERROR;
   }
   
   for(Trial = 0 ; Trial < 3 ; Trial++) {
      if(!CmdTestUnitReady()) {
         continue;
      }

      break;
   }

   if(Trial == 3) {
      return RES_ERROR;
   }

   if(!CmdGetMaxLun(&MaxLun)) {
      return RES_ERROR;
   }

   if(!CmdTestUnitReady()) {
      return RES_ERROR;
   }

   if(!CmdRequestSense(&SenseKey, &SenseKeyAdditional)) {
      return RES_ERROR;
   }

   if(!CmdInquiry()) {
      return RES_ERROR;
   }

   if(!CmdRequestSense(&SenseKey, &SenseKeyAdditional)) {
      return RES_ERROR;
   }

   if(!CmdReadCapacity(&SectorCount, &SectorSize)) {
      return RES_ERROR;
   }

   if(!CmdTestUnitReady()) {
      return RES_ERROR;
   }

   if(!CmdRequestSense(&SenseKey, &SenseKeyAdditional)) {
      return RES_ERROR;
   }

   if(!CmdReadCapacity(&SectorCount, &SectorSize)) {
      return RES_ERROR;
   }

   BlockSize = 1;
   DiskStatus = STA_OK;

   return RES_OK;
}

/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : disk_read
* Returned Value : 
* Comments       : Read Sector(s) 
*
*END*--------------------------------------------------------------------*/
DRESULT disk_read 
  (
    /* [IN] Physical drive number (0) */
  	uint_8 drv,			
  	/* [OUT] Pointer to the data buffer to store read data */
  	uint_8 *buff,			
  	/* [IN] Start sector number (LBA) */
  	uint_32 sector,		
  	/* [IN] Sector count (1..128) */
  	uint_8 count			
  )
{
   printf("R-%d-%d\n", count, sector);
   disk_flush();
   if(!CmdRead10(sector, buff, count)) {
      return RES_ERROR;
   }
   return RES_OK;
}


/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : disk_write
* Returned Value : 
* Comments       : Write Sector(s) 
*
*END*--------------------------------------------------------------------*/

#define CACHE_SECTOR_LENGTH      32
#define CACHE_LENGTH             512 * CACHE_SECTOR_LENGTH

uint_8 Cache[CACHE_LENGTH];
dword LastSector = -1;
dword CachePtr = 0;

DRESULT disk_write 
  (
    /* [IN] Physical drive nmuber (0) */
  	uint_8 drv,		
  	/* [IN] Pointer to the data to be written */	
  	uint_8 *buff,	
  	/* [IN] Start sector number (LBA) */
  	uint_32 sector,		
  	/* [IN] Sector count (1..128) */
  	uint_8 count			
  )
{
   if(!CmdWrite10(sector, buff, count)) {
      return RES_ERROR;
   }

   return RES_OK;


   printf("C-%d-%d\n", count, sector);
   if(sector != LastSector + 1) {
      if(CachePtr) {
         if(!CmdWrite10(LastSector - CachePtr + 1, Cache, CachePtr)) {
            return RES_ERROR;
         }
         CachePtr = 0;
      }
   }

   while(count) {
      byte NumberOfCachedSectors =  CACHE_SECTOR_LENGTH - CachePtr;

      if(NumberOfCachedSectors > count) {
         NumberOfCachedSectors = count;
      }

      memcpy(&Cache[CachePtr * 512], buff, NumberOfCachedSectors * 512);
      count -= NumberOfCachedSectors;
      CachePtr += NumberOfCachedSectors;
      LastSector = sector;
      sector += NumberOfCachedSectors;

      if(CachePtr == CACHE_SECTOR_LENGTH) {
         if(!CmdWrite10(LastSector - CachePtr + 1, Cache, CachePtr)) {
            return RES_ERROR;
         }
         CachePtr = 0;
      }
   }

   return RES_OK;

/*
   if(!CmdWrite10(sector, buff, count)) {
         return RES_ERROR;
   }

   return RES_OK;*/
}


DRESULT disk_flush( void) {
   if(CachePtr) {
      if(!CmdWrite10(LastSector - CachePtr + 1, Cache, CachePtr)) {
         return RES_ERROR;
      }
      CachePtr = 0;
   }

byte SenseKey;
byte SenseKeyAdditional;

   CmdRequestSense(&SenseKey, &SenseKeyAdditional);

   return RES_OK;
}



/*FUNCTION*----------------------------------------------------------------
*
* Function Name  : disk_ioctl
* Returned Value : 
* Comments       : The disk_ioctl function controls device specified features
*                  and miscellaneous functions other than disk read/write 
*
*END*--------------------------------------------------------------------*/

typedef union {
   word SectorSize;
   dword SectorCount;
   dword BlockSize;
} TFatFsIoctlUnion;

DRESULT disk_ioctl 
(
  /* [IN] Drive number */
  uint_8 Drive,      
  /* [IN] Control command code */
  uint_8 Command, 
  /* [IN/OUT] Parameter and data buffer */   
  void* Buff     
)
{
   TFatFsIoctlUnion *FatFsUnion = (TFatFsIoctlUnion *) Buff;
   UNUSED(Drive);

   if(DiskStatus != STA_OK) {
      return RES_ERROR;
   }

   switch(Command) {
      case GET_SECTOR_SIZE:
         FatFsUnion->SectorSize = SectorSize;
         return RES_OK;

      case GET_SECTOR_COUNT:
         FatFsUnion->SectorCount = SectorCount;
         return RES_OK;

      case GET_BLOCK_SIZE:
         FatFsUnion->BlockSize = BlockSize;
         return RES_OK;

      case CTRL_SYNC:
         return disk_flush();
   }

   return RES_ERROR;
}

DSTATUS disk_status( uint_8 Drive)
{
   if(Drive > 0) {
      return STA_NOINIT;		/* Supports only drive 0 */
   }
   
   return DiskStatus;
}

void callback_bulk_pipe
   (
      /* [IN] Status of this command */
      USB_STATUS status,

      /* [IN] pointer to USB_MASS_BULK_ONLY_REQUEST_STRUCT*/
      pointer p1,

      /* [IN] pointer to the command object*/
      pointer  p2,

      /* [IN] Length of data transmitted */
      uint_32 buffer_length
   )
{ /* Body */

   UNUSED(p1)
   UNUSED(p2)
   UNUSED(buffer_length)

   bCallBack = TRUE;
   bStatus = status;


switch(status) {
case  USB_OK:
   printf("USB_OK ");
   break;
case  USBERR_ALLOC:
   printf("USBERR_ALLOC ");
   break;
   case  USBERR_BAD_STATUS:
   printf("USBERR_BAD_STATUS ");
   break;
   case  USBERR_CLOSED_SERVICE:
   printf("USBERR_CLOSED_SERVICE ");
   break;
   case  USBERR_OPEN_SERVICE:
   printf("USBERR_OPEN_SERVICE ");
   break;
   case  USBERR_TRANSFER_IN_PROGRESS:
   printf("USBERR_TRANSFER_IN_PROGRESS ");
   break;
   case  USBERR_ENDPOINT_STALLED:
   printf("USBERR_ENDPOINT_STALLED ");
   break;
case  USBERR_ALLOC_STATE:
   printf("USBERR_ALLOC_STATE ");
   break;
   case  USBERR_DRIVER_INSTALL_FAILED:
   printf("USBERR_DRIVER_INSTALL_FAILED ");
   break;
   case  USBERR_DRIVER_NOT_INSTALLED:
   printf("USBERR_DRIVER_NOT_INSTALLED ");
   break;
   case  USBERR_INSTALL_ISR:
   printf("USBERR_INSTALL_ISR ");
   break;
   case  USBERR_INVALID_DEVICE_NUM:
   printf("USBERR_INVALID_DEVICE_NUM ");
   break;
   case  USBERR_ALLOC_SERVICE:
   printf("USBERR_ALLOC_SERVICE ");
   break;
   case  USBERR_INIT_FAILED:
   printf("USBERR_INIT_FAILED ");
   break;

case  USBERR_SHUTDOWN:
   printf("USBERR_SHUTDOWN ");
   break;
   case  USBERR_INVALID_PIPE_HANDLE:
   printf("USBERR_INVALID_PIPE_HANDLE ");
   break;
   case  USBERR_OPEN_PIPE_FAILED:
   printf("USBERR_OPEN_PIPE_FAILED ");
   break;
   case  USBERR_INIT_DATA:
   printf("USBERR_INIT_DATA ");
   break;
   case  USBERR_SRP_REQ_INVALID_STATE:
   printf("USBERR_SRP_REQ_INVALID_STATE ");
   break;
   case  USBERR_TX_FAILED:
   printf("USBERR_TX_FAILED ");
   break;
   case  USBERR_RX_FAILED:
   printf("USBERR_RX_FAILED ");
   break;
      case  USBERR_EP_INIT_FAILED:
   printf("USBERR_EP_INIT_FAILED ");
   break;
      case  USBERR_EP_DEINIT_FAILED:
   printf("USBERR_EP_DEINIT_FAILED ");
   break;
      case  USBERR_TR_FAILED:
   printf("USBERR_TR_FAILED ");
   break;
      case  USBERR_BANDWIDTH_ALLOC_FAILED:
   printf("USBERR_BANDWIDTH_ALLOC_FAILED ");
   break;
      case  USBERR_INVALID_NUM_OF_ENDPOINTS:
   printf("USBERR_INVALID_NUM_OF_ENDPOINTS ");
   break;
   case  USBERR_ADDRESS_ALLOC_FAILED:
   printf("USBERR_ADDRESS_ALLOC_FAILED ");
   break;
   case  USBERR_PIPE_OPENED_FAILED:
   printf("USBERR_PIPE_OPENED_FAILED ");
   break;
   case  USBERR_DEVICE_NOT_FOUND:
   printf("USBERR_DEVICE_NOT_FOUND ");
   break;
   case  USBERR_DEVICE_BUSY:
   printf("USBERR_DEVICE_BUSY ");
   break;
   case  USBERR_NO_DEVICE_CLASS:
   printf("USBERR_NO_DEVICE_CLASS ");
   break;
   case  USBERR_UNKNOWN_ERROR:
   printf("USBERR_UNKNOWN_ERROR ");
   break;
   case  USBERR_INVALID_BMREQ_TYPE:
   printf("USBERR_INVALID_BMREQ_TYPE ");
   break;
   case  USBERR_GET_MEMORY_FAILED:
   printf("USBERR_GET_MEMORY_FAILED ");
   break;
case  USBERR_INVALID_MEM_TYPE:
   printf("USBERR_INVALID_MEM_TYPE ");
   break;
   case  USBERR_NO_DESCRIPTOR:
   printf("USBERR_NO_DESCRIPTOR ");
   break;
   case  USBERR_NULL_CALLBACK:
   printf("USBERR_NULL_CALLBACK ");
   break;
   case  USBERR_NO_INTERFACE:
   printf("USBERR_NO_INTERFACE ");
   break;
   case  USBERR_INVALID_CFIG_NUM:
   printf("USBERR_INVALID_CFIG_NUM ");
   break;
   case  USBERR_INVALID_ANCHOR:
   printf("USBERR_INVALID_ANCHOR ");
   break;
   case  USBERR_INVALID_REQ_TYPE:
   printf("USBERR_INVALID_REQ_TYPE ");
   break;
   case  USBERR_ERROR:
   printf("USBERR_ERROR ");
   break;
case  USBERR_ALLOC_EP_QUEUE_HEAD:
   printf("USBERR_ALLOC_EP_QUEUE_HEAD ");
   break;
   case  USBERR_ALLOC_TR:
   printf("USBERR_ALLOC_TR ");
   break;
   case  USBERR_ALLOC_DTD_BASE:
   printf("USBERR_ALLOC_DTD_BASE ");
   break;
   case  USBERR_CLASS_DRIVER_INSTALL:
   printf("USBERR_CLASS_DRIVER_INSTALL ");
   break;
   default:
   printf("_UNKNOWN_ %d ", status);
   break;
}
printf("\n");
} /* Endbody */


static void usb_host_mass_ctrl_callback
   (
      /* [IN] pointer to pipe */
      _usb_pipe_handle  pipe_handle,

      /* [IN] user-defined parameter */
      pointer           user_parm,

      /* [IN] buffer address */
      uchar_ptr         buffer,

      /* [IN] length of data transferred */
      uint_32           buflen,

      /* [IN] status, hopefully USB_OK or USB_DONE */
      uint_32           status
   )
{ /* Body */

    UNUSED(pipe_handle)
    UNUSED(user_parm)
    UNUSED(buffer)
    UNUSED(buflen)
    
   bCallBack = TRUE;
   bStatus = status;

} /* Endbody */


#include "Time/uDateTime.h"
#include "System/System.h"

uint_32 get_fattime (void)
{
UDateTime DT;
   uDateTime( &DT, SysDateTime());        
        
        return	  ((uint_32)((2000 + DT.Date.Year - 1980) << 25)
			| ((uint_32)DT.Date.Month << 21)
			| ((uint_32)DT.Date.Day << 16)
			| ((uint_32)DT.Time.Hour << 11)
			| ((uint_32)DT.Time.Min << 5)
			| ((uint_32)DT.Time.Sec >> 1));
}



static inline void _CmdStart( void) {
   bCallBack = FALSE;
}


static TYesNo _CmdEnd( void) {
   if((bStatus != USB_OK) && (bStatus != USB_STATUS_TRANSFER_QUEUED)) {
      return NO;
   } else {
      while(!bCallBack) {
         Poll();
      }
      
      if(!bStatus) {
         return YES;
      } else {
         return NO;
      }         
   }
}


static TYesNo CmdTestUnitReady( void) {
   _CmdStart();
   bStatus = usb_mass_ufi_test_unit_ready(&pCmd);
   return _CmdEnd();
}

static TYesNo CmdWrite10( dword sector, void *buff, byte SectorCount) {
   printf("W-%d-%d\n", SectorCount, sector);
   _CmdStart();
   bStatus = usb_mass_ufi_write_10(&pCmd, sector, buff, (uint_32)(512*SectorCount), SectorCount);
   return _CmdEnd();
}

static TYesNo CmdRead10( dword sector, void *buff, byte SectorCount) {
   _CmdStart();
   bStatus = usb_mass_ufi_read_10(&pCmd, sector, buff, (uint_32)(512*SectorCount), SectorCount);
   return _CmdEnd();
}


static TYesNo CmdReadCapacity(dword *LastBlock, dword *BlockLength) {
   MASS_STORAGE_READ_CAPACITY_CMD_STRUCT_INFO  read_capacity;
   _CmdStart();
   bStatus = usb_mass_ufi_read_capacity(&pCmd, (uchar_ptr)&read_capacity, sizeof(MASS_STORAGE_READ_CAPACITY_CMD_STRUCT_INFO));

   TYesNo Result = _CmdEnd();

   *LastBlock = HOST_READ_BEOCT_32(read_capacity.BLLBA);
   *BlockLength = HOST_READ_BEOCT_32(read_capacity.BLENGTH);

   return Result;
}

static TYesNo CmdInquiry( void) {
   INQUIRY_DATA_FORMAT inquiry;
   _CmdStart();
   bStatus = usb_mass_ufi_inquiry(&pCmd, (uchar_ptr) &inquiry, sizeof(INQUIRY_DATA_FORMAT));
   return _CmdEnd();
}

static TYesNo CmdGetMaxLun( byte *MaxLun) {
   _CmdStart();
   bStatus = usb_class_mass_getmaxlun_bulkonly((pointer)&mass_device.class_intf, MaxLun, usb_host_mass_ctrl_callback);
   return _CmdEnd();
}





/*! \name Sense Key Definitions
 */
//! @{
#define SBC_SENSE_KEY_NO_SENSE                          0x00
#define SBC_SENSE_KEY_RECOVERED_ERROR                   0x01
#define SBC_SENSE_KEY_NOT_READY                         0x02
#define SBC_SENSE_KEY_MEDIUM_ERROR                      0x03
#define SBC_SENSE_KEY_HARDWARE_ERROR                    0x04
#define SBC_SENSE_KEY_ILLEGAL_REQUEST                   0x05
#define SBC_SENSE_KEY_UNIT_ATTENTION                    0x06
#define SBC_SENSE_KEY_DATA_PROTECT                      0x07
#define SBC_SENSE_KEY_BLANK_CHECK                       0x08
#define SBC_SENSE_KEY_VENDOR_SPECIFIC                   0x09
#define SBC_SENSE_KEY_COPY_ABORTED                      0x0A
#define SBC_SENSE_KEY_ABORTED_COMMAND                   0x0B
#define SBC_SENSE_KEY_VOLUME_OVERFLOW                   0x0D
#define SBC_SENSE_KEY_MISCOMPARE                        0x0E
//! @}

/*! \name Additional Sense Code Definitions
 */
//! @{
#define SBC_ASC_NO_ADDITIONAL_SENSE_INFORMATION         0x00
#define SBC_ASC_LOGICAL_UNIT_NOT_READY                  0x04
#define SBC_ASC_INVALID_FIELD_IN_CDB                    0x24
#define SBC_ASC_WRITE_PROTECTED                         0x27
#define SBC_ASC_FORMAT_ERROR                            0x31
#define SBC_ASC_INVALID_COMMAND_OPERATION_CODE          0x20
#define SBC_ASC_NOT_READY_TO_READY_CHANGE               0x28
#define SBC_ASC_MEDIUM_NOT_PRESENT                      0x3A
//! @}


static TYesNo CmdRequestSense( byte *SenseKey, byte *SenseKeyAdd) {
   REQ_SENSE_DATA_FORMAT req_sense;
   _CmdStart();
   bStatus = usb_mass_ufi_request_sense(&pCmd, &req_sense, sizeof(REQ_SENSE_DATA_FORMAT));
   TYesNo Result = _CmdEnd();

   *SenseKey = req_sense.BSENSE_KEY & 0x0F;
   *SenseKeyAdd = req_sense.BADITIONAL_CODE;
  /*switch (sense_key)
  {
      case SBC_SENSE_KEY_NOT_READY:
         if (sense_key_add == SBC_ASC_MEDIUM_NOT_PRESENT)
            return CTRL_NO_PRESENT;
         break;

      case SBC_SENSE_KEY_UNIT_ATTENTION:
         if (sense_key_add == SBC_ASC_NOT_READY_TO_READY_CHANGE)
            return CTRL_BUSY;
         if (sense_key_add == SBC_ASC_MEDIUM_NOT_PRESENT)
            return CTRL_NO_PRESENT;
         break;

      case SBC_SENSE_KEY_HARDWARE_ERROR:
      case SBC_SENSE_KEY_DATA_PROTECT:
         break;

      default:
         sense_key = SBC_SENSE_KEY_NO_SENSE;
         break;
  }*/

   return Result;
}
