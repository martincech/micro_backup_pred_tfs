//******************************************************************************
//
//   Efs.h           External File System
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "Cpu/Cpu.h"
#include "File/Efs.h"
#include "System/System.h"
#include "Timer/Timer.h"

#include "Usb/UsbModule.h"
#include "Usb/Usb.h"
#include "Multitasking/Multitasking.h"

#include "diskio.h"


#include "usb_bsp.h"
#include "usb_host_msd_bo.h"

//#include "Log/Logger.h"


typedef struct {
   byte EfsError;
   byte FatfsError;
} TEfsLogItem;

#define ATTACH_TIMEOUT     1800
#define CONNECT_TIMEOUT    1000
#define DETACH_TIMEOUT     1000

#define printf    (void)

#define _SetError(Error)  //LogEfs( Error);

static TYesNo _TranslateError(FRESULT Result) {
   switch(Result) {
      case FR_OK:
         printf("FR_OK\n");
         return YES;
      case FR_DISK_ERR:
         printf("FR_DISK_ERR\n");
         break;
      case FR_INT_ERR:
         printf("FR_INT_ERR\n");
         break;
      case FR_NOT_READY:
         printf("FR_NOT_READY\n");
         break;
      case FR_NO_FILE:
         printf("FR_NO_FILE\n");
         break;
      case FR_NO_PATH:
         printf("FR_NO_PATH\n");
         break;
      case FR_INVALID_NAME:
         printf("FR_INVALID_NAME\n");
         break;
      case FR_DENIED:
         printf("FR_DENIED\n");
         break;
      case FR_EXIST:
         printf("FR_EXIST\n");
         break;
      case FR_INVALID_OBJECT:
         printf("FR_INVALID_OBJECT\n");
         break;
      case FR_WRITE_PROTECTED:
         printf("FR_WRITE_PROTECTED\n");
         break;
      case FR_INVALID_DRIVE:
         printf("FR_INVALID_DRIVE\n");
         break;
      case FR_NOT_ENABLED:
         printf("FR_NOT_ENABLED\n");
         break;
      case FR_NO_FILESYSTEM:
         printf("FR_NO_FILESYSTEM\n");
         break;
      case FR_MKFS_ABORTED:
         printf("FR_MKFS_ABORTED\n");
         break;
      case FR_LOCKED:
         printf("FR_LOCKED\n");
         break;
      case FR_TIMEOUT:
         printf("FR_TIMEOUT\n");
         break;
      case FR_NOT_ENOUGH_CORE:
         printf("FR_NOT_ENOUGH_CORE\n");
         break;
     case FR_TOO_MANY_OPEN_FILES:
         printf("FR_TOO_MANY_OPEN_FILES\n");
         break;
      case FR_INVALID_PARAMETER:
         printf("FR_INVALID_PARAMETER\n");
         break;
      default:
         printf("_UNKNOWN_ %d\n", Result);
         break;
   }
   _SetError(EFS_ERROR_UNKNOWN | (Result << 8));
   return NO;
}

 /* USB device states */
#define  USB_DEVICE_IDLE                   (0)
#define  USB_DEVICE_ATTACHED               (1)
#define  USB_DEVICE_CONFIGURED             (2)
#define  USB_DEVICE_SET_INTERFACE_STARTED  (3)
#define  USB_DEVICE_INTERFACED             (4)
#define  USB_DEVICE_DETACHED               (5)
#define  USB_DEVICE_OTHER                  (6)

/**************************************************************************
   Global variables
**************************************************************************/
extern volatile DEVICE_STRUCT       mass_device;   /* mass storage device struct */

void   usb_host_mass_device_event (_usb_device_instance_handle,_usb_interface_descriptor_handle,uint_32);

const USB_HOST_DRIVER_INFO DriverInfoTable[] = {
   /* Floppy drive */
   {
      {0x00,0x00},                  /* Vendor ID per USB-IF             */
      {0x00,0x00},                  /* Product ID per manufacturer      */
      USB_CLASS_MASS_STORAGE,       /* Class code                       */
      USB_SUBCLASS_MASS_UFI,        /* Sub-Class code                   */
      USB_PROTOCOL_MASS_BULK,       /* Protocol                         */
      0,                            /* Reserved                         */
      usb_host_mass_device_event    /* Application call back function   */
   },

   /* USB 2.0 hard drive */
   {

      {0x49,0x0D},                  /* Vendor ID per USB-IF             */
      {0x00,0x30},                  /* Product ID per manufacturer      */
      USB_CLASS_MASS_STORAGE,       /* Class code                       */
      USB_SUBCLASS_MASS_SCSI,       /* Sub-Class code                   */
      USB_PROTOCOL_MASS_BULK,       /* Protocol                         */
      0,                            /* Reserved                         */
      usb_host_mass_device_event    /* Application call back function   */
   },
   {
      {0x00,0x00},                  /* All-zero entry terminates        */
      {0x00,0x00},                  /*    driver info list.             */
      0,
      0,
      0,
      0,
      NULL
   }

};

FATFS fatfs;
USB_STATUS           status = USB_OK;
_usb_host_handle     host_handle;

#define DRIVE     0
 
void __irq UsbIsrHost( void);

static volatile TYesNo Attached;
#warning Inicializovat mutex
static TMutex Mutex = 0;


#define DeviceConnected()    mass_device.dev_state == USB_DEVICE_INTERFACED    

TYesNo EfsInit( void)
// Initialisation
{
   if(!UsbHostEnable(YES)) {
      _SetError(EFS_ERROR_INIT_DRIVE);
      return NO;
   }

   MultitaskingMutexSet( &Mutex);
   InterruptDisable();
   Attached = NO;
   CpuIrqAttach(USB0_IRQn, 5, UsbIsrHost);
   CpuIrqEnable(USB0_IRQn);
   host_handle = NULL;
   status = _usb_host_init(HOST_CONTROLLER_NUMBER,    /* Use value in header file */
                           MAX_FRAME_SIZE,            /* Frame size per USB spec  */
                           &host_handle);             /* Returned pointer */
         
   if(status != USB_OK) {
      InterruptEnable();
      _SetError(EFS_ERROR_INIT_DRIVE);
      EfsSwitchOff();
      return NO;
   }

   status = _usb_host_driver_info_register(host_handle, (void*)DriverInfoTable);

   if(status != USB_OK) {
      InterruptEnable();
      _SetError(EFS_ERROR_INIT_DRIVE);
      EfsSwitchOff();
      return NO;
   }

   InterruptEnable();

word Timer;

   Timer = SysTimer() + ATTACH_TIMEOUT;

   forever {
      Poll();
      if(Attached) {
         break;
      }
      if(TimerAfter(SysTimer(), Timer)) {
         _SetError(EFS_ERROR_NO_DRIVE);
         EfsSwitchOff();
         return NO;
      }
   }

   Timer = SysTimer() + CONNECT_TIMEOUT;

   forever {
      Poll();
      if(DeviceConnected()) {
         break;
      }
      if(TimerAfter(SysTimer(), Timer)) {
         _SetError(EFS_ERROR_INIT_DRIVE);
         EfsSwitchOff();
         return NO;
      }
   }

   if(disk_initialize(DRIVE) != RES_OK) {
      _SetError(EFS_ERROR_INIT_DRIVE);
      EfsSwitchOff();
      return NO;
   }

   if(!_TranslateError(f_mount(DRIVE, &fatfs))) {
      _SetError(EFS_ERROR_MOUNT);
      EfsSwitchOff();
      return NO;
   }

   return YES;
} // EfsInit

TYesNo EfsClockSet( dword Clock)
// Synchronize filesystem with <Clock>
{
   return YES;
} // EfsClockSet

TYesNo EfsFormat( void)
// Format
{
   if(!_TranslateError(f_mkfs(DRIVE, 0, 0))) {
      return NO;
   }

   return YES;
}

TYesNo EfsDirectoryExists( const char *Path)
// Check for directory <Name>
{
   DIR Dir;

   if(!_TranslateError(f_opendir (&Dir, Path))) {
      return NO;
   }

   return YES;
}

TYesNo EfsDirectoryCreate( const char *_Path)
// Create new directory with <Path> and make it current directory
{
byte Path[EFS_PATH_MAX + 1];
byte *Slash;
byte *NextChunk = Path;

   strcpy(Path, _Path);

   do {
      Slash = strchr(NextChunk, '/');

      if(Slash) {
         *Slash = '\0';
      }

      if(!EfsDirectoryExists(  Path)) {
         if(!_TranslateError(f_mkdir( Path))) {
            return NO;
         }
      }

      if(Slash) {
         *Slash = '/';
         NextChunk = Slash + 1;
      }
   } while(Slash);


   if(!EfsDirectoryChange(Path)) {
      return NO;
   }

   return YES;
}

TYesNo EfsDirectoryChange( const char *Path)
// Change current directory to <Name>
{
   if(!_TranslateError(f_chdir(Path))) {
      return NO;
   }

   return YES;
}

TYesNo EfsDirectoryDelete( const char *Path)
// Delete directory <Name>
{
   if(!_TranslateError(f_unlink(Path))) {
      return NO;
   }

   return YES;
}

TYesNo EfsDirectoryRename( const char *Path, const char *NewName)
// Renames selected directory
{
   byte NewPath[EFS_PATH_MAX + 1];

   byte *Slash = strrchr(Path, '/');

   if(Slash) {
      dword ParentPathLength = (dword)Slash - (dword)Path + 1;
      memcpy(NewPath, Path, ParentPathLength);
      strcpy(NewPath + ParentPathLength, NewName);
   } else {
      strcpy(NewPath, NewName);
   }

   if(!_TranslateError(f_rename(Path, NewPath))) {
      return NO;
   }

   return YES;
}

TYesNo EfsFileExists( const char *Name)
// Check for file <Name>
{
   FIL File;
   if(!_TranslateError(f_open(&File, Name, FA_READ))) {
      return NO;
   }

   f_close(&File);

   return YES;
}

#ifndef FA_WRITE
   #define FA_WRITE     0
#endif

#ifndef FA_CREATE_ALWAYS
   #define FA_CREATE_ALWAYS     0
#endif


TYesNo EfsFileCreate( TEfsFile *File, const char *FileName, dword Size)
// Create <FileName>
{
   if(!_TranslateError(f_open(File, FileName, FA_WRITE | FA_READ | FA_CREATE_ALWAYS))) {
      File = 0;
      return NO;
   }

   if(Size == 0) {
      return YES;
   }

   if(!EfsFileSeek( File, Size, EFS_SEEK_START)) {
      EfsFileClose( File);
      return NO;
   }

   if(!EfsFileSeek( File, 0, EFS_SEEK_START)) {
      EfsFileClose( File);
      return NO;
   }
   return YES;
} // EfsFileCreate

TYesNo EfsFileOpen( TEfsFile *File, const char *FileName)
// Open <FileName> for read/write
{
   if(!_TranslateError(f_open(File, FileName, FA_WRITE | FA_READ))) {
      return NO;
   }

   return YES;
} // EfsFileOpen

TYesNo EfsFileDelete( const char *FileName)
// Delete <FileName>
{
   if(!_TranslateError(f_unlink(FileName))) {
      return NO;
   }

   return YES;
}

dword EfsFileSize( TEfsFile *File)
{
   return f_size(File);
}

void EfsFileClose( TEfsFile *File)
// Close opened file
{
   f_close(File);
}

word EfsFileWrite( TEfsFile *File, const void *Data, word Count)
// Write <Data> with <Count> bytes, returns number of bytes written
{
   dword BytesWritten;

   f_write(File, Data, Count, &BytesWritten);

   return BytesWritten;
}

word EfsFileRead( TEfsFile *File, void *Data, word Count)
// Read <Data> with <Count> bytes, returns number of bytes read
{
   dword BytesRead;

   f_read(File, Data, Count, &BytesRead);
      
   return BytesRead;
}

TYesNo EfsFileSeek( TEfsFile *File, int32 Pos, EEfsSeekMode Whence)
// Seek at <Pos> starting from <Whence>
{
   switch(Whence) {
      case EFS_SEEK_START:
         break;

      case EFS_SEEK_END:
         Pos = f_size(File) + Pos;
         break;

      case EFS_SEEK_CURRENT:
         Pos = f_tell(File) + Pos;
         break;

      default:
         return NO;
   }

   if(Pos < 0) {
      return NO;
   }

   f_lseek(File, Pos);

   int32 CurrentOffset = f_tell(File);

   if(CurrentOffset != Pos) {
      printf("Current != Offset, %d != %d\n", CurrentOffset, Pos);
      return NO;
   } else {
      printf("Current = %d\n", CurrentOffset);
   }

   return YES;
}

void EfsSwitchOff( void)
// Safely unmount drive
{
   InterruptEnable();
   f_mount(DRIVE, NULL);  // unmount
   UsbSwitchDisable(); // remove drive

word Timer;
   Timer = SysTimer() + DETACH_TIMEOUT;

   while(Attached) {
      Poll();
      if(TimerAfter(SysTimer(), Timer)) {
         break;
      }
   }

   if(Attached) {
      #warning Pokud tohle nastane, tak dojde k memory leak. Osetrit
   }

   if(host_handle) {
      _usb_host_shutdown(host_handle);
   }
   memset((void *)&mass_device, 0, sizeof(mass_device));
   UsbHostEnable(NO);
   MultitaskingMutexRelease( &Mutex);
}

//------------------------------------------------------------------------------

void usb_host_mass_device_event
   (
      /* [IN] pointer to device instance */
      _usb_device_instance_handle      dev_handle,

      /* [IN] pointer to interface descriptor */
      _usb_interface_descriptor_handle intf_handle,

      /* [IN] code number for event causing callback */
      uint_32           event_code
   )
{ /* Body */
   INTERFACE_DESCRIPTOR_PTR   intf_ptr =
      (INTERFACE_DESCRIPTOR_PTR)intf_handle;
   
   switch (event_code) 
   {
      case USB_CONFIG_EVENT:
         /* Drop through into attach, same processing */
      case USB_ATTACH_EVENT:
         Attached = YES;
         if (mass_device.dev_state == USB_DEVICE_IDLE) 
         {
            mass_device.dev_handle = dev_handle;
            mass_device.intf_handle = intf_handle;
            mass_device.dev_state = USB_DEVICE_ATTACHED;
            
            printf( "Mass Storage Device Attached\n\r" );
            mass_device.dev_state = USB_DEVICE_SET_INTERFACE_STARTED;
            status = _usb_hostdev_select_interface(mass_device.dev_handle,
            mass_device.intf_handle, (pointer)&mass_device.class_intf);
         } 
         else 
         {
            printf("Mass Storage Device Is Already Attached\n\r");
         } /* EndIf */
         break;
         
      case USB_INTF_EVENT:
         mass_device.dev_state = USB_DEVICE_INTERFACED;
         break;
         
      case USB_DETACH_EVENT:
         Attached = NO;
         if (mass_device.dev_state != USB_DEVICE_IDLE) 
         {
            mass_device.dev_handle = NULL;
            mass_device.intf_handle = NULL;
            mass_device.dev_state = USB_DEVICE_DETACHED;
            mass_device.dev_state = USB_DEVICE_IDLE;
            printf ( "\n\rMass Storage Device Detached\n\r" );
         } 
         else 
         {
            printf("Mass Storage Device Is Not Attached\n\r");
         } /* EndIf */
         break;
         
      default:
         mass_device.dev_state = USB_DEVICE_IDLE;
         break;
   } /* EndSwitch */
} /* Endbody */