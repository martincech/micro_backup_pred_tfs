//*****************************************************************************
//
//    uCommon.h   Kinetis definitions
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __uCommon_H__
   #include "Kinetis/Cpu/uCommon.h"
   #define __uCommon_H__

#ifdef F_FAST_INTERNAL
   #undef F_FAST_INTERNAL
#endif
#define F_FAST_INTERNAL 2000000ull

#define CLOCK_DIVIDER_MAX        16 // SIM_CLKDIV max value
#define PLL_MULTIPLIER_MIN       24 // 

#define F_UART0   F_SYSTEM
#define F_UART1   F_SYSTEM
#define F_UART2   F_BUS
#define F_UART3   F_BUS

#define LpTimerClockEnable()       SIM->SCGC5 |= SIM_SCGC5_LPTIMER_MASK
#define LpTimerClockDisable()      SIM->SCGC5 &= ~SIM_SCGC5_LPTIMER_MASK

#endif