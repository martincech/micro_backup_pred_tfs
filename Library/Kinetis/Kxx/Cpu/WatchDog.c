//*****************************************************************************
//
//    WatchDog.c     Kxx WatchDog
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "WatchDog.h"
#include "Hardware.h"
#include "Unisys/Uni.h"

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void WatchDogInit( void)
// Initialize watchdog
{
#ifdef WATCHDOG_PERIOD
   #define WATCHDOG_CLOCK_PRESCALER  8
   #define WATCHDOG_CYCLES          ((1LL * WATCHDOG_PERIOD * (F_BUS / WATCHDOG_CLOCK_PRESCALER)) / 1000)

   #if WATCHDOG_CYCLES > 0xFFFFFFFF
      #error Too long watchdog period
   #endif

   InterruptConditionallyDisable();
   WDOG->UNLOCK = 0xC520;
   WDOG->UNLOCK = 0xD928;
   WDOG->STCTRLH = WDOG_STCTRLH_WAITEN_MASK | WDOG_STCTRLH_STOPEN_MASK | WDOG_STCTRLH_ALLOWUPDATE_MASK | WDOG_STCTRLH_WDOGEN_MASK | WDOG_STCTRLH_CLKSRC_MASK;
   WDOG->PRESC = WDOG_PRESC_PRESCVAL(WATCHDOG_CLOCK_PRESCALER - 1);
   WDOG->TOVALH = (word) (WATCHDOG_CYCLES >> 16);
   WDOG->TOVALL = (word)  WATCHDOG_CYCLES;
   InterruptConditionallyEnable();
#else
   WatchDogDisable();
#endif
   
} // WatchDogInit

//-----------------------------------------------------------------------------
// Disable
//-----------------------------------------------------------------------------

void WatchDogDisable( void)
// Stop watchdog
{
   InterruptConditionallyDisable();
   WDOG->UNLOCK = 0xC520;
   WDOG->UNLOCK = 0xD928;
   WDOG->STCTRLH = WDOG_STCTRLH_ALLOWUPDATE_MASK;
   InterruptConditionallyEnable();
} // WatchDogInit

//-----------------------------------------------------------------------------
// Stop mode enable
//-----------------------------------------------------------------------------

void WatchDogStopModeEnable( TYesNo Enable)
// Enable/disable watchdog in stop mode
{
#ifdef WATCHDOG_PERIOD
   InterruptConditionallyDisable();
   WDOG->UNLOCK = 0xC520;
   WDOG->UNLOCK = 0xD928;
   if(Enable) {
      WDOG->STCTRLH |= WDOG_STCTRLH_WAITEN_MASK | WDOG_STCTRLH_STOPEN_MASK;
   } else {
      WDOG->STCTRLH &= ~(WDOG_STCTRLH_WAITEN_MASK | WDOG_STCTRLH_STOPEN_MASK);
   }
   InterruptConditionallyEnable();
#endif
}

//-----------------------------------------------------------------------------
// WatchDog
//-----------------------------------------------------------------------------

void WatchDog( void)
// Refresh watchdog
{
#ifdef WATCHDOG_PERIOD
   InterruptConditionallyDisable();
   WDOG->REFRESH = 0xA602;
   WDOG->REFRESH = 0xB480;
   InterruptConditionallyEnable();
#endif
} // WatchDog