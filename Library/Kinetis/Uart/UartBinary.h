//*****************************************************************************
//
//    UartBinary.h   UART binary local functions
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

static void _UartBinaryModeSet( TUartAddress Uart, TUartMode Mode);
// Set binary mode

static void __irq _Usart0BinaryHandler( void);
// USART0 binary handler

static void __irq _Usart1BinaryHandler( void);
// USART1 binary handler

static void __irq _Usart2BinaryHandler( void);
// USART2 binary handler

static void __irq _Usart3BinaryHandler( void);
// USART3 binary handler

static void __irq _Usart4BinaryHandler( void);
// USART4 binary handler

static void __irq _Usart5BinaryHandler( void);
// USART5 binary handler

static void __irq _Usart6BinaryHandler( void);
// USART6 binary handler

static void __irq _Usart7BinaryHandler( void);
// USART7 binary handler

static void __irq _Usart8BinaryHandler( void);
// USART8 binary handler

static void __irq _Usart9BinaryHandler( void);
// USART9 binary handler

static void _UsartBinaryHandler( const TUartDescriptor *Uart);
// Common binary handler
