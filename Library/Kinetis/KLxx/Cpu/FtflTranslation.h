//*****************************************************************************
//
//    FtflTranslation.h   Flash module translation
//    Version 1.0         (c) Veit Electronics
//
//*****************************************************************************

#ifndef __FtflTranslation_H__
   #define __FtflTranslation_H__

#define FTFL       FTFA

/* FSTAT Bit Fields */
#define FTFL_FSTAT_MGSTAT0_MASK                  FTFA_FSTAT_MGSTAT0_MASK
#define FTFL_FSTAT_MGSTAT0_SHIFT                 FTFA_FSTAT_MGSTAT0_SHIFT
#define FTFL_FSTAT_FPVIOL_MASK                   FTFA_FSTAT_FPVIOL_MASK
#define FTFL_FSTAT_FPVIOL_SHIFT                  FTFA_FSTAT_FPVIOL_SHIFT
#define FTFL_FSTAT_ACCERR_MASK                   FTFA_FSTAT_ACCERR_MASK
#define FTFL_FSTAT_ACCERR_SHIFT                  FTFA_FSTAT_ACCERR_SHIFT
#define FTFL_FSTAT_RDCOLERR_MASK                 FTFA_FSTAT_RDCOLERR_MASK
#define FTFL_FSTAT_RDCOLERR_SHIFT                FTFA_FSTAT_RDCOLERR_SHIFT
#define FTFL_FSTAT_CCIF_MASK                     FTFA_FSTAT_CCIF_MASK
#define FTFL_FSTAT_CCIF_SHIFT                    FTFA_FSTAT_CCIF_SHIFT
/* FCNFG Bit Fields */
#define FTFL_FCNFG_ERSSUSP_MASK                  FTFA_FCNFG_ERSSUSP_MASK
#define FTFL_FCNFG_ERSSUSP_SHIFT                 FTFA_FCNFG_ERSSUSP_SHIFT
#define FTFL_FCNFG_ERSAREQ_MASK                  FTFA_FCNFG_ERSAREQ_MASK
#define FTFL_FCNFG_ERSAREQ_SHIFT                 FTFA_FCNFG_ERSAREQ_SHIFT
#define FTFL_FCNFG_RDCOLLIE_MASK                 FTFA_FCNFG_RDCOLLIE_MASK
#define FTFL_FCNFG_RDCOLLIE_SHIFT                FTFA_FCNFG_RDCOLLIE_SHIFT
#define FTFL_FCNFG_CCIE_MASK                     FTFA_FCNFG_CCIE_MASK
#define FTFL_FCNFG_CCIE_SHIFT                    FTFA_FCNFG_CCIE_SHIFT
/* FSEC Bit Fields */
#define FTFL_FSEC_SEC_MASK                       FTFA_FSEC_SEC_MASK
#define FTFL_FSEC_SEC_SHIFT                      FTFA_FSEC_SEC_SHIFT
#define FTFL_FSEC_SEC(x)                         FTFA_FSEC_SEC(x)
#define FTFL_FSEC_FSLACC_MASK                    FTFA_FSEC_FSLACC_MASK
#define FTFL_FSEC_FSLACC_SHIFT                   FTFA_FSEC_FSLACC_SHIFT
#define FTFL_FSEC_FSLACC(x)                      FTFA_FSEC_FSLACC(x)
#define FTFL_FSEC_MEEN_MASK                      FTFA_FSEC_MEEN_MASK
#define FTFL_FSEC_MEEN_SHIFT                     FTFA_FSEC_MEEN_SHIFT
#define FTFL_FSEC_MEEN(x)                        FTFA_FSEC_MEEN(x)
#define FTFL_FSEC_KEYEN_MASK                     FTFA_FSEC_KEYEN_MASK
#define FTFL_FSEC_KEYEN_SHIFT                    FTFA_FSEC_KEYEN_SHIFT
#define FTFL_FSEC_KEYEN(x)                       FTFA_FSEC_KEYEN(x)
/* FOPT Bit Fields */
#define FTFL_FOPT_OPT_MASK                       FTFA_FOPT_OPT_MASK
#define FTFL_FOPT_OPT_SHIFT                      FTFA_FOPT_OPT_SHIFT
#define FTFL_FOPT_OPT(x)                         FTFA_FOPT_OPT(x)

#endif