//*****************************************************************************
//
//    WatchDog.c     KLxx WatchDog
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "WatchDog.h"
#include "Hardware.h"
#include "Unisys/Uni.h"

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void WatchDogInit( void)
// Initialize watchdog
{
#ifdef WATCHDOG_PERIOD
   SIM->COPC = SIM_COPC_COPT(2);
#else
   SIM->COPC = 0;
#endif
} // WatchDogInit

//-----------------------------------------------------------------------------
// Disable
//-----------------------------------------------------------------------------

void WatchDogDisable( void)
// Stop watchdog
{
   SIM->COPC = 0;
} // WatchDogInit

//-----------------------------------------------------------------------------
// WatchDog
//-----------------------------------------------------------------------------

void WatchDog( void)
// Refresh watchdog
{
#ifdef WATCHDOG_PERIOD
   SIM->SRVCOP = 0x55;
   SIM->SRVCOP = 0xAA;
#endif
} // WatchDog