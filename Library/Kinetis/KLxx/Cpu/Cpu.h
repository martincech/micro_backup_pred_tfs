//*****************************************************************************
//
//    Cpu.h        Kinetis CPU services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Cpu_H__
   #include "Kinetis/Cpu/Cpu.h"
   #define __Cpu_H__

//-----------------------------------------------------------------------------
// Interrupts
//-----------------------------------------------------------------------------

#define InterruptDisable()  __disable_irq()
#define InterruptEnable()   __enable_irq()

// Conditionally enable/disable
typedef dword TIntState;

static TIntState inline _InterruptConditionallyDisable()
// Disables interrupts and returns previous interrupt enable state
{
   InterruptDisable();
   return 0;
}

static void inline _InterruptConditionallyEnable( TIntState State)
// Conditionally enables interrupts based on <State>
{
   InterruptEnable();
}

// Helpers:
#define InterruptConditionallyDisable()   TIntState IntState; IntState = _InterruptConditionallyDisable()
#define InterruptConditionallyEnable()    _InterruptConditionallyEnable( IntState)

#endif
