//******************************************************************************
//
//    Delay.c      System delay loops
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "System/Delay.h"
#include "Hardware.h"

//------------------------------------------------------------------------------
//   Delay ms
//------------------------------------------------------------------------------

#define DELAY_MS_MULT    F_CPU / 3000

void SysDelay( word ms)
// Milisecond delay
{
   dword dms = ms * DELAY_MS_MULT;
   __asm__ __volatile__ (" mov r0, %[Ms] \n"
   "loop2: sub r0, #1 \n"
   " bne loop2 \n" : : [Ms] "r" (dms));
} // SysDelay

//------------------------------------------------------------------------------
//   Delay us
//------------------------------------------------------------------------------

#define DELAY_US_MULT    F_CPU / 1200000
#define DELAY_US_DIV     3

void SysUDelay( word us)
// Microsecond delay
{
   dword dus = us * DELAY_US_MULT;
   dus /= DELAY_US_DIV;
   __asm__ __volatile__ (" mov r0, %[Us] \n"
   "loop: sub r0, #1 \n"
   " bne loop \n" : : [Us] "r" (dus));
} // SysUDelay