//******************************************************************************
//
//   Clock.c         Bat2 clock
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "Clock/Pll.h"
#include "Cpu/Cpu.h"

#if F_CRYSTAL < 4000000 && F_CRYSTAL > 8000000
   #error Unsupported crystal
#endif

#define F_BUS_TRIM 16000000

#define PLL_PREDIVIDER    (F_CRYSTAL / 2000000)
#define PLL_MULTIPLIER    (F_USB / 2000000)
#define FLL_DIVIDER       (F_CRYSTAL / 32 / 31250)

#define OUTDIV_TRIM        F_USB / F_BUS_TRIM

#if OUTDIV_TRIM * F_BUS_TRIM != F_USB
   #error Unable auto trim
#endif

#if FLL_DIVIDER <= 1
   #define FRDIV 0
   #define FLL_DIVIDER_REAL    (1 * 32)
#elif FLL_DIVIDER <= 2
   #define FRDIV 1
   #define FLL_DIVIDER_REAL    (2 * 32)
#elif FLL_DIVIDER <= 4
   #define FRDIV 2
   #define FLL_DIVIDER_REAL    (4 * 32)
#elif FLL_DIVIDER <= 8
   #define FRDIV 3
   #define FLL_DIVIDER_REAL    (8 * 32)
#elif FLL_DIVIDER <= 16
   #define FRDIV 4
   #define FLL_DIVIDER_REAL    (16 * 32)
#elif FLL_DIVIDER <= 32
   #define FRDIV 5
   #define FLL_DIVIDER_REAL    (32 * 32)
#elif FLL_DIVIDER <= 64
   #define FRDIV 6
   #define FLL_DIVIDER_REAL    (64 * 32)
#else
   #define FRDIV 7
   #define FLL_DIVIDER_REAL    (128 * 32)
#endif

#if F_CRYSTAL / FLL_DIVIDER_REAL > 39062 || F_CRYSTAL / FLL_DIVIDER_REAL < 31250
   #error Unable divide FLL input clock
#endif

//------------------------------------------------------------------------------
//  Init
//------------------------------------------------------------------------------

void ClockInit( void)
// Init clock
{
   // Do autotrim of slow internal reference
   PllInit( OSCINIT,   // Initialize the oscillator circuit
            F_CRYSTAL,  // CLKIN0 frequency
            LOW_POWER,     // Set the oscillator for low power mode
            CRYSTAL,     // Crystal or canned oscillator clock input
            PLL_PREDIVIDER,    // PLL predivider value
            PLL_MULTIPLIER,     // PLL multiplier
            MCGOUT);

   word TrimVal = 21 * (F_BUS_TRIM / 32768);
   MCG->ATCVH = TrimVal >> 8;
   MCG->ATCVL = TrimVal;

   SIM->CLKDIV1 = ( 0
                  | SIM_CLKDIV1_OUTDIV1(OUTDIV_TRIM - 1)
                  | SIM_CLKDIV1_OUTDIV2(OUTDIV_TRIM - 1)
                  | SIM_CLKDIV1_OUTDIV3(OUTDIV_TRIM - 1)
                  | SIM_CLKDIV1_OUTDIV4(OUTDIV_TRIM - 1));

   MCG->SC |= MCG_SC_ATME_MASK;
   while(MCG->SC & MCG_SC_ATME_MASK);
   // autotrim complete, switch to FEI
   // PBE
   MCG->C1 = MCG_C1_CLKS(0x02) | MCG_C1_FRDIV(FRDIV);
   while((MCG->S & MCG_S_CLKST_MASK) != MCG_S_CLKST(0x02));

   // FBE
   MCG->C6 = 0;
   while(MCG->S & MCG_S_PLLST_MASK);

   // FEI
   MCG->C1 = MCG_C1_CLKS(0x00) | MCG_C1_IREFS_MASK | MCG_C1_FRDIV(FRDIV);
   while(!(MCG->S & MCG_S_IREFST_MASK));
   while((MCG->S & MCG_S_CLKST_MASK) != MCG_S_CLKST(0x00));

   SIM->CLKDIV1 = ( 0
                  | SIM_CLKDIV1_OUTDIV1(0)
                  | SIM_CLKDIV1_OUTDIV2(0)
                  | SIM_CLKDIV1_OUTDIV3(15)
                  | SIM_CLKDIV1_OUTDIV4(F_CPU / F_FLASH_MAX));

   
   /*
   // FEE - FLL with RTC reference clock
   RtcClockEnable();
   RTC->CR = RTC_CR_SC8P_MASK | RTC_CR_OSCE_MASK;
   SysDelay(10);
   MCG->C7 = MCG_C7_OSCSEL_MASK;
   MCG->C1 &= ~MCG_C1_IREFS_MASK;
   */
   
   MCG->C4 |= MCG_C4_DMX32_MASK;
   MCG->C4 |= MCG_C4_DRST_DRS(1);
   while((MCG->C4 & MCG_C4_DRST_DRS_MASK) != MCG_C4_DRST_DRS(1));
   //PinFunction(KINETIS_PIN_PORTC03, KINETIS_PIN_CLKOUT_PORTC03_FUNCTION);
} // ClockInit

//------------------------------------------------------------------------------
//  Usb clock start
//------------------------------------------------------------------------------

void ClockUsbFsStart( void)
// Start USB clock
{
   //PinFunction(KINETIS_PIN_PORTC03, KINETIS_PIN_CLKOUT_PORTC03_FUNCTION);
   //OSC->CR = OSC_CR_SC2P_MASK | OSC_CR_SC4P_MASK | OSC_CR_SC8P_MASK | OSC_CR_SC16P_MASK
   /*
   // temporarily enable PLL as main clock source
   InterruptDisable();
   // Init OSC
   MCG->C2 = MCG_C2_RANGE0(1) | MCG_C2_EREFS0_MASK;

   // FBI
   MCG->C1 = MCG_C1_CLKS(0x01) | MCG_C1_FRDIV(FRDIV) | MCG_C1_IREFS_MASK;
   while(!(MCG->S & MCG_S_IREFST_MASK));
   while((MCG->S & MCG_S_CLKST_MASK) != MCG_S_CLKST(0x01));

   // FBE
   MCG->C7 &= ~MCG_C7_OSCSEL_MASK;
   MCG->C1 = MCG_C1_CLKS(0x02) | MCG_C1_FRDIV(FRDIV);
   while(!(MCG->S & MCG_S_OSCINIT0_MASK));
   while(MCG->S & MCG_S_IREFST_MASK);
   while((MCG->S & MCG_S_CLKST_MASK) != MCG_S_CLKST(0x02));

   // PBE
   MCG->C5 = MCG_C5_PRDIV0(PLL_PREDIVIDER - 1);    //set PLL ref divider
   MCG->C6 = MCG_C6_PLLS_MASK | MCG_C6_VDIV0(PLL_MULTIPLIER - 24); // write new VDIV and enable PLL
   while(!(MCG->S & MCG_S_PLLST_MASK));
   while(!(MCG->S & MCG_S_LOCK0_MASK));

   // PEE
   MCG->C1 &= ~MCG_C1_CLKS_MASK; // clear CLKS to switch CLKS mux to select PLL as MCG_OUT
   while((MCG->S & MCG_S_CLKST_MASK) != MCG_S_CLKST(0x03));
   InterruptEnable();
   */

   PllInit( OSCINIT,   // Initialize the oscillator circuit
            F_CRYSTAL,  // CLKIN0 frequency
            LOW_POWER,     // Set the oscillator for low power mode
            CRYSTAL,     // Crystal or canned oscillator clock input
            PLL_PREDIVIDER,    // PLL predivider value
            PLL_MULTIPLIER,     // PLL multiplier
            PLL_ONLY);

   //PinFunction(KINETIS_PIN_PORTC03, KINETIS_GPIO_FUNCTION);
} // UsbFsClockStart

//------------------------------------------------------------------------------
//  Usb clock stop
//------------------------------------------------------------------------------

void ClockUsbFsStop( void)
// Start USB clock
{
   /*
   // disable PLL, switch back to FLL with RTC reference
   InterruptDisable();
   if((MCG->S & MCG_S_CLKST_MASK) == MCG_S_CLKST(0x03)) {
      // PBE
      MCG->C1 = MCG_C1_CLKS(0x02) | MCG_C1_FRDIV(FRDIV);
      while((MCG->S & MCG_S_CLKST_MASK) != MCG_S_CLKST(0x02));

      // FBE
      MCG->C6 = 0;
      while(MCG->S & MCG_S_PLLST_MASK);
   }

   // FEI
   MCG->C1 = MCG_C1_CLKS(0x00) | MCG_C1_IREFS_MASK | MCG_C1_FRDIV(FRDIV);
   while(!(MCG->S & MCG_S_IREFST_MASK));
   while((MCG->S & MCG_S_CLKST_MASK) != MCG_S_CLKST(0x00));

   // switch to RTC oscillator
   MCG->C7 = MCG_C7_OSCSEL_MASK;

   // FEE
   MCG->C1 = 0;
   InterruptEnable();
   */
   PllShutdown();
} // UsbFsClockStop





