//******************************************************************************
//
//   Clock.h         Bat2 clock
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef _Uni_H__
   #include "Unisys/Uni.h"
#endif

void ClockInit( void);
// Init clock

void ClockUsbFsStart( void);
// Start USB clock

void ClockUsbFsStop( void);
// Start USB clock