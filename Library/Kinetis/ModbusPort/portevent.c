/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- Variables ----------------------------------------*/
#define QUEUE_SIZE 10
static eMBEventType eQueuedEvent[QUEUE_SIZE];
static byte read;
static byte write;

void Increment(byte *i){
   (*i)++;
   if(*i == QUEUE_SIZE){
      *i = 0;
   }
}

BOOL FullQueue(){
   return (write + 1 == read) || (write + 1 == QUEUE_SIZE && read == 0);
}
/* ----------------------- Start implementation -----------------------------*/
BOOL
xMBPortEventInit( void )
{
   read = write = 0;
   return TRUE;
}

BOOL
xMBPortEventPost( eMBEventType eEvent )
{
   if(FullQueue()){
      return FALSE;
   }
   eQueuedEvent[write] = eEvent;
   Increment(&write);
   return TRUE;
}

BOOL
xMBPortEventGet( eMBEventType * eEvent )
{
   if(read == write){
      return FALSE;
   }

   *eEvent = eQueuedEvent[read];
   Increment(&read);
   return TRUE;
}

