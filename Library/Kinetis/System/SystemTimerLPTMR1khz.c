//******************************************************************************
//
//    SystemTimer.c  System timer
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Cpu/Cpu.h"
#include "Hardware.h"

// timer interrupt :
#define timerEnable()        InterruptEnable();
#define timerDisable()       InterruptDisable();

#define LPTMR_PRESCALE  2

void __irq LPTimer_IRQHandler( void);

// timer initialization :
static inline void timerInit() {
   LptmrClockEnable();

   LPTMR0->PSR = LPTMR_PSR_PRESCALE(0) | LPTMR_PSR_PCS(1); // ERCLK32K as source for LPTMR

   LPTMR0->CMR = 1000 * TIMER_PERIOD / LPTMR_PRESCALE / 1000;

   LPTMR0->CSR = LPTMR_CSR_TIE_MASK | LPTMR_CSR_TEN_MASK;
   CpuIrqAttach(LPTimer_IRQn, 3, LPTimer_IRQHandler);
   CpuIrqEnable(LPTimer_IRQn);
}

// timer handler header :
#define timerHandler()       void __irq LPTimer_IRQHandler( void)

// timer interrupt confirm :
#define timerInterruptConfirm()    (LPTMR0->CSR |= LPTMR_CSR_TCF_MASK)
