//******************************************************************************
//
//    Delay.c      System delay loops
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "System/Delay.h"
#include "Hardware.h"

#if F_CPU < 48000000
   #error Too slow F_CPU
#endif

#define UDELAY_FIXED_CYCLES      48

//------------------------------------------------------------------------------
//   Delay ms
//------------------------------------------------------------------------------

void SysDelay( word ms)
// Milisecond delay
{
   while(ms--) {
      SysUDelay( 1000);
   }
} // SysDelay

//------------------------------------------------------------------------------
//   Delay us
//------------------------------------------------------------------------------

void SysUDelay( word us)
// Microsecond delay
{
dword Cycles = F_BUS * us / 1000000;
   if(Cycles > UDELAY_FIXED_CYCLES) {
      Cycles -= UDELAY_FIXED_CYCLES;
   } else {
      Cycles = 0;
   }
   PitClockEnable();
   PIT->MCR = 0;
   PIT->CHANNEL[0].LDVAL = Cycles;
   PIT->CHANNEL[0].TCTRL = PIT_TCTRL_TEN_MASK;
   while(!(PIT->CHANNEL[0].TFLG));
   PIT->CHANNEL[0].TFLG = PIT_TFLG_TIF_MASK;
   PIT->CHANNEL[0].TCTRL = 0;
} // SysUDelay
