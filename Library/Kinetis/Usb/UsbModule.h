//*****************************************************************************
//
//    UsbModule.h  Usb module
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __UsbModule_H__
   #define __UsbModule_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void UsbModuleInit( void);
// Initialize

void UsbModulePullDownEnable( TYesNo Enable);
// Enable/disable transceiver pull down

void UsbModuleSwitchOn( void);
// Switch on USB module

void UsbModuleSwitchOff( void);
// Switch off USB module

#endif