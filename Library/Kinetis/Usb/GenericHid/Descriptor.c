//*****************************************************************************
//
//    Descriptor.c               Generic HID descriptors
//    Version 1.0                (c) VEIT Electronics
//
//*****************************************************************************

#include "USB_Config.h"         // configuration
#include "usb_descriptor.h"     // stack
#include "Usb/UsbDescriptor.h"  // descriptor types
#include "Hardware.h"

#define MANUFACTURER_STRING_DESCRIPTOR_ID   1
#define PRODUCT_STRING_DESCRIPTOR_ID        2

// Report descriptor
static byte ReportDescriptor[] = {
   0x06, 0xFF, 0xFF,                   // 04|2   , Usage Page (vendor defined?)
   0x09, 0x01,                         // 08|1   , Usage      (vendor defined
   0xA1, 0x01,                         // A0|1   , Collection (Application)
   // IN report
   0x09, 0x02,                         // 08|1   , Usage      (vendor defined)
   0x09, 0x03,                         // 08|1   , Usage      (vendor defined)
   0x15, 0x00,                         // 14|1   , Logical Minimum(0 for signed byte?)
   0x26, 0xFF, 0x00,                   // 24|1   , Logical Maximum(255 for signed byte?)
   0x75, 0x08,                         // 74|1   , Report Size(8) = field size in bits = 1 byte
   0x95, HID_GENERIC_REPORT_IN_SIZE,   // 94|1   , ReportCount(size) = repeat count of previous item
   0x81, 0x02,                         // 80|1   , IN report (Data,Variable, Absolute)
   // OUT report
   0x09, 0x04,                         // 08|1   , Usage      (vendor defined)
   0x09, 0x05,                         // 08|1   , Usage      (vendor defined)
   0x15, 0x00,                         // 14|1   , Logical Minimum(0 for signed byte?)
   0x26, 0xFF, 0x00,                   // 24|1   , Logical Maximum(255 for signed byte?)
   0x75, 0x08,                         // 74|1   , Report Size(8) = field size in bits = 1 byte
   0x95, HID_GENERIC_REPORT_OUT_SIZE,  // 94|1   , ReportCount(size) = repeat count of previous item
   0x91, 0x02,                         // 90|1   , OUT report (Data,Variable, Absolute)
   // Feature report
   0x09, 0x06,                         // 08|1   , Usage      (vendor defined)
   0x09, 0x07,                         // 08|1   , Usage      (vendor defined)
   0x15, 0x00,                         // 14|1   , LogicalMinimum(0 for signed byte)
   0x26, 0xFF, 0x00,                   // 24|1   , Logical Maximum(255 for signed byte)
   0x75, 0x08,                         // 74|1   , Report Size(8) =field size in bits = 1 byte
   0x95, HID_GENERIC_REPORT_FEATURE_SIZE,// 94|x   , ReportCount in byte
   0xB1, 0x02,                         // B0|1   , Feature report
   0xC0                                // C0|0   , End Collection
};

// Device descriptor
static TUsbDeviceDescriptor DeviceDescriptor = {
   .bLength = sizeof(TUsbDeviceDescriptor),
   .bDescriptorType = USB_DESCRIPTOR_DEVICE,
   .bcdUSB = 0x0200,
   .bDeviceClass = 0,
   .bDeviceSubClass = 0,
   .bDeviceProtocol = 0,
   .bMaxPacketSize0 = CONTROL_MAX_PACKET_SIZE,
   .idVendor = USB_VID,
   .idProduct = USB_PID,
   .bcdDevice = 0x0002,
   .iManufacturer = MANUFACTURER_STRING_DESCRIPTOR_ID,
   .iProduct = PRODUCT_STRING_DESCRIPTOR_ID,
   .iSerialNumber = 0,
   .bNumConfigurations = 1
};

// Configuration descriptor
static struct {
   TUsbConfigurationDescriptor ConfigurationDescriptor;
   TUsbInterfaceDescriptor     InterfaceDescriptor;
   TUsbHidDescriptor           HidDescriptor;
   TUsbEndpointDescriptor      HidEndpointInDescriptor;
   TUsbEndpointDescriptor      HidEndpointOutDescriptor;
} ConfigurationDescriptor = {
   { // Configuration descriptor
     .bLength = sizeof(TUsbConfigurationDescriptor),
     .bDescriptorType = USB_DESCRIPTOR_CONFIGURATION,
     .wTotalLength = sizeof(ConfigurationDescriptor),
     .bNumInterfaces = 1,
     .bConfigurationValue = 1,
     .iConfiguration = 0,
     .bmAttributes = 0,//BUS_POWERED|SELF_POWERED|(REMOTE_WAKEUP_SUPPORT<<REMOTE_WAKEUP_SHIFT),
     .MaxPower = USB_POWER / 2
   },
   { // Interface descriptor
     .bLength = sizeof(TUsbInterfaceDescriptor),
     .bDescriptorType = USB_DESCRIPTOR_INTERFACE,
     .bInterfaceNumber = 0,
     .bAlternateSetting = 0,
     .bNumEndpoints = HID_DESC_ENDPOINT_COUNT,
     .bInterfaceClass = 3,
     .bInterfaceSubClass = 0,
     .bInterfaceProtocol = 0,
     .iInterface = 0
   },
   { // Hid descriptor
     .bLength = sizeof(TUsbHidDescriptor),
     .bDescriptorType = USB_DESCRIPTOR_HID,
     .wHIDClassSpecComp = 0x0111,
     .bCountry = 0,
     .bNumDescriptors = 1,
     .b1stDescType = USB_DESCRIPTOR_REPORT,
     .w1stDescLength = sizeof(ReportDescriptor)
   },
   { // Endpoint IN descriptor
     .bLength = sizeof(TUsbEndpointDescriptor),
     .bDescriptorType = USB_DESCRIPTOR_ENDPOINT,
     .bEndpointAddress = HID_ENDPOINT_IN | (USB_SEND << 7),
     .bmAttributes = USB_INTERRUPT_PIPE,
     .wMaxPacketSize = HID_GENERIC_REPORT_IN_SIZE,
     .bInterval = 0x01
   },
   { // Endpoint OUT descriptor
     .bLength = sizeof(TUsbEndpointDescriptor),
     .bDescriptorType = USB_DESCRIPTOR_ENDPOINT,
     .bEndpointAddress = HID_ENDPOINT_OUT | (USB_RECV << 7),
     .bmAttributes = USB_INTERRUPT_PIPE,
     .wMaxPacketSize = HID_GENERIC_REPORT_OUT_SIZE,
     .bInterval = 0x01
   }
};

// stack endpoint descriptor
static USB_ENDPOINTS usb_desc_ep = {
   .count = HID_DESC_ENDPOINT_COUNT,
   {
      {
         HID_ENDPOINT_IN,
         USB_INTERRUPT_PIPE,
         USB_SEND,
         HID_GENERIC_REPORT_IN_SIZE
      },
      {
         HID_ENDPOINT_OUT,
         USB_INTERRUPT_PIPE,
         USB_RECV,
         HID_GENERIC_REPORT_OUT_SIZE
      }
   }
};

#define LANGUAGE "\x09\x04"

#define LANGUAGE_STRING_DESCRIPTOR_SIZE   UsbStringDescriptorSize(sizeof(LANGUAGE))
TUsbStringDescriptor LanguageStringDescriptor = {
   .bLength = LANGUAGE_STRING_DESCRIPTOR_SIZE,
   .bDescriptorType = USB_STRING_DESCRIPTOR,
   .bString = LANGUAGE
};
#define MANUFACTURER_STRING_DESCRIPTOR_SIZE   UsbStringDescriptorSize(sizeof(USB_MANUFACTURER))
TUsbStringDescriptor ManufacturerStringDescriptor = {
   .bLength = MANUFACTURER_STRING_DESCRIPTOR_SIZE,
   .bDescriptorType = USB_STRING_DESCRIPTOR,
   .bString = USB_MANUFACTURER
};
#define PRODUCT_STRING_DESCRIPTOR_SIZE   UsbStringDescriptorSize(sizeof(USB_PRODUCT))
TUsbStringDescriptor ProductStringDescriptor = {
   .bLength = PRODUCT_STRING_DESCRIPTOR_SIZE,
   .bDescriptorType = USB_STRING_DESCRIPTOR,
   .bString = USB_PRODUCT
};

/*****************************************************************************
 * Global Functions
 *****************************************************************************/
/**************************************************************************//*!
 *
 * @name  USB_Desc_Get_Descriptor
 *
 * @brief The function returns the correponding descriptor
 *
 * @param controller_ID : Controller ID
 * @param type          : Type of descriptor requested
 * @param sub_type      : String index for string descriptor
 * @param index         : String descriptor language Id
 * @param descriptor    : Output descriptor pointer
 * @param size          : Size of descriptor returned
 *
 * @return USB_OK                              When Successfull
 *         USBERR_INVALID_REQ_TYPE             when Error
 *****************************************************************************
 * This function is used to pass the pointer of the requested descriptor
 *****************************************************************************/

uint_8 USB_Desc_Get_Descriptor(
     uint_8 controller_ID,   /* [IN]  Controller ID */
     uint_8 type,            /* [IN]  Type of descriptor requested */
     uint_8 str_num,         /* [IN]  String index for string descriptor */
     uint_16 index,          /* [IN]  String descriptor language Id */
     uint_8_ptr *descriptor, /* [OUT] Output descriptor pointer */
     USB_PACKET_SIZE *size   /* [OUT] Size of descriptor returned */
)
{
   UNUSED (controller_ID)
   switch(type) {
      case USB_DESCRIPTOR_REPORT:
         *descriptor = (uint_8_ptr)&ReportDescriptor;
         *size = sizeof(ReportDescriptor);
         break;
      case USB_DESCRIPTOR_HID:
         *descriptor = (uint_8_ptr)&ConfigurationDescriptor.HidDescriptor;
         *size = sizeof(ConfigurationDescriptor.HidDescriptor);
         break;
      case USB_STRING_DESCRIPTOR:
         switch(str_num) {
            case 0:
               *descriptor = (uint_8_ptr)&LanguageStringDescriptor;
               *size = LANGUAGE_STRING_DESCRIPTOR_SIZE;
               break;
            case MANUFACTURER_STRING_DESCRIPTOR_ID:
               *descriptor = (uint_8_ptr)&ManufacturerStringDescriptor;
               *size = MANUFACTURER_STRING_DESCRIPTOR_SIZE;
               break;
            case PRODUCT_STRING_DESCRIPTOR_ID:
               *descriptor = (uint_8_ptr)&ProductStringDescriptor;
               *size = PRODUCT_STRING_DESCRIPTOR_SIZE;
               break;
            default:
               return USBERR_INVALID_REQ_TYPE;
         }
         break;
      case USB_DESCRIPTOR_DEVICE:
         *descriptor = (uint_8_ptr)&DeviceDescriptor;
         *size = sizeof(DeviceDescriptor);
         break;
      case USB_DESCRIPTOR_CONFIGURATION:
         *descriptor = (uint_8_ptr)&ConfigurationDescriptor;
         *size = sizeof(ConfigurationDescriptor);
         break;
      default :
         return USBERR_INVALID_REQ_TYPE;
   }
   return USB_OK;
}

/**************************************************************************//*!
 *
 * @name  USB_Desc_Get_Interface
 *
 * @brief The function returns the alternate interface
 *
 * @param controller_ID : Controller ID
 * @param interface     : Interface number
 * @param alt_interface : Output alternate interface
 *
 * @return USB_OK                              When Successfull
 *         USBERR_INVALID_REQ_TYPE             when Error
 *****************************************************************************
 *This function is called by the framework module to get the current interface
 *****************************************************************************/
uint_8 USB_Desc_Get_Interface(
      uint_8 controller_ID,     /* [IN] Controller ID */
      uint_8 interface,         /* [IN] Interface number */
      uint_8_ptr alt_interface  /* [OUT] Output alternate interface */
)
{
    UNUSED (controller_ID)
    /* if interface valid */
    if(interface < 1)
    {
        /* get alternate interface*/
        *alt_interface = 0;
        return USB_OK;
    }
    return USBERR_INVALID_REQ_TYPE;
}

/**************************************************************************//*!
 *
 * @name  USB_Desc_Set_Interface
 *
 * @brief The function sets the alternate interface
 *
 * @param controller_ID : Controller ID
 * @param interface     : Interface number
 * @param alt_interface : Input alternate interface
 *
 * @return USB_OK                              When Successfull
 *         USBERR_INVALID_REQ_TYPE             when Error
 *****************************************************************************
 *This function is called by the framework module to set the interface
 *****************************************************************************/
uint_8 USB_Desc_Set_Interface(
      uint_8 controller_ID, /* [IN] Controller ID */
      uint_8 interface,     /* [IN] Interface number */
      uint_8 alt_interface  /* [IN] Input alternate interface */
)
{
    UNUSED (controller_ID)
    /* if interface valid */
    if(interface < 1)
    {
        /* set alternate interface*/
        return USB_OK;
    }

    return USBERR_INVALID_REQ_TYPE;
}

/**************************************************************************//*!
 *
 * @name  USB_Desc_Valid_Configation
 *
 * @brief The function checks whether the configuration parameter
 *        input is valid or not
 *
 * @param controller_ID : Controller ID
 * @param config_val    : Configuration value
 *
 * @return TRUE           When Valid
 *         FALSE          When Error
 *****************************************************************************
 * This function checks whether the configuration is valid or not
 *****************************************************************************/
boolean USB_Desc_Valid_Configation(
      uint_8 controller_ID,/*[IN] Controller ID */
      uint_16 config_val   /*[IN] Configuration value */
)
{
    UNUSED (controller_ID)
    
   if(config_val <= 1) {
      return TRUE;
   }

   return FALSE;
}
/**************************************************************************//*!
 *
 * @name  USB_Desc_Valid_Interface
 *
 * @brief The function checks whether the interface parameter
 *        input is valid or not
 *
 * @param controller_ID : Controller ID
 * @param interface     : Target interface
 *
 * @return TRUE           When Valid
 *         FALSE          When Error
 *****************************************************************************
 * This function checks whether the interface is valid or not
 *****************************************************************************/
boolean USB_Desc_Valid_Interface(
      uint_8 controller_ID, /*[IN] Controller ID */
      uint_8 interface      /*[IN] Target interface */
)
{
    uint_8 loop_index=0;
    UNUSED (controller_ID)
    
   if(interface < 1) {
      return TRUE;
   }

    return FALSE;
}
/**************************************************************************//*!
 *
 * @name  USB_Desc_Remote_Wakeup
 *
 * @brief The function checks whether the remote wakeup is supported or not
 *
 * @param controller_ID : Controller ID
 *
 * @return REMOTE_WAKEUP_SUPPORT (TRUE) - If remote wakeup supported
 *****************************************************************************
 * This function returns remote wakeup is supported or not
 *****************************************************************************/
boolean USB_Desc_Remote_Wakeup(
      uint_8 controller_ID      /* [IN] Controller ID */
)
{
    UNUSED (controller_ID)
    return REMOTE_WAKEUP_SUPPORT;
}


/**************************************************************************//*!
 *
 * @name  USB_Desc_Get_Endpoints
 *
 * @brief The function returns with the list of all non control endpoints used
 *
 * @param controller_ID : Controller ID
 *
 * @return pointer to USB_ENDPOINTS
 *****************************************************************************
 * This function returns the information about all the non control endpoints
 * implemented
 *****************************************************************************/
void* USB_Desc_Get_Endpoints(
      uint_8 controller_ID      /* [IN] Controller ID */
)
{
    UNUSED (controller_ID)
    return (void*)&usb_desc_ep;
}
