//*****************************************************************************
//
//    SocketIfUsb.c     Usb socket interface
//    Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#include "Remote/SocketIfUsb.h"
#include "Remote/SocketIfUsbDef.h"
#include "Remote/Frame.h"
#include "Cpu/Cpu.h"
#include "Usb/UsbModule.h"
#include "usb_hid.h" // Stack

#define HID_REPORT_LENGTH 64

#define CONTROLLER_ID       0
#define RX_REPLY_TIMEOUT    (0 / TIMER_PERIOD)
#define RX_TIMEOUT          (500 / TIMER_PERIOD)

#define SendReport()      USB_Class_HID_Send_Data(CONTROLLER_ID, HID_ENDPOINT_IN, ChunkBuffer, HID_REPORT_LENGTH)
#define ReceiveReport()   USB_Class_HID_Recv_Data(CONTROLLER_ID, HID_ENDPOINT_OUT, ChunkBuffer, HID_REPORT_LENGTH)

static TFrameState _State;
static byte Status;
static TYesNo UsbReady;
static byte ChunkBuffer[HID_REPORT_LENGTH];
static TYesNo SocketReady;
static byte CurrentId;

static void Close( void);
// Socket close

static void StackCallback( byte controller_ID, byte event_type, void *val);
// Stack callback

uint_8 StackCallbackRequest( uint_8 request, uint_16 value, uint_8_ptr* data, USB_PACKET_SIZE* size);
// Stack callback

void __irq UsbIsrDevice( void);
// Handler

//------------------------------------------------------------------------------
//   Permission
//------------------------------------------------------------------------------

byte SocketIfUsbPermission( TSocket *Socket)
// Permission
{
   return FILE_MODE_READ_WRITE;
} // SocketIfUsbPermission

//------------------------------------------------------------------------------
//   Listen
//------------------------------------------------------------------------------

TYesNo SocketIfUsbListen( TSocket *Socket) {
   CpuIrqDisable(USB0_IRQn);
   if(!SocketReady) {
      CpuIrqEnable(USB0_IRQn);
      return NO;
   }
   SocketReady = NO;
   Socket->Id = CurrentId;
   Status = SOCKET_STATE_CONNECTED;
   CpuIrqEnable(USB0_IRQn);
   return YES;
} // SocketIfUsbListen

//------------------------------------------------------------------------------
//  Init
//------------------------------------------------------------------------------

void SocketIfUsbInit( void)
// Initialization
{
   FrameInit( &_State);
   FrameTimeoutSet( &_State, RX_TIMEOUT, RX_REPLY_TIMEOUT);
   InterruptDisable();
   CpuIrqAttach(USB0_IRQn, 0, UsbIsrDevice);
   CpuIrqEnable(USB0_IRQn);
   USB_Class_HID_Init(CONTROLLER_ID, StackCallback, 0, StackCallbackRequest);
   InterruptEnable();
} // SocketIfUsbInit

//------------------------------------------------------------------------------
//  Deinit
//------------------------------------------------------------------------------

void SocketIfUsbDeinit( void)
// Deinitialization
{
   Close();
   USB_Class_HID_DeInit(CONTROLLER_ID);
} // SocketIfUsbDeinit

//------------------------------------------------------------------------------
//  Timer
//------------------------------------------------------------------------------

void SocketIfUsbTimer( void)
// Timer
{
   FrameTimer( &_State);
} // SocketIfUsbTimer

//------------------------------------------------------------------------------
//  State
//------------------------------------------------------------------------------

byte SocketIfUsbState( TSocket *Socket)
// Gets state of socket
{
   if(Socket->Id == CurrentId) {
      return Status;
   } else {
      return SOCKET_STATE_DISCONNECTED;
   }
} // SocketIfUsbState

//------------------------------------------------------------------------------
//  Receive
//------------------------------------------------------------------------------

TYesNo SocketIfUsbReceive( TSocket *Socket, void *Buffer, int Size)
// Receive into <Buffer> with <Size>
{
   if(Socket->Id != CurrentId) {
      return NO;
   }
   if(Status == SOCKET_STATE_DISCONNECTED) {
      return NO;
   }
   if(!FrameReceiveStart(&_State, Buffer, Size)) {
      return NO;
   }
   if(!ReceiveReport()) {
      return NO;
   }
   Status = SOCKET_STATE_RECEIVE_ACTIVE;
   return YES;
} // SocketIfUsbReceive

//------------------------------------------------------------------------------
//  Receive size
//------------------------------------------------------------------------------

int SocketIfUsbReceiveSize( TSocket *Socket)
// Gets number of received bytes
{
   return _State.SizeProccessed;
} // SocketIfUsbReceiveSize

//------------------------------------------------------------------------------
//  Send
//------------------------------------------------------------------------------

TYesNo SocketIfUsbSend( TSocket *Socket, const void *Buffer, int Size)
// Send <Buffer> with <Size>
{
int ChunkSize;
   if(Socket->Id != CurrentId) {
      return NO;
   }
   if(Status == SOCKET_STATE_DISCONNECTED) {
      return NO;
   }
   if(!FrameSendInit(&_State, Buffer, Size)) {
      return NO;
   }
   if(!FrameSendProccess(&_State, ChunkBuffer, HID_REPORT_LENGTH, &ChunkSize)) {
      return NO;
   }
   if(!SendReport()) {
      return NO;
   }
   Status = SOCKET_STATE_SEND_ACTIVE;
   return YES;
} // SocketIfUsbSend

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void SocketIfUsbClose( TSocket *Socket)
// Close socket
{
   if(Socket->Id != CurrentId) {
      return;
   }
   Close();
} // SocketIfUsbClose

//------------------------------------------------------------------------------
//  Connected
//------------------------------------------------------------------------------

TYesNo SocketIfUsbConnected( void)
// Socket connected
{
   return Status != SOCKET_STATE_DISCONNECTED;
} // SocketIfUsbReady

//------------------------------------------------------------------------------
//  Ready
//------------------------------------------------------------------------------

TYesNo SocketIfUsbReady( void)
// Socket ready
{
   return UsbReady;
} // SocketIfUsbReady

//******************************************************************************

//------------------------------------------------------------------------------
// Stack callback
//------------------------------------------------------------------------------

static void StackCallback( byte controller_ID, byte event_type, void *val)
// Stack callback
{
int ChunkSize;
   PTR_USB_DEV_EVENT_STRUCT event = (PTR_USB_DEV_EVENT_STRUCT) val;
   switch(event_type) {
      case USB_APP_DATA_RECEIVED:
         switch(Status) {
            case SOCKET_STATE_RECEIVE_ACTIVE:
               FrameReceiveProccess(&_State, ChunkBuffer, HID_REPORT_LENGTH);
               switch(_State.Status) {
                  case FRAME_STATE_RECEIVE_ACTIVE :
                  case FRAME_STATE_RECEIVE_WAITING_HEADER :
                     break;
                  case FRAME_STATE_RECEIVE_FRAME:
                     Status = SOCKET_STATE_RECEIVE_DONE;
                     return;
                  default:
                     Status = SOCKET_STATE_RECEIVE_ERROR;
                     return;
               }
               ReceiveReport();
               break;
         }
         break;

      case USB_APP_SEND_COMPLETE:
         switch(Status) {
            case SOCKET_STATE_SEND_ACTIVE:
               switch(_State.Status) {
                  case FRAME_STATE_SEND_ACTIVE:
                     break;
                  case FRAME_STATE_SEND_DONE:
                     Status = SOCKET_STATE_SEND_DONE;
                     return;
                  default:
                     Status = SOCKET_STATE_SEND_ERROR;
                     return;
               }
               if(!FrameSendProccess(&_State, ChunkBuffer, HID_REPORT_LENGTH, &ChunkSize)) {
                  Status = SOCKET_STATE_SEND_ERROR;
                  return;
               }
               SendReport();
               break;
         }
         break;

      case USB_APP_ENUM_COMPLETE:
         UsbReady = YES;
         Close();
         break;
      case USB_APP_BUS_RESET:
         UsbReady = NO;       
         Close();
         break;
      case USB_APP_CONFIG_CHANGED:
         Close();
         break;
      case USB_APP_ERROR:
         Close();
         break;
      default:
         break;
   }
} // StackCallback

//------------------------------------------------------------------------------
// Stack callback request
//------------------------------------------------------------------------------

uint_8 StackCallbackRequest( uint_8 request, uint_16 value, uint_8_ptr* data, USB_PACKET_SIZE* size)
// Stack callback
{
TSocketIfUsbCmd *Cmd;
TSocketIfUsbReply *Reply;
   Cmd = (TSocketIfUsbCmd *) *data;
   Reply = (TSocketIfUsbReply *) *data;
   switch(request)
   {
      case USB_HID_GET_REPORT_REQUEST :
         *size = 4;
         memset(Reply, 0, sizeof(*Reply));
         if(Status == SOCKET_STATE_RECEIVE_ACTIVE) {
            Reply->ReadyReceive = YES;
         }
         break;

      case USB_HID_SET_REPORT_REQUEST :
         *size = 0;
         if(Cmd->Open == NO) {
            Close();
            SocketReady = NO;
         } else {
            if(Status == SOCKET_STATE_DISCONNECTED) {
               SocketReady = YES;
            }
         }
         break;
   }

   return USB_OK;
} // StackCallbackRequest

//------------------------------------------------------------------------------
// Socket close
//------------------------------------------------------------------------------

static void Close( void)
// Socket close
{
   CpuIrqDisable(USB0_IRQn);
   if(Status != SOCKET_STATE_DISCONNECTED) {
      CurrentId++;
      Status = SOCKET_STATE_DISCONNECTED;
   }
   CpuIrqEnable(USB0_IRQn);
} // Close