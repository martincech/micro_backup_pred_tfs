//*****************************************************************************
//
//    SocketIfEthernet.c    Ethernet socket interface
//    Version 1.0           (c) VEIT Electronics
//
//*****************************************************************************

#include "Remote/SocketIfEthernet.h"
#include "Remote/Frame.h"
#include "fnet.h"
#include "Multitasking/Multitasking.h"
#include "Ethernet/EthernetConfig.h"
#include "Ethernet/EthernetLibrary.h"
#include "System/System.h"                 // Sysclock
#include "Timer/Timer.h"                   // Systimer

#define CHUNK_SIZE   650

typedef enum {
   SOCK_ETH_STATUS_DEINITED,
   SOCK_ETH_STATUS_OFF,
   SOCK_ETH_STATUS_LINK_UP,
   SOCK_ETH_STATUS_DNS_RESOLVE,
   SOCK_ETH_STATUS_DNS_RESOLVING,
   SOCK_ETH_STATUS_DNS_UNRESOLVED,
   SOCK_ETH_STATUS_CONNECTING,
   SOCK_ETH_STATUS_CONNECTING_WAIT,
   SOCK_ETH_STATUS_OPERATIONAL,
   SOCK_ETH_STATUS_LINK_LOST,
   SOCK_ETH_STATUS_SHUTDOWN
} EEthernetSocketStatus;

typedef struct {
   SOCKET Socket;
   byte SocketState;
   TFrameState State;
} TEthernetSocket;

// Local variables
static EEthernetSocketStatus  _SStatus = SOCK_ETH_STATUS_DEINITED;
static TEthernetSocket        _Socket = {SOCKET_INVALID, SOCKET_STATE_DISCONNECTED, 0};
static fnet_ip4_addr_t        _ServerAddr;
static dword                  _Timer;

// Local functions
static void _RWData();
// read write routine
static void _DnsCallback(fnet_ip4_addr_t address, long cookie);
// dns resolve callback function

#define RX_REPLY_TIMEOUT      0
#define RX_TIMEOUT            500
// TODO set reasonable values
#define RECONNECT_TIMEOUT     5  // s - after unsuccessful connection
#define RX_ASK_MODULE_TIMEOUT 1  // s - ask GSM whether new data available
#define TIMEOUT_INVALID -1

#define _TimeoutSet( value)   (_Timer = SysTime() + value)
#define _TimeoutReset()       (_Timer = TIMEOUT_INVALID)
#define _TimeoutRunning()     (_Timer != TIMEOUT_INVALID)
#define _Timeout()            ( !_TimeoutRunning() || TimeAfter(SysTime(), _Timer))

//------------------------------------------------------------------------------
//   Permission
//------------------------------------------------------------------------------

byte SocketIfEthernetPermission( TSocket *Socket)
// Permission
{
   return FILE_MODE_READ_WRITE;
} // SocketIfEthernetPermission

static TYesNo Occupied;

TYesNo SocketIfEthernetListen( TSocket *Socket) {
   return NO;
   /*if(Occupied) {
      return NO;
   }
   if(_Socket.SocketState == SOCKET_STATE_DISCONNECTED) {
      return NO;
   }
   Occupied = YES;
   return YES;*/
}

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

static void SocketIfEthernetInit( void) {
   // init sessions
   _Socket.Socket = SOCKET_INVALID;
   _Socket.SocketState = SOCKET_STATE_DISCONNECTED;
   _SStatus = SOCK_ETH_STATUS_OFF;
   EthernetLibraryInitNetworkInterface();
   #warning Pokud je ethernet uspany, nesmi se komunikovat pres MII! Zde neni splneno.
   EthernetSleepEnable( YES);
   _ServerAddr = 0;
   _TimeoutReset();
}

//------------------------------------------------------------------------------
//   Free
//------------------------------------------------------------------------------

static void SocketIfEthernetDeinit( void) {
   if( _Socket.Socket != SOCKET_INVALID){
      closesocket( _Socket.Socket);
   }
   _Socket.Socket = SOCKET_INVALID;
   _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
   _Socket.SocketState = SOCKET_STATE_DISCONNECTED;
   _SStatus = SOCK_ETH_STATUS_DEINITED;
   EthernetLibraryFreeNetworkInterface();
   _ServerAddr = 0;
}

//------------------------------------------------------------------------------
//   Main executive
//------------------------------------------------------------------------------

void SocketIfEthernetExecute( void) {
fnet_socket_state_t  state;
int                  Length;
fnet_error_t         ErrCode;
struct sockaddr_in   addr;

   if( _SStatus == SOCK_ETH_STATUS_DEINITED){
      if( !EthernetOptions.Enabled){
         return;
      }
      SocketIfEthernetInit();
   }
   EthernetLibraryExecute();
   switch( _SStatus){
      case SOCK_ETH_STATUS_OFF :
         if(!EthernetOptions.Enabled){
            _TimeoutReset();
            SocketIfEthernetDeinit();
            break;
         }
         if(!EthernetIsLink()){
            break;
         }
         _SStatus = SOCK_ETH_STATUS_LINK_UP;,         
         EthernetSleepEnable(NO);
         break;

      case SOCK_ETH_STATUS_LINK_UP:
         if(!EthernetOptions.Enabled){
            _SStatus = SOCK_ETH_STATUS_OFF;
            break;
         }
         if( !EthernetIsLink()){
            break;
         }

//         if( fnet_inet_aton( EthernetOptions.Address, &addr.sin_addr) == FNET_ERR){
//            // not valid IP, try get IP by DNS query
//            _SStatus = SOCK_ETH_STATUS_DNS_RESOLVE;
//         }else {
//            _ServerAddr = addr.sin_addr.s_addr;
//            _SStatus = SOCK_ETH_STATUS_CONNECTING_WAIT;
//         }
         break;

//      case SOCK_ETH_STATUS_DNS_RESOLVE :
//      {
//         char DnsServerAddress[FNET_IP4_ADDR_STR_SIZE];
//         EthernetLibraryDnsServer( DnsServerAddress, FNET_IP4_ADDR_STR_SIZE);
//         struct fnet_dns_params dnsParams = {
//            0,                      /* DNS server address */
//            EthernetOptions.Address,/* host name to resolve */
//            &_DnsCallback,          /* callback pointer */
//            0                       /* no cookie needed */
//         };
//         fnet_inet_aton(DnsServerAddress, (struct in_addr*)&dnsParams.dns_server);
//         if( fnet_dns_init(&dnsParams) == FNET_ERR){
//            _SStatus = SOCK_ETH_STATUS_LINK_UP;
//            break;
//         }
//         _SStatus = SOCK_ETH_STATUS_DNS_RESOLVING;
//      }
//         break;
//
//      case SOCK_ETH_STATUS_DNS_RESOLVING :
//         if( !EthernetOptions.Enabled){
//            _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
//            break;
//         }
//         break;
//
//      case SOCK_ETH_STATUS_DNS_UNRESOLVED :
//         if( !EthernetOptions.Enabled){
//            _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
//            break;
//         }
//         if( _Timeout()){
//            _SStatus = SOCK_ETH_STATUS_LINK_UP;
//            _TimeoutReset();
//         }
//         break;
//
//      case SOCK_ETH_STATUS_CONNECTING_WAIT :
//         if( !EthernetOptions.Enabled){
//            _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
//            break;
//         }
//         if( _Timeout()){
//            _SStatus = SOCK_ETH_STATUS_CONNECTING;
//            _TimeoutReset();
//         }
//         break;
//
//      case SOCK_ETH_STATUS_CONNECTING :
//         if( !EthernetOptions.Enabled){
//            _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
//            break;
//         }
//         if(!EthernetIsLink()){
//            _SStatus = SOCK_ETH_STATUS_LINK_LOST;
//            break;
//         }
//
//         if( _Socket.Socket == SOCKET_INVALID){
//            addr.sin_family = AF_INET;
//         #if USE_FNET_STACK
//            addr.sin_port = FNET_HTONS(EthernetOptions.Port);
//         #else
//            addr.sin_port = EthernetOptions.Port;
//         #endif
//            addr.sin_addr.s_addr = _ServerAddr;
//            if( (_Socket.Socket = socket( addr.sin_family, SOCK_STREAM, 0)) == SOCKET_INVALID){
//                break;
//            }
//            Length = 1;
//            // test for socket aliveness
//            setsockopt( _Socket.Socket, SOL_SOCKET, SO_KEEPALIVE, (char *) &Length, sizeof(int));
//            // idle time(s)
//            Length = 20;
//            setsockopt( _Socket.Socket, IPPROTO_TCP, TCP_KEEPIDLE, (char *) &Length, sizeof(int));
//            // retransmit probe time(s)
//            Length = 10;
//            setsockopt( _Socket.Socket, IPPROTO_TCP, TCP_KEEPINTVL, (char *) &Length, sizeof(int));
//            // retransmit attempts count
//            Length = 3;
//            setsockopt( _Socket.Socket, IPPROTO_TCP, TCP_KEEPCNT, (char *) &Length, sizeof(int));
//
//            //connect socket
//            if( connect( _Socket.Socket, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) == SOCKET_ERROR){
//               // try again later
//               closesocket( _Socket.Socket);
//               _Socket.Socket = SOCKET_INVALID;
//               break;
//            }
//         } else {
//            Length = sizeof(fnet_socket_state_t);
//            ErrCode = getsockopt( _Socket.Socket, SOL_SOCKET, SO_STATE, (char*)&state, &Length);
//            if( ErrCode != FNET_OK || state == SS_UNCONNECTED){
//               // connection problem
//               _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
//               _TimeoutSet( RECONNECT_TIMEOUT);
//               break;
//            }
//            if( state == SS_CONNECTED){
//               // connected, fine
//               _Socket.SocketState = SOCKET_STATE_CONNECTED;
//               FrameInit( &_Socket.State);
//               FrameTimeoutSet( &_Socket.State, RX_TIMEOUT, RX_REPLY_TIMEOUT);
//               _SStatus = SOCK_ETH_STATUS_OPERATIONAL;
//               _TimeoutReset();
//               break;
//            }
//            // still connecting
//         }
//         break;
//      case SOCK_ETH_STATUS_OPERATIONAL :
//         if( !EthernetOptions.Enabled){
//            _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
//            break;
//         }
//         if(!EthernetIsLink()){
//            _SStatus = SOCK_ETH_STATUS_LINK_LOST;
//            break;
//         }
//         Length = sizeof(fnet_socket_state_t);
//         ErrCode = getsockopt( _Socket.Socket, SOL_SOCKET, SO_STATE, (char*)&state, &Length);
//         if( ErrCode != FNET_OK || state == SS_UNCONNECTED){
//            // terminated from other side
//            _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
//         }
//         break;
//
//      case SOCK_ETH_STATUS_LINK_LOST :
//         if( EthernetIsLink()){
//            _SStatus = SOCK_ETH_STATUS_CONNECTING;
//         }
//         break;
//
//      default :
//         _Socket.SocketState = SOCKET_STATE_DISCONNECTED;
//         if( _Socket.Socket != SOCKET_INVALID){
//            if( closesocket( _Socket.Socket) == FNET_ERR){
//               break;
//            }
//         }
//         _Socket.Socket = SOCKET_INVALID;
//         _SStatus = SOCK_ETH_STATUS_OFF;
//         EthernetSleepEnable(YES);
//         break;
//   }
//
//   if( _SStatus != SOCK_ETH_STATUS_OPERATIONAL){
//      return;
   }

   _RWData();
}

//------------------------------------------------------------------------------
//   RW executive
//------------------------------------------------------------------------------

static void _RWData()
// read write routine
{
int               Length;
int               LengthSent;
byte              Chunk[CHUNK_SIZE];
int               ChunkSize;
fnet_error_t      ErrCode;

   switch( _Socket.SocketState){
      case SOCKET_STATE_RECEIVE_ACTIVE :
         if( !_Timeout()){
            // dont ask yet
            break;
         }
         _TimeoutReset();
         forever {
            Length = recv( _Socket.Socket, Chunk, CHUNK_SIZE, 0);
            if(Length == 0) {
               // no data, ask later
               _TimeoutSet( RX_ASK_MODULE_TIMEOUT);
               break;
            }
            if(Length < 0) {
               ErrCode = fnet_error_get();
               switch(ErrCode) {
                  case FNET_ERR_AGAIN:
                     return;

                  default:
                     // read problem, probably terminated
                     _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
                     return;

               }
            }
            FrameReceiveProccess( &_Socket.State, Chunk, Length);
            MultitaskingReschedule();

            switch(_Socket.State.Status) {
               case FRAME_STATE_RECEIVE_ACTIVE:
               case FRAME_STATE_RECEIVE_WAITING_HEADER :
                  break;

               case FRAME_STATE_RECEIVE_FRAME:
                  _Socket.SocketState = SOCKET_STATE_RECEIVE_DONE;
                  return;

               default:
                  _Socket.SocketState = SOCKET_STATE_RECEIVE_ERROR;
                  return;
            }
         }
         break;

      case SOCKET_STATE_SEND_ACTIVE:
         forever {
            if(!FrameSendProccess( &_Socket.State, Chunk, CHUNK_SIZE, &ChunkSize)) {
               _Socket.SocketState = SOCKET_STATE_SEND_ERROR;
               break;
            }
            LengthSent = 0;
            while(LengthSent != ChunkSize) {
               Length = send( _Socket.Socket, &Chunk[LengthSent], ChunkSize - LengthSent, 0);
               if( Length == SOCKET_ERROR) {
                  ErrCode = fnet_error_get();
                  switch(ErrCode) {
                     case FNET_ERR_AGAIN:
                        return;

                     default:
                        // write problem, probably terminated
                        _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
                        return;
                  }
               }
               LengthSent += Length;
               MultitaskingReschedule();
            }

            switch(_Socket.State.Status) {
               case FRAME_STATE_SEND_ACTIVE:
                  break;

               case FRAME_STATE_SEND_DONE:
                  _Socket.SocketState = SOCKET_STATE_SEND_DONE;
                  return;

               default:
                  _Socket.SocketState = SOCKET_STATE_SEND_ERROR;
                  return;
            }
         }
         break;
   }
}

//------------------------------------------------------------------------------
//   DNS
//------------------------------------------------------------------------------

static void _DnsCallback(fnet_ip4_addr_t address, long cookie)
// dns resolve callback function
{
   _ServerAddr = address;
   if( address == FNET_ERR){
      // problem, try again to connect later
      _SStatus = SOCK_ETH_STATUS_DNS_UNRESOLVED;
      _TimeoutSet(RECONNECT_TIMEOUT);
   } else {
      // continue connecting
      _SStatus = SOCK_ETH_STATUS_CONNECTING;
   }
}

//------------------------------------------------------------------------------
//   Socket API routines
//------------------------------------------------------------------------------

void SocketIfEthernetTimer( void) {
   FrameTimer( &_Socket.State);
}

byte SocketIfEthernetState( TSocket *Socket) {
   return _Socket.SocketState;
}

int SocketIfEthernetReceiveSize( TSocket *Socket) {
   return _Socket.State.SizeProccessed;
}

TYesNo SocketIfEthernetSend( TSocket *Socket, const void *Buffer, int Size) {
byte Chunk[CHUNK_SIZE];
int Length;
   if( _SStatus == SOCK_ETH_STATUS_OPERATIONAL){
      while(recv( _Socket.Socket, Chunk, CHUNK_SIZE, 0) > 0); // flush
   }
   Length = send( _Socket.Socket, Buffer, Size, 0);
   if( Length != Size) {
      _Socket.SocketState = SOCKET_STATE_SEND_ERROR;
      return NO;
   }
   _Socket.SocketState = SOCKET_STATE_SEND_DONE;
   return YES;
   /*if(!FrameSendInit( &_Socket.State, Buffer, Size)) {
      return NO;
   }
   _Socket.SocketState = SOCKET_STATE_SEND_ACTIVE;
   return YES;*/
}

TYesNo SocketIfEthernetReceive( TSocket *Socket, void *Buffer, int Size) {
   if(!FrameReceiveStart( &_Socket.State, Buffer, Size)) {

      return NO;
   }
   _Socket.SocketState = SOCKET_STATE_RECEIVE_ACTIVE;
   return YES;
}

void SocketIfEthernetClose( TSocket *Socket)
// Close socket
{
   _SStatus = SOCK_ETH_STATUS_SHUTDOWN;
   Occupied = NO;
}
