//*****************************************************************************
//
//    UartCore.c   UART simple services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Uart/Uart.h"
#include "Cpu/UsartReg.h"
#include "System/System.h"

//------------------------------------------------------------------------------
//   UART descriptor
//------------------------------------------------------------------------------

#define UART_PORTS_COUNT            4            // number of UARTS

typedef UART_Type TUsart;

// Read/Write data :
typedef struct {
   dword ReplyTimeout;
   dword IntercharacterTimeout;
} TUartData;

static TUartData _UartData[ UART_PORTS_COUNT];

// Read only data :
typedef struct {
   TUsart     *Usart;
   TUartData  *Data;
} TUartDescriptor;

// Uart descriptor :
static const TUartDescriptor _Uart[ UART_PORTS_COUNT] = {
   { UART0, &_UartData[ 0]},
   { UART1, &_UartData[ 1]},
   { UART2, &_UartData[ 2]},
   { UART3, &_UartData[ 3]}
};

#define UsartGet( Uart)   _Uart[ Uart].Usart

//-----------------------------------------------------------------------------
// Base services
//-----------------------------------------------------------------------------

#include "Uart/UartBase.c"

//-----------------------------------------------------------------------------
// Direct services
//-----------------------------------------------------------------------------

#include "Uart/UartSimple.c"