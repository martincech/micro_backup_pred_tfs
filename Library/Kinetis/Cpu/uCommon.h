//*****************************************************************************
//
//    uCommon.h   Kinetis definitions
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __uCommon_H__
   #define __uCommon_H__

#include <Kinetis.h>

#define  PmcBandgapEnable()   PMC->REGSC |=  PMC_REGSC_BGBE_MASK
#define  PmcBandgapDisable()  PMC->REGSC &= ~PMC_REGSC_BGBE_MASK

#define MCU_FLASH_OFFSET   0x00000000

#define F_FAST_INTERNAL 4000000ull
#define F_SLOW_INTERNAL 32678ull

// Generalized registers
#define PORT(Num)   ((PORT_Type *)(PORTA_BASE + (PORTB_BASE - PORTA_BASE) * (Num)))
#define PT(Num)     ((GPIO_Type *)(PTA_BASE +   (PTB_BASE - PTA_BASE)     * (Num)))

#define PIN_MASK( Pin)  (((1) << ((Pin) & 0x1F)))
#define PORT_OFFSET     32

// Transformations
#define PIN_NUMBER(PORT, PIN)   (PIN + PORT * PORT_OFFSET) // Port & Pin to PinNumber
#define N2PIN( PinNumber)    (PinNumber % PORT_OFFSET)     // PinNumber to Pin
#define N2PORT( PinNumber)   (PinNumber / PORT_OFFSET)     // PinNumber to Port

//-----------------------------------------------------------------------------
// Pin & function definitions
//-----------------------------------------------------------------------------

#define KINETIS_PORTA   0
#define KINETIS_PORTB   1
#define KINETIS_PORTC   2
#define KINETIS_PORTD   3
#define KINETIS_PORTE   4
#define KINETIS_PORTF   5
#define KINETIS_PORTG   6
#define KINETIS_PORTH   7

#define KINETIS_GPIO_FUNCTION   1   // global GPIO function

#endif