//*****************************************************************************
//
//    Pwm.c        PWM controller
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Pwm.h"

#define COUNTER_MAX     0xFFFF

dword Duty[_PWM_CHANNEL_COUNT] = {0, 0};

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void PwmInit( unsigned FrequencyHz)
// Initialisation
{
   Ftm2ClockEnable();
   FTM2->SC = 0;
   FTM2->CNTIN = 0;
   FTM2->CNT = 0;
   FTM2->MOD = F_SYSTEM / 2 / FrequencyHz;
   FTM2->SC = FTM_SC_CLKS(1) | FTM_SC_PS(1);
   PwmPortInit();
} // PwmInit

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void PwmStop( int Channel)
// Stop PWM
{
   FTM2->CONTROLS[Channel].CnV = 0;
   while(FTM2->CNT != FTM2->MOD);
} // PwmStop

//-----------------------------------------------------------------------------
// Run
//-----------------------------------------------------------------------------

void PwmStart( int Channel)
// Start PWM
{
   FTM2->CONTROLS[Channel].CnSC = FTM_CnSC_MSB_MASK | FTM_CnSC_ELSB_MASK;
   FTM2->CONTROLS[Channel].CnV = Duty[Channel];
} // PwmStart

//-----------------------------------------------------------------------------
// Duty
//-----------------------------------------------------------------------------

void PwmDutySet( int Channel, unsigned _Duty)
// Set PWM <Duty> cycle
{
   Duty[Channel] = FTM2->MOD * _Duty / 100;
   FTM2->CONTROLS[Channel].CnV = Duty[Channel];
} // PwmDutySet
