//*****************************************************************************
//
//    Llwu.c           Llwu
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#include "Llwu.h"
#include "Cpu/Cpu.h"
#include "Hardware.h"

#define LLWU_CHANNELS_PER_REGISTER       4

__irq void LLW_IRQHandler( void);
// LLWU handler

//*****************************************************************************
// Init
//*****************************************************************************

void LlwuInit( void)
// Initialize module
{
   LlwuClockEnable();
   LLWU->F1 = 0xFF;
   LLWU->F2 = 0xFF;
   CpuIrqAttach(LLW_IRQn, LLWU_INTERRUPT_PRIORITY, LLW_IRQHandler);
   CpuIrqEnable(LLW_IRQn);
} // LlwuInit

//*****************************************************************************
// Enable
//*****************************************************************************

void LlwuEnable(int Channel, int Flags)
// Enable <Channel>, with <Flags>
{
volatile byte *Register;
byte Mask;

   if(Channel < LLWU_M0IF) {
      Register = &LLWU->PE1 + Channel / LLWU_CHANNELS_PER_REGISTER;
      Mask = (Flags) << (2 * (Channel % LLWU_CHANNELS_PER_REGISTER));
   } else {
      Mask = 1 << (Channel - LLWU_M0IF);
      Register = &LLWU->ME;
   }
   *Register |= Mask;
}

//*****************************************************************************
// Disable
//*****************************************************************************

void LlwuDisable(int Channel)
// Enable <Channel>
{
volatile byte *Register;
byte Mask;

   if(Channel < LLWU_M0IF) {
      Register = &LLWU->PE1 + Channel / LLWU_CHANNELS_PER_REGISTER;
      Mask = (0x3) << (2 * (Channel % LLWU_CHANNELS_PER_REGISTER));
   } else {
      Mask = 1 << (Channel - LLWU_M0IF);
      Register = &LLWU->ME;
   }
   *Register &= ~Mask;
} // LlwuEnable

//-----------------------------------------------------------------------------

//*****************************************************************************
// IRQ Handler
//*****************************************************************************

__irq void LLW_IRQHandler( void)
// LLWU handler
{
dword Flags = (LLWU->F3 << 16) | (LLWU->F2 << 8) | (LLWU->F1 << 0);
int Source = LLWU_P0;
   while(Flags) {
      if(Flags & 0x1) {
         LlwuEvent(Source);
      }
      Source++;
      Flags >>= 1;
   }
   
   LLWU->F1 = 0xFF;
   LLWU->F2 = 0xFF;
} // Handler