//*****************************************************************************
//
//    Llwu.h           Llwu
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Llwu_H__
   #define __Llwu_H__

#include "Unisys/Uni.h"
#include "Llwu/LlwuDef.h"

typedef enum {
   LLWU_P0,
   LLWU_P1,
   LLWU_P2,
   LLWU_P3,
   LLWU_P4,
   LLWU_P5,
   LLWU_P6,
   LLWU_P7,
   LLWU_P8,
   LLWU_P9,
   LLWU_P10,
   LLWU_P11,
   LLWU_P12,
   LLWU_P13,
   LLWU_P14,
   LLWU_P15,
   LLWU_M0IF,
   LLWU_M1IF,
   LLWU_M2IF,
   LLWU_M3IF,
   LLWU_M4IF,
   LLWU_M5IF,
   LLWU_M6IF,
   LLWU_M7IF
} ELlwuPin;

typedef enum {
   LLWU_FLAG_NONE,
   LLWU_RISING_EDGE,
   LLWU_FALLING_EDGE,
   LLWU_ANY_EDGE
} ELlwuFlag;

void LlwuInit( void);
// Initialize module

void LlwuEnable(int Channel, int Flags);
// Enable <Channel>, with <Flags>

void LlwuDisable(int Channel);
// Enable <Channel>

void LlwuEvent(int Channel);
// Llwu callback

#endif
