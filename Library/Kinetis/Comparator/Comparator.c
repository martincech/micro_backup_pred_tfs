//*****************************************************************************
//
//    Comparator.c   Comparator
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#include "Comparator.h"

#warning Netestovano !!!

typedef enum {
  CMP0_INPUT_CMP0_IN0,
  CMP0_INPUT_CMP0_IN1,
  CMP0_INPUT_CMP0_IN2,
  CMP0_INPUT_CMP0_IN3,
  CMP0_INPUT_DAC1_OUT_CMP0_IN4,
  CMP0_INPUT_VREF_OUT_CMP0_IN5,
  CMP0_INPUT_BANDGAP,
  CMP0_INPUT_DAC0_REF
} TCmp0Input;

typedef enum {
  CMP1_INPUT_CMP1_IN0,
  CMP1_INPUT_CMP1_IN1,
  CMP1_INPUT_CMP1_IN2,
  CMP1_INPUT_DAC0_OUT_CMP1_IN3,
  CMP1_INPUT_UNAVAILABLE,
  CMP1_INPUT_VREF_OUT_CMP1_IN5,
  CMP1_INPUT_BANDGAP,
  CMP1_INPUT_DAC1_REF
} TCmp1Input;

typedef enum {
  CMP2_INPUT_CMP2_IN0,
  CMP2_INPUT_CMP2_IN1,
  CMP2_INPUT_CMP2_IN2,
  CMP2_INPUT_DAC1_OUT_CMP2_IN3,
  CMP2_INPUT_UNAVAILABLE0,
  CMP2_INPUT_UNAVAILABLE1,
  CMP2_INPUT_BANDGAP,
  CMP2_INPUT_DAC2_REF
} TCmp2Input;



#define CMP0_PSEL      CMP0_INPUT_CMP0_IN0 
#define CMP0_NSEL      CMP0_INPUT_BANDGAP 

void ComparatorInit( void)
// Init
{
   ComparatorClockEnable();
   CMP0->CR1 = CMP_CR1_EN_MASK;
   CMP0->MUXCR = CMP_MUXCR_PSEL(CMP0_PSEL) | CMP_MUXCR_NSEL(CMP0_NSEL);
} // ComparatorInit

TYesNo ComparatorGet( void)
// Get comparator output
{
   if(CMP0->SCR & CMP_SCR_COUT_MASK) {
      return YES;
   }
   return NO;
} // ComparatorGet

void ComparatorShutdown( void)
// Shutdown
{
   CMP0->CR1 = 0;
   ComparatorClockDisable();
} // ComparatorShutdown