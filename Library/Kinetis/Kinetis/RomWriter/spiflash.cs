/*
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/

 * \file spiflash.cs
 * \Author Suikan
 * \brief fash programmer alogorithm part
*/

using System;
using System.Collections.Generic;
using System.Threading;
using GUI;
using Kinetis.JtagKey;
using Kinetis.JtagKey.JtagKey;

namespace Kinetis.RomWriter
{
   /// <summary>
   /// \brief SPI Flash programming algorithm class
   /// 
   /// Wraps SPI/USB interface as SPI programmer.
   /// 
   /// </summary>
   public class SpiFlash : AbstractFlash
   {
      #region Private types

      private enum VendorName
      {
         Unknown,
         Freescale
      }

      /// <summary>
      /// \brief Devices supported
      /// 
      /// If you add your favorite device here, You should be care:
      /// -# It should be "Uniform" block architecuture device
      /// -# You should modify GetID() method
      /// -# You should change DeviceBitSize property
      /// -# You should change BlockByteSize property
      /// -# You should change PageByteSize property
      /// 
      /// Generally speaking, newer SPI flash device is almost compatible
      /// each other, Check following carefully:
      /// - Page Programming command
      /// - Protection bit on Status Regsiter.
      /// 
      /// The reference device of this programmer is STMicro M25P40
      /// </summary>
      private enum DeviceName
      {
         Unknown,
         MK20
      }

      #endregion

      #region Private fields

      // Interpreted informaiton
      private readonly VendorName vendor = VendorName.Unknown;
      private readonly DeviceName device = DeviceName.Unknown;

      #endregion

      #region Public interface

      /// <summary>
      /// \brief constructor
      /// </summary>
      public SpiFlash(EzPort ezPort, ITextBox aTextBox, IProgressBar aProgressBar):base(ezPort,aTextBox,aProgressBar)
      {
         vendor = VendorName.Freescale;
         device = DeviceName.MK20;
      }

      /// <summary>
      /// \brief return Vender name
      /// 
      /// The value is calcurated based on the result of GetID method.
      /// </summary>
      public override string VendorString
      {
         get { return vendor.ToString(); }
      }

      /// <summary>
      /// \brief return device name
      /// 
      /// The value is calcurated based on the result of GetID method.
      /// </summary>
      public override string DeviceString
      {
         get { return device.ToString(); }
      }

      /// <summary>
      /// \brief return device size
      /// 
      /// The value is calcurated based on the result of GetID method.
      /// </summary>
      public override int DeviceBitSize
      {
         get
         {
            const int mbits = 1024*1024;

            switch (device)
            {
               case DeviceName.MK20:
                  return 4*mbits;

               default:
                  return 0;
            }
         }
      }

      /// <summary>
      /// \brief return device size
      /// 
      /// The value is calcurated based on the result of GetID method.
      /// </summary>
      public override int DeviceByteSize
      {
         get { return DeviceBitSize/8; }
      }


      /// <summary>
      /// \brief return page size
      /// 
      /// The value is calcurated based on the result of GetID method.
      /// This value could be vary based on the device. You should change
      /// this property if you add new device.
      /// 
      /// </summary>
      public override int PageByteSize
      {
         get { return 2048; }
      }

      /// <summary>
      /// \brief return block count
      /// 
      /// The value is calcurated based on the result of GetID method.
      /// </summary>
      public override int BlockCount
      {
         get { return DeviceByteSize/PageByteSize; }
      }

      /// <summary>
      /// \brief return block size
      /// 
      /// The value is calcurated based on the result of GetID method.
      /// This value could be vary based on the device. You should change
      /// this property if you add new device.
      /// 
      /// Anyway, this class supports only the uniform block size architecture.
      /// </summary>
      public override BlockInfo[] Block
      {
         get
         {
            var blocks = new BlockInfo[BlockCount];
            var blocksize = PageByteSize;

            for (var i = 0; i < BlockCount; i++)
            {
               blocks[i].size = blocksize;
               blocks[i].startAddress = blocksize*i;
               blocks[i].endAddress = blocksize*(i + 1) - 1;
            }
            return blocks; // 64kB
         }
      }


      /// <summary>
      /// \brief get SPI Flash ID by RDID Command
      /// \return true if known, false if unknown
      /// 
      /// Get ID and put the result into internal variable.
      /// </summary>
      public override bool GetID()
      {
         return true;
      }

      /// <summary>
      /// \brief set Flash ID of ID Command is not existing
      /// \param aDevices an integer which represent parts number.
      /// \return true if know, false if unknown
      /// 
      /// Get ID and put the result into internal variable.
      /// </summary>
      public override bool SetID(int aDevice)
      {
         return true;
      }

      /// <summary>
      /// \brief Init the target, if required.
      /// \param Clock frequency of target by MHz.
      /// 
      /// This implement the EzPort activation based on the Coldfire specification.
      /// EzPort is activated when you keep EZPCS pin L during Reset. 
      /// 
      /// </summary>
      protected override bool InitTarget()
      {
         var cmdRc = new byte[] {0xB9}; // command  ResetChip:B9
         var cmdWren = new byte[] {0x06}; // command  WREN:06

         // Assert SRST then assert Cs
         Spi.AssertSRST();
         Spi.AssertCs(LatchOn.Rising);

         // reset Chip and wait 100mS
         Thread.Sleep(100);

         // Deassert SRST then deassert Cs
         Spi.DeassertSRST();

         // wait 100mS
         Thread.Sleep(100);
         Spi.DeassertCs(LatchOn.Rising);

         // Set Flash Clock divider
         // execute WREN command
         Spi.Transfer(cmdWren, null, LatchOn.Rising);

         return !IsBusy;
      }


      /// <summary>
      /// \brief Do bulk erase
      /// 
      /// Erase entire contents of SPI Flash.
      /// </summary>
      public override void EraseAll()
      {
         WriteMessage(Properties.Resources.ErasingDevice);
         //EraseBlockByBlock();
         //return;
         var cmdWren = new byte[] {0x06}; // command  WREN:06
         var cmdBe = new byte[] {0xC7}; // command  Bulk Erase:C7
         SetProgress(0);
         // wait until ongoin write/erase becomes complete
         if (!WaitCompletition())
         {
            return;
         }

         // execute WREN command
         Spi.Transfer(cmdWren, null, LatchOn.Rising);
         SetProgress(50);

         // execute BE command
         Spi.Transfer(cmdBe, null, LatchOn.Rising);
         SetProgress(75);

         // wait until ongoin write/erase becomes complete
         if (!WaitCompletition())
         {
            return;
         }
         SetProgress(100);
         WriteMessage(Properties.Resources.ErasedDevice);
      }

      /// <summary>
      /// \brief Check if entire flash rom is blank
      /// \return true if entire ROM is blank. false if not.
      /// 
      /// Check entire flash. If all blank, return true. Else, false.
      /// </summary>
      public override bool IsBlanROM()
      {
         byte[] data;
         var address = 0;
         return ReadROM(out data, bytes =>
         {
            // Verify readed block
            for (long i = 0; i < bytes.Length; i++)
            {
               if (bytes[i] == 0xFF) continue;

               WriteMessage(string.Format(Properties.Resources.NotBlankAt, (address + i).ToString("X04"),bytes[i].ToString("X02"))); 
                          
               return false;
            }
            address += bytes.Length;
            return true;
         });
      }

// ReSharper disable once InconsistentNaming
      private bool ReadROM(out byte[] data, Func<byte[], bool> afterBlockReadedAction = null )
      {
         //const int blockSize = 256; // For each time, read 4096 bytes
         var cmdRd = new byte[] {0x03, 0, 0, 0}; // command Read:03
         var block = new byte[4 + PageByteSize];
         data = new byte[] { };
         SetProgress(0);

         // wait until ongoin write/erase becomes complete
         if (!WaitCompletition())
         {
            return false;
         }

         // Read entire ROM
         for (var pageTopAddress = 0; pageTopAddress < DeviceByteSize; pageTopAddress += PageByteSize)
         {
            if (Cancel)
            {
               return false;
            }
            // set address. Address is big endian
            cmdRd[1] = BitConverter.GetBytes(pageTopAddress)[2];
            cmdRd[2] = BitConverter.GetBytes(pageTopAddress)[1];
            cmdRd[3] = BitConverter.GetBytes(pageTopAddress)[0];

            // execute Read command
            Spi.Transfer(cmdRd, block, LatchOn.Rising);

            // execute action on readed block
            if (afterBlockReadedAction != null)
            {
               var dataBlock = new byte[PageByteSize];
               Array.Copy(block, 4, dataBlock, 0, PageByteSize);
               if (!afterBlockReadedAction(dataBlock))
               {
                  return false;
               }
            }
            // copy readed data to output buffer            
            var offset = data.Length;
            Array.Resize(ref data, data.Length + PageByteSize);
            Array.Copy(block, 4, data, offset, PageByteSize);
            
            SetProgress((pageTopAddress*100)/DeviceByteSize);
            WriteDot();
            if (!WaitCompletition())
            {
               return false;
            }
         }
         SetProgress(100);
         return true; // success
      }

      /// <summary>
      /// Read data on specific adress
      /// </summary>
      /// <param name="data"></param>
      /// <param name="address"></param>
      /// <param name="dataLenght"></param>
      /// <returns></returns>
      public bool ReadData(out byte[] data, int address, int dataLenght)
      {
         //const int blockSize = 256; // For each time, read 4096 bytes
         var cmdRd = new byte[] {0x03, 0, 0, 0}; // command Read:03
         var block = new byte[4 + dataLenght];
         data = new byte[2];


         // wait until ongoin write/erase becomes complete
         if (!WaitCompletition())
         {
            return false;
         }

         if (Cancel)
         {
            return false;
         }
         // set address. Address is big endian
         cmdRd[1] = BitConverter.GetBytes(address)[2];
         cmdRd[2] = BitConverter.GetBytes(address)[1];
         cmdRd[3] = BitConverter.GetBytes(address)[0];

         // execute Read command
         Spi.Transfer(cmdRd, block, LatchOn.Rising);
         Array.Copy(block, 4, data, 0, dataLenght);

         if (!WaitCompletition())
         {
            return false;
         }
         return true; // success
      }

      /// <summary>
      /// \brief program all are
      /// \param memory : total memory image of ROM
      /// \param eraseAllMemory : true if you want all memory
      /// \param systemClockMHz EzPort target requires this. Ignore for normal SPI Flash.
      /// 
      /// This method programs all memory area. 
      /// Note that memory should be the total image of ROM.
      /// </summary>
      public override bool Program(byte[] memory, bool eraseAllMemory, float systemClockMHz)
      {
         OnProgramming(true);

         Spi.AssertCs(LatchOn.Rising); // Asserting Chipselect, if needed 

         try
         {
            // erase all if required
            if (eraseAllMemory)
            {
               EraseAll();
               if (!WaitCompletition())
               {
                  return false;
               }
            }
            else
            {
               WriteMessage(Properties.Resources.ErasingDevice);
               // block by block erase only dirtyBlocks blocks
               var dirty = new bool[BlockCount]; // "dirtyBlocks" check flag for each sector(block)
               // check all blcoks if it is required to be erased.
               for (var blockIndex = 0; blockIndex < BlockCount; blockIndex++)
               {
                  dirty[blockIndex] = IsDirtyToProgram(Block[blockIndex].startAddress, memory);
                  SetProgress((blockIndex * 50) / BlockCount);
               } // check device

               if (!EraseBlockByBlock(dirty))
               {
                  return false;
               }
               if (!WaitCompletition())
               {
                  return false;
               }
               WriteMessage(Properties.Resources.ErasedDevice);
            } // erase memory
           

            WriteMessage(Properties.Resources.StartProgramming);
            SetProgress(0);
            // count up how many blocks have to be write
            var sectorAmount = 0;
            for (long adrs = 0; adrs < DeviceByteSize; adrs += PageByteSize)
            {
               sectorAmount++;
            }

            // write data
            var count = 0;
            for (long adrs = 0; adrs < DeviceByteSize; adrs += PageByteSize)
            {
               SectionProgram(adrs, memory);
               count++;
               SetProgress((count*100)/sectorAmount);
               if (Cancel)
               {
                  return false;
               }
            }
            //WriteMessage(""); // dummy return for dots.
            WriteMessage(Properties.Resources.ProgrammingDone);
         }
         finally
         {
            Spi.DeassertCs(LatchOn.Rising); // Deasert Chip Select pin, if needed
            OnProgramming(false);
         }
         return true;
      }

      

      private bool EraseBlockByBlock(IList<bool> dirtyBlocks = null)
      {
         SetProgress(0);

         WriteMessage(Properties.Resources.EraseEachBlock);
         // Now, we can erase blocks
         for (var blockIndex = 0; blockIndex < BlockCount; blockIndex++)
         {
            if (Cancel)
            {
               return false;
            }
            if (dirtyBlocks != null && !dirtyBlocks[blockIndex]) continue;
            WriteMessage(Properties.Resources.ErasingBlock + blockIndex.ToString("d3") + "...");
            EraseBlock(Block[blockIndex].startAddress);
            if (!WaitCompletition())
            {
               return false;
            }
            SetProgress((blockIndex * 100) / BlockCount);
         }
         return true;
      }

// Program    

      #region Overrides of AbstractFlash

      /// <summary>
      /// Read the whole memory
      /// </summary>
      /// <param name="memory">total memory image of ROM</param>
      /// <param name="systemClockMHz">EzPort target requires system clock</param>
      /// <returns>execution status</returns>
      public override bool ReadMemory(out byte[] memory, float systemClockMHz)
      {
         return ReadROM(out memory);
      }

      #endregion

      /// <summary>
      /// Event which is raised when programming progres changed (from true to false and back)
      /// </summary>
      public event EventHandler<bool> Programming;

      #endregion

      #region Protected helpers

      /// <summary>
      /// \brief Do Block erase
      /// 
      /// Block erase is somtimes called as block erase ( depend on the device ).
      /// For M25P40, the block size is 64KB, and can obtain by BlockByteSize property.
      /// This method erase a block wich contains specified address.
      /// To be sure, erase one block 3 times.
      /// </summary>
      protected override void EraseBlock(long address)
      {
         byte[] cmdWren = {0x06}; // command  WREN:06
         byte[] cmdSe = {0xD8, 0, 0, 0}; // command  block Erase:D8

         // wait until ongoin write/erase becomes complete
         if (!WaitCompletition())
         {
            return;
         }

         // execute WREN command
         Spi.Transfer(cmdWren, null, LatchOn.Rising);

         // set address. Address is big endian
         cmdSe[1] = (byte) ((address >> 16) & 0xFF);
         cmdSe[2] = (byte) ((address >> 8) & 0xFF);
         cmdSe[3] = (byte) (address & 0xF8);
         // execute WRSR command
         Spi.Transfer(cmdSe, null, LatchOn.Rising);

         // wait until ongoin write/erase becomes complete
         if (!WaitCompletition())
         {
            return;
         }
      }

      protected virtual void OnProgramming(bool e)
      {
         var handler = Programming;
         if (handler != null) handler(this, e);
      }

      /// <summary>
      /// \brief status of Chip ID
      /// 
      /// This is interface property to get/set Chip Status register through SPI.
      /// </summary>
      protected override byte Status
      {
         get
         {
            var cmdRdsr = new byte[] {0x05}; // command  RDSR:05
            var data = new byte[2];

            // execute RDSR command
            Spi.Transfer(cmdRdsr, data, LatchOn.Rising);

            return data[1]; // return it
         }

         set { }
      }

      /// <summary>
      /// \brief check if the SPI Flash is busy.
      /// \return true if SPI flashis busy. false if ready to write
      /// </summary>
      protected override bool IsBusy
      {
         get { return (Status & 0x01) != 0; // check bit 0 of status register
         }
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// \brief Check if given block is dirtyBlocks and have to be programmed
      /// \param pageTopAddress : Address of top of page. 
      /// \param memory : total memory image of ROM
      /// \return True if dirtyBlocks
      /// 
      /// Check if given program block is dirtyBlocks. Dirty mean not all FF. 
      /// </summary>
      private bool IsDirtyToProgram(long pageTopAddress, byte[] memory)
      {
         for (long i = 0; i < PageByteSize; i++)
         {
            if (memory[pageTopAddress + i] != 0xFF) // if something is not 0xff, have to program
               return true;
         }

         return false;
      }

      /// <summary>
      /// \brief page program
      /// \param pageTopAddress : Address of top of page. 
      /// \param memory : total memory image of ROM
      /// 
      /// This method programs specified page by parameter. The page size is
      /// vary on device. Note that memory should be the total image of ROM.
      /// </summary>
      private void SectionProgram(long sectorTopAddress, byte[] memory)
      {
         var cmdWren = new byte[] {0x06}; // command  WREN:06
         var cmdRd = new byte[] {0x03, 0, 0, 0}; // command Read:03
         var cmdPp = new byte[4 + PageByteSize];
         var data = new byte[4 + PageByteSize];


         for (long i = 0; i < PageByteSize; i++)
         {
            cmdPp[i + 4] = memory[sectorTopAddress + i]; // copy page into buffer
         }


         // writeMessage("Programming  0x" + pageTopAddress.ToString("X8"));
         //WriteDot();

         // wait until ongoin write/erase becomes complete
         if (!WaitCompletition())
         {
            return;
         }

         // execute WREN command
         Spi.Transfer(cmdWren, null, LatchOn.Rising);

         // set address. Address is big endian
         cmdPp[0] = 0x02; // page program command.
         cmdPp[1] = BitConverter.GetBytes(sectorTopAddress)[2];
         cmdPp[2] = BitConverter.GetBytes(sectorTopAddress)[1];
         cmdPp[3] = BitConverter.GetBytes(sectorTopAddress)[0];
         // execute PP command
         Spi.Transfer(cmdPp, null, LatchOn.Rising);

         // Now, verify it.

         // wait until ongoin write/erase becomes complete
         if (!WaitCompletition())
         {
            return;
         }
         // set address. Address is big endian
         cmdRd[1] = BitConverter.GetBytes(sectorTopAddress)[2];
         cmdRd[2] = BitConverter.GetBytes(sectorTopAddress)[1];
         cmdRd[3] = BitConverter.GetBytes(sectorTopAddress)[0];


         // verify
         var fail = true;
         var counter = 0;
         var failAddress = 0;

         while (fail & (counter < 4))
         {
            // execute Read command
            Spi.Transfer(cmdRd, data, LatchOn.Rising);

            // supporse it is success ful
            fail = false;
            for (var i = 0; i < PageByteSize; i++)
               if (data[4 + i] != memory[sectorTopAddress + i])
               {
                  failAddress = i;
                  fail = true;
               }
            counter++;
         }
         if (fail)
            throw new FlashVerifyException(Properties.Resources.VerifyError +
                                           (sectorTopAddress + failAddress).ToString("X04"));
      }

      #endregion
   }
} // namespace JtagKey