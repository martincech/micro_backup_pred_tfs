/*
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/
 * \file abstractflash.cs
 * \Author Suikan
 * \brief fash programmer alogorithm part
 */


using System;
using System.Threading;
using GUI;
using Kinetis.JtagKey;

namespace Kinetis.RomWriter
{
   ///<summary>
   /// \brief Flash ROM programming algorithm class
   /// 
   /// This is abstract class to define a flash writer. 
   /// 
   ///</summary>
   public abstract class AbstractFlash
   {
      protected AbstractFlash(AbstractSpi aSpi, ITextBox aTextBox, IProgressBar aProgressBar)
      {
         Spi = aSpi;
         textbox = aTextBox;
         progressbar = aProgressBar;
      }

      protected AbstractSpi Spi;
      private ITextBox textbox; // reference to the indicator
      private IProgressBar progressbar; // reference to the indicator
      private CancellationToken? cancellation;


      ///<summary>
      /// \brief return Vender name
      ///</summary>
      public abstract string VendorString { get; }

      ///<summary>
      /// \brief return device name
      ///</summary>
      public abstract string DeviceString { get; }

      ///<summary>
      /// \brief return device size
      ///</summary>
      public abstract int DeviceBitSize { get; }

      ///<summary>
      /// \brief return device size
      ///</summary>
      public abstract int DeviceByteSize { get; }


      ///<summary>
      /// \brief return page size
      /// 
      /// This must be meaningful only for SPI flash.
      /// 
      ///</summary>
      public abstract int PageByteSize { get; }

      ///<summary>
      /// \brief return block count
      /// 
      /// The value is calcurated based on the result of GetID method.
      ///</summary>
      public abstract int BlockCount { get; }

      ///<summary>
      /// \brief return block size
      /// 
      /// The value is calcurated based on the result of GetID method.
      /// This value could be vary based on the device. You should change
      /// this property if you add new device.
      /// 
      /// Anyway, this class supports only the uniform block size architecture.
      ///</summary>
      public abstract BlockInfo[] Block { get; }

      
      ///<summary>
      /// \brief Initializing obuject
      /// \param aSpi : USB-SPI communicator
      /// \param aTextBox : Message output text box. can be null
      /// \param aProgressBar : Progress output bar for programming
      /// 
      /// Initialize internal object
      ///</summary>
      public virtual void Initialize(CancellationToken? aCancellation = null)
      {
         cancellation = aCancellation;
      }

      ///<summary>
      /// \brief get Flash ID by Command
      /// \return true if known, false if unknown
      /// 
      /// Get ID and put the result into internal variable.
      ///</summary>
      public abstract bool GetID();

      ///<summary>
      /// \brief set Flash ID of ID Command is not existing
      /// \param aDevices an integer which represent parts number.
      /// \return true if known, false if unknown
      /// 
      /// Get ID and put the result into internal variable.
      ///</summary>
      public abstract bool SetID(int aDevice);

      ///<summary>
      /// \brief status of Chip ID
      /// 
      /// This is interface property to get/set Chip Status register through SPI.
      ///</summary>
      protected abstract byte Status { get; set; }

      protected bool Cancel
      {
         get
         {
            var cancel = cancellation.HasValue && cancellation.Value.IsCancellationRequested;
            if (cancel)
            {
               WriteMessage(Properties.Resources.OperationCanceled);
            }
            return cancel;
         }
      }

      ///<summary>
      /// \brief check if the SPI Flash is busy.
      /// \return true if SPI flashis busy. false if ready to write
      ///</summary>
      protected abstract bool IsBusy { get; }


      ///<summary>
      /// \brief Reset the target, if required.
      /// \param systemClockMHz Clock frequency of coldfire by MHz.
      /// 
      /// In this abstract layer, do nothing
      /// 
      ///</summary>
      protected virtual bool InitTarget()
      {
         return (true);
      }

      ///<summary>
      /// \brief Do bulk erase
      /// 
      /// Erase entire contents of SPI Flash.
      ///</summary>
      public abstract void EraseAll();

      ///<summary>
      /// \brief Do Block erase
      /// 
      /// Block erase is somtimes called as sector erase ( depend on the device ).
      /// For M25P40, the block size is 64KB, and can obtain by BlockByteSize property.
      /// This method erase a block wich contains specified address.
      ///</summary>
      protected abstract void EraseBlock(long address);

      ///<summary>
      /// \brief Check if entire flash rom is blank
      /// \return true if entire ROM is blank. false if not.
      /// 
      /// Check entire flash. If all blank, return true. Else, false.
      ///</summary>
      public abstract bool IsBlanROM();

      ///<summary>
      /// \brief program all are
      /// \param memory : total memory image of ROM
      /// \param eraseAllMemory : true if you want all memory
      /// \param systemClockMHz EzPort target requires system clock.
      /// 
      /// This method programs all memory area. 
      /// Note that memory should be the total image of ROM.
      ///</summary>
      public abstract bool Program(byte[] memory, bool eraseAllMemory, Single systemClockMHz); // Program    

      /// <summary>
      /// Read the whole memory
      /// </summary>
      /// <param name="memory">total memory image of ROM</param>
      /// <param name="systemClockMHz">EzPort target requires system clock</param>
      /// <returns>execution status</returns>
      public abstract bool ReadMemory(out byte[] memory, Single systemClockMHz);
      /// <summary>
      /// \brief debug support
      /// \param s string to write to textbox
      /// 
      /// Receive a string and print it into the console textbox
      /// </summary>
      protected void WriteMessage(string s)
      {
         if (textbox != null)
         {
            textbox.AppendText(s + Environment.NewLine);
         }
      }

      /// <summary>
      /// \brief Print a dot into console to tell end user still alive.
      /// 
      /// </summary>
      protected void WriteDot()
      {
         if (textbox != null)
         {
            textbox.AppendText(".");
         }
      }

      protected void SetProgress(double value)
      {
         if (progressbar != null)
         {
            progressbar.Value = value;
         }
        
      }

      protected bool WaitCompletition()
      {
         while (IsBusy)
         {
            if (!Cancel) continue;
            return false;
         }
         return true;
      }
   }
} // namespace JtagKey