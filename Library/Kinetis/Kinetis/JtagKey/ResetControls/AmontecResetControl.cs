using Ftdi;

namespace Kinetis.JtagKey
{
   public class AmontecResetControl : IResetControl
   {
      public void EnableTap(ref InputOutputPins gpioH)
      {
         gpioH.Pin0 = LogicLevel.H; // deassert TRST;
         gpioH.Pin2 = LogicLevel.L; // enable TRST

         gpioH.Pin1 = LogicLevel.L; // keep it L always
         gpioH.Pin3 = LogicLevel.H; // Hi-Z  SRST
      }

      public void DisableTap(ref InputOutputPins gpioH)
      {
         gpioH.Pin0 = LogicLevel.L; // eassert TRST;
         gpioH.Pin2 = LogicLevel.H; // disable TRST

         gpioH.Pin1 = LogicLevel.L; // keep it L always
         gpioH.Pin3 = LogicLevel.H; // Hi-Z  SRST
      }

      public void AssertSRST(ref InputOutputPins gpioH)
      {
         // state of pin0 is unchanged.
         gpioH.Pin2 = LogicLevel.L; // enable TRST

         gpioH.Pin1 = LogicLevel.L; // SRST = L 
         gpioH.Pin3 = LogicLevel.L; // SRST is output (L)
      }

      public void DeassertSRST(ref InputOutputPins gpioH)
      {
         // state of pin0 is unchanged.
         gpioH.Pin2 = LogicLevel.L; // enable TRST

         gpioH.Pin1 = LogicLevel.L; // Keep it L
         gpioH.Pin3 = LogicLevel.H; // Hi-Z SRST
      }

      public void AssertTRST(ref InputOutputPins gpioH)
      {
         gpioH.Pin0 = LogicLevel.L; // Assert
         gpioH.Pin2 = LogicLevel.L; // enable TRST

         gpioH.Pin1 = LogicLevel.L; // keep it L
         // state of pin3 is unchanged.
      }

      public void DeassertTRST(ref InputOutputPins gpioH)
      {
         gpioH.Pin0 = LogicLevel.H; // Deassert
         gpioH.Pin2 = LogicLevel.L; // enable TRST

         gpioH.Pin1 = LogicLevel.L; // keep it L always
         // state of pin3 is unchanged.
      }
   }
}