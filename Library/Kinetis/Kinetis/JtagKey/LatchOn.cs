namespace Kinetis.JtagKey
{
   /// <summary>
   /// Latchin edge type.
   /// 
   /// This type is used to specify the latching edge of SPI. The shifting was done on 
   /// the other edge. Note that this edge specification is common for both TX and RX.
   /// </summary>
   public enum LatchOn
   {
      /// Latching is done on Rising edge. Shifting is done on Falling edge.
      Rising,

      /// Latching is done on Falling edge. Shifting is done on Rising edge.
      Falling
   };
}