/**
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/
* \file abstractspi.cs
* \Author Suikan
* Interface for SPI by Amontec JTAG 
* 
* This Provide asbstract class for accessing SPI device through Amontech JTAGKEY
* 
*/

using System;
using System.Collections.Generic;
using System.Text;
using Ftdi.Spi;

namespace Kinetis.JtagKey
{
   /// <summary>
   /// Interface class for SPI by Amontec JTAG
   /// 
   /// This class provides limited but simplifed software interface to use Amontec JTAG Key
   /// as SPI master device. By its simplicity, this class hide away FTCSPI.DLL's complicated
   /// interface ans settings.
   /// 
   /// This class assumes you use .Net Framework as interface parts. To start the
   /// task, you should step following sequence.
   /// 
   /// -# Create an instance of AbstractSpi with clock, edge information.
   /// -# Create a list of FT2232 devices by static method UsbSpi.ListUp(). This will 
   /// create a string list. So, you should have a comboBox to select device
   /// on your interface.
   /// -# If user select an item from the above comboBox, call open() mthod to 
   /// set the specified JTAG Key ready to use. 
   /// -# Now, you can read() and write() method to communicate with your target device.
   /// -# call close() when you leave your application.
   /// </summary>
   public abstract class AbstractSpi
   {
      /// <summary> Freq= 6/(1+divisor)MHz  </summary>
      protected int Divisor;

      /// <summary> variable to be a handle for FT2232 device  </summary>
      protected int Handle;

      /// <summary> for management of state  </summary>
      public bool Opened { get; protected set; }

      /// <summary> synchlonized location list to the comboBox's list  </summary>
      protected static List<int> DeviceLocationList = new List<int>();

      /// <summary> synchlonized name list to the comboBox's list  </summary>
      protected static List<string> DeviceNameList = new List<string>();


      public virtual bool Identified(out string vender, out string name)
      {
         vender = "";
         name = "";
         return false;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      protected AbstractSpi()
      {
         Opened = false;
      }

      /// <summary>
      /// Internal method to handle DLL's error 
      /// \param status  returned status from function.
      /// 
      /// This internal method is used to throw an exception when FTCSPI.DLL or FTCJTAG.DLL returns
      /// error status. Status is converted to string and throw away with exception.
      /// </summary>
      protected abstract void ThrowError(object status);


      /// <summary>
      /// list up the name of FT2232 device in this sytem.
      /// \return List of FT2232 device
      /// 
      /// This method seach the system and count up all the found FT2232. And put their
      /// name into the list of string. Some of these devices may have same name.
      /// In this case, this function doesn't care and simply put them into the list as same 
      /// name. In other word, customer cannot distinguish such the devices.
      /// 
      /// Beside of this, between calling this function and choosing list by customer, other
      /// devices may be set into system by USB connection. In this case, the list
      /// becomes older, and UsbSpi.open() function may call with older number. To prevent the
      /// trouble and guarantee to open realy chosen device, UsbSpi class has its own list 
      /// internaly.
      /// </summary>
      public static List<string> ListUp()
      {
         int numDevice; // number of FT2232 device in this sytem
         var deviceName = new StringBuilder(255);
         var target = new List<string>();

         target.Clear(); // clean up current list
         DeviceNameList.Clear();
         DeviceLocationList.Clear();

         // count how many FT2232 is in system
         // stat = SpiDll.GetNumDevices(out numDevice);
         try
         {
            // count how many FT2232 is in system
            FtcSpi.GetNumDevices(out numDevice);
         }
         catch (DllNotFoundException)
         {
            numDevice = 0;
         }

         // enumurate the name of devices as list in the combo box.
         for (var i = 0; i < numDevice; i++)
         {
            // get name.
            int location; // location of FT2232 device in USB subsystem
            FtcSpi.GetDeviceNameLocID(
               i,
               deviceName,
               deviceName.Capacity,
               out location);
            // add name in the list
            target.Add(deviceName.ToString());
            DeviceLocationList.Add(location); // update internal list
            DeviceNameList.Add(deviceName.ToString()); // update internal list
         }
         return target;
      }

      /// <summary>
      /// retur the version string 
      /// </summary>
      public abstract string GetDllVersion();

      /// <summary>
      /// constructor
      /// \param aDivisor  12/(1+aDivisor)/2 MHz.
      /// 
      /// Setup given parameter into the instans variable to be ready to open the device.
      /// </summary>
      protected AbstractSpi(int aDivisor)
      {
         Divisor = aDivisor;
      }

      /// <summary>
      /// Open the FT2232 device.
      /// \param deviceIndex  is the selected index of ComboBox which has list of devices.
      /// 
      /// This function open the FT2232 device which is specified by parameter. After opening, 
      /// It calls FTDI.SPI.InitDevice function and FTDI.SPI.SetGPIOs to set the chipselect function
      /// and speed.
      /// </summary>
      public abstract void Open(int deviceIndex);

      /// <summary>
      /// closing FT2232
      /// </summary>
      public abstract void Close();

      /// <summary>
      /// Write/Read data bytes from SPI device.
      /// \param txData  transmite data buffer to send to device
      /// \param rxData  data buffer to receive data
      /// \param LatchingEdge  The edge which MISO samples data. 
      /// \param chipSelect specify which chip select is used. The value is 0,1,2...
      /// </summary>
      public abstract void Transfer(byte[] txData, byte[] rxData, LatchOn latchingEdge);


      /// <summary>
      /// Set the CS pin as "L"
      /// \param LatchingEdge  The edge which MISO samples data. 
      /// </summary>
      public virtual void AssertCs(LatchOn latchingEdge)
      {
      }

      /// <summary>
      /// Set the CS pin as "H"
      /// \param LatchingEdge  The edge which MISO samples data. 
      /// </summary>
      public virtual void DeassertCs(LatchOn latchingEdge)
      {
      }

      /// <summary>
      /// Set the CS pin as "L"
      /// </summary>
// ReSharper disable once InconsistentNaming
      public virtual void AssertSRST()
      {
      }

      /// <summary>
      /// Set the CS pin as "H"
      /// </summary>
// ReSharper disable once InconsistentNaming
      public virtual void DeassertSRST()
      {
      }
   }
}