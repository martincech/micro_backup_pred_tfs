//******************************************************************************
//
//    KinetisFlash.cs     Kinetis flash
//    Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

using System;
using System.Diagnostics;
using System.Threading;
using GUI;
using Kinetis.JtagKey.JtagKey;
using Kinetis.RomWriter;
namespace Kinetis {
   public class Flash : SpiFlash
   {
      //-----------------------------------------------------------------------------
      // Initialisation
      //-----------------------------------------------------------------------------
      /// <summary>
      /// Initialize flash object
      /// </summary>
      /// <param name="ezPort">ez port</param>
      /// <param name="aTextBox">text box</param>
      /// <param name="aProgressBar">progress bar</param>
      public Flash(EzPort ezPort, ITextBox aTextBox, IProgressBar aProgressBar)
         : base(ezPort, aTextBox, aProgressBar)
      {
         EzPort = ezPort;
         Fccob = new Fccob(this);
      } // Initialize

      //-----------------------------------------------------------------------------
      // Fccob
      //-----------------------------------------------------------------------------

      public Fccob Fccob { get; private set; } // _Fccob

      //-----------------------------------------------------------------------------
      // ezPort
      //-----------------------------------------------------------------------------

      public EzPort EzPort { get; private set; }

      public bool IsConnected
      {
         get { return EzPort.Opened && !IsBusy; }
      }

      public bool Busy
      {
         get { return base.IsBusy; }
      }
     
     
      private static object lockObject = new object();
    
      /// <summary>
      /// Execute operation on flash in other thread
      /// </summary>
      /// <param name="actionWithPort">action to be performed on flash</param>
      /// <param name="index">index of connected ftdi device</param>
      /// <param name="cancellation">token for cancelation operation</param>
      public void ExecuteOperation(Action<Flash> actionWithPort, int index,
         CancellationTokenSource cancellation)
      {
         ThreadPool.QueueUserWorkItem(device => Execute(actionWithPort, (int)device, cancellation), index);
      }

      /// <summary>
      /// Execute operation on flash in same thread (waiting for complete operation)
      /// </summary>
      /// <param name="actionWithPort">action to be performed on flash</param>
      /// <param name="index">index of connected ftdi device</param>
      /// <param name="cancellation">token for cancelation operation</param>
      public void Execute(Action<Flash> actionWithPort, int index, CancellationTokenSource cancellation)
      {
         lock (lockObject)
         {
            if (OnExecutionStart != null)
               OnExecutionStart(this, new EventArgs());
            try
            {

               EzPort.Open(index);
            }
            catch
            {
               return;
            }
            InitTarget();
            Initialize(cancellation.Token);
            actionWithPort.Invoke(this);
            EzPort.AssertSRST();
            Thread.Sleep(100);
            EzPort.DeassertSRST();
            EzPort.Close();

            if (OnExecutionStop != null)
               OnExecutionStop(this, new EventArgs());
         }
      }

      public event EventHandler OnExecutionStart;
      public event EventHandler OnExecutionStop;
   }
}