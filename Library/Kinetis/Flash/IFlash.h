//*****************************************************************************
//
//    IFlash.h       Kinetis internal flash programming
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#ifndef __IFlash_H__
   #define __IFlash_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

void IFlashInit( void);
// Init

TYesNo IFlashReadOnce(byte Address, void *_Data, byte Length);
// Read once from <Address>

TYesNo IFlashErase( dword StartSectorAddress, dword EndSectorAddress);
// Erase <StartSectorAddress;EndSectorAddress>; adresses internally extended to sectors boundaries

TYesNo IFlashProgram( dword Address, byte *Data, dword Size);
// Program <Address> by <Data> with <Size>
// <Address>, <Size> aligned to 4/8 multiplies depending on flash configuration

#ifdef __cplusplus
   }
#endif

#endif
