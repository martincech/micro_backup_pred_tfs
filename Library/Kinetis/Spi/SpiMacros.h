//*****************************************************************************
//
//   SpiMacros.h   Kinetis SPI macros
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SpiMacros_H__
   #define __SpiMacros_H__

#ifndef SPI_CHANNELS_PER_SUBMODULE
   #define SPI_CHANNELS_PER_SUBMODULE  1
#endif

#define CHANNELS_TOTAL_COUNT    (MODULE_COUNT * SUBMODULES_PER_MODULE * SPI_CHANNELS_PER_SUBMODULE)

#if CHANNELS_TOTAL_COUNT > 24
   #error Maximum number of Spi channels exceeded
#endif

static const TPin CsPins[CHANNELS_TOTAL_COUNT] = {
#if CHANNELS_TOTAL_COUNT >= 1
   #ifndef SPI0_CS_PIN
      #define SPI0_CS_PIN     0
   #endif
   SPI0_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 2
   #ifndef SPI1_CS_PIN
      #define SPI1_CS_PIN     0
   #endif
   , SPI1_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 3
   #ifndef SPI2_CS_PIN
      #define SPI2_CS_PIN     0
   #endif
   , SPI2_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 4
   #ifndef SPI3_CS_PIN
      #define SPI3_CS_PIN     0
   #endif
   , SPI3_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 5
   #ifndef SPI4_CS_PIN
      #define SPI4_CS_PIN     0
   #endif
   , SPI4_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 6
   #ifndef SPI5_CS_PIN
      #define SPI5_CS_PIN     0
   #endif
   , SPI5_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 7
   #ifndef SPI6_CS_PIN
      #define SPI6_CS_PIN     0
   #endif
   , SPI6_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 8
   #ifndef SPI7_CS_PIN
      #define SPI7_CS_PIN     0
   #endif
   , SPI7_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 9
   #ifndef SPI8_CS_PIN
      #define SPI8_CS_PIN     0
   #endif
   , SPI8_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 10
   #ifndef SPI9_CS_PIN
      #define SPI9_CS_PIN     0
   #endif
   , SPI9_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 11
   #ifndef SPI10_CS_PIN
      #define SPI10_CS_PIN     0
   #endif
   , SPI10_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 12
   #ifndef SPI11_CS_PIN
      #define SPI11_CS_PIN     0
   #endif
   , SPI11_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 13
   #ifndef SPI12_CS_PIN
      #define SPI12_CS_PIN     0
   #endif
   , SPI12_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 14
   #ifndef SPI13_CS_PIN
      #define SPI13_CS_PIN     0
   #endif
   , SPI13_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 15
   #ifndef SPI14_CS_PIN
      #define SPI14_CS_PIN     0
   #endif
   , SPI14_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 16
   #ifndef SPI15_CS_PIN
      #define SPI15_CS_PIN     0
   #endif
   , SPI15_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 17
   #ifndef SPI16_CS_PIN
      #define SPI16_CS_PIN     0
   #endif
   , SPI16_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 18
   #ifndef SPI17_CS_PIN
      #define SPI17_CS_PIN     0
   #endif
   , SPI17_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 16
   #ifndef SPI18_CS_PIN
      #define SPI18_CS_PIN     0
   #endif
   , SPI18_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 20
   #ifndef SPI19_CS_PIN
      #define SPI19_CS_PIN     0
   #endif
   , SPI19_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 21
   #ifndef SPI20_CS_PIN
      #define SPI20_CS_PIN     0
   #endif
   , SPI20_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 22
   #ifndef SPI21_CS_PIN
      #define SPI21_CS_PIN     0
   #endif
   , SPI21_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 23
   #ifndef SPI22_CS_PIN
      #define SPI22_CS_PIN     0
   #endif
   , SPI22_CS_PIN
#if CHANNELS_TOTAL_COUNT >= 24
   #ifndef SPI23_CS_PIN
      #define SPI23_CS_PIN     0
   #endif
   , SPI1_CS_PIN
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
}; // CsPins


#endif