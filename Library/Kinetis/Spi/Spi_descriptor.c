//*****************************************************************************
//
//   Spi.c        Kinetis SPI master interface
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Spi/Spi.h"
#include "Hardware.h"

#define MODULE_COUNT          3
#define PCS_PER_MODULE_COUNT  2

// Baud rate prescalers definition
static const byte Prescalers[] = { 2, 3, 5, 7};
#define PRESCALERS_COUNT sizeof(Prescalers) / sizeof(Prescalers[0])

// Baud rate scalers definition
static const word Scalers[] =    { 2, 4, 6, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768};
#define SCALERS_COUNT sizeof(Scalers) / sizeof(Scalers[0])

typedef struct {
   TPinFunction ClkPin;
   TPinFunction DinPin;
   TPinFunction DoutPin;
   TPin PcsPins[PCS_PER_MODULE_COUNT];
   unsigned Frequency;
} TSpiDescriptor;

// Descriptors
static const TSpiDescriptor Descriptors[MODULE_COUNT] = {
   {
      .ClkPin =  {KINETIS_PIN_SPI0_SCK_PORTD01, KINETIS_PIN_SPI0_SCK_PORTD01_FUNCTION},
      .DinPin =  {KINETIS_PIN_SPI0_SIN_PORTD03, KINETIS_PIN_SPI0_SIN_PORTD03_FUNCTION},
      .DoutPin = {KINETIS_PIN_SPI0_SOUT_PORTD02, KINETIS_PIN_SPI0_SOUT_PORTD02_FUNCTION},
      .PcsPins = {KINETIS_PIN_SPI0_PCS0_PORTD00, KINETIS_PIN_SPI0_PCS1_PORTD04},
      .Frequency = SPI0_CLOCK
   },
   {
      .ClkPin =  {KINETIS_PIN_SPI1_SCK_PORTE02, KINETIS_PIN_SPI1_SCK_PORTE02_FUNCTION},
      .DinPin =  {KINETIS_PIN_SPI1_SIN_PORTE03, KINETIS_PIN_SPI1_SIN_PORTE03_FUNCTION},
      .DoutPin = {KINETIS_PIN_SPI1_SOUT_PORTE01, KINETIS_PIN_SPI1_SOUT_PORTE01_FUNCTION},
      .PcsPins = {KINETIS_PIN_SPI1_PCS0_PORTE04, KINETIS_PIN_SPI1_PCS1_PORTE00},
      .Frequency = SPI1_CLOCK
   },
   {
      .ClkPin =  {KINETIS_PIN_SPI2_SCK_PORTB21, KINETIS_PIN_SPI2_SCK_PORTB21_FUNCTION},
      .DinPin =  {KINETIS_PIN_SPI2_SOUT_PORTB22, KINETIS_PIN_SPI2_SOUT_PORTB22_FUNCTION},
      .DoutPin = {KINETIS_PIN_SPI2_SIN_PORTB23, KINETIS_PIN_SPI2_SIN_PORTB23_FUNCTION},
      .PcsPins = {KINETIS_PIN_SPI2_PCS0_PORTB20, KINETIS_PIN_SPI0_PCS1_PORTC03},
      .Frequency = SPI2_CLOCK
   }
};

#define PcsPinGet( )  (Descriptor->PcsPins[SpiPcs])

// Channel decomposition :
#define SpiDeviceIdGet( SpiAddress)  (SpiAddress / PCS_PER_MODULE_COUNT)
#define SpiNpcsGet( SpiAddress)      (SpiAddress % PCS_PER_MODULE_COUNT)

// Transceive macros:
#define Send( Val) (SpiDevice->PUSHR = Val)
#define Read( )    (SpiDevice->POPR)

#define TransferComplete( Device) (SpiDevice->SR & SPI_SR_TCF_MASK)
#define TransferCompleteClear( Device) (SpiDevice->SR = SPI_SR_TCF_MASK)

// Configuration:
#define Spi2ClockEnable()   SIM->SCGC3 |= SIM_SCGC3_SPI2_MASK;
#define Spi1ClockEnable()   SIM->SCGC6 |= SIM_SCGC6_SPI1_MASK;
#define Spi0ClockEnable()   SIM->SCGC6 |= SIM_SCGC6_SPI0_MASK;
#define ConfigureModule( Val) (SpiDevice->MCR = Val)
#define ConfigureChannel( Val) (SpiDevice->CTAR[SpiPcs] = Val)
#define ConfigureBaudRate( Half, Prescaler, Scaler)     \
   SpiDevice->CTAR[SpiPcs] &= ~(SPI_CTAR_DBR_MASK | SPI_CTAR_PBR_MASK, SPI_CTAR_BR_MASK); \
   SpiDevice->CTAR[SpiPcs] |= (Half == 0 ? 0 : SPI_CTAR_DBR_MASK) | SPI_CTAR_PBR(Prescaler) | SPI_CTAR_BR(Scaler);

#define Cpol( Val) (Val ? SPI_CTAR_CPOL_MASK : 0)
#define Cpha( Val) (Val ? SPI_CTAR_CPHA_MASK : 0)
#define FrameSize( Val) SPI_CTAR_FMSZ(Val - 1)
#define Master( Val) (Val ? SPI_MCR_MSTR_MASK : 0)

// Misc:
#define Average(a, b)   (((a) + (b)) / 2)
#define Abs(a)          (((signed)a) >= 0 ? (a) : -(a))

// Local functions :
static volatile SPI_Type *_SpiDeviceGet( TSpiAddress SpiAddress);
// Returns SPI device by <SpiAddress>

static void BaudRateSet(TSpiAddress Spi, unsigned BaudRate);
// Sets <BaudRate>
//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void SpiInit( TSpiAddress Spi)
// Initialize bus
{
byte SpiDeviceId;
volatile SPI_Type *SpiDevice;
byte SpiPcs;
const TSpiDescriptor *Descriptor;
  SpiDeviceId = SpiDeviceIdGet(Spi);
   SpiDevice   = _SpiDeviceGet( Spi);
   SpiPcs      = SpiNpcsGet( Spi);
   Descriptor  = &Descriptors[SpiDeviceId];
   // GPIO functions :
   GpioSet( PcsPinGet());
   GpioOutput( PcsPinGet());
   PinFunction( PcsPinGet(), KINETIS_GPIO_FUNCTION);

   PinFunction( Descriptor->ClkPin.Pin, Descriptor->ClkPin.Function);
   PinFunction( Descriptor->DinPin.Pin, Descriptor->DinPin.Function);
   PinFunction( Descriptor->DoutPin.Pin, Descriptor->DoutPin.Function);

   switch(SpiDeviceId) {
      case 0:
         Spi0ClockEnable();
         break;

      case 1:
         Spi1ClockEnable();
         break;

      case 2:
         Spi2ClockEnable();
         break;

      default:
         return;
   }

   ConfigureModule( Master(YES));
   ConfigureChannel( Cpol(0) | Cpha(0) | FrameSize(8));
   BaudRateSet(Spi, Descriptor->Frequency);
} // SpiInit

//-----------------------------------------------------------------------------
// Attach
//-----------------------------------------------------------------------------

void SpiAttach( TSpiAddress Spi)
// Attach SPI bus - activate chipselect
{
byte SpiDeviceId;
byte SpiPcs;
const TSpiDescriptor *Descriptor;
   SpiDeviceId = SpiDeviceIdGet( Spi);
   SpiPcs      = SpiNpcsGet( Spi);
   Descriptor  = &Descriptors[SpiDeviceId];

   GpioClr(PcsPinGet());
} // SpiAttach

//-----------------------------------------------------------------------------
// Release
//-----------------------------------------------------------------------------

void SpiRelease( TSpiAddress Spi)
// Release SPI bus - deactivate chipselect
{
byte SpiDeviceId;
byte SpiPcs;
const TSpiDescriptor *Descriptor;
   SpiDeviceId = SpiDeviceIdGet( Spi);
   SpiPcs      = SpiNpcsGet( Spi);
   Descriptor  = &Descriptors[SpiDeviceId];

   GpioSet(PcsPinGet());
} // SpiRelease

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

byte SpiByteRead( TSpiAddress Spi)
// Read byte from SPI
{
volatile SPI_Type *SpiDevice;
word Data;
   SpiDevice   = _SpiDeviceGet( Spi);
   Send(0xFF);
   while(!TransferComplete(SpiDevice));
   TransferCompleteClear();
   Data = Read();;
   return (byte)Data;
} // SpiByteRead

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void SpiByteWrite( TSpiAddress Spi, byte Value)
// Write byte to SPI
{
volatile SPI_Type *SpiDevice;
word Data;
   SpiDevice   = _SpiDeviceGet( Spi);
   Send(Value);
   while(!TransferComplete(SpiDevice));
   TransferCompleteClear();
} // SpiByteWrite

//-----------------------------------------------------------------------------
// Transceive
//-----------------------------------------------------------------------------

byte SpiByteTransceive( TSpiAddress Spi, byte TxValue)
// Write/read SPI. Returns value read
{
volatile SPI_Type *SpiDevice;
word Data;
   SpiDevice   = _SpiDeviceGet( Spi);
   Send(TxValue);
   while(!TransferComplete(SpiDevice));
   TransferCompleteClear();
   Data = Read();
   return (byte)Data;
} // SpiByteTransceive

//******************************************************************************

//-----------------------------------------------------------------------------
//  Get device
//-----------------------------------------------------------------------------

static volatile SPI_Type *_SpiDeviceGet( TSpiAddress SpiAddress)
// Returns SPI device by <SpiAddress>
{
   switch(SpiDeviceIdGet(SpiAddress)) {
      case 0:
         return SPI0;

      case 1:
         return SPI1;

      case 2:
         return SPI2;

      default:
         return SPI0;
   }
} // _SpiDeviceGet

//-----------------------------------------------------------------------------
//  Baud rate set
//-----------------------------------------------------------------------------

static void BaudRateSet(TSpiAddress Spi, unsigned BaudRate)
// Sets <BaudRate>
{
/*
   Funkce na blbouna projde všechny kombinace Double Baud Rate, Prescaler, scaler 
   a najde takovou baudrate, která je nejblíže požadovné baudrate
*/
byte BestFitHalfBaudRateEncoding = 0;
byte BestFitPrescalerEncoding = 0;
byte BestFitScalerEncoding = 0;
unsigned BestFitRealBaudRate = 0;
unsigned RealBaudRate;
byte i;
byte j;
byte k;
volatile SPI_Type *SpiDevice;
byte SpiPcs;
   
   for(i = 0 ; i < 2 ; i++) {
      for(j = 0 ; j < PRESCALERS_COUNT ; j++) {
         for(k = 0 ; k < SCALERS_COUNT ; k++) {
            RealBaudRate = F_BUS / Prescalers[j] * (i + 1) / Scalers[k];
            int RealDifference = RealBaudRate - BaudRate;
            int BestDifference = BestFitRealBaudRate - BaudRate;
            if(Abs(RealDifference) < Abs(BestDifference)) {
               BestFitRealBaudRate = RealBaudRate;
               BestFitHalfBaudRateEncoding = i;
               BestFitPrescalerEncoding = j;
               BestFitScalerEncoding = k;
            }
         }
      }
   }
   
   SpiDevice   = _SpiDeviceGet( Spi);
   SpiPcs      = SpiNpcsGet( Spi);

   ConfigureBaudRate(BestFitHalfBaudRateEncoding, BestFitPrescalerEncoding, BestFitScalerEncoding);
} // BaudRateSet