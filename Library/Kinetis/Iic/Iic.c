//*****************************************************************************
//
//   Iic.c        Kinetis I2C master interface
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Iic/Iic.h"
#include "Hardware.h"
#include "System/Delay.h"

// Baud rate prescalers definition
static const word Prescalers[] = { 20, 22, 24, 26, 28, 30, 34, 40, 28, 32, 36, 40, 44, 48, 56, 68,
                                   48, 56, 64, 72, 80, 88, 104, 128, 80, 96, 112, 128, 144, 160, 192, 
                                   240, 160, 192, 224, 256, 288, 320, 384, 480, 320, 384, 448, 512, 
                                   576, 640, 768, 960, 640, 768, 896, 1024, 1152, 1280, 1536, 1920, 
                                   1280, 1536, 1792, 2048, 2304, 2560, 3072, 3840};
#define PRESCALERS_COUNT sizeof(Prescalers) / sizeof(Prescalers[0])

// Transceive macros:
#define Send( Val)  (IicDevice->D = Val)
#define Read( )     (IicDevice->D)

#define ArbitrationLost( )       (IicDevice->S & (I2C_S_ARBL_MASK))
#define ArbitrationLostClear( )       (IicDevice->S |= I2C_S_ARBL_MASK)
#define TransferComplete( )      (IicDevice->S & I2C_S_IICIF_MASK)
#define TransferCompleteClear( ) (IicDevice->S |= I2C_S_IICIF_MASK)

#define Reset( Val)                          (IicDevice->C1 = 0)
#define ConfigureModule( Val)                (IicDevice->C1 = Val)
#define ConfigureBaudRate( Mult, Prescaler)  (IicDevice->F = I2C_F_MULT(Mult) | I2C_F_ICR(Prescaler))

#define Enable( Val)          (Val ? I2C_C1_IICEN_MASK : 0)
#define SetTransmitMode( Val) if(Val) IicDevice->C1 |= I2C_C1_TX_MASK; else IicDevice->C1 &= ~I2C_C1_TX_MASK
#define Start( )              (IicDevice->C1 |= I2C_C1_MST_MASK)
#define Stop( )               (IicDevice->C1 &= ~I2C_C1_MST_MASK)
#define Transmitting( )       (IicDevice->C1 & I2C_C1_MST_MASK)
#define Acknowledge( Val)     if(!Val) IicDevice->C1 |= I2C_C1_TXAK_MASK; else IicDevice->C1 &= ~I2C_C1_TXAK_MASK
#define RepeatStart()         (IicDevice->C1 |= I2C_C1_RSTA_MASK)
#define Acknowledged()        (!(IicDevice->S & I2C_S_RXAK_MASK))
#define LineBusy()            (IicDevice->S & I2C_S_BUSY_MASK)

// Misc:
#define Abs(a)          (((signed)a) >= 0 ? (a) : -(a))

// Local functions :
static volatile I2C_Type *_IicDeviceGet( TIicAddress IicAddress);
// Returns SPI device by <SpiAddress>

static void BaudRateSet(TIicAddress Iic, unsigned BaudRate);
// Sets <BaudRate>

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void IicInit( TIicAddress Iic)
// Initialize bus
{
volatile I2C_Type *IicDevice;
unsigned Clock;
   
   IicDevice   = _IicDeviceGet( Iic);

   switch(Iic) {
      case IIC_IIC0:
         Iic0PortInit();
         Iic0ClockEnable();
         Clock = IIC0_CLOCK;
         break;
      case IIC_IIC1:
         Iic1PortInit();
         Iic1ClockEnable();
         Clock = IIC1_CLOCK;
         break;
      default:
         return;
   }
   SysDelay(1);
   Reset();
   ConfigureModule( Enable(YES));

   BaudRateSet( Iic, Clock);
} // IicInit

//-----------------------------------------------------------------------------
// Denitialization
//-----------------------------------------------------------------------------

void IicDeinit( TIicAddress Iic)
// Deinitialize bus
{
volatile I2C_Type *IicDevice;

   IicDevice   = _IicDeviceGet( Iic);

   switch(Iic) {
      case IIC_IIC0:
         Iic0PortDeinit();
         Iic0ClockDisable();
         break;
      case IIC_IIC1:
         Iic1PortDeinit();
         Iic1ClockDisable();
         break;
      default:
         return;
   }
   Reset();
} // IicDeinit

//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void IicStart( TIicAddress Iic)
// Sends start sequence
{
volatile I2C_Type *IicDevice;
   IicDevice   = _IicDeviceGet( Iic);
   ArbitrationLostClear();
   TransferCompleteClear();
   SetTransmitMode( YES);
   if(Transmitting()) {
      RepeatStart();
   } else {
      while(LineBusy()); // wait until line is safely released
      Start();
   }
   while(!LineBusy());
   //SetTransmitMode(YES);
} // IicStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void IicStop( TIicAddress Iic)
// Sends stop sequence
{
volatile I2C_Type *IicDevice;
   IicDevice   = _IicDeviceGet( Iic);
   Stop();
} // IicStop

//-----------------------------------------------------------------------------
// Send
//-----------------------------------------------------------------------------

TYesNo IicSend( TIicAddress Iic, byte Value)
// Sends <Value>, returns YES if ACK
{
volatile I2C_Type *IicDevice;
   IicDevice   = _IicDeviceGet( Iic);

   Send( Value);
   while(!TransferComplete()) {
      if(ArbitrationLost()) {
         return NO;
      }
   }
   TransferCompleteClear();
   if(Acknowledged()) {
      return YES;
   }

   return NO;
} // IicSend

//-----------------------------------------------------------------------------
// Receive
//-----------------------------------------------------------------------------

byte IicReceive( TIicAddress Iic, TYesNo Ack)
// Returns received byte, sends confirmation <ack>.
{
volatile I2C_Type *IicDevice;
byte Value;
   IicDevice   = _IicDeviceGet( Iic);

   SetTransmitMode( NO);
   Acknowledge( Ack);
   Read();
   while(!TransferComplete()) {
      if(ArbitrationLost()) {
         return 0;
      }
   }
   TransferCompleteClear();
   SetTransmitMode( YES);
   Value = Read();
   return Value;
} // SpiByteWrite

//******************************************************************************

//-----------------------------------------------------------------------------
//  Get device
//-----------------------------------------------------------------------------

static volatile I2C_Type *_IicDeviceGet( TIicAddress IicAddress)
// Returns IIC device by <IicAddress>
{
   switch(IicAddress) {
      case 0:
         return I2C0;

      case 1:
         return I2C1;

      default:
         return I2C0;
   }
} // _IicDeviceGet

//-----------------------------------------------------------------------------
//  Baud rate set
//-----------------------------------------------------------------------------

static void BaudRateSet(TIicAddress Iic, unsigned BaudRate)
// Sets <BaudRate>
{
/*
   Funkce tupe projde vsechny kombinace Double Baud Rate, Prescaler, scaler 
   a najde takovou baudrate, ktera je nejblize pozadovane baudrate
*/

byte BestFitMul = 0;
byte BestFitPrescaler = 0;
dword BestFitRealBaudRate = 0x7FFFFFFF;
dword RealBaudRate;
dword RealDifference;
dword BestDifference;
byte i;
byte j;
volatile I2C_Type *IicDevice;

   for(i = 0 ; i <= 2 ; i++) {
      for(j = 0 ; j < PRESCALERS_COUNT ; j++) {
         RealBaudRate = F_BUS / ( (1 << i) * Prescalers[j] );
         RealDifference = RealBaudRate - BaudRate;
         BestDifference = BestFitRealBaudRate - BaudRate;
         if(Abs(RealDifference) < Abs(BestDifference)) {
            BestFitRealBaudRate = RealBaudRate;
            BestFitMul = i;
            BestFitPrescaler = j;
         }
      }
   }
   
   IicDevice   = _IicDeviceGet( Iic);

   ConfigureBaudRate(BestFitMul, BestFitPrescaler);
} // BaudRateSet