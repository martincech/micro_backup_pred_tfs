//******************************************************************************
//
//   MenuPlatformCalibration.h  Platform calibration menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformCalibration_H__
   #define __MenuPlatformCalibration_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformCalibration( void);
// Menu platform calibration

#endif
