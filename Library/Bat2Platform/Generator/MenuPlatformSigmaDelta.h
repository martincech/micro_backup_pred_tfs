//******************************************************************************
//
//   MenuPlatformSigmaDelta.h  Platform sigma delta menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformSigmaDelta_H__
   #define __MenuPlatformSigmaDelta_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformSigmaDelta( void);
// Menu platform sigma delta

#endif
