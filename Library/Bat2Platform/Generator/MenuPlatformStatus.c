//******************************************************************************
//
//   MenuPlatformStatus.c  Platform status menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformStatus.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"


static DefMenu( PlatformStatusMenu)
   STR_OPERATION,
   STR_ERROR,
   STR_POWER,
   STR_SAMPLES_COUNT,
   STR__DUMMY2,
   STR_WEIGHT,
EndMenu()

typedef enum {
   MI_OPERATION,
   MI_ERROR,
   MI_POWER,
   MI_SAMPLES_COUNT,
   MI__DUMMY2,
   MI_WEIGHT
} EPlatformStatusMenu;

// Local functions :

static void PlatformStatusParameters( int Index, int y, TPlatformStatus *Parameters);
// Draw platform status parameters

//------------------------------------------------------------------------------
//  Menu PlatformStatus
//------------------------------------------------------------------------------

void MenuPlatformStatus( void)
// Edit platform status parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_STATUS, PlatformStatusMenu, (TMenuItemCb *)PlatformStatusParameters, &PlatformStatus, &MData)){
         ConfigPlatformStatusSave();
         return;
      }
      switch( MData.Item){
         case MI_OPERATION :
            i = PlatformStatus.Operation;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PLATFORM_OPERATION, _PLATFORM_OPERATION_LAST)){
               break;
            }
            PlatformStatus.Operation = (byte)i;
            break;

         case MI_ERROR :
            i = PlatformStatus.Error;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PLATFORM_ERROR, _PLATFORM_ERROR_LAST)){
               break;
            }
            PlatformStatus.Error = (byte)i;
            break;

         case MI_POWER :
            i = PlatformStatus.Power;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PLATFORM_POWER, _PLATFORM_POWER_LAST)){
               break;
            }
            PlatformStatus.Power = (byte)i;
            break;

         case MI_SAMPLES_COUNT :
            i = PlatformStatus.SamplesCount;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_STATUS_SAMPLES_COUNT_MIN, PLATFORM_STATUS_SAMPLES_COUNT_MAX, 0)){
               break;
            }
            PlatformStatus.SamplesCount = (word)i;
            break;

         case MI__DUMMY2 :
            i = PlatformStatus._Dummy2;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_STATUS__DUMMY2_MIN, PLATFORM_STATUS__DUMMY2_MAX, 0)){
               break;
            }
            PlatformStatus._Dummy2 = (word)i;
            break;

         case MI_WEIGHT :
            i = PlatformStatus.Weight;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformStatus.Weight = (TWeightGauge)i;
            break;

      }
   }
} // MenuPlatformStatus

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformStatusParameters( int Index, int y, TPlatformStatus *Parameters)
// Draw platform status parameters
{
   switch( Index){
      case MI_OPERATION :
         DLabelEnum( Parameters->Operation, ENUM_PLATFORM_OPERATION, DMENU_PARAMETERS_X, y);
         break;

      case MI_ERROR :
         DLabelEnum( Parameters->Error, ENUM_PLATFORM_ERROR, DMENU_PARAMETERS_X, y);
         break;

      case MI_POWER :
         DLabelEnum( Parameters->Power, ENUM_PLATFORM_POWER, DMENU_PARAMETERS_X, y);
         break;

      case MI_SAMPLES_COUNT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->SamplesCount, 0);
         break;

      case MI__DUMMY2 :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->_Dummy2, 0);
         break;

      case MI_WEIGHT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->Weight);
         break;

   }
} // PlatformStatusParameters
