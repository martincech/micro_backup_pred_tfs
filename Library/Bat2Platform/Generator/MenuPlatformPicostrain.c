//******************************************************************************
//
//   MenuPlatformPicostrain.c  Platform picostrain menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformPicostrain.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"


static DefMenu( PlatformPicostrainMenu)
   STR_RATE,
   STR_PREFILTER,
   STR_FILTER,
   STR_MODE,
   STR_ACCURACY_SWITCH,
   STR_ACCURACY_FINE,
   STR_ACCURACY_COARSE,

EndMenu()

typedef enum {
   MI_RATE,
   MI_PREFILTER,
   MI_FILTER,
   MI_MODE,
   MI_ACCURACY_SWITCH,
   MI_ACCURACY_FINE,
   MI_ACCURACY_COARSE,

} EPlatformPicostrainMenu;

// Local functions :

static void PlatformPicostrainParameters( int Index, int y, TPlatformPicostrain *Parameters);
// Draw platform picostrain parameters

//------------------------------------------------------------------------------
//  Menu PlatformPicostrain
//------------------------------------------------------------------------------

void MenuPlatformPicostrain( void)
// Edit platform picostrain parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_PICOSTRAIN, PlatformPicostrainMenu, (TMenuItemCb *)PlatformPicostrainParameters, &PlatformPicostrain, &MData)){
         ConfigPlatformPicostrainSave();
         return;
      }
      switch( MData.Item){
         case MI_RATE :
            i = PlatformPicostrain.Rate;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_PICOSTRAIN_RATE_MIN, PLATFORM_PICOSTRAIN_RATE_MAX, "Hz")){
               break;
            }
            PlatformPicostrain.Rate = (word)i;
            break;

         case MI_PREFILTER :
            i = PlatformPicostrain.Prefilter;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_PICOSTRAIN_PREFILTER_MIN, PLATFORM_PICOSTRAIN_PREFILTER_MAX, 0)){
               break;
            }
            PlatformPicostrain.Prefilter = (word)i;
            break;

         case MI_FILTER :
            i = PlatformPicostrain.Filter;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PICOSTRAIN_FILTER, _PICOSTRAIN_FILTER_LAST)){
               break;
            }
            PlatformPicostrain.Filter = (byte)i;
            break;

         case MI_MODE :
            i = PlatformPicostrain.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PICOSTRAIN_MODE, _PICOSTRAIN_MODE_LAST)){
               break;
            }
            PlatformPicostrain.Mode = (byte)i;
            break;

         case MI_ACCURACY_SWITCH :
            i = PlatformPicostrain.AccuracySwitch;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformPicostrain.AccuracySwitch = (byte)i;
            break;

         case MI_ACCURACY_FINE :
            i = PlatformPicostrain.AccuracyFine;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_PICOSTRAIN_ACCURACY_FINE_MIN, PLATFORM_PICOSTRAIN_ACCURACY_FINE_MAX, 0)){
               break;
            }
            PlatformPicostrain.AccuracyFine = (byte)i;
            break;

         case MI_ACCURACY_COARSE :
            i = PlatformPicostrain.AccuracyCoarse;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_PICOSTRAIN_ACCURACY_COARSE_MIN, PLATFORM_PICOSTRAIN_ACCURACY_COARSE_MAX, 0)){
               break;
            }
            PlatformPicostrain.AccuracyCoarse = (byte)i;
            break;

      }
   }
} // MenuPlatformPicostrain

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformPicostrainParameters( int Index, int y, TPlatformPicostrain *Parameters)
// Draw platform picostrain parameters
{
   switch( Index){
      case MI_RATE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->Rate, "Hz");
         break;

      case MI_PREFILTER :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Prefilter, 0);
         break;

      case MI_FILTER :
         DLabelEnum( Parameters->Filter, ENUM_PICOSTRAIN_FILTER, DMENU_PARAMETERS_X, y);
         break;

      case MI_MODE :
         DLabelEnum( Parameters->Mode, ENUM_PICOSTRAIN_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_ACCURACY_SWITCH :
         DLabelEnum( Parameters->AccuracySwitch, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_ACCURACY_FINE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->AccuracyFine, 0);
         break;

      case MI_ACCURACY_COARSE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->AccuracyCoarse, 0);
         break;

   }
} // PlatformPicostrainParameters
