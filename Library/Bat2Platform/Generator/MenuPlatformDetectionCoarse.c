//******************************************************************************
//
//   MenuPlatformDetectionCoarse.c  Platform detection coarse menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformDetectionCoarse.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"


static DefMenu( PlatformDetectionCoarseMenu)
   STR_AVERAGING_WINDOW,
   STR_STABLE_WINDOW,
   STR_ABSOLUTE_RANGE,
   STR_SWITCHOVER_RANGE,
EndMenu()

typedef enum {
   MI_AVERAGING_WINDOW,
   MI_STABLE_WINDOW,
   MI_ABSOLUTE_RANGE,
   MI_SWITCHOVER_RANGE
} EPlatformDetectionCoarseMenu;

// Local functions :

static void PlatformDetectionCoarseParameters( int Index, int y, TPlatformDetectionCoarse *Parameters);
// Draw platform detection coarse parameters

//------------------------------------------------------------------------------
//  Menu PlatformDetectionCoarse
//------------------------------------------------------------------------------

void MenuPlatformDetectionCoarse( void)
// Edit platform detection coarse parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DETECTION_COARSE, PlatformDetectionCoarseMenu, (TMenuItemCb *)PlatformDetectionCoarseParameters, &PlatformDetectionCoarse, &MData)){
         ConfigPlatformDetectionCoarseSave();
         return;
      }
      switch( MData.Item){
         case MI_AVERAGING_WINDOW :
            i = PlatformDetectionCoarse.AveragingWindow;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_DETECTION_COARSE_AVERAGING_WINDOW_MIN, PLATFORM_DETECTION_COARSE_AVERAGING_WINDOW_MAX, 0)){
               break;
            }
            PlatformDetectionCoarse.AveragingWindow = (byte)i;
            break;

         case MI_STABLE_WINDOW :
            i = PlatformDetectionCoarse.StableWindow;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_DETECTION_COARSE_STABLE_WINDOW_MIN, PLATFORM_DETECTION_COARSE_STABLE_WINDOW_MAX, 0)){
               break;
            }
            PlatformDetectionCoarse.StableWindow = (byte)i;
            break;

         case MI_ABSOLUTE_RANGE :
            i = PlatformDetectionCoarse.AbsoluteRange;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformDetectionCoarse.AbsoluteRange = (TWeightGauge)i;
            break;

         case MI_SWITCHOVER_RANGE :
            i = PlatformDetectionCoarse.SwitchoverRange;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformDetectionCoarse.SwitchoverRange = (TWeightGauge)i;
            break;

      }
   }
} // MenuPlatformDetectionCoarse

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformDetectionCoarseParameters( int Index, int y, TPlatformDetectionCoarse *Parameters)
// Draw platform detection coarse parameters
{
   switch( Index){
      case MI_AVERAGING_WINDOW :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->AveragingWindow, 0);
         break;

      case MI_STABLE_WINDOW :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->StableWindow, 0);
         break;

      case MI_ABSOLUTE_RANGE :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->AbsoluteRange);
         break;

      case MI_SWITCHOVER_RANGE :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->SwitchoverRange);
         break;

   }
} // PlatformDetectionCoarseParameters
