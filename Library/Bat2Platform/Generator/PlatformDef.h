//******************************************************************************
//
//   PlatformDef.h  Bat2 weighing platform data
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __PlatformDef_H__
   #define __PlatformDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define PLATFORM_VERSION_WEIGHT_MAX_MIN 0
#define PLATFORM_VERSION_WEIGHT_MAX_MAX 0
#define PLATFORM_VERSION_WEIGHT_MAX_DEFAULT 500000

#define PLATFORM_VERSION_CALIBRATION_TIME_MIN 0
#define PLATFORM_VERSION_CALIBRATION_TIME_MAX 0
#define PLATFORM_VERSION_CALIBRATION_TIME_DEFAULT 0

#define PLATFORM_VERSION_SAMPLES_COUNT_MIN 0
#define PLATFORM_VERSION_SAMPLES_COUNT_MAX 0
#define PLATFORM_VERSION_SAMPLES_COUNT_DEFAULT 100

#define PLATFORM_COMMUNICATION_BAUD_RATE_MIN 1200
#define PLATFORM_COMMUNICATION_BAUD_RATE_MAX 115200
#define PLATFORM_COMMUNICATION_BAUD_RATE_DEFAULT 9600

#define PLATFORM_COMMUNICATION_BUS_ADDRESS_MIN 2
#define PLATFORM_COMMUNICATION_BUS_ADDRESS_MAX 99
#define PLATFORM_COMMUNICATION_BUS_ADDRESS_DEFAULT 2

#define PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_MIN 1
#define PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_MAX 500
#define PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_DEFAULT 30

#define PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_MIN 0
#define PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_MAX 500
#define PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_DEFAULT 10

#define PLATFORM_COMMUNICATION_STATUS_FRAME_COUNT_MIN 0
#define PLATFORM_COMMUNICATION_STATUS_FRAME_COUNT_MAX 10000
#define PLATFORM_COMMUNICATION_STATUS_FRAME_COUNT_DEFAULT 0

#define PLATFORM_COMMUNICATION_STATUS_WRONG_CHARACTER_COUNT_MIN 0
#define PLATFORM_COMMUNICATION_STATUS_WRONG_CHARACTER_COUNT_MAX 10000
#define PLATFORM_COMMUNICATION_STATUS_WRONG_CHARACTER_COUNT_DEFAULT 0

#define PLATFORM_COMMUNICATION_STATUS_WRONG_FRAME_COUNT_MIN 0
#define PLATFORM_COMMUNICATION_STATUS_WRONG_FRAME_COUNT_MAX 10000
#define PLATFORM_COMMUNICATION_STATUS_WRONG_FRAME_COUNT_DEFAULT 0

#define PLATFORM_COMMUNICATION_STATUS_OTHER_ERRORS_MIN 0
#define PLATFORM_COMMUNICATION_STATUS_OTHER_ERRORS_MAX 10000
#define PLATFORM_COMMUNICATION_STATUS_OTHER_ERRORS_DEFAULT 0

#define PLATFORM_STATUS_SAMPLES_COUNT_MIN 1
#define PLATFORM_STATUS_SAMPLES_COUNT_MAX 10000
#define PLATFORM_STATUS_SAMPLES_COUNT_DEFAULT 0

#define PLATFORM_STATUS__DUMMY2_MIN 0
#define PLATFORM_STATUS__DUMMY2_MAX 0
#define PLATFORM_STATUS__DUMMY2_DEFAULT 

#define PLATFORM_STATUS_WEIGHT_MIN 0
#define PLATFORM_STATUS_WEIGHT_MAX 0
#define PLATFORM_STATUS_WEIGHT_DEFAULT 0

#define PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_MIN 1
#define PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_MAX 16
#define PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_DEFAULT 3

#define PLATFORM_DIAGNOSTICS_BURST_COUNT_MIN 1
#define PLATFORM_DIAGNOSTICS_BURST_COUNT_MAX 100
#define PLATFORM_DIAGNOSTICS_BURST_COUNT_DEFAULT 10

#define PLATFORM_DIAGNOSTICS_AVERAGING_MIN 0
#define PLATFORM_DIAGNOSTICS_AVERAGING_MAX 100
#define PLATFORM_DIAGNOSTICS_AVERAGING_DEFAULT 1

#define PLATFORM_DIAGNOSTICS_FRAME_COUNT_MIN 1
#define PLATFORM_DIAGNOSTICS_FRAME_COUNT_MAX 100
#define PLATFORM_DIAGNOSTICS_FRAME_COUNT_DEFAULT 1

#define PLATFORM_PICOSTRAIN_RATE_MIN 1
#define PLATFORM_PICOSTRAIN_RATE_MAX 20
#define PLATFORM_PICOSTRAIN_RATE_DEFAULT 10

#define PLATFORM_PICOSTRAIN_PREFILTER_MIN 1
#define PLATFORM_PICOSTRAIN_PREFILTER_MAX 50
#define PLATFORM_PICOSTRAIN_PREFILTER_DEFAULT 1

#define PLATFORM_PICOSTRAIN_ACCURACY_FINE_MIN 1
#define PLATFORM_PICOSTRAIN_ACCURACY_FINE_MAX 100
#define PLATFORM_PICOSTRAIN_ACCURACY_FINE_DEFAULT 100

#define PLATFORM_PICOSTRAIN_ACCURACY_COARSE_MIN 1
#define PLATFORM_PICOSTRAIN_ACCURACY_COARSE_MAX 100
#define PLATFORM_PICOSTRAIN_ACCURACY_COARSE_DEFAULT 100

#define PLATFORM_SIGMA_DELTA_RATE_MIN 1
#define PLATFORM_SIGMA_DELTA_RATE_MAX 5000
#define PLATFORM_SIGMA_DELTA_RATE_DEFAULT 10

#define PLATFORM_SIGMA_DELTA_PREFILTER_MIN 1
#define PLATFORM_SIGMA_DELTA_PREFILTER_MAX 500
#define PLATFORM_SIGMA_DELTA_PREFILTER_DEFAULT 1

#define PLATFORM_DETECTION_FINE_AVERAGING_WINDOW_MIN 1
#define PLATFORM_DETECTION_FINE_AVERAGING_WINDOW_MAX 50
#define PLATFORM_DETECTION_FINE_AVERAGING_WINDOW_DEFAULT 10

#define PLATFORM_DETECTION_FINE_STABLE_WINDOW_MIN 1
#define PLATFORM_DETECTION_FINE_STABLE_WINDOW_MAX 50
#define PLATFORM_DETECTION_FINE_STABLE_WINDOW_DEFAULT 10

#define PLATFORM_DETECTION_FINE_ABSOLUTE_RANGE_MIN 0
#define PLATFORM_DETECTION_FINE_ABSOLUTE_RANGE_MAX 0
#define PLATFORM_DETECTION_FINE_ABSOLUTE_RANGE_DEFAULT 500

#define PLATFORM_DETECTION_FINE_SWITCHOVER_RANGE_MIN 0
#define PLATFORM_DETECTION_FINE_SWITCHOVER_RANGE_MAX 0
#define PLATFORM_DETECTION_FINE_SWITCHOVER_RANGE_DEFAULT 1000

#define PLATFORM_DETECTION_COARSE_AVERAGING_WINDOW_MIN 1
#define PLATFORM_DETECTION_COARSE_AVERAGING_WINDOW_MAX 50
#define PLATFORM_DETECTION_COARSE_AVERAGING_WINDOW_DEFAULT 3

#define PLATFORM_DETECTION_COARSE_STABLE_WINDOW_MIN 1
#define PLATFORM_DETECTION_COARSE_STABLE_WINDOW_MAX 50
#define PLATFORM_DETECTION_COARSE_STABLE_WINDOW_DEFAULT 3

#define PLATFORM_DETECTION_COARSE_ABSOLUTE_RANGE_MIN 0
#define PLATFORM_DETECTION_COARSE_ABSOLUTE_RANGE_MAX 0
#define PLATFORM_DETECTION_COARSE_ABSOLUTE_RANGE_DEFAULT 1000

#define PLATFORM_DETECTION_COARSE_SWITCHOVER_RANGE_MIN 0
#define PLATFORM_DETECTION_COARSE_SWITCHOVER_RANGE_MAX 0
#define PLATFORM_DETECTION_COARSE_SWITCHOVER_RANGE_DEFAULT 500

#define PLATFORM_ACCEPTANCE_LOW_LIMIT_MIN 0
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_MAX 0
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_DEFAULT 1000

#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_MIN 0
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_MAX 0
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_DEFAULT 1500

#define PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_MIN 0
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_MAX 0
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_DEFAULT 500

#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_MIN 0
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_MAX 0
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_DEFAULT 1000

#define PLATFORM_CALIBRATION_FULL_RANGE_MIN 0
#define PLATFORM_CALIBRATION_FULL_RANGE_MAX 0
#define PLATFORM_CALIBRATION_FULL_RANGE_DEFAULT 50000

#define PLATFORM_CALIBRATION_DELAY_MIN 1000
#define PLATFORM_CALIBRATION_DELAY_MAX 50000
#define PLATFORM_CALIBRATION_DELAY_DEFAULT 1000

#define PLATFORM_CALIBRATION_DURATION_MIN 1000
#define PLATFORM_CALIBRATION_DURATION_MAX 50000
#define PLATFORM_CALIBRATION_DURATION_DEFAULT 3000


//------------------------------------------------------------------------------
//  Picostrain filter
//------------------------------------------------------------------------------

typedef enum {
   PICOSTRAIN_FILTER_OFF,
   PICOSTRAIN_FILTER_SINC3,
   PICOSTRAIN_FILTER_SINC5,
   _PICOSTRAIN_FILTER_LAST
} EPicostrainFilter;

//------------------------------------------------------------------------------
//  Picostrain mode
//------------------------------------------------------------------------------

typedef enum {
   PICOSTRAIN_MODE_CONTINUOUS,
   PICOSTRAIN_MODE_STRETCHED,
   PICOSTRAIN_MODE_OVERLAPPED,
   _PICOSTRAIN_MODE_LAST
} EPicostrainMode;

//------------------------------------------------------------------------------
//  Sigma delta filter
//------------------------------------------------------------------------------

typedef enum {
   SIGMA_DELTA_FILTER_SINC3,
   SIGMA_DELTA_FILTER_SINC4,
   _SIGMA_DELTA_FILTER_LAST
} ESigmaDeltaFilter;

//------------------------------------------------------------------------------
//  Acceptance mode
//------------------------------------------------------------------------------

typedef enum {
   ACCEPTANCE_MODE_SINGLE,
   ACCEPTANCE_MODE_MIXED,
   ACCEPTANCE_MODE_STEP,
   _ACCEPTANCE_MODE_LAST
} EAcceptanceMode;

//------------------------------------------------------------------------------
//  Acceptance step
//------------------------------------------------------------------------------

typedef enum {
   ACCEPTANCE_STEP_ENTER,
   ACCEPTANCE_STEP_LEAVE,
   ACCEPTANCE_STEP_BOTH,
   _ACCEPTANCE_STEP_LAST
} EAcceptanceStep;

//------------------------------------------------------------------------------
//  Serial format
//------------------------------------------------------------------------------

typedef enum {
   SERIAL_FORMAT_8_BIT_NO_PARITY,
   SERIAL_FORMAT_8_BIT_EVEN,
   SERIAL_FORMAT_8_BIT_ODD,
   _SERIAL_FORMAT_LAST
} ESerialFormat;

//------------------------------------------------------------------------------
//  Platform operation
//------------------------------------------------------------------------------

typedef enum {
   PLATFORM_OPERATION_UNDEFINED,
   PLATFORM_OPERATION_STOP,
   PLATFORM_OPERATION_WEIGHING,
   PLATFORM_OPERATION_CALIBRATION,
   PLATFORM_OPERATION_DIAGNOSTICS,
   PLATFORM_OPERATION_SLEEP,
   PLATFORM_OPERATION_MATCHING,
   _PLATFORM_OPERATION_LAST
} EPlatformOperation;

//------------------------------------------------------------------------------
//  Platform error
//------------------------------------------------------------------------------

typedef enum {
   PLATFORM_ERROR_OK,
   PLATFORM_ERROR_BREAK,
   PLATFORM_ERROR_OVERRUN,
   PLATFORM_ERROR_CONFIGURATION,
   PLATFORM_ERROR_CALIBRATION,
   _PLATFORM_ERROR_LAST
} EPlatformError;

//------------------------------------------------------------------------------
//  Platform power
//------------------------------------------------------------------------------

typedef enum {
   PLATFORM_POWER_OK,
   PLATFORM_POWER_MAINS,
   PLATFORM_POWER_CHARGER,
   PLATFORM_POWER_LOW,
   PLATFORM_POWER_SHUTDOWN,
   _PLATFORM_POWER_LAST
} EPlatformPower;





//------------------------------------------------------------------------------
//  Platform version
//------------------------------------------------------------------------------

typedef struct {
   TDeviceVersion Device; // Device version description
   TWeightGauge WeightMax; // Maximum load
   UDateTimeGauge CalibrationTime; // Last calibration time
   word SamplesCount; // Maximu FIFO capacity
   byte SensorInversion; // Sensor inversion
   byte _Dummy;
} TPlatformVersion;

//------------------------------------------------------------------------------
//  Platform communication
//------------------------------------------------------------------------------

typedef struct {
   dword BaudRate; // Serial communication Baud rate
   byte BusAddress; // RS485 bus address
   byte Format; // Serial communication format
   word IntercharacterTimeout; // Intercharacter timeout
   word TransmitterDelay; // Transmitter delay
   word _Dummy;
} TPlatformCommunication;

//------------------------------------------------------------------------------
//  Platform communication status
//------------------------------------------------------------------------------

typedef struct {
   word FrameCount; // Number of received frames
   word WrongCharacterCount; // Number of damaged characters (parity, framing)
   word WrongFrameCount; // Number of unacceptable frames
   word OtherErrors; // Other errors (eg. character overrun)
} TPlatformCommunicationStatus;

//------------------------------------------------------------------------------
//  Platform status
//------------------------------------------------------------------------------

typedef struct {
   byte Operation; // Operation mode
   byte Error; // Last error status
   byte Power; // Power status
   byte _Dummy1;
   word SamplesCount; // Actual samples count in FIFO
   word _Dummy2;
   TWeightGauge Weight; // Actual platform load
} TPlatformStatus;

//------------------------------------------------------------------------------
//  Platform diagnostics
//------------------------------------------------------------------------------

typedef struct {
   byte SampleSize; // Sample size
   byte BurstCount; // Maximum samples count per frame
   byte Averaging; // ADC averaging
   byte FrameCount; // Number of frames per request
} TPlatformDiagnostics;

//------------------------------------------------------------------------------
//  Platform picostrain
//------------------------------------------------------------------------------

typedef struct {
   word Rate; // Sampling rate [Hz]
   word Prefilter; // Averaging filter
   byte Filter; // OFF/SINC3/SINC5
   byte Mode; // Working mode
   byte AccuracySwitch; // Accuracy switching enabled/disabled
   byte _Dummy1;
   byte AccuracyFine; // Internal accuracy fine
   byte AccuracyCoarse; // Internal accuracy coarse
   word _Dummy2;
} TPlatformPicostrain;

//------------------------------------------------------------------------------
//  Platform sigma delta
//------------------------------------------------------------------------------

typedef struct {
   word Rate; // Sampling rate [Hz]
   word Prefilter; // Averaging filter
   byte Filter; // Sigma delta fitering
   byte Chop; // Chopped mode
   word _Dummy;
} TPlatformSigmaDelta;

//------------------------------------------------------------------------------
//  Platform detection
//------------------------------------------------------------------------------

typedef struct {
   TDetectionData Fine; // Fine detection data
   TDetectionData Coarse; // Coarse detection data
} TPlatformDetection;

//------------------------------------------------------------------------------
//  Platform detection fine
//------------------------------------------------------------------------------

typedef struct {
   byte AveragingWindow; // Samples count for floating average
   byte StableWindow; // Samples count for stable value
   byte _Dummy;
   TWeightGauge AbsoluteRange; // Absolute stabilization range
   TWeightGauge SwitchoverRange; // Switchover-to-Coarse treshold range
} TPlatformDetectionFine;

//------------------------------------------------------------------------------
//  Platform detection coarse
//------------------------------------------------------------------------------

typedef struct {
   byte AveragingWindow; // Samples count for floating average
   byte StableWindow; // Samples count for stable value
   byte _Dummy;
   TWeightGauge AbsoluteRange; // Absolute stabilization range
   TWeightGauge SwitchoverRange; // Switchover-to-Fine treshold range
} TPlatformDetectionCoarse;

//------------------------------------------------------------------------------
//  Platform acceptance
//------------------------------------------------------------------------------

typedef struct {
   byte Mode; // Acceptance mode
   byte Sex; // Sex for Single Sex Mode
   byte Step; // Acceptance step
   byte _Dummy;
   TWeightGauge LowLimit; // Mixed mode Low Limit Male
   TWeightGauge HighLimit; // Mixed mode High Limit Male
   TWeightGauge LowLimitFemale; // Mixed mode Low Limit Female
   TWeightGauge HighLimitFemale; // Mixed mode High Limit Female
} TPlatformAcceptance;

//------------------------------------------------------------------------------
//  Platform calibration
//------------------------------------------------------------------------------

typedef struct {
   TWeightGauge FullRange; // Full range weight
   word Delay; // Idle delay after loading
   word Duration; // Weighing duration
} TPlatformCalibration;



//------------------------------------------------------------------------------
#endif
