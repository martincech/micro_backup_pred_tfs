//******************************************************************************
//
//   MenuPlatformPicostrain.h  Platform picostrain menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformPicostrain_H__
   #define __MenuPlatformPicostrain_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformPicostrain( void);
// Menu platform picostrain

#endif
