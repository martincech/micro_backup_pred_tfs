//******************************************************************************
//
//   Calibration.h  Calibration utility
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Calibration_H__
   #define __Calibration_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __CalibrationDef_H__
   #include "CalibrationDef.h"
#endif

extern TCalibration Calibration;
extern const TCalibration CalibrationDefault;

#ifdef OPTION_SIGMA_DELTA
   #include "Calibration/Sigma/Calibration.h"
#else
   #include "Calibration/Pico/Calibration.h"
#endif
   
#endif
