//******************************************************************************
//
//   CalibrationDef.h  SigmaDelta calibration definition
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __uDateTime_H__
   #include "Time/uDateTime.h"
#endif

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef word TCalibrationCrc;          // calibration checksum

typedef struct {
   UDateTimeGauge  Time;               // Last calibration time
   byte            Points;
   TRawWeight      Raw[CALIBRATION_POINTS_MAX];             // range calibration
   TWeightGauge    Weight[CALIBRATION_POINTS_MAX];              // physical range
   byte            Inversion;          // reversed bridge polarity
   byte            _Spare;
   TCalibrationCrc Crc;                // record checksum
} TCalibration;
