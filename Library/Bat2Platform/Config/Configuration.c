//******************************************************************************
//
//   Configuration.c  Configuration utilities
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#include "Configuration.h"
#include "Eeprom/Eeprom.h"
#include "Config/EepromLayout.h"    // EEPROM layout
#include "Platform/Platform.h"

#define CONFIG_ADDRESS        offsetof( TEepromLayout, Configuration)

#define CONFIG_VERSION       (CONFIG_ADDRESS)
#define CONFIG_COMMUNICATION (CONFIG_ADDRESS + offsetof( TConfiguration, Communication))
#define CONFIG_CRC           (CONFIG_ADDRESS + offsetof( TConfiguration, Crc))
#define CONFIG_DATA_SIZE     (word)offsetof( TConfiguration, Crc)

// Local functions :

static TYesNo ConfigurationCheck( void);
// Check for configuration validity

static TConfigurationCrc CalculateCrc( void);
// Calculate CRC

//------------------------------------------------------------------------------
// Load
//------------------------------------------------------------------------------

TYesNo ConfigLoad( void)
// Load configuration from EEPROM
{
   if( !ConfigurationCheck()){
      // set defaults
      uConstCopy( &PlatformCommunication, &PlatformCommunicationDefault, sizeof( TPlatformCommunication));
      ConfigSave();
      return( NO);
   }
   EepromRead( CONFIG_COMMUNICATION, &PlatformCommunication, sizeof( TPlatformCommunication));
   return( YES);
} // ConfigurationLoad

//------------------------------------------------------------------------------
// Save
//------------------------------------------------------------------------------

void ConfigSave( void)
// Save configuration to EEPROM
{
TConfigurationCrc Crc;
   EepromWordWrite( CONFIG_VERSION, CONFIGURATION_VERSION);
   EepromWrite( CONFIG_COMMUNICATION, &PlatformCommunication, sizeof( TPlatformCommunication));
   Crc = CalculateCrc();
   EepromWordWrite( CONFIG_CRC, Crc);
   EepromCommit();
} // ConfigSave

#include "Calibration/Calibration.h"

void ConfigCalibrationSave( void)
// Save calibration
{
}

void ConfigCalibrationLoad( void)
// LoadCalibration
{
}

//******************************************************************************

//------------------------------------------------------------------------------
// Check
//------------------------------------------------------------------------------

static TYesNo ConfigurationCheck( void)
// Check for configuration validity
{
TConfigurationCrc Crc;

   Crc = CalculateCrc();
   if( EepromWordRead( CONFIG_CRC) != Crc){
      return( NO);
   }
   if( EepromWordRead( CONFIG_VERSION) != CONFIGURATION_VERSION){
      return( NO);
   }
   return( YES);
} // ConfigurationCheck

//------------------------------------------------------------------------------
// Calculate CRC
//------------------------------------------------------------------------------

static TConfigurationCrc CalculateCrc( void)
// Calculate CRC
{
TConfigurationCrc Crc;
word              Size;
word              Address;

   Crc     = 0;
   Size    = CONFIG_DATA_SIZE;
   Address = CONFIG_ADDRESS;
   do {
      Crc += EepromByteRead( Address);
      Address++;
   } while( --Size);
   return( ~Crc);
} // CalculateCrc
