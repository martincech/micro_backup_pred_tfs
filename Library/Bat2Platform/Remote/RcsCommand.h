//*****************************************************************************
//
//    RcsCommand.h  Remote control command processing
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __RcsCommand_H__
   #define __RcsCommand_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif   

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
//  Command
//------------------------------------------------------------------------------

void RcsCommandInit( void);
// Initialize

TYesNo RcsCommand( byte Address, void *Command, word CommandSize, void *Reply, word *ReplySize);
// Execute received command

void RcsAddressAssign( byte Address);
// Communication address assignment

//------------------------------------------------------------------------------

#ifdef __cplusplus
   }
#endif
  
#endif
