//*****************************************************************************
//
//    PlatformCommand.c  Remote control command processing
//    Version 1.0        (c) VEIT Electronics
//
//*****************************************************************************

#include "PlatformCommand.h"
#include "Platform/PlatformRpc.h"      // Platform remote calls
#include "Platform/Wepl.h"             // Weighing platform utility

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void PlatformCommandInit( void)
// Initialize
{
   WeplInit();
} // PlatformCommandInit

//-----------------------------------------------------------------------------
// Command
//-----------------------------------------------------------------------------

TYesNo PlatformCommand( TPlatformCommand *Command, word CommandSize, TPlatformReply *Reply, word *ReplySize)
// Execute received command
{
word Count;

   *ReplySize = PlatformSimpleReplySize();       // simple reply
   switch( Command->Command){
      case PLATFORM_CMD_VERSION_GET :
         if( CommandSize != PlatformSimpleCommandSize()){
            return( NO);
         }
         WeplVersionGet( &Reply->Data.Version);
         *ReplySize = PlatformReplySize( Version);
         break;

      case PLATFORM_CMD_STATUS_GET :
         if( CommandSize != PlatformSimpleCommandSize()){
            return( NO);
         }
         WeplStatusGet( &Reply->Data.Status);
         *ReplySize = PlatformReplySize( Status);
         break;

      case PLATFORM_CMD_CLOCK_SET :
         if( CommandSize != PlatformCommandSize( Clock)){
            return( NO);
         }
         WeplClockSet( Command->Data.Clock);
         break;

#ifdef OPTION_SIGMA_DELTA
      case PLATFORM_CMD_SIGMA_DELTA_SET :
         if( CommandSize != PlatformCommandSize( SigmaDelta)){
            return( NO);
         }
         WeplSigmaDeltaSet( &Command->Data.SigmaDelta);
         break;
#endif // OPTION_SIGMA_DELTA

#ifdef OPTION_PICOSTRAIN
      case PLATFORM_CMD_PICOSTRAIN_SET :
         if( CommandSize != PlatformCommandSize( Picostrain)){
            return( NO);
         }
         WeplPicostrainSet( &Command->Data.Picostrain);
         break;
#endif // OPTION_PICOSTRAIN

      case PLATFORM_CMD_DETECTION_SET :
         if( CommandSize != PlatformCommandSize( Detection)){
            return( NO);
         }
         WeplDetectionSet( &Command->Data.Detection);
         break;

      case PLATFORM_CMD_ACCEPTANCE_SET :
         if( CommandSize != PlatformCommandSize( Acceptance)){
            return( NO);
         }
         WeplAcceptanceSet( &Command->Data.Acceptance);
         break;

      case PLATFORM_CMD_CALIBRATION_SET :
         if( CommandSize != PlatformCommandSize( Calibration)){
            return( NO);
         }
         WeplCalibrationSet( &Command->Data.Calibration);
         break;

      case PLATFORM_CMD_WEIGHING_START :
         if( CommandSize != PlatformSimpleCommandSize()){
            return( NO);
         }
         WeplWeighingStart();
         break;

      case PLATFORM_CMD_DIAGNOSTIC_START :
         if( CommandSize != PlatformSimpleCommandSize()){
            return( NO);
         }
         WeplDiagnosticStart();
         break;

      case PLATFORM_CMD_DIAGNOSTIC_WEIGHT_GET :
         if( CommandSize != PlatformCommandSize(DiagnosticWeightGet)){
            return( NO);
         }
         WeplDiagnosticWeightGet(Command->Data.Diagnostic.Id, &Reply->Data.Diagnostic.Id, &Reply->Data.Diagnostic.Frame);
         *ReplySize = PlatformDiagnosticWeightReplySize( &Reply->Data.Diagnostic.Frame);
         break;

      case PLATFORM_CMD_STOP :
         if( CommandSize != PlatformSimpleCommandSize()){
            return( NO);
         }
         WeplStop();
         break;

      case PLATFORM_CMD_SAVED_WEIGHT_GET :
         if( CommandSize != PlatformSimpleCommandSize()){
            return( NO);
         }
         WeplSavedWeightGet( &Count, Reply->Data.SavedWeight.Weight);
         Reply->Data.SavedWeight.Count = Count;
         *ReplySize = PlatformSavedWeightReplySize( Count);
         break;

      case PLATFORM_CMD_SAVED_WEIGHT_CONFIRM :
         if( CommandSize != PlatformCommandSize( SavedWeightConfirm)){
            return( NO);
         }
         WeplSavedWeightConfirm( Command->Data.Confirm.Count);
         break;

      case PLATFORM_CMD_COMMUNICATION_SET :
         if( CommandSize != PlatformCommandSize( Communication)){
            return( NO);
         }
         WeplCommunicationSet( &Command->Data.Communication);
         break;

      case PLATFORM_CMD_COMMUNICATION_STATUS_GET :
         if( CommandSize != PlatformSimpleCommandSize()){
            return( NO);
         }
         WeplCommunicationStatusGet( &Reply->Data.CommunicationStatus);
         *ReplySize = PlatformReplySize( CommunicationStatus);
         break;

      default :
         return( NO);
   }
   Reply->Reply = PlatformReply( Command->Command);
   return( YES);
} // PlatformCommand