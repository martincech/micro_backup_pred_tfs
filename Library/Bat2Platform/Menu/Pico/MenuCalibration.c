//*****************************************************************************
//
//   MenuCalibration.c  Calibration menu
//   Version 1.0        (c) VEIT Electronics
//
//*****************************************************************************

#include "Menu/MenuCalibration.h"
#include "System/System.h"
#include "Platform/Platform.h"         // Platform data
#include "Device/Status.h"             // Status display
#include "Weighing/Weighing.h"         // Weighing operations
#include "AdcPs081/AdcPs081.h"         // Adc

//-----------------------------------------------------------------------------
// Calibration
//-----------------------------------------------------------------------------

void MenuCalibration( void)
// Calibration menu
{
TYesNo     GetZero;
TYesNo     Restart;

   StatusDisplay( STATUS_DISPLAY_ZERO);          // wait for release
   GetZero  = YES;
   Restart  = NO;

   forever {
      switch( SysEventWait()){
         case K_MODE :
            if(AdcCalibrationInProgress()) {
                AdcCalibrationCancel();
            }
            StatusDisplay( STATUS_DISPLAY_CALIBRATION_STOP);
            StatusOperationSet( PLATFORM_OPERATION_STOP);
            return;

         case K_OK :
            // check for calibration restart :
            if( Restart){
               Restart = NO;
               GetZero = YES;                                // get zero again
               StatusDisplay( STATUS_DISPLAY_ZERO);          // wait for release
               break;
            }
            if( GetZero){
               AdcRateSet(PlatformPicostrain.Rate); // Adc config
               // zero weighing
               StatusDisplay( STATUS_DISPLAY_ZERO_WEIGHING);
               AdcCalibrationStart(PlatformCalibration.FullRange, (PlatformCalibration.Duration * PlatformPicostrain.Rate) / 1000);
            } else {
               // load weighing
               StatusDisplay( STATUS_DISPLAY_LOAD_WEIGHING);
               AdcCalibrationContinue();
            }
            break;

         case K_ILLUMINATION :
            break;

         case K_TIMER_SLOW :
            if( PlatformOperation() != PLATFORM_OPERATION_CALIBRATION){
               if(AdcCalibrationInProgress()) {
                   AdcCalibrationCancel();
               }	
               StatusDisplay( STATUS_DISPLAY_CALIBRATION_STOP);
               return;                                   // stopped by external command
            }
         
            if(AdcCalibrationWaiting()) {
               StatusDisplay( STATUS_DISPLAY_LOAD);
               GetZero = NO;
               break;
            }            
         
            if(AdcCalibrationInProgress()) {
               break;
            }
         
            if(Restart || GetZero) {
               break;
            }
         
            // calibration ended
            StatusDisplay( STATUS_DISPLAY_CALIBRATION_OK);
            PlatformErrorSet( PLATFORM_ERROR_OK);     // clear calibration error
            Restart = YES;
            break;

         default :
            break;
      }
   } // forever
} // MenuCalibration