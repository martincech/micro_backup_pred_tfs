//******************************************************************************
//
//   pDiagnosticFifo.h     DiagnosticSamples FIFO
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#ifndef __pDiagnosticFifo_H__
   #define __pDiagnosticFifo_H__

#ifndef __DiagnosticDef_H__
   #include "Diagnostic/DiagnosticDef.h"
#endif

typedef word TDiagnosticFifoCount;

typedef struct {
   dword             Timestamp;
   TDiagnosticWeight Weight;
} TDiagnosticFifoSample;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void pDiagnosticFifoInit( void);
// Initialize

void pDiagnosticFifoPut( TDiagnosticFifoSample Weight);
// Save <Weight>

TDiagnosticFifoCount pDiagnosticFifoCount( void);
// Returns samples count

TDiagnosticFifoCount pDiagnosticFifoOverrun( void);
// Returns lost samples

void pDiagnosticFifoMark( TDiagnosticFifoCount Count);
// Mark <Count> items for read

TYesNo pDiagnosticFifoGet( TDiagnosticFifoCount Index, TDiagnosticFifoSample *Weight);
// Returns <Weight> at <Index>

TYesNo pDiagnosticFifoRemove( TDiagnosticFifoCount Count);
// Remove <Count> items

//------------------------------------------------------------------------------

#endif
