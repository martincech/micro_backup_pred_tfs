//******************************************************************************
//
//   pDiagnostic.c   Platform diagnostic
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "pDiagnostic.h"
#include "pDiagnosticFifo.h"
#include "Ad7192/Ad7192f.h"
#include "Calibration/Calibration.h"
#include "System/System.h"

// Diagnostic internals
static word _BurstCount = 10;
static dword LastId;
static word SamplesCount = 0;

//-----------------------------------------------------------------------------
//  Start
//-----------------------------------------------------------------------------

void pDiagnosticStart( void)
{
   LastId = -1;
   SamplesCount = 0;
   pDiagnosticFifoInit();
} // DiagnosticStart

//-----------------------------------------------------------------------------
//  Stop
//-----------------------------------------------------------------------------

void pDiagnosticStop( void)
{
   AdcCallbackEnable(NO);
   AdcStop();
} // DiagnosticStop

//-----------------------------------------------------------------------------
//  Set Burst Count
//-----------------------------------------------------------------------------

void pDiagnosticSetBurstCount(byte BurstCount)
// Sets <BurstCount> samples per one frame
{
   if(BurstCount > DIAGNOSTIC_BURST_MAX) {
      BurstCount = DIAGNOSTIC_BURST_MAX;
   }
   
   _BurstCount = BurstCount;
} // DiagnosticSetBurstCount

//-----------------------------------------------------------------------------
//  Frame get
//-----------------------------------------------------------------------------

TYesNo pDiagnosticFrameGet( dword IdIn, dword *IdOut, TDiagnosticFrame *Frame)
// Get frame
{
word i;
TDiagnosticFifoSample FifoSample;
   if( IdIn == LastId) {
      pDiagnosticFifoRemove(SamplesCount);
   }

   Frame->LostSamples = pDiagnosticFifoOverrun();

   // get FIFO samples :
   SamplesCount = pDiagnosticFifoCount();         // get total samples count
   if( SamplesCount > DIAGNOSTIC_BURST_MAX){
      SamplesCount = DIAGNOSTIC_BURST_MAX;
   }
   pDiagnosticFifoMark( 0);                       // mark zero items for failure
   Frame->Count = 0;                         // empty list for failure
   for( i = 0; i < SamplesCount; i++){
      if( !pDiagnosticFifoGet( i, &FifoSample)){
         return( NO);                  // FIFO read failure
      }
      if(i == 0) {
         Frame->Timestamp = FifoSample.Timestamp;
      }
      Frame->Weight[i] = FifoSample.Weight;
   }
   Frame->Count = (byte)SamplesCount;        // items count
   pDiagnosticFifoMark( SamplesCount);            // mark items for read
   LastId = IdIn + 1;
   *IdOut = LastId;
   return( YES);
} // pDiagnosticFrameGet

//-----------------------------------------------------------------------------
//  Adc callback
//-----------------------------------------------------------------------------

void AdcCallback( void) {
   TDiagnosticFifoSample Sample;
   TWeightGauge Weight = CalibrationWeight( AdcRawRead());
   DiagnosticWeightCompose(&Sample.Weight, Weight);
   Sample.Timestamp = SysClock();

   pDiagnosticFifoPut( Sample);
} // AdcCallback