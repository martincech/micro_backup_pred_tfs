//******************************************************************************
//
//   Acceptance.c Weighing acceptance
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Acceptance.h"
#include "Platform/PlatformDef.h"

extern TPlatformAcceptance PlatformAcceptance;

//------------------------------------------------------------------------------
// Accept
//------------------------------------------------------------------------------

TYesNo AcceptWeight( TWeightGauge *ActualWeight, byte *WeightFlags)
// <Weight> acceptance, returns <WeightFlags>
{
byte         Flags;
TWeightGauge Weight;

   Flags  = WEIGHT_FLAG_NORMAL;
   Weight = *ActualWeight;
   // check for step mode :
   if( Weight >= 0){
      // step up
      if( PlatformAcceptance.Step == PLATFORM_STEP_LEAVE){
         return( NO);
      }
   } else {
      // step down
      if( PlatformAcceptance.Step == PLATFORM_STEP_ENTER){
         return( NO);
      }
      Weight = -Weight;
      Flags |= WEIGHT_FLAG_STEP_DOWN;
   }
   switch( PlatformAcceptance.Mode){
      case PLATFORM_ACCEPTANCE_MODE_STEP :
         break;

      case PLATFORM_ACCEPTANCE_MODE_SINGLE :
         // single fixed sex :
         if( PlatformAcceptance.Sex == SEX_MALE){
            Flags |= WEIGHT_FLAG_MALE;
         }			
         if( PlatformAcceptance.Sex == SEX_FEMALE){
            Flags |= WEIGHT_FLAG_FEMALE;
         }
         if( Weight > PlatformAcceptance.HighLimit){
            return( NO);
         }
         if( Weight < PlatformAcceptance.LowLimit){
            return( NO);
         }
         break;

      case PLATFORM_ACCEPTANCE_MODE_MIXED :
         // check for male gender :
         if( Weight > PlatformAcceptance.HighLimit){
            return( NO);
         }
         if( Weight >= PlatformAcceptance.LowLimit){
			   Flags |= WEIGHT_FLAG_MALE;
            break;                     // accept male gender
         }
         // check for female gender :
         if( Weight > PlatformAcceptance.HighLimitFemale){
            return( NO);
         }
         if( Weight < PlatformAcceptance.LowLimitFemale){
            return( NO);
         }
         Flags |= WEIGHT_FLAG_FEMALE;  // accept female gender
         break;
   }
   *ActualWeight = Weight;             // update weight
   *WeightFlags  = Flags;              // update flags
   return( YES);
} // AcceptWeight
