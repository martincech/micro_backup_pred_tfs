//******************************************************************************
//
//   PlatformDef.h  Platform data definitions
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __PlatformDef_H__
   #ifndef _MANAGED
      #define __PlatformDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __VersionDef_H__
   #include "Device/VersionDef.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __SexDef_H__
   #include "Weight/SexDef.h"
#endif

#ifndef __WeightFlagDef_H__
   #include "Weight/WeightFlagDef.h"
#endif

#ifndef __UartDef_H__
   #include "Uart/UartDef.h"
#endif

#ifndef __uDateTime_H__
   #include "Time/uDateTime.h"
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------
#ifndef _MANAGED
#define PLATFORM_COMMUNICATION_BAUD_RATE_MIN     1200
#define PLATFORM_COMMUNICATION_BAUD_RATE_MAX     115200
#define PLATFORM_COMMUNICATION_BAUD_RATE_DEFAULT 9600

#define PLATFORM_COMMUNICATION_FORMAT_DEFAULT    UART_8BIT

#define PLATFORM_COMMUNICATION_BUS_ADDRESS_MIN     2
#define PLATFORM_COMMUNICATION_BUS_ADDRESS_MAX     99
#define PLATFORM_COMMUNICATION_BUS_ADDRESS_DEFAULT 2

#define PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_MIN     1
#define PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_MAX     500
#define PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_DEFAULT 30

#define PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_MIN     0
#define PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_MAX     500
#define PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_DEFAULT 10

#define PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_MIN     1
#define PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_MAX     16
#define PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_DEFAULT 3

#define PLATFORM_DIAGNOSTICS_BURST_COUNT_MIN     1
#define PLATFORM_DIAGNOSTICS_BURST_COUNT_MAX     100
#define PLATFORM_DIAGNOSTICS_BURST_COUNT_DEFAULT 10

#define PLATFORM_DIAGNOSTICS_AVERAGING_MIN     0
#define PLATFORM_DIAGNOSTICS_AVERAGING_MAX     100
#define PLATFORM_DIAGNOSTICS_AVERAGING_DEFAULT 1

#define PLATFORM_DIAGNOSTICS_FRAME_COUNT_MIN     1
#define PLATFORM_DIAGNOSTICS_FRAME_COUNT_MAX     100
#define PLATFORM_DIAGNOSTICS_FRAME_COUNT_DEFAULT 1

#define PLATFORM_PICOSTRAIN_RATE_MIN     1
#define PLATFORM_PICOSTRAIN_RATE_MAX     20
#define PLATFORM_PICOSTRAIN_RATE_DEFAULT 10

#define PLATFORM_PICOSTRAIN_PREFILTER_MIN     1
#define PLATFORM_PICOSTRAIN_PREFILTER_MAX     50
#define PLATFORM_PICOSTRAIN_PREFILTER_DEFAULT 1

#define PLATFORM_PICOSTRAIN_FILTER_DEFAULT          PLATFORM_PICOSTRAIN_FILTER_SINC5
#define PLATFORM_PICOSTRAIN_MODE_DEFAULT            PLATFORM_PICOSTRAIN_MODE_CONTINUOUS
#define PLATFORM_PICOSTRAIN_ACCURACY_SWITCH_DEFAULT NO

#define PLATFORM_PICOSTRAIN_ACCURACY_FINE_MIN       1
#define PLATFORM_PICOSTRAIN_ACCURACY_FINE_MAX       100
#define PLATFORM_PICOSTRAIN_ACCURACY_FINE_DEFAULT   100

#define PLATFORM_PICOSTRAIN_ACCURACY_COARSE_MIN     1
#define PLATFORM_PICOSTRAIN_ACCURACY_COARSE_MAX     100
#define PLATFORM_PICOSTRAIN_ACCURACY_COARSE_DEFAULT 100

#define PLATFORM_SIGMA_DELTA_RATE_MIN     1
#define PLATFORM_SIGMA_DELTA_RATE_MAX     5000
#define PLATFORM_SIGMA_DELTA_RATE_DEFAULT 10

#define PLATFORM_SIGMA_DELTA_PREFILTER_MIN     1
#define PLATFORM_SIGMA_DELTA_PREFILTER_MAX     500
#define PLATFORM_SIGMA_DELTA_PREFILTER_DEFAULT 1

#define PLATFORM_DETECTION_AVERAGING_WINDOW_MIN 1
#define PLATFORM_DETECTION_AVERAGING_WINDOW_MAX 50
#define PLATFORM_DETECTION_STABLE_WINDOW_MIN    1
#define PLATFORM_DETECTION_STABLE_WINDOW_MAX    50

#define PLATFORM_DETECTION_FINE_AVERAGING_WINDOW_DEFAULT   10
#define PLATFORM_DETECTION_FINE_STABLE_WINDOW_DEFAULT      10
#define PLATFORM_DETECTION_FINE_ABSOLUTE_RANGE_DEFAULT     500
#define PLATFORM_DETECTION_FINE_SWITCHOVER_RANGE_DEFAULT   1000

#define PLATFORM_DETECTION_COARSE_AVERAGING_WINDOW_DEFAULT 3
#define PLATFORM_DETECTION_COARSE_STABLE_WINDOW_DEFAULT    3
#define PLATFORM_DETECTION_COARSE_ABSOLUTE_RANGE_DEFAULT   1000
#define PLATFORM_DETECTION_COARSE_SWITCHOVER_RANGE_DEFAULT 500

#define PLATFORM_ACCEPTANCE_MODE_DEFAULT              PLATFORM_ACCEPTANCE_MODE_STEP
#define PLATFORM_ACCEPTANCE_SEX_DEFAULT               SEX_UNDEFINED
#define PLATFORM_ACCEPTANCE_STEP_DEFAULT              PLATFORM_STEP_BOTH
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_DEFAULT         1000
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_DEFAULT        1500
#define PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_DEFAULT  500
#define PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_DEFAULT 1000

#define PLATFORM_CALIBRATION_POINTS_MIN       2
#define PLATFORM_CALIBRATION_POINTS_MAX       5
#define PLATFORM_CALIBRATION_POINTS_DEFAULT   2

#define PLATFORM_CALIBRATION_DELAY_MIN          1000
#define PLATFORM_CALIBRATION_DELAY_MAX          50000
#define PLATFORM_CALIBRATION_DELAY_DEFAULT      1000

#define PLATFORM_CALIBRATION_DURATION_MIN       1000
#define PLATFORM_CALIBRATION_DURATION_MAX       50000
#define PLATFORM_CALIBRATION_DURATION_DEFAULT   10000

//------------------------------------------------------------------------------

#define PLATFORM_DETECTION_ABSOLUTE_RANGE_MIN   20   // stabilization range [0.1g]

//------------------------------------------------------------------------------
//  Version
//------------------------------------------------------------------------------

typedef struct {
   TDeviceVersion Device;              // standard device record
   TWeightGauge   WeightMax;           // maximum weight
   UDateTimeGauge CalibrationTime;     // Last calibration time
   word           SamplesCount;        // FIFO size
   byte           SensorInversion;     // sensor polarity inversion
   byte           _dummy;
} TPlatformVersion;

//------------------------------------------------------------------------------
//  Communication
//------------------------------------------------------------------------------

typedef struct {
   dword BaudRate;                     // baud rate [Bd]
   byte  BusAddress;                   // RS485 device address
   byte  Format;                       // serial format eg. 8-n-1
   word  IntercharacterTimeout;        // intercharacter timeout [ms]
   word  TransmitterDelay;             // transmitter reply delay
   word  _dummy;
} TPlatformCommunication;

//------------------------------------------------------------------------------
//  Communication status
//------------------------------------------------------------------------------

typedef struct {
   word FrameCount;                    // number of received frames
   word WrongCharacterCount;           // number of damaged characters (parity,framing)
   word WrongFrameCount;               // number of unacceptable frames
   word OtherErrors;                   // other errors (eg. character overrun)
} TPlatformCommunicationStatus;

//------------------------------------------------------------------------------
//  Device Status
//------------------------------------------------------------------------------

// platform operation mode :
typedef enum {
   PLATFORM_OPERATION_UNDEFINED,       // unknown operation (line break)
   PLATFORM_OPERATION_STOP,            // operation stopped
   PLATFORM_OPERATION_WEIGHING,        // weighing active
   PLATFORM_OPERATION_CALIBRATION,     // calibration in progress
   PLATFORM_OPERATION_DIAGNOSTICS,     // ADC diagnostics
   PLATFORM_OPERATION_SLEEP,           // low power sleep
   PLATFORM_OPERATION_MATCHING,        // platform communication matching
   _PLATFORM_OPERATION_LAST
} EPlatformOperation;

// platform error codes :
typedef enum {
   PLATFORM_ERROR_OK,                  // operation OK
   PLATFORM_ERROR_BREAK,               // communication break
   PLATFORM_ERROR_OVERRUN,             // data overrun (FIFO overflow)
   PLATFORM_ERROR_CONFIGURATION,       // configuration record corrupted
   PLATFORM_ERROR_CALIBRATION,         // calibration record corrupted
   _PLATFORM_ERROR_LAST
} EPlatformError;

// platform power status :
typedef enum {
   PLATFORM_POWER_OK,                  // accumulator OK
   PLATFORM_POWER_MAINS,               // external power
   PLATFORM_POWER_CHARGER,             // charger connected
   PLATFORM_POWER_LOW,                 // accumulator Low
   PLATFORM_POWER_SHUTDOWN,            // accumulator Discharged
   _PLATFORM_POWER_LAST
} EPlatformPower;

#define PLATFORM_INTENSITY_MAX 0xFF    // LED illumination intensity 0..MAX

typedef struct {
   byte         Operation;             // operation mode
   byte         Error;                 // last error status
   byte         Power;                 // power status
   byte         _dummy1;
   word         SamplesCount;          // actual samples count in FIFO
   word         _dummy2;
   TWeightGauge Weight;                // actual platform load
} TPlatformStatus;

//------------------------------------------------------------------------------
//  Clock
//------------------------------------------------------------------------------

typedef dword TPlatformClock;

//------------------------------------------------------------------------------
//  Diagnostics
//------------------------------------------------------------------------------

typedef struct {
   byte SampleSize;                         // sample size
   byte BurstCount;                         // maximum samples count per frame
   byte Averaging;                          // ADC averaging
   byte FrameCount;                         // number of frames per request
} TPlatformDiagnostics;

//------------------------------------------------------------------------------
//  Picostrain
//------------------------------------------------------------------------------

typedef enum {
   PLATFORM_PICOSTRAIN_FILTER_OFF,
   PLATFORM_PICOSTRAIN_FILTER_SINC3,
   PLATFORM_PICOSTRAIN_FILTER_SINC5,
   _PLATFORM_PICOSTRAIN_FILTER_LAST
} EPlatformPicostrainFilter;

typedef enum {
   PLATFORM_PICOSTRAIN_MODE_CONTINUOUS,
   PLATFORM_PICOSTRAIN_MODE_STRETCHED,
   PLATFORM_PICOSTRAIN_MODE_OVERLAPPED,
   _PLATFORM_PICOSTRAIN_MODE_LAST
} EPlatformPicostrainMode;

typedef struct {
   word Rate;                               // Sampling rate [Hz]
   word Prefilter;                          // Samples count for averaging
   byte Filter;                             // SINCx fitering
   byte Mode;                               // Working mode
   byte AccuracySwitch;                     // Accuracy switching enabled/disabled
   byte _Dummy1;
   byte AccuracyFine;                       // Internal accuracy fine
   byte AccuracyCoarse;                     // Internal accuracy coarse
   word _Dummy2;
} TPlatformPicostrain;

//------------------------------------------------------------------------------
//  Sigma Delta
//------------------------------------------------------------------------------

typedef enum {
   PLATFORM_SIGMA_DELTA_FILTER_SINC3,
   PLATFORM_SIGMA_DELTA_FILTER_SINC4,
   _PLATFORM_SIGMA_DELTA_FILTER_LAST
} EPlatformSigmaDeltaFilter;

typedef struct {
   word Rate;                               // Sampling rate [Hz]
   word Prefilter;                          // Samples count for averaging
   byte Filter;                             // SINCx filtering
   byte Chop;                               // CHOP mode YES/NO
   word _dummy;
} TPlatformSigmaDelta;

//------------------------------------------------------------------------------
//  Detection
//------------------------------------------------------------------------------

// fine/coarse detection data :
typedef struct {
   byte              AveragingWindow;       // samples count for floating average
   byte              StableWindow;          // samples count for stable value
   word              _Dummy;
   TWeightGauge      AbsoluteRange;         // absolute stabilization range
   TWeightGauge      SwitchoverRange;       // fine/coarse switchover treshold range
} TPlatformDetectionData;

// detection data :
typedef struct {
   TPlatformDetectionData Fine;             // fine detection data
   TPlatformDetectionData Coarse;           // coarse detection data
} TPlatformDetection;

#endif
//------------------------------------------------------------------------------
//  Acceptance
//------------------------------------------------------------------------------

// target weight prediction mode :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class PlatformAcceptanceModeE{
#else
typedef enum {
#endif
   PLATFORM_ACCEPTANCE_MODE_SINGLE,         // acceptance without sex differentiation
   PLATFORM_ACCEPTANCE_MODE_MIXED,          // acceptance with sex differentiation
   PLATFORM_ACCEPTANCE_MODE_STEP,           // acceptance mode disabled (accept all stable weights)
#ifndef _MANAGED
   _PLATFORM_ACCEPTANCE_MODE_LAST
} EPlatformAcceptanceMode;

#else
   };
}
#endif

#ifdef _MANAGED
namespace Bat2Library{
   public enum class PlatformStepModeE{
#else
typedef enum {
#endif
   PLATFORM_STEP_ENTER,
   PLATFORM_STEP_LEAVE,
   PLATFORM_STEP_BOTH,
#ifndef _MANAGED
   _PLATFORM_STEP_LAST
} EPlatformStepMode;
#else
   };
}
#endif

#ifndef _MANAGED
typedef struct {
   byte         Mode;                       // acceptance mode
   byte         Sex;                        // fixed sex
   byte         Step;                       // step mode
   byte         _Dummy;
   TWeightGauge LowLimit;                   // low limit male/both
   TWeightGauge HighLimit;                  // high limit male/both
   TWeightGauge LowLimitFemale;             // low limit female
   TWeightGauge HighLimitFemale;            // high limit female
} TPlatformAcceptance;

//------------------------------------------------------------------------------
//  Calibration
//------------------------------------------------------------------------------

typedef struct {
   byte         Points;                  // full range weight
   TWeightGauge Weight[PLATFORM_CALIBRATION_POINTS_MAX];
   word         Delay;                      // idle delay after loading
   word         Duration;                   // weighing duration
} TPlatformCalibration;

//------------------------------------------------------------------------------
//  Weight record
//------------------------------------------------------------------------------

#ifdef __WIN32__
   #pragma pack( push, 1)                   // byte alignment
#endif

typedef struct {
   TWeightGauge Weight;                     // Weight & Flag composition
   word         Timestamp;                  // time from start of day [2s]
} __packed TPlatformWeight;

#ifdef __WIN32__
   #pragma pack( pop)                       // original alignment
#endif

// Weight composition :
#define PlatformWeightGet( w)     ((int32)((w) & 0xFFFFFF))
#define PlatformFlagGet( w)       ((byte)((w) >> 24))
#define PlatformWeightSet( w, f)  (((w) & 0xFFFFFF) | ((dword)(f) << 24))

// Weight signum extension :
#define PlatformWeightSignumExtension( w) ((w) & 0x00800000L) ? ((w) | 0xFF000000L) : (w)

#endif
//------------------------------------------------------------------------------
#ifdef _MANAGED
   #undef _MANAGED
   #include "PlatformDef.h"
   #define _MANAGED
#endif

#endif
