//******************************************************************************
//
//    Platform.c     Platform services
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Platform.h"
#include "Config/Bat2WireDef.h"
#include <string.h>

TPlatformStatus      PlatformStatus;
TPlatformDiagnostics PlatformDiagnostics;

TPlatformDetection   PlatformDetection;
TPlatformAcceptance  PlatformAcceptance;
TPlatformCalibration PlatformCalibration;

TPlatformCommunication       PlatformCommunication;
TPlatformCommunicationStatus PlatformCommunicationStatus;

//------------------------------------------------------------------------------
//  Weighing
//------------------------------------------------------------------------------

#ifdef OPTION_SIGMA_DELTA
   TPlatformSigmaDelta  PlatformSigmaDelta;

   uConstDeclare( TPlatformSigmaDelta) PlatformSigmaDeltaDefault = {
      /* Rate */      PLATFORM_SIGMA_DELTA_RATE_DEFAULT,
      /* Prefilter */ PLATFORM_SIGMA_DELTA_PREFILTER_DEFAULT,
      /* Filter */    PLATFORM_SIGMA_DELTA_FILTER_SINC4,
      /* Chop */      NO,
      /* _Dummy */    0
   };
#endif // OPTION_SIGMA_DELTA

#ifdef OPTION_PICOSTRAIN
   TPlatformPicostrain PlatformPicostrain;
   
   uConstDeclare( TPlatformPicostrain) PlatformPicostrainDefault = {
      /* Rate */           PLATFORM_PICOSTRAIN_RATE_DEFAULT,
      /* Prefilter */      PLATFORM_PICOSTRAIN_PREFILTER_DEFAULT,
      /* Filter */         PLATFORM_PICOSTRAIN_FILTER_DEFAULT,
      /* Mode */           PLATFORM_PICOSTRAIN_MODE_DEFAULT,
      /* AccuracySwitch */ PLATFORM_PICOSTRAIN_ACCURACY_SWITCH_DEFAULT,
      /* _Dummy1 */        0,
      /* AccuracyFine */   PLATFORM_PICOSTRAIN_ACCURACY_FINE_DEFAULT,
      /* AccuracyCoarse */ PLATFORM_PICOSTRAIN_ACCURACY_COARSE_DEFAULT,
      /* _Dummy2 */        0     
   };
#endif // OPTION_PICOSTRAIN

uConstDeclare( TPlatformDetection) PlatformDetectionDefault = {
   /* Fine */ {
      /* AveragingWindow */ PLATFORM_DETECTION_FINE_AVERAGING_WINDOW_DEFAULT,
      /* StableWindow */    PLATFORM_DETECTION_FINE_STABLE_WINDOW_DEFAULT,
      /* _Dummy */ 0,
      /* AbsoluteRange */   PLATFORM_DETECTION_FINE_ABSOLUTE_RANGE_DEFAULT,
      /* SwitchoverRange */ PLATFORM_DETECTION_FINE_SWITCHOVER_RANGE_DEFAULT
   },
   /* Coarse */ {
      /* AveragingWindow */ PLATFORM_DETECTION_COARSE_AVERAGING_WINDOW_DEFAULT,
      /* StableWindow */    PLATFORM_DETECTION_COARSE_STABLE_WINDOW_DEFAULT,
      /* _Dummy */ 0,
      /* AbsoluteRange */   PLATFORM_DETECTION_COARSE_ABSOLUTE_RANGE_DEFAULT,
      /* SwitchoverRange */ PLATFORM_DETECTION_COARSE_SWITCHOVER_RANGE_DEFAULT
   }
};

uConstDeclare( TPlatformAcceptance) PlatformAcceptanceDefault = {
   /* Mode */            PLATFORM_ACCEPTANCE_MODE_DEFAULT,
   /* Sex */             PLATFORM_ACCEPTANCE_SEX_DEFAULT,
   /* Step */            PLATFORM_ACCEPTANCE_STEP_DEFAULT,
   /* _Dummy */          0,
   /* LowLimit */        PLATFORM_ACCEPTANCE_LOW_LIMIT_DEFAULT,
   /* HighLimit */       PLATFORM_ACCEPTANCE_HIGH_LIMIT_DEFAULT,
   /* LowLimitFemale */  PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_DEFAULT,
   /* HighLimitFemale */ PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_DEFAULT
};

uConstDeclare( TPlatformCalibration) PlatformCalibrationDefault = {
   /* Points */    PLATFORM_CALIBRATION_POINTS_DEFAULT,
   /* Weight    */ {0, 10000, 20000, 30000, 40000},
   /* Delay */     PLATFORM_CALIBRATION_DELAY_DEFAULT,
   /* Duration */  PLATFORM_CALIBRATION_DURATION_DEFAULT
};

//------------------------------------------------------------------------------
//  Communication
//------------------------------------------------------------------------------

uConstDeclare( TPlatformCommunication) PlatformCommunicationDefault = {
   /* BaudRate */              PLATFORM_COMMUNICATION_BAUD_RATE_DEFAULT,
   /* BusAddress */            PLATFORM_COMMUNICATION_BUS_ADDRESS_DEFAULT,
   /* Format */                PLATFORM_COMMUNICATION_FORMAT_DEFAULT,
   /* IntercharacterTimeout */ PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_DEFAULT,
   /* TransmitterDelay */      PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_DEFAULT,
   /* _Dummy */ 0
};

uConstDeclare( TPlatformVersion) pPlatformVersion = {
   /* Device */ {
      /* Class */         DEVICE_PLATFORM_RS485,
#ifdef OPTION_SIGMA_DELTA  
	   /* Modification */  DEVICE_MODIFICATION_SIGMA_DELTA,
#else // OPTION_PICOSTRAIN
	   /* Modification */  DEVICE_MODIFICATION_PICOSTRAIN,
#endif	   
      /* Software */      BAT2_SW_VERSION,
      /* Hardware */      BAT2_HW_VERSION,
      /* SerialNumber */  SERIAL_NUMBER_INVALID
   },
   /* WeightMax */        BAT2_WEIGHT_MAX,
   /* SamplesCount */     BAT2_SAMPLES_MAX,
   /* SensorInversion */  0
};

//------------------------------------------------------------------------------
//  Diagnostics
//------------------------------------------------------------------------------

uConstDeclare( TPlatformDiagnostics) PlatformDiagnosticsDefault = {
   /* SampleSize */ PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_DEFAULT,
   /* BurstCount */ PLATFORM_DIAGNOSTICS_BURST_COUNT_DEFAULT,
   /* Averaging */  PLATFORM_DIAGNOSTICS_AVERAGING_DEFAULT,
   /* FrameCount */ PLATFORM_DIAGNOSTICS_FRAME_COUNT_DEFAULT
};

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void PlatformInit( void)
// Initialize platform
{
   // weighing parameters :
#ifdef OPTION_SIGMA_DELTA
   uConstCopy( &PlatformSigmaDelta,    &PlatformSigmaDeltaDefault,    sizeof( TPlatformSigmaDelta));
#endif   
#ifdef OPTION_PICOSTRAIN
   uConstCopy( &PlatformPicostrain,    &PlatformPicostrainDefault,    sizeof( TPlatformPicostrain));
#endif
   uConstCopy( &PlatformDetection,     &PlatformDetectionDefault,     sizeof( TPlatformDetection));
   uConstCopy( &PlatformAcceptance,    &PlatformAcceptanceDefault,    sizeof( TPlatformAcceptance));
   uConstCopy( &PlatformCalibration,   &PlatformCalibrationDefault,   sizeof( TPlatformCalibration));
   // communication parameters :
   uConstCopy( &PlatformCommunication, &PlatformCommunicationDefault, sizeof( TPlatformCommunication));
   // diagnostics :
   uConstCopy( &PlatformDiagnostics,   &PlatformDiagnosticsDefault,   sizeof( TPlatformDiagnostics));
   // communication status :
   memset( &PlatformCommunicationStatus, 0, sizeof( TPlatformCommunicationStatus));
   memset( &PlatformStatus,              0, sizeof( TPlatformStatus));
} // PlatformInit
