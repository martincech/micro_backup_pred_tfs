 //******************************************************************************
//
//   VersionDef.h   Bat device version
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __VersionDef_H__
   #ifndef _MANAGED
      #define __VersionDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
//  Device class
//------------------------------------------------------------------------------
#ifdef _MANAGED
namespace Bat2Library{
   public enum class DeviceClassE{
#else
typedef enum {
#endif  
   DEVICE_SCALE_COMPACT,               // Bat2 compact
   DEVICE_SCALE_HANDHELD               // Bat1 standard
#ifndef _MANAGED
	,
   _DEVICE_LAST
}EDeviceClass;
#else
   };
}
#endif

//------------------------------------------------------------------------------
//  Device modification
//------------------------------------------------------------------------------

#ifdef _MANAGED
namespace Bat2Library{
   using namespace System;
   [Flags]
   public enum class DeviceModificationE{
#else
typedef enum {
#endif
   BAT2_ETHERNET_MODIFICATION = 0x0001,
   BAT2_GSM_MODIFICATION      = 0x0002,
   BAT2_WIFI_MODIFICATION     = 0x0004,
   BAT2_ZIGBEE_MODIFICATION   = 0x0008,
   BAT2_RS485_I1_MODIFICATION = 0x0010,
   BAT2_RS485_I2_MODIFICATION = 0x0020
#ifndef _MANAGED
	,
   _DEVICE_MODIFICATION_LAST
}EDeviceModification;
#else
   };
}
#endif

#ifndef _MANAGED

#define DeviceVersionSet( Major, Minor, Build) ((((Major) & 0x0F) << 12) | (((Minor) & 0xFF) << 4) | (Build & 0x0F))
#define DeviceBuild( Version)                  ((Version) & 0x000F)
#define DeviceVersion( Version)                ((Version) >> 4)
#define DeviceVersionMajorGet( Version)        (((Version) >> 12) & 0x0F)
#define DeviceVersionMinorGet( Version)        (((Version) >> 4) & 0xFF)
#define DeviceVersionBuilGet( Version)         ((Version) & 0xF)

//------------------------------------------------------------------------------
//  Version
//------------------------------------------------------------------------------

typedef struct {
   word  Class;                        // device class
   word  Modification;                 // device modification
   word  Software;                     // software revision 0x Major:Minor
   word  Hardware;                     // hardware revision 0x Major:Minor
   dword SerialNumber;                 // serial number
} TDeviceVersion;

extern TDeviceVersion Bat2Version;

	// for debuging and testing purposes
#ifdef DEBUG
#ifndef BAT2_MODIFICATION
   #define BAT2_MODIFICATION  0xFFFF
#endif
#endif

#define BAT2_HAS_INTERNET_CONNECTION( config)   NO //(((config) & (BAT2_ETHERNET_MODIFICATION | BAT2_GSM_MODIFICATION | BAT2_WIFI_MODIFICATION)) != 0 ? YES : NO)
#define BAT2_HAS_GSM_MODULE( config)            (((config) & (BAT2_GSM_MODIFICATION)) != 0 ? YES : NO)
#define BAT2_HAS_ETHERNET_MODULE( config)       (((config) & (BAT2_ETHERNET_MODIFICATION)) != 0 ? YES : NO)
#define BAT2_HAS_WIFI_MODULE( config)           (((config) & (BAT2_WIFI_MODIFICATION)) != 0 ? YES : NO)
#define BAT2_HAS_ZIGBEE_MODULE( config)         (((config) & (BAT2_ZIGBEE_MODIFICATION)) != 0 ? YES : NO)
#define BAT2_HAS_RS485_I1_MODULE( config)       (((config) & (BAT2_RS485_I1_MODIFICATION)) != 0 ? YES : NO)
#define BAT2_HAS_RS485_I2_MODULE( config)       (((config) & (BAT2_RS485_I2_MODIFICATION)) != 0 ? YES : NO)
#define BAT2_HAS_RS485_MODULE( config)          (((config) & (BAT2_RS485_I1_MODIFICATION | BAT2_RS485_I2_MODIFICATION)) != 0 ? YES : NO)

#define SERIAL_NUMBER_INVALID       0xFFFFFFFF

#endif
#ifdef _MANAGED
   #undef _MANAGED
   #include "VersionDef.h"
   #define _MANAGED
#endif

#endif //__VersionDef_H__
