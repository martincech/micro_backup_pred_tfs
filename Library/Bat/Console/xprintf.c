//*****************************************************************************
//
//    xprintf.c - simple formatted output template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"                  // conditional compilation
#include "xprint.h"
#include "Convert/uBcd.h"
#ifdef PRINTF_STRING
   #include "String/StrDef.h"
#endif


//-----------------------------------------------------------------------------
// xprintf
//-----------------------------------------------------------------------------

void xprintf( TPutchar *xputchar, const char *Format, ...)
// jednoduchy formatovany vystup
{
va_list Arg;

   va_start( Arg, Format);
   xvprintf( xputchar, Format, Arg);
   va_end( Arg);
} // xprintf

//-----------------------------------------------------------------------------
// xvprintf
//-----------------------------------------------------------------------------

void xvprintf( TPutchar *xputchar, const unsigned char *Format, va_list Arg)
// jednoduchy formatovany vystup
{
int       width;
int       decimals;
unsigned  flags;
unsigned char      ch;
char      *s;
#ifdef PRINTF_STRING
   Format = StrGet( Format);
#endif
   while( *Format){
      ch = *Format;
      if( ch != '%'){
         xputchar( ch);
         Format++;
         continue;
      }
      ch = *(++Format);                          // preskoc %
      width    = 0;                              // delka podle potreby
      decimals = 0;                              // desetiny
      flags    = 0;                              // zadne priznaky
      if( ch == '+'){
         flags |= FMT_PLUS;
         ch = *(++Format);
      }
      // vedouci nula :
      if( ch == '0'){
         flags |= FMT_LEADING_0;
         ch = *(++Format);
      }
      // sirka :
      if( (*Format >= '1') &&  (*Format <= '9')){
         width = uCharToDec( ch);
         ch = *(++Format);
         if( (ch >= '0') &&  (ch <= '9')){
            width *= 10;
            width += uCharToDec( ch);
            ch = *(++Format);
         }
      }
      // desetiny :
      if( ch == '.'){
         ch = *(++Format);
         if( (ch >= '1') &&  (ch <= '9')){
            decimals = uCharToDec( ch);
            ch = *(++Format);
         }
      }
      // typ pole :
      switch( ch){
#ifdef PRINTF_HEXA
         case 'B' :
            // byte hexadecimalne
            xprinthex( xputchar, (byte)va_arg( Arg, unsigned),  2 | FMT_LEADING_0);  // byte
            break;
         case 'W' :
            // word hexadecimalne
            xprinthex( xputchar, (word)va_arg( Arg, unsigned),  4 | FMT_LEADING_0);  // word
            break;
         case 'D' :
            // dword hexadecimalne
            xprinthex( xputchar, (dword)va_arg( Arg, unsigned), 8 | FMT_LEADING_0);  // dword
            break;
#endif
         case 'f' :
            xfloat( xputchar, va_arg( Arg, int), width, decimals, flags);
            break;
         case 'X' :
         case 'x' :
            xprinthex( xputchar, (dword)va_arg( Arg, unsigned), width | flags);   // implicitne dword
            break;

         case 'u' :
            width |= FMT_UNSIGNED;
            xprintdec( xputchar, (dword)va_arg( Arg, unsigned), width | flags);   // implicitne int32
            break;

         case 'd' :
            xprintdec( xputchar, (int32)va_arg( Arg, int), width | flags);   // implicitne int32
            break;

         case 'c' :
            xputchar( va_arg( Arg, int));
            break;

         case 's' :
            s = va_arg( Arg, char *);
            if( width){
               // string with width set
               for(flags = 0; flags < width; flags++){
                  xputchar( s[flags]);
                  if(s[flags] == '\0'){
                     break;
                  }
               }
            } else {
               xputs( xputchar, s);
            }
            break;

         case '%' :
         default :
            xputchar( ch);                        // je to jen znak
            break;
      }
      Format++;
   }
   va_end( Arg);
} // xvprintf
