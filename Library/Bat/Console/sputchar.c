//*****************************************************************************
//
//    sputchar.c   string putchar
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "sputchar.h"

// Local variables :

static int   _Index;
static char *_Buffer;

//-----------------------------------------------------------------------------
// Buffer
//-----------------------------------------------------------------------------

void sputcharbuffer( char *buffer)
// set sputchar buffer
{
   _Buffer = buffer;
   _Index  = 0;
} // ssetbufer

//-----------------------------------------------------------------------------
// sputchar
//-----------------------------------------------------------------------------

int sputchar( int c)
// write character to buffer
{
   _Buffer[ _Index++] = (char)c;
   return( c);
} // sputchar
