//*****************************************************************************
//
//    xprint.c     Simple formatting services template
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Hardware.h"                  // conditional compilation
#include "xprint.h"
#include "Convert/uBcd.h"
#ifdef PRINTF_STRING
   #include "String/StrDef.h"
#endif

//-----------------------------------------------------------------------------
// Retezec
//-----------------------------------------------------------------------------

void xputs( TPutchar *xputchar, const char *string)
// vystup retezce <string>
{
#ifdef PRINTF_STRING
   string = StrGet( string);
#endif
   while( *string){
      xputchar( *string);
      string++;
   }
} // xputs

//-----------------------------------------------------------------------------
// Retezec
//-----------------------------------------------------------------------------

void xputsn(  TPutchar *xputchar, const char *string)
// vystup retezce <string> az po LF
{
#ifdef PRINTF_STRING
   string = StrGet( string);
#endif
   while( *string && *string != '\n'){
      xputchar( *string);
      string++;
   }
} // xputsn

//-----------------------------------------------------------------------------
// Dekadicke cislo se znamenkem
//-----------------------------------------------------------------------------

void xprintdec( TPutchar *xputchar, int32 x, dword flags)
// vystup dekadickeho cisla <x>, <flags> koduje sirku
{
   if( !(flags & FMT_UNSIGNED)){
      // se znamenkem
      if( x < 0){
         flags |= FMT_NEGATIVE;                  // znamenko minus
         x = -x;
      }
   }
   x = uBinaryToBcd( x);
   xprinthex( xputchar, x, flags);
} // xprintdec

//-----------------------------------------------------------------------------
// Hexadecimalni
//-----------------------------------------------------------------------------

void xprinthex( TPutchar *xputchar, dword x, dword flags)
// vystup hexa cisla <x>, <flags> koduje sirku
{
dword nibble;
int i;
int decimals;      // pocet za teckou
int width;         // sirka pole
int digits;        // pocet platnych cislic
int spaces;        // pocet vedoucich mezer

   decimals = FmtGetDecimals( flags);            // pocet za carkou
   width    = FmtGetWidth( flags);               // sirka pole
   if( flags & FMT_LEADING_0){
      // pocet platnych cislic odvodit od sirky pole
      digits = width;
      if( (flags & FMT_NEGATIVE) || (flags & FMT_PLUS)){
         digits--;                               // jedna pozice na znamenko
      }
      if( decimals){
         digits--;                               // jedna pozice na tecku
      }
   } else {     
      digits = uBcdWidth( x);                    // spocitat pocet platnych cislic v cisle
   }
   // korekce na 0. pro float :
   if( digits <= decimals){
      digits = decimals + 1;                     // nula pred teckou + cisla za teckou
   }
   // pocet vedoucich mezer :
   if((flags & FMT_NEGATIVE) || (flags & FMT_PLUS)){
      spaces = width - digits - 1;               // pozice na znamenko
   } else {
      spaces = width - digits;
   }
   if( decimals){
      spaces--;                                  // povinna tecka
   }
   if( spaces < 0){
      spaces = 0;                                // spatna sirka
   }
   for( i = 0; i < spaces; i++){
      xputchar( ' ');
   }
   if( flags & FMT_NEGATIVE){
      xputchar( '-');
   } else if( flags & FMT_PLUS){
      xputchar( '+');
   }
   x <<= (8 - digits) * 4;                        // odriznuti vedoucich nul
   // jednotlive nibbly ve smeru od MSB..LSB v sirce digits :
   for( i = digits; i > 0; i--){
      nibble = x >> 28;
      x <<= 4;      
      if( decimals == i){
         xputchar( '.');
      }
      xputchar( uNibbleToHex( nibble));
   }
} // xprinthex

//-----------------------------------------------------------------------------
// BCD s desetinnou teckou
//-----------------------------------------------------------------------------

void xfloat( TPutchar *xputchar, dword x, int w, int d, dword flags)
// Tiskne binarni cislo <x> jako float s celkovou sirkou <w>
// a <d> desetinnymi misty. <flags> jsou doplnujici atributy
// (FMT_PLUS)
{
   xprintdec( xputchar, x, FmtPrecision( w, d) | flags);
} // xfloat
