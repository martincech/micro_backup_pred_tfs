//******************************************************************************
//
//   Country.c     Country & locales utility
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Country.h"
#include "Time/uClock.h"

#include "CountryTable.c"         // project specific country tables

TCountry Country;                 // Country data

const TCountry CountryDefault = {
   /* Country  */      COUNTRY_INTERNATIONAL,
   /* Language */      LNG_ENGLISH,
   /* _Spare   */      0,
   /* Locale   */      { CPG_LATIN, DATE_FORMAT_DDMMYYYY, '.', '.', TIME_FORMAT_24, ':', DAYLIGHT_SAVING_OFF, 0},
};

#if LANGUAGE_COUNT > 1  

static int SearchCodePage( int Language);
// Search code page by <Language>
#endif

//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

void CountrySet( int CountryCode)
// Fill country data by <CountryCode>
{
   Country.Country  = CountryCode;
   Country.Language = _LocaleIndex[ CountryCode].Language;
   Country.Locale   = _Locales[ _LocaleIndex[ CountryCode].Locale];
} // CountrySet

//------------------------------------------------------------------------------
//  Language
//------------------------------------------------------------------------------

int CountryLanguage( void)
// Returns language code
{
#if LANGUAGE_COUNT > 1  
   return( Country.Language);
#else
   return( 0);
#endif
} // CountryLanguage

void CountryLanguageSet( int Language)
// Set <Language> and code page
{
#if LANGUAGE_COUNT > 1  
   Country.Language        = Language;
   Country.Locale.CodePage = SearchCodePage( Language);
#endif
} // CountryLanguage

//------------------------------------------------------------------------------
//  Code Page
//------------------------------------------------------------------------------

int CountryCodePage( void)
// Returns code page
{
   return( Country.Locale.CodePage);
} // CountryCodePage

//------------------------------------------------------------------------------
//  Date format
//------------------------------------------------------------------------------

int CountryDateFormat( void)
// Returns date format enum
{
   return( Country.Locale.DateFormat);
} // CountryDateFormat

//------------------------------------------------------------------------------
//  Date Separator 1
//------------------------------------------------------------------------------

char CountryDateSeparator1( void)
// Returns date separator character
{
   return( Country.Locale.DateSeparator1);
} // CountryDateSeparator1

//------------------------------------------------------------------------------
//  Date Separator 2
//------------------------------------------------------------------------------

char CountryDateSeparator2( void)
// Returns date separator character
{
   return( Country.Locale.DateSeparator2);
} // CountryDateSeparator2

//------------------------------------------------------------------------------
//  Time format
//------------------------------------------------------------------------------

int CountryTimeFormat( void)
// Returns time format enum
{
   return( Country.Locale.TimeFormat);
} // CountryTimeFormat

//------------------------------------------------------------------------------
//  Time separator
//------------------------------------------------------------------------------

char CountryTimeSeparator( void)
// Returns time separator character
{
   return( Country.Locale.TimeSeparator);
} // CountryTimeSeparator

//------------------------------------------------------------------------------
//  DST
//------------------------------------------------------------------------------

int CountryDaylightSavingType( void)
// Returns daylight saving type
{
   return( Country.Locale.DaylightSavingType);
} // CountryDaylightSavingType

//******************************************************************************

#if LANGUAGE_COUNT > 1  
//------------------------------------------------------------------------------
//  Search Code Page
//------------------------------------------------------------------------------

static int SearchCodePage( int Language)
// Search code page by <Language>
{
int i;

   for( i = 0; i < _COUNTRY_COUNT; i++){
      if( _LocaleIndex[ i].Language == Language){
         return( _Locales[ _LocaleIndex[ i].Locale].CodePage);
      }
   }
   return( 0);
} // SearchCodePage
#endif
