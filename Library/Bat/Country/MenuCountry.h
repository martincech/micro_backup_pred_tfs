//******************************************************************************
//                                                                            
//   MenuCountry.h   Country configuration menu
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuCountry_H__
   #define __MenuCountry_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuCountry( void);
// Country configuration menu

#endif
