//******************************************************************************
//
//    SexDef.h       Sex data definitions
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __SexDef_H__
   #ifndef _MANAGED
      #define __SexDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifdef _MANAGED
namespace Bat2Library{
   public enum class SexE{
#else
typedef enum {
#endif
   SEX_MALE,
   SEX_FEMALE,
   SEX_UNDEFINED,
#ifndef _MANAGED
   _SEX_LAST
} ESex;
#else
   };
}
#endif

#ifndef _MANAGED
#define SEX_INVALID 0xFF
#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "SexDef.h"
   #define _MANAGED
#endif

#endif
