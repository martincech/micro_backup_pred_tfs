//******************************************************************************
//
//   DEnterIp.c     Enter Ip
//   Version 1.0    (c) Veit Electronics
//
//******************************************************************************

#include "DEnter.h"
#include "DEvent.h"
#include "Sound/Beep.h"
#include "Graphic/Graphic.h"
#include "DIp.h"

#define POSITION_MAX   (4*3 - 1)

static void IpUpDown( fnet_ip4_addr_t *Ip, int Position, TYesNo UpDown);
// Increment/decrement IP address digit on <Position>

//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

TYesNo DEnterIp( fnet_ip4_addr_t *Ip, int x, int y)
// Display and edit <Ip>
{
int    Position;      // cursor position
int    CursorPosition;
int    Event;
TYesNo Redraw;        // redraw text
TYesNo ShowCursor;    // show/hide cursor
TYesNo DrawCursor;    // draw/skip cursor

   Position    = POSITION_MAX;                             // at last character
   Redraw      = YES;                           // first draw
   ShowCursor  = YES;                           // show cursor
   DrawCursor  = NO;                            // wait for flash
   forever {
      if( Redraw){
         // clear area :
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y,  DEnterTextWidth( FNET_IP4_ADDR_STR_SIZE), DENTER_H);
         GSetColor( DCOLOR_ENTER);
         // draw text :
         GSetFixedPitch();             // set nonproportional font
         DIp(*Ip, x + 1, y);
         GSetNormalPitch();            // restore font setting
         // draw cursor :
         if( !ShowCursor || DrawCursor){
            GSetMode( GMODE_XOR);
            GSetColor( DCOLOR_ENTER_CURSOR);
            CursorPosition = FNET_IP4_ADDR_STR_SIZE - 1 - Position - Position / 3;
            GBox( x + 1 + CursorPosition * GCharWidth(), y, GCharWidth() - 1, DENTER_H);
            GSetMode( GMODE_REPLACE);
         }
         GSetColor( DCOLOR_DEFAULT);
         Redraw = NO;
         GFlush();                     // redraw
      }
      Event = DEventWait();
      switch( Event){
         default:
            if(Event & K_RELEASED){
               ShowCursor = YES;       // key released, redraw cursor
               Redraw     = YES;
            }
         break;
         case K_UP :
         case K_UP | K_REPEAT :
            BeepKey();
            ShowCursor = NO;           // disable cursor
            IpUpDown( Ip, Position, YES);
            Redraw = YES;              // redraw edit box
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            BeepKey();
            ShowCursor = NO;           // disable cursor
            IpUpDown( Ip, Position, NO);
            Redraw = YES;              // redraw edit box
            break;

         case K_RIGHT | K_REPEAT :
         case K_RIGHT :
            ShowCursor = NO;           // disable cursor
            if( Position == 0){
               BeepError();
               break;
            }
            BeepKey();
            Position--;
            Redraw = YES;
            break;

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            ShowCursor = NO;           // disable cursor
            if( Position == POSITION_MAX){
               BeepError();
               break;
            }
            BeepKey();
            Position++;
            Redraw = YES;
            break;

         case K_RELEASED :
            ShowCursor = YES;       // key released, redraw cursor
            Redraw     = YES;
            break;

         case K_FLASH1 :
            DrawCursor = YES;
            Redraw     = YES;
            break;

         case K_FLASH2 :
            DrawCursor = NO;
            Redraw     = YES;
            break;

         case K_ENTER :
            BeepKey();
            return( YES);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DEnterIp

//------------------------------------------------------------------------------
//  IP width
//------------------------------------------------------------------------------

int DEnterIpWidth( void)
// Returns width of IP field
{
   return( FNET_IP4_ADDR_STR_SIZE * GCharWidth());
} // DEnterIpWidth

//******************************************************************************

//------------------------------------------------------------------------------
//  Up/Down
//------------------------------------------------------------------------------

static void IpUpDown( fnet_ip4_addr_t *Ip, int Position, TYesNo UpDown)
// Increment/decrement IP address digit on <Position>
{
   int InTriplePosition = Position % 3;
   int TriplePosition = Position / 3;
   int Tens;
   byte *IpByte = (byte *) Ip;
   int Triple = IpByte[TriplePosition];
   int TripleNulled;
   int DigitMax;
   int Digit;

   switch(InTriplePosition) {
      case 0:
         Tens = 1;
         break;

      case 1:
         Tens = 10;
         break;

      case 2:
         Tens = 100;
         break;
   }

   // extract number being edited
   Digit = (Triple / Tens) % 10;
   // null the number
   TripleNulled = Triple - Digit * Tens;

   // determine max allowable number to stay in valid range of IP address triple (0 - 255)
   switch(InTriplePosition) {
      case 0:
         if(TripleNulled >= 250) {
            DigitMax = 5;
         } else {
            DigitMax = 9;
         }
         break;

      case 1:
         if(TripleNulled > 205) {
            DigitMax = 4;
         } else if(TripleNulled >= 200) {
            DigitMax = 5;
         } else {
            DigitMax = 9;
         }
         break;

      case 2:
         if(TripleNulled > 55) {
            DigitMax = 1;
         } else {
            DigitMax = 2;
         }
         break;
   }

   // increment/decrement
   if(UpDown) {
      Digit++;
      if(Digit > DigitMax) {
         Digit = 0;
      }

   } else {
      if(Digit == 0) {
         Digit = DigitMax;
      } else {
         Digit--;
      }
   }
   Triple = TripleNulled + Digit * Tens;
   IpByte[TriplePosition] = Triple;
} // IpUpDown
