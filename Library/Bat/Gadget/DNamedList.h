//******************************************************************************
//                                                                            
//   DNamedList.h  Named list editor
//   Version 1.1   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DNamedList_H__
   #define __DNamedList_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __uNamedList_H__
   #include "Data/uNamedList.h"
#endif


typedef enum {
   DLIST_OPERATION_EDIT,
   DLIST_OPERATION_CREATE,
   DLIST_OPERATION_COPY,
   DLIST_OPERATION_DELETE,
   DLIST_OPERATION_RENAME,
   DLIST_OPERATION_UNDEFINED = -1,
   DLIST_OPERATION_CANCEL = - 2
} ENamedListOperation;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

int DNamedListOperation( TUniStr Title, UNamedList *List, UDirectoryIndex *Index, char *Name);
// Returns list item operation. Set default <Name> before call !

#endif
