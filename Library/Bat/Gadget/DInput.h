//******************************************************************************
//
//   DInput.h       Display input box
//   Version 1.1    (c) VymOs
//
//******************************************************************************

#ifndef __DInput_H__
   #define __DInput_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

#include "fnet_ip.h"

#ifndef __DCallback_H__
   #include "Gadget/DCallback.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

TYesNo DInputNumber( TUniStr Caption, TUniStr Text, int32 *Value,
                     int Decimals, int LoLimit, int HiLimit, TUniStr Units);
// Input number

TYesNo DInputText( TUniStr Caption, TUniStr Text, char *String, int Width, TYesNo EnableEmpty);
// Input text up to <Width> letters. <EnableEmpty> string

TYesNo DInputTextL( TUniStr Caption, TUniStr Text, char *String, int Width, TYesNo EnableEmpty, const char *AllowedLetters);
// Input text up to <Width> letters. <EnableEmpty> string, letters limited to <AllowedLetters>

TYesNo DInputEnum( TUniStr Caption, TUniStr Text, int *Value,
                   TUniStr Base, int EnumCount);
// Input enum

#ifdef DENTER_CALLBACK
   TYesNo DInputEnumCallback( TUniStr Caption, TUniStr Text, int *Value,
                              TUniStr Base, int EnumCount, TAction *OnChange);
   // Input enum with <OnChange> callback
#endif

TYesNo DInputProgress( TUniStr Caption, TUniStr Text, int *Value,
                       int MaxValue, TAction *OnChange);
// Input value by progress bar

void DInputPassword( TUniStr Caption, TUniStr Text, char *Password, int Width);
// Input password

TYesNo DInputTime( TUniStr Caption, TUniStr Text, UTime *Time);
// Enter time

TYesNo DInputDate( TUniStr Caption, TUniStr Text, UDate *Date);
// Enter date

TYesNo DInputIp( TUniStr Caption, TUniStr Text, fnet_ip4_addr_t *Ip);
// Enter IP address

//TYesNo DInputDateTime( TUniStr Caption, TUniStr Text, UDateTime *DateTime);
// Enter time

#endif
