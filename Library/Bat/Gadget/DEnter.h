//******************************************************************************
//
//  DEnter.h       Display enter value
//  Version 1.1    (c) VymOs
//
//******************************************************************************

#ifndef __DEnter_H__
   #define __DEnter_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

#ifndef __DCallback_H__
   #include "Gadget/DCallback.h"
#endif

#ifndef __DLabel_H__
   #include "Gadget/DLabel.h"   // Center type
#endif

#include "fnet_ip.h"

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

TYesNo DEnterNumber( int *InitialValue, int Width, int Decimals, int EditWidth, int x, int y);
// Display and edit <InitialValue>.
// <Width> - total digits, <Decimals> digits after decimal dot, <EditWidth> editable width

int DEnterNumberWidth( int Width, int Decimals);
// Returns pixel width of number field

TYesNo DEnterText( char *Text, int Width, int x, int y);
// Display and edit <Text> with maximal <Width>

TYesNo DEnterIp( fnet_ip4_addr_t *Ip, int x, int y);
// Enter IP

TYesNo DEnterTextL( char *Text, int Width, int x, int y, const char *AllowedLetters);
// Display and edit <Text> with maximal <Width>, letters limited to <AllowedLetters>

int DEnterTextWidth( int Width);
// Returns pixel width of text field

#ifndef DENTER_CALLBACK
   TYesNo DEnterEnum( int *InitialValue, TUniStr Base, int EnumCount, int x, int y, TCenterType Center, unsigned Mask);
   // Display and edit <InitialValue>
#else
   TYesNo DEnterEnum( int *InitialValue, TUniStr Base, int EnumCount, TAction *OnChange, int x, int y, TCenterType Center, unsigned Mask);
   // Display and edit <InitialValue>
#endif

int DEnterEnumWidth( TUniStr Base, int EnumCount);
// Returns pixel width of enum field

TYesNo DEnterProgress( int *InitialValue, int MaxValue, TAction *OnChange, int x, int y);
// Enter value by progress bar

int DEnterProgressWidth( void);
// Returns pixel width of progress field

void DEnterPassword( char *Password, int Width, int x, int y);
// Enter password into <Password> array

int DEnterPasswordWidth( int Width);
// Returns pixel width of password field

TYesNo DEnterTime( UTime *Time, int x, int y);
// Enter time

int DEnterTimeWidth( void);
// Returns character width of time field

TYesNo DEnterDate( UDate *Date, int x, int y);
// Enter date

int DEnterDateWidth( void);
// Returns character width of date field

TYesNo DEnterIp( fnet_ip4_addr_t *Ip, int x, int y);
// Display and edit <Ip>

int DEnterIpWidth( void);
// Returns width of IP field

#endif
