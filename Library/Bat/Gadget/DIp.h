//******************************************************************************
//
//  DIp.h          Display IP
//  Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DIp_H__
   #define __DIp_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#include "fnet_ip.h"

void DIp( fnet_ip4_addr_t Ip, int x, int y);
// Display IP

//------------------------------------------------------------------------------
//   Right aligned
//------------------------------------------------------------------------------

void DIpShortRight( fnet_ip4_addr_t Ip, int x, int y);
// Display IP

#endif
