//******************************************************************************
//
//  DTime.c        Display time
//  Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Dtime.h"
#include "Graphic/Graphic.h"
#include "Console/sputchar.h"
#include "Gadget/DLabel.h"         // text label
#include "Console/conio.h"         // putchar
#include "Country/xTime.h"         // date & time

//------------------------------------------------------------------------------
//  Time
//------------------------------------------------------------------------------

void DTime( UTimeGauge Now, int x, int y)
// Display time only
{
   GTextAt( x, y);
   xTime( cputchar, Now);
} // DTime

//------------------------------------------------------------------------------
//  Short Time
//------------------------------------------------------------------------------

void DTimeShort( UTimeGauge Now, int x, int y)
// Display time without seconds
{
   GTextAt( x, y);
   xTimeShort( cputchar, Now);
} // DTimeShort

//------------------------------------------------------------------------------
//  Date
//------------------------------------------------------------------------------

void DDate( UDateGauge Now, int x, int y)
// Display date only
{
   GTextAt( x, y);
   xDate( cputchar, Now);
} // DDate

//------------------------------------------------------------------------------
//  Date & Time
//------------------------------------------------------------------------------

void DDateTime( UDateTimeGauge Now, int x, int y)
// Display date and time
{
   GTextAt( x, y);
   xDateTime( cputchar, Now);
} // DDateTime

//------------------------------------------------------------------------------
//  Short Date & Time
//------------------------------------------------------------------------------

void DDateTimeShort( UDateTimeGauge Now, int x, int y)
// Display date and time without seconds
{
   GTextAt( x, y);
   xDateTimeShort( cputchar, Now);
} // DDateTimeShort

//******************************************************************************

//------------------------------------------------------------------------------
//  Time right
//------------------------------------------------------------------------------

void DTimeRight( UTimeGauge Now, int x, int y)
// Display time only
{
char Buffer[ DLABEL_LENGTH_MAX + 1];

   sputcharbuffer( Buffer);
   xTime( sputchar, Now);
   sputchar( '\0');                    // string terminator
   DLabelRight( Buffer, x, y);
} // DTimeRight

//------------------------------------------------------------------------------
//  Short Time
//------------------------------------------------------------------------------

void DTimeShortRight( UTimeGauge Now, int x, int y)
// Display time without seconds
{
char Buffer[ DLABEL_LENGTH_MAX + 1];

   sputcharbuffer( Buffer);
   xTimeShort( sputchar, Now);
   sputchar( '\0');                    // string terminator
   DLabelRight( Buffer, x, y);
} // DTimeShortRight

//------------------------------------------------------------------------------
//  Date
//------------------------------------------------------------------------------

void DDateRight( UDateGauge Now, int x, int y)
// Display date only
{
char Buffer[ DLABEL_LENGTH_MAX + 1];

   sputcharbuffer( Buffer);
   xDate( sputchar, Now);
   sputchar( '\0');                    // string terminator
   DLabelRight( Buffer, x, y);
} // DDateRight

//------------------------------------------------------------------------------
//  Date & Time
//------------------------------------------------------------------------------

void DDateTimeRight( UDateTimeGauge Now, int x, int y)
// Display date and time
{
char Buffer[ DLABEL_LENGTH_MAX + 1];

   sputcharbuffer( Buffer);
   xDateTime( sputchar, Now);
   sputchar( '\0');                    // string terminator
   DLabelRight( Buffer, x, y);
} // DDateTimeRight

void DDateTimeCenter( UDateTimeGauge Now, int x, int y)
// Display date and time centered
{
char Buffer[ DLABEL_LENGTH_MAX + 1];

   sputcharbuffer( Buffer);
   xDateTime( sputchar, Now);
   sputchar( '\0');                    // string terminator
   DLabelCenter( Buffer, x, y, G_WIDTH, GTextHeight(Buffer));
   } // DDateTimeRight

//------------------------------------------------------------------------------
//  Short Date & Time
//------------------------------------------------------------------------------

void DDateTimeShortRight( UDateTimeGauge Now, int x, int y)
// Display date and time without seconds
{
char Buffer[ DLABEL_LENGTH_MAX + 1];

   sputcharbuffer( Buffer);
   xDateTimeShort( sputchar, Now);
   sputchar( '\0');                    // string terminator
   DLabelRight( Buffer, x, y);
} // DDateTimeShortRight
