//******************************************************************************
//                                                                            
//   DNamedList.c  Named list editor
//   Version 1.1   (c) VEIT Electronics
//
//******************************************************************************

#include "DNamedList.h"
#include "Gadget/DDirectory.h"    // Display list directory
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DMsg.h"          // Message box
#include "Gadget/DInput.h"        // Input box
#include "Str.h"                  // project directory strings

static DefMenu( ListMenu)
   STR_EDIT,
   STR_CREATE,
   STR_COPY,
   STR_DELETE,
   STR_RENAME,
EndMenu()

// Local functions :
static TYesNo _NameEnter( TUniStr Title, const UNamedList *List, char *Name);
// Enter a new <Name>

//------------------------------------------------------------------------------
//  List operation
//------------------------------------------------------------------------------

int DNamedListOperation( TUniStr Title, UNamedList *List, UDirectoryIndex *Index, char *Name)
// Returns list item operation. Set default <Name> before call !
{
TMenuData       MData;
UDirectoryIndex idx;

   DMenuClear( MData);
   forever {
      MData.Mask = 0;
      // check for directory capacity :
      if( uDirectoryCount( (UDirectory *)uNamedListDirectory( List)) == uDirectoryCapacity( (const UDirectoryDescriptor *)uNamedListDirectory( List->Descriptor))){
         MData.Mask |= (1 << DLIST_OPERATION_CREATE);  // disable create
         MData.Mask |= (1 << DLIST_OPERATION_COPY);    // disable copy
      }
      if( uDirectoryCount( (UDirectory *)uNamedListDirectory( List)) == 0){
         MData.Mask |= (1 << DLIST_OPERATION_EDIT);    // disable edit
         MData.Mask |= (1 << DLIST_OPERATION_COPY);    // disable copy
         MData.Mask |= (1 << DLIST_OPERATION_DELETE);  // disable delete
         MData.Mask |= (1 << DLIST_OPERATION_RENAME);  // disable rename
      }
      // select operation :
      if( !DMenu( Title, ListMenu, 0, 0, &MData)){
         return( DLIST_OPERATION_CANCEL);
      }
      // check for create first :
      if( MData.Item == DLIST_OPERATION_CREATE){
         // edit item name :
         if( !_NameEnter( Title, List, Name)){
            return( DLIST_OPERATION_UNDEFINED);
         }
         // add data item to list :
         *Index = uNamedListAdd( List, Name, 0);
         return( DLIST_OPERATION_CREATE);
      }
      // select item :
      idx = DDirectorySelect( Title, uNamedListDirectory( List), 0, 0);
      if( idx == UDIRECTORY_INDEX_INVALID){
         return( DLIST_OPERATION_UNDEFINED);
      }
      uDirectoryItemName( (UDirectory *)uNamedListDirectory( List), idx, Name);
      // execute operation :
      switch( MData.Item){
         case DLIST_OPERATION_EDIT :
            *Index = idx;
            return( DLIST_OPERATION_EDIT);

         case DLIST_OPERATION_COPY :
            // edit item name :
            if( !_NameEnter( Title, List, Name)){
               return( DLIST_OPERATION_UNDEFINED);
            }
            // add data item to list :
            *Index = uNamedListCopy( List, Name, idx);
            return( DLIST_OPERATION_COPY);

         case DLIST_OPERATION_DELETE :
            if( !DMsgYesNo( STR_CONFIRMATION, STR_REALLY_DELETE, Name)){
               return( DLIST_OPERATION_UNDEFINED);
            }
            uNamedListDelete( List, idx);
            *Index = UDIRECTORY_INDEX_INVALID;
            return( DLIST_OPERATION_DELETE);

         case DLIST_OPERATION_RENAME :
            // enter a new name :
            if( !_NameEnter( Title, List, Name)){
               return( DLIST_OPERATION_UNDEFINED);
            }
            *Index = uNamedListRename( List, idx, Name);
            return( DLIST_OPERATION_RENAME);
      }
      return( DLIST_OPERATION_UNDEFINED);
   }
} // DNamedListOperation

//------------------------------------------------------------------------------
//   Enter name
//------------------------------------------------------------------------------

static TYesNo _NameEnter( TUniStr Title, const UNamedList *List, char *Name)
// Enter a new <Name>
{
   forever {
      if( !DInputText( Title, STR_ENTER_NAME, Name, UDIRECTORY_NAME_SIZE, NO)){
         return( NO);
      }
      // check for name exists :
      if( uDirectoryFind((UDirectory *) uNamedListDirectory( List), Name) != UDIRECTORY_INDEX_INVALID){
         DMsgOk( STR_ERROR, STR_NAME_EXISTS, 0);
         continue;                  // enter again
      }
      return( YES);
   }
} // _NameEnter
