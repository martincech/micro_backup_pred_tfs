//******************************************************************************
//                                                                            
//   DDirectory.h  Directory viewer
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DDirectory_H__
   #define __DDirectory_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __uDirectory_H__
   #include "Data/uDirectory.h"
#endif

#define DDIRECTORY_INDEX_SPECIAL  0xFE

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

UDirectoryIndex DDirectorySelect( TUniStr Title, const UDirectory *Directory, UDirectoryIndex InitialIndex, TUniStr SpecialItem);
// Select item from <Directory>, move cursor at <InitialIndex> item. <SpecialItem> added at end of list

#define DDirectorySpecial( Index)          ((Index) == DDIRECTORY_INDEX_SPECIAL)
// Test for special item selected

#endif
