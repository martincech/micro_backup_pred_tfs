//******************************************************************************
//
//  DMsg.h         Display text message box
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DMsg_H__
   #define __DMsg_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif
#ifdef __cplusplus
   extern "C" {
#endif

void DMsgOk( TUniStr Caption, TUniStr Text, TUniStr Text2);
// Displays <Text> with title <Caption> with OK button

void DMsgCancel( TUniStr Caption, TUniStr Text, TUniStr Text2);
// Displays window with Cancel button. Warning : doesn't wait for a key

TYesNo DMsgOkCancel( TUniStr Caption, TUniStr Text, TUniStr Text2);
// Displays <Text> with title <Caption> with OK/Cancel buttons

TYesNo DMsgYesNo( TUniStr Caption, TUniStr Text, TUniStr Text2);
// Displays <Text>/<Text2> with title <Caption> with Yes/No buttons

void DMsgWait( void);
// Display waiting

#ifdef __cplusplus
   }
#endif
#endif
