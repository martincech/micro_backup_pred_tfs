//******************************************************************************
//
//   Calibration.c  Calibration utility
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Calibration.h"
#include "CalibrationDef.h"
#include "Data/uConst.h"            // Constant data
#include "Eeprom/Eep.h"             // EEPROM services
#include "Config/EepromLayout.h"    // EEPROM layout

static TCalibration Calibration;

static uConstDeclare( TCalibration) CalibrationDefault = {
   /* Offset */     0,
   /* Factor */     1000,
   /* Range */      1000,
   /* Inversion */  0,
   /* _Spare */     0,
   /* Crc */        0
};

// EEPROM address :
#define CALIBRATION_ADDRESS   offsetof( TEepromLayout, Calibration)

// Local functions :

static TCalibrationCrc CalibrationCrc( void);
// Calculate CRC

//------------------------------------------------------------------------------
// Load
//------------------------------------------------------------------------------

TYesNo CalibrationLoad( void)
// Load calibration from EEPROM
{
   EepDataLoad( CALIBRATION_ADDRESS, &Calibration, sizeof( TCalibration));
   if( CalibrationCrc() == Calibration.Crc){
      return( YES);                    // calibration OK
   }
   // wrong checksum, set defaults
   uConstCopy( &Calibration, &CalibrationDefault, sizeof( TCalibration));
   CalibrationSave();
   return( NO);
} // CalibrationLoad

//------------------------------------------------------------------------------
// Save
//------------------------------------------------------------------------------

void CalibrationSave( void)
// Save calibration to EEPROM
{
   Calibration.Crc = CalibrationCrc();
   EepDataSave( CALIBRATION_ADDRESS, &Calibration, sizeof( TCalibration));
} // CalibrationSave

//------------------------------------------------------------------------------
// Update
//------------------------------------------------------------------------------

void CalibrationUpdate( TRawWeight RawZero, TRawWeight RawRange, TWeightGauge Range)
// Update calibration : ADC value <RawZero> of zero, <RawRange> of physical <Range>
{
   Calibration.Inversion = NO;
   if( RawZero > RawRange){
      // bridge inversion
      RawZero  = -RawZero;
      RawRange = -RawRange;
      Calibration.Inversion = YES;
   }
   Calibration.Offset = RawZero;
   Calibration.Factor = RawRange - RawZero;
   if( Calibration.Factor == 0){
      Calibration.Factor = 1;          // divide by zero prevention
   }
   Calibration.Range  = Range;
} // CalibrationUpdate

//------------------------------------------------------------------------------
// Get range
//------------------------------------------------------------------------------

TWeightGauge CalibrationRangeGet( void)
// Returns physical range
{
   return( Calibration.Range);
} // CalibrationRangeGet

//------------------------------------------------------------------------------
// Inversion
//------------------------------------------------------------------------------

TYesNo CalibrationInversion( void)
// Returns bridge polarity inversion
{
   return( Calibration.Inversion);
} // CalibrationInversion

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

TWeightGauge CalibrationWeight( TRawWeight RawValue)
// Calculate physical weight of <RawValue>
{
TMulWeight tmp;

   RawValue -= Calibration.Offset;
   tmp = (TMulWeight)RawValue * Calibration.Range;
   return((TWeightGauge)(tmp / Calibration.Factor));
} // CalibrationWeight

//------------------------------------------------------------------------------
// Raw Weight
//------------------------------------------------------------------------------

TRawWeight CalibrationRawWeight( TWeightGauge Value)
// Calculate raw weight of physical <Value>
{
TRawWeight RawValue;

   RawValue  = (TRawWeight)(((TMulWeight)Value * Calibration.Factor) / Calibration.Range);
   RawValue += Calibration.Offset;
   return( RawValue);
} // CalibrationRawWeight

//******************************************************************************

//------------------------------------------------------------------------------
// CRC
//------------------------------------------------------------------------------

#define CALIBRATION_DATA_SIZE    (byte)offsetof( TCalibration, Crc)

static TCalibrationCrc CalibrationCrc( void)
// Calculate CRC
{
byte            *p;
TCalibrationCrc Crc;
byte            Size;

   p   = (byte *)&Calibration;
   Crc = 0;
   Size = CALIBRATION_DATA_SIZE;
   do {
      Crc += *p;
      p++;
   } while( --Size);
   return( ~Crc);
} // CalibrationCrc
