//******************************************************************************
//                                                                            
//  Beep.h         Sound processing
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "BeepDef.h"         // tone definitions

//------------------------------------------------------------------------------
void BeepKey( void);
// Key press beep

void BeepError( void);
// Error key press beep

void BeepTimeout( void);
// User inactivity beep

//------------------------------------------------------------------------------

void BeepGreeting( void);
// Power on greeting

void BeepBoot( void);
// Boot power on

void BeepBye( void);
// Power off

void BeepWeighingHeavy( void);
// Weighing heavy done

void BeepWeighingLight( void);
// Weighing light done

void BeepWeighingOk( void);
// Weighing OK done

void BeepWeighingDefault( void);
// Weighing middle done

void BeepCalibration( void);
// Calibration weighing done

void BeepTest( int Volume);
// Test beep volume

void BeepToneTest( int Tone);
// Test <Tone> selection

void BeepRollover( void);
// Keyboard beep - list rollover
