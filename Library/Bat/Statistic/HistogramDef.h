//*****************************************************************************
//
//    HistogramDef.h  Histogram data definition
//    Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __HistogramDef_H__
  #define __HistogramDef_H__

//------------------------------------------------------------------------------
// Histogram
//------------------------------------------------------------------------------

typedef struct 
{
   THistogramValue Average;                 // Average value (true histogram center)
   THistogramValue Center;                  // Histogram center (updated for display)
   THistogramValue Step;                    // Histogram step
   THistogramCount Slot[ HISTOGRAM_SLOTS];
} THistogram;

#endif
