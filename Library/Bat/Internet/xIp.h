//******************************************************************************
//
//  xIp.h          putchar based display IP address
//  Version 1.0    (c) Veit Electronics
//
//******************************************************************************

#ifndef __xIp_H__
   #define __xIp_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PutcharDef_H__
   #include "Console/PutcharDef.h"
#endif

#include "fnet_ip.h"

void xIp( TPutchar *xputchar, fnet_ip4_addr_t Ip);
// Display Ip

void xIpShort( TPutchar *xputchar, fnet_ip4_addr_t Ip);
// Display Ip short

#endif
