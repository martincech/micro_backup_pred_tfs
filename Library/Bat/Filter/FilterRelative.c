//******************************************************************************
//
//   FilterRelative.c Relative weight filtering
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#include "Filter/FilterRelative.h"

TFilterRecord FilterRecord;
static byte   StableCounter;

static TRawWeight Fifo[ FILTER_MAX_AVERAGING];
static byte       FifoPointer;
static TSumWeight FifoSum;

// Local functions :

static void FifoInitialize( TRawWeight Fill);
// Initialize FIFO data

static TRawWeight FifoAverage( void);
// Calculate average of the contents

static void FifoPut( TRawWeight Weight);
// Put to FIFO

//******************************************************************************
// Start
//******************************************************************************

void FilterStart( void)
// Initialize & start filtering
{
   FifoInitialize( FilterRecord.ZeroWeight);
   FilterRecord.RawWeight = FilterRecord.ZeroWeight;
   FilterRecord.LowPass = FilterRecord.ZeroWeight;
   FilterRecord.LastStableWeight = FilterRecord.ZeroWeight;
   FilterRecord.Weight = FilterRecord.ZeroWeight;
   FilterRecord.Status = FILTER_WAIT;
   FilterRecord.Ready  = NO;
   FilterRecord.NotFirstStable  = NO;
   StableCounter = 0;
} // FilterStart

//******************************************************************************
// Stop
//******************************************************************************

void FilterStop( void)
// Stop filtering
{
   FilterRecord.Status = FILTER_STOP;  // clear ready also
   FilterRecord.Ready  = NO;
} // FilterStop

//******************************************************************************
// Sample
//******************************************************************************

void FilterNextSample( TRawWeight Sample)
// Process next sample
{
TRawWeight TmpHighPass;
byte       LastStatus;

   if( FilterRecord.Inversion){
      Sample = -Sample;                                 // reversed bridge polarity
   }
   FilterRecord.RawWeight = Sample;                     // read sample
   FifoPut( Sample);                                    // save sample
   // filtering :
   FilterRecord.LowPass    = FifoAverage();
   FilterRecord.HighPass   = FilterRecord.RawWeight - FilterRecord.LowPass;
   LastStatus              = FilterRecord.Status;       // remember old status
   // check for stability range :
   TmpHighPass = FilterRecord.HighPass;
   if( TmpHighPass < 0){
      TmpHighPass = -TmpHighPass;      // absolute value
   }
   if( TmpHighPass < FilterRecord.StableRange){
      if( StableCounter < FilterRecord.StableWindow){
         StableCounter++;
      } // else saturation
   } else {
      StableCounter = 0;
   }
   // check for stability duration :
   if( StableCounter < FilterRecord.StableWindow){
      // short stable value or unstable
      FilterRecord.Status = FILTER_WAIT;
      FilterRecord.Ready  = NO;
      if( LastStatus == FILTER_STABLE){
         // fall edge of the stable
         FilterRecord.LastStableWeight = FilterRecord.Weight; // remember last weight
      }
      return;
   }
   // stable value
   if( LastStatus == FILTER_WAIT){
      // rise edge of the stable
      FilterRecord.Ready = YES;                  // handshaking
   }
   FilterRecord.Status = FILTER_STABLE;          // stable value
   FilterRecord.Weight = FilterRecord.LowPass;   // set on averaged value
} // FilterNextSample

//******************************************************************************
// Read
//******************************************************************************

TYesNo FilterRead( TRawWeight *Weight)
// Read filtered value
{
   if( !FilterRecord.Ready){
      return( NO);
   }
   FilterRecord.Ready  = NO;                     // handshake
   if( !FilterRecord.NotFirstStable){
      FilterRecord.NotFirstStable = YES;
      return( NO);
   }
   *Weight = FilterRecord.Weight - FilterRecord.LastStableWeight + FilterRecord.ZeroWeight;
   return( YES);
} // FilterRead

//******************************************************************************
// Fifo initialize
//******************************************************************************

static void FifoInitialize( TRawWeight Fill)
// Initialize FIFO data
{
TSamplesCount i;
   FifoSum = 0;
   for( i = 0; i < FilterRecord.AveragingWindow; i++){
      Fifo[ i] = Fill;
      FifoSum += Fill;
   }
   FifoPointer = 0;
} // FifoInitialize

//******************************************************************************
// Fifo average
//******************************************************************************

static TRawWeight FifoAverage( void)
// Calculate average of the contents
{
   return( FifoSum / FilterRecord.AveragingWindow);
} // FifoAverage

//******************************************************************************
// Fifo put
//******************************************************************************

static void FifoPut( TRawWeight Weight)
// Put to FIFO
{
   FifoSum -= Fifo[ FifoPointer];           // remove old value
   Fifo[ FifoPointer++] = Weight;           // save new value
   FifoSum += Weight;                       // add new value
   if( FifoPointer >= FilterRecord.AveragingWindow){
      FifoPointer = 0;
   }
} // FifoPut
