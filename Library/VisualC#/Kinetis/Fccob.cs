//******************************************************************************
//
//    Fccob.cs            Flash common command object over EzPort
//    Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kinetis {
   class Fccob {
      protected Kinetis.Flash Flash;

      public Fccob(Kinetis.Flash _Flash) {
         Flash = _Flash;
      }

      const byte WRFCCOB = 0xBA;
      const byte READFCCOB = 0xBB;

      const byte PGMONCE = 0x43;
      const byte READONCE = 0x41;

      const int PROGRAM_ONCE_SIZE = 64;
      const int PROGRAM_ONCE_FRAGMENT = 4;

      const int WREN = 0x06;

      //-----------------------------------------------------------------------------
      // Program once
      //-----------------------------------------------------------------------------

      public bool ProgramOnce(int Address, byte[] Data)
      // Programs <Address> with <Data>, Address aligned to PROGRAM_ONCE_FRAGMENT
      {
         if(Address % PROGRAM_ONCE_FRAGMENT != 0) {
            return false;
         }
         if(Data.Length == 0) {
            return true;
         }
         int CurrentAddress = Address / PROGRAM_ONCE_FRAGMENT;
         int FragmentLength;
         int Length = Data.Length;
         int DataPtr = 0;
         while(Length != 0) {
            if(CurrentAddress >= PROGRAM_ONCE_SIZE / PROGRAM_ONCE_FRAGMENT) {
               return false;
            }

            if(Length >= PROGRAM_ONCE_FRAGMENT) {
               FragmentLength = PROGRAM_ONCE_FRAGMENT;
            } else {
               FragmentLength = Length;
            }

            byte[] Cmd = new byte[] { PGMONCE, (byte)CurrentAddress, 0, 0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

            for(int i = 0; i < FragmentLength; i++) {
               Cmd[i + 4] = Data[DataPtr++];
            }

            if(!SendCmd(Cmd)) {
               return false;
            }

            CurrentAddress++;
            Length -= FragmentLength;
         }
         return true;
      } // ProgramOnce

      //-----------------------------------------------------------------------------
      // Read once
      //-----------------------------------------------------------------------------

      public bool ReadOnce(int Address, ref byte[] Data)
         // Reads <Address>
      {
         int Length = Data.Length;
         if(Length == 0) {
            return true;
         }

         int IgnoreCount = Address % PROGRAM_ONCE_FRAGMENT;
         int FragmentLength;
         int DataPtr = 0;
         while(Length != 0) {
            if(Address >= PROGRAM_ONCE_SIZE) {
               return false;
            }

            byte[] Cmd = new byte[] { READONCE, (byte)(Address / PROGRAM_ONCE_FRAGMENT) };

            if(!SendCmd(Cmd)) {
               return false;
            }

            byte[] Reply = new byte[4 + PROGRAM_ONCE_FRAGMENT];

            if(!ReceiveReply(Reply)) {
               return false;
            }

            if(Reply[0] != READONCE) {
               return false;
            }

            if(Length >= PROGRAM_ONCE_FRAGMENT) {
               FragmentLength = PROGRAM_ONCE_FRAGMENT;
            } else {
               FragmentLength = Length;
            }

            if(4 - IgnoreCount < FragmentLength) {
               FragmentLength = PROGRAM_ONCE_FRAGMENT - IgnoreCount;
            }

            Buffer.BlockCopy(Reply, 4 + IgnoreCount, Data, DataPtr, FragmentLength);

            Length -= FragmentLength;
            DataPtr += FragmentLength;
            IgnoreCount = 0;

            Address += PROGRAM_ONCE_FRAGMENT;
         }
         return true;
      } // ReadOnce

      //*****************************************************************************

      //-----------------------------------------------------------------------------
      // Send FCCOB command via EzPort
      //-----------------------------------------------------------------------------

      protected bool SendCmd(byte[] Cmd)
         // Write FCCOB
      {
         byte[] EzPortCmd;

         if(Cmd.Length > 12) {
            return false;
         }

         EzPortCmd = Enumerable.Repeat((byte)0xFF, 13).ToArray();
         Buffer.BlockCopy(Cmd, 0, EzPortCmd, 1, Cmd.Length);
         EzPortCmd[0] = WRFCCOB;

         while(Flash.Busy) ;
         Flash.EzPort.transfer(new byte[] { WREN }, null, Suikan.JtagKey.LatchOn.Rising);
         Flash.EzPort.transfer(EzPortCmd, null, Suikan.JtagKey.LatchOn.Rising);
         
         return true;
      } // SendCmd

      //-----------------------------------------------------------------------------
      // Read FCCOB via EzPort
      //-----------------------------------------------------------------------------

      protected bool ReceiveReply(byte[] Reply)
         // Read FCCOB
      {
         byte[] EzPortCmd = new byte[] { READFCCOB, 0xFF }; // Dummy 
         byte[] EzPortReply = new byte[Reply.Length + 2];
         if(Reply.Length == 0) {
            return false;
         }

         if(Reply.Length > 12) {
            return false;
         }

         while(Flash.Busy) ;
         Flash.EzPort.transfer(EzPortCmd, EzPortReply, Suikan.JtagKey.LatchOn.Rising);
         Buffer.BlockCopy(EzPortReply, 2, Reply, 0, Reply.Length);
         return true;
      } // ReceiveReply
   }
}