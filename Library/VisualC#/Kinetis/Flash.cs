//******************************************************************************
//
//    KinetisFlash.cs     Kinetis flash
//    Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kinetis {
   class Flash : Suikan.RomWriter.SpiFlash {
      //-----------------------------------------------------------------------------
      // Initialisation
      //-----------------------------------------------------------------------------

      public virtual void Initialize(Suikan.JtagKey.EzPort EzPort, TextBox aTextBox, ProgressBar aProgressBar) {
         _EzPort = EzPort;
         _Fccob = new Fccob(this);

         base.Initialize(EzPort, aTextBox, aProgressBar);
      } // Initialize

      //-----------------------------------------------------------------------------
      // Busy
      //-----------------------------------------------------------------------------

      public bool Busy {
         get {
            return base.IsBusy;
         }
      } // Busy

      //-----------------------------------------------------------------------------
      // Fccob
      //-----------------------------------------------------------------------------

      private Fccob _Fccob;
      public Fccob Fccob {
         get {
            return _Fccob;
         }
      } // _Fccob

      //-----------------------------------------------------------------------------
      // EzPort
      //-----------------------------------------------------------------------------

      private Suikan.JtagKey.EzPort _EzPort;
      public Suikan.JtagKey.EzPort EzPort {
         get {
            return _EzPort;
         }
      } // _EzPort
   }
}