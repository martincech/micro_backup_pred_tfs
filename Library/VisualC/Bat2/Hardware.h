//*****************************************************************************
//
//    Hardware.h  Bat2 compact PC hardware definitions
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Unisys/Uni.h"

#define BAT2_HW_VERSION DeviceVersionSet(1, 0, 0)
#define OPTION_SIGMA_DELTA

//-----------------------------------------------------------------------------
// Nonvolatile memory
//-----------------------------------------------------------------------------

#define NVM_SIZE   (8 * 1024 * 1024)
#define FLASH_SIZE   NVM_SIZE

#include "Country/Country.h"

//-----------------------------------------------------------------------------
// Keyboard
//-----------------------------------------------------------------------------

// keyboard/event codes :
#define _K_SYSTEM_BASE            1    // skip K_NULL
#include "System/SystemKey.h"          // system events

#define _K_KEYBOARD_BASE          _K_SYSTEM_LAST
#include "Kbd/KbxKey.h"                // keyboard and user events

#define _K_USB_BASE               _K_KEY_LAST
#include "Usb/UsbKey.h"

//-----------------------------------------------------------------------------
// Samples FIFO
//-----------------------------------------------------------------------------

#define FIFO_SIZE       65536            // samples FIFO capacity

//-----------------------------------------------------------------------------
// Diagnostic
//-----------------------------------------------------------------------------

#define DIAGNOSTIC_FRAME_COUNT  10    // Frame FIFO capacity

//-----------------------------------------------------------------------------
// Calibration
//-----------------------------------------------------------------------------

#include "Platform/PlatformDef.h"

#define CALIBRATION_POINTS_MAX   PLATFORM_CALIBRATION_POINTS_MAX

#define RS485_0_PHYSICAL_DEVICE 0
#define RS485_1_PHYSICAL_DEVICE 0

#define MY_VID 0xA600
#define MY_PID 0xE2A0 

#endif
