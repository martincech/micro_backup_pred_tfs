﻿using System.Windows;
using BatClientApp.ViewModel;

namespace BatClientApp.View
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow
   {
      public MainWindow()
      {
         InitializeComponent();
         //Closing += (s, e) => ViewModelLocator.Cleanup();
      }
   }
}
