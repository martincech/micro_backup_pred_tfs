//******************************************************************************
//
//   HidStdio.h      Hid stdio
//   Version 1.0     (c) Veit Electronics
//
//******************************************************************************

#ifndef __THidStdio_H__
	#define __THidStdio_H__

#include "Usb/HidNative/HidNative.h"

#define BUFFER_SIZE     64

//******************************************************************************
// THid
//******************************************************************************

class THidStdio {
public :
   THidStdio();
   // Constructor

   ~THidStdio();
   // Destructor

   int putchar(int ch);

   int kbhit( void);

   int getchar(const byte *Firmware, int Size);
   // Verify <Firmware> of <Size>

//---------------------------------------------------------------------------
protected :
   THidNative *HidNative;

   char BufferTx[BUFFER_SIZE];
   int BufferTxPtr;

   char BufferHid[BUFFER_SIZE];

   char BufferRx[BUFFER_SIZE];
   int BufferRxPtr;
   int BufferRxSize;
}; // TFwUpgrader

#endif

