//******************************************************************************
//
//   CrtDump.h    CRT based data dump
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef CRTDUMP_H
#define CRTDUMP_H

#include "Unisys/Uni.h"
#include "Crt/Crt.h"

//------------------------------------------------------------------------------
//  CRT dump
//------------------------------------------------------------------------------

class CrtDump
{
public:
   typedef enum {
      BINARY,
      TEXT,
      MIXED
   } DumpMode;

   typedef enum {
      NO_TYPE,
      RX,
      TX,
      GARBAGE            // RX garbage
   } DumpType;

   CrtDump( Crt *crt);

   void setMode( DumpMode mode);
   // set visual display mode

   void setEnable( bool enable);
   // enable/disable dump

   void setEnableOffset( bool enableOffset);
   // enable/disable leftmost data offset column
   void setEnableHeader( bool enableHeader);
   // enable/disable rx/tx text
   void puts( const char *string);
   // print string

   void printf( const char *format, ...);
   // formatted output

   void show( DumpType type, const void *data, int size);
   // show binary <data> with <size> with <type> title

//------------------------------------------------------------------------------

   // internal functions :
   void binary( const void *data, int size);
   // show binary dump
   void text( const void *data, int size);
   // show text dump
   void mixed( const void *data, int size);
   // show mixed dump

   void binaryLine( const void *data, int size);
   // single binary line
   void textLine( const void *data, int size);
   // single text line

//------------------------------------------------------------------------------
protected :
   DumpMode _dumpMode;
   bool     _enable;
   bool     _enableOffset;
   bool     _enableHeader;
   Crt      *_crt;
}; // CrtDump

#endif // CRTDUMP_H
