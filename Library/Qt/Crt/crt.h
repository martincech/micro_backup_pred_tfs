//******************************************************************************
//
//   Crt.h        CRT simple terminal window
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef CRT_H
#define CRT_H

#include <QPlainTextEdit>
#include <stdarg.h>
#ifdef __UDEBUG__
#include <QFile>
#include <QTextStream>
#endif
//------------------------------------------------------------------------------
//  CRT
//------------------------------------------------------------------------------

class Crt : public QPlainTextEdit
{
   Q_OBJECT
public:
   explicit Crt(QWidget *parent = 0);
   ~Crt();
   void setMaximumRows( int Rows);
   int maximumRows();

   void clear();
   // base C characters :
   void putch( const char ch);
   void puts( const char *string);
   void printf( const char *format, ...);
   void vprintf( const char *format, va_list arg);

   // Qt characters :
   void putch( QChar ch);
   void puts( QString string);

signals :
   void addText( QString);

#ifdef __UDEBUG__
private slots:
   void appendToFile(QString);
private :
   QFile ofile;
   QTextStream *ostream;
#endif

private :
   int _maximumRows;
}; // Crt

#endif // CRT_H
