//*****************************************************************************
//
//   MainWindow.cpp  Device Simulator main window
//   Version 1.0     (c) VymOs
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "devicepanel.h"
#include "Uart/UartModem.h"
#include "Message/GsmMessage.h"
#include "Multitasking/Multitasking.h"
#include "Debug/uDebug.h"
#include <QMainWindow>
#include <QCloseEvent>
#include <QMessageBox>
#include <QThread>
#include <QtConcurrentRun>
const QString MainWindow::SETTINGS_FILE = QString("settings.dat");

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
   ui->setupUi(this);                  // initialize widgets
   cleanedUp = false;
   _uart = NULL;
   //status bar
   UartParamsDialog = NULL;
   ui->statusBar->addPermanentWidget(&_status);
   _uart = initUartFromFile();
   if(_uart == NULL || !_uart->checkOpen()){
      if(_uart != NULL){
         delete _uart;
      }
      _uart = NULL;
   }
   connect(this, SIGNAL(updateModemStatus()), this, SLOT(setModemStatus()));
   connect(this, SIGNAL(closeSignal()), this, SLOT(close()));
   setupModem();
} // MainWindow


//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------

MainWindow::~MainWindow()
{
   while(qApp->hasPendingEvents()){
      qApp->processEvents();
   }
   delete ui;
} // ~MainWindow

//-----------------------------------------------------------------------------
// Close window
//-----------------------------------------------------------------------------
#include <QtConcurrentRun>
#include "sleepthread.h"

void MainWindow::waitForCleanup()
{
   SleepThread::sleep(0.5);
   this->cleanedUp = true;
   emit closeSignal();
}

void MainWindow::closeEvent( QCloseEvent *event)
{
   ui->devicePanel->powerOffSwitch();
   if(cleanedUp){
      TRACE( "closeEvent accepted");
      event->accept();
      qApp->exit(1);
   }else{
      TRACE( "closeEvent waiting for clean");
      QtConcurrent::run(this, &MainWindow::waitForCleanup);
      event->ignore();
      hide();

   }
} // closeEvent

//-----------------------------------------------------------------------------
// Exit
//-----------------------------------------------------------------------------

void MainWindow::on_actionExit_triggered()
{
   TRACE( "Exit received");
   close();                            // close main window
} // on_actionExit_triggered()

//-----------------------------------------------------------------------------
// Copy
//-----------------------------------------------------------------------------
WinUart *MainWindow::initUartFromFile(){
   if(!QFile::exists(SETTINGS_FILE)){return NULL;}
   WinUart *uart = new WinUart();

   QFile file(SETTINGS_FILE);
   file.open(QIODevice::ReadOnly);
   QDataStream in(&file);   // we will serialize the data into the file
   in >> uart;
   file.close();
   return uart;
}

void MainWindow::on_actionCopy_triggered()
{
   ui->devicePanel->copy();
} // on_actionCopy_triggered

void MainWindow::on_actionGSM_modem_triggered()
{
   ui->devicePanel->powerOffSwitch();
   UartParamsDialog;
   WinUart *uart = initUartFromFile();
   if(uart == NULL){
      UartParamsDialog = new UartParameters(this);
      UartParamsDialog->setPortName("COM43");
      UartParamsDialog->setFlowControl(WinUart::FLOW_RTS_CTS);
      UartParamsDialog->setBaudRate(115200);
   } else {
      UartParamsDialog = new UartParameters(uart, this);
      delete uart;
   }

   connect(UartParamsDialog, SIGNAL(accepted()), this, SLOT(UartConnected()));
   UartParamsDialog->execute();
   disconnect(UartParamsDialog, SIGNAL(accepted()), this, SLOT(UartConnected()));
   UartParamsDialog->deleteLater();
   UartParamsDialog = NULL;
   ui->devicePanel->powerOnSwitch();
}


void MainWindow::UartConnected()
{

   WinUart *uart = new WinUart();
   UartParamsDialog->toUart(uart);
   if( _uart != NULL){
      _uart->disconnect();
      delete _uart;
      _uart = NULL;
   }
   _uart = uart;
   if(!(_uart->checkOpen())){
      QMessageBox::critical(this, tr("Error"),
                            tr("Can't connect"));
      delete _uart;
      _uart = NULL;
      return;
   }

   try{
      QFile file(SETTINGS_FILE);
      file.open(QIODevice::WriteOnly);
      QDataStream out(&file);   // we will serialize the data into the file
      out << _uart;
      file.close();
   }catch(std::exception ex){

   }
   setupModem();
}

void MainWindow::setupModem()
{
   UartModemSetup( _uart, NULL);
   emit updateModemStatus();
}

void MainWindow::setModemStatus()
{
   if(_uart == NULL){
      _status.setText("Modem: unconnected");
   } else {
      UartParameters *tmpParams = new UartParameters(_uart, this);
      _status.setText("Modem: " + tmpParams->toString());
      delete tmpParams;
   }
}
