//******************************************************************************
//
//   SchedulerDummy.c Basic Scheduler
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include "Hardware.h"
#include "System/System.h"

//******************************************************************************
// Scheduler
//******************************************************************************

int SysScheduler( void)
// Scheduler cycle, returns any event
{
   return( SysSchedulerDefault());        // default scheduler only
} // SysScheduler
