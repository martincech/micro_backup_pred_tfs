//*****************************************************************************
//
//   MainWindow.cpp  Device Simulator main window
//   Version 1.0     (c) VymOs
//
//*****************************************************************************

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include "Uart/WinUart.h"
#include "Uart/UartParameters.h"

namespace Ui {
   class MainWindow;
}

//-----------------------------------------------------------------------------
// Main window
//-----------------------------------------------------------------------------

class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();

protected:
   void closeEvent( QCloseEvent *event);

private slots:
   void UartConnected();

   void on_actionExit_triggered();

   void on_actionCopy_triggered();

   void on_actionGSM_modem_triggered();
   void setModemStatus();
signals:
   void updateModemStatus();
   void closeSignal();
private:
    Ui::MainWindow *ui;
    QLabel  _status;
    UartParameters *UartParamsDialog;
    WinUart *_uart;
   bool cleanedUp;
   void waitForCleanup();
   static const QString SETTINGS_FILE;

   WinUart *initUartFromFile();
   void setupModem();

}; // MainWindow

#endif // MAINWINDOW_H
