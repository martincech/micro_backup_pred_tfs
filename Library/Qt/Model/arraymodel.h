//******************************************************************************
//
//    ArrayModel.h Array model
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef ARRAYMODEL_H
#define ARRAYMODEL_H

#include <QStandardItemModel>
#include <QStringList>

//------------------------------------------------------------------------------
//  Array Model
//------------------------------------------------------------------------------

class ArrayModel : public QStandardItemModel
{
    Q_OBJECT

public :
   ArrayModel( int rows, int columns);
   // allocate array with <rows> and <columns> count

   void setRowCount( int rows){setRowsCount(rows);}
   // overloaded method
   void setRowsCount( int rows);
   // reallocate array for new <rows> count

   void clear();
   // clear array contents (without reallocation)

   void setTitle( int column, QString text);
   // set <column> title to <text>

   void setItem( int row, int column, QString text);
   // set item at <row><column> to <text>

   QString getItem( int row, int column);
   // returns item at <row><column>

   void setRow( int row, const QStringList &items);
   // set <row> with <items>

   void addRow( const QStringList &items);
   // append row with <items>

   int getRowsCount();
   // returns number of rows

   int getColumnsCount();
   // returns number of columns
private :
   int _rows;
   int _columns;
}; // ArrayModel

//------------------------------------------------------------------------------
//  Array Model Proxy
//------------------------------------------------------------------------------

class ArrayModelPtr
{
public:
   ArrayModelPtr(ArrayModel *a):_a(a){}
   ~ArrayModelPtr(){
      delete _a;
   }

   ArrayModel *operator->(){return _a;}
   ArrayModel &operator*(){ return *_a;}
private:
   ArrayModelPtr(const ArrayModelPtr&);
   ArrayModelPtr& operator=(const ArrayModelPtr&);
private:
      ArrayModel *_a;
};
#endif
