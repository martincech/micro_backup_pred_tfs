//******************************************************************************
//
//    ArrayModel.cpp Array model
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "ArrayModel.h"

#define rowOk( r)        (((r) >= 0) && ((r) < _rows))
#define columnOk( c)     (((c) >= 0) && ((c) < _columns))
#define indexOk( r, c)   (rowOk( r) && columnOk( c))
#define itemIndex( r, c) ((r) * (_columns) + (c))

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

ArrayModel::ArrayModel( int rows, int columns) : QStandardItemModel()
// allocate array with <rows> and <columns> count
{
   _rows    = 0;
   _columns = columns;
   for( int column = 0; column < _columns; column++){
      QStandardItemModel::setHorizontalHeaderItem( column, new QStandardItem);
   }
   setRowsCount( rows);
} // ArrayModel

//------------------------------------------------------------------------------
//  realloc
//------------------------------------------------------------------------------

void ArrayModel::setRowsCount( int rows)
// reallocate array for new <rows> count
{
   if( rows == _rows){
      return;
   }
   QStandardItemModel::setRowCount( rows);
   if( rows < _rows){
      _rows = rows;
      return;
   } // else rows > _rows
   QStandardItem *item;
   for( int row = _rows; row < rows; row++){
      for( int column = 0; column < _columns; column++){
         item = new QStandardItem;
         QStandardItemModel::setItem( row, column, item);
      }
   }
   _rows = rows;
} // setRowsCount

//------------------------------------------------------------------------------
//  clear
//------------------------------------------------------------------------------

void ArrayModel::clear()
// clear array contents (without reallocation)
{
   for( int row = 0; row < _rows; row++){
      for( int column = 0; column < _columns; column++){
         QStandardItemModel::item( row, column)->setText( QString());
      }
   }
} // clear

//------------------------------------------------------------------------------
//  Set title
//------------------------------------------------------------------------------

void ArrayModel::setTitle( int column, QString text)
// set <column> title to <text>
{
   if( !columnOk( column)){
      return;
   }
   QFont titleFont = QStandardItemModel::horizontalHeaderItem( column)->font();
   titleFont.setWeight( QFont::Bold);
   QStandardItemModel::horizontalHeaderItem( column)->setFont( titleFont);
   QStandardItemModel::horizontalHeaderItem( column)->setText( text);
} // setTitle

//------------------------------------------------------------------------------
//  Set item
//------------------------------------------------------------------------------

void ArrayModel::setItem( int row, int column, QString text)
// set item at <row><column> to <text>
{
   if( !indexOk( row, column)){
      return;
   }
   QStandardItemModel::item( row, column)->setText( text);
} // setItem

//------------------------------------------------------------------------------
//  Get item
//------------------------------------------------------------------------------

QString ArrayModel::getItem( int row, int column)
// returns item at <row><column>
{
   if( !indexOk( row, column)){
      return QString("");
   }
   return( QStandardItemModel::item( row, column)->text());
} // getItem

//------------------------------------------------------------------------------
//  Set row
//------------------------------------------------------------------------------

void ArrayModel::setRow( int row, const QStringList &items)
// set <row> with <items>
{
   // get columns count :
   int columns = _columns;
   if( items.size() < _columns){
      columns = items.size();
   }
   // clear items :
   for( int column = 0; column < _columns; column++){
      QStandardItemModel::item( row, column)->setText( QString());
   }
   // copy items :
   for( int column = 0; column < columns; column++){
      QStandardItemModel::item( row, column)->setText( items.at( column));
   }
} // setRow

//------------------------------------------------------------------------------
//  Add row
//------------------------------------------------------------------------------

void ArrayModel::addRow( const QStringList &items)
// append row with <items>
{
   int row = _rows;                    // new row index
   setRowsCount( _rows + 1);           // allocate new row
   setRow( row, items);                // fill with values
} // addRow

//------------------------------------------------------------------------------
//  Rows count
//------------------------------------------------------------------------------

int ArrayModel::getRowsCount()
// returns number of rows
{
   return( _rows);
} // getRowsCount

//------------------------------------------------------------------------------
//  Colums count
//------------------------------------------------------------------------------

int ArrayModel::getColumnsCount()
// returns number of columns
{
   return( _columns);
} // getColumnsCount
