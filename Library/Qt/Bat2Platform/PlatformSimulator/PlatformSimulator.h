//******************************************************************************
//
//   MainWindow.h    Bat2Platform simulator window
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef PlatformSimulator_H
#define PlatformSimulator_H

#include <QMainWindow>
#include <QTimer>
#include "Samples.h"

namespace Ui {
   class PlatformSimulator;
}

class PlatformSimulator : public QMainWindow
{
   Q_OBJECT
   
public:
   explicit PlatformSimulator(QWidget *parent = 0);
   ~PlatformSimulator();
   
private slots:
   void on_actionOpen_triggered();
   void on_actionFixed_triggered();

   void on_playbackSpeed_currentIndexChanged( int index);

   void on_samplesGauge_sliderMoved(int position);
   void on_samplesGauge_sliderReleased();
   void on_samplesGauge_sliderPressed();

   void on_actionPlay_triggered();
   void on_actionPause_triggered();
   void on_actionStop_triggered();
   void on_actionFastBackward_triggered();
   void on_actionBackward_triggered();
   void on_actionForward_triggered();
   void on_actionFastForward_triggered();

   void on_buttonSet_clicked();

   void timerSlowTimeout();

private:
   void updateTime( int index);
   QString samplesToTime( int index);

   Ui::PlatformSimulator *ui;
   QTimer         *_timerSlow;
   Samples        *_samples;
   bool            _playback;
   bool            _suspended;
};

#endif // PlatformSimulator_H
