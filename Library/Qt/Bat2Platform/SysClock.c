//*****************************************************************************
//
//   SysClock.c   System date/time utility
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "System/System.h"
#include "Time/uClock.h"
#include <windows.h>               // Sleep only
#include <time.h>

// fake implementation for Bat2Platform simulator

dword _Clock;                      // clock driven by samples

//-----------------------------------------------------------------------------
// Clock
//-----------------------------------------------------------------------------

dword SysClock( void)
// Returns actual date/time
{
   return( _Clock);
} // SysClock

void SysClockSet( dword DateTime)
// Set actual date/time
{
   // disable external clock synchronization by Wepl
} // SysClockSet

//-----------------------------------------------------------------------------
// Date time
//-----------------------------------------------------------------------------

dword SysDateTime( void)
// Returns wall clock date/time
{
   return( _Clock);
} // SysDateTime

void SysDateTimeSet( dword DateTime)
// Set wall clock <DataTime>
{
   _Clock = DateTime;                  // sychronize by sample time
} // SysDateTimeSet

TYesNo SysDateTimeInvalid( void)
// Has system invalid time?
{
   return NO;
} // SysDateTimeInvalid

//-----------------------------------------------------------------------------
// Delay
//-----------------------------------------------------------------------------

void SysDelay( word ms)
// Delay loop [ms]
{
   Sleep( ms);             // Win32 API
} // SysDelay

void SysUDelay( word us)
// Delay loop [us]
{
   Sleep( 1);              // short delay
} // SysUDelay
