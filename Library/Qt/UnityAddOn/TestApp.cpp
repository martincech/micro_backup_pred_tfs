#include "TestApp.h"
#include "Test/TestRunner.h"

TestApp::TestApp( int argc, char *argv[]) : QCoreApplication(argc, argv) {

}

int TestApp::exec( void) {
   UnityMain(0, 0, TestRunner);
}

int main(int argc, char *argv[])
{
   TestApp a(argc, argv);

   return a.exec();
}
