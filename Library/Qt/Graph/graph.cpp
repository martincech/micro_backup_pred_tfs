//******************************************************************************
//
//    Graph.cpp    Basic graph
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Graph.h"
#include <qwt_symbol.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_text.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_rescaler.h>

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

Graph::Graph( QWidget *parent):
    QwtPlot( parent)
{
    setAutoReplot( false);

    (void) new QwtPlotPanner( canvas());     // panning with the left mouse button
    (void) new QwtPlotMagnifier( canvas());  // zoom in/out with the wheel
    setAutoFillBackground( true);

    setTitle("Title");
    setCanvasBackground( QColor( Qt::gray));

    // legend
    QwtLegend *legend = new QwtLegend;
    insertLegend( legend, QwtPlot::RightLegend);

    // grid :
    QwtPlotGrid *grid = new QwtPlotGrid;
    grid->enableXMin( true);
    grid->enableYMin( true);
    grid->setMajPen( QPen( Qt::white,    0, Qt::DotLine));
    grid->setMinPen( QPen( Qt::darkGray, 0, Qt::DotLine));
    grid->attach( this);

    // axes :
    setAxisTitle( QwtPlot::xBottom, "X axis");
    setAxisTitle( QwtPlot::yLeft, "Y axis");

    setAxisMaxMajor( QwtPlot::xBottom, 6);
    setAxisMaxMinor( QwtPlot::xBottom, 10);

    // canvas :
    canvas()->setLineWidth( 1 );
    canvas()->setFrameStyle( QFrame::Panel | QFrame::Sunken);
    canvas()->setBorderRadius( 1);     // must be set >= 1 for panning

    QPalette canvasPalette( Qt::gray);
    canvasPalette.setColor( QPalette::Foreground, Qt::darkGray);
    canvas()->setPalette( canvasPalette);

    // rescale :
/*
    QwtPlotRescaler *rescaler = new QwtPlotRescaler( canvas());
    rescaler->setReferenceAxis( QwtPlot::xBottom);
    rescaler->setAspectRatio( QwtPlot::yLeft, 1.0);
    rescaler->setAspectRatio( QwtPlot::yRight, 0.0);
    rescaler->setAspectRatio( QwtPlot::xTop, 0.0);
    rescaler->setRescalePolicy( QwtPlotRescaler::Expanding);
*/
    setAutoReplot( true);
} // Graph

//------------------------------------------------------------------------------
//  New curve
//------------------------------------------------------------------------------

QwtPlotCurve *Graph::newCurve( const char *title, const QColor color)
{
   QwtPlotCurve *curve;
   curve = new QwtPlotCurve( title);
   curve->setRenderHint( QwtPlotItem::RenderAntialiased);
   curve->setPen( QPen( color));
   curve->setLegendAttribute( QwtPlotCurve::LegendShowLine);
   curve->setYAxis( QwtPlot::yLeft);
   curve->attach( this);
   return( curve);
} // newCurve

//------------------------------------------------------------------------------
//  Move X
//------------------------------------------------------------------------------

void Graph::moveX( double offset, double width)
// move graph horizontally, start at <offset> with window <width>
{
   setAxisScale( QwtPlot::xBottom, offset, offset + width);
} // moveX

//------------------------------------------------------------------------------
//  Move Y
//------------------------------------------------------------------------------

void Graph::moveY( double offset, double height)
// move graph vertically, start at <offset> with window <height>
{
   setAxisScale( QwtPlot::yLeft, offset, offset + height);
} // moveY

//------------------------------------------------------------------------------
//  Fit graph
//------------------------------------------------------------------------------

void Graph::fit()
// autoscale graph to fit all curves
{
   setAxisAutoScale( QwtPlot::xBottom, true);
   setAxisAutoScale( QwtPlot::yLeft, true);
} // fit
