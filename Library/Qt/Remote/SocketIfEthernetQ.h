//******************************************************************************
//
//   SocketIfEthernet.h    Ethernet socket
//   Version 1.0           (c) Veit Electronics
//
//******************************************************************************

#ifndef _SocketIfEthernetQ_H__
   #define _SocketIfEthernetQ_H__

#include "SocketIf.h"
#include "Socket/TcpRawClient.h"
#ifdef __WIN32__
   #include "Crt/crtdump.h"
#endif
class SocketIfEthernet : virtual public SocketIf, public TcpRawServer
{
public :
   SocketIfEthernet();
#ifdef __WIN32__
   SocketIfEthernet(CrtDump *logger);
#endif
   // Constructor
   
   virtual ~SocketIfEthernet();
   // Destructor
   
   byte State( void);
   // Gets state of socket

   TYesNo Receive( void *Buffer, int Size);
   // Receive into <Buffer> with <Size>

   int ReceiveSize( void);
   // Gets number of received bytes

   TYesNo Send( const void *Buffer, int Size);
   // Send <Buffer> with <Size>

   void Close( void);
   // Close socket

   byte Permission( void);
   //
private:
   byte _State;
   int ReplySize;
};

#endif // _SocketIfEthernet_H__
