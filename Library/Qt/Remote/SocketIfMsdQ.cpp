//******************************************************************************
//
//   SocketIfBootloaderMsd.h         MSD socket
//   Version 1.0           (c) Veit Electronics
//
//******************************************************************************

#include "Remote/SocketIfBootloaderMsd.h"
#include "SocketIfMsdQ.h"
#include "File/Efs.h"
#include "Remote/Frame.h"
#include <stdio.h>

//******************************************************************************
// Constructor
//******************************************************************************

SocketIfMsd::SocketIfMsd( void)
{
}

//******************************************************************************
// Destructor
//******************************************************************************

SocketIfMsd::~SocketIfMsd()
// Destructor
{
}

TYesNo SocketIfMsd::OpenForReceiveCmd( void)
// Open socket for cmd receive
{
    return SocketIfBootloaderMsdOpenForReceiveCmd();
}

TYesNo SocketIfMsd::OpenForSendCmd( void)
// Open socket for send cmd
{
    return SocketIfBootloaderMsdOpenForSendCmd(0);
}

byte SocketIfMsd::State( void)
// Gets state of socket
{
   return SocketIfBootloaderMsdState(0);
}

TYesNo SocketIfMsd::Receive( void *Buffer, int Size)
// Receive into <Buffer> with <Size>
{
   return SocketIfBootloaderMsdReceive(0, Buffer, Size);
}

int SocketIfMsd::ReceiveSize( void)
// Gets number of received bytes
{
   return SocketIfBootloaderMsdReceiveSize(0);
}

TYesNo SocketIfMsd::Send( const void *Buffer, int Size)
// Send <Buffer> with <Size>
{
   return SocketIfBootloaderMsdSend(0, Buffer, Size);
}

void SocketIfMsd::Close( void)
// Close socket
{
    SocketIfBootloaderMsdClose(0);
}

byte SocketIfMsd::Permission( void)
//
{
   return SocketIfBootloaderMsdPermission(0);
}
