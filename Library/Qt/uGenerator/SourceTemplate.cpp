//******************************************************************************
//
//   SourceTemplate.cpp  Source code template
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "SourceTemplate.h"
#include "Parse/TextFile.h"
#include <QRegExp>
#include <QDir>
//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

SourceTemplate::SourceTemplate()
{
   _text = QString();
} // SourceTemplate

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

SourceTemplate::SourceTemplate( QString fileName)
{
   setFileName( fileName);
} // TextFile

//------------------------------------------------------------------------------
//  File name
//------------------------------------------------------------------------------

void SourceTemplate::setFileName( QString fileName)
// set new <fileName>
{
   QDir dir;
   QString fname = dir.absoluteFilePath(fileName);
   TextFile file( fname);
   _text = file.load();
} // setFileName

//------------------------------------------------------------------------------
//  Parameter
//------------------------------------------------------------------------------

bool SourceTemplate::setParameter( QString name, QString value)
// search for <Name>, replace it with <Value>
{
   name = QString( "$") + name + QString( "$");  // macro name
   QString newText = _text;
   newText.replace( name, value, Qt::CaseSensitive);
   if( newText == _text){
      return( false);                            // no occurence found
   }
   _text = newText;
   return( true);
} // setParameter

//------------------------------------------------------------------------------
//  To string
//------------------------------------------------------------------------------

const QString SourceTemplate::toString()
// returns actual text
{
   //replace every keyword by empty string and return this text
   QString newText = _text;
   newText.replace( QRegExp("\\$[^\\$]*\\$"), "");
   return( newText);
} // toString
