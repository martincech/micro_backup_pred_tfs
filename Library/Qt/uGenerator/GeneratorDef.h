//******************************************************************************
//
//   GeneratorDef.h  Micro code generator definitions
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef UGENERATORDEF_H
   #define UGENERATORDEF_H

#include <QString>
#include <QVector>
#include "Parse/Enum.h"

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define GENERATOR_TEMPLATE_PATH  "Template/"

#define OBJECT_COLUMNS_MAX      12     // object columns
#define EMBEDDED_COLUMNS_MAX    12     // embedded data columns
#define DICTIONARY_COLUMNS_MAX   4     // dictionary columns

//------------------------------------------------------------------------------
//  Keywords
//------------------------------------------------------------------------------

#define KEYWORD_TITLE        "title"
#define KEYWORD_PREFIX       "prefix"
#define KEYWORD_MENU_PREFIX  "mprefix"
#define KEYWORD_FILE         "file"
#define KEYWORD_OBJECT       "Object"
#define KEYWORD_NAME         "Name"        // embedded data only
#define KEYWORD_MENU         "menu"
#define KEYWORD_DATA         "data"
#define KEYWORD_GLOBAL       "global"

//------------------------------------------------------------------------------
//  Dictionary item type
//------------------------------------------------------------------------------

typedef enum {
   DICTIONARY_TYPE_UNDEFINED,
   DICTIONARY_TYPE_ENUM,
   DICTIONARY_TYPE_GLOBAL,
   DICTIONARY_TYPE_BYTE,
   DICTIONARY_TYPE_WORD,
   DICTIONARY_TYPE_DWORD,
   _DICTIONARY_TYPE_LAST
} EDictionaryType;

#define ENUM_DICTIONARY_TYPE \
   "undefined\0"\
   "enum\0"\
   "global\0"\
   "byte\0"\
   "word\0"\
   "dword\0"

//------------------------------------------------------------------------------
//  Data type
//------------------------------------------------------------------------------

typedef enum {
   DATA_TYPE_UNDEFINED,
   DATA_TYPE_CUSTOM,
   DATA_TYPE_STRUCT,
   DATA_TYPE_ALIAS,
   DATA_TYPE_MENU,
   DATA_TYPE_METHOD,
   _DATA_TYPE_LAST
} EDataType;

#define ENUM_DATA_TYPE \
   "undefined\0"\
   "custom\0"\
   "struct\0"\
   "alias\0"\
   "menu\0"\
   "method\0"

//------------------------------------------------------------------------------
//  Data class
//------------------------------------------------------------------------------

typedef enum {
   CLASS_TYPE_UNDEFINED,
   CLASS_TYPE_YESNO,
   CLASS_TYPE_UINT,
   CLASS_TYPE_INT,
   CLASS_TYPE_FIXED,
   CLASS_TYPE_FLOAT,
   CLASS_TYPE_CHAR,
   CLASS_TYPE_STRING,
   CLASS_TYPE_PASSWORD,
   CLASS_TYPE_IP,
   CLASS_TYPE_ARRAY,
   CLASS_TYPE_ENUM,
   CLASS_TYPE_EMBEDDED,
   CLASS_TYPE_CUSTOM,
   CLASS_TYPE_SPARE,
   _CLASS_TYPE_LAST
} EClassType;

#define ENUM_CLASS_TYPE \
   "undefined\0"\
   "YesNo\0"\
   "uint\0"\
   "int\0"\
   "fixed\0"\
   "float\0"\
   "char\0"\
   "string\0"\
   "password\0"\
   "IP\0"\
   "array\0"\
   "enum\0"\
   "embedded\0"\
   "custom\0"\
   "spare\0"

//------------------------------------------------------------------------------
//  Dictionary record
//------------------------------------------------------------------------------

typedef struct {
   QString name;                       // dictionary item definition name
   int     type;                       // dictionary item type
   Enum   *itemsList;                  // enum definition items
} DictionaryDefinition;

//------------------------------------------------------------------------------
//  Data item descriptor (attribute)
//------------------------------------------------------------------------------

typedef struct {
   QString   name;                     // item name
   int       dataType;                 // EDataType
   QString   dataName;                 // data type name
   int       classType;                // EClassType
   QString   className;                // data class name
   QString   units;                    // numeric physical units
   int       width;                    // fixed/float total number width
   int       decimals;                 // fixed/float digits after decimal dot
   double    lowLimit;                 // numeric low limit / string min width
   double    highLimit;                // numeric low limit / string max width
   QString   defaultValue;             // default value as variant
   QString   editor;                   // default editor name
   QString   comment;                  // item comment
} DataItem;

typedef DataItem *DataItemPointer;

//------------------------------------------------------------------------------
//  Data items list
//------------------------------------------------------------------------------

typedef QVector<DataItem> DataItems;

typedef DataItems *DataItemsPointer;

//------------------------------------------------------------------------------
//  Data definition
//------------------------------------------------------------------------------

typedef struct {
   QString   prefix;                   // data structure
   QString   menuPrefix;               // menu prefix
   QString   name;                     // data structure / menu name
   bool      menu;                     // generate menu only
   bool      data;                     // generate data only
   DataItems item;                     // data structure items / menu items
} DataDefinition;

//typedef DataDefinition *DataDefinitionPointer;

//------------------------------------------------------------------------------
//  Embedded data definition
//------------------------------------------------------------------------------

typedef struct {
   QString   name;                     // item name
   int       dataType;                 // EDataType
   QString   dataName;                 // data type name
   int       classType;                // EClassType
   QString   className;                // data class name
   QString   units;                    // numeric physical units
   int       decimals;                 // fixed/float digits after decimal dot
   double    lowLimit;                 // numeric low limit / string min width
   double    highLimit;                // numeric low limit / string max width
   QString   defaultValue;             // default value as variant
   QString   editor;                   // default editor name
   QString   comment;                  // item comment
} EmbeddedDefinition;

//------------------------------------------------------------------------------
#endif // UGENERATORDEF_H
