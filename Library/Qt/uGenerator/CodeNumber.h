//******************************************************************************
//
//   CodeNumber.h    Source code number
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef CODENUMBER_H
   #define CODENUMBER_H

namespace CodeNumber
{
   int fixedRangeWidth( double lowLimit, double highLimit, int decimals);
   // returns total width of <lowLimit>..<highLimit> with <decimals>

   int fixedWidth( double value, int decimals);
   // returns total width of <value> with <decimals>

   double fixedNumber( double value, int decimals);
   // returns <value> normalized with <decimals>

} // CodeNumber

#endif // CODENUMBER_H
