//******************************************************************************
//
//   CodeHeader.cpp Source code header
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "CodeHeader.h"
#include "uGenerator/SourceTemplate.h"
#include "uGenerator/CodeSeparator.h"
#include "Parse/nameTransformation.h"

#define HEADER_FILE_NAME        GENERATOR_TEMPLATE_PATH "Header.h"
#define HEADER_SUFFIX           "Def"

#define CONSTANT_SUFFIX_SIZE          QString( "Size")
#define CONSTANT_SUFFIX_LOW_LIMIT     QString( "Min")
#define CONSTANT_SUFFIX_HIGH_LIMIT    QString( "Max")
#define CONSTANT_SUFFIX_DEFAULT_VALUE QString( "Default")

#define ENUM_SUFFIX_LAST              QString( "Last")

//------------------------------------------------------------------------------
//  Source code
//------------------------------------------------------------------------------

QString CodeHeader::sourceCode( DataParser *dataParser, DictionaryParser *dictionaryParser)
// generate full source code by <dataParser> and <dictionaryParser>
{
   // get header template :
   SourceTemplate headerTemplate( HEADER_FILE_NAME);
   // file title :
   headerTemplate.setParameter( "NAME", dataParser->fileName() + QString( HEADER_SUFFIX));
   headerTemplate.setParameter( "DESCRIPTION", dataParser->title());
   headerTemplate.setParameter( "INCLUDE", CodeHeader::includeFile( "Unisys", "Uni"));
   // generate constants :
   QString constantsText;
   for( int i = 0; i < dataParser->itemsCount(); i++){
      constantsText += CodeHeader::constantsList( dataParser->definitionAt( i));
   }
   if( !constantsText.isEmpty()){
      // some constants generated, add section header
      constantsText = CodeSeparator::section( "Constants") + QChar( '\n') + constantsText;
      constantsText.remove( constantsText.size() - 1, 1);  // remove last linefeed
   }
   headerTemplate.setParameter( "CONSTANTS", constantsText);
   // generate enums :
   QString enumText;
   enumText = enumsList( dictionaryParser);
   headerTemplate.setParameter( "ENUM", enumText);
   // generate data definitions :
   QString dataTypeText;
   dataTypeText = dataTypeDeclaration( dictionaryParser);
   if( !dataTypeText.isEmpty()){
      dataTypeText = CodeSeparator::section( "Data types") + QChar( '\n') + dataTypeText;
      dataTypeText.remove( dataTypeText.size() - 1, 1);  // remove last linefeed
   }
   headerTemplate.setParameter( "DATA_TYPES", dataTypeText);
   // generate structure definitions :
   QString dataText;
   for( int i = 0; i < dataParser->itemsCount(); i++){
      dataText += CodeHeader::structDeclaration( dataParser->definitionAt( i));
   }
   headerTemplate.setParameter( "DATA", dataText);
   return( headerTemplate.toString());
} // sourceCode

//------------------------------------------------------------------------------
//  Constants list
//------------------------------------------------------------------------------

QString CodeHeader::constantsList( const DataDefinition *definition)
// generate constants list for <definition>
{
   QString         text;
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item = &definition->item[ i];
      // check for array :
      if( item->classType == CLASS_TYPE_ARRAY){
         // array size constant
         text += CodeHeader::constantDeclaration( definition->prefix, definition->name,
                                                  item->name, CONSTANT_SUFFIX_SIZE,
                                                  QString::number( item->highLimit));
         text += QChar( '\n');
         continue;
      }
      // check for custom data type :
      if( item->dataType != DATA_TYPE_CUSTOM){
         continue;                     // don't generate constant
      }
      switch( item->classType){
         case CLASS_TYPE_UINT :
         case CLASS_TYPE_INT :
         case CLASS_TYPE_FIXED :
         case CLASS_TYPE_FLOAT :
         case CLASS_TYPE_EMBEDDED :
            text += CodeHeader::constantDeclaration( definition->prefix, definition->name,
                                                     item->name, CONSTANT_SUFFIX_LOW_LIMIT,
                                                     QString::number( item->lowLimit));
            text += CodeHeader::constantDeclaration( definition->prefix, definition->name,
                                                     item->name, CONSTANT_SUFFIX_HIGH_LIMIT,
                                                     QString::number( item->highLimit));
            text += CodeHeader::constantDeclaration( definition->prefix, definition->name,
                                                     item->name, CONSTANT_SUFFIX_DEFAULT_VALUE,
                                                     item->defaultValue);
            text += QChar( '\n');
            break;

         case CLASS_TYPE_CHAR :
            text += CodeHeader::constantDeclaration( definition->prefix, definition->name,
                                                     item->name, CONSTANT_SUFFIX_DEFAULT_VALUE,
                                                     QChar( '\'') + item->defaultValue + QChar( '\''));
            text += QChar( '\n');
            break;

         case CLASS_TYPE_STRING :
         case CLASS_TYPE_PASSWORD :
            text += CodeHeader::constantDeclaration( definition->prefix, definition->name,
                                                     item->name, CONSTANT_SUFFIX_SIZE,
                                                     QString::number( item->highLimit));
            text += CodeHeader::constantDeclaration( definition->prefix, definition->name,
                                                     item->name, CONSTANT_SUFFIX_DEFAULT_VALUE,
                                                     QChar( '"') + item->defaultValue + QChar( '"'));
            text += QChar( '\n');
            break;

         default :
            break;
      }
   }
   return( text);
} // constantsList

//------------------------------------------------------------------------------
//  Constant declaration
//------------------------------------------------------------------------------

QString CodeHeader::constantDeclaration( QString prefix, QString name, QString item, QString suffix, QString value)
// generate single constant declaration
{
   QString text;
   text = QString( "#define %1 %2\n").arg( CodeHeader::constantIdentifier( prefix, name, item, suffix)).arg( value);
   return( text);
} // constantDeclaration

//------------------------------------------------------------------------------
//  Constant size
//------------------------------------------------------------------------------

QString CodeHeader::constantArraySize( QString prefix, QString name, QString item)
// generate constant identifier for array size
{
   return( constantIdentifier( prefix, name, item, CONSTANT_SUFFIX_SIZE));
} // constantArraySize

//------------------------------------------------------------------------------
//  Constant low limit
//------------------------------------------------------------------------------

QString CodeHeader::constantLowLimit( QString prefix, QString name, QString item)
// generate constant identifier for low limit
{
   return( constantIdentifier( prefix, name, item, CONSTANT_SUFFIX_LOW_LIMIT));
} // constantLowLimit

//------------------------------------------------------------------------------
//  Constant high limit
//------------------------------------------------------------------------------

QString CodeHeader::constantHighLimit( QString prefix, QString name, QString item)
// generate constant identifier for high limit
{
   return( constantIdentifier( prefix, name, item, CONSTANT_SUFFIX_HIGH_LIMIT));
} // constantHighLimit

//------------------------------------------------------------------------------
//  Constant default value
//------------------------------------------------------------------------------

QString CodeHeader::constantDefaultValue( QString prefix, QString name, QString item)
// generate constant identifier for default value
{
   return( constantIdentifier( prefix, name, item, CONSTANT_SUFFIX_DEFAULT_VALUE));
} // constantDefaultValue

//------------------------------------------------------------------------------
//  Constant identifier
//------------------------------------------------------------------------------

QString CodeHeader::constantIdentifier( QString prefix, QString name, QString item, QString suffix)
// generate constant identifier
{
   return( OName::toUpperCase( prefix + QChar( ' ') + name + QChar( ' ') + item + QChar( ' ') + suffix));
} // constantIdentifier

//------------------------------------------------------------------------------
//  Enums list
//------------------------------------------------------------------------------

QString CodeHeader::enumsList( DictionaryParser *dictionaryParser)
// generate enums
{
   QString text;
   const DictionaryDefinition *definition;
   for( int i = 0; i < dictionaryParser->itemsCount(); i++){
      definition = dictionaryParser->definitionAt( i);
      if( definition->type != DICTIONARY_TYPE_ENUM){
         continue;
      }
      text += CodeHeader::enumDeclaration( definition->name, definition->itemsList);
   }
   return( text);
} // enumsList

//------------------------------------------------------------------------------
//  Enum declaration
//------------------------------------------------------------------------------

QString CodeHeader::enumDeclaration( QString enumName, Enum *enumDefinition)
// generate enum list for <enumName> by <enumDefinition>
{
   QString text;
   // title :
   text  = CodeSeparator::section( OName::toText( enumName)) + QChar( '\n');
   text += QString( "typedef enum {\n");
   // enum items :
   for( int i = 0; i < enumDefinition->itemsCount(); i++){
      text += QString( "   ") + CodeHeader::enumConstant( enumName, enumDefinition->toString( i)) + QString( ",\n");
   }
   text += QString( "   ") + CodeHeader::enumConstantLast( enumName) + QChar( '\n');
   text += QString( "} E%1;\n").arg( OName::toCamelCase( enumName));
   text += QChar( '\n');               // space after definition
   return( text);
} // enumDeclaration

//------------------------------------------------------------------------------
//  Enum constant
//------------------------------------------------------------------------------

QString CodeHeader::enumConstant( QString enumName, QString item)
// generate enum constant by <enumName> for <item>
{
   return( OName::toUpperCase( enumName + QString( " ") + item));
} // enumConstant

//------------------------------------------------------------------------------
//  Enum constant last
//------------------------------------------------------------------------------

QString CodeHeader::enumConstantLast( QString enumName)
// generate enum last constant by <enumName>
{
   return( QString( "_") + OName::toUpperCase( enumName + QChar( ' ') + ENUM_SUFFIX_LAST));
} // enumConstantLast

//------------------------------------------------------------------------------
//  Data type declaration
//------------------------------------------------------------------------------

QString CodeHeader::dataTypeDeclaration( DictionaryParser *dictionaryParser)
// generate data types
{
   QString dataTypeText;
   const DictionaryDefinition *definition;
   for( int i = 0; i < dictionaryParser->itemsCount(); i++){
      definition = dictionaryParser->definitionAt( i);
      switch( definition->type){
         case DICTIONARY_TYPE_BYTE :
            dataTypeText += CodeHeader::dataTypeItemDeclaration( definition->name, "byte");
            break;
         case DICTIONARY_TYPE_WORD :
            dataTypeText += CodeHeader::dataTypeItemDeclaration( definition->name, "word");
            break;
         case DICTIONARY_TYPE_DWORD :
            dataTypeText += CodeHeader::dataTypeItemDeclaration( definition->name, "dword");
            break;
         default :
            continue;
      }
   }
   return( dataTypeText);
} // dataTypesDeclaration

//------------------------------------------------------------------------------
//  Data type item
//------------------------------------------------------------------------------

QString CodeHeader::dataTypeItemDeclaration( QString typeName, QString dataType)
// generate <item> with <dataType>
{
   return( QString( "typedef ") + dataType + QChar(' ') + dataTypeName( typeName) + QString(";\n"));
} // dataTypeItemDeclaration

//------------------------------------------------------------------------------
//  Data type name
//------------------------------------------------------------------------------

QString CodeHeader::dataTypeName( QString typeName)
// generate data type by <typeName>
{
   return( QChar( 'T') + OName::toCamelCase( typeName));
} // dataTypeName

//------------------------------------------------------------------------------
//  Struct declaration
//------------------------------------------------------------------------------

QString CodeHeader::structDeclaration( const DataDefinition *definition)
// generate struct list by <definition> items
{
   if( definition->menu){
      return( QString());             // skip menu definition
   }
   QString text;
   QString name = OName::toCamelCase( definition->prefix + QChar( ' ') + definition->name); // short struct name
   // title :
   text  = CodeSeparator::section( OName::toText( definition->prefix + QChar( ' ') + definition->name)) + QChar( '\n');
   text += QString( "typedef struct {\n");
   // data items :
   QString         dataTypeName;
   const DataItem *item;
   for( int i = 0; i < definition->item.count(); i++){
      item = &definition->item[ i];
      // check for data type :
      switch( item->dataType){
         case DATA_TYPE_CUSTOM :
            dataTypeName = item->dataName;
            break;

         case DATA_TYPE_STRUCT :
            if( item->classType == CLASS_TYPE_ARRAY){
               dataTypeName = item->dataName;
               break;
            }
            dataTypeName = QChar( 'T') + OName::toCamelCase( item->className);
            break;

         default :
            continue;                  // don't generate struct item
      }
      // check for class type :
      switch( item->classType){
         case CLASS_TYPE_STRING :
         case CLASS_TYPE_PASSWORD :
            text += CodeHeader::structItemArrayDeclaration( item->name, dataTypeName,
                                                            constantArraySize( definition->prefix, definition->name, item->name) + QString( " + 1"),
                                                            item->comment);
            break;

         case CLASS_TYPE_ARRAY :
            text += CodeHeader::structItemArrayDeclaration( item->name, dataTypeName,
                                                            constantArraySize( definition->prefix, definition->name, item->name),
                                                            item->comment);
            break;

         default :
            text += CodeHeader::structItemDeclaration( item->name, dataTypeName, item->comment);
            break;
      }
   }
   text += QString( "} T%1;\n").arg( name);
   text += QChar( '\n');               // space after definition
   return( text);
} // structDeclaration

//------------------------------------------------------------------------------
//  Struct Item
//------------------------------------------------------------------------------

QString CodeHeader::structItemDeclaration( QString item, QString dataType, QString comment)
// generate struct <item> with <dataType> and <comment>
{
   QString text;
   text = QString( "   ") + dataType + QChar( ' ') + OName::toCamelCase( item) + QChar( ';');
   if( !comment.isEmpty()){
      text += QString( " // %1").arg( comment);
   }
   text += QChar( '\n');
   return( text);
} // structItemDeclaration

//------------------------------------------------------------------------------
//  Struct Array Item
//------------------------------------------------------------------------------

QString CodeHeader::structItemArrayDeclaration( QString item, QString dataType, QString Size, QString comment)
// generate struct <item>[ Size] with <dataType> and <comment>
{
   QString text;
   text  = QString( "   ") + dataType + QChar( ' ') + OName::toCamelCase( item);
   text += QString( "[ ") + Size + QString( "];");
   if( !comment.isEmpty()){
      text += QString( " // %1").arg( comment);
   }
   text += QChar( '\n');
   return( text);
} // structItemArrayDeclaration

//------------------------------------------------------------------------------
//  Include file
//------------------------------------------------------------------------------

QString CodeHeader::includeFile( QString filePath, QString fileName)
// generate include statement for <filePath> and <fileName>
{
   QString text;
   text  =  QString( "#ifndef __%1_H__\n").arg( fileName);
   if( filePath.isEmpty()){
      text +=  QString( "   #include \"%1.h\"\n").arg( fileName);
   } else {
      text +=  QString( "   #include \"%1/%2.h\"\n").arg( filePath).arg( fileName);
   }
   text +=  QString( "#endif\n");
   return( text);
} // includeFile
