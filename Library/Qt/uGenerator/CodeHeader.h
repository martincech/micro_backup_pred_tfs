//******************************************************************************
//
//   CodeHeader.h   Source code header
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef CODEHEADER_H
   #define CODEHEADER_H

#include <QString>
#include "uGenerator/DataParser.h"
#include "uGenerator/DictionaryParser.h"

namespace CodeHeader
{

   QString sourceCode( DataParser *dataParser, DictionaryParser *dictionaryParser);
   // generate full source code by <dataParser> and <dictionaryParser>

   //---------------------------------------------------------------------------
   QString constantsList( const DataDefinition *definition);
   // generate constants list for <definition>

   QString constantDeclaration( QString prefix, QString name, QString item, QString suffix, QString value);
   // generate single constant declaration

   QString constantArraySize( QString prefix, QString name, QString item);
   // generate constant identifier for array size

   QString constantLowLimit( QString prefix, QString name, QString item);
   // generate constant identifier for low limit

   QString constantHighLimit( QString prefix, QString name, QString item);
   // generate constant identifier for high limit

   QString constantDefaultValue( QString prefix, QString name, QString item);
   // generate constant identifier for default value

   QString constantIdentifier( QString prefix, QString name, QString item, QString suffix);
   // generate constant identifier

   //---------------------------------------------------------------------------
   QString enumsList( DictionaryParser *dictionaryParser);
   // generate enums

   QString enumDeclaration( QString enumName, Enum *enumDefinition);
   // generate enum list for <enumName> by <enumDefinition>

   QString enumConstant( QString enumName, QString item);
   // generate enum constant by <enumName> for <item>

   QString enumConstantLast( QString enumName);
   // generate enum last constant by <enumName>

   //---------------------------------------------------------------------------
   QString dataTypeDeclaration( DictionaryParser *dictionaryParser);
   // generate data types

   QString dataTypeItemDeclaration( QString typeName, QString dataType);
   // generate <item> with <dataType>

   QString dataTypeName( QString typeName);
   // generate data type by <typeName>

   //---------------------------------------------------------------------------
   QString structDeclaration( const DataDefinition *definition);
   // generate struct list by <definition> items

   QString structItemDeclaration( QString item, QString dataType, QString comment);
   // generate struct <item> with <dataType> and <comment>

   QString structItemArrayDeclaration( QString item, QString dataType, QString Size, QString comment);
   // generate struct <item>[ Size] with <dataType> and <comment>

   //---------------------------------------------------------------------------
   QString includeFile( QString filePath, QString fileName);
   // generate include statement for <filePath> and <fileName>

} // CodeHeader

#endif // CODEHEADER_H
