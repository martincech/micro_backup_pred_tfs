//******************************************************************************
//
//   CodeNumber.cpp  Source code default values
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "CodeNumber.h"
#include <QtCore/qmath.h>

//------------------------------------------------------------------------------
//  Fixed range width
//------------------------------------------------------------------------------

int CodeNumber::fixedRangeWidth( double lowLimit, double highLimit, int decimals)
// returns total width of <lowLimit>..<highLimit> with <decimals>
{
   int lowWidth  = CodeNumber::fixedWidth( lowLimit, decimals);
   int highWidth = CodeNumber::fixedWidth( highLimit, decimals);
   if( lowWidth > highWidth){
      return( lowWidth);
   }
   return( highWidth);
} // fixedRangeWidth

//------------------------------------------------------------------------------
//  Fixed width
//------------------------------------------------------------------------------

int CodeNumber::fixedWidth( double value, int decimals)
// returns total width of <value> with <decimals>
{
   int width = 0;
   if( value < 0){
      width = 1;                       // add signum
      value = -value;                  // to positive
   }
   if( decimals > 0){
      width += decimals + 1;           // add dot with decimals
   }
   // digits count :
   while( value >= 10){
      value /= 10;
      width++;
   }
   width++;                            // at least one digit
   return( width);
} // fixedWidth

//------------------------------------------------------------------------------
//  Fixed number
//------------------------------------------------------------------------------

double CodeNumber::fixedNumber( double value, int decimals)
// returns <value> normalized with <decimals>
{
   qreal fixedValue = value;
   fixedValue *= qPow( 10, decimals);
   return( fixedValue);
} // fixedNumber
