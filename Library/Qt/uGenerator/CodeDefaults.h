//******************************************************************************
//
//   CodeDefaults.h  Source code default values
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef CODEDEFAULTS_H
   #define CODEDEFAULTS_H

#include <QString>
#include "uGenerator/DataParser.h"
#include "uGenerator/DictionaryParser.h"

namespace CodeDefaults
{

   QString sourceCode( DataParser *dataParser, DictionaryParser *dictionaryParser);
   // generate full source code by <dataParser> and <dictionaryParser>

   //---------------------------------------------------------------------------
   QString globalDeclaration( DataParser *dataParser);
   // generate global variables list

   QString globalItemDeclaration( QString name);
   // generate global variable with <name>

   QString structDeclaration( const DataDefinition *definition);
   // generate struct list by <definition> items

   QString structItemDeclaration( QString item, QString value);
   // generate struct <item> with <value>

   QString structEnumItemDeclaration( QString item, QString enumName, QString value);
   // generate struct <item> with <value> for <enumName>

} // CodeDefaults

#endif // CODEDEFAULTS_H
