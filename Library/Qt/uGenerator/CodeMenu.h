//******************************************************************************
//
//   CodeMenu.h    Source code menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef CODEMENU_H
   #define CODEMENU_H

#include <QString>
#include "uGenerator/DataParser.h"
#include "uGenerator/DictionaryParser.h"

namespace CodeMenu
{

   QStringList sourceCode( DataParser *dataParser, DictionaryParser *dictionaryParser);
   // generate full source code by <dataParser> and <dictionaryParser>

   QStringList headerCode(  DataParser *dataParser, DictionaryParser *dictionaryParser);
   // generate headers source code

   QStringList sourceFiles( DataParser *dataParser);
   // returns source file names

   //---------------------------------------------------------------------------
   QString menuSource( QString fileName, const DataDefinition *definition);
   // generate menu source by <definition> with main <fileName> header

   QString includeList( QString fileName, const DataDefinition *definition);
   // generate menu include list with main <fileName> by <definition>

   QString stringsList( const DataDefinition *definition);
   // generate menu strings list

   QString enumList( const DataDefinition *definition);
   // generate menu enum list

   QString dataList( const DataDefinition *definition);
   // generate menu data definition list

   QString caseList( const DataDefinition *definition);
   // generate menu case list

   QString parametersList( const DataDefinition *definition);
   // generate menu parameters list

   QString enumConstant( QString name);
   // generate menu item constant

   QString itemUnits( const DataItem *item);
   // generate units format string

   //---------------------------------------------------------------------------
   QString menuHeader( QString fileName, const DataDefinition *definition);
   // generate menu header source by <definition> with main <fileName> header

} // CodeMenu

#endif // CODEMENU_H
