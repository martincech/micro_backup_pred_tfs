//*****************************************************************************
//
//    MultitaskingDef.h   MultitaskingDef
//    Version 1.0        (c) Veit Electronics
//
//*****************************************************************************

#ifndef __MultitaskingDef_H__
   #define __MultitaskingDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

typedef int TMutex;

typedef struct sTTask{
   void* Task;
   dword Event;
   dword EventLong;
   TYesNo Idle;
   void (*EntryPoint)(struct sTTask *);
   void *Stack;
   int StackSize;
} TTask;

#endif
