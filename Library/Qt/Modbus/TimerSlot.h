#ifndef TIMERSLOT_H
#define TIMERSLOT_H

#include <QObject>

typedef void TTimerFunc(void);

class TimerSlot : public QObject
{
   Q_OBJECT

private :
   TTimerFunc *cfunc;

public:
   explicit TimerSlot(TTimerFunc *func, QObject *parent = 0): cfunc(func), QObject(parent){
   }

   ~TimerSlot(){}

public slots:
   void timerExpired( void);


};

#endif // TIMERSLOT_H
