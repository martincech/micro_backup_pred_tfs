#include "ModelParser.h"
#include <QString>

#define GROUP_COLUMN          0
#define ADDRESS_COLUMN        1
#define NAME_COLUMN           2
#define RW_COLUMN             3
#define GENERATOR_TYPE_COLUMN 4
#define RANGE_COLUMN          5
#define DEFAULT_VALUE_COLUMN  6
#define DATA_NAME_COLUMN      8
#define DATA_FILE_COLUMN      9
#define DATA_GETTER_COLUMN    10
#define DATA_SETTER_COLUMN    11
#define DATA_GS_LOCALS_COLUMN 12
#define DESCRIPTION_COLUMN    13

#define CHAR_THREE_DOTS  ((wchar_t)0x2026) //"�"

const QString ColumnNames[] = {
   "Name",
   "Number",
   "Type",
   "Readable",
   "Writable",
   "Range from",
   "Range to",
   "Data name",
   "Data file"
};
#define COLUMN_COUNT 8

static int addArrayRegisters( RegisterGroup *toGroup, Register *first, int to);

//------------------------------------------------------------------------------
//  Parsem array model and create registers
//------------------------------------------------------------------------------
ArrayModel *ModelParser::GetModel(ARegister *reg)
{
   if(!reg){
      return NULL;
   }
   ArrayModel *model = new ArrayModel(0, 5);
   QStringList slist;
   slist.append( reg->Name());
   slist.append(QString::number(reg->Number()));
   slist.append(ARegister::encodeType(reg->Type()));
   slist.append(reg->Readable()? "true" : "false");
   slist.append(reg->Writable()? "true" : "false");
   model->addRow(slist);
   SetModelTitle(model);
   return model;
}

ARegister *ModelParser::Parse( )
{// get version as string list (major, minor)
int            rows;
QString        groupName;
QString        groupValue;
QString        groupVars;
QString        loGroupName;
QStringList    slist;
RegisterGroup  *AllRegs;
RegisterGroup  *LastReg;
int            titleRow;

   titleRow = -1;
   rows = model->getRowsCount();
   AllRegs = new RegisterGroup(QString("All"), slist, 0);
   for( int row = 0; row < rows; row++){
      groupName = model->getItem(row, GROUP_COLUMN);
      groupValue = model->getItem(row, ADDRESS_COLUMN);
      groupVars = model->getItem(row, NAME_COLUMN);
      if( groupName.isEmpty()){
         continue;
      }
      // new group name or keyword,
      // check for keywords
      loGroupName = groupName.toLower();
      loGroupName.remove(" ");
      if( loGroupName == QString( KEYWORD_VERSION)){
         // version
         continue;
      }
      if(loGroupName == QString( KEYWORD_GROUP)){
         // data header
         int cols = model->getColumnsCount();
         for( int col = 0; col < cols; col++){
            model->setTitle(col, model->getItem(row, col));
         }
         titleRow = row;
         continue;
      }

      // no keyword, group name
      slist.clear();
      if( !groupVars.isEmpty()){
         slist = groupVars.split(";");
         slist.removeAll("");
      }
      LastReg = new RegisterGroup(groupName, slist, groupValue.toInt());
      AllRegs->Add(LastReg);

      // parse whole group
      row = ParseGroup( LastReg, model, row);
   }
   if( titleRow >= 0){
      model->removeRows( titleRow, 1);
   }

   return AllRegs;
}

int ModelParser::ParseGroup( RegisterGroup *group, ArrayModel *model, int fromRow)
{// parse register group and return last row of of group
int row;
QString        regAddress;
QString        regRW;
QString        regValueRange;
QString        regName;
QString        regDataName;
QStringList    regDataFile;
QString        regDataGetter;
QString        regDataSetter;
QString        regDataGSLocal;
QString        regType;
QString        regDesc;
QString        regValueDefault;
int            currentAddress;
int            arrayTypeFirstRow;
Register       *reg;
int            groupN;
int            sGroupN;
int            regN;

   row = fromRow;
   reg = NULL;
   arrayTypeFirstRow = -1;
   groupN = sGroupN = regN = 0;
   while( row < model->getRowsCount()){
      row++;
      if( !model->getItem(row, GROUP_COLUMN).isEmpty()){
         return --row;
      }
      //register values
      regRW          = model->getItem(row, RW_COLUMN).toLower();
      regValueRange  = model->getItem(row, RANGE_COLUMN);
      regName        = model->getItem(row, NAME_COLUMN);
      regAddress     = model->getItem(row, ADDRESS_COLUMN);
      regDataName    = model->getItem(row, DATA_NAME_COLUMN);
      regDataFile    = model->getItem(row, DATA_FILE_COLUMN).split(";");
      regDataFile.replaceInStrings(QRegExp("\\.h"), "");
      regDataFile.replaceInStrings(QRegExp("[ ]*"), "");
      regDataGetter  = model->getItem(row, DATA_GETTER_COLUMN);
      regDataSetter  = model->getItem(row, DATA_SETTER_COLUMN);
      regDataGSLocal = model->getItem(row, DATA_GS_LOCALS_COLUMN);
      regDataGetter.replace(QRegExp(";[ ]*"), QString(";"));
      regDataSetter.replace(QRegExp(";[ ]*"), QString(";"));
      regDataGSLocal.replace(QRegExp(";[ ]*"), QString(";"));
      regDataGetter.remove(QRegExp(";[ ]*$"));
      regDataSetter.remove(QRegExp(";[ ]*$"));
      regDataGSLocal.remove(QRegExp(";[ ]*$"));
      regType        = model->getItem(row, GENERATOR_TYPE_COLUMN);
      regDesc        = model->getItem(row, DESCRIPTION_COLUMN);
      regValueDefault= model->getItem(row, DEFAULT_VALUE_COLUMN);
      //create register from this values
      if( !regAddress.isEmpty()){
         // just subgroup separator
         currentAddress = regAddress.toInt() + group->Number();
         continue;
      }
      if( regName.isEmpty() && !regValueRange.isEmpty()){
         //just range to
         if( reg){
            updateRangeTo(reg, regValueRange);
         }
         continue;
      }
      if( !regName.isEmpty() && (( regName == "...") || ( regName.unicode()[0] == CHAR_THREE_DOTS))){
         //array definition
         arrayTypeFirstRow = row - 1;
         continue;
      }

      if( regName.isEmpty()){
         //empty line or comment line
         continue;
      }
      if( arrayTypeFirstRow > 0){
         currentAddress = addArrayRegisters(group, reg, regName.split("_").last().toInt());
         arrayTypeFirstRow = -1;
         continue;
      }
      reg = new Register( regName, ARegister::decodeType( regType), currentAddress, regDataFile, regRW.contains("r"), regRW.contains("w"));
      reg->SetDefaultValue(regValueDefault);
      reg->SetDescription(regDesc);
      reg->SetGetter(regDataGetter);
      reg->SetSetter(regDataSetter);
      reg->SetLocals(regDataGSLocal);
      reg->SetDataName(regDataName);
      decodeRange(reg, regValueRange);
      // index replacement
      reg->SetGetter(reg->Getter().replace(QRegExp("\\[.*\\]"), "[ i]"));
      reg->SetGetter(reg->Getter().replace(QRegExp("[ ]+\\[ i\\]"), " i"));
      reg->SetSetter(reg->Setter().replace(QRegExp("\\[.*\\]"), "[ i]"));
      reg->SetSetter(reg->Setter().replace(QRegExp("[ ]+\\[ i\\]"), " i"));
      reg->SetDataName(reg->DataName().replace(QRegExp("\\[.*\\]"), "[ i]"));
      reg->SetDataName(reg->DataName().replace(QRegExp("[ ]+\\[ i\\]"), " i"));

      group->Add(reg);
      currentAddress++;
   }

   return row;
}

//------------------------------------------------------------------------------
//  Version
//------------------------------------------------------------------------------

QString ModelParser::GetVersionMajor( )
{
   return GetVersion( model).at(0);
}

QString ModelParser::GetVersionMinor()
{
   return GetVersion( model).at(1);
}

//------------------------------------------------------------------------------
//  title model
//------------------------------------------------------------------------------

void ModelParser::SetModelTitle( ArrayModel *model )
{

   if( !model){
      return;
   }

   for(int i = 0; i < model->getColumnsCount(); i++){
      if(i >= COLUMN_COUNT){
         return;
      }
      model->setTitle(i, ColumnNames[i]);
   }
}

//------------------------------------------------------------------------------
//  private helper
//------------------------------------------------------------------------------

QStringList ModelParser::GetVersion( ArrayModel *model)
{
int            rows;
QString        groupName;
QString        groupValue;

   rows = model->getRowsCount();
   for( int row = 0; row < rows; row++){
      groupName = model->getItem(row, GROUP_COLUMN).toLower();
      groupValue = model->getItem(row, ADDRESS_COLUMN);
      if( !groupName.isEmpty()){
         groupName.remove(" ");
         if( groupName == QString( KEYWORD_VERSION)){
            // version
            if( groupValue.isEmpty()){
               continue;
            }
            return groupValue.split(".");
         }
      }
   }
   return QStringList("1");
}

//------------------------------------------------------------------------------
//  Update register range
//------------------------------------------------------------------------------

void ModelParser::updateRangeTo( Register *reg, QString &regValueRangeField)
{
bool     isInt;
int      to;

   to = regValueRangeField.toInt(&isInt);
   if( !isInt){
      //range not numeric but character or string
      if( regValueRangeField.length() > 1){
         reg->SetTo(regValueRangeField);
      } else {
         reg->SetTo(regValueRangeField.toAscii().at(0));
      }
   }
}

void ModelParser::decodeRange( Register *reg, QString &regValueRangeField)
{
bool           isInt;
QStringList    stringList;
QByteArray     byteArray;

   if( regValueRangeField.isEmpty()){
      reg->SetFrom(0);
      reg->SetTo(0xffff);
      return;
   }

   stringList = regValueRangeField.split("..");

   if( regValueRangeField.contains("/")){
      stringList = regValueRangeField.split("/");
   }
   reg->SetFrom(stringList.at(0).toInt(&isInt));
   if( !isInt){
      if( stringList.at(0).length() > 1){
         //range not numeric but string
         reg->SetFrom(stringList.at(0));
      } else {
         //range not numeric but character
         byteArray = stringList.at(0).toAscii();
         if( byteArray.isEmpty()){
            reg->SetFrom(' ');
         } else {
            reg->SetFrom(byteArray.at(0));
         }
      }
   }
   if( stringList.count() <= 1){
      reg->SetTo(0);
      return;
   }
   reg->SetTo(stringList.at(1).toInt(&isInt));
   if( !isInt){
      //range not numeric but character
      byteArray = stringList.at(1).toAscii();
      if( byteArray.isEmpty()){
         reg->SetTo(' ');
      } else {
         reg->SetTo( byteArray.at(0));
      }
   }
}

//------------------------------------------------------------------------------
//  Add all array registers
//------------------------------------------------------------------------------

int addArrayRegisters( RegisterGroup *toGroup, Register *first, int to)
{
QStringList nameList;
Register    *newReg;
RegisterGroup *newArray;
bool        TwoNumbers;
int         num;
bool        nameSet;

   toGroup->Remove(first);
   newArray = new RegisterGroup(first->Name(), nameList, first->Number());
   newArray->Add(first);
   nameSet = false;
   TwoNumbers = false;
   num = first->Number();
   nameList = first->Name().split("_");
   if( nameList.count() - 2 >= 0){
      nameList.at(nameList.count() - 2).toInt(&TwoNumbers);
   }

   for( int i = nameList.last().toInt() + 1; i <= to; TwoNumbers? i+=2 : i++){
      newReg = new Register(*first);
      nameList.removeLast();
      if( TwoNumbers){
         nameList.removeLast();
      }
      if(!nameSet){newArray->SetName(nameList.join("_"));}
      nameList.append(QString::number(i));
      if( TwoNumbers){
         nameList.append(QString::number(i+1));
      }
      newReg->SetName( nameList.join("_"));
      newReg->SetNumber(++num);
      newArray->Add(newReg);
   }
   toGroup->Add(newArray);
   return ++num;
}
