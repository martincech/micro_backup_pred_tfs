#include "RegisterCreator.h"
#include <exception>

const ArrayModel *RegisterCreator::GetModel()
{
   if(!parser){
      return NULL;
   }
   return parser->GetModel();
}

ArrayModel *RegisterCreator::CreateModel(ARegister *reg)
{
   if(!parser){
      return NULL;
   }
   return parser->GetModel(reg);
}

CsvCreator::CsvCreator( QString FileName)
   : RegisterCreator()
{
   reader = new Csv(255);
   registers = NULL;
   iFile = FileName;
}

CsvCreator::~CsvCreator()
{
   delete( reader);
}

ARegister *CsvCreator::CreateRegisters()
{
   if( !registers){
      if( !reader->load(iFile)){
         return NULL;
      }
      parser = new ModelParser(reader->array());
      registers = parser->Parse();
   }
   return registers;
}
