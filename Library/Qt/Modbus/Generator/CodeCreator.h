//******************************************************************************
//
//   CodeCreator.h     Source code generator
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CodeCreator_H__
   #define __CodeCreator_H__

#include <QString>
#include <QStringList>
#include "uGenerator/SourceTemplate.h"
#include "Modbus/Generator/GeneratorDef.h"

//------------------------------------------------------------------------------
//  Abstract builder
//------------------------------------------------------------------------------

class CodeCreator
{
protected:
   QString output;

   SourceTemplate sourceTemplate;
   QString sourceTemplateFileName;

   QString oFileName;

   CodeCreator(QString templateFile);
   QString indentation(int times = 1);

public:
//   virtual QString RepresentConstant() = 0;
   virtual QString RepresentInclude(QString path, QString name, bool local = true) = 0;
   virtual QString RepresentFunctionDeclaration(QString returnType, QString name, QStringList parameters, QString comment = QString()) = 0;
   virtual QString RepresentFunctionDefinition( QString returnType, QString name, QStringList parameters, QString body, QString comment = QString()) = 0;
   virtual QString RepresentFunctionCall( QString name, QStringList parameters = QStringList()) = 0;
   virtual QString RepresentGlobalVariableDefinition( QString type, QString name, QString defaultValue = QString(), QString description = QString()) = 0;
   virtual QString RepresentVariableDefinition( QString type, QString name, QString defaultValue= QString()) = 0;
   virtual QString RepresentGlobalVariableUse(QString name) = 0;
   virtual void SetCodeName(QString name) = 0;


   virtual void CreateIncludeFiles(QStringList &FilesList) = 0;
   virtual void CreateFunctions(QStringList &Declarations, QStringList &Definitions) = 0;
   virtual void CreateVariables(QStringList &vars) = 0;

   //   virtual void CreateFileHeader() = 0;
   virtual QString GetResult() = 0;

}; // CodeCreator


//------------------------------------------------------------------------------
//  Source .c builder
//------------------------------------------------------------------------------

class CSourceCreator : public CodeCreator
{
protected:
   QString includes;
   QString constants;
   QString functionsDecl;
   QString functionsDef;
   QString name;
   QString variables;

public:
   CSourceCreator() : CodeCreator(QString(SOURCE_FILE_NAME)){}

   virtual QString RepresentInclude(QString path, QString name, bool local = true);
   virtual QString RepresentFunctionDeclaration(QString returnType, QString name, QStringList parameters, QString comment = QString());
   virtual QString RepresentFunctionDefinition( QString returnType, QString name, QStringList parameters, QString body, QString comment = QString());
   virtual QString RepresentFunctionCall( QString name, QStringList parameters = QStringList());
   virtual QString RepresentGlobalVariableDefinition( QString type, QString name, QString defaultValue, QString description);
   virtual QString RepresentVariableDefinition( QString type, QString name, QString defaultValue= QString());
   virtual QString RepresentGlobalVariableUse(QString name);
   virtual QString RepresentConstantDefinition() = 0;
   virtual QString RepresentConstantUse() = 0;
   virtual void SetCodeName(QString name);

   virtual void CreateIncludeFiles(QStringList &FilesList);
   virtual void CreateFunctions(QStringList &Declarations, QStringList &Definitions);
   virtual void CreateVariables(QStringList &vars);

   virtual QString GetResult();
};

//------------------------------------------------------------------------------
//  Source .c buider
//------------------------------------------------------------------------------

class CHeaderCreator : public CSourceCreator
{
protected:
   QString enums;
   QString dataTypes;
   QString data;
   QString constants;

public:
   CHeaderCreator();

  virtual QString GetResult();

};
#endif // __CodeCreator_H__


