//******************************************************************************
//
//   mGenerator.h  modbus generator
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef MGENERATOR_H
    #define MGENERATOR_H

#include "mGenerator/ModbusRegParser.h"

class mGenerator
{
public:
    mGenerator();
    ~mGenerator();

    bool parse( ArrayModel *registers);
    // Parse <registers>

    //---------------------------------------------------------------------------
    ModbusRegParser *getRegParser( void);
    // returns register parser

    QStringList globalHeaders();
    // returns global header files
    QStringList globalHeadersFileNames();
    // returns global header file names
    QStringList globalSources();
    // returns global source files
    QStringList globalSourcesFileNames();
    // returns global source file names

    QStringList groupHeaders();
    // return register group by group headers
    QStringList groupHeadersFileNames();
    // return register group by group headers file names
    QStringList groupSources();
    // return register group by group sources
    QStringList groupSourcesFileNames();
    // return register group by group sources file names
 private :
    ModbusRegParser *registersParser;

    QStringList groupFileNames( bool h = true);
    // return register group by group file names with .h or .c suffix
    QString _GenModbusRegRangeCheckHeader();
    // generate source code for ModbusRegRangeCheck.h file
    QString _GenModbusRegRangeCheckSource();
    // generate source code for ModbusRegRangeCheck.c file
    QString _GenModbusRegHeader();
    // generate source code for ModbusReg.h file
    QString _GenModbusRegSource();
    // generate source code for ModbusReg.c file
};

#endif // MGENERATOR_H
