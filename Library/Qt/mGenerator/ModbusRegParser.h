#ifndef MODBUSREGPARSER_H
   #define MODBUSREGPARSER_H

#include "Model/ArrayModel.h"
#include "mGenerator/GeneratorDef.h"

typedef QVector<RegisterGroup> RegisterGroups;

class ModbusRegParser
{
public:
   ModbusRegParser( ArrayModel *registerArray);
   // create modbus regs from <registerArray>

   ~ModbusRegParser();
   // destructor

   int itemsCount();
   // returns number of items

   const RegisterGroup *getGroup( QString name);
   // returns enumeration definition by <name>

   RegisterGroup *groupAt( int index);
   // returns enumeration definition at <index> position

   const ArrayModel *regGroupToArrayModel( int index);
   const ArrayModel *regGroupToArrayModel( QString name);
   const ArrayModel *regGroupToArrayModel( const RegisterGroup *group);
      // returns array model of all registers inside group

   bool hasName( QString name);
   // check for <name>

   QString getVersionMajor(){ return QString::number(_versionMajor);}
   QString getVersionMinor(){ return QString::number(_versionMinor);}
private :
   RegisterGroups   _registerList;
   int              _versionMajor;
   int              _versionMinor;
   ArrayModel      *regModel;

   int addArrayRegisters( RegisterGroup *toGroup, Register *first, int to);
   void updateRangeTo( RegisterGroup *group, int index, QString &regValueRangeField);
   void decodeRange( Register *reg, QString &regValueRangeField);
   ERegisterType decodeType( QString &typeField);
   void updateArrayType( RegisterGroup *group, int index, int type = true);
};

#endif // MODBUSREGPARSER_H
