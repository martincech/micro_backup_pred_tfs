//******************************************************************************
//
//   mGenerator.h  modbus generator
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "mGenerator.h"
#include "mGenerator/CodeSource.h"
#include "uGenerator/CodeHeader.h"
#include "uGenerator/CodeSeparator.h"
#include "uGenerator/SourceTemplate.h"
#include "Parse/nameTransformation.h"
#include "mGenerator/GeneratorDef.h"
#include "mGenerator/CodeGroup.h"



// Local functions :
static QString regCheck(ModbusRegParser *registersParser, bool read = true);
// helper function for ModbusReg.c generation - get condition code for read registers check or write registers check
static QString groupCall( ModbusRegParser *registersParser, bool read = true);
// helper function for ModbusReg.c generation - get function call for group register read/write

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

mGenerator::mGenerator()
{
   registersParser = NULL;
}

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

mGenerator::~mGenerator()
{
   if( registersParser){
      delete registersParser;
   }
}

//------------------------------------------------------------------------------
//  Parse
//------------------------------------------------------------------------------

bool mGenerator::parse( ArrayModel *registers)
// Parse <registers>
{
   QString name;
   // delete old data :
   if( registersParser){
      delete registersParser;
   }
   registersParser      = new ModbusRegParser( registers);

   return( true);
}

//------------------------------------------------------------------------------
//  return parser
//------------------------------------------------------------------------------

ModbusRegParser *mGenerator::getRegParser( void)
// returns register parser
{
   return registersParser;
}

//------------------------------------------------------------------------------
//  Global header
//------------------------------------------------------------------------------

QStringList mGenerator::globalHeaders()
// returns global header files
{
QStringList headers;

   headers.append(_GenModbusRegRangeCheckHeader());
   headers.append(_GenModbusRegHeader());
   return headers;
}
QStringList mGenerator::globalHeadersFileNames()
// returns global header file names
{
QStringList headers;

   headers.append(MODBUS_RANGE_CHECK_FILENAME".h");
   headers.append(MODBUS_REG_FILENAME".h");
   return headers;
}
//------------------------------------------------------------------------------
//  Global source
//------------------------------------------------------------------------------

QStringList mGenerator::globalSources()
// returns global source files
{
QStringList sources;

   sources.append(_GenModbusRegRangeCheckSource());
   sources.append(_GenModbusRegSource());
   return sources;
}

QStringList mGenerator::globalSourcesFileNames()
// returns global source file names
{
QStringList sources;

   sources.append(MODBUS_RANGE_CHECK_FILENAME".c");
   sources.append(MODBUS_REG_FILENAME".c");
   return sources;
}

//------------------------------------------------------------------------------
//  Group headers
//------------------------------------------------------------------------------

QStringList mGenerator::groupHeaders()
// return register group by group headers
{
int                  groupCount;
const RegisterGroup *group;
QStringList          headers;

   groupCount = registersParser->itemsCount();
   for( int i = 0; i < groupCount; i++){
      group = registersParser->groupAt(i);
      headers.append( CodeGroup::headerCode( group));
   }

   return headers;
}

QStringList mGenerator::groupHeadersFileNames()
// return register group by group headers file names
{
   return groupFileNames(true);
}

//------------------------------------------------------------------------------
//  Group sources
//------------------------------------------------------------------------------

QStringList mGenerator::groupSources()
// return register group by group sources
{
int                  groupCount;
RegisterGroup       *group;
QStringList          sources;

   groupCount = registersParser->itemsCount();
   for( int i = 0; i < groupCount; i++){
      group = registersParser->groupAt(i);
      sources.append( CodeGroup::sourceCode(group));
   }

   return sources;
}

QStringList mGenerator::groupSourcesFileNames()
// return register group by group sources file names
{
   return groupFileNames(false);
}

QStringList mGenerator::groupFileNames( bool h)
// return register group by group file names with .h or .c suffix
{
const RegisterGroup *group;
int                  groupCount;
QStringList          names;

   groupCount = registersParser->itemsCount();
   for( int i = 0; i < groupCount; i++){
      group = registersParser->groupAt(i);
      if( h){
         names.append(CodeGroup::fileName( group) + ".h");
      } else {
         names.append(CodeGroup::fileName( group) + ".c");
      }
   }
   return names;
}

//------------------------------------------------------------------------------
//  Header 1
//------------------------------------------------------------------------------

QString mGenerator::_GenModbusRegRangeCheckHeader()
// generate source code for ModbusRegRangeCheck.h file
{
const RegisterGroup *group;
Register             reg;

   // get header template :
   SourceTemplate headerTemplate( HEADER_FILE_NAME);
   headerTemplate.setParameter( "NAME", MODBUS_RANGE_CHECK_FILENAME);
   headerTemplate.setParameter( "DESCRIPTION", "Modbus write registers range check functions");
   headerTemplate.setParameter( "INCLUDE", CodeHeader::includeFile("Unisys", "Uni") + CodeHeader::includeFile("", MODBUS_REG_FILENAME));

   //generate constants and functions
   QString constantsText;
   constantsText += CodeSeparator::section("Constants");
   QString functionsText;
   functionsText += CodeSeparator::section("Functions");

   for(int i = 0; i < registersParser->itemsCount(); i++){
      group = registersParser->groupAt(i);
      constantsText += CodeSeparator::section(group->name + " registers range");
      Registers writableRegs = group->RegistersOfType(REGISTER_W, true, true);
      if(writableRegs.isEmpty()){continue;}

      // max and mins for each register or register array
      for(int j = 0; j < writableRegs.count(); j++){
         reg = writableRegs[j];
         if( reg.arrayType){
            j += group->RegistersOfTheSameArrayType(reg).count() - 1;
            reg.name = reg.ArrayName();
         }
         if(reg.registerType == REGISTER_COMMAND || reg.registerType == REGISTER_STRING){
            constantsText += CodeHeader::constantDeclaration("modbus reg", group->name, reg.name, "Min", QString::number(1));
            constantsText += CodeHeader::constantDeclaration("modbus reg", group->name, reg.name, "Max", QString::number(1));
         } else{
            constantsText += CodeHeader::constantDeclaration("modbus reg", group->name, reg.name, "Min", QString::number(reg.from));
            constantsText += CodeHeader::constantDeclaration("modbus reg", group->name, reg.name, "Max", QString::number(reg.to));
         }
      }
      // declaration of check functions
      SourceTemplate functionTemplate( RANGE_CHECK_DECLARATION);
      functionTemplate.setParameter( "CCNAME", OName::toCamelCase( group->name));
      functionTemplate.setParameter( "NAME", group->name);
      functionsText += functionTemplate.toString();
   }

   headerTemplate.setParameter( "CONSTANTS", constantsText);
   headerTemplate.setParameter( "FUNCTIONS", functionsText);
   // no enums, data types nor data
   headerTemplate.setParameter( "ENUM", "");
   headerTemplate.setParameter( "DATA_TYPES", "");
   headerTemplate.setParameter( "DATA", "");
   return( headerTemplate.toString());
}

//------------------------------------------------------------------------------
//  Source 1
//------------------------------------------------------------------------------

QString mGenerator::_GenModbusRegRangeCheckSource()
// generate source code for ModbusRegRangeCheck.c file
{
const RegisterGroup *group;
Register             reg;
Registers            regs;

   SourceTemplate sourceTemplate( SOURCE_FILE_NAME);
   sourceTemplate.setParameter( "NAME", MODBUS_RANGE_CHECK_FILENAME);
   sourceTemplate.setParameter( "DESCRIPTION", "Modbus write registers range check functions");
   // no includes nor local functions
   sourceTemplate.setParameter( "INCLUDE", "");
   sourceTemplate.setParameter( "LOCAL_DECLARATIONS", "");
   sourceTemplate.setParameter( "LOCAL_DEFINITIONS", "");

   QString functionsText;
   for(int i = 0; i < registersParser->itemsCount(); i++){
      group = registersParser->groupAt(i);
      Registers writableRegs = group->RegistersOfType(REGISTER_W, true, true);
      if(writableRegs.isEmpty()){continue;}

      SourceTemplate functionTemplate( RANGE_CHECK_DEFINITION);
      functionTemplate.setParameter( "CCNAME", OName::toCamelCase( group->name));
      functionTemplate.setParameter( "NAME", group->name);
      QString functionBody;
      SourceTemplate arrayTemplate;
      // array registers range check
      QList<Registers> arrayRegs = writableRegs.OfArrayType();
      for(int j = 0; j < arrayRegs.count(); j++){
         regs = arrayRegs[j];
         arrayTemplate.setFileName( RANGE_CHECK_ARRAY);
         arrayTemplate.setParameter( "ARRAY_FIRST", CodeHeader::constantIdentifier("modbus reg", group->name, regs.first().name, ""));
         arrayTemplate.setParameter( "ARRAY_LAST", CodeHeader::constantIdentifier("modbus reg", group->name, regs.last().name, ""));
         arrayTemplate.setParameter( "VALUE_MIN", CodeHeader::constantIdentifier("modbus reg", group->name, regs.first().ArrayName(), "Min"));
         arrayTemplate.setParameter( "VALUE_MAX", CodeHeader::constantIdentifier("modbus reg", group->name, regs.first().ArrayName(), "Max"));
         functionBody += arrayTemplate.toString();
      }

      // non array registers range check
      QString caseStatements;
      SourceTemplate caseTemplate;
      regs = writableRegs.OfNonArrayType();
      for(int j = 0; j < regs.count(); j++){
         reg = regs[j];
         caseTemplate.setFileName( RANGE_CHECK_CASE);
         caseTemplate.setParameter("REG_NAME", CodeHeader::constantIdentifier("modbus reg", group->name, reg.name, ""));
         caseTemplate.setParameter("MIN_VALUE", CodeHeader::constantIdentifier("modbus reg", group->name, reg.name, "Min"));
         caseTemplate.setParameter("MAX_VALUE", CodeHeader::constantIdentifier("modbus reg", group->name, reg.name, "Max"));
         caseStatements += caseTemplate.toString() ;
      }
      if( !caseStatements.isEmpty()){
         //some case statements
         SourceTemplate switchTemplate( RANGE_CHECK_SWITCH);
         switchTemplate.setParameter("CASE", caseStatements);
         functionBody += switchTemplate.toString();
      } else {
         functionBody += "\n   return NO;";
      }

      functionTemplate.setParameter( "FUNCTION_BODY", functionBody);
      functionsText += functionTemplate.toString();
   }

   sourceTemplate.setParameter( "FUNCTIONS", functionsText);
   return( sourceTemplate.toString());
}


//------------------------------------------------------------------------------
//  Header 2
//------------------------------------------------------------------------------

QString mGenerator::_GenModbusRegHeader()
// generate source code for ModbusReg.h file
{
int                  groupCount;
RegisterGroup        *group;
Register             reg;
int                  prevNum;
QString              sbRegsConst;
int                  sbRegsCount = 0;
QString              unionsText;
QString              constantsText;
   // get header template :
   SourceTemplate headerTemplate( HEADER_FILE_NAME);
   // enums
   QString enumsText;
   enumsText = "typedef enum{\n";

   groupCount = registersParser->itemsCount();
   for( int i = 0; i < groupCount; i++){
      group = registersParser->groupAt(i);
      int bufferedCount = 0;
      QHash<QString, RegisterGroup> buffered = *group->BufferedSubgroups();

      // constants

      Registers sbRegs = group->RegistersOfType(REGISTER_STRING_BUFFER, REGISTER_W);
      foreach (RegisterGroup g, buffered) {
         bufferedCount += g.Count();
      }
      if(!buffered.isEmpty()){
         constantsText += "\n// Register count definitions for "+group->name+" register group\n";
         constantsText += CodeHeader::constantDeclaration("modbus", group->name, "", "reg write values", QString::number(bufferedCount));
      }
      if(!sbRegs.isEmpty()){
         if(sbRegsCount < sbRegs.count()){
            sbRegsCount = sbRegs.count();
            sbRegsConst = "";
            sbRegsConst += "\n// StringBuffer definition length\n";
            sbRegsConst += CodeHeader::constantDeclaration("_modbus", "string buff", "", "len", QString::number(sbRegs.count()));
         }
      }

      // data types
      SourceTemplate structTemplate;

      // regular types
      if(bufferedCount > 0){
         structTemplate.setFileName( REG_BUFFER_STRUCT);
         structTemplate.setParameter( "NAME", group->name);
         structTemplate.setParameter( "VAR", OName::toCamelCase(group->name));
         structTemplate.setParameter( "SIZE", CodeHeader::constantIdentifier("modbus", group->name, "", "reg write values"));
         unionsText += structTemplate.toString();
      }

      // enums
      enumsText += CodeSeparator::section(group->name + " registers");
      prevNum = -2;
      for(int j = 0; j < group->Count(); j++){
         reg = group->item.at(j);
         enumsText += "   " + CodeHeader::constantIdentifier("modbus reg", group->name, reg.name, "");
         if( prevNum + 1 != reg.num){
            enumsText += " = " + QString::number(reg.num);
         }
         enumsText += ", ";
         if( (reg.num % 10) == 0 && !(prevNum + 1 != reg.num)){
            enumsText += "   // "+ QString::number(reg.num);
         }
         enumsText += "\n";
         prevNum = reg.num;
      }
   }

   QString dataTypesText;
   SourceTemplate bufferTemplate( REG_BUFFER);
   SourceTemplate nonUnionTemplate;
   constantsText += sbRegsConst;

   //string buffer types
   if(!sbRegsConst.isEmpty()){
      nonUnionTemplate.setFileName( REG_BUFFER_NON_UNION);
      nonUnionTemplate.setParameter( "NAME", "CharacterBuffer");
      nonUnionTemplate.setParameter( "VAR", "CharacterBuffer");
      nonUnionTemplate.setParameter( "SIZE", CodeHeader::constantIdentifier("_modbus", "string buff", "", "len"));
      bufferTemplate.setParameter( "NON_UNIONS", nonUnionTemplate.toString());
   }
   if(!unionsText.isEmpty()){
      bufferTemplate.setParameter( "UNIONS", unionsText);
   }
   if(!sbRegsConst.isEmpty() || !unionsText.isEmpty()){
      bufferTemplate.setParameter("DESCRIPTION", "Temporary buffer for string variables and for save commands");
      bufferTemplate.setParameter( "TYPE_NAME", "ModbusRWBuffer");
      dataTypesText += bufferTemplate.toString();
   }

   dataTypesText += "\ntypedef const struct{\n";
   dataTypesText += "   const byte Major;\n";
   dataTypesText += "   const byte Minor;\n";
   dataTypesText += "} TModbusVersion;";

   // data
   QString dataText;
   dataText += "extern TModbusVersion ModbusVersion;\n";
   if(!sbRegsConst.isEmpty() || !unionsText.isEmpty()){
      dataText += "extern TModbusRWBuffer WriteValuesBuffer;";
   }

   // enums
   enumsText.remove(enumsText.lastIndexOf(','), 1);
   enumsText += "} EModbusRegNum;\n\n";

   // functions
   QString functionsText;
   functionsText = CodeSeparator::section("Functions");
   functionsText += CodeSource::functionDeclaration("void",   "ModbusRegsInit",             "",                "init modbus registers");
   functionsText += CodeSource::functionDeclaration("TYesNo", "ModbusRegReadIsCorrectNum",  "EModbusRegNum R", "checks read register number and return YES/NO whether its number is correct or not");
   functionsText += CodeSource::functionDeclaration("word",   "ModbusRegRead",              "EModbusRegNum R", "read register value");
   functionsText += CodeSource::functionDeclaration("TYesNo", "ModbusRegWriteIsCorrectNum", "EModbusRegNum R", "checks write register number and return YES/NO whether its number is correct or not");
   QStringList params;params.append("EModbusRegNum R");params.append("word D");
   functionsText += CodeSource::functionDeclaration("TYesNo", "ModbusRegWrite",              params,           "write register value");


   headerTemplate.setParameter( "NAME", MODBUS_REG_FILENAME);
   headerTemplate.setParameter( "DESCRIPTION", "Modbus register map and access functions");
   headerTemplate.setParameter( "INCLUDE", CodeHeader::includeFile("Unisys", "Uni"));
   headerTemplate.setParameter( "CONSTANTS", constantsText);
   headerTemplate.setParameter( "ENUM", enumsText);
   headerTemplate.setParameter( "DATA_TYPES", dataTypesText);
   headerTemplate.setParameter( "DATA", dataText);
   headerTemplate.setParameter( "FUNCTIONS", functionsText);

   return( headerTemplate.toString());
}

//------------------------------------------------------------------------------
//  Source 2
//------------------------------------------------------------------------------

QString mGenerator::_GenModbusRegSource()
// generate source code for ModbusReg.c file
{
int                  groupCount;
RegisterGroup        *group;
bool                 needBuffer = false;

   SourceTemplate sourceTemplate( SOURCE_FILE_NAME);

   // includes
   QString includesText;
   groupCount = registersParser->itemsCount();
   for( int i = 0; i < groupCount; i++){
      group = registersParser->groupAt(i);
      includesText += CodeSource::includeFileLocal("","Modbus" + OName::toCamelCase(group->name) + "Group");
      if(needBuffer){continue;}
      QHash<QString, RegisterGroup> buffered = *group->BufferedSubgroups();
      Registers sbRegs = group->RegistersOfType(REGISTER_STRING_BUFFER, REGISTER_W);
      if(!buffered.isEmpty() || !sbRegs.isEmpty()){
         needBuffer = true;
      }
   }

   includesText += CodeSource::includeFileGlobal("","string");


   QString globalsText;
   if(needBuffer){
      globalsText += "TModbusRWBuffer WriteValuesBuffer;\n";
   }
   globalsText += "TModbusVersion ModbusVersion = { " +registersParser->getVersionMajor()+", " +registersParser->getVersionMinor()+"};\n";

   //functions
   QString functionsText;
   QString functionBody;
   //init function
   functionsText += CodeSeparator::section("Register init");
   if(needBuffer){
      functionBody   = "memset( &WriteValuesBuffer, 0, sizeof(WriteValuesBuffer));\n";
   }
   functionsText += CodeSource::functionDefinition("void",   "ModbusRegsInit",             "",                "init modbus registers", functionBody);

   // reg read check function
   functionsText += CodeSeparator::section("Check correct read register number");
   functionBody = regCheck(registersParser);
   SourceTemplate ifTemplate( REG_CORRECT_NUM);
   ifTemplate.setParameter("CONDITION", functionBody);
   functionBody = ifTemplate.toString();
   functionsText += CodeSource::functionDefinition("TYesNo", "ModbusRegReadIsCorrectNum",  "EModbusRegNum R", "checks read register number and return YES/NO whether its number is correct or not", functionBody);
   // reg read function
   functionBody = groupCall(registersParser);
   functionsText += CodeSource::functionDefinition("word",   "ModbusRegRead",              "EModbusRegNum R", "read register value", functionBody);
   // reg write check function
   functionsText += CodeSeparator::section("Check correct write register number");
   functionBody = regCheck(registersParser, false);
   ifTemplate.setFileName( REG_CORRECT_NUM);
   ifTemplate.setParameter("CONDITION", functionBody);
   functionBody = ifTemplate.toString();
   functionsText += CodeSource::functionDefinition("TYesNo", "ModbusRegWriteIsCorrectNum", "EModbusRegNum R", "checks write register number and return YES/NO whether its number is correct or not", functionBody);
   //reg write function
   functionBody = groupCall(registersParser, false);
   QStringList params;params.append("EModbusRegNum R");params.append("word D");
   functionsText += CodeSource::functionDefinition("TYesNo", "ModbusRegWrite", params, "write register value", functionBody);

   sourceTemplate.setParameter( "INCLUDE", includesText);
   sourceTemplate.setParameter( "NAME", MODBUS_REG_FILENAME);
   sourceTemplate.setParameter( "DESCRIPTION", "Modbus register map and access functions");
   sourceTemplate.setParameter( "GLOBALS", globalsText);
   sourceTemplate.setParameter( "LOCAL_DECLARATIONS", "");
   sourceTemplate.setParameter( "LOCAL_DEFINITIONS", "");
   sourceTemplate.setParameter("FUNCTIONS", functionsText);
   return sourceTemplate.toString();
}


//------------------------------------------------------------------------------
//  Helper function regCheck
//------------------------------------------------------------------------------

static QString regCheck( ModbusRegParser *registersParser, bool read)
// helper function for ModbusReg.c generation - get condition code for read registers check or write registers check
{
int                  groupCount;
RegisterGroup        *group;
Register             reg;
Register             firstReg;
Register             prevReg;
QString              functionBody;
Registers             regs;

   SourceTemplate conditionTemplate;
   groupCount = registersParser->itemsCount();
   for(int i = 0; i < groupCount; i++){
      group = registersParser->groupAt(i);
      regs = group->RegistersOfType(read?  REGISTER_R :  REGISTER_W, true, true);
      if(regs.isEmpty()){ continue;}
      QString conditionText;
      prevReg = firstReg = regs[0];

      if(regs.count() == 1){
         conditionText = "      // "+group->name+"\n";
         conditionTemplate.setFileName( REG_CORRECT_NUM_CONDITION);
         conditionTemplate.setParameter( "MIN", CodeHeader::constantIdentifier("modbus reg", group->name, firstReg.name, ""));
         conditionTemplate.setParameter( "MAX", CodeHeader::constantIdentifier("modbus reg", group->name, prevReg.name, ""));
         conditionText += conditionTemplate.toString() + " && \n";
      }
      else {
         for(int j = 1; j < regs.count(); j++){
            reg = regs[j];
            if( prevReg.num + 1 != reg.num || j == regs.count() - 1){
               if( conditionText.isEmpty()){
                  conditionText = "      // "+group->name+"\n";
               }
               conditionTemplate.setFileName( REG_CORRECT_NUM_CONDITION);
               conditionTemplate.setParameter( "MIN", CodeHeader::constantIdentifier("modbus reg", group->name, firstReg.name, ""));
               conditionTemplate.setParameter( "MAX", CodeHeader::constantIdentifier("modbus reg", group->name, j == regs.count() - 1? reg.name : prevReg.name, ""));
               firstReg = prevReg = reg;
               conditionText += conditionTemplate.toString() + " && \n";
            } else{
               prevReg = reg;
            }
         }
      }
      functionBody += conditionText;
   }
   if( !functionBody.isEmpty()){
       functionBody.remove(functionBody.count() - 5, 4); //"remove last "&& \n"
   } else {
      functionBody += "1";
   }
   return functionBody;
}

//------------------------------------------------------------------------------
//  Helper function groupCall
//------------------------------------------------------------------------------
static QString groupCall( ModbusRegParser *registersParser, bool read)
// helper function for ModbusReg.c generation - get function call for group register read/write
{
int                  groupCount;
RegisterGroup        *group;
QString              functionBody;
SourceTemplate groupCallTemplate;
Registers            regs;
   groupCount = registersParser->itemsCount();
   for(int i = 0; i < groupCount; i++){
      group = registersParser->groupAt(i);
      regs = group->RegistersOfType(read?  REGISTER_R :  REGISTER_W, true, true);
      if(regs.isEmpty()){continue;}
      groupCallTemplate.setFileName( REG_GROUP_CALL);
      groupCallTemplate.setParameter("REG_NAME", CodeHeader::constantIdentifier("modbus reg", group->name, regs.last().name, ""));
      groupCallTemplate.setParameter("FUNCTION_NAME", OName::toCamelCase( QString("modbus reg ") + QString((read?"read ":"write ")) + group->name));
      groupCallTemplate.setParameter("DATA", read? "": ", D");
      functionBody += groupCallTemplate.toString();
   }

   functionBody += QString("   return ") + QString(read?"0" : "NO") + QString(";\n");
   return functionBody;
}

