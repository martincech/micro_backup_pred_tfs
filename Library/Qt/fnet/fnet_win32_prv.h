#ifndef __FNET_WIN32_PRV_H__
#define __FNET_WIN32_PRV_H__

#include <QNetworkInterface>

class eth_if_t;

typedef void (*isr_handler)(eth_if_t *, const unsigned char *, const int );

class eth_if_t
{
public:
   void              *pcapdev;
   isr_handler       rxhandler;
   QNetworkInterface networkdesc;

   eth_if_t(){
      pcapdev = NULL;
      rxhandler = NULL;
   }
};




eth_if_t *winApiInit(isr_handler interruptHandler);
void winApiFree(eth_if_t *ethif);
void winApiGetIpAddr(eth_if_t *ethif, char *ip_addr);
void winApiGetMacAddr(eth_if_t *ethif, char *mac_addr);
bool winApiIsConnected(eth_if_t *ethif);
bool winApiOutput(eth_if_t *ethif, const unsigned char *data, int length);

#endif // __FNET_WIN32_PRV_H__
