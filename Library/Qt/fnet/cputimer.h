#ifndef CPUTIMER_H
#define CPUTIMER_H

#include <QThread>

class CpuTimerWorker : public QObject
{
    Q_OBJECT
private slots:
    void tick();
};


class CpuTimer : QThread
{
   Q_OBJECT
public:
   CpuTimer(int msec);
   ~CpuTimer();

private:
//   QTimer *timer;
   int timeout;
protected:
   void run();
};


#endif // CPUTIMER_H
