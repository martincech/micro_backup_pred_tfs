#ifndef FNETLOGWINDOW_H
#define FNETLOGWINDOW_H

#include <QMainWindow>
#include <QWaitCondition>
#include <QMutex>

namespace Ui {
   class FNetLogWindow;
}

class FNetLogWindow : public QMainWindow
{
   Q_OBJECT

public:
   explicit FNetLogWindow(QWidget *parent = 0);
   ~FNetLogWindow();
   static void putchar(char character);
   static void countersChanged();
signals:
   void changeCounters();
   void printChar(int c);
private slots:
   void redrawCounters();
   void putCharacter(int c);

private:
   Ui::FNetLogWindow *ui;
   static FNetLogWindow* Instance;
   static QList<QString> ColorCodes;
   static QList<QString> GraphicCodes;
   QMutex logMutex;
   QWaitCondition waitLog;

   void reformat(char c);

};

#endif // FNETLOGWINDOW_H
