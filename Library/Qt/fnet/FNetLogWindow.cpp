#include "FNetLogWindow.h"
#include "ui_FNetLogWindow.h"
#include "fnet_win32.h"
#include "fnet_user_config.h"
#include <QThread>
#include <QScrollBar>
extern "C"{
   void fnet_log(int c){
      FNetLogWindow::putchar((char)c);
   }
}
QList<QString> FNetLogWindow::ColorCodes = QList<QString>();
QList<QString> FNetLogWindow::GraphicCodes = QList<QString>();
FNetLogWindow* FNetLogWindow::Instance = NULL;

FNetLogWindow::FNetLogWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::FNetLogWindow)
{
   ui->setupUi(this);
   if(Instance == NULL){
      Instance = this;
      connect(this, SIGNAL(printChar(int)), this, SLOT(putCharacter(int)));
      /* Foreground color.*/
      ColorCodes.append(QString(FNET_SERIAL_ESC_FG_BLACK));
      ColorCodes.append(QString(FNET_SERIAL_ESC_FG_RED));
      ColorCodes.append(QString(FNET_SERIAL_ESC_FG_GREEN));
      ColorCodes.append(QString(FNET_SERIAL_ESC_FG_YELLOW));
      ColorCodes.append(QString(FNET_SERIAL_ESC_FG_BLUE));
      ColorCodes.append(QString(FNET_SERIAL_ESC_FG_MAGENTA));
      ColorCodes.append(QString(FNET_SERIAL_ESC_FG_CYAN));
      ColorCodes.append(QString(FNET_SERIAL_ESC_FG_WHITE));
      /* Background color.*/
      ColorCodes.append(QString(FNET_SERIAL_ESC_BG_BLACK));
      ColorCodes.append(QString(FNET_SERIAL_ESC_BG_RED));
      ColorCodes.append(QString(FNET_SERIAL_ESC_BG_GREEN));
      ColorCodes.append(QString(FNET_SERIAL_ESC_BG_YELLOW));
      ColorCodes.append(QString(FNET_SERIAL_ESC_BG_BLUE));
      ColorCodes.append(QString(FNET_SERIAL_ESC_BG_MAGENTA));
      ColorCodes.append(QString(FNET_SERIAL_ESC_BG_CYAN));
      ColorCodes.append(QString(FNET_SERIAL_ESC_BG_WHITE));

      /* SGR -- Select Graphic Rendition.*/
      GraphicCodes.append(FNET_SERIAL_ESC_ATTR_RESET);
      GraphicCodes.append(FNET_SERIAL_ESC_ATTR_BOLD);
      GraphicCodes.append(FNET_SERIAL_ESC_ATTR_UNDERLINE);
      GraphicCodes.append(FNET_SERIAL_ESC_ATTR_BLINK);
      GraphicCodes.append(FNET_SERIAL_ESC_ATTR_REVERSE);

   }
   connect(this, SIGNAL(changeCounters()), this, SLOT(redrawCounters()));
}

FNetLogWindow::~FNetLogWindow()
{
    waitLog.wakeAll();
    if(Instance == this){
       disconnect(this, SIGNAL(printChar(int)), this, SLOT(putCharacter(int)));
       Instance = NULL;
       ColorCodes.clear();
       GraphicCodes.clear();
    }
   delete ui;
}

void FNetLogWindow::reformat(char c)
{
static QList<int> lastGraphicCodes;

   // check if character is end of the escape sequence
   if(c != ColorCodes.first().right(1).toStdString().c_str()[0]){
      return;
   }
   QString text = ui->textEdit->toPlainText();
   // COLOR CODES
   int length = ColorCodes.first().length();
   int code = ColorCodes.indexOf(text.mid(text.length() - length));
   if(code != -1){
      for(int i = 0; i< length; i++){
         ui->textEdit->textCursor().deletePreviousChar();
      }
   }
   switch(code){
   /* Foreground color.*/
   case FNET_COLOR_BLACK:
      ui->textEdit->setTextColor(Qt::black);
      break;
   case FNET_COLOR_RED:
      ui->textEdit->setTextColor(Qt::red);
      break;
   case FNET_COLOR_GREEN:
      ui->textEdit->setTextColor(Qt::green);
      break;
   case FNET_COLOR_YELLOW:
      ui->textEdit->setTextColor(Qt::yellow);
      break;
   case FNET_COLOR_BLUE:
      ui->textEdit->setTextColor(Qt::blue);
      break;
   case FNET_COLOR_MAGENTA:
      ui->textEdit->setTextColor(Qt::magenta);
      break;
   case FNET_COLOR_CYAN:
      ui->textEdit->setTextColor(Qt::cyan);
      break;
   case FNET_COLOR_WHITE:
      ui->textEdit->setTextColor(Qt::white);
      break;
      /* Background color.*/
   case FNET_COLOR_WHITE + 1 + FNET_COLOR_BLACK:
      ui->textEdit->setTextBackgroundColor(Qt::black);
      break;
   case FNET_COLOR_WHITE + 1 + FNET_COLOR_RED:
      ui->textEdit->setTextBackgroundColor(Qt::red);
      break;
   case FNET_COLOR_WHITE + 1 + FNET_COLOR_GREEN:
      ui->textEdit->setTextBackgroundColor(Qt::green);
      break;
   case FNET_COLOR_WHITE + 1 + FNET_COLOR_YELLOW:
      ui->textEdit->setTextBackgroundColor(Qt::yellow);
      break;
   case FNET_COLOR_WHITE + 1 + FNET_COLOR_BLUE:
      ui->textEdit->setTextBackgroundColor(Qt::blue);
      break;
   case FNET_COLOR_WHITE + 1 + FNET_COLOR_MAGENTA:
      ui->textEdit->setTextBackgroundColor(Qt::magenta);
      break;
   case FNET_COLOR_WHITE + 1 + FNET_COLOR_CYAN:
      ui->textEdit->setTextBackgroundColor(Qt::cyan);
      break;
   case FNET_COLOR_WHITE + 1 + FNET_COLOR_WHITE:
      ui->textEdit->setTextBackgroundColor(Qt::white);
      break;
   default:
      code = -1;
      break;
   }
   if(code != -1){
      return;
   }

   // GRAPHIC CODE
   length = GraphicCodes.first().length();
   code = GraphicCodes.indexOf(text.mid(text.length() - length));
   if(code != -1){
      for(int i = 0; i< length; i++){
         ui->textEdit->textCursor().deletePreviousChar();
      }
   }

   switch(code){
   /* Foreground color.*/
   case FNET_ATTR_RESET:
      if(!lastGraphicCodes.empty()){
         switch(lastGraphicCodes.first()){
         case FNET_ATTR_BOLD:
            ui->textEdit->insertHtml("</b></html>");
            break;
         case FNET_ATTR_UNDERLINE:
            ui->textEdit->setFontUnderline(false);
            break;
         default:
            break;
         }
         lastGraphicCodes.removeFirst();
      }
      break;
   case FNET_ATTR_BOLD:
      ui->textEdit->insertHtml("<html><b>");
      lastGraphicCodes.append(FNET_ATTR_BOLD);
      break;
   case FNET_ATTR_UNDERLINE:
      ui->textEdit->setFontUnderline(true);
      lastGraphicCodes.append(FNET_ATTR_UNDERLINE);
      break;
   default:
      code = -1;
      break;
   }
}

void FNetLogWindow::countersChanged()
{
   if(Instance == NULL){
      return;
   }
   emit Instance->changeCounters();
}

void FNetLogWindow::redrawCounters(){
fnet_netif_statistics stats;
   if( fnet_netif_get_statistics(fnet_netif_get_default(), &stats) != FNET_ERR){
      Instance->ui->RX->setText(QString::number(stats.rx_packet));
      Instance->ui->TX->setText(QString::number(stats.tx_packet));
   }else{
      Instance->ui->RX->setText(":ERR:");
      Instance->ui->TX->setText(":ERR:");
   }
}

// STATIC
void FNetLogWindow::putchar(char character)
{
   if(Instance != NULL){
      Instance->putCharacter((int)character);
   }

}

// nonstatic
void FNetLogWindow::putCharacter(int c)
{
   if(!ui->allowLog->isChecked()){
      waitLog.wakeAll();
      return;
   }
   logMutex.lock();
   if(QThread::currentThread() != this->thread()){
      emit printChar(c);
      waitLog.wait(&logMutex);
   } else {
      ui->textEdit->insertPlainText(QString((char)c));
      reformat(c);
      QScrollBar *sb = ui->textEdit->verticalScrollBar();
      sb->setValue(sb->maximum());
      waitLog.wakeAll();
   }
   logMutex.unlock();
}
