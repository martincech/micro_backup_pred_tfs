#include "Threads.h"
#include "Multitasking/Multitasking.h"
#include <QDebug>
#include <QMap>

static QString SmsStatusToText(ESmsStatus status){
   QMap <ESmsStatus, QString>stringMap;
   stringMap.insert(SMS_ST_OK_RECEIVED_BY_SME, "SMS_ST_OK_RECEIVED_BY_SME");
   stringMap.insert(SMS_ST_OK_FORWARDED_TO_SME, "SMS_ST_OK_FORWARDED_TO_SME");
   stringMap.insert(SMS_ST_OK_REPLACED_BY_SC, "SMS_ST_OK_REPLACED_BY_SC");
   //TRYING group
   stringMap.insert(SMS_ST_TRYING_CONGESTION, "SMS_ST_TRYING_CONGESTION");
   stringMap.insert(SMS_ST_TRYING_SME_BUSY, "SMS_ST_TRYING_SME_BUSY");
   stringMap.insert(SMS_ST_TRYING_NO_SME_RESPONSE, "SMS_ST_TRYING_NO_SME_RESPONSE");
   stringMap.insert(SMS_ST_TRYING_REJECTED, "SMS_ST_TRYING_REJECTED");
   stringMap.insert(SMS_ST_TRYING_QOS_NOT_AVAILABLE, "SMS_ST_TRYING_QOS_NOT_AVAILABLE");
   stringMap.insert(SMS_ST_TRYING_SME_ERROR, "SMS_ST_TRYING_SME_ERROR");
   //permanent ERROR group
   stringMap.insert(SMS_ST_ERROR_REMOTE_PROCEDURE, "SMS_ST_ERROR_REMOTE_PROCEDURE");
   stringMap.insert(SMS_ST_ERROR_INCOMPATIBLE_DEST, "SMS_ST_ERROR_INCOMPATIBLE_DEST");
   stringMap.insert(SMS_ST_ERROR_REJECTED_BY_SME, "SMS_ST_ERROR_REJECTED_BY_SME");
   stringMap.insert(SMS_ST_ERROR_NOT_OBTAINABLE, "SMS_ST_ERROR_NOT_OBTAINABLE");
   stringMap.insert(SMS_ST_ERROR_QOS_NOT_AVAILABLE, "SMS_ST_ERROR_QOS_NOT_AVAILABLE");
   stringMap.insert(SMS_ST_ERROR_NO_ITERWORKING, "SMS_ST_ERROR_NO_ITERWORKING");
   stringMap.insert(SMS_ST_ERROR_SM_VALIDITY_EXPIRED, "SMS_ST_ERROR_SM_VALIDITY_EXPIRED");
   stringMap.insert(SMS_ST_ERROR_SM_DELETED_BY_SENDER, "SMS_ST_ERROR_SM_DELETED_BY_SENDER");
   stringMap.insert(SMS_ST_ERROR_SM_DELETED_BY_SC, "SMS_ST_ERROR_SM_DELETED_BY_SC");
   stringMap.insert(SMS_ST_ERROR_SM_NOT_EXIST, "SMS_ST_ERROR_SM_NOT_EXIST");
   //temporary ERROR group
   stringMap.insert(SMS_ST_ERROR_TEMP_CONGESTION, "SMS_ST_ERROR_TEMP_CONGESTION");
   stringMap.insert(SMS_ST_ERROR_TEMP_SME_BUSY, "SMS_ST_ERROR_TEMP_SME_BUSY");
   stringMap.insert(SMS_ST_ERROR_TEMP_NO_SME_RESPONSE, "SMS_ST_ERROR_TEMP_NO_SME_RESPONSE");
   stringMap.insert(SMS_ST_ERROR_TEMP_REJECTED, "SMS_ST_ERROR_TEMP_REJECTED");
   stringMap.insert(SMS_ST_ERROR_TEMP_QOS_NOT_AVAILABLE, "SMS_ST_ERROR_TEMP_QOS_NOT_AVAILABLE");
   stringMap.insert(SMS_ST_ERROR_TEMP_SME_ERROR, "SMS_ST_ERROR_TEMP_SME_ERROR");
   //unknown
   stringMap.insert(SMS_ST_UNKNOWN, "SMS_ST_UNKNOWN");
   return stringMap[status];
}

static QString SendStatusToText( EChannelStatus status){
   QMap <EChannelStatus, QString>stringMap;
   stringMap.insert(GSM_SMS_SEND_OK, "GSM_SMS_SEND_OK");
   stringMap.insert(GSM_SMS_SEND_WAIT_START, "GSM_SMS_SEND_WAIT_START");
   stringMap.insert(GSM_SMS_SEND_WAIT_FIRST_ACK, "GSM_SMS_SEND_WAIT_FIRST_ACK");
   stringMap.insert(GSM_SMS_SEND_SENDING, "GSM_SMS_SEND_SENDING");
   stringMap.insert(GSM_SMS_SEND_RESENDING, "GSM_SMS_SEND_RESENDING");
   stringMap.insert(GSM_SMS_SEND_WAIT_SOME_ACK, "GSM_SMS_SEND_WAIT_SOME_ACK");
   stringMap.insert(GSM_SMS_SEND_ERROR, "GSM_SMS_SEND_ERROR");
   stringMap.insert(GSM_SMS_SEND_ERROR_INTERNAL, "GSM_SMS_SEND_ERROR_INTERNAL");
   stringMap.insert(GSM_SMS_SEND_TIMEOUT, "GSM_SMS_SEND_TIMEOUT");
   stringMap.insert(GSM_SMS_SEND_STOPPED, "GSM_SMS_SEND_STOPPED");
   stringMap.insert(GSM_SMS_SEND_WRONG_CHANNEL_DESC, "GSM_SMS_SEND_WRONG_CHANNEL_DESC");
   stringMap.insert(GSM_SMS_SEND_RESEND_FIRST, "GSM_SMS_SEND_RESEND_FIRST");
   return stringMap[status];
}

//------------------------------------------------------------------------------
// Thread interface
//------------------------------------------------------------------------------

void Thread::sleep(int secs)
{
   static_cast<SleepThread *>(QThread::currentThread())->sleep(secs);
}

void Thread::finish()
{
   emit done();
   this->thread()->quit();
}

void Thread::stop()
{
   this->terminate = true;
   this->thread()->exit();
}

//------------------------------------------------------------------------------
// Init
//------------------------------------------------------------------------------

InitThread::InitThread(){
   operatorName[0] = '\0';
   signalStrength = 0;
   registered = false;
   smsInited = false;
}

void InitThread::doWork()
{
   terminate = false;
   emit progressRange(0, 5);
   emit progress(0);
   while(!terminate && !GsmReset()){
      sleep(1);
   }
   if(terminate){return;}
   emit progress(1);
   while(!terminate && !GsmRegistered()){
      sleep(1);
   }
   if(terminate){return;}
   registered = true;
   emit progress(2);
   while(!terminate && !GsmOperator(operatorName)){
      sleep(1);
   }
   emit progress(3);
   if(terminate){return;}
   signalStrength = GsmSignalStrength();
   emit progress(4);
   if(terminate){return;}
   smsInited = GsmSmsInit();
   emit progress(5);
   finish();
}

//------------------------------------------------------------------------------
// Send
//------------------------------------------------------------------------------

SmsSendThread::SmsSendThread()
{
   phoneNumber[0] = '\0';
   smsText[0] = '\0';
}

void SmsSendThread::doWork()
{
   emit progressRange(0,1);
   emit progress(0);
   GsmSmsSend(phoneNumber, smsText);
   emit progress(1);
   finish();
}

//------------------------------------------------------------------------------
// Send command
//------------------------------------------------------------------------------
extern "C" {
   TYesNo _GsmSmsCommand(TSmsId *id, ESmsCommandType ct);
}

CommandSendThread::CommandSendThread()
{
   phoneNumber[0] = '\0';
   ct = SMS_CT_ENQUIRY;
}

void CommandSendThread::doWork()
{
TSmsId id;

   _PhoneToAddress(phoneNumber, id.Da);
   id.Mr = mr;
   emit progressRange(0,1);
   emit progress(0);
   _GsmSmsCommand(&id, ct);
   emit progress(2);
   finish();
}
//------------------------------------------------------------------------------
// Read
//------------------------------------------------------------------------------

SmsReadThread::SmsReadThread()
{
   phoneNumber[0] = '\0';
   smsText[0] = '\0';
}

void SmsReadThread::doWork()
{
UDateTimeGauge dtg;
UDateTime dt;

   emit progressRange(0,1);
   emit progress(0);
   status = GsmSmsRead(phoneNumber, smsText, &dtg);
   uDateTime(&dt, dtg);
   timestamp.setDate(QDate(uYearGet(dt.Date.Year), dt.Date.Month, dt.Date.Day));
   timestamp.setTime(QTime(dt.Time.Hour, dt.Time.Min, dt.Time.Sec));
   emit progress(1);
   finish();
}
//------------------------------------------------------------------------------
// Read status
//------------------------------------------------------------------------------

StatusReadThread::StatusReadThread()
{
   status = SMS_ST_UNKNOWN;
}

QString StatusReadThread::getStatus()
{
   return SmsStatusToText(status);
}

void StatusReadThread::doWork()
{
TSmsId st;
UDateTime dt;
int Length;

   emit progressRange(0,1);
   emit progress(0);
   status = SMS_ST_UNKNOWN;
   if(_GsmStatusRead( &st)){
      uDateTime(&dt, st.TimeStamp);
      timestamp.setDate(QDate(uYearGet(dt.Date.Year), dt.Date.Month, dt.Date.Day));
      timestamp.setTime(QTime(dt.Time.Hour, dt.Time.Min, dt.Time.Sec));
      mr = st.Mr;
      strcpy(smsText, "STATUS_REPORT_SM");
      Length = strlen(smsText);
      _AddressToPhone(st.Da, phoneNumber);
      status = (ESmsStatus)st.Status;
   }

   emit progress(1);
   finish();
}


//------------------------------------------------------------------------------
// Channel executive
//------------------------------------------------------------------------------
extern TSmsChannelDesc *DescHead;
extern TSmsIChannelHistory *DescHistory;
#include "System/System.h"                 // Sysclock

static QStringList ShowChannel(TSmsChannelDesc *desc)
{
QStringList slist;
double d;

   //number
   slist << QString("%1").arg( desc->Descriptor);
   //dir
   if( desc->Direction == SMS_CHANNEL_INPUT){
      slist << "IN";
   } else {
      slist << "OUT";
   }
   //ack channel
   if( desc->AckChannel == SMS_CHANNEL_ACK){
      slist << "YES";
   } else {
      slist << "NO";
   }
   //send status
   if( desc->Direction == SMS_CHANNEL_INPUT){
      slist << "---";
   } else {
      slist << SendStatusToText((EChannelStatus)desc->SendStatus);
   }
   //chunks
   slist << QString("%1").arg( desc->ChunkCount);
   //chunkmax
   slist << QString("%1").arg( desc->ChunkMaxCount);
   //delay
   slist << QString("%1/%2").arg( desc->Delay*60).arg(desc->Delay);
   //rest time
   d = abs(SysClock() - desc->Timer);
   slist << QString("%1/%2").arg(d).arg(d/60);
   //intra
   slist << QString("%1/%2").arg( desc->Intra*60).arg(desc->Intra);
   //retrycount
   slist << QString("%1").arg( desc->RetryCount);
   //last sms
   slist << QString("%1").arg( desc->LastSmsTimeStamp);
   //chunks
   QString s;
   for(int i = 0; i < desc->ChunkMaxCount; i++){
      s += QString("%1\n").arg(i)+QString("  MR: ") + QString::number( desc->Chunks[i].Mr) + QString("\n");
      s += QString("  ") + SmsStatusToText((ESmsStatus)desc->Chunks[i].Status) + QString("\n");
   }
   slist << s;
   return slist;
}

static QStringList ShowHistory( TSmsIChannelHistory *hist){
QStringList slist;
char phone[30];
double d;

   slist << QString( "History");
   _AddressToPhone( hist->Address, phone);
   slist << QString(phone);
   slist << "RN:" <<QString::number(hist->RN);
   slist << "TN:" <<QString::number(hist->TN);
   slist << "" << "" << "";
   d = abs(SysClock() - hist->Timer);
   slist << QString("%1/%2").arg(d).arg(d/60);
   return slist;
}

SmsChannelExecutive::SmsChannelExecutive()
{
   GsmSmsChannelInit();
   OutChannelsModel = new ArrayModel(0, 3);
   OutChannelsModel->setTitle(0, "Number");
   OutChannelsModel->setTitle(1, "Status");
   OutChannelsModel->setTitle(2, "Alive");

   IncChannelsModel = new ArrayModel(0, 5);
   IncChannelsModel->setTitle(0, "Number");
   IncChannelsModel->setTitle(1, "From");
   IncChannelsModel->setTitle(2, "Data len");
   IncChannelsModel->setTitle(3, "Data");
   IncChannelsModel->setTitle(4, "Info");

   ChannelsModel = new ArrayModel(0,12);
   ChannelsModel->setTitle(0, "Number");
   ChannelsModel->setTitle(1, "Dir");
   ChannelsModel->setTitle(2, "Ack channel");
   ChannelsModel->setTitle(3, "Send status");
   ChannelsModel->setTitle(4, "Chunks");
   ChannelsModel->setTitle(5, "ChunksMax");
   ChannelsModel->setTitle(6, "Delay[s/min]");
   ChannelsModel->setTitle(7, "Rest time[s/min]");
   ChannelsModel->setTitle(8, "Intra delay[s/min]");
   ChannelsModel->setTitle(9, "Retries last");
   ChannelsModel->setTitle(10, "Last timestamp");
   ChannelsModel->setTitle(11, "Chunks");
}

SmsChannelExecutive::~SmsChannelExecutive()
{
   GsmSmsChannelDeinit();
}

void SmsChannelExecutive::doWork()
{
EChannelStatus status;
TSmsChannel chan;
char Phone[50];
int Size;
int i;
QStringList slist;
TSmsChannelDesc *desc;
TSmsIChannelHistory *hist;

   terminate = false;
   while(!terminate){
      GsmSmsChannelExecutive();
      // all channels model change
      sleep(1);
      desc = DescHead;
      i = 0;
      while(desc){
         i++;
         if( desc->next != NULL){
            desc = desc->next;
         }else {
            break;
         }
      }

      hist = DescHistory;
      while(hist){
         i++;
         hist = hist->next;
      }
      ChannelsModel->setRowsCount(i);
      i = 0;
      while(desc){
         QThread::yieldCurrentThread();
         slist = ShowChannel(desc);
         ChannelsModel->setRow(i, slist);
         i++;
         desc = desc->prev;
      }
      // history
      hist = DescHistory;
      while(hist){
         QThread::yieldCurrentThread();
         slist = ShowHistory(hist);
         ChannelsModel->setRow(i, slist);
         i++;
         hist = hist->next;
      }

      //out model change
      for(i = 0; i < channels.size(); i++){
         QThread::yieldCurrentThread();
         if(channelsAlive[i] &&
            (status = (EChannelStatus)GsmSmsChannelSendStatus(channels[i])) != GSM_SMS_SEND_WRONG_CHANNEL_DESC){
            channelsStatus[i] = status;
         } else {
            channelsAlive[i] = false;
         }
         QStringList slist;
         slist << QString("%1").arg((int)channels[i]);
         slist << QString("%1").arg(SendStatusToText((EChannelStatus)channelsStatus[i]));
         slist << QString(channelsStatus[i]?"YES":"NO");
         OutChannelsModel->setRow( i,slist);
      }

      //inc model change
      chan = GsmSmsChannelIncomming(Phone, &Size);
      if(chan != SMS_INVALID_CHANNEL){
         QThread::yieldCurrentThread();
         slist.clear();
         slist << QString("%1").arg(chan);
         slist << Phone;
         slist << QString("%1").arg(Size);
         QByteArray ba;
         ba.reserve(Size+1);
         i = GsmSmsChannelRead( chan, (byte *)ba.data(), Size);
         ba.data()[Size] = '\0';
         slist << QString(ba.data());
         if(i != Size){
            slist << "readsize != incsize";
         }else{
            slist << "OK";
         }
         IncChannelsModel->addRow(slist);

      }
      QThread::yieldCurrentThread();
   }
}

bool SmsChannelExecutive::RegisterStatusCheck(TSmsChannel chan)
{
   if( channels.contains(chan)){

   }

   channels.append(chan);
   channelsStatus.append(SMS_ST_UNKNOWN);
   channelsAlive.append(true);
   QStringList slist;
   slist << QString("%1").arg((int)channels.last());
   slist << QString("%1").arg(SendStatusToText((EChannelStatus)channelsStatus.last()));
   slist << QString(channelsStatus.last()?"YES":"NO");
   OutChannelsModel->addRow( slist);
   return true;
}

//------------------------------------------------------------------------------
// Channel simple executive
//------------------------------------------------------------------------------

SmsChannelSimpleExecutive::SmsChannelSimpleExecutive()
{
   GsmSmsChannelInit();

   ChannelsModel = new ArrayModel(0,12);
   ChannelsModel->setTitle(0, "Number");
   ChannelsModel->setTitle(1, "Dir");
   ChannelsModel->setTitle(2, "Ack channel");
   ChannelsModel->setTitle(3, "Send status");
   ChannelsModel->setTitle(4, "Chunks");
   ChannelsModel->setTitle(5, "ChunksMax");
   ChannelsModel->setTitle(6, "Delay[s/min]");
   ChannelsModel->setTitle(7, "Rest time[s/min]");
   ChannelsModel->setTitle(8, "Intra delay[s/min]");
   ChannelsModel->setTitle(9, "Retries last");
   ChannelsModel->setTitle(10, "Last timestamp");
   ChannelsModel->setTitle(11, "Chunks");
}

SmsChannelSimpleExecutive::~SmsChannelSimpleExecutive()
{
   GsmSmsChannelDeinit();
}

void SmsChannelSimpleExecutive::doWork()
{
int i;
QStringList slist;
TSmsChannelDesc *desc;
TSmsIChannelHistory *hist;

   terminate = false;
   while(!terminate){
      GsmSmsChannelExecutive();
      // all channels model change
      sleep(1);
      desc = DescHead;
      i = 0;
      while(desc){
         i++;
         if( desc->next != NULL){
            desc = desc->next;
         }else {
            break;
         }
      }

      hist = DescHistory;
      while(hist){
         i++;
         hist = hist->next;
      }
      ChannelsModel->setRowsCount(i);
      i = 0;
      while(desc){
         QThread::yieldCurrentThread();
         slist = ShowChannel(desc);
         ChannelsModel->setRow(i, slist);
         i++;
         desc = desc->prev;
      }
      // history
      hist = DescHistory;
      while(hist){
         QThread::yieldCurrentThread();
         slist = ShowHistory(hist);
         ChannelsModel->setRow(i, slist);
         i++;
         hist = hist->next;
      }
   }
}
