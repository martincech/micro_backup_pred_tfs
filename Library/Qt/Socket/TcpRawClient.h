//******************************************************************************
//
//   TcpRawClient.h  Reimplements TCP client to send/receive raw data
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef TCPRAWCLIENT_H
#define TCPRAWCLIENT_H

#include "tcpclient.h"
#include "tcpserver.h"

class TcpRawClient : public TcpClient
{
public:
   TcpRawClient();
   TcpRawClient(const TcpRawClient &copy);
   virtual ~TcpRawClient();

   bool send( void *data, int size);
   // Send data to client

   int receive( void *data, int size);
   // Receive <data> with total <size>, returns size received
};

class TcpRawServer : public TcpServer
{
public:
   TcpRawServer();
   TcpRawServer(const TcpRawServer &copy);
   virtual ~TcpRawServer();

   bool send( void *data, int size);
   // Send data to client

   int receive( void *data, int size);
   // Receive <data> with total <size>, returns size received
};

#endif // TCPRAWCLIENT_H
