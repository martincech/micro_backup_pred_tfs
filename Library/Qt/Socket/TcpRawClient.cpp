//******************************************************************************
//
//   TcpRawClient.h  Reimplements TCP client to send/receive raw data
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "TcpRawClient.h"

#define LoggerTx( frame, size)       if( _logger) _logger->show( CrtDump::TX, frame, size)
#define LoggerRx( frame, size)       if( _logger) _logger->show( CrtDump::RX, frame, size)
#define LoggerGarbage( frame, size)  if( _logger) _logger->show( CrtDump::GARBAGE, frame, size)
#define LoggerReport( txt, ...)      if( _logger) _logger->printf( txt, ##__VA_ARGS__)


#define SOCKET_RX_TIMEOUT 20000

TcpRawClient::TcpRawClient() : TcpClient()
{
}

TcpRawClient::TcpRawClient(const TcpRawClient &copy) : TcpClient(copy)
{

}

TcpRawClient::~TcpRawClient()
{
}

TcpRawServer::TcpRawServer() : TcpServer()
{
}

TcpRawServer::TcpRawServer(const TcpRawServer &copy) : TcpServer()
{

}

TcpRawServer::~TcpRawServer()
{
}

//------------------------------------------------------------------------------
//   Send
//------------------------------------------------------------------------------
#define RAW_SEND() \
                   if( size > SOCKET_MESSAGE_SIZE_MAX){\
                     return( false);                   \
                   }                                   \
                   if( !_socket){                      \
                     return( false);                   \
                   }                                   \
                   memcpy( _message, data, size);      \
                   qint64 sizeWritten;                 \
                   sizeWritten = _socket->write( (const char *)_message, size); \
                   if( sizeWritten != size){           \
                     LoggerReport( "Send size mismatch\n"); \
                     return( false);                   \
                   }                                   \
                   _socket->flush();                   \
                   _receivedSize = 0;                  \
                   LoggerTx( _message, sizeWritten);   \
                   return( true)

//------------------------------------------------------------------------------
//  Receive
//------------------------------------------------------------------------------

#define RAW_RCV() \
                  if( !_socket){                                              \
                     return( 0);                                              \
                  }                                                           \
                  if( !_socket->waitForReadyRead( SOCKET_RX_TIMEOUT)){        \
                     return( 0);                                              \
                  }                                                           \
                  qint64 availableSize;                                       \
                  availableSize = _socket->bytesAvailable();                  \
                  if( availableSize == 0){                                    \
                     return( 0);                                              \
                  }                                                           \
                  qint64  readSize;                                           \
                  readSize = _socket->read( (char *)&_message, availableSize);\
                  if( readSize != availableSize){                             \
                     LoggerReport( "Message body size mismatch\n");           \
                     _receivedSize = 0;                                       \
                     return( 0);                                              \
                  }                                                           \
                  if( readSize > size){                                       \
                     LoggerReport( "Message too large! Max: %d, Act: %d\n", size, readSize);\
                     _receivedSize = 0;                                       \
                     return( 0);                                              \
                  }                                                           \
                  memcpy( data, &_message[ 0], (size_t)readSize);             \
                  LoggerRx( _message, readSize);                              \
                  _receivedSize = 0;                                          \
                  return( (int)readSize)

//------------------------------------------------------------------------------
//   Send
//------------------------------------------------------------------------------

bool TcpRawClient::send( void *data, int size)
// Send data to client
{
   RAW_SEND();
}

//------------------------------------------------------------------------------
//   Receive
//------------------------------------------------------------------------------

int TcpRawClient::receive( void *data, int size)
// Receive <data> with total <size>, returns size received
{
   RAW_RCV();
}

//------------------------------------------------------------------------------
//   Send
//------------------------------------------------------------------------------

bool TcpRawServer::send( void *data, int size)
// Send data to client
{
   RAW_SEND();
}

//------------------------------------------------------------------------------
//   Receive
//------------------------------------------------------------------------------

int TcpRawServer::receive( void *data, int size)
// Receive <data> with total <size>, returns size received
{
   RAW_RCV();
}
