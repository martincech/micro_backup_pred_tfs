//******************************************************************************
//
//   SocketDef.h  Socket data definition
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef SOCKETDEF_H
#define SOCKETDEF_H

#define SOCKET_MESSAGE_SIZE_MAX  4094
#define SOCKET_HEADER_SIZE       2
#define SOCKET_PORT              8080

typedef quint16 SocketMessageSize;

// Message format :
// <Size LSB> <Size MSB> <data with Size>

// total message length :
#define SocketMessageLength( Size) ((Size) + sizeof( SocketMessageSize))

// message header lenght :
#define SocketHeaderLenght()       (sizeof( SocketMessageSize))

#endif // SOCKETDEF_H
