//*****************************************************************************
//
//    Fw.c           Firmware
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#include "Fw.h"
#include "Crypt/Crypt.h"
#include "Fw/FwDef.h"
#include "Flash/IFlash.h"
#include <string.h>

static TYesNo Done;        // Fw processing done
static TFwDataCrc DataCrc; // Fw Crc
static TApplicationHeader ApplicationHeader; // Application header

void MagicValueGet( byte *Magic);
// Get magic value

static TYesNo MagicSet( void);
// Set magic in flash

#define CHUNK_SIZE_MAX     64

static byte Buf[sizeof(TFwCmdCrc) + CHUNK_SIZE_MAX];
static TFwCmdCrc *CmdCrc = (TFwCmdCrc *)Buf;

static int CurrentLength;
static TYesNo TestRun;
static int NextAddress;
//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void FwInit( TYesNo _TestRun)
// Inits processing
{
   TestRun = _TestRun;
   FwDataCrcStart( &DataCrc);
   Done = NO;
   CurrentLength = 0;
   NextAddress = 0xFFFFFFFF; // invalidate
   memset(&ApplicationHeader, 0xFF, sizeof(ApplicationHeader));
} // FwInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

byte FwExecute( void *Buffer, int Length)
// Exexute with new data, returns status
{
word i;
word CmdLength;
TApplicationHeader *Header;
dword DataHeaderStart;
dword HeaderStart;
dword HeaderCount;
byte Encrypted[CHUNK_SIZE_MAX];
int EncryptedSize;
byte *Data;
TFwCrc Crc;
int RemainingLength;
   if(Done) {
      return FW_STATUS_DONE;
   }

   if(Length > CHUNK_SIZE_MAX) {
      return NO;
   }

   EncryptedSize = EncryptSize(Length);

   if(EncryptedSize > Length) {
      return NO;
   }
   memcpy(Encrypted, Buffer, Length);
   //Decrypt(Encrypted, Length);

   memcpy((byte *)CmdCrc + CurrentLength, Encrypted, EncryptedSize);
   CurrentLength += EncryptedSize;

   if(CurrentLength < sizeof(CmdCrc->Length)) {
      return FW_STATUS_PROCESSING;
   }
   if(FwCrcSize( CmdCrc->Length) > sizeof(*CmdCrc)) {
      return NO;
   }
   if(CurrentLength < FwCrcSize( CmdCrc->Length)) {
      return FW_STATUS_PROCESSING;
   }

   Data = (byte *)&CmdCrc->Cmd;
   FwCrcStart(&Crc);
   for(i = 0; i < CmdCrc->Length; i++) {
      FwCrcProcess( &Crc, *Data);
      Data++;
   }
   FwCrcEnd(&Crc);
   /*int a = &CmdCrc;
   int b = sizeof(CmdCrc.Length);
   int c = CmdCrc.Length;
   TFwCrc *RealCrc = (a + b + c );
   TFwCrc abc = ;*/
   if(Crc != FwCrc(*CmdCrc)) {
      return NO;
   }

   switch(CmdCrc->Cmd.Cmd) {
      case FW_NONSENSE:
         if(CmdCrc->Length != FwSizeOf( NonSense)) {
            return NO;
         }
         break;

      case FW_VERSION:
         if(CmdCrc->Length != FwSizeOf( Version)) {
            return NO;
         }
         if(!FwVersionCheck(&CmdCrc->Cmd.Data.Version)) {
            return NO;
         }
         if(!TestRun) {
            IFlashErase(OFFSETOF_APPLICATION, OFFSETOF_APPLICATION + APPLICATION_SIZE - 1);
         }
         
         NextAddress = OFFSETOF_APPLICATION;
         break;

      case FW_DATA:
         if(CmdCrc->Length != FwSizeOfData( CmdCrc->Cmd.Data.Data.Count)) {
            return NO;
         }
         int Address = CmdCrc->Cmd.Data.Data.Address;
         if(NextAddress != Address) {
            return NO;
         }

         for(i = 0 ; i < CmdCrc->Cmd.Data.Data.Count ; i++) {
            FwDataCrcProcess(&DataCrc, CmdCrc->Cmd.Data.Data.Data[i]);
         }

         if(NextAddress == OFFSETOF_APPLICATION) {
            // Store application header
            memcpy((byte *)&ApplicationHeader, (byte *)CmdCrc->Cmd.Data.Data.Data, sizeof(TApplicationHeader));
            memset((byte *)CmdCrc->Cmd.Data.Data.Data, 0xFF, sizeof(TApplicationHeader));
         }

         if(!TestRun) {
            if(!IFlashProgram(CmdCrc->Cmd.Data.Data.Address, CmdCrc->Cmd.Data.Data.Data, CmdCrc->Cmd.Data.Data.Count)) {
               return NO;
            }
         }
         NextAddress += CmdCrc->Cmd.Data.Data.Count;
         break;

      case FW_TERMINATOR:
         if(CmdCrc->Length != FwSizeOf( Terminator)) {
            return NO;
         }
         FwDataCrcEnd( &DataCrc);
         if(DataCrc != CmdCrc->Cmd.Data.Terminator.Crc) {
            return NO;
         }
         if(!TestRun) {
            if(!IFlashProgram(OFFSETOF_APPLICATION_HEADER, (void *)&ApplicationHeader, sizeof(ApplicationHeader))) {
               return NO;
            }
         }
         Done = YES;
         return FW_STATUS_DONE;

      default:
         return NO;
   }
   RemainingLength = CurrentLength - FwCrcSize( CmdCrc->Length);
   memcpy(CmdCrc, (byte *)CmdCrc + FwCrcSize( CmdCrc->Length), RemainingLength);
   CurrentLength = RemainingLength;
   return FW_STATUS_PROCESSING;
} // FwExecute

//-----------------------------------------------------------------------------
// Validate
//-----------------------------------------------------------------------------

TYesNo FwValidate( void)
// Validate Fw 
{
TApplicationHeader *ApplicationHeader = (TApplicationHeader *)OFFSETOF_APPLICATION_HEADER;

   if(ApplicationHeader->ResetVector == 0xFFFFFFFF) {
      return NO;
   }

   return YES;
} // FwValidate



void FwApplicationRun( void) {
   __asm__ __volatile__ (
      "ldr sp, [%0]\n\t"
      "ldr r5, [%1]\n\t"
      "blx r5\n\t"
      "" : : "r" (OFFSETOF_APPLICATION_HEADER + offsetof(TApplicationHeader, StackPointer)), "r" (OFFSETOF_APPLICATION_HEADER + offsetof(TApplicationHeader, ResetVector)));
}