//*****************************************************************************
//
//    SMBusSlave.h           SMBus
//    Version 1.0            (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SMBusSlave_H__
   #define __SMBusSlave_H__

#include "Unisys/Uni.h"

void SMBusSlaveInit( void);
// Init

void SMBusSlaveWriteWord(word Data);
// Use only synchronously with SMBus callbacks

void SMBusSlaveWriteBlock(void *Data, int Size);
// Use only synchronously with SMBus callbacks

void SMBusSlaveReadWord( word *Data);
// Use only synchronously with SMBus callbacks

void SMBusSlaveReadBlock(void *Data, int MaxSize);
// Use only synchronously with SMBus callbacks


void SMBusSlaveByteProcess( byte Data);

TYesNo SMBusSlaveByteGet( byte *Data);

void SMBusSlaveReceiving( void);

void SMBusSlaveSending( void);

void SMBusSlaveSuccess( void);

void SMBusSlaveError( void);

int SMBusSlaveReadLength( void);
// Read length

#endif