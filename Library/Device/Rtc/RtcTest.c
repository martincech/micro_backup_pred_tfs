//*****************************************************************************
//
//    RtcTest.c   Real-time clock device test
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#include "Rtc/Rtc.h"
#include "System/System.h"        // SysDelay

//------------------------------------------------------------------------------
//   RTC test
//------------------------------------------------------------------------------

#define RTC_TEST_TIME  3

TYesNo RtcTest( void)
// Run RTC test
{
UDateTime      Local;
UDateTimeGauge Start, Now, Delta;

   Local.Date.Day   = 11;
   Local.Date.Month = 12;
   Local.Date.Year  = 13;
   Local.Time.Hour  = 14;
   Local.Time.Min   = 15;
   Local.Time.Sec   = 16;
   Start = uDateTimeGauge( &Local);
   RtcSave( Start);
   SysDelay( RTC_TEST_TIME * 1000);
   Now = RtcLoad();
   Delta = Now - Start;
   if( Delta < RTC_TEST_TIME - 1){
      return( NO);
   }
   if( Delta > RTC_TEST_TIME + 1){
      return( NO);
   }
   return( YES);
} // RtcTest
