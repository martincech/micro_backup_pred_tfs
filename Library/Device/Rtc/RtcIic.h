//*****************************************************************************
//
//    RtcIic.c     Realtime clock I2C controller
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __RtcIic_H__
   #define __RtcIic_H__
   
//--- Default I2C access
#include "Iic/Iic.h"

#define iicRtcInit()                IicInit()
// Interface initialisation

#define iicRtcStart()               IicStart()
// Sends start sequence

#define iicRtcStop()                IicStop()
// Sends stop sequence

#define iicRtcSend( Value)          IicSend( Value)
// Sends <value>, returns YES if ACK

#define iicRtcReceive( Ack)         IicReceive( Ack)
// Returns received byte, sends confirmation <ack>.
// <ack> = YES sends ACK, NO sends NOT ACK

#define iicRtcWrite( Buffer, Size)  IicWrite( Buffer, Size)
// Sends <Size> bytes from <Buffer>

#define iicRtcRead( Buffer, Size)   IicRead( Buffer, Size)
// Receives of <Size> bytes into <Buffer>

#endif
