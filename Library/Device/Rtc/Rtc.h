//*****************************************************************************
//
//    Rtc.h        Real time clock
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Rtc_H__
   #define __Rtc_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __uDateTime_H__
   #include "Time/uDateTime.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

void RtcInit( void);
// Initialisation

UDateTimeGauge RtcLoad( void);
// Returns RTC time

void RtcSave( UDateTimeGauge DateTime);
// Set RTC time by <DateTime>

TYesNo RtcInvalid( void);
// is invalid time?

#ifdef __cplusplus
   }
#endif

#endif
