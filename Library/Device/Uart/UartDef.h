//*****************************************************************************
//
//    UartDef.h     RS232/485 definitions
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __UartDef_H__
   #define __UartDef_H__


// Format :
// do not change this enum ordering!
typedef enum {
   UART_7BIT,                // 7bit without parity
   UART_7BIT_EVEN,           // 7bit even parity
   UART_7BIT_ODD,            // 7bit odd parity
   UART_7BIT_MARK,           // 7bit const parity - 2 stopbits
   UART_7BIT_SPACE,          // 7bit constant 0 parity
   UART_8BIT,                // 8bit without parity
   UART_8BIT_EVEN,           // 8bit even parity
   UART_8BIT_ODD,            // 8bit odd parity
   UART_8BIT_MARK,           // 8bit constant parity - 2 stopbits
   UART_8BIT_SPACE,          // 8bit constant 0 parity
   UART_9BIT,                // 9 bit without parity
   _UART_FORMAT_LAST
} EUartFormat;

#define UART_BAUD_MIN  1200            // slightes useful value
#define UART_BAUD_MAX  115200          // probably PC limited value

typedef enum {
   UART_FLOW_CONTROL_NONE = 0x00,
   UART_FLOW_CONTROL_HARDWARE = 0x10
} EUartFlowControl;

#endif
