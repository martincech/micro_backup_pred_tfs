//*****************************************************************************
//
//    Adp5062.h    Linear Li-ion battery charger
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Adp5062_H__
   #define __Adp5062_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef enum {
   ADP5062_CHARGER_OFF,
   ADP5062_CHARGER_TRICKLE,
   ADP5062_CHARGER_FAST_CC,
   ADP5062_CHARGER_FAST_CV,
   ADP5062_CHARGER_COMPLETE,
   ADP5062_CHARGER_LDO,
   ADP5062_CHARGER_TIMER_EXPIRED,
   ADP5062_CHARGER_BATTERY_DETECTION
} EAdp5062ChargerStatus;

typedef struct {
   TYesNo OverVoltage;
   TYesNo VoltageOk;
   TYesNo CurrentLimited;
   TYesNo ThermalLimited;
   TYesNo Charged;
   byte ChargerStatus;
} TAdp5062ChargerStatus;

typedef enum {
   ADP5062_BATTERY_THERMAL_OFF,
   ADP5062_BATTERY_THERMAL_COLD,
   ADP5062_BATTERY_THERMAL_COOL,
   ADP5062_BATTERY_THERMAL_WARM,
   ADP5062_BATTERY_THERMAL_HOT,
   ADP5062_BATTERY_THERMAL_OK = 7
} EAdp5062BatteryThermalStatus;

typedef enum {
   ADP5062_BATTERY_MONITOR_OFF,
   ADP5062_BATTERY_NO_BATTERY,
   ADP5062_BATTERY_DEAD,
   ADP5062_BATTERY_WEAK,
   ADP5062_BATTERY_OK
} EAdp5062BatteryStatus;

typedef struct {
   byte ThermalStatus;
   byte Status;
   TYesNo Shorted;
} TAdp5062BatteryStatus;

typedef enum {
   ADP5062_INTERRUPT_INPUT_VOLTAGE = 0x01,
   ADP5062_INTERRUPT_CHARGER_MODE = 0x02,
   ADP5062_INTERRUPT_BATTERY_THRESHOLD = 0x04,
   ADP5062_INTERRUPT_TEMPERATURE = 0x08,
   ADP5062_INTERRUPT_OVERTEMPERATURE = 0x10,
   ADP5062_INTERRUPT_WATCHDOG = 0x20,
   ADP5062_INTERRUPT_ISOTHERMAL = 0x40
} EAdp5062Interrupt;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void Adp5062Init( void);
// Initialisation

TYesNo Adp5062VinIsOk( void);
// Input voltage OK

void Adp5062InterruptEnable( byte Interrupt);
// Enable <Interrupt>

byte Adp5062Interrupt( void);
// Check for <Interrupt>

#define Adp5062IsInterrupt( State, Interrupt)  (State & Interrupt)

void Adp5062InputLimitHigh( TYesNo Enable);
// Controls DIG_IO1 pin

void Adp5062InputLimitSet( int Miliamps);
// Set input limit

void Adp5062ChargeEnable(TYesNo Enable);
// Enable/disable charging

void Adp5062ClearFault( void);
// Clear temperature and charger faults

void Adp5062SystemVoltageSet( int VoltageMv);
// Set system voltage

TAdp5062ChargerStatus Adp5062ChargerStatusGet( void);
// Get charger status

TAdp5062BatteryStatus Adp5062BatteryStatusGet( void);
// Get battery status

#endif
