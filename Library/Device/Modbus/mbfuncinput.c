/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006 Christian Walter <wolti@sil.at>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * File: $Id: mbfuncinput.c,v 1.10 2007/09/12 10:15:56 wolti Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbframe.h"
#include "mbproto.h"
#include "mbconfig.h"
#include "mbutils.h"
/* ----------------------- Defines ------------------------------------------*/
#define MB_PDU_FUNC_READ_ADDR_OFF           ( MB_PDU_DATA_OFF )
#define MB_PDU_FUNC_READ_REGCNT_OFF         ( MB_PDU_DATA_OFF + 2 )
#define MB_PDU_FUNC_READ_SIZE               ( 4 )
#define MB_PDU_FUNC_READ_REGCNT_MAX         ( 0x007D )

#define MBM_PDU_FUNC_READ_BYTE_COUNT_OFF   ( MB_PDU_DATA_OFF )
#define MBM_PDU_FUNC_READ_DATA_OFF         ( MBM_PDU_FUNC_READ_BYTE_COUNT_OFF + 1)
#define MBM_PDU_FUNC_READ_SIZE_MIN         ( MB_PDU_SIZE_MIN + 1 )

/* ----------------------- Static functions ---------------------------------*/
eMBException    prveMBError2Exception( eMBErrorCode eErrorCode );

/* ----------------------- Start implementation -----------------------------*/
#if MB_FUNC_READ_INPUT_ENABLED > 0

eMBException
eMBFuncReadInputRegister( UCHAR * pucFrame, USHORT * usLen )
{
    USHORT          usRegAddress;
    USHORT          usRegCount;
    UCHAR          *pucFrameCur;

    eMBException    eStatus = MB_EX_NONE;
    eMBErrorCode    eRegStatus;

    if( *usLen == ( MB_PDU_FUNC_READ_SIZE + MB_PDU_SIZE_MIN ) )
    {
        usRegAddress = ( USHORT )( pucFrame[MB_PDU_FUNC_READ_ADDR_OFF] << 8 );
        usRegAddress |= ( USHORT )( pucFrame[MB_PDU_FUNC_READ_ADDR_OFF + 1] );
        

        usRegCount = ( USHORT )( pucFrame[MB_PDU_FUNC_READ_REGCNT_OFF] << 8 );
        usRegCount |= ( USHORT )( pucFrame[MB_PDU_FUNC_READ_REGCNT_OFF + 1] );

        /* Check if the number of registers to read is valid. If not
         * return Modbus illegal data value exception. 
         */
        if( ( usRegCount >= 1 )
            && ( usRegCount < MB_PDU_FUNC_READ_REGCNT_MAX ) )
        {
            /* Set the current PDU data pointer to the beginning. */
            pucFrameCur = &pucFrame[MB_PDU_FUNC_OFF];
            *usLen = MB_PDU_FUNC_OFF;

            /* First byte contains the function code. */
            *pucFrameCur++ = MB_FUNC_READ_INPUT_REGISTER;
            *usLen += 1;

            /* Skip number of bytes for now (because of the alignment) */
            pucFrameCur++;
            /* Align to word boundary*/
            USHORT *regPtr = xMBUtilAllignToWordBoundary(pucFrameCur, 0);
            /* Make callback to fill the buffer. */
            eRegStatus = eMBRegInputCB( regPtr, usRegAddress, usRegCount);
            if((int)regPtr != (int)pucFrameCur){
               memmove(pucFrameCur, regPtr, usRegCount*2);
            }

            /* Second byte in the response contain the number of bytes. */
            *--pucFrameCur = ( UCHAR ) ( usRegCount * 2 );
            *usLen += 1;

            /* If an error occured convert it into a Modbus exception. */
            if( eRegStatus != MB_ENOERR )
            {
                eStatus = prveMBError2Exception( eRegStatus );
            }
            else
            {
                *usLen += usRegCount * 2;
            }
        }
        else
        {
            eStatus = MB_EX_ILLEGAL_DATA_VALUE;
        }
    }
    else
    {
        /* Can't be a valid read input register request because the length
         * is incorrect. */
        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
    }
    return eStatus;
}
#if MB_MASTER_ENABLED

eMBErrorCode eMBMReadInputRegisters(UCHAR ucSlaveAddress, USHORT usRegStartAddress, UCHAR ubNRegs)
{
UCHAR   ucMBFrame[MB_PDU_FUNC_READ_SIZE + 1]; 

   if(ubNRegs > MB_PDU_FUNC_READ_REGCNT_MAX){
      return MB_EINVAL;
   }
   ucMBFrame[MB_PDU_FUNC_OFF] = MB_FUNC_READ_INPUT_REGISTER;
   ucMBFrame[MB_PDU_FUNC_READ_ADDR_OFF] = (UCHAR)(usRegStartAddress >> 8);
   ucMBFrame[MB_PDU_FUNC_READ_ADDR_OFF + 1] = (UCHAR) usRegStartAddress;
   ucMBFrame[MB_PDU_FUNC_READ_REGCNT_OFF] = (UCHAR)(ubNRegs >> 8);
   ucMBFrame[MB_PDU_FUNC_READ_REGCNT_OFF+1] = (UCHAR) ubNRegs;
   return SendFrame(ucSlaveAddress, ucMBFrame, MB_PDU_FUNC_READ_SIZE + 1);
}

eMBException    eMBMFuncReadInputRegister( UCHAR * pucFrame, USHORT * usLen )
{
    UCHAR    usByteCount;

    if( *usLen < MBM_PDU_FUNC_READ_SIZE_MIN  )
    {
      return MB_EX_ILLEGAL_DATA_VALUE;
    }
    usByteCount = pucFrame[MBM_PDU_FUNC_READ_BYTE_COUNT_OFF];
    if(*usLen - MBM_PDU_FUNC_READ_SIZE_MIN != usByteCount){
         /* Can't be a valid request because the length is incorrect. */
         return MB_EX_ILLEGAL_DATA_VALUE;
    }     

    USHORT *dataPtr = xMBUtilAllignToWordBoundary(&pucFrame[MBM_PDU_FUNC_READ_DATA_OFF], *usLen);
    /* Make callback to update the value. */        
    eMBMRegInputCB(dataPtr, usByteCount / 2);    
    return MB_EX_NONE;   
}

#endif 

#endif
