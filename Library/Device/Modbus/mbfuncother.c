/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006 Christian Walter <wolti@sil.at>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * File: $Id: mbfuncother.c,v 1.8 2006/12/07 22:10:34 wolti Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbframe.h"
#include "mbproto.h"
#include "mbconfig.h"

#define MB_PDU_FUNC_SLAVEID_BYTE_COUNT_OFF   ( MB_PDU_DATA_OFF )
#define MB_PDU_FUNC_SLAVEID_DATA_OFF         ( MB_PDU_FUNC_SLAVEID_BYTE_COUNT_OFF + 1 )

#define MBM_PDU_FUNC_SLAVEID_SIZE_MIN         ( MB_PDU_SIZE_MIN + 1 )

#if MB_FUNC_OTHER_REP_SLAVEID_ENABLED > 0

/* ----------------------- Static variables ---------------------------------*/
static UCHAR    ucMBSlaveID[MB_FUNC_OTHER_REP_SLAVEID_BUF];
static USHORT   usMBSlaveIDLen;

/* ----------------------- Start implementation -----------------------------*/

eMBErrorCode
eMBSetSlaveID( UCHAR const *ucSlaveID, USHORT usSlaveIDLen, BOOL xIsRunning,
               UCHAR const *pucAdditional, USHORT usAdditionalLen )
{
    eMBErrorCode    eStatus = MB_ENOERR;

    /* the first byte and second byte in the buffer is reserved for
     * the parameter ucSlaveID and the running flag. The rest of
     * the buffer is available for additional data. */
    if( usAdditionalLen + usSlaveIDLen + 1 < MB_FUNC_OTHER_REP_SLAVEID_BUF )
    {
        usMBSlaveIDLen = 0;
        memcpy( &ucMBSlaveID[usMBSlaveIDLen], ucSlaveID,
                    ( size_t )usSlaveIDLen );
        usMBSlaveIDLen += usSlaveIDLen;
        ucMBSlaveID[usMBSlaveIDLen++] = ( UCHAR )( xIsRunning ? 0xFF : 0x00 );
        if( usAdditionalLen > 0 )
        {
            memcpy( &ucMBSlaveID[usMBSlaveIDLen], pucAdditional,
                    ( size_t )usAdditionalLen );
            usMBSlaveIDLen += usAdditionalLen;
        }
    }
    else
    {
        eStatus = MB_ENORES;
    }
    return eStatus;
}

eMBException
eMBFuncReportSlaveID( UCHAR * pucFrame, USHORT * usLen )
{
   pucFrame[MB_PDU_FUNC_SLAVEID_BYTE_COUNT_OFF] = usMBSlaveIDLen;
   memcpy( &pucFrame[MB_PDU_FUNC_SLAVEID_DATA_OFF], ucMBSlaveID, ( size_t )usMBSlaveIDLen );
   *usLen = ( USHORT )( MB_PDU_FUNC_SLAVEID_DATA_OFF + usMBSlaveIDLen );
   return MB_EX_NONE;
}

#if MB_MASTER_ENABLED
eMBErrorCode eMBMReportSlaveID(UCHAR ucSlaveAddress)
{
UCHAR   ucMBFrame;

   ucMBFrame = MB_FUNC_OTHER_REPORT_SLAVEID;
   return SendFrame(ucSlaveAddress, &ucMBFrame, 1);
}

eMBException eMBMFuncReportSlaveID( UCHAR * pucFrame, USHORT * usLen )
{
    UCHAR    usByteCount;

    if( *usLen < MBM_PDU_FUNC_SLAVEID_SIZE_MIN  )
    {
      return MB_EX_ILLEGAL_DATA_VALUE;
    }
    usByteCount = pucFrame[MB_PDU_FUNC_SLAVEID_BYTE_COUNT_OFF];
    if(*usLen - MBM_PDU_FUNC_SLAVEID_SIZE_MIN != usByteCount){
         /* Can't be a valid request because the length is incorrect. */
         return MB_EX_ILLEGAL_DATA_VALUE;
    } 
        /* Make callback to update the value. */        
    eMBMSlaveIdCB(&pucFrame[MB_PDU_FUNC_SLAVEID_DATA_OFF], usByteCount);    
    return MB_EX_NONE;   
}

#endif
#endif
