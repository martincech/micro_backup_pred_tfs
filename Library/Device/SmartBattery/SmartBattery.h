//*****************************************************************************
//
//    SmartBattery.h         Smart battery
//    Version 1.0            (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SmartBattery_H__
   #define __SmartBattery_H__

#include "Unisys/Uni.h"

TYesNo SBTemperature( word *Temperature);
// Temperature

TYesNo SBCurrent( word *Current);
// Current

TYesNo SBVoltage( word *Voltage);
// Voltage

TYesNo SBRelativeStateOfCharge( word *State);
// Relative state of charge

TYesNo SBManufacturerName( byte *Name);
// Manufacturer name

TYesNo SBDeviceName( byte *Name);
// Device name

TYesNo SBDeviceChemistry( byte *Name);
// Device chemistry

int SBManufacturerData( byte *Data);
// Manufacturer data

#endif