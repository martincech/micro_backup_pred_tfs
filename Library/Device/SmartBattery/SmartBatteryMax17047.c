//*****************************************************************************
//
//    SmartBatteryMax17047.c     Smart battery implementation
//    Version 1.0                (c) VEIT Electronics
//
//*****************************************************************************

#include "SmartBattery.h"
#include "Accu/Accu.h"
#include <string.h>

#define SB_MANUFACTURER_NAME     "VEIT"
#define SB_DEVICE_CHEMISTRY      "LION"
#define SB_DEVICE_NAME           "VEIT BATTERY 1.0"


TYesNo SBTemperature( word *Temperature)
// Temperature
{
   *Temperature = AccuTemperature() * 10 + 2732;
   return YES;
} // SBTemperature


TYesNo SBCurrent( word *Current)
// Current
{
   *Current = AccuCurrent();
   return YES;
} // SBCurrent

TYesNo SBVoltage( word *Voltage)
// Voltage
{
   *Voltage = AccuVoltage();
   return YES;
} // SBVoltage

TYesNo SBRelativeStateOfCharge( word *State)
// Relative state of charge
{
   *State = AccuCapacityRemaining();
   if(*State == ACCU_CAPACITY_INVALID) {
      return NO;
   }
   return YES;
} // SBRelativeStateOfCharge

TYesNo SBManufacturerName( byte *Name)
// Manufacturer name
{
   strcpy(Name, SB_MANUFACTURER_NAME);
   return YES;
} // SBManufacturerName


TYesNo SBDeviceChemistry( byte *Name)
// Device chemistry
{
   strcpy(Name, SB_DEVICE_CHEMISTRY);
   return YES;
} // SBDeviceChemistry


TYesNo SBDeviceName( byte *Name)
// Device name
{
   strcpy(Name, SB_DEVICE_NAME);
   return YES;
} // SBDeviceName


int SBManufacturerData( byte *Data)
// Manufacturer data
{
   Data[0] = "0x00";
   return 1;
} // SBManufacturerData