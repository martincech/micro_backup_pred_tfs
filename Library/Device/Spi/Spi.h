//*****************************************************************************
//
//   Spi.h       SPI interface
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Spi_H__
   #define __Spi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// SPI address :
typedef enum {
   SPI_SPI0,
   SPI_SPI1,
   SPI_SPI2,
   SPI_SPI3,
   SPI_SPI4,
   SPI_SPI5,
   SPI_SPI6,
   SPI_SPI7,
   SPI_SPI8,
   SPI_SPI9,
} ESpiAddress;

// Enumeration types :
typedef byte TSpiAddress;             // data type for ESpiAddress

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void SpiInit( TSpiAddress Spi);
// Initialize bus

void SpiAttach( TSpiAddress Spi);
// Attach SPI bus - activate chipselect

void SpiRelease( TSpiAddress Spi);
// Release SPI bus - deactivate chipselect

byte SpiByteRead( TSpiAddress Spi);
// Read byte from SPI

void SpiByteWrite( TSpiAddress Spi, byte Value);
// Write byte to SPI

byte SpiByteTransceive( TSpiAddress Spi, byte TxValue);
// Write/read SPI. Returns value read

#endif
