//*****************************************************************************
//
//   SpiSingleSw.c   Single programmed SPI interface
//   Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#include "SpiSingle.h"

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void SpiInit( void)
// Initialize bus
{
   SpiSckClr();      // default is L
   SpiMosiSet();     // default as H
   SpiRelease();     // deselect
   SpiPortInit();
} // SpiInit

//-----------------------------------------------------------------------------
// Chipselect
//-----------------------------------------------------------------------------

void SpiAttach( void)
// Attach SPI bus - activate chipselect
{
   SpiCsClr();        // activate CS
} // SpiAttach

void SpiRelease( void)
// Release SPI bus - deactivate chipselect
{
   SpiCsSet();        // deactivate CS
} // SpiAttach

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

byte SpiByteRead( void)
// Read byte from SPI
{
byte Value = 0;
byte i = 8;

   do {
      SpiSckSet();
      Value <<= 1;
      if( SpiMisoGet()){
         Value |= 1;
      }
      SpiSckClr();
   } while( --i);
   return( Value);
} // SpiReadByte

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void SpiByteWrite( byte Value)
// Write byte to SPI
{
byte i;

   i = 8;
   do {
      SpiMosiClr();
      if( Value & 0x80) {
         SpiMosiSet();
      }
      SpiSckSet();
      Value <<= 1;
      SpiSckClr();
   } while( --i);
} // SpiWriteByte

#ifdef SPI_TRANSCEIVE
//-----------------------------------------------------------------------------
// Write/Read
//-----------------------------------------------------------------------------

byte SpiByteTransceive( byte TxValue)
// Write/read SPI. Returns value read
{
byte i;
byte RxValue;
   
   RxValue = 0;
   i       = 8;
   do {
      SpiMosiClr();
      if( TxValue & 0x80) {
         SpiMosiSet();
      }      
      SpiSckSet();
      RxValue  <<= 1;  
      TxValue  <<= 1;   
      SpiSckClr();
      if( SpiMisoGet()){
         RxValue |= 1;
      }     
   } while( --i);
   return( RxValue);
} // SpiByteTransceive

#endif
