//*****************************************************************************
//
//   SpiSingleSlave.h  Single SPI slave interface
//   Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SpiSingleSlave_H__
   #define __SpiSingleSlave_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void SpiInit( void);
// Initialize bus

void SpiRxStart( void);
// Start Rx

TYesNo SpiRxReady( void);
// Returns YES, on Rx data ready

byte SpiRxSize( void);
// Returns received DATA bytes count

void *SpiRxData( void);
// Returns Rx data buffer address

void *SpiTxData( void);
// Returns Tx data buffer address

void SpiTx( byte Size);
// Send <Size>


#endif
