//*****************************************************************************
//
//    Random.h         Random number
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Random_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

dword RandomGet( void);
// Random get

#ifdef __cplusplus
   }
#endif


#endif
