//*****************************************************************************
//
//    Dacs.c       Serial port Dacs communication
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "Dacs.h"
#include "Uart/Uart.h"
#include "Dacs/DacsConfig.h"  // Dacs protocol configuration
#include "Crc/Crc.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

//------------------------------------------------------------------------------
//  Dacs module definitions
//------------------------------------------------------------------------------

#define DACS_FORMAT                  UART_8BIT
#define DACS_INTERCHARACTER_TIMEOUT  50
#define DACS_BAUD_DEFAULT            9600
#define DACS_SEND_DELAY              0

static TDacsPacket *DacsBuffer;
static byte _DacsRxTxBuffer[sizeof(TDacsPacket) + 1];

// Local functions :
static void DacsReverseGenderDataEndianity(TDacsGenderDataReply *Data);
// change endianity before sending for gender
static EUartAddress   uartAddress;
//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void DacsInit( int uartPort)
// Initialize
{
char LeaderSeq[]  = { DACS_DLE, DACS_SOH, '\0'};
char TrailerSeq[] = { DACS_DLE, DACS_ETX, '\0'};
   uartAddress = uartPort;
   UartInit( uartAddress);
   UartSetup( uartAddress, DACS_BAUD_DEFAULT, DACS_FORMAT);
   UartTimeoutSet( uartAddress, UART_TIMEOUT_OFF, DACS_INTERCHARACTER_TIMEOUT);
   UartSendDelaySet( uartAddress, DACS_SEND_DELAY);

   DacsBuffer = (TDacsPacket *)&_DacsRxTxBuffer[1];
   _DacsRxTxBuffer[0] = DACS_SOH; // for crc calculation
   UartBufferSet( uartAddress, DacsBuffer, sizeof(TDacsPacket));
   UartModeSet( uartAddress, UART_MODE_DACS_SLAVE);
   UartFramingSetLeaderTrailerSeq( uartAddress, LeaderSeq, TrailerSeq);
}// DacsInit

//------------------------------------------------------------------------------
//   Deinitialize
//------------------------------------------------------------------------------

void DacsDeinit( void)
// Deinitialize
{
    DacsListenStop();
    UartDeinit( uartAddress);
}

//------------------------------------------------------------------------------
//   Status
//------------------------------------------------------------------------------

TDacsStatus DacsStatus( void)
// Check for last operation status
{
   switch( UartReceiveStatus( uartAddress)){
      case UART_IDLE:
         return( DACS_IDLE);

      case UART_RECEIVE_ACTIVE:
         return( DACS_RECEIVE_LISTENING);

      case UART_RECEIVE_FRAMING_ERROR :
      case UART_RECEIVE_OVERRUN_ERROR :
      case UART_RECEIVE_SIZE_ERROR :
      case UART_RECEIVE_CRC_ERROR:
         return( DACS_RECEIVE_ERROR);      // receive error

      case UART_RECEIVE_FRAME:
         return( DACS_RECEIVE_DATA_WAITING);       // receive O.K.

      case UART_SEND_ACTIVE :
         return( DACS_SEND_ACTIVE);

      case UART_SEND_DONE :
         return( DACS_SEND_DONE);

      default :
         // unexpected status
         return( DACS_RECEIVE_ERROR);     // receive error
   }
}// DacsStatus

//------------------------------------------------------------------------------
//   RX reset
//------------------------------------------------------------------------------

void DacsListenStart( void)
// Resets listening on port when error occurred
{
   UartFlush( uartAddress);
   UartReceive( uartAddress);
}

//------------------------------------------------------------------------------
//   RX/TX stop
//------------------------------------------------------------------------------

void DacsListenStop( void)
// Stops listening on port
{
   UartStop( uartAddress);
}

//------------------------------------------------------------------------------
//   Read RX buffer
//------------------------------------------------------------------------------

TDacsRequest *DacsReceive( void)
// Reads incoming data from receive buffer, succeed only when DACS_RECEIVE_DATA_WAITING
{
int            ReceiveSize;
int            Len;
int            i;
byte           *EndPtr;

   switch( UartReceiveStatus( uartAddress)){
      case UART_RECEIVE_FRAME:
         break;                              // receive O.K., continue

      default :
         return NULL;
   }

   ReceiveSize    = UartReceiveSize( uartAddress);
   if( ReceiveSize < 1){
      return 0;
   }

   TDacsRequest  *Request;
   Request = (TDacsRequest *)DacsBuffer->Data;
   // first calculate crc
   Len = ENDIAN_FROM_LITTLE_WORD( DacsBuffer->Len);
   DacsBuffer->Crc = ENDIAN_FROM_LITTLE_WORD( *(word*)&DacsBuffer->Data[ Len]);
   if( DacsBuffer->Crc != CalculateCrc16( (char*)_DacsRxTxBuffer, ReceiveSize - sizeof(word) + 1, 0)){
      return NULL;
   }
   if(DacsBuffer->TargetAddress != DacsOptions.Address){
      //wrong target address
      return NULL;
   }
   DacsBuffer->Len = Len;

   switch( Request->RequestCode){
      case DACS_REQUEST_DATA :
         if( ReceiveSize != DacsPacketSize( DataRequest) ||
             DacsBuffer->Len != sizeof(TDacsDataRequest)){
            return NULL;
         }
         //reverse endianity
         Request->Data.ProductionDay = ENDIAN_FROM_LITTLE_WORD(Request->Data.ProductionDay);
         Request->Data.Year = ENDIAN_FROM_LITTLE_DWORD(Request->Data.Year);
         break;

      case DACS_REQUEST_VERSION :
         if( ReceiveSize != DacsPacketSize( VersionRequest) ||
            DacsBuffer->Len != sizeof(TDacsVersionRequest)){
            return NULL;
         }
         break;

      default :
         return NULL;
   }
   return (TDacsRequest*) Request;
}

//------------------------------------------------------------------------------
//   Send TX buffer
//------------------------------------------------------------------------------

TYesNo DacsSend( void)
// Sends reply,  succeed only when DACS_IDLE, DACS_SEND_DONE, DACS_RECEIVE_LISTENING or DACS_RECEIVE_DATA_WAITING
{
int         SendSize;
TDacsReply  *Reply;
word        *CrcPtr;
int         i;
   switch( UartReceiveStatus( uartAddress)){
      case UART_IDLE:
      case UART_RECEIVE_FRAME:
      case UART_SEND_DONE :
      case UART_RECEIVE_ACTIVE :
         break;

      default :
         return NO;      
   }

   SendSize = 0;
   Reply = DacsGetReplyBuffer();
   DacsBuffer->TargetAddress = DacsBuffer->SourceAddress;
   DacsBuffer->SourceAddress = DacsOptions.Address;
   switch( Reply->ReplyCode){
      case DACS_REPLY_VERSION :
         SendSize = DacsPacketSize(VersionReply);
         DacsBuffer->Len = sizeof(TDacsVersionReply);
         //reverse endianity
         DacsBuffer->Len = ENDIAN_TO_LITTLE_WORD( DacsBuffer->Len);
         // calculate crc
         DacsBuffer->Crc = CalculateCrc16( _DacsRxTxBuffer, DacsPacketDataSize(VersionReply) + 1, 0);
         break;

      case DACS_REPLY_DATA :
         SendSize = DacsPacketSize(DataReply);
         DacsBuffer->Len = sizeof(TDacsDataReply);
         //reverse endianity
         DacsBuffer->Len = ENDIAN_TO_LITTLE_WORD( DacsBuffer->Len);
         DacsReverseGenderDataEndianity(&Reply->Data.Female);
         DacsReverseGenderDataEndianity(&Reply->Data.Male);
         // calculate crc
         DacsBuffer->Crc = CalculateCrc16( _DacsRxTxBuffer, DacsPacketDataSize(DataReply) + 1, 0);
         break;
      default :
         return NO;
   }
   CrcPtr = (word *)&DacsBuffer->Data[DacsBuffer->Len];
   *CrcPtr = ENDIAN_TO_LITTLE_WORD(DacsBuffer->Crc);

   UartSend( uartAddress, DacsBuffer, SendSize);
   return YES;
}

//------------------------------------------------------------------------------
//   Get tx buffer pointer
//------------------------------------------------------------------------------

TDacsReply *DacsGetReplyBuffer()
// Returns buffer for outgoing message, should be filled before calling DacsSend
{
   return (TDacsReply *) DacsBuffer->Data;
}

//------------------------------------------------------------------------------
//  Dacs Helper functions
//------------------------------------------------------------------------------

static void DacsReverseGenderDataEndianity(TDacsGenderDataReply *Data)
// change endianity before sending for gender
{
   Data->Average = ENDIAN_TO_LITTLE_WORD(Data->Average);
   Data->TargetWeight = ENDIAN_TO_LITTLE_WORD(Data->TargetWeight);
   Data->Count = ENDIAN_TO_LITTLE_WORD(Data->Count);
   Data->Gain = ENDIAN_TO_LITTLE_DWORD(Data->Gain);
   Data->Sigma = ENDIAN_TO_LITTLE_WORD(Data->Sigma);
   Data->Cv = ENDIAN_TO_LITTLE_WORD(Data->Cv);
}


