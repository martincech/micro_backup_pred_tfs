//*****************************************************************************
//
//   DacsDef.h    DACS protocol definitions & declarations
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __DacsDef_H__
   #define __DacsDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// DACS 6 - new version
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------
#define DACS_DLE               0x10
#define DACS_SOH               0x01
#define DACS_ETX               0x03
#define DACS_LEADER            ((DACS_DLE << 8) | DACS_SOH)
#define DACS_TRAILER           ((DACS_DLE << 8) | DACS_ETX)

//-----------------------------------------------------------------------------
// Command codes
//-----------------------------------------------------------------------------

#define DACS_REQUEST_DATA       0xA1
#define DACS_REQUEST_VERSION    0xD0
#define DACS_REPLY_DATA         (DACS_REQUEST_DATA + 1)
#define DACS_REPLY_VERSION      (DACS_REQUEST_VERSION + 1)

//-----------------------------------------------------------------------------
// Data fields
//-----------------------------------------------------------------------------
#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

//-----------------------------------------------------------------------------
// Requests
//-----------------------------------------------------------------------------
typedef struct {
   byte        RequestCode;
   byte        TypeCode;
   byte        Reserved;
   word        ProductionDay;
   byte        Day;
   byte        Month;
   word        Year;
   byte        Hour;
   byte        Min;
   byte        Sec;
} __packed TDacsDataRequest;

typedef enum {
   DACS_DATA_TYPE_STATISTICS = 0x80,
   _DACS_DATA_TYPE_LAST
} EDacsDataTypeCode;

typedef struct {
   byte RequestCode;
} __packed TDacsVersionRequest;

typedef union {
   byte RequestCode;
   TDacsDataRequest Data;
   TDacsVersionRequest Version;
} __packed TDacsRequest;


//-----------------------------------------------------------------------------
// Replies
//-----------------------------------------------------------------------------

typedef struct {  //data for one gender
   dword TargetWeight;                           // Target weight
   dword Average;                                // average mass
   word  Count;                                  // weighing count
   dword WeightNow;
   int32 Gain;                                   // dayly gain
   byte  Uniformity;                             // uniformity percentage
   word  Sigma;
   word  Cv;
} __packed TDacsGenderDataReply;

typedef struct {
   byte  ReplyCode;
   byte  TypeCode;
   byte  Reserved;
   TDacsGenderDataReply Female;
   TDacsGenderDataReply Male;
} __packed TDacsDataReply;

typedef struct {
   byte ReplyCode;
   byte Reserved;
   byte Minor;
   byte Major;
} __packed TDacsVersionReply;

typedef union {
   byte                 ReplyCode;
   TDacsDataReply      Data;
   TDacsVersionReply   Version;
} __packed TDacsReply;


#define DACS_DATA_SIZE_MAX (sizeof(TDacsRequest) > sizeof(TDacsReply)? sizeof(TDacsRequest) : sizeof(TDacsReply))

//-----------------------------------------------------------------------------
// Packet
//-----------------------------------------------------------------------------

typedef struct {
   byte                 TargetAddress;                     // destination address
   byte                 SourceAddress;                     // source address
   word                 Len;                               // length of data field
   byte                 Data[DACS_DATA_SIZE_MAX];         // data
   word                 Crc;                               // CRC
} __packed TDacsPacket;

#define DacsPacketSize( Item) (sizeof(TDacsPacket) - DACS_DATA_SIZE_MAX + sizeof(TDacs##Item))
#define DacsPacketDataSize( Item) (DacsPacketSize( Item) - sizeof(word)) //without crc

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif
#endif
