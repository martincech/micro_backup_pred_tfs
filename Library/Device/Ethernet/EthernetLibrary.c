//------------------------------------------------------------------------------
//  Stack/Interface initialization
//------------------------------------------------------------------------------
#include "Ethernet/EthernetLibrary.h"
#include "fnet.h"
#include "Str.h"
#include "Message/MGsm.h"
#include "Communication/Communication.h"
#include <string.h>

typedef enum {
   ETHERNET_STATUS_OFF,
   ETHERNET_STATUS_CABLE_DISCONNECTED,
   ETHERNET_STATUS_ON
} EEthernetStatus;

static byte                      _fnetStackHeap[FNET_CFG_HEAP_SIZE];
static fnet_netif_desc_t         _netif; /* network interface */

static TYesNo DhcpInited;
// Local functions :
static TYesNo EthernetLibraryInitDHCPService();
// init DHCP service (provided by fnet)
static void EthernetLibraryFreeDHCPService();
// free DHCP service and resources

//------------------------------------------------------------------------------
//  Executive
//------------------------------------------------------------------------------

void EthernetLibraryExecute()
{
   if(Ethernet.Dhcp == YES && !DhcpInited){
      DhcpInited = EthernetLibraryInitDHCPService();
   }
   if (Ethernet.Dhcp == NO)
   {
      if(DhcpInited){
         EthernetLibraryFreeDHCPService();
         DhcpInited = NO;
      }
      fnet_ip4_addr_t ip;
      // IP
      ip = fnet_netif_get_ip4_addr(_netif);
      if(memcmp(&ip, &Ethernet.Ip, sizeof(fnet_ip4_addr_t))!=0){
         fnet_netif_set_ip4_addr(_netif, Ethernet.Ip);
      }
      // subnet mask
      ip = fnet_netif_get_ip4_subnet_mask(_netif);
      if(memcmp(&ip, &Ethernet.SubnetMask, sizeof(fnet_ip4_addr_t))!=0){
         fnet_netif_set_ip4_subnet_mask(_netif, Ethernet.SubnetMask);
      }
      // gateway
      ip = fnet_netif_get_ip4_gateway(_netif);
      if(memcmp(&ip, &Ethernet.Gateway, sizeof(fnet_ip4_addr_t))!=0){
         fnet_netif_set_ip4_gateway(_netif, Ethernet.Gateway);
      }

      ip = fnet_netif_get_ip4_dns(_netif);
      if(memcmp(&ip, &Ethernet.PrimaryDns, sizeof(fnet_ip4_addr_t))!=0){
         fnet_netif_set_ip4_dns(_netif, Ethernet.PrimaryDns);
      }
   }

   fnet_poll_services();
}

//------------------------------------------------------------------------------
//  Init
//------------------------------------------------------------------------------

TYesNo EthernetLibraryInitNetworkInterface()
// init fnet stack and network interface
{
struct fnet_init_params fnetInitParams;

#ifdef INITIALIZE_STACK
     memset( _fnetStackHeap, 0xCC, FNET_CFG_HEAP_SIZE);
#endif
   fnetInitParams.netheap_ptr = _fnetStackHeap;
   fnetInitParams.netheap_size = FNET_CFG_HEAP_SIZE;
   // init stack
   _netif = NULL;
   if( fnet_init(&fnetInitParams) == FNET_ERR){
      return NO;
   }
#if (FNET_CFG_CPU_ETH0 ||FNET_CFG_CPU_ETH1)
   _netif = fnet_netif_get_default();
#else
   _netif = &fnet_3G_if;
   fnet_netif_set_ip4_addr( _netif, 0);
   fnet_netif_set_ip4_dns(_netif, FNET_CFG_IP4_DEFAULT_DNS);
   fnet_netif_set_ip4_addr_automatic( _netif);
   fnet_netif_set_default( _netif);
#endif
   if(Ethernet.Dhcp == YES){
      DhcpInited = EthernetLibraryInitDHCPService();
   }

#if FNET_CFG_DEBUG && FNET_CFG_DEBUG_TRACE
   #ifndef FNET_CFG_CPU_SERIAL_PORT_BAUD_RATE
     #define FNET_CFG_CPU_SERIAL_PORT_BAUD_RATE (115200)
   #endif
   #ifndef FNET_CFG_CPU_SERIAL_PORT_DEFAULT
     #define FNET_CFG_CPU_SERIAL_PORT_DEFAULT (5)
   #endif
   fnet_cpu_serial_init( FNET_CFG_CPU_SERIAL_PORT_DEFAULT, FNET_CFG_CPU_SERIAL_PORT_BAUD_RATE);
   fnet_printf( "Fnet library initialized.\n");
#endif
   return YES;
}

//------------------------------------------------------------------------------
//  Free
//------------------------------------------------------------------------------

void EthernetLibraryFreeNetworkInterface()
// free fnet stack
{
   EthernetLibraryFreeDHCPService();
   fnet_release();
}

//------------------------------------------------------------------------------
//  DHCP
//------------------------------------------------------------------------------

static TYesNo EthernetLibraryInitDHCPService()
// init DHCP service (provided by fnet)
{
#if FNET_CFG_DHCP
struct fnet_dhcp_params dhcpParams;

   dhcpParams.requested_ip_address.s_addr = 0;
   dhcpParams.requested_lease_time = 0; // dont't request any specified lease time
   if( fnet_dhcp_init( _netif, &dhcpParams) == FNET_ERR){
      return NO;
   }
#endif
   return YES;
}

static void EthernetLibraryFreeDHCPService()
// free DHCP service and resources
{
#if FNET_CFG_DHCP
   fnet_dhcp_release();
#endif
}

//------------------------------------------------------------------------------
//  Name
//------------------------------------------------------------------------------

void EthernetLibraryEthName( char *buffer, int bufferSize)
// return zero terminated interface name string
{
   fnet_netif_get_name( _netif, buffer, bufferSize);
}

//------------------------------------------------------------------------------
//  IPv4
//------------------------------------------------------------------------------

void EthernetLibraryIPv4( char *buffer, int bufferSize)
// return zero terminated IPv4 address with automatic or manual subscript(based on DHCP service)
{
struct in_addr IPv4;

   buffer[0] = '\0';
   if( bufferSize < FNET_IP4_ADDR_STR_SIZE){
      return;
   }
   IPv4.s_addr = fnet_netif_get_ip4_addr( _netif);
   fnet_inet_ntoa( IPv4, buffer); //convert to string
}

void EthernetLibraryDnsServer( char *buffer, int bufferSize)
// return zero terminated IPv4 dns server address
{
struct in_addr IPv4;

   buffer[0] = '\0';
   if( bufferSize < FNET_IP4_ADDR_STR_SIZE){
      return;
   }
   IPv4.s_addr = fnet_netif_get_ip4_dns( _netif);
   fnet_inet_ntoa( IPv4, buffer); //convert to string
}

//------------------------------------------------------------------------------
//  Status
//------------------------------------------------------------------------------

void EthernetLibraryLinkStatus( char *buffer, int bufferSize)
// return zero terminated link status string
{
   if( fnet_netif_connected( _netif)){
      strncpy( buffer, StrGet( STR_CONNECTED), bufferSize);
   } else {
      strncpy( buffer, StrGet( STR_NOT_CONNECTED), bufferSize);
   }
}

//------------------------------------------------------------------------------
//  Statistics up time
//------------------------------------------------------------------------------

dword EthernetLibraryUpTime()
// return time in seconds for which is library used
{
   return fnet_timer_seconds();
}

//------------------------------------------------------------------------------
//  Statistics RX
//------------------------------------------------------------------------------

dword EthernetLibraryRxPacketCount()
// return received packet count
{
struct fnet_netif_statistics Stats;

   if(fnet_netif_get_statistics( _netif, &Stats) != FNET_OK){
      return 0;
   }
   return Stats.rx_packet;
}

//------------------------------------------------------------------------------
//  Statistics TX
//------------------------------------------------------------------------------

dword EthernetLibraryTxPacketCount()
// return transmit packet count
{
struct fnet_netif_statistics Stats;

   if(fnet_netif_get_statistics( _netif, &Stats) != FNET_OK){
      return 0;
   }
   return Stats.tx_packet;
}

//------------------------------------------------------------------------------
//  Sleep enable
//------------------------------------------------------------------------------
#include "fnet_fec.h"

void EthernetSleepEnable( TYesNo Enable) {
#if (FNET_CFG_CPU_ETH0 ||FNET_CFG_CPU_ETH1)
   fnet_netif_t *netif = (fnet_netif_t *) _netif;
   fnet_fec_if_t *ethif = ((fnet_eth_if_t *)(netif->if_ptr))->if_cpu_ptr;

   #warning Zakomentovano z duvodu nespravneho pouziti funkce v programu
   /*if(Enable) {
      fnet_fec_mii_write(ethif, 0x18, 0x0000);
      fnet_fec_mii_write(ethif, 0x10, 0x0010);
   } else {
      fnet_fec_mii_write(ethif, 0x18, 0x0800);
   }*/
#endif
}


//------------------------------------------------------------------------------
//  Link status
//------------------------------------------------------------------------------

TYesNo EthernetIsLink( void) {
   if(_netif == NULL) {
      return NO;
   }

#if (FNET_CFG_CPU_ETH0 ||FNET_CFG_CPU_ETH1)
   fnet_netif_t *netif = (fnet_netif_t *) _netif;
   fnet_eth_if_t *ethif = (fnet_eth_if_t *) netif->if_ptr;
   return ethif->connection_flag;
#else // 3G
   return MGsmRegistered();
#endif

}

TYesNo EthernetLibraryIpAssigned()
{
struct in_addr IPv4;

   if(_netif == NULL) {
      return NO;
   }

   IPv4.s_addr = fnet_netif_get_ip4_addr( _netif);
   if(IPv4.s_addr == 0){
     return NO;
   }
   return YES;
}

