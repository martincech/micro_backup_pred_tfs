//******************************************************************************
//
//   fnet_tcp_gsm.c     TCP stack on GSM device implementation
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "fnet_config.h"

#if FNET_CFG_TCP_GSM

#include "fnet.h"
#include "fnet_error.h"
#include "fnet_socket.h"
#include "fnet_socket_prv.h"
#include "fnet_timer_prv.h"
#include "fnet_tcp_gsm.h"
#include "fnet_isr.h"
#include "fnet_checksum.h"
#include "fnet_prot.h"
#include "fnet_stdlib.h"
#include "fnet_debug.h"
#include "Message/MGsm.h"

static void fnet_tcp_gsm_timo( void *cookie );
//timer timed out
static void fnet_tcp_gsm_status_read( void* sk);
//timo poll routine
static int fnet_tcp_gsm_init( void );
//initialization
static void fnet_tcp_gsm_release( void );
//free

// socket api
static int fnet_tcp_gsm_attach( fnet_socket_t *sk );
static int fnet_tcp_gsm_close( fnet_socket_t *sk );
static int fnet_tcp_gsm_connect( fnet_socket_t *sk, struct sockaddr *foreign_addr);
static int fnet_tcp_gsm_rcv( fnet_socket_t *sk, char *buf, int len, int flags, struct sockaddr *foreign_addr);
static int fnet_tcp_gsm_snd( fnet_socket_t *sk, char *buf, int len, int flags, const struct sockaddr *foreign_addr);
static int fnet_tcp_gsm_shutdown( fnet_socket_t *sk, int how );
static int fnet_tcp_gsm_setsockopt( fnet_socket_t *sk, int level, int optname, char *optval, int optlen );
static int fnet_tcp_gsm_getsockopt( fnet_socket_t *sk, int level, int optname, char *optval, int *optlen );

/*****************************************************************************
 * Protocol API structure.
 ******************************************************************************/
/* Socket API interface */
static const fnet_socket_prot_if_t fnet_tcp_gsm_socket_api =
{
    1,                        /* TRUE = connection required by protocol.*/
    fnet_tcp_gsm_attach,      /* A new socket has been created.*/
    fnet_tcp_gsm_close,
    fnet_tcp_gsm_connect,
    0,
    fnet_tcp_gsm_rcv,
    fnet_tcp_gsm_snd,
    fnet_tcp_gsm_shutdown,
    fnet_tcp_gsm_setsockopt,
    fnet_tcp_gsm_getsockopt,
    0
};

/* Protocol interface.*/
fnet_prot_if_t fnet_tcp_gsm_prot_if =
{
    0, 
    AF_INET,                           /* Protocol family.*/
    SOCK_STREAM,                       /* Socket type.*/
    FNET_IP_PROTOCOL_TCP,              /* Protocol number.*/ 
    fnet_tcp_gsm_init,
    fnet_tcp_gsm_release,
    0,                                 /* Control input (from below).*/
    0, 
    &fnet_tcp_gsm_socket_api           /* Socket API */
};

static fnet_timer_desc_t fnet_tcp_gsm_timer;

/************************************************************************
* NAME: fnet_tcp_gsm_init
*
* DESCRIPTION: This function performs a protocol initialization.
*
* RETURNS: If no error occurs, this function returns ERR_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/
static int fnet_tcp_gsm_init( void )
{
    /* Create the timer.*/
    fnet_tcp_gsm_timer = fnet_timer_new(FNET_TCP_SLOWTIMO*2 / FNET_TIMER_PERIOD_MS, fnet_tcp_gsm_timo, 0);

    if(!fnet_tcp_gsm_timer)
    {
        fnet_tcp_gsm_timer = 0;
        return FNET_ERR;
    }
    MGsmPowerOn();
    return FNET_OK;
}
/************************************************************************
* NAME: fnet_tcp_gsm_release
*
* DESCRIPTION: This function resets and deletes sockets and releases timers.
*
* RETURNS: None.
*************************************************************************/
static void fnet_tcp_gsm_release( void )
{
    /* Release sockets.*/
    while(fnet_tcp_gsm_prot_if.head){
        fnet_tcp_gsm_close(fnet_tcp_gsm_prot_if.head);
    }
    /* Free timers.*/
    fnet_timer_free(fnet_tcp_gsm_timer);
    fnet_tcp_gsm_timer = 0;
    MGsmPowerResume();
}

/************************************************************************
* NAME: fnet_tcp_gsm_attach
*
* DESCRIPTION: This function performs the initialization of the  
*              socket's options
*
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/
static int fnet_tcp_gsm_attach( fnet_socket_t *sk )
{
   
    sk->protocol_control = 0;
    /* Set the maximal segment size option.*/
    sk->options.tcp_opt.mss         = FNET_CFG_SOCKET_TCP_MSS;
    /* Default setting of the flags.*/
    sk->options.tcp_opt.flags       = 0;
    sk->options.tcp_opt.keep_idle   = FNET_TCP_KEEPIDLE_DEFAULT;      /* TCP_KEEPIDLE option. */
    sk->options.tcp_opt.keep_intvl  = FNET_TCP_KEEPINTVL_DEFAULT;    /* TCP_KEEPINTVL option. */
    sk->options.tcp_opt.keep_cnt    = FNET_TCP_KEEPCNT_DEFAULT;        /* TCP_KEEPCNT option. */
    
    sk->options.flags = SO_KEEPALIVE;

    /* Set the buffer sizes.*/
    sk->send_buffer.count_max = FNET_TCP_TX_BUF_MAX;
    sk->receive_buffer.count_max = FNET_TCP_RX_BUF_MAX;

    return FNET_OK;
}

/************************************************************************
* NAME: fnet_tcp_gsm_close
*
* DESCRIPTION: This function performs the connection termination.
*
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/

static int fnet_tcp_gsm_close( fnet_socket_t *sk )
{
   if( sk->state != SS_UNCONNECTED){
      while( !MGsmSocketClose(sk->descriptor)){}
   }
   fnet_socket_release( &fnet_tcp_gsm_prot_if.head, sk);
   sk->state = SS_UNCONNECTED;
   fnet_memset_zero(&sk->foreign_addr, sizeof(sk->foreign_addr));
   
   if( fnet_tcp_gsm_prot_if.head == 0){
      fnet_netif_t *netif = (fnet_netif_t *)fnet_netif_get_default();
      fnet_netif_set_ip4_addr(( fnet_netif_desc_t)netif, 0);
      fnet_netif_set_ip4_addr_automatic(( fnet_netif_desc_t)netif);
      if( netif->api && netif->if_ptr){
         ((fnet_3G_if_t*)netif->if_ptr)->connected = NO;
      }
   }
   return FNET_OK;
}

/************************************************************************
* NAME: fnet_tcp_connect
*
* DESCRIPTION: This function performs the connection establishment.
*   
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/
static int fnet_tcp_gsm_connect( fnet_socket_t *sk, struct sockaddr *foreign_addr)
{

   if(fnet_socket_addr_is_multicast(foreign_addr))
   {
       fnet_socket_set_error(sk, FNET_ERR_ADDRNOTAVAIL);
       return FNET_ERR;
   }
   if( !MGsmSocketOpenTcp( sk->descriptor, (struct sockaddr_in *)foreign_addr,
                           sk->options.flags & SO_KEEPALIVE? &sk->options.tcp_opt : 0)){
      fnet_socket_set_error(sk, FNET_ERR_HOSTUNREACH);
      return FNET_ERR;
   }

   sk->state = SS_CONNECTING;
   sk->foreign_addr = *foreign_addr;
   return FNET_OK;
}

/************************************************************************
* NAME: fnet_tcp_gsm_rcv
*
* DESCRIPTION: This function receives the data
*
* RETURNS: If no error occurs, this function returns the length
*          of the received data. Otherwise, it returns FNET_ERR.
*************************************************************************/

static int fnet_tcp_gsm_rcv( fnet_socket_t *sk, char *buf, int len, int flags, struct sockaddr *foreign_addr)
{
int rcvLen;
fnet_netif_t *netif;

   FNET_COMP_UNUSED_ARG(foreign_addr);
   FNET_COMP_UNUSED_ARG(flags);

   /* If the socket is not connected, return*/
   if(sk->state != SS_CONNECTED){
       fnet_socket_set_error(sk, FNET_ERR_NOTCONN);
       return FNET_ERR;
   }

   rcvLen = MGsmSocketReceive( sk->descriptor, buf, len);
   netif = (fnet_netif_t *)fnet_netif_get_default();
   if( rcvLen < 0) {
      return FNET_ERR;
   }
   if( rcvLen && netif->api && netif->if_ptr){
         ((fnet_3G_if_t *)netif->if_ptr)->RxCount++;
   }
   return rcvLen;
}

/************************************************************************
* NAME: fnet_tcp_snd
*
* DESCRIPTION: This sends the data that can be sent.
*
* RETURNS: If no error occurs, this function returns the length
*          of the data is send
*          Otherwise, it returns FNET_ERR.
*************************************************************************/
static int fnet_tcp_gsm_snd( fnet_socket_t *sk, char *buf, int len, int flags, const struct sockaddr *foreign_addr)
{
int sndLen;
fnet_netif_t *netif;

   FNET_COMP_UNUSED_ARG(foreign_addr);
   FNET_COMP_UNUSED_ARG(flags);

   /* If the socket is not connected, return*/
   if(sk->state != SS_CONNECTED){
       fnet_socket_set_error(sk, FNET_ERR_NOTCONN);
       return FNET_ERR;
   }

   sndLen = MGsmSocketSend( sk->descriptor, buf, len);
   netif = (fnet_netif_t *)fnet_netif_get_default();
   if( sndLen && netif->api && netif->if_ptr){
         ((fnet_3G_if_t *)netif->if_ptr)->TxCount++;
   }
   return sndLen;
}

/************************************************************************
* NAME: fnet_tcp_gsm_shutdown
*
* DESCRIPTION: This function closes a write-half, read-half, or
*              both halves of the connection.
*
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/

static int fnet_tcp_gsm_shutdown( fnet_socket_t *sk, int how )
{
   /* If the socket is not connected, return.*/
   if(sk->state != SS_CONNECTED)
   {
       fnet_socket_set_error(sk, FNET_ERR_NOTCONN);
       return FNET_ERR;
   }
   else
   {
       /* Shutdown the writing.*/
       if(how & SD_WRITE && !sk->send_buffer.is_shutdown)
       {
           /* Set the flag of the buffer.*/
           sk->send_buffer.is_shutdown = 1;
       }

       /* Shutdown the reading.*/
       if(how & SD_READ && !sk->receive_buffer.is_shutdown)
       {
           fnet_socket_buffer_release(&sk->receive_buffer);
           /* Set the flag of the buffer (Data can't be read).*/
           sk->receive_buffer.is_shutdown = 1;
       }

       return FNET_OK;
   }
}

/************************************************************************
* NAME: fnet_tcp_gsm_setsockopt
*
* DESCRIPTION: This function sets a TCP option.
*
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/

static int fnet_tcp_gsm_setsockopt( fnet_socket_t *sk, int level, int optname, char *optval, int optlen )
{
   /* If the level is not IPPROTO_TCP, go to IP processing.*/
   if(level == IPPROTO_TCP)
   {
       /* Check the option size.*/
       switch(optname){
           case TCP_KEEPCNT:
           case TCP_KEEPINTVL:
           case TCP_KEEPIDLE:
               if(optlen != sizeof(int)){
                  fnet_socket_set_error(sk, FNET_ERR_INVAL);
                  return FNET_ERR;
               }
               break;
           default:
               /* The option is not supported.*/
          fnet_socket_set_error(sk, FNET_ERR_NOPROTOOPT);
          return FNET_ERR;
       }

       /* Process the option.*/
       switch(optname){
           /* Keepalive probe retransmit limit.*/
           case TCP_KEEPCNT:
               if(!(*((unsigned int *)(optval)))){
                  fnet_socket_set_error(sk, FNET_ERR_INVAL);
                  return FNET_ERR;
               }

               sk->options.tcp_opt.keep_cnt = *((int *)(optval));
               break;
           /* Keepalive retransmit interval.*/
           case TCP_KEEPINTVL:
               if(!(*((unsigned int *)(optval)))){
                  fnet_socket_set_error(sk, FNET_ERR_INVAL);
                  return FNET_ERR;
               }

               sk->options.tcp_opt.keep_intvl = *((int *)(optval));
               break;
           /* Time between keepalive probes.*/
           case TCP_KEEPIDLE:
               if(!(*((unsigned int *)(optval)))){
                  fnet_socket_set_error(sk, FNET_ERR_INVAL);
                  return FNET_ERR;
               }

               sk->options.tcp_opt.keep_idle = *((int *)(optval));
               break;
       }

       return FNET_OK;
   } else {
      /* IP level option processing - cannot be set*/
      fnet_socket_set_error(sk, FNET_ERR_NOPROTOOPT);
      return FNET_ERR;
   }
}

/************************************************************************
* NAME: fnet_tcp_gsm_getsockopt
*
* DESCRIPTION: This function receives a TCP option.
*
* RETURNS: If no error occurs, this function returns FNET_OK. Otherwise
*          it returns FNET_ERR.
*************************************************************************/

static int fnet_tcp_gsm_getsockopt( fnet_socket_t *sk, int level, int optname, char *optval, int *optlen )
{
   if(level == IPPROTO_TCP)
   {
       switch(optname)
       {
           case TCP_KEEPCNT:
               *((int *)(optval)) = sk->options.tcp_opt.keep_cnt;
               break;
           case TCP_KEEPINTVL:
               *((int *)(optval)) = sk->options.tcp_opt.keep_intvl;
               break;
           case TCP_KEEPIDLE:
               *((int *)(optval)) = sk->options.tcp_opt.keep_idle;
               break;
           default:
               fnet_socket_set_error(sk, FNET_ERR_NOPROTOOPT);
               return FNET_ERR;
       }

       *optlen = sizeof(int);

       return FNET_OK;
   }
   else
   {
       /* IP level option processing - cannot be set*/
      fnet_socket_set_error(sk, FNET_ERR_NOPROTOOPT);
      return FNET_ERR;
   }
}

/************************************************************************
* NAME: fnet_tcp_gsm_timo
*
* DESCRIPTION: This function processes the timeouts.
*
* RETURNS: None.
*************************************************************************/
static fnet_poll_desc_t stateService = FNET_ERR;

static void fnet_tcp_gsm_timo( void *cookie )
{
fnet_socket_t *sk;

   sk = fnet_tcp_gsm_prot_if.head;
   while(sk)
   {
      if( sk->state != SS_UNCONNECTED && stateService == FNET_ERR){
         stateService =
               fnet_poll_service_register( &fnet_tcp_gsm_status_read, (void *)sk);
      }
      sk = sk->next;
   }
}
static void fnet_tcp_gsm_status_read(void *sock)
//timo poll routine
{
fnet_socket_t *sk = (fnet_socket_t *)sock;
fnet_socket_state_t skState;
fnet_netif_t *netif;

   if( MGsmRegistered()){
      skState = MGsmSocketState(sk->descriptor);
      if(!(sk->state == SS_CONNECTED && skState == SS_CONNECTING)){
      //communication problem with GSM module (locked by someone else or ...
         if( sk->state != skState){
            netif = (fnet_netif_t *)fnet_netif_get_default();
            switch (skState){
               case SS_CONNECTED :
                  if( !MGsmSocketAddress( sk->descriptor, (struct sockaddr_in *)&sk->local_addr)){
                     return; // try again later
                  }
                  fnet_netif_set_ip4_addr((fnet_netif_desc_t)netif, ((struct sockaddr_in *)&sk->local_addr)->sin_addr.s_addr);
                  fnet_netif_set_ip4_addr_automatic( netif);
                  if( netif->api && netif->if_ptr){
                     ((fnet_3G_if_t *)netif->if_ptr)->connected = YES;
                  }
                  break;

               case SS_UNCONNECTED :
                  if( !MGsmSocketClose( sk->descriptor)){
                     return; // try close again next time
                  }

               default :
                  fnet_netif_set_ip4_addr( netif, 0);
                  fnet_netif_set_ip4_addr_automatic( netif);
                  if( netif->api && netif->if_ptr){
                     ((fnet_3G_if_t*)netif->if_ptr)->connected = NO;
                  }
                  break;
            }
         }

         sk->state = skState;
      }
   }

   fnet_poll_service_unregister( stateService);
   stateService = FNET_ERR;
}

#endif
