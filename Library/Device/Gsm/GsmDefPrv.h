//******************************************************************************
//
//   GsmDefPrv.h     Gsm module internal definitions
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GsmDefPrv_H__
#define __GsmDefPrv_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

#ifndef __GsmDef_H__
   #include "GsmDef.h"
#endif

typedef struct {
   // SEND channel values
   /*[mins(multiplier of 5)]*/
                  byte        FirstSmsTimeout;  // timeout for first sms delivery
   /*[count]*/    byte        FirstSmsRetries;  // retry count for first sms
   /*[count]*/    byte        SmsRetries;       // retry count for sms's(except first one)
   /*[mins]*/     byte        AckSmsTimeout;    // timeout for ack message

   // RECEIVE Channel values
   // selective ack expiration timer recomputed for new data - exponential moving average coeficient
   /*[mins(multiplier of 5)]*/
                  byte        StartValue;          // first value for expiration timer
   /*[0.0-1.0]*/  float       alpha;               // value between 0 and 1 - weight of previous calculation
   /*[count]*/    byte        beta;                // result multiplier
   // selective ack expiration timer for timer expiration (next period multiplier)
   /*[count]*/    byte        gama;
} TGsmChannelParameters;

extern       TGsmChannelParameters GsmChannelParams;

// SMS <stat> code
typedef enum {
   GSM_SMS_REC_UNREAD,
   GSM_SMS_REC_READ,
   GSM_SMS_REC_UNSENT,
   GSM_SMS_REC_SENT,
   GSM_SMS_ALL
} ESmsStatCode;

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif
// array sizes
#define SMS_ADDRESS_SIZE   12
#define SMS_TIMESTAMP_SIZE 7
#define SMS_UDL_SIZE       GSM_SMS_BYTE_SIZE

// masks for first byte
#define SMS_FO_MTI_MASK    0x3u
#define SMS_FO_MTI_SHIFT   0u
#define SMS_FO_MMS_MASK    0x4u
#define SMS_FO_MMS_SHIFT   2u
#define SMS_FO_RD_MASK     SMS_FO_MMS_MASK
#define SMS_FO_RD_SHIFT    SMS_FO_MMS_SHIFT
#define SMS_FO_LP_MASK     0x8u
#define SMS_FO_LP_SHIFT    3u
#define SMS_FO_VPF_MASK    0x18u
#define SMS_FO_VPF_SHIFT   SMS_FO_LP_SHIFT
#define SMS_FO_SRI_MASK    0x20
#define SMS_FO_SRI_SHIFT   5u
#define SMS_FO_SRR_MASK    SMS_FO_SRI_MASK
#define SMS_FO_SRR_SHIFT   SMS_FO_SRI_SHIFT
#define SMS_FO_SRQ_MASK    SMS_FO_SRI_MASK
#define SMS_FO_SRQ_SHIFT   SMS_FO_SRI_SHIFT
#define SMS_FO_UDHI_MASK   0x40
#define SMS_FO_UDHI_SHIFT  6u
#define SMS_FO_RP_MASK     0x80
#define SMS_FO_RP_SHIFT    7u

// masks for address specification
#define SMS_TON_MASK       0x70
#define SMS_TON_SHIFT      4u
#define SMS_NPI_MASK       0xF
#define SMS_NPI_SHIFT      0u

typedef enum {
   SMS_DELIVER = 0,
   SMS_DELIVER_REPORT = 0,
   SMS_SUBMIT = 1,
   SMS_SUBMIT_REPORT = 1,
   SMS_COMMAND = 2,
   SMS_STATUS_REPORT = 2,
   SMS_INVALID = 3
} ESmsPduType;

typedef enum {
   SMS_VPF_NONE,
   SMS_VPF_ENHANCED,
   SMS_VPF_RELATIVE,
   SMS_VPF_ABSOLUTE
} ESmsVPType;

typedef enum {
   SMS_DCS_7BIT_DEFAULT,
   SMS_DCS_8BIT = 1<<2,
   SMS_DCS_UCS2 = 1<<3,
   SMS_DCS_NONE = 0xFF
} ESmsEncodingType;

typedef enum {
   SMS_TON_UNKNOWN,
   SMS_TON_INTERNATIONAL,
   SMS_TON_NATIONAL,
   SMS_TON_NETWORK_SPECIFIC,
   SMS_TON_SUBSCRIBER,
   SMS_TON_ALPHANUMERIC,
   SMS_TON_ABBREVIATED,
   SMS_TON_RESERVED
} ESmsTypeOfNumber;

typedef enum {
   SMS_NPI_UNKNOWN,
   SMS_NPI_ISDN_PHONE,
   SMS_NPI_DATA = 3,
   SMS_NPI_TELEX,
   SMS_NPI_SCSPECIFIC1,
   SMS_NPI_SCSPECIFIC2,
   SMS_NPI_NATIONAL = 8,
   SMS_NPI_PRIVATE,
   SMS_NPI_ERMES,
   SMS_NPI_RESERVED = 0xF
} ESmsNumberingPlanIdentifier;

typedef enum {
   SMS_CT_ENQUIRY,
   SMS_CT_CANCEL,
   SMS_CT_DELETE,
   SMS_CT_ENABLE_SR,
   _SMS_CT_LAST
} ESmsCommandType;

typedef enum {
   //OK group
   SMS_ST_OK = 0,
   SMS_ST_OK_RECEIVED_BY_SME = SMS_ST_OK,
   SMS_ST_OK_FORWARDED_TO_SME,
   SMS_ST_OK_REPLACED_BY_SC,
   //TRYING group
   SMS_ST_TRYING = 0x20,
   SMS_ST_TRYING_CONGESTION = SMS_ST_TRYING,
   SMS_ST_TRYING_SME_BUSY,
   SMS_ST_TRYING_NO_SME_RESPONSE,
   SMS_ST_TRYING_REJECTED,
   SMS_ST_TRYING_QOS_NOT_AVAILABLE,
   SMS_ST_TRYING_SME_ERROR,
   //permanent ERROR group
   SMS_ST_ERROR = 0x40,
   SMS_ST_ERROR_REMOTE_PROCEDURE = SMS_ST_ERROR,
   SMS_ST_ERROR_INCOMPATIBLE_DEST,
   SMS_ST_ERROR_REJECTED_BY_SME,
   SMS_ST_ERROR_NOT_OBTAINABLE,
   SMS_ST_ERROR_QOS_NOT_AVAILABLE,
   SMS_ST_ERROR_NO_ITERWORKING,
   SMS_ST_ERROR_SM_VALIDITY_EXPIRED,
   SMS_ST_ERROR_SM_DELETED_BY_SENDER,
   SMS_ST_ERROR_SM_DELETED_BY_SC,
   SMS_ST_ERROR_SM_NOT_EXIST,
   //temporary ERROR group
   SMS_ST_ERROR_TEMP = 0x60,
   SMS_ST_ERROR_TEMP_CONGESTION = SMS_ST_ERROR_TEMP,
   SMS_ST_ERROR_TEMP_SME_BUSY,
   SMS_ST_ERROR_TEMP_NO_SME_RESPONSE,
   SMS_ST_ERROR_TEMP_REJECTED,
   SMS_ST_ERROR_TEMP_QOS_NOT_AVAILABLE,
   SMS_ST_ERROR_TEMP_SME_ERROR,
   //unknown
   SMS_ST_UNKNOWN = 0xFF,
   _SMS_ST_LAST = SMS_ST_UNKNOWN,
   // other statuses
   SMS_ERR_NETWORK_LOST = 538
} ESmsStatus;

// defines for pdu bytes/bits settings
#define SmsPduSetMti(to, type) (to) &= ~SMS_FO_MTI_MASK;(to) |= ((type) << SMS_FO_MTI_SHIFT) & SMS_FO_MTI_MASK
#define SmsPduGetMti(from)     (((from) & SMS_FO_MTI_MASK) >> SMS_FO_MTI_SHIFT)
#define SmsPduSetSrr(to)       (to) |= (1 << SMS_FO_SRR_SHIFT)
#define SmsPduClrSrr(to)       (to) &= ~(1 << SMS_FO_SRR_SHIFT)
#define SmsPduSetVpf(to, type) (to) &= ~SMS_FO_VPF_MASK;(to) |= ((type) << SMS_FO_VPF_SHIFT) & SMS_FO_VPF_MASK
#define SmsPduSetUdhi(to)      (to) |= (1 << SMS_FO_UDHI_SHIFT)
#define SmsPduGetUdhi(from)    (((from) & SMS_FO_UDHI_MASK) >> SMS_FO_UDHI_SHIFT)
#define SmsPduSetRp(to)        (to) |= (1 << SMS_FO_RP_SHIFT)
#define SmsPduClrRp(to)        (to) &= ~(1 << SMS_FO_RP_SHIFT)

#define SmsPduSetTon(to, type) (to) &= ~SMS_TON_MASK;(to) |= 0x80 | (((type) << SMS_TON_SHIFT) & SMS_TON_MASK)
#define SmsPduGetTon(from)     (((from) & SMS_TON_MASK) >> SMS_TON_SHIFT)
#define SmsPduSetNpi(to, type) (to) &= ~SMS_NPI_MASK;(to) |= ((type) << SMS_NPI_SHIFT) & SMS_NPI_MASK
#define SmsPduGetNpi(from)     (((from) & SMS_NPI_MASK) >> SMS_NPI_SHIFT)

//------------------------------------------------------------------------------
//   Submit
//------------------------------------------------------------------------------

typedef struct {
   byte Fo;                   // first octet
   byte Mr;                   // message reference
   byte Da[SMS_ADDRESS_SIZE]; // destination address
   byte Pid;                  // protocol identifier
   byte Dcs;                  // data coding scheme
   byte Vp;                   // validity period
   byte Udl;                  // user data length
   byte Ud[SMS_UDL_SIZE];     // user data
} __packed TSmsSubmit;

extern int TSmsSubmitToRaw(TSmsSubmit *packet);
// convert structure representation to raw byte representation, return raw size

//------------------------------------------------------------------------------
//   Command + report
//------------------------------------------------------------------------------

typedef struct {
   byte Fo;
   byte Mr;
   byte Pid;
   byte Ct;
   byte Mn;
   byte Da[SMS_ADDRESS_SIZE];
   byte Cdl;
   byte Cd[156];
} __packed TSmsCommand;

extern int TSmsCommandToRaw(TSmsCommand *packet);
// convert structure representation to raw byte representation, return raw size

typedef struct {
   byte Fo;
   byte Mr;
   byte Ra[SMS_ADDRESS_SIZE];
   byte Scts[SMS_TIMESTAMP_SIZE];
   byte Dt[SMS_TIMESTAMP_SIZE];
   byte St;
   byte Pi;
   byte Pid;
   byte Dcs;
   byte Udl;
   byte Ud[143];
} __packed TSmsStatusReport;

extern void TSmsRawToStatusReport(TSmsStatusReport *packet, int rawSize);
// convert raw representation to structure representation

//------------------------------------------------------------------------------
//   Deliver
//------------------------------------------------------------------------------


#define SMS_UDH_IEI_CONCAT_MSG         0  // IEI code for concatenation sms
#define SMS_UDH_IELEN_CONCAT_MSG       3  // IEI data length for concat sms
#define SMS_UDH_SIZE                   6  // User Data Header size
#define SMS_UDH_LEN                    (SMS_UDH_SIZE - 1)  // UdhLength field value
#define SMS_UD_DATA_SIZE               (SMS_UDL_SIZE-SMS_UDH_SIZE)

// UDH Source Indicator is used as selective ack message indicator
#define SMS_UDHSI_LEN                  3
#define SMS_UDHSI_SIZE                 (SMS_UDH_SIZE + sizeof(TSmsAckData) - TSizeOf(TSmsAckData, BitMask))
#define SMS_UDH_IEI_SOURCE_INDICATOR   1  // IEI code for concatenation sms
#define SMS_UDH_IELEN_SOURCE_INDICATOR 1  // IEI data length for concat sms
#define SMS_UDHSI_VALUE                1

typedef struct {
   byte Udl;
   byte UdhLength;
   byte UdhIEI;
   byte UdhIELen;
   byte UdhRN;
   byte UdhTN;
   byte UdhSN;
   byte Ud[SMS_UD_DATA_SIZE];
} __packed TSmsBinaryData;

#define SmsBinaryDataLen( udl)   ((udl) + 1)

typedef struct {
   byte UdhSIIEI;
   byte UdhSIIELen;
   byte UdhSI;          // UdhSI field
   byte ForRN;          // which UdhRN this ack refers to
   byte BitMask;        // bit mask of variable size
} __packed TSmsAckData;



typedef struct {
   byte Udl;
   byte Ud[SMS_UDL_SIZE];
} __packed TSmsTextData; // same size as binary data

typedef struct {
   byte Fo;
   byte Oa[SMS_ADDRESS_SIZE];
   byte Pid;
   byte Dcs;
   byte Scts[SMS_TIMESTAMP_SIZE];
   union {
      TSmsBinaryData binary;
      TSmsTextData   text;
   } Data;
} __packed TSmsDeliver;


extern void TSmsRawToDeliver(TSmsDeliver *packet, int rawSize);
// convert raw representation to structure representation

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

//------------------------------------------------------------------------------
//   Message identificator
//------------------------------------------------------------------------------
typedef struct {
   byte           Mr;                        // message reference
   byte           Da[SMS_ADDRESS_SIZE];      // destination address(phone number)
   word           Status;                    // remote status according to status report (status from SC)
   UDateTimeGauge TimeStamp;                 // status reception timestamp
} TSmsId;

//------------------------------------------------------------------------------
//   Channel descriptors
//------------------------------------------------------------------------------

#define SMS_CHANNEL_INPUT           0
#define SMS_CHANNEL_OUTPUT          1
#define SMS_CHANNEL_READ            1
#define SMS_CHANNEL_UNREAD          0
#define SMS_CHANNEL_ACK             1
#define SMS_CHANNEL_NONACK          0
#define SMS_CHANNEL_DELETE_AFTER    1
#define SMS_CHANNEL_NODELETE_AFTER  0


// single sms with descriptor and data pointer
typedef struct {
   byte   Mr;
   byte   Status;
   void  *SmsData;
#ifdef __GSM_SMS_STATISTICS__
   UClockGauge SendTime;
   UClockGauge AckTime;
#endif
} TSmsChunk;

// one I/O channel descriptor
typedef struct _SmsChannel{
   struct _SmsChannel      *next;        // pointer to next
   struct _SmsChannel      *prev;        // pointer to prev
   TSmsChannel             Descriptor;   // channel descriptor
   TSmsChunk               *Chunks;      // channel chunks - sms's
   int                     ChunkCount;   // channel sms's current count
   int                     ChunkMaxCount;// channel sms's maximum count
   byte                    Address[SMS_ADDRESS_SIZE]; // address (phonenumber)
   unsigned int            Direction  :1;// channel info
   unsigned int            Readed     :1;// valid only for input direction, sets whether allready readed this data
   unsigned int            AckChannel :1;// this channel is internal channel used for ack sending
   unsigned int            DeleteAfter:1;// delete after send - valid only for send channel
   unsigned int            SendStatus :4;// channel send status

   float                   Delay;            // delay for timer[mins]
   dword                   Timer;            // timer
   byte                    RetryCount;       // Rest retries of resending
   UClockGauge             LastSmsTimeStamp; // last timestamp of some action
   float                   Intra;            // intra timer (only for input ch.)
   dword                   LastAckTimer; // last send ack timestamp (for incomming channel only)
} TSmsChannelDesc;

typedef struct _SmsIChannelHistory{
   //5 minute history for all existing incomming channels
   struct _SmsIChannelHistory  *next;
   byte                       Address[SMS_ADDRESS_SIZE];
   byte                       RN;         // reference for channel
   byte                       TN;         // total count of sms in channel
   dword                      Timer;      // rest history record validity
} TSmsIChannelHistory;

//------------------------------------------------------------------------------
//   Function description
//------------------------------------------------------------------------------
#ifdef __cplusplus
   extern "C" {
#endif
TYesNo _GsmStatusRead(TSmsId *id);
// read sms status report
TYesNo _GsmDataRead( TSmsId *id, TSmsBinaryData *data);
// get binary sms from memory if some available
TYesNo _GsmDataWrite(TSmsBinaryData *data, TSmsId *id, int validityMins);
// send binary sms to network if possible
TYesNo _AddressToPhone(byte *Address, char *PhoneNumber);
// Converts address to ASCII <PhoneNumber> representation
TYesNo _PhoneToAddress(char *PhoneNumber, byte *Address);
// Converts ASCII <PhoneNumber> to address representation
TYesNo _PhoneAddressesEqu(byte *Address1, byte * Address2);
// check whether 2 addresses are equal
#ifdef __cplusplus
   }
#endif
#endif // __GsmDefPrv_H__


