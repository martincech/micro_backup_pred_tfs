//******************************************************************************
//
//   GsmChannel.c     Gsm channel routines
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#include "Gsm.h"
#include "GsmDefPrv.h"
#include "Multitasking/Multitasking.h"
#include "Memory/mempool.h"
#include "System/System.h"                 // Sysclock
#include "Timer/Timer.h"                   // Systimer
#include <string.h>

static byte          _GsmSmsChannelStack[GSM_SMS_CHANNEL_STACK_SIZE];
static TMempoolDesc  _GsmMemPool;

// Timeouts
#define GSM_CHANNEL_TIMEOUT_INVALID          -1
#define GSM_CHANNEL_REF_TIMER                SysTime()
#define GSM_CHANNEL_REF_TIMER                SysTime()
#define _GsmChannelTimeout(timer)            (!_GsmChannelTimeoutRunning(timer) || TimeAfter(GSM_CHANNEL_REF_TIMER, timer))
#define _GsmChannelTimeoutSet(timer, min)    (timer = GSM_CHANNEL_REF_TIMER + (dword)((((float)min)+0.5)*(float)TIME_MIN))
#define _GsmChannelTimeoutReset(timer)       (timer = GSM_CHANNEL_TIMEOUT_INVALID)
#define _GsmChannelTimeoutRunning(timer)     (timer != GSM_CHANNEL_TIMEOUT_INVALID)

#ifndef __WIN32__
static
#endif
TSmsChannelDesc *DescHead;
// all channels queue header

#ifndef __WIN32__
static
#endif
TSmsIChannelHistory *DescHistory;
// history channels queue header

// Local functions :
static TSmsChannelDesc *_ChannelDescAlloc();
// allocate new channel
static void _ChannelDescFree(TSmsChannelDesc *desc);
// free channel
static TSmsChannelDesc *_ChannelDescFind( TSmsChannel channel);
// find channel by its number
static void _GsmSmsChunkFree(TSmsChunk *chunk);
// Free resources for chunk data
static void _GsmSmsChunksFree(TSmsChunk *chunk, int chunkCount);
// Free resources for all chunks


static void _GsmSmsChannelOutputExecutive();
// Executive for output channels processing, send data for current send channel
static void _GsmSmsChannelInputExecutive();
// Executive for processing new incomming message, adding to channel and creating one for new messages
static void _HistoryExecute();
// remove all old history records(older than timeout)
static TYesNo _HistoryAppend( TSmsChannelDesc *desc);
// append new channel to history
static TYesNo _HistoryIsHistory( TSmsId *incSmsId, TSmsBinaryData *incSms);
// go through history and check whether incomming data belongs to history
static TYesNo _GsmSmsChannelDuplicateMessage( TSmsChannelDesc *desc, TSmsBinaryData *Data);
// Check if this message allready received in channel
static void _GsmSmsChannelParseAck( TSmsBinaryData *ackSms, TSmsId *id);
// Parse incomming selective ack message
static TYesNo _GsmSmsChannelSendAck( TSmsChannelDesc *desc);
// Send selective ack for channel
static byte _GsmSmsChannelGetExclusiveUdhRN(TSmsChannelDesc *desc);
// get exclusive reference number for this channel, phone and direction must be set
static float _InputChannelCalcIntraTimer( TSmsChannelDesc *desc);
// calc new value of timer when duplicite message arrives
static float _InputChannelCalcExpiredTimer( TSmsChannelDesc *desc);
// calc new value of timer when timer previously expired
static TYesNo _GsmSmsNoSignalFailure( word status);
// check status value if it corresponds with no signal signature
static TSmsChannel _GsmSmsChannelSend( char *PhoneNumber, byte *Data, int DataSize, TYesNo DelAfter);
// Send data stream to <PhoneNumber>, SMS_INVALID_CHANNEL returned when problem occured

//------------------------------------------------------------------------------
//   Init
//------------------------------------------------------------------------------

TYesNo GsmSmsChannelInit( void)
// Initialize SMS channel service
{
   _GsmMemPool = MempoolInit(_GsmSmsChannelStack, GSM_SMS_CHANNEL_STACK_SIZE);
   DescHead = NULL;
   DescHistory = NULL;
   return YES;
}

//------------------------------------------------------------------------------
//   Free
//------------------------------------------------------------------------------

void GsmSmsChannelDeinit( void)
// Deinitialize channel
{
TSmsIChannelHistory *hist;

   while(DescHead){
      _GsmSmsChunksFree(DescHead->Chunks, DescHead->ChunkCount);
      _ChannelDescFree(DescHead);
   }

   hist = DescHistory;
   while(hist){
      _GsmChannelTimeoutReset(hist->Timer);
      hist = hist->next;
   }
   _HistoryExecute();
   MempoolRelease( _GsmMemPool);
}

TYesNo GsmSmsChannelHasLiveChannel( void)
// return YES if there is some existing channel(out or in)
{
   return (DescHead != NULL);
}

//------------------------------------------------------------------------------
//   Send
//------------------------------------------------------------------------------
TSmsChannel GsmSmsChannelSendAndDel( char *PhoneNumber, byte *Data, int DataSize)
// Send data stream to <PhoneNumber>, SMS_INVALID_CHANNEL returned when problem occured
// delete channel after succesfull/unsuccesfull send, GsmSmsChannelSendStatus on this channel is not valid
{
   return _GsmSmsChannelSend( PhoneNumber, Data, DataSize, YES);
}

TSmsChannel GsmSmsChannelSend( char *PhoneNumber, byte *Data, int DataSize)
// Send data stream to <PhoneNumber>, SMS_INVALID_CHANNEL returned when problem occured
{
   return _GsmSmsChannelSend( PhoneNumber, Data, DataSize, NO);
}

static TSmsChannel _GsmSmsChannelSend( char *PhoneNumber, byte *Data, int DataSize, TYesNo DelAfter)
// Send data stream to <PhoneNumber>, SMS_INVALID_CHANNEL returned when problem occured
{
int               SmsCount;
int               RN;
int               i;
int               Size;
TSmsBinaryData    *SmsData;
TSmsChannelDesc   *desc;
byte              *DataPtr;

   if( DataSize > GSM_SMS_CHANNEL_DATA_MAX_SIZE){
      return SMS_INVALID_CHANNEL;
   }
   // allocate descriptor
   desc = _ChannelDescAlloc();
   if( !desc){return SMS_INVALID_CHANNEL;}

   // set some of channel structure fields
   if( !_PhoneToAddress( PhoneNumber, desc->Address)){
      _ChannelDescFree(desc);
      return SMS_INVALID_CHANNEL;
   }
   desc->Direction = SMS_CHANNEL_OUTPUT;
   desc->AckChannel = SMS_CHANNEL_NONACK;
   desc->Readed = SMS_CHANNEL_UNREAD;
   desc->DeleteAfter = DelAfter ? SMS_CHANNEL_DELETE_AFTER : SMS_CHANNEL_NODELETE_AFTER ;
   desc->SendStatus = GSM_SMS_SEND_WAIT_START;
   // Calc required fields for packeting data
   RN = _GsmSmsChannelGetExclusiveUdhRN(desc);
   SmsCount = (DataSize / SMS_UD_DATA_SIZE);
   SmsCount += (DataSize % SMS_UD_DATA_SIZE) != 0? 1 : 0;
   // allocate memory for chunks
   desc->Chunks = MempoolMalloc( _GsmMemPool, sizeof(TSmsChunk)*SmsCount);
   if( !desc->Chunks){_ChannelDescFree(desc);return SMS_INVALID_CHANNEL;}
   desc->ChunkMaxCount = SmsCount;
   desc->ChunkCount = 0;

   // allocate place for data in chunks and copy data from buffer
   DataPtr = Data;
   for( i = 0; i < SmsCount; i++){
      if( DataSize >= SMS_UD_DATA_SIZE){
         Size = SMS_UDL_SIZE;
      } else {
         Size = DataSize + SMS_UDH_SIZE;
      }
      desc->Chunks[i].SmsData = MempoolMalloc( _GsmMemPool, SmsBinaryDataLen(Size));
      // check allocation problems, free allready allocaten memory when some
      if( !desc->Chunks[i].SmsData){
         _GsmSmsChunksFree(desc->Chunks, i-1);
         _ChannelDescFree(desc);
         return SMS_INVALID_CHANNEL;
      }

      SmsData = (TSmsBinaryData*)desc->Chunks[i].SmsData;
      // create UDH
      SmsData->Udl = Size;
      SmsData->UdhLength = SMS_UDH_LEN;
      SmsData->UdhIEI = SMS_UDH_IEI_CONCAT_MSG;
      SmsData->UdhIELen = SMS_UDH_IELEN_CONCAT_MSG;
      SmsData->UdhRN = RN;
      SmsData->UdhSN = i + 1; // sms indices begin at 1
      SmsData->UdhTN = SmsCount;
      // copy data
      Size -= SMS_UDH_SIZE;
      memcpy(SmsData->Ud, DataPtr, Size);
      DataPtr  +=  Size;      // move data ptr
      DataSize -= Size;       // calc rest data size
      // fill in channel chunk other fields
      desc->Chunks[i].Status = SMS_ST_UNKNOWN;
      desc->Chunks[i].Mr = 0;
      _GsmChannelTimeoutReset(desc->Timer);
   }

   return (EChannelStatus)desc->Descriptor;
}

//------------------------------------------------------------------------------
//   Send status
//------------------------------------------------------------------------------

TSmsChannelSendStatus GsmSmsChannelSendStatus( TSmsChannel channel)
// Get status of current channel
{
TSmsChannelDesc   *desc;
TSmsChannelSendStatus ret;

   desc = _ChannelDescFind(channel);
   if( !desc || desc->Direction != SMS_CHANNEL_OUTPUT ||
       desc->DeleteAfter == SMS_CHANNEL_DELETE_AFTER){
      return GSM_SMS_SEND_WRONG_CHANNEL_DESC;
   }

   ret = desc->SendStatus;
   switch( desc->SendStatus){
      case GSM_SMS_SEND_OK:
      case GSM_SMS_SEND_ERROR:
      case GSM_SMS_SEND_ERROR_INTERNAL:
      case GSM_SMS_SEND_TIMEOUT:
         desc->SendStatus = GSM_SMS_SEND_STOPPED;
      default:
         break;
   }

   return ret;
}

//------------------------------------------------------------------------------
//   Read
//------------------------------------------------------------------------------

TSmsChannel GsmSmsChannelIncomming(char *PhoneNumber, int *DataSize)
// Check new incomming data, return <Data> and it description if some new data avaible, SMS_INVALID_CHANNEL returned when no new unread data present
{
TSmsChannelDesc   *desc;

   desc = DescHead;
   while( desc){
      if( desc->Direction == SMS_CHANNEL_INPUT &&     // input channel
          desc->ChunkMaxCount == desc->ChunkCount &&  // with complete data
          desc->Readed != SMS_CHANNEL_READ){          // still unread
         //current desc is unread channel with complete data
         _AddressToPhone(desc->Address, PhoneNumber);
         desc->Readed = SMS_CHANNEL_READ;
         *DataSize = (desc->ChunkMaxCount-1) * SMS_UD_DATA_SIZE;
         *DataSize += ((TSmsBinaryData *)desc->Chunks[desc->ChunkMaxCount-1].SmsData)->Udl - SMS_UDH_SIZE;
         desc->ChunkCount = 1;            // no chunk readed
         return desc->Descriptor;
      }
      desc = desc->next;
   }
   return SMS_INVALID_CHANNEL;
}

int GsmSmsChannelRead(TSmsChannel channel, byte *data, int maxSize)
// Read data from channel with incomming data (as previously get by GsmSmsChannelInput)
// return size of actually readed data or SMS_INVALID_CHANNEL when all data readed or wrong channel descriptor
{
TSmsChannelDesc   *desc;
TSmsBinaryData    *SmsData;
int               Size;
int               i, count;

   desc = _ChannelDescFind( channel);
   if( !desc ||                                 // channel exists
       desc->Direction != SMS_CHANNEL_INPUT ||  // input channel
       desc->Readed != SMS_CHANNEL_READ){       // was readed before(returned by incomming function)
      return SMS_INVALID_CHANNEL;
   }

   Size = 0;

   while( maxSize){
      //find first unread chunk of data by sequence number
      for(i = 0; i < desc->ChunkMaxCount; i++){
         SmsData = (TSmsBinaryData *)desc->Chunks[i].SmsData;
         if(SmsData && SmsData->UdhSN == desc->ChunkCount){
            break;
         }
      }

      // copy data from this chunk to output buffer
      count = SmsData->Udl - SMS_UDH_SIZE;
      if( maxSize < count){
         count = maxSize;
      }
      memcpy( &data[Size], SmsData->Ud, count);
      Size += count;
      // clear chunk data and move chunk count index
      SmsData->Udl -= count;
      if( SmsData->Udl == SMS_UDH_SIZE){
         // append channel to history before deleting
         if( desc->ChunkCount == desc->ChunkMaxCount){
            _HistoryAppend( desc);
         }
         // free ocupied memory for data
         _GsmSmsChunkFree(&desc->Chunks[i]);
         desc->ChunkCount++;
         if(desc->ChunkCount > desc->ChunkMaxCount){
            // all data read allready, clear channel and return
            _GsmSmsChunksFree( desc->Chunks, 0);
            _ChannelDescFree(desc);
            return Size;
         }
      } else {
         // move rest unread data to begining of memory
         memmove(SmsData->Ud, &SmsData->Ud[i], SmsData->Udl);
      }
      maxSize -= count;
   }
   return Size;
}

//------------------------------------------------------------------------------
//   Close
//------------------------------------------------------------------------------

void GsmSmsChannelClose( TSmsChannel channel)
// Close existing Send stop data sending prematurely
{
TSmsChannelDesc *desc;

   desc = _ChannelDescFind( channel);
   if( !desc ||
       desc->AckChannel != SMS_CHANNEL_NONACK){
      return;
   }
   if( desc->Direction == SMS_CHANNEL_OUTPUT){
      desc->SendStatus = GSM_SMS_SEND_STOPPED;
   }else{
      _GsmSmsChunksFree( desc->Chunks, desc->ChunkMaxCount);
      _ChannelDescFree(desc);
   }
}

//------------------------------------------------------------------------------
//   Executive
//------------------------------------------------------------------------------

void GsmSmsChannelExecutive()
// Executive for sms channel I/O operations
{
   // process output channel
   _GsmSmsChannelOutputExecutive();
   // process input channel
   _GsmSmsChannelInputExecutive();
   // process history
   _HistoryExecute();
}

//------------------------------------------------------------------------------
//   OUTPUT
//------------------------------------------------------------------------------

static void _GsmSmsChannelOutputExecutive()
// Executive for output channels processing, send data for current send channel
{
TSmsChannelDesc *desc;
TSmsBinaryData  *DataPtr;
TSmsId           NewSmsId;
byte             Status;
int              i;
byte             count;

   // process any incomming status reports
   while( _GsmStatusRead(&NewSmsId)){
      MultitaskingReschedule();
      desc = DescHead;
      while(desc){
         if(desc->Direction != SMS_CHANNEL_OUTPUT ||
            !memequ(desc->Address, NewSmsId.Da, SMS_ADDRESS_SIZE)){
            // doesn't belong to this channel
            desc = desc->next;
            continue;
         }
         // may belong to this channel, check message reference
         if(desc->SendStatus == GSM_SMS_SEND_RESENDING){
            count = desc->ChunkMaxCount;
         }else {
            count = desc->ChunkCount;
         }
         for(i = 0; i < count; i++){
            if( desc->Chunks[i].Mr == NewSmsId.Mr){
               break;
            }
         }
         if( i == count){
            // this MR doesn't belong to this channel
            desc = desc->next;
            continue;
         }

         // OK, it belong to current chanel, don't go any further
         if( desc->Chunks[i].Status >= SMS_ST_ERROR_TEMP ||
            (desc->Chunks[i].Status >= SMS_ST_TRYING &&
             desc->Chunks[i].Status < SMS_ST_ERROR)){
            //change status only if it is not permanent error or OK
            desc->Chunks[i].Status = NewSmsId.Status;
            desc->LastSmsTimeStamp = NewSmsId.TimeStamp;
#ifdef __GSM_SMS_STATISTICS__
            desc->Chunks[i].AckTime = NewSmsId.TimeStamp;
            GsmStatistics.AverageDeliverTime += desc->Chunks[i].AckTime - desc->Chunks[i].SendTime;
            GsmStatistics.AverageDeliverTime /= ++GsmStatistics.AverageDeliverCount;
#endif
         }
         if( desc->AckChannel != SMS_CHANNEL_ACK){
            _GsmChannelTimeoutSet(desc->Timer, desc->Delay);
         }
         break;
      }
   }

   // send sms for every channel which needs to be sent
   desc = DescHead;
   while( desc){
      if( desc->Direction != SMS_CHANNEL_OUTPUT){
         desc = desc->next;
         continue;
      }
      MultitaskingReschedule();
      //------------------------------------------------------------------------------
      /* SEND FSM */
      switch( desc->SendStatus){
         case GSM_SMS_SEND_RESEND_FIRST:
#ifdef __GSM_SMS_STATISTICS__
   GsmStatistics.SmsBLostCount++;
#endif
         case GSM_SMS_SEND_WAIT_START:
            // send first chunk and set long timeout
            desc->ChunkCount = 0;
            DataPtr = desc->Chunks[desc->ChunkCount].SmsData;
            memcpy( NewSmsId.Da, desc->Address, SMS_ADDRESS_SIZE);
            if( !_GsmDataWrite(DataPtr, &NewSmsId, GsmChannelParams.FirstSmsTimeout)){
               if( _GsmSmsNoSignalFailure(NewSmsId.Status)){
                  // no signal, try again later
                  break;
               }
               desc->SendStatus = GSM_SMS_SEND_ERROR_INTERNAL;
               break;
            }
#ifdef __GSM_SMS_STATISTICS__
            desc->Chunks[desc->ChunkCount].SendTime = GSM_CHANNEL_REF_TIMER;
#endif
            desc->Chunks[desc->ChunkCount].Mr = NewSmsId.Mr;
            desc->Chunks[desc->ChunkCount].Status = NewSmsId.Status;
            desc->ChunkCount++;
            if(desc->SendStatus == GSM_SMS_SEND_WAIT_START){
               desc->RetryCount = GsmChannelParams.FirstSmsRetries;
               desc->Delay = GsmChannelParams.FirstSmsTimeout+1;
            }
            _GsmChannelTimeoutSet(desc->Timer, desc->Delay);
            desc->SendStatus = GSM_SMS_SEND_WAIT_FIRST_ACK;
            break;

         case GSM_SMS_SEND_WAIT_FIRST_ACK:
            Status = desc->Chunks[0].Status;
            if( Status < SMS_ST_TRYING){
               // first sms delivered
               if( desc->ChunkCount == desc->ChunkMaxCount){
                  // if it was the only one, go to OK state immediately
                  desc->SendStatus = GSM_SMS_SEND_OK;
                  break;
               }
               desc->SendStatus = GSM_SMS_SEND_SENDING;
               desc->Delay = GsmChannelParams.FirstSmsTimeout;
               desc->RetryCount = GsmChannelParams.SmsRetries;
               _GsmChannelTimeoutSet(desc->Timer, desc->Delay+1);
               break;
            }
            if( Status >= SMS_ST_ERROR &&
                Status < SMS_ST_ERROR_TEMP){
               // delivery problem
               desc->SendStatus = GSM_SMS_SEND_ERROR;
               break;
            }

            if( _GsmChannelTimeout(desc->Timer)){
               // timeout
               if( desc->RetryCount){
                  desc->RetryCount--;
                  desc->SendStatus = GSM_SMS_SEND_RESEND_FIRST;
                  break;
               }
               desc->SendStatus = GSM_SMS_SEND_TIMEOUT;
#ifdef __GSM_SMS_STATISTICS__
               if( desc->AckChannel == SMS_CHANNEL_ACK){
                  GsmStatistics.SmsALostCount++;
               }
#endif
            }
            // just wait for timeout or other event
            break;

         case GSM_SMS_SEND_RESENDING:
            // get first non acked
            while(( desc->ChunkCount != desc->ChunkMaxCount) &&
                  (desc->Chunks[desc->ChunkCount].Status < SMS_ST_TRYING)){
               desc->ChunkCount++;
            }
#ifdef __GSM_SMS_STATISTICS__
   GsmStatistics.SmsBLostCount++;
#endif
         case GSM_SMS_SEND_SENDING:
            if( desc->ChunkCount == desc->ChunkMaxCount){
               //all sms's sent, wait for acks
               desc->SendStatus = GSM_SMS_SEND_WAIT_SOME_ACK;
               _GsmChannelTimeoutSet(desc->Timer, desc->Delay+1);
               break;
            }

            //send chunk
            DataPtr = desc->Chunks[desc->ChunkCount].SmsData;
            memcpy( NewSmsId.Da, desc->Address, SMS_ADDRESS_SIZE);
            if( !_GsmDataWrite(DataPtr, &NewSmsId, desc->Delay)){
               if( _GsmSmsNoSignalFailure(NewSmsId.Status)){
                  // no signal, try again later
                  if( _GsmChannelTimeout( desc->Timer)){
                     if( !desc->RetryCount){
                        desc->SendStatus = GSM_SMS_SEND_ERROR;
                        break;
                     }
                     desc->RetryCount--;
                     _GsmChannelTimeoutSet(desc->Timer, desc->Delay+1);
                  }
                  break;
               }
               desc->SendStatus = GSM_SMS_SEND_ERROR_INTERNAL;
               break;
            }

#ifdef __GSM_SMS_STATISTICS__
            desc->Chunks[desc->ChunkCount].SendTime = GSM_CHANNEL_REF_TIMER;
#endif
            desc->Chunks[desc->ChunkCount].Mr = NewSmsId.Mr;
            desc->ChunkCount++;
            _GsmChannelTimeoutSet(desc->Timer, desc->Delay+1);
            //continue with another chunk
            break;

         case GSM_SMS_SEND_WAIT_SOME_ACK :
            desc->SendStatus = GSM_SMS_SEND_OK;
            for(i = 0; i < desc->ChunkCount; i++){
               Status = desc->Chunks[i].Status;
               if( Status < SMS_ST_TRYING){
                  //ok, confirmed
                  if( desc->AckChannel != SMS_CHANNEL_ACK){
                     _GsmSmsChunkFree(&desc->Chunks[i]);
                  }
                  continue;
               }
               if( Status >= SMS_ST_ERROR && Status < SMS_ST_ERROR_TEMP){
                  //permanent error, stop sending
                  desc->SendStatus = GSM_SMS_SEND_ERROR;
                  break;
               }
               //still trying
               desc->SendStatus = GSM_SMS_SEND_WAIT_SOME_ACK;
            }

            // send again when timed out
            if( desc->SendStatus == GSM_SMS_SEND_WAIT_SOME_ACK
                && _GsmChannelTimeout( desc->Timer)){

               if( !desc->RetryCount){
                  desc->SendStatus = GSM_SMS_SEND_ERROR;
                  break;
               }
               //resend all non-acked
               if( _GsmChannelTimeoutRunning(desc->Timer)){
                  desc->RetryCount--;
               }
               desc->ChunkCount = 0;
               desc->SendStatus = GSM_SMS_SEND_RESENDING;
               _GsmChannelTimeoutReset(desc->Timer);
            }
            break;

         case GSM_SMS_SEND_STOPPED:{
            _GsmSmsChunksFree( desc->Chunks, desc->ChunkMaxCount);
            TSmsChannelDesc *descBack = desc;
            desc = desc->next;
            _ChannelDescFree(descBack);
            continue;
            }

         case GSM_SMS_SEND_OK:
         case GSM_SMS_SEND_ERROR:
         case GSM_SMS_SEND_ERROR_INTERNAL:
         case GSM_SMS_SEND_TIMEOUT:
            //clear internal channels, other channels cleared by status reading
            if( desc->AckChannel == SMS_CHANNEL_ACK){
               TSmsChannelDesc   *adesc;
               TSmsAckData       *ack;
               ack = (TSmsAckData *)((TSmsBinaryData*)desc->Chunks[0].SmsData)->Ud;
               desc->SendStatus = GSM_SMS_SEND_STOPPED;
               adesc = DescHead;
               while( adesc){
                  if( adesc->Direction != SMS_CHANNEL_INPUT || adesc->AckChannel != SMS_CHANNEL_NONACK){
                     adesc = adesc->next;
                     continue;
                  }
                  //possible channel, check RN
                  if(((TSmsBinaryData*)adesc->Chunks[0].SmsData)->UdhRN != ack->ForRN){
                     // wrong RN, different channel
                     adesc = adesc->next;
                     continue;
                  }
                  // allow new ack resending for input channel
                  _GsmChannelTimeoutReset( adesc->LastAckTimer);
                  break;
               }
            } else if( desc->DeleteAfter){
               desc->SendStatus = GSM_SMS_SEND_STOPPED;
            }
         default:
            desc = desc->next;
            continue;
      }
      //------------------------------------------------------------------------------
      desc = desc->next;
   }
}

//------------------------------------------------------------------------------
//   INPUT
//------------------------------------------------------------------------------

static void _GsmSmsChannelInputExecutive()
// Executive for processing new incomming message, adding to channel and creating one for new messages
{
TSmsChannelDesc *desc;
TSmsBinaryData  *DataPtr;
TSmsBinaryData   NewSmsData;
TSmsId           NewSmsId;

   // process new incomming data
   while( _GsmDataRead(&NewSmsId, &NewSmsData)){
      MultitaskingReschedule();
      // check correctness of message
      if( NewSmsData.Udl <= SMS_UDH_SIZE ||
          (NewSmsData.UdhLength != SMS_UDH_LEN &&
           NewSmsData.UdhLength != (SMS_UDH_LEN + SMS_UDHSI_LEN)) ||
          NewSmsData.UdhIEI != SMS_UDH_IEI_CONCAT_MSG ||
          NewSmsData.UdhIELen != SMS_UDH_IELEN_CONCAT_MSG ||
          NewSmsData.UdhSN == 0 || NewSmsData.UdhTN == 0 ||
          NewSmsData.UdhSN > NewSmsData.UdhTN){
         continue;
      }
      if( NewSmsData.UdhLength == (SMS_UDH_LEN + SMS_UDHSI_LEN)){
         // selective ack message, different behavior
         _GsmSmsChannelParseAck( &NewSmsData, &NewSmsId);
         continue;
      }

      // check if this data belongs to any known channel
      desc = DescHead;
      while( desc){
         if( desc->Direction == SMS_CHANNEL_INPUT &&
             memequ(NewSmsId.Da, desc->Address, SMS_ADDRESS_SIZE) &&
             (((TSmsBinaryData  *)desc->Chunks[0].SmsData)->UdhRN ==
               NewSmsData.UdhRN)){
            break;
         }
         desc = desc->next;
      }
      if( !desc){
         if( _HistoryIsHistory( &NewSmsId, &NewSmsData)){
            // is part of history - this data allready readed
            continue;
         }
         // doesn't belong to any channel, create one
         desc = _ChannelDescAlloc();
         if( !desc){ break;} // no free space, data will be lost

         desc->ChunkCount = 0;
         desc->ChunkMaxCount = NewSmsData.UdhTN;
         desc->Chunks = MempoolMalloc( _GsmMemPool, sizeof(TSmsChunk)*desc->ChunkMaxCount);
         if(!desc->Chunks){_ChannelDescFree(desc);break;} // no free space, data will be lost

         memcpy(desc->Address, NewSmsId.Da, SMS_ADDRESS_SIZE);
         desc->Direction = SMS_CHANNEL_INPUT;
         desc->AckChannel = SMS_CHANNEL_NONACK;
         desc->Readed = SMS_CHANNEL_UNREAD;
         desc->SendStatus = GSM_SMS_SEND_WRONG_CHANNEL_DESC;

         desc->Delay = GsmChannelParams.FirstSmsTimeout;
         desc->Timer = 0;
         desc->RetryCount = GsmChannelParams.SmsRetries;
         desc->LastSmsTimeStamp = NewSmsId.TimeStamp;
         _GsmChannelTimeoutReset(desc->LastAckTimer);
         desc->Intra = GsmChannelParams.StartValue;
      }

      // known channel, check duplicity of arrival message
      if( _GsmSmsChannelDuplicateMessage(desc, &NewSmsData)){
         // this message allready present
         if( _GsmChannelTimeout(desc->LastAckTimer)){
            // last ack is not still sending
            if( _GsmSmsChannelSendAck(desc)){
               _GsmChannelTimeoutSet(desc->LastAckTimer, GsmChannelParams.AckSmsTimeout);
            }
         }
      } else{
         DataPtr = MempoolMalloc( _GsmMemPool, SmsBinaryDataLen(NewSmsData.Udl));
         if( !DataPtr){break;} // this sms will be lost
         memcpy(DataPtr, &NewSmsData, SmsBinaryDataLen(NewSmsData.Udl));
         desc->Chunks[desc->ChunkCount].SmsData = (void *)DataPtr;
         desc->ChunkCount++;
         _GsmChannelTimeoutReset(desc->LastAckTimer);
      }

      if( desc->ChunkCount == desc->ChunkMaxCount){
         _GsmChannelTimeoutReset( desc->Timer);
      } else{
         desc->Delay = _InputChannelCalcIntraTimer( desc);
         _GsmChannelTimeoutSet(desc->Timer, desc->Delay);
      }
   }

   // process timeouts
   desc = DescHead;
   while( desc){
      if( desc->Direction == SMS_CHANNEL_OUTPUT ||
          desc->AckChannel == SMS_CHANNEL_ACK ||
          desc->Readed == SMS_CHANNEL_READ){
         desc = desc->next;
         continue;
      }

      if( _GsmChannelTimeoutRunning(desc->Timer) && _GsmChannelTimeout(desc->Timer) &&
          _GsmChannelTimeout(desc->LastAckTimer)){
         // send Ack sms
         if( !_GsmChannelTimeoutRunning(desc->LastAckTimer)){
            if( !desc->RetryCount){
               _GsmSmsChunksFree(desc->Chunks, desc->ChunkCount);
               TSmsChannelDesc *db = desc->next;
               _ChannelDescFree(desc);
               desc = db;
               continue;
            }
            desc->RetryCount--;
         }
         if( _GsmSmsChannelSendAck( desc)){
            _GsmChannelTimeoutSet(desc->LastAckTimer, GsmChannelParams.AckSmsTimeout);
         }
         desc->Delay = _InputChannelCalcExpiredTimer(desc);
         _GsmChannelTimeoutSet(desc->Timer, desc->Delay);
         MultitaskingReschedule();
         continue;
      }
      desc = desc->next;
   }
}

//------------------------------------------------------------------------------
//   Channel allocation
//------------------------------------------------------------------------------

static TSmsChannelDesc *_ChannelDescAlloc()
{
TSmsChannelDesc *desc;

   desc = MempoolMalloc( _GsmMemPool, sizeof(TSmsChannelDesc));
   if( desc == NULL){return NULL;}
   memset(desc, 0, sizeof(TSmsChannelDesc));
   desc->SendStatus = GSM_SMS_SEND_WAIT_START;

   if(!DescHead){
      DescHead = desc;
      return desc;
   }
   desc->next = DescHead;
   DescHead->prev = desc;
   desc->Descriptor = DescHead->Descriptor + 1;
   if( desc->Descriptor < 0){desc->Descriptor = 0;} //avoid overflow
   // move header pointer
   DescHead = desc;
   return desc;
}

//------------------------------------------------------------------------------
//   Free channel
//------------------------------------------------------------------------------

static void _ChannelDescFree(TSmsChannelDesc *desc)
{
   if( !desc){return;}
   if(desc->prev){
      desc->prev->next = desc->next;
   }
   if( desc->next){
      desc->next->prev = desc->prev;
   }
   if( desc == DescHead){
      DescHead = desc->next;
   }
   MempoolFree( _GsmMemPool, desc);
}

//------------------------------------------------------------------------------
//   Find descriptor
//------------------------------------------------------------------------------

static TSmsChannelDesc *_ChannelDescFind( TSmsChannel channel)
{
TSmsChannelDesc *desc;

   if( channel == SMS_INVALID_CHANNEL){
      return NULL;
   }
   desc = DescHead;
   while( desc){
      if( desc->Descriptor == channel){
         break;
      }
      desc = desc->next;
   }
   return desc;
}

//------------------------------------------------------------------------------
//   Free chunk
//------------------------------------------------------------------------------

static void _GsmSmsChunkFree(TSmsChunk *chunk)
// Free resources for chunk data
{
   if(chunk->SmsData == NULL){
      return;
   }

   MempoolFree( _GsmMemPool, chunk->SmsData);
   chunk->SmsData = NULL;
}

//------------------------------------------------------------------------------
//   Free all chunks
//------------------------------------------------------------------------------

static void _GsmSmsChunksFree(TSmsChunk *chunk, int chunkCount)
// Free resources for all chunks
{
   if( !chunk){
      return;
   }
   while(chunkCount-- > 0){
      _GsmSmsChunkFree( &chunk[chunkCount]);
   }
   if( chunk){
      MempoolFree(_GsmMemPool, chunk);
      chunk = NULL;
   }
}

//------------------------------------------------------------------------------
//   Duplicate message test
//------------------------------------------------------------------------------

static TYesNo _GsmSmsChannelDuplicateMessage( TSmsChannelDesc *desc, TSmsBinaryData *Data)
// Check if this message allready received in channel
{
int i;

   if ( desc->Readed == SMS_CHANNEL_READ ||
        desc->ChunkCount == desc->ChunkMaxCount){
      return YES;
   }

   for(i = 0; i < desc->ChunkCount; i++){
      if(((TSmsBinaryData  *)desc->Chunks[i].SmsData)->UdhSN == Data->UdhSN){
         return YES;
         break;
      }
   }
   return NO;
}

//------------------------------------------------------------------------------
//   Selective ack parse
//------------------------------------------------------------------------------
static void _GsmSmsChannelParseAck( TSmsBinaryData *ackSms, TSmsId *id)
// Parse incomming selective ack message
{
TSmsAckData       *ack;
TSmsChannelDesc   *desc;
byte              *bitmask;
int               i;

   ack = (TSmsAckData*)ackSms->Ud;
   if( ack->UdhSIIEI != SMS_UDH_IEI_SOURCE_INDICATOR ||
       ack->UdhSIIELen != SMS_UDH_IELEN_SOURCE_INDICATOR ||
       ack->UdhSI != SMS_UDHSI_VALUE){
      //wrong format
      return;
   }

   desc = DescHead;
   while(desc){
      if( desc->Direction != SMS_CHANNEL_OUTPUT ||
          desc->AckChannel != SMS_CHANNEL_NONACK){
         desc = desc->next;
         continue;
      }
      //possible channel, find RN
      for(i = 0; i < desc->ChunkMaxCount; i++){
         if( !desc->Chunks[i].SmsData){
            continue;
         }
         break;
      }
      if( (i == desc->ChunkMaxCount) ||
          ((TSmsBinaryData*)desc->Chunks[i].SmsData)->UdhRN != ack->ForRN){
         // wrong RN or channel completelly acked
         desc = desc->next;
         continue;
      }

      // found channel for which is this ack
      bitmask = &ack->BitMask;
      for( i = 0; i < desc->ChunkMaxCount; i++){
         if( bitmask[i >> 3] & (0x80 >> (i & 7))){
#ifdef __GSM_SMS_STATISTICS__
            if( desc->Chunks[i].Status != SMS_ST_OK){
               desc->Chunks[i].AckTime = id->TimeStamp;
               GsmStatistics.AverageDeliverTime += desc->Chunks[i].AckTime - desc->Chunks[i].SendTime;
               GsmStatistics.AverageDeliverTime /= ++GsmStatistics.AverageDeliverCount;
            }
#endif
            desc->Chunks[i].Status = SMS_ST_OK;
         }
      }

      _GsmChannelTimeoutReset(desc->Timer);
      return;
   }
}

//------------------------------------------------------------------------------
//   Selective ack
//------------------------------------------------------------------------------
static TYesNo _GsmSmsChannelSendAck( TSmsChannelDesc *forDesc)
// Send selective ack for channel
{
TSmsChannelDesc   *desc;
TSmsBinaryData    *Data;
TSmsAckData       *AckData;
TSmsId             SmsId;
byte               Size;
byte               *BitMaskPtr;
int                i,j;

   // allocate descriptor
   desc = _ChannelDescAlloc();
   if(!desc){return NO;}
   desc->Chunks = MempoolMalloc(_GsmMemPool, sizeof(TSmsChunk));
   if(!desc->Chunks){return NO;}
   memcpy(desc->Address, forDesc->Address, SMS_ADDRESS_SIZE);
   desc->Direction = SMS_CHANNEL_OUTPUT;
   desc->AckChannel = SMS_CHANNEL_ACK;
   desc->ChunkCount = desc->ChunkMaxCount = 1;
   desc->Readed = SMS_CHANNEL_READ; // prevent read from outside this module
   desc->RetryCount = 0;
   //compute byte size for bitfield (div8)
   Size = SmsBinaryDataLen(SMS_UDHSI_SIZE + (forDesc->ChunkMaxCount >> 3));
   if( forDesc->ChunkMaxCount & 7){
      Size += 1; // add one for rest bits
   }
   desc->Chunks[0].SmsData = MempoolMalloc( _GsmMemPool, Size);
   Data = (TSmsBinaryData *)desc->Chunks[0].SmsData;
   if( !Data){
      _GsmSmsChunksFree(desc->Chunks, desc->ChunkMaxCount);
      _ChannelDescFree(desc);
      return NO;
   }
   memset(Data, 0, Size);
   AckData = (TSmsAckData *)Data->Ud;
   // create UDH
   Data->Udl = Size - sizeof(Data->Udl);
   Data->UdhLength = SMS_UDH_LEN + SMS_UDHSI_LEN;
   Data->UdhIEI = SMS_UDH_IEI_CONCAT_MSG;
   Data->UdhIELen = SMS_UDH_IELEN_CONCAT_MSG;
   Data->UdhRN = _GsmSmsChannelGetExclusiveUdhRN(desc);
   Data->UdhTN = Data->UdhSN = 1;
   AckData->UdhSIIEI = SMS_UDH_IEI_SOURCE_INDICATOR;
   AckData->UdhSIIELen = SMS_UDH_IELEN_SOURCE_INDICATOR;
   AckData->UdhSI = SMS_UDHSI_VALUE;
   AckData->ForRN = ((TSmsBinaryData *)forDesc->Chunks[0].SmsData)->UdhRN;
   BitMaskPtr = &AckData->BitMask;
   for( i = 0; i < forDesc->ChunkCount; i++){
      j = ((TSmsBinaryData *)forDesc->Chunks[i].SmsData)->UdhSN - 1;
      BitMaskPtr[j >> 3] |= 0x80 >> (j & 7);
   }

   memcpy( SmsId.Da, desc->Address, SMS_ADDRESS_SIZE);
   // try to send immediatelly out of order
   if( !_GsmDataWrite(Data, &SmsId, GsmChannelParams.FirstSmsTimeout)){
      if( _GsmSmsNoSignalFailure(SmsId.Status)){
         // no signal, add to sending queue
         desc->SendStatus = GSM_SMS_SEND_WAIT_START;
         return YES;
      }
      _GsmSmsChunksFree(desc->Chunks, desc->ChunkMaxCount);
      _ChannelDescFree(desc);
      return NO;
   }
   desc->SendStatus = GSM_SMS_SEND_WAIT_FIRST_ACK;
   desc->Delay = GsmChannelParams.FirstSmsTimeout+1;
   _GsmChannelTimeoutSet(desc->Timer, desc->Delay);
   desc->Chunks[0].Mr = SmsId.Mr;
   desc->Chunks[0].Status = SmsId.Status;
   return YES;
}

//------------------------------------------------------------------------------
//   Exclusive RN
//------------------------------------------------------------------------------

static byte _GsmSmsChannelGetExclusiveUdhRN(TSmsChannelDesc *desc)
// get exclusive reference number for this channel, phone and direction must be set
{
static byte       NextRN = 0;
byte              RN;
TSmsChannelDesc   *listDesc;

   RN = NextRN;
   listDesc = desc->next;
   while( listDesc){
      // calc from list reference number for UDH
      if(listDesc->Direction == desc->Direction &&
         memequ(listDesc->Address, desc->Address, SMS_ADDRESS_SIZE) &&
         (RN == ((TSmsBinaryData *)  listDesc->Chunks[0].SmsData)->UdhRN)){
         // reference number allready used
            RN++;
            // check new reference number
            listDesc = desc->next;
            continue;
      }
      listDesc = listDesc->next;
   }
   NextRN = RN + 1;
   return RN;
}

//------------------------------------------------------------------------------
//   Timer calculation
//------------------------------------------------------------------------------

static float _InputChannelCalcIntraTimer( TSmsChannelDesc *desc)
// calc new value of timer when duplicite message arrives
{
   //intra = aplha(intra) + (1-alpha)(timenow - timeprev)
   desc->Intra = (GsmChannelParams.alpha * desc->Intra)
                 + (1.0 - GsmChannelParams.alpha) *((GSM_CHANNEL_REF_TIMER - desc->LastSmsTimeStamp) / TIME_MIN);
   return desc->Intra*GsmChannelParams.beta;
}

static float _InputChannelCalcExpiredTimer( TSmsChannelDesc *desc)
// calc new value of timer when timer previously expired
{
   return desc->Delay * GsmChannelParams.gama;
}

//------------------------------------------------------------------------------
//   History
//------------------------------------------------------------------------------

static TYesNo _HistoryAppend( TSmsChannelDesc *desc)
// append new channel to history
{
TSmsIChannelHistory *newHistory;
int                 i;

   newHistory = DescHistory;
   while(newHistory){
      //get last item in history and append this new item as next item
      if( !newHistory->next){
         break;
      }
      newHistory = newHistory->next;
   }
   if( newHistory){
      newHistory->next = MempoolMalloc( _GsmMemPool, sizeof(TSmsIChannelHistory));
      newHistory = newHistory->next;
   }else{
      DescHistory = MempoolMalloc( _GsmMemPool, sizeof(TSmsIChannelHistory));
      newHistory = DescHistory;
   }

   if( !newHistory){
      return NO;
   }

   newHistory->next = NULL;
   memcpy(newHistory->Address, desc->Address, SMS_ADDRESS_SIZE);
   for( i = 0; i < desc->ChunkMaxCount; i++){
      if( desc->Chunks[i].SmsData != NULL){
         newHistory->RN = ((TSmsBinaryData*)desc->Chunks[i].SmsData)->UdhRN;
         newHistory->TN = ((TSmsBinaryData*)desc->Chunks[i].SmsData)->UdhTN;
         break;
      }
   }
   if( i == desc->ChunkMaxCount){
      MempoolFree( _GsmMemPool, newHistory);
      newHistory = NULL;
      return NO;
   }
   _GsmChannelTimeoutSet( newHistory->Timer, GsmChannelParams.FirstSmsTimeout + 1);
   return YES;
}

static void _HistoryExecute()
// remove all old history records(older than timeout)
{
TSmsIChannelHistory *newHistory;

   while( DescHistory && _GsmChannelTimeout(DescHistory->Timer)){
      newHistory = DescHistory->next;
      MempoolFree( _GsmMemPool, DescHistory);
      DescHistory = newHistory;
   }
}

static TYesNo _HistoryIsHistory( TSmsId *incSmsId, TSmsBinaryData *incSms)
// go through history and check whether incomming data belongs to history
{
TSmsIChannelHistory *history;

   history = DescHistory;
   while( history){
      if( memequ(history->Address, incSmsId->Da, SMS_ADDRESS_SIZE) &&
          (history->RN == incSms->UdhRN) && (incSms->UdhTN == history->TN)){
         return YES;
      }
      history = history->next;
   }
   return NO;
}

//------------------------------------------------------------------------------
//   No signal check
//------------------------------------------------------------------------------

static TYesNo _GsmSmsNoSignalFailure( word status)
// check status value if it corresponds with no signal signature
{
   switch( status){
      case SMS_ERR_NETWORK_LOST:
         return YES;
      default: return NO;
   }
}


