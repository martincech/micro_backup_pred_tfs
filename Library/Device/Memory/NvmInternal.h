//*****************************************************************************
//
//   NvmInternal.h   Nonvolatile memory internals
//   Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __NvmInternal_H__
   #define __NvmInternal_H__

#ifndef __NvmDef_H__
   #include "Memory/NvmDef.h"
#endif

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

#define NVM_PAGE_SIZE_MODULE       256

typedef word TNvmPageSize;

#define NvmPageSize( Size)        ((Size) * NVM_PAGE_SIZE_MODULE)
#define NvmPageSizeSet( Size)     ((Size) / NVM_PAGE_SIZE_MODULE)

//------------------------------------------------------------------------------
//  Page cache
//------------------------------------------------------------------------------

// Page case Status
typedef enum {
   NVM_PAGE_NEW,                       // page loaded, not modified
   NVM_PAGE_DIRTY,                     // page loaded, modified
   NVM_PAGE_SAVED,                     // page already saved
   _NVM_PAGE_STATUS_LAST,              // last page status
   NVM_PAGE_STATUS_INVALID = 0xFFFFFFFF
} ENvmPageStatus;

typedef struct {
   dword        Status;                // page status
   TNvmAddress  Address;               // cached page address
} TNvmCache;

#define NvmCacheSize()                  sizeof( TNvmCache)

#define NVM_PAGE_MAGIC   0x1234ABCD

typedef struct {
   dword     Magic;                    // page magic number
   TNvmCache Cache;                    // cached data description
   byte      *Data;                    // page data
} TNvmPage;

#define NvmPageLayoutSize( Size)      (sizeof( TNvmPage) + (Size))
#define NvmPageHeaderSize()            sizeof( TNvmPage)
#define NvmPageDataAddress( Address)  ((Address) + sizeof( TNvmPage))

//------------------------------------------------------------------------------
//  Section
//------------------------------------------------------------------------------

// Section mode :
typedef enum {
   NVM_SECTION_STANDARD,               // standard read/write page cache
   NVM_SECTION_WRITE_ONLY,             // don't read flash page to cache
   NVM_SECTION_COMMIT_ONLY,            // write cache on commit only

   NVM_SECTION_NOCACHE,                // direct read/write without cache
   NVM_SECTION_CACHE_ONLY,             // direct read/write to cache only

   NVM_SECTION_RAM,                    // volatile RAM

   _NVM_SECTION_OPTION_LAST
} ENvmSectionMode;

typedef struct {
   TNvmAddress  StartAddress;          // section start address
   TNvmAddress  EndAddress;            // section end address
   TNvmAddress  CacheAddress;          // page cache address
   TNvmPageSize PageSize;              // section page size
   word         Mode;                  // section mode
   TNvmCache   *Cache;                 // dynamic cache data
} TNvmSection;   

#endif
