//*****************************************************************************
//
//   FileInternal.h   File internal definitions
//   Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __FileInternal_H__
   #define __FileInternal_H__

#ifndef __NvmDef_H__
   #include "Memory/NvmDef.h"
#endif

#ifndef __FileDef_H__
   #include "Memory/FileDef.h"
#endif

#define FILE_UNUSED_PTR       (0)

typedef byte     (*TFilePermission)( void);
typedef TYesNo   (*TFileOpen)( TFile *, TFileName, TFileMode);
typedef TYesNo   (*TFileClose)( TFile *);
typedef TYesNo   (*TFileSave)( TFile *, TFileAddress, const void *, int);
typedef TYesNo   (*TFileLoad)( TFile *, TFileAddress, void *, int);
typedef TYesNo   (*TFileFill)( TFile *, TFileAddress, byte, int);
typedef TYesNo   (*TFileMatch)( TFile *, TFileAddress, const void*, int);
typedef TFileSum (*TFileSumF)( TFile *, TFileAddress, int);
typedef TYesNo   (*TFileMove)( TFile *, TFileAddress, TFileAddress, int);
typedef TYesNo   (*TFileCommit)( TFile *);

typedef struct {
   TFilePermission  Permission;
   TFileOpen       Open;
   TFileClose      Close;
   TFileSave       Save;
   TFileLoad       Load;
   TFileFill       Fill;
   TFileMatch      Match;
   TFileSumF       Sum;
   TFileMove       Move;
   TFileCommit     Commit;
} TFileInterface;


typedef const struct {
   TNvmAddress Start;
   TNvmAddress Size;
} TFileDescriptor;

extern TFileDescriptor FileDescriptor[_FILE_COUNT];



extern const TFileItem FileItem[_FILE_LIST_COUNT];

#endif
