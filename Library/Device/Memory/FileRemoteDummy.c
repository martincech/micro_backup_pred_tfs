//******************************************************************************
//
//   FileRemoteDummy.c      File
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#include "FileRemote.h"

byte FileRemotePermission( void)
// Permission
{
   return FILE_MODE_READ_WRITE;
} // FileRemotePermission


//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo FileRemoteOpen( TFile *File, TFileName FileName, byte Mode)
// Open <FileName> with <Mode> access
{
   return NO;
} // FileOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

TYesNo FileRemoteClose( TFile *File)
// Close <File>
{
   return NO;
} // FileClose

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

TYesNo FileRemoteSave( TFile *File, TFileAddress Address, const void *Data, int Size)
// Save <Data> with <Size> at <Address>
{
   return NO;
} // FileSave

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

TYesNo FileRemoteLoad( TFile *File, TFileAddress Address, void *Data, int Size)
// Load <Data> with <Size> from <Address>
{
   return NO;
} // FileLoad

//------------------------------------------------------------------------------
//   Fill
//------------------------------------------------------------------------------

TYesNo FileRemoteFill( TFile *File, TFileAddress Address, byte Pattern, int Size)
// Write <Pattern> with <Size> at <Address>
{
   return NO;
} // FileFill

//------------------------------------------------------------------------------
//   Match
//------------------------------------------------------------------------------

TYesNo FileRemoteMatch( TFile *File, TFileAddress Address, const void *Data, int Size)
// Compare <Data> with <Size> at <Address>
{
   return NO;
} // FileMatch

//------------------------------------------------------------------------------
//   Sum
//------------------------------------------------------------------------------

TFileSum FileRemoteSum( TFile *File, TFileAddress Address, int Size)
// Sum NVM contents at <Address> with <Size>
{
   return 0;
} // FileSum


//------------------------------------------------------------------------------
//   Sum
//------------------------------------------------------------------------------

TFileSum FileRemoteCommit( TFile *File)
// Commit sandboxed <File>
{
   return 0;
} // FileCommit
