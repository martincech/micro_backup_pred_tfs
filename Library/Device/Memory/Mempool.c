//******************************************************************************
//
//   Mempool.h     Memory pool implementation (malloc etc)
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************
#include "mempool.h"

#ifdef __WIN32__
   #include <stdlib.h>
   #include <string.h>
   //array of pools for size computation
   typedef struct{
      void *ChunkPtr;
      int   ChunkSize;
   }  TMemPoolChunk;
   typedef struct{
      long used;
      long maxSize;
      TMemPoolChunk *chunks;
      int chunksSize;
   } TMemPoolId;

   #define MEMPOOLS_MAX 255
   static TMemPoolId pools[ MEMPOOLS_MAX];
   static int currentPools = 0;

   TMempoolDesc MempoolInit( void *PoolPtr, unsigned long PoolSize)
   // init new mempool and return its descriptor
   {
      pools[currentPools].used = 0;
      pools[currentPools].maxSize = PoolSize;
      pools[currentPools].chunks = NULL;
      pools[currentPools].chunksSize = 0;
      return currentPools++;
   }
   void MempoolRelease( TMempoolDesc mempool)
   // release mempool
   {
      int i,j;
      if( mempool >= MEMPOOLS_MAX){
         return;
      }
      for( i = 0; i < currentPools; i++){
         if( pools[i].chunksSize == 0){ continue;}
         for( j = 0; j < pools[i].chunksSize; j++){
            free( pools[i].chunks[i].ChunkPtr);
         }
         free(pools[i].chunks);
      }

      memmove(&pools[mempool], &pools[mempool + 1], sizeof(TMemPoolId)*(MEMPOOLS_MAX - mempool));
      currentPools--;
   }

   void *MempoolMalloc( TMempoolDesc mempool, unsigned nbytes)
   // malloc nbytes of data, return its descriptor
   {
      if( mempool >= currentPools){
         return NULL;
      }
      if( pools[mempool].used + nbytes > pools[mempool].maxSize){
         return NULL;
      }
      pools[mempool].used += nbytes;
      pools[mempool].chunks = realloc(pools[mempool].chunks, sizeof(TMemPoolChunk)*(pools[mempool].chunksSize + 1));
      pools[mempool].chunks[pools[mempool].chunksSize].ChunkPtr = malloc(nbytes);
      pools[mempool].chunks[pools[mempool].chunksSize].ChunkSize = nbytes;
      return pools[mempool].chunks[pools[mempool].chunksSize++].ChunkPtr;
   }

   void MempoolFree( TMempoolDesc mempool, void *ap)
   // free previously malloced data
   {
      if( mempool >= currentPools){
         return;
      }
      int chunk;
      for( chunk = 0; chunk < pools[mempool].chunksSize; chunk++){
          if( pools[mempool].chunks[chunk].ChunkPtr == ap){
             break;
          }
      }
      if( chunk == pools[mempool].chunksSize){return;}
      free(pools[mempool].chunks[chunk].ChunkPtr);
      pools[mempool].used -= pools[mempool].chunks[chunk].ChunkSize;
      pools[mempool].chunks = realloc(pools[mempool].chunks, sizeof(TMemPoolChunk)*pools[mempool].chunksSize - 1);
      pools[mempool].chunksSize--;
   }

   long MempoolFreeStatus(TMempoolDesc mempool)
   // return current free memory size
   {
      if( mempool >= currentPools){
         return 0;
      }
      return pools[mempool].maxSize - pools[mempool].used;
   }

   long MempoolMallocMax(TMempoolDesc mempool)
   // return maximum size of possible allocated chunk
   {
      if( mempool >= currentPools){
         return 0;
      }
      return MempoolFreeStatus(mempool);
   }
#else
   //******************************************************************************
   // !!!!!!!!!!!!WARNING current implementation uses modified fnet library
   //******************************************************************************
   #include "fnet_mempool.h"

   TMempoolDesc MempoolInit( void *PoolPtr, unsigned long PoolSize)
   // init new mempool and return its descriptor
   {
      return fnet_mempool_init( PoolPtr, PoolSize, FNET_MEMPOOL_ALIGN_8);
   }
   void MempoolRelease( TMempoolDesc mempool)
   // release mempool
   {
      return fnet_mempool_release( mempool);
   }

   void *MempoolMalloc( TMempoolDesc mempool, unsigned nbytes)
   // malloc nbytes of data, return its descriptor
   {
      return fnet_mempool_malloc( mempool, nbytes );
   }

   void MempoolFree( TMempoolDesc mempool, void *ap)
   // free previously malloced data
   {
      fnet_mempool_free(mempool, ap);
   }

   long MempoolFreeStatus(TMempoolDesc mempool)
   // return current free memory size
   {
      return fnet_mempool_free_mem_status(mempool);
   }

   long MempoolMallocMax(TMempoolDesc mempool)
   // return maximum size of possible allocated chunk
   {
      return fnet_mempool_malloc_max( mempool);
   }
#endif
