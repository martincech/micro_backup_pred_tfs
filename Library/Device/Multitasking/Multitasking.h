//*****************************************************************************
//
//    Multitasking.h     Multitasking
//    Version 1.0        (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Multitasking_H__
   #define __Multitasking_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __MultitaskingDef_H__
   #include "Multitasking/MultitaskingDef.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

void MultitaskingInit( void);
// Initialization

void MultitaskingTaskRun( TTask *Task);
// Start task

void MultitaskingTaskExit( TTask *Task);
// Terminates task, waits for task exit

void MultitaskingEventSet( TTask *Task, int Event);
// Set event 0 - 31

TYesNo MultitaskingTaskIdle( TTask *Task);
// Checks for task idle

int MultitaskingTaskScheduler( TTask *Task);
// Task scheduler

void MultitaskingReschedule( void);
// Reschedule tasks

void MultitaskingMutexInit(TMutex *Mutex);
// Init mutex

void MultitaskingMutexSet( TMutex *Mutex);
// Set mutex

TYesNo MultitaskingMutexTrySet( TMutex *Mutex);
// try set mutex return immediatelly with lock status

void MultitaskingMutexRelease( TMutex *Mutex);
// Release mutex

#ifdef __cplusplus
   }
#endif

#endif
