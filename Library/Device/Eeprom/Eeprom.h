//*****************************************************************************
//
//    Eeprom.h         Eeprom
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Eeprom_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

void EepromInit( void);
// Init

void EepromWrite(int Address, const void *Data, int Size);
// Write

void EepromDwordWrite(int Address, dword Data);
// Write dword

void EepromWordWrite(int Address, word Data);
// Write word

void EepromByteWrite(int Address, byte Data);
// Write byte

void EepromRead(int Address, void *Data, int Size);
// Read

dword EepromDwordRead(int Address);
// Read dword

word EepromWordRead(int Address);
// Read dword

byte EepromByteRead(int Address);
// Read byte

#ifdef __cplusplus
   }
#endif


#endif
