//*****************************************************************************
//
//    EepromFlash.c    Flash emulated eeprom
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#include "Eeprom.h"
#include "Flash/IFlash.h"
#include "Hardware.h"
#include <string.h>
#include "Random/Random.h"

// defined for memory file
#ifndef EEPROM_SIZE
   #define EEPROM_SIZE           127     // must be 2^n - 1 <2-1; FLASH_SECTOR_SIZE-1>
   #define EEPROM_FLASH_SIZE    2048     // {2*FLASH_SECTOR_SIZE; 3*FLASH_SECTOR_SIZE; 4*FLASH_SECTOR_SIZE; ...}
#endif
#if EEPROM_SIZE != 63 && EEPROM_SIZE != 127 && EEPROM_SIZE != 255
   #error
#endif

#if EEPROM_BANK_SIZE > FLASH_SECTOR_SIZE
   #error
#endif

#if EEPROM_FLASH_SIZE < 2 * FLASH_SECTOR_SIZE
   #error
#endif


// internals
#define EEPROM_BANK_SIZE            (EEPROM_SIZE + 1)
#define EEPROM_BANK_COUNT           (EEPROM_FLASH_SIZE / EEPROM_BANK_SIZE)
#define EEPROM_FLASH_SECTOR_COUNT   (EEPROM_FLASH_SIZE / FLASH_SECTOR_SIZE)
#define EEPROM_BANKS_PER_SECTOR     (MCU_FLASH_SECTOR_SIZE / EEPROM_BANK_SIZE)

#if EEPROM_BANK_COUNT > 0xFD // valid tag count - 1, there must be detectable discontinuity
   #error Too much
#endif

typedef struct {
   byte Data[EEPROM_SIZE];
   byte Tag;
} TEepromBank;

typedef const struct {
   TEepromBank Banks[EEPROM_BANK_COUNT];
} TEeprom;

TEeprom * const Eeprom = (TEeprom *) (MCU_FLASH_OFFSET + MCU_FLASH_SIZE - EEPROM_FLASH_SIZE);

static TEepromBank CurrentBank;

static dword CurrentBankId;

static int FindNextEmptyBankAndEraseSectorIfNeccesary( int StartBankId);

static TYesNo BankDirty( int BankId);

static TYesNo BanksOnTheSameSector(int BankId1, int BankId2);

static TYesNo BankIsValid( int BankId);

static TYesNo TagsAreContinuous(byte FirstTag, byte SecondTag);

static dword RandomBankIdGet( void);

//------------------------------------------------------------------------------
//   Initialisation
//------------------------------------------------------------------------------

void EepromInit( void)
// Init
{
int BankId;
int LastBankId;
byte FirstTag;
byte LastTag;
byte TagsFound = 0;
byte Tag;
byte DiscontinuityCount = 0;

   for(BankId = 0 ; BankId < EEPROM_BANK_COUNT ; BankId++) {
      if(!BankIsValid( BankId)) {
         continue;
      }
      Tag = Eeprom->Banks[BankId].Tag;

      // check discontinuity
      switch(TagsFound) {
         case 0: // first tag found, can't check discontinuity yet
            CurrentBankId = BankId;
            FirstTag = Tag;
            break;

         default: // check discontinuity
            if(!TagsAreContinuous(LastTag, Tag)) {
               DiscontinuityCount++;
               CurrentBankId = LastBankId;
            }
            break;
      }

      TagsFound++;
      LastTag = Tag;
      LastBankId = BankId;
   }

   switch(TagsFound) {
      case 0:
         IFlashErase(&Eeprom->Banks[0], &Eeprom->Banks[EEPROM_BANK_COUNT - 1]);
         CurrentBankId = RandomBankIdGet();
         break;;
      default:
         if(!TagsAreContinuous(LastTag, FirstTag)) { // check discontinuity between last and first
            DiscontinuityCount++;
            CurrentBankId = LastBankId;
         }
         break;
   }

   switch(DiscontinuityCount) {
      case 1:
         break;;
      default:
         IFlashErase(&Eeprom->Banks[0], &Eeprom->Banks[EEPROM_BANK_COUNT - 1]);
         CurrentBankId = RandomBankIdGet();
         break;
   }

   memcpy(&CurrentBank, &Eeprom->Banks[CurrentBankId], sizeof(TEepromBank));
} // EepInit

//------------------------------------------------------------------------------
//   Write
//------------------------------------------------------------------------------

void EepromWrite(int Address, const void *Data, int Size)
// Write
{
   if(Address + Size >= EEPROM_SIZE) {
      return;
   }
   memcpy(&CurrentBank.Data[Address], Data, Size);
} // EepWrite

//------------------------------------------------------------------------------
//   Write dword
//------------------------------------------------------------------------------

void EepromDwordWrite(int Address, dword Data)
// Write dword
{
   EepromWrite(Address, &Data, sizeof(Data));
} // EepromDwordWrite

//------------------------------------------------------------------------------
//   Write word
//------------------------------------------------------------------------------

void EepromWordWrite(int Address, word Data)
// Write word
{
   EepromWrite(Address, &Data, sizeof(Data));
} // EepromWordWrite

//------------------------------------------------------------------------------
//   Write byte
//------------------------------------------------------------------------------

void EepromByteWrite(int Address, byte Data)
// Write byte
{
   EepromWrite(Address, &Data, sizeof(Data));
} // EepromByteWrite

//------------------------------------------------------------------------------
//   Execute - perform write operation
//------------------------------------------------------------------------------

void EepromCommit( void)
// Perform write operation
{
   if(!memcmp(&CurrentBank, &Eeprom->Banks[CurrentBankId], sizeof(TEepromBank))) {
      return;
   }
   int NextBankId = CurrentBankId;
   forever {
      NextBankId = FindNextEmptyBankAndEraseSectorIfNeccesary( NextBankId);

      // write data only
      CurrentBank.Tag = 0xFF;
      if(!IFlashProgram(&Eeprom->Banks[NextBankId], &CurrentBank, sizeof(TEepromBank))) {
         continue;
      }
      
      // verify   
      if(memcmp(&Eeprom->Banks[NextBankId], &CurrentBank, sizeof(TEepromBank))) {
         continue;
      }
      
      // confirm by writing tag
      memset(&CurrentBank.Data[sizeof(TEepromBank) - 4], 0xFF, 4);
      CurrentBank.Tag = Eeprom->Banks[CurrentBankId].Tag + 1;
      if(CurrentBank.Tag == 0xFF) {
         CurrentBank.Tag = 0;
      }
      #warning Chyba pri zapisu tagu = problem - pri dalsi inicializaci erase
      IFlashProgram(&Eeprom->Banks[NextBankId].Data[sizeof(TEepromBank) - 4], &CurrentBank.Data[sizeof(TEepromBank) - 4], 4);

      CurrentBankId = NextBankId;
      memcpy(&CurrentBank, &Eeprom->Banks[CurrentBankId], sizeof(TEepromBank));
      break;
   }
}

//------------------------------------------------------------------------------
//   Read
//------------------------------------------------------------------------------

void EepromRead(int Address, void *Data, int Size)
// Read
{
   if(Address + Size >= EEPROM_SIZE) {
      return;
   }
   memcpy(Data, &CurrentBank.Data[Address], Size);
} // EepromRead

//------------------------------------------------------------------------------
//   Read dword
//------------------------------------------------------------------------------

dword EepromDwordRead(int Address)
// Read dword
{
   dword Data;
   EepromRead( Address, &Data, sizeof(Data));
   return Data;
} // EepromDwordRead

//------------------------------------------------------------------------------
//   Read word
//------------------------------------------------------------------------------

word EepromWordRead(int Address)
// Read dword
{
   word Data;
   EepromRead( Address, &Data, sizeof(Data));
   return Data;
} // EepromWordRead

//------------------------------------------------------------------------------
//   Read byte
//------------------------------------------------------------------------------

byte EepromByteRead(int Address)
// Read dword
{
   byte Data;
   EepromRead( Address, &Data, sizeof(Data));
   return Data;
} // EepromByteRead

//******************************************************************************

static int FindNextEmptyBankAndEraseSectorIfNeccesary( int BankId) {
   forever {
      BankId++;
      if(BankId == EEPROM_BANK_COUNT) {
         BankId = 0;
      }
      if(BankId % EEPROM_BANKS_PER_SECTOR == 0) { // crossing sectors, erase?
         if(BanksOnTheSameSector(BankId, CurrentBankId)) {
            BankId = CurrentBankId;            // searching came back to the initial sector, start again
            continue;
         } else {
            forever {
               if(IFlashErase(&Eeprom->Banks[BankId], &Eeprom->Banks[BankId])) {
                  return BankId;
               }
            }

         }

      }
      if(BankDirty( BankId)) {
         continue;
      }

      return BankId;
   }
}

static TYesNo BankDirty( int BankId) {
   byte *Data = &Eeprom->Banks[BankId];
   int i = EEPROM_BANK_SIZE;
   while(i--) {
      if(*Data != 0xFF) {
         return YES;
      }
      Data++;
   }
   return NO;
}

static TYesNo BankIsValid( int BankId) {
   return Eeprom->Banks[BankId].Tag != 0xFF;
}

static TYesNo BanksOnTheSameSector(int BankId1, int BankId2) {
   int Sector1 = (int)&Eeprom->Banks[BankId1] & ~(MCU_FLASH_SECTOR_SIZE - 1);
   int Sector2 = (int)&Eeprom->Banks[BankId2] & ~(MCU_FLASH_SECTOR_SIZE - 1);
   return Sector1 == Sector2;
}

static TYesNo TagsAreContinuous(byte FirstTag, byte SecondTag) {
   FirstTag++;
   if(FirstTag == 0xFF) {
      FirstTag = 0;
   }
   if(FirstTag == SecondTag) {
      return YES;
   }
   return NO;
}

static dword RandomBankIdGet( void) {
   return RandomGet() % EEPROM_BANK_COUNT;
}
