//******************************************************************************
//
//   EfsMaster.c    External File System
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "File/Efs.h"
#include "Spi/SpiCrc.h"
#include "File/EfsRpc.h"
#include "Hardware.h"
#include "System/System.h"


static TEfsCommand Command;
static TEfsReply   Reply;

static TYesNo CommandExecute( word Size, word ReplySize);
// Execute command

static inline void CommandStart( void) { SpiCrcSelect(); }
// Command start

static inline void CommandStop( void) { SpiCrcUnselect(); }
// Command stop

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void EfsInit( void)
// Initialisation
{
   //UsbhPowerInit();
   //UsbhPowerOn();
   SpiCrcInit();
   //SysDelay(1000);
	//Command.Command = EFS_CMD_INIT; z�sek, proto�e v tuhle chv�li je�t� nejsou povolen� p�eru�en�
	//NoDataCommandExecute(EfsSimpleCommandSize(), EfsSimpleReplySize());
   EfsClockSet(SysDateTime());
} // EfsInit

//------------------------------------------------------------------------------
//  Directory create
//------------------------------------------------------------------------------

TYesNo EfsDirectoryCreate( const char *Path)
// Create new directory with <Name>
{
	Command.Command = EFS_CMD_DIRECTORY_CREATE;
	strcpy( Command.Data.DirectoryCreate.Path, Path);
	return CommandExecute(EfsPathCommandSize( Command.Data.DirectoryCreate), EfsSimpleReplySize());
} // EfsDirectoryCreate

//------------------------------------------------------------------------------
//  Directory change
//------------------------------------------------------------------------------

TYesNo EfsDirectoryChange( const char *Path)
// Change current directory to <Name>
{
   Command.Command = EFS_CMD_DIRECTORY_CHANGE;
   strcpy( Command.Data.DirectoryChange.Path, Path);
   return CommandExecute(EfsPathCommandSize( Command.Data.DirectoryChange), EfsSimpleReplySize());
} // EfsDirectoryChange

//------------------------------------------------------------------------------
//  Directory delete
//------------------------------------------------------------------------------

TYesNo EfsDirectoryDelete( const char *Path)
// Delete directory <Name>
{
   Command.Command = EFS_CMD_DIRECTORY_DELETE;
   strcpy( Command.Data.DirectoryDelete.Path, Path);
   return CommandExecute(EfsPathCommandSize( Command.Data.DirectoryDelete), EfsSimpleReplySize());
} // EfsDirectoryDelete

//------------------------------------------------------------------------------
//  File create
//------------------------------------------------------------------------------

TYesNo EfsFileCreate( const char *FileName, dword Size)
// Create <FileName>
{
   Command.Command = EFS_CMD_FILE_CREATE;
   strcpy( Command.Data.FileCreate.Name, FileName);
   Command.Data.FileCreate.Size = Size;
   return CommandExecute(EfsNameCommandSize( Command.Data.FileCreate), EfsSimpleReplySize());
} // EfsFileCreate

//------------------------------------------------------------------------------
//  File open
//------------------------------------------------------------------------------

TYesNo EfsFileOpen( const char *FileName)
// Open <FileName> for read/write
{
   Command.Command = EFS_CMD_FILE_OPEN;
   strcpy( Command.Data.FileOpen.Name, FileName);
   return CommandExecute(EfsNameCommandSize( Command.Data.FileOpen), EfsSimpleReplySize());
} // EfsFileOpen

//------------------------------------------------------------------------------
//  File delete
//------------------------------------------------------------------------------

TYesNo EfsFileDelete( const char *FileName)
// Delete <FileName>
{
   Command.Command = EFS_CMD_FILE_DELETE;
   strcpy( Command.Data.FileDelete.Name, FileName);
   return CommandExecute(EfsNameCommandSize( Command.Data.FileDelete), EfsSimpleReplySize());
} // EfsFileDelete

//------------------------------------------------------------------------------
//  File close
//------------------------------------------------------------------------------

void EfsFileClose( void)
// Close opened file
{
   Command.Command = EFS_CMD_FILE_CLOSE;
   CommandExecute(EfsSimpleCommandSize(), EfsSimpleReplySize());
} // EfsFileClose

//------------------------------------------------------------------------------
//  File write
//------------------------------------------------------------------------------

word EfsFileWrite( void *Data, word Count)
// Write <Data> with <Count> bytes, returns number of bytes written
{
   word PieceNumber = 0;
   word CountRemaining;
   word CountToBeSend;

   Command.Command = EFS_CMD_FILE_WRITE;
   Command.Data.FileWrite.Count = Count;
   
   CommandStart();

   if(!SpiCrcPacketSend( &Command, EfsCommandSize( FileWrite))) {
      CommandStop();
      return 0;
   }

   CountRemaining = Count;
   
   while(CountRemaining) {
	   CountToBeSend = CountRemaining > EFS_DATA_CHUNK_SIZE ? EFS_DATA_CHUNK_SIZE : CountRemaining;
      
      if(!SpiCrcPacketSend( Data, CountToBeSend)) {
         break;
      }

      if(!SpiCrcPacketReceive( &Reply, sizeof(Reply))) {
         break;
      }

      if(SpiCrcReceiveSize() != EfsReplySize(FileWriteConfirmation)) {
         break;
      }
   
      if(Reply.Reply != EfsReply( EFS_CMD_FILE_WRITE)) {
         break;
      }

      if(Reply.Data.FileWriteConfirmation.PieceNumber != PieceNumber) {
         break;
      }

      PieceNumber++;
	   Data += EFS_DATA_CHUNK_SIZE;
	   CountRemaining -= CountToBeSend;
   }

   CommandStop();
   return Count - CountRemaining;
} // EfsFileWrite

//------------------------------------------------------------------------------
//  File read
//------------------------------------------------------------------------------

word EfsFileRead( void *Data, word Count)
// Read <Data> with <Count> bytes, returns number of bytes read
{
   word PieceNumber = 0;
   word CountRemaining;
   word CountReceived;

   Command.Command = EFS_CMD_FILE_READ;
   Command.Data.FileRead.Count = Count;

   CommandStart();

   if(!SpiCrcPacketSend( &Command, EfsCommandSize( FileRead))) {
      CommandStop();
      return 0;
   }

   CountRemaining = Count;

   while(CountRemaining) {
      if(!SpiCrcPacketReceive( Data, CountRemaining)) {
         break;
      }

      CountReceived = SpiCrcReceiveSize();

      Command.Command = EFS_CMD_FILE_READ;
      Command.Data.FileReadConfirmation.PieceNumber = PieceNumber;
      Command.Data.FileReadConfirmation.Repeat = NO;

      if(!SpiCrcPacketSend( &Command, EfsCommandSize( FileReadConfirmation))) {
         break;
      }

      CountRemaining -= CountReceived;

      if(CountReceived != EFS_DATA_CHUNK_SIZE) {
         break;
      }

      Data += EFS_DATA_CHUNK_SIZE;
      PieceNumber++;
   }

   CommandStop();
   return Count - CountRemaining;
} // EfsFileRead

//------------------------------------------------------------------------------
//  File seek
//------------------------------------------------------------------------------

TYesNo EfsFileSeek( int32 Pos, EEfsSeekMode Whence)
// Seek at <Pos> starting from <Whence>
{	
   Command.Command = EFS_CMD_FILE_SEEK;
   Command.Data.FileSeek.Pos = Pos;
   Command.Data.FileSeek.Whence = Whence;
   return CommandExecute( EfsCommandSize( FileSeek), EfsSimpleReplySize());
} // EfsFileSeek

//------------------------------------------------------------------------------
//  Clock set
//------------------------------------------------------------------------------

TYesNo EfsClockSet( dword Clock)
// Synchronize filesystem with <Clock>
{
   Command.Command = EFS_CMD_CLOCK_SET;
   Command.Data.ClockSet.Clock = SysClock();
   return CommandExecute( EfsCommandSize( ClockSet), EfsSimpleReplySize());
} // EfsClockSet

//------------------------------------------------------------------------------
//  Unmount
//------------------------------------------------------------------------------

void EfsUnmount( void)
// Safely unmount drive
{
   Command.Command = EFS_CMD_UNMOUNT;
   return CommandExecute( EfsSimpleCommandSize(), EfsSimpleReplySize());
} // EfsUnmount

//------------------------------------------------------------------------------
//  Directory exists
//------------------------------------------------------------------------------

TYesNo EfsDirectoryExists( const char *Path)
// Check for directory <Name>
{
   Command.Command = EFS_CMD_DIRECTORY_EXISTS;
   strcpy( Command.Data.DirectoryExists.Path, Path);
   if(!CommandExecute( EfsPathCommandSize(Command.Data.DirectoryExists), EfsExistsReplySize()))
   {
      return NO;
   }
   return Reply.Data.DirectoryExists.Exists;
} // EfsDirectoryExists

//------------------------------------------------------------------------------
//  File exists
//------------------------------------------------------------------------------

TYesNo EfsFileExists( const char *Name)
// Check for file <Name>
{
   Command.Command = EFS_CMD_FILE_EXISTS;
   strcpy( Command.Data.FileExists.Name, Name);
   if(!CommandExecute( EfsNameCommandSize(Command.Data.FileExists), EfsExistsReplySize()))
   {
      return NO;
   }
   return Reply.Data.FileExists.Exists;
} // EfsFileExists

//------------------------------------------------------------------------------
//  Directory rename
//------------------------------------------------------------------------------

TYesNo EfsDirectoryRename( const char *Path, const char *NewName)
// Renames selected directory
{
   Command.Command = EFS_CMD_DIRECTORY_RENAME;
   strcpy( Command.Data.DirectoryRename.Path, Path);

   CommandStart();

   if(!SpiCrcPacketSend( &Command, EfsPathCommandSize( Command.Data.DirectoryRename))) {
      CommandStop();
      return NO;
   }

   if(!SpiCrcPacketReceive( &Reply, EfsSimpleReplySize())) {
      CommandStop();
      return NO;
   }

   if(Reply.Reply != EfsReply( Command.Command)) {
      CommandStop();
      return NO;
   }

   Command.Command = EFS_CMD_DIRECTORY_RENAME;
   strcpy( Command.Data.DirectoryRename.Path, NewName);

   if(!SpiCrcPacketSend( &Command, EfsPathCommandSize( Command.Data.DirectoryRename))) {
      CommandStop();
      return NO;
   }

   if(!SpiCrcPacketReceive( &Reply, EfsSimpleReplySize())) {
      CommandStop();
      return NO;
   }

   if(Reply.Reply != EfsReply( Command.Command)) {
      CommandStop();
      return NO;
   }

   CommandStop();
   return YES;
} // EfsDirectoryRename

//*****************************************************************************

//------------------------------------------------------------------------------
//  Command execute
//------------------------------------------------------------------------------

static TYesNo CommandExecute( word Size, word ReplySize)
// Execute command
{
   CommandStart();

   if(!SpiCrcPacketSend( &Command, Size)) {
      CommandStop();
      return NO;
   }

   if(!SpiCrcPacketReceive( &Reply, sizeof(Reply))) {
      CommandStop();
      return NO;
   }

   if(SpiCrcReceiveSize() != ReplySize) {
      CommandStop();
      return NO;
   }
   
   if(Reply.Reply != EfsReply( Command.Command)) {
      CommandStop();
      return NO;
   }
   
   CommandStop();
   return YES;
} // CommandExecute

