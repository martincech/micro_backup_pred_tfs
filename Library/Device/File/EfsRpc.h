//******************************************************************************
//
//   EfsRpc.h       External file system remote procedure call
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __EfsRpc_H__
   #define __EfsRpc_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Efs_H__
   #include "File/Efs.h"
#endif

#include <string.h> // strlen

#define EFS_DATA_CHUNK_SIZE   256

//------------------------------------------------------------------------------
//  Command code
//------------------------------------------------------------------------------

typedef enum {
   EFS_CMD_INIT,
   EFS_CMD_DIRECTORY_EXISTS,
   EFS_CMD_DIRECTORY_CREATE,
   EFS_CMD_DIRECTORY_CHANGE,
   EFS_CMD_DIRECTORY_DELETE,
   EFS_CMD_DIRECTORY_RENAME,
   EFS_CMD_FILE_EXISTS,
   EFS_CMD_FILE_CREATE,
   EFS_CMD_FILE_OPEN,
   EFS_CMD_FILE_DELETE,
   EFS_CMD_FILE_CLOSE,
   EFS_CMD_FILE_WRITE,
   EFS_CMD_FILE_READ,
   EFS_CMD_FILE_SEEK,
   EFS_CMD_CLOCK_SET,
   EFS_CMD_UNMOUNT,
   _EFS_CMD_LAST
} EEfsCommand;   

//------------------------------------------------------------------------------
//  Command data
//------------------------------------------------------------------------------

typedef struct {
   char Path[EFS_PATH_MAX + 1];
} TEfsDirectoryExists;

typedef struct {
   char Path[EFS_PATH_MAX + 1];
} TEfsDirectoryCreate;

typedef struct {
   char Path[EFS_PATH_MAX + 1];
} TEfsDirectoryChange;

typedef struct {
   char Path[EFS_PATH_MAX + 1];
} TEfsDirectoryDelete;

typedef struct {
   union {
      char Path[EFS_PATH_MAX + 1];
      char NewName[EFS_NAME_MAX + 1];
   };
} TEfsDirectoryRename;

typedef struct {
   char Name[EFS_NAME_MAX + 1];
} TEfsFileExists;

typedef struct {
   dword Size;
   char Name[EFS_NAME_MAX + 1];
} TEfsFileCreate;

typedef struct {
   char Name[EFS_NAME_MAX + 1];
} TEfsFileOpen;

typedef struct {
   char Name[EFS_NAME_MAX + 1];
} TEfsFileDelete;

typedef struct {
   dword Count;
} TEfsFileWrite;

typedef struct {
   byte Data[EFS_DATA_CHUNK_SIZE];
} TEfsDataChunk;

typedef struct {
   dword Count;
} TEfsFileRead;

typedef struct {
   word  PieceNumber;
   byte  Repeat;
} TEfsFileReadConfirmation;

typedef struct {
   dword Clock;
} TEfsClockSet;

typedef struct {
   dword Pos;
   byte Whence;
} TEfsFileSeek;

//------------------------------------------------------------------------------
//  Command Data union
//------------------------------------------------------------------------------
   
typedef union {
   TEfsDirectoryExists  DirectoryExists;
   TEfsDirectoryCreate  DirectoryCreate;
   TEfsDirectoryChange  DirectoryChange;
   TEfsDirectoryDelete  DirectoryDelete;
   TEfsDirectoryRename  DirectoryRename;
   TEfsFileExists       FileExists;
   TEfsFileCreate       FileCreate;
   TEfsFileOpen         FileOpen;
   TEfsFileDelete       FileDelete;
   TEfsFileWrite        FileWrite;
   TEfsFileRead         FileRead;
   TEfsFileReadConfirmation FileReadConfirmation;
   TEfsFileSeek         FileSeek;
   TEfsDataChunk        DataChunk;
   TEfsClockSet         ClockSet;
} __packed TEfsCommandUnion;

//------------------------------------------------------------------------------
//  Command
//------------------------------------------------------------------------------

typedef struct {
   byte              Command;
   TEfsCommandUnion  Data;
} __packed TEfsCommand;

#define EfsSimpleCommandSize()                        (1)
#define EfsCommandSize( Item)                         (EfsSimpleCommandSize() + sizeof( TEfs##Item))
#define EfsNameCommandSize( NameCommand)              (EfsSimpleCommandSize() + sizeof(NameCommand) - EFS_NAME_MAX + strlen(NameCommand.Name))
#define EfsPathCommandSize( PathCommand)              (EfsSimpleCommandSize() + sizeof(PathCommand) - EFS_PATH_MAX + strlen(PathCommand.Path))

//******************************************************************************

//------------------------------------------------------------------------------
//  Reply code
//------------------------------------------------------------------------------

// almost same as command :
#define EfsReply( Command)   ((Command) | 0x80)

//------------------------------------------------------------------------------
//  Reply data
//------------------------------------------------------------------------------

typedef struct {
   word  PieceNumber;
   byte  Repeat;
} TEfsFileWriteConfirmation;

typedef struct {
   TYesNo Exists;
} TEfsDirectoryExistsReply;

typedef struct {
   TYesNo Exists;
} TEfsFileExistsReply;

//------------------------------------------------------------------------------
//  Reply Data union
//------------------------------------------------------------------------------

typedef union {
   TEfsFileWriteConfirmation  FileWriteConfirmation;
   TEfsDataChunk              DataChunk;
   TEfsDirectoryExistsReply   DirectoryExists;
   TEfsFileExistsReply        FileExists;
} __packed TEfsReplyUnion;

//------------------------------------------------------------------------------
//  Reply 
//------------------------------------------------------------------------------

typedef struct {
   byte            Reply;
   TEfsReplyUnion  Data;
} __packed TEfsReply;

#define EfsSimpleReplySize()                 (1)
#define EfsExistsReplySize()                 (1 + sizeof(TEfsDirectoryExistsReply))
#define EfsReplySize( Item)                  (EfsSimpleReplySize() + sizeof( TEfs##Item))

#endif
