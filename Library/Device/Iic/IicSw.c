//-----------------------------------------------------------------------------
//
//    IicSw.c      Programmed I2C bus services
//    Version 1.0  (c) VEIT Electronics
//
//-----------------------------------------------------------------------------

#include "Iic.h"
#include "System/System.h"   // SysUDelay
#include "Hardware.h"

#ifdef Iic1PortInit
   #define IIC1_ENABLE
#endif

#ifdef IIC_STRONG_PULLUP
   #define SdaInput()    IicSdaInput()
   #define SdaOutput()   IicSdaOutput()
   #define Sda1Input()   Iic1SdaInput()
   #define Sda1Output()  Iic1SdaOutput()
#else
   #define SdaInput()
   #define SdaOutput()
   #define Sda1Input()
   #define Sda1Output()
#endif

// I2C halfclock delay :
#if defined(IIC_WAIT) && IIC_WAIT != 0
   #define IicWait()   SysUDelay( IIC_WAIT)
#else
   #define IicWait()
#endif

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void IicInit( TIicAddress Iic)
// Bus initialization
{
   switch( Iic) {
#ifdef IIC1_ENABLE
      case IIC_IIC1:
         Iic1PortInit();
         Iic1SdaSet();                        // bus idle
         Iic1SclSet();
         Sda1Output();                        // connect pullup side driver
         break;
#endif
      default:
         IicPortInit();
         IicSdaSet();                        // bus idle
         IicSclSet();
         SdaOutput();                        // connect pullup side driver
         break;
   }
} // IicInit

//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void IicStart( TIicAddress Iic)
// Generates start bus sequence
{
   switch( Iic) {
#ifdef IIC1_ENABLE
      case IIC_IIC1:
         Iic1SdaSet();                        // bus idle
         Iic1SclSet();
         IicWait();                          // setup before startbit
         Iic1SdaClr();                        // start
         IicWait();                          // clock setup
         Iic1SclClr();                        // bus attack SCL=0, SDA=0
         break;
#endif
      default:
         IicSdaSet();                        // bus idle
         IicSclSet();
         IicWait();                          // setup before startbit
         IicSdaClr();                        // start
         IicWait();                          // clock setup
         IicSclClr();                        // bus attack SCL=0, SDA=0
         break;
   }
} // IicStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void IicStop( TIicAddress Iic)
// Generates stop bus sequence
{
   switch( Iic) {
#ifdef IIC1_ENABLE
      case IIC_IIC1:
         Iic1SdaClr();
         Iic1SclSet();
         IicWait();                          // delay before stopbit
         Iic1SdaSet();                        // stopbit bus idle SCL=1 SDA=1
         break;
#endif
      default:
         IicSdaClr();
         IicSclSet();
         IicWait();                          // delay before stopbit
         IicSdaSet();                        // stopbit bus idle SCL=1 SDA=1
         break;
   }
} // IicStop

//-----------------------------------------------------------------------------
// Send
//-----------------------------------------------------------------------------

TYesNo IicSend( TIicAddress Iic, byte Value)
// Send <Value>, returns YES on ACK
{
int    i;
TYesNo Ack;

   switch( Iic) {
#ifdef IIC1_ENABLE
      case IIC_IIC1:
         i = 8;
         do {
            Iic1SdaClr();
            if( Value & 0x80){
               Iic1SdaSet();
            }
            IicWait();
            Iic1SclSet();
            IicWait();
            Iic1SclClr();
            Value <<= 1;
         } while( --i);
         // receive ACK/NAK :
         Sda1Input();                         // disconnect pullup side driver
         Iic1SdaSet();                        // data to pullup
         IicWait();
         Iic1SclSet();
         IicWait();
         Ack = !Iic1Sda();                    // positive ACK == 0
         Iic1SclClr();
         Sda1Output();                        // connect pullup side driver
         IicWait();                          // finalize clock
         return( Ack);
#endif
      default:
         i = 8;
         do {
            IicSdaClr();
            if( Value & 0x80){
               IicSdaSet();
            }
            IicWait();
            IicSclSet();
            IicWait();
            IicSclClr();
            Value <<= 1;
         } while( --i);
         // receive ACK/NAK :
         SdaInput();                         // disconnect pullup side driver
         IicSdaSet();                        // data to pullup
         IicWait();
         IicSclSet();
         IicWait();
         Ack = !IicSda();                    // positive ACK == 0
         IicSclClr();
         SdaOutput();                        // connect pullup side driver
         IicWait();                          // finalize clock
         return( Ack);
   }
} // IicSend

//-----------------------------------------------------------------------------
// Receive
//-----------------------------------------------------------------------------

byte IicReceive( TIicAddress Iic, TYesNo Ack)
// Receives and returns byte. Confirms with <Ack>
{
byte Value;
int i;

   switch( Iic) {
#ifdef IIC1_ENABLE
      case IIC_IIC1:
         Value  = 0;
         Sda1Input();                         // disconnect pullup side driver
         Iic1SdaSet();                        // data to pullup
         i = 8;
         do {
            Value <<= 1;
            IicWait();
            Iic1SclSet();
            IicWait();
            if( Iic1Sda()){
               Value++;
            }
            Iic1SclClr();
         } while( --i);
         Sda1Output();                        // connect pullup side driver
         // send ACK/NAK :
         if( Ack){
            Iic1SdaClr();                     // positive ACK == 0
         } // else IicSetSDA() already done
         IicWait();
         Iic1SclSet();
         IicWait();
         Iic1SclClr();
         IicWait();                          // finalize clock
         return( Value);
#endif
      default:
         Value  = 0;
         SdaInput();                         // disconnect pullup side driver
         IicSdaSet();                        // data to pullup
         i = 8;
         do {
            Value <<= 1;
            IicWait();
            IicSclSet();
            IicWait();
            if( IicSda()){
               Value++;
            }
            IicSclClr();
         } while( --i);
         SdaOutput();                        // connect pullup side driver
         // send ACK/NAK :
         if( Ack){
            IicSdaClr();                     // positive ACK == 0
         } // else IicSetSDA() already done
         IicWait();
         IicSclSet();
         IicWait();
         IicSclClr();
         IicWait();                          // finalize clock
         return( Value);
   }
} // IicReceive

#ifdef IIC_WRITE
//------------------------------------------------------------------------------
//  Read
//------------------------------------------------------------------------------

TYesNo IicWrite( TIicAddress Iic, void *Buffer, int Size)
// Sends <Size> bytes from <Buffer>
{
int   i;
byte *p;

   p = (byte *)Buffer;
   for( i = 0; i < Size; i++){
#ifndef IIC_WRITE_NOACK	   
      if( !IicSend( Iic, *p)){
         return( NO);                  // invalid ACK response
      }
#else	  
      IicSend( Iic, *p);                    // don't check for ACK
#endif	  
      p++;
   }
   return( YES);
} // IicWrite
#endif // IIC_WRITE

#ifdef IIC_READ
//------------------------------------------------------------------------------
//  Read
//------------------------------------------------------------------------------

void IicRead( TIicAddress Iic, void *Buffer, int Size)
// Receives of <Size> bytes into <Buffer>
{
int  i;
byte *p;

   p = (byte *)Buffer;
   for( i = 0; i < Size; i++){
      *p = IicReceive( Iic, i != Size - 1);
      p++;
   }
} // IicRead
#endif // IIC_READ
