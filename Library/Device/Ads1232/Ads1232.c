//*****************************************************************************
//
//    Ads1232.c    A/D convertor
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Ads1232.h"
#include "System/System.h"
#include "Cpu/Cpu.h"
#include "Filter/FilterRelative.h"

#define ADC_CALIBRATION_CLOCK  26

// Parametric configuration values :
static TYesNo Chop;
static TYesNo Sinc3;
static TYesNo Running;

//-----------------------------------------------------------------------------
// Internal variables
//-----------------------------------------------------------------------------

static TAdcValue _AdcRawValue;
static word      _AdcCount;
static int64     _AdcSum;
static TYesNo    _CallBackEnable;

//-----------------------------------------------------------------------------
// Local functions :
//-----------------------------------------------------------------------------

static dword ReadWord( void);
// Read 24bit data from ADC

AdcInterruptHandler();
// Adc interrupt

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void AdcInit( void)
// Initialisation
{
   AdcPortInit();
   // Defaults :
   AdcIdle();                           // stop conversion
   AdcClrSCLK();
   // prepare default values :
   Chop    = NO;
   Sinc3   = NO;
   // prepare interrupt :
   AdcDisableInt();              // disable ADC external interrupt
   AdcInitInt();
   Running = NO;                        // stopped
   _AdcSum   = 0;
   _AdcCount = 0;
   FilterStop();                        // default filter status
} // AdcInit

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

void AdcReset( void)
// Reset communication
{
   AdcIdle();
} // AdcReset

//-----------------------------------------------------------------------------
// Data rate
//-----------------------------------------------------------------------------

void AdcDataRateSet( word Hertz)
// Set conversion data rate
{
} // AdcDataRateSet

//-----------------------------------------------------------------------------
// Enable SINC3
//-----------------------------------------------------------------------------

void AdcSinc3Set( TYesNo Enable)
// <Enable> SINC3 filtering
{
   Sinc3 = Enable;
} // AdcSinc3Set

//-----------------------------------------------------------------------------
// Enable CHOP
//-----------------------------------------------------------------------------

void AdcChopSet( TYesNo Enable)
// <Enable> CHOP filtering
{
   Chop = Enable;
} // AdcChopSet

//-----------------------------------------------------------------------------
// Idle
//-----------------------------------------------------------------------------

void AdcIdle( void)
// Set idle mode
{
   AdcPowerDown();
} // AdcIdle

//-----------------------------------------------------------------------------
// Power down
//-----------------------------------------------------------------------------

void AdcPowerDown( void)
// Set power down mode
{
   AdcDisableInt();
   AdcPowerDownEnable();
   PowerAdcOff();
} // AdcPoweDown

//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void AdcStart( void)
// Start continuous conversion
{
   AdcIdle();                                         // stop conversion
   PowerAdcEn();
   // Configuration register :  
   AdcPowerDownDisable();
   while(!AdcGetRDY());
   byte i = 26;
   do {
      AdcSetSCLK();
      Nop();
      AdcClrSCLK();   
   } while( --i);
   Running = YES;                                     // conversions started
   FilterStart();                                     // start filtering
   AdcClearInt();                                     // clear old interupt flag
   AdcEnableInt();                                    // enable external interrupt
} // AdcStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void AdcStop( void)
// Stop continous conversion
{
   AdcDisableInt();
   AdcIdle();                                         // stop conversion
   FilterStop();                                      // stop filtering
   Running = NO;                                      // conversions stopped
} // AdcStop

//-----------------------------------------------------------------------------
// Raw read
//-----------------------------------------------------------------------------

TAdcValue AdcRawRead( void)
// Read actual conversion
{
TAdcValue Value;

   AdcDisableInt();                    // disable external interrupt
   Value = _AdcRawValue;               // get value
   AdcEnableInt();                     // enable external interrupt
   return( Value);
} // AdcRawRead

//-----------------------------------------------------------------------------
// Average read
//-----------------------------------------------------------------------------

TAdcValue AdcAverageRead( void)
// Read averaged value
{
TAdcValue Average;
int64     Sum;
word      Count;

   AdcDisableInt();                    // disable external interrupt
   Sum       = _AdcSum;                // remember value
   Count     = _AdcCount;              // remember value
   _AdcSum   = 0;                      // start new sum
   _AdcCount = 0;                      // start new count
   AdcEnableInt();                     // enable external interrupt
   if( Count == 0){
      return( 0);
   }
   Average = (TAdcValue)(Sum / Count);
   return( Average);
} // AdcAverageRead

//-----------------------------------------------------------------------------
// Low pass read
//-----------------------------------------------------------------------------

TAdcValue AdcLowPassRead( void)
// Read filtered conversion
{
TRawWeight Value;

   AdcDisableInt();                    // disable external interrupt
   Value = FilterRecord.LowPass;       // low pass value
   if( Running){
      AdcEnableInt();
   }
   return( Value);
} // AdcLowPassRead

//-----------------------------------------------------------------------------
// Stable value read
//-----------------------------------------------------------------------------

TYesNo AdcRead( TAdcValue *Weight)
// Read stable value
{
TYesNo Result;

   AdcDisableInt();                    // disable external interrupt
   Result = FilterRead( Weight);
   if( Running){
      AdcEnableInt();
   }
   return( Result);
} // AdcRead

//-----------------------------------------------------------------------------
// Callback enable
//-----------------------------------------------------------------------------

void AdcCallbackEnable( TYesNo Enable)
// Enables/disables ADC callback
{
   _CallBackEnable = Enable;
} // AdcCallBackEnable

//-----------------------------------------------------------------------------
// Read word
//-----------------------------------------------------------------------------

static dword ReadWord( void)
// Read 24bit data from ADC
{
dword Value;
byte  i;

   Value  = 0;
   i = 24;
   do {
      Value <<= 1;
      AdcSetSCLK();
      Nop();
      AdcClrSCLK();   
      if( AdcGetDOUT()){
         Value |= 1;
      }
   } while( --i);
   return( Value);
} // ReadWord

//-----------------------------------------------------------------------------
// Interrupt handler
//-----------------------------------------------------------------------------

AdcInterruptHandler()
{
   AdcReadSample();
} // ADC_INT_vect

void AdcReadSample() {
   if(GpioGet(ADC_DOUT_RDY)) { // DRDY_DOUT high, false trigger
      return;
   }
   AdcDisableInt();
   _AdcRawValue  = ReadWord();         // get conversion data
   if(_AdcRawValue & 0x800000) {
      _AdcRawValue |= 0xFF000000;
   }
   AdcClearInt();                     // clear flag set by data read edges
   AdcEnableInt();
   _AdcSum      += _AdcRawValue;       // averaging sum
   _AdcCount++;                        // averaging count
   FilterNextSample( _AdcRawValue);    // filtering

   if(_CallBackEnable) {
      AdcCallback();
   }
}