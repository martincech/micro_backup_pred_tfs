//*****************************************************************************
//
//    Ad7192f.h    A/D convertor AD7192 services 
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Ad7192f_H__
   #define __Ad7192f_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef int32 TAdcValue;

#ifdef __cplusplus
   extern "C" {
#endif

void AdcInit( void);
// Inicializace prevodniku

void AdcReset( void);
// Reset communication

void AdcDataRateSet( word Hertz);
// Set <Hertz> conversion rate

void AdcSinc3Set( TYesNo Enable);
// <Enable> SINC3 filtering

void AdcChopSet( TYesNo Enable);
// <Enable> CHOP filtering

void AdcIdle( void);
// Set idle mode

void AdcPowerDown( void);
// Set power down mode

void AdcStart( void);
// Start continuous conversion

void AdcStop( void);
// Stop continous conversion

TAdcValue AdcRawRead( void);
// Read actual conversion

TAdcValue AdcAverageRead( void);
// Read averaged value

TAdcValue AdcLowPassRead( void);
// Read filtered conversion

TYesNo AdcRead( TAdcValue *Weight);
// Read stable value

void AdcReadSample( void);
// Performs reading a sample from device. Do not use!

//------------------------------------------------------------------------------
// Callback
//------------------------------------------------------------------------------

void AdcCallback( void);
// User implemented callback function

void AdcCallbackEnable( TYesNo Enable);
// Enables/disables ADC callback

#ifdef __cplusplus
   }
#endif

#endif
