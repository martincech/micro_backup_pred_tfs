//*****************************************************************************
//
//    Eint.h       External interrupt services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Eint_H__
   #define __Eint_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// External interrupt sense :
#define EINT_SENSE_LOW_LEVEL 
#define EINT_SENSE_HIGH_LEVEL
#define EINT_SENSE_ANY_LEVEL

#define EINT_SENSE_FALLING_EDGE
#define EINT_SENSE_RISING_EDGE
#define EINT_SENSE_ANY_EDGE

// External interrupt commands :
#define EintDisableAll()
#define EintDisable( n) 
#define EintEnable( n)
#define EintSense( n, Sense)
#define EintClearFlag( n)
#define EintGetFlag( n)

static void EintSetup( void);
// Nastaveni externiho preruseni

static void EintInterruptEnable( void);
// Enable external interrupt

static void EintInterruptDisable( void);
// Disable external interrupt

#endif
