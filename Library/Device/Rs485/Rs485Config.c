//******************************************************************************
//
//   Rs485Config.c     RS485 module configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "Rs485Config.h"
#include "Megavi/MegaviTask.h"

TRs485Options Rs485Options[RS485_INTERFACE_COUNT];

const TRs485Options Rs485OptionsDefault[] = {
   { // Interface 1
     /* Enabled */                    YES,
     /* Mode */                       RS485_MODE_SENSOR_PACK,
   },
   { // Interface 2
     /* Enabled */                    NO,
     /* Mode */                       RS485_MODE_MEGAVI,
   },
};
