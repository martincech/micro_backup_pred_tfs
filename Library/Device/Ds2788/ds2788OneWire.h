//*****************************************************************************
//
//   ds2788OneWire.h     Ds2788 One Wire
//   Version 1.0         (c) Veit Electronics
//
//*****************************************************************************

#ifndef __ds2788OneWire_H__
   #define __ds2788OneWire_H__

#include "OneWire/OneWire.h"
#include "Hardware.h"

#define ds2788OneWireInit()       OneWireInit()
// Initialization

#define ds2788OneWireReset()      OneWireReset(DS2788_ONE_WIRE_CHANNEL)
// Resets bus, returns YES if device present

#define ds2788OneWireByteWrite( Value)  OneWireByteWrite(DS2788_ONE_WIRE_CHANNEL, Value)
// Write byte

#define ds2788OneWireByteRead()   OneWireByteRead(DS2788_ONE_WIRE_CHANNEL)
// read byte

#endif
