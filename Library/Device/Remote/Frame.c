//*****************************************************************************
//
//    Frame.c           Frame
//    Version 1.0       (c) Veit Electronics
//
//*****************************************************************************

#include "Frame.h"

#define SOF         0x55

#ifdef __WIN32__
   #pragma pack( push, 1)                   // byte alignment
#endif

typedef enum {
   INDEX_HEADER,
   INDEX_DATA,
   INDEX_CRC0,
   INDEX_CRC1,
   INDEX_END
} EIndex;

#define FrameHeaderCrc( FramePtr)   (-(((FramePtr)->Length & 0xFF) + (((FramePtr)->Length >> 8) & 0xFF)))
#define FRAME_HEADER_SIZE          offsetof(TFrame, Data)
#define FRAME_DATA_CRC_SIZE        sizeof(TDataCrc)

#define InitDataCrc(Crc)           (Crc = 0)
#define ProccessDataCrc(Crc, Ch)   (Crc ^= ((byte)(Ch)))
#define FinishDataCrc(Crc)         (Crc = -Crc)

#define _timerSet( Time)  State->Timer = Time

typedef struct {
   byte Sof;
   word Length;
   byte HeaderCrc;
   byte Data[1];
   TDataCrc DataCrc; // aligned to word boundary
} __packed TFrame;

#ifdef __WIN32__
   #pragma pack( pop)                       // original alignment
#endif

//******************************************************************************
// Init
//******************************************************************************

void FrameInit( TFrameState *State)
// Init frame processing
{
   State->Status = FRAME_STATE_IDLE;
   State->Timer = 0;
} // FrameInit

//******************************************************************************
// Timeout set
//******************************************************************************

void FrameTimeoutSet( TFrameState *State, int RxTimeout, int RxReplyTimeout)
// Timeout set
{
   State->RxTimeout = RxTimeout;
   State->RxReplyTimeout = RxReplyTimeout;
} // FrameTimeoutSet

//******************************************************************************
// Send init
//******************************************************************************

TYesNo FrameSendInit( TFrameState *State, const void *Buffer, word Size)
// Init send processing
{
   State->SizeTotal = Size;
   State->Index = INDEX_HEADER;
   State->SendBuffer = (byte *) Buffer;
   State->Status = FRAME_STATE_SEND_ACTIVE;
   State->SizeProccessed = 0;

   return YES;
} // FrameSendInit

//******************************************************************************
// Receive init
//******************************************************************************

TYesNo FrameReceiveStart( TFrameState *State, void *Buffer, word Size)
// Init receive processing
{
   State->SizeTotal = FRAME_HEADER_SIZE;
   State->Index = INDEX_HEADER;
   State->Status = FRAME_STATE_RECEIVE_ACTIVE;
   State->ReceiveBufferSize = Size;
   State->ReceiveBuffer = (byte *) Buffer;
   State->SizeProccessed = 0;

   _timerSet(State->RxReplyTimeout);
   return YES;
} // FrameReceiveStart

//******************************************************************************
// Send
//******************************************************************************

TYesNo FrameSendProccess( TFrameState *State, const void *ChunkBuffer, int ChunkBufferSize, int *ChunkSize)
// Send process
{
int i;
TFrame *Frame;
word SizeRemaining;

   byte *Buffer = (byte *) ChunkBuffer;
   *ChunkSize = 0;

   switch(State->Index) {
      case INDEX_HEADER:
         if(ChunkBufferSize < FRAME_HEADER_SIZE){
            return NO;
         }
         Frame = (TFrame *) Buffer;

         Frame->Sof = SOF;
         Frame->Length = State->SizeTotal;
         Frame->HeaderCrc = FrameHeaderCrc( Frame);

         Buffer += FRAME_HEADER_SIZE;
         ChunkBufferSize -= FRAME_HEADER_SIZE;
         (*ChunkSize) += FRAME_HEADER_SIZE;
         InitDataCrc( State->DataCrc);
         State->Index = INDEX_DATA;

      case INDEX_DATA:
         SizeRemaining = State->SizeTotal - State->SizeProccessed;
         if(SizeRemaining > ChunkBufferSize) {
            SizeRemaining = ChunkBufferSize;
         }
         for(i = 0 ; i < SizeRemaining ; i++) {
            byte Ch = State->SendBuffer[State->SizeProccessed++];
            *Buffer = Ch;
            ProccessDataCrc( State->DataCrc, Ch);
            Buffer++;
            ChunkBufferSize--;
            (*ChunkSize)++;
         }
         SizeRemaining = State->SizeTotal - State->SizeProccessed;
         if(SizeRemaining != 0) {
            break;
         }
         FinishDataCrc( State->DataCrc);
         State->Index = INDEX_CRC0;

      case INDEX_CRC0:
         if(!ChunkBufferSize) {
            break;
         }
         *Buffer = State->DataCrc >> 8;
         Buffer++;
         ChunkBufferSize--;
         (*ChunkSize)++;
         State->Index = INDEX_CRC1;

      case INDEX_CRC1:
         if(!ChunkBufferSize) {
            break;
         }
         *Buffer = State->DataCrc & 0xFF;
         Buffer++;
         ChunkBufferSize--;
         (*ChunkSize)++;
         State->Index = INDEX_END;

      default:
         State->Status = FRAME_STATE_SEND_DONE;
         _timerSet(State->RxTimeout);
         break;
   }

   return YES;
} // FrameSendProccess

//******************************************************************************
// Receive
//******************************************************************************

int FrameReceiveProccess( TFrameState *State, void *ChunkBuffer, int ChunkBufferSize)
// Receive process
{
int i;
TFrame *Frame;
byte HeaderCrc;
int SizeRemaining;
int ChunkBufferSizeRemaining = ChunkBufferSize;

   byte *Buffer = (byte *) ChunkBuffer;

   State->Status = FRAME_STATE_RECEIVE_ACTIVE;
   switch(State->Index) {
      case INDEX_HEADER:
         if(ChunkBufferSizeRemaining < FRAME_HEADER_SIZE){
            State->Status = FRAME_STATE_RECEIVE_WAITING_HEADER;
            return 0;
         }
         Frame = (TFrame *) Buffer;
         if(Frame->Sof != SOF) {
            State->Status = FRAME_STATE_RECEIVE_HEADER_ERROR;
            return offsetof(TFrame, Sof) + sizeof(Frame->Sof);
         }
         HeaderCrc = FrameHeaderCrc(Frame);
         if(Frame->HeaderCrc != HeaderCrc) {
            State->Status = FRAME_STATE_RECEIVE_HEADER_CRC_ERROR;
            return FRAME_HEADER_SIZE;
         }

         if(Frame->Length > State->ReceiveBufferSize) {
            State->Status = FRAME_STATE_RECEIVE_SIZE_ERROR;
            return FRAME_HEADER_SIZE;
         }
         State->SizeTotal = Frame->Length;
         Buffer += FRAME_HEADER_SIZE;
         ChunkBufferSizeRemaining -= FRAME_HEADER_SIZE;
         InitDataCrc( State->DataCrc);
         State->SizeProccessed = 0;
         State->Index = INDEX_DATA;

      case INDEX_DATA:
         SizeRemaining = State->SizeTotal - State->SizeProccessed;
         if(SizeRemaining > ChunkBufferSizeRemaining) {
            SizeRemaining = ChunkBufferSizeRemaining;
         }
         for(i = 0 ; i < SizeRemaining ; i++) {
            byte Ch = *Buffer;
            State->ReceiveBuffer[State->SizeProccessed++] = Ch;
            ProccessDataCrc( State->DataCrc, Ch);
            Buffer++;
            ChunkBufferSizeRemaining--;
         }
         SizeRemaining = State->SizeTotal - State->SizeProccessed;
         if(SizeRemaining != 0) {
            break;
         }
         FinishDataCrc( State->DataCrc);
         State->Index = INDEX_CRC0;

      case INDEX_CRC0:
         if( ChunkBufferSizeRemaining < 1){
            break;
         }
         if((State->DataCrc >> 8) != *Buffer) {
            State->Status = FRAME_STATE_RECEIVE_CRC_ERROR;
            return ChunkBufferSize - ChunkBufferSizeRemaining;
         }
         Buffer++;
         ChunkBufferSizeRemaining--;
         State->Index = INDEX_CRC1;

      case INDEX_CRC1:
         if( ChunkBufferSizeRemaining < 1){
            break;
         }
         if((State->DataCrc & 0xFF) != *Buffer) {
            State->Status = FRAME_STATE_RECEIVE_CRC_ERROR;
            return ChunkBufferSize - ChunkBufferSizeRemaining;
         }
         Buffer++;
         ChunkBufferSizeRemaining--;
         State->Index = INDEX_END;

      case INDEX_END:
         State->Status = FRAME_STATE_RECEIVE_FRAME;
         break;
   }

   _timerSet(State->RxTimeout);
   
   return ChunkBufferSize - ChunkBufferSizeRemaining;
} // FrameReceiveProccess

//******************************************************************************
// Timer
//******************************************************************************

void FrameTimer( TFrameState *State)
// Timer, call periodically
{
   if(!State->Timer) {
      return;
   }
   if( --State->Timer) {
      return;
   }

   switch(State->Status) {
      case FRAME_STATE_RECEIVE_ACTIVE :
      case FRAME_STATE_RECEIVE_WAITING_HEADER :
         if(State->SizeProccessed == 0) {
            State->Status = FRAME_STATE_RECEIVE_REPLY_TIMEOUT;
         } else {
            State->Status = FRAME_STATE_RECEIVE_TIMEOUT;
         }
         break;

      default:
         break;
   }
} // FrameTimer
