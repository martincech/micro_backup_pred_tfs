//*****************************************************************************
//
//    Socket.c           Socket
//    Version 1.0        (c) VEIT Electronics
//
//*****************************************************************************

#include "Socket.h"
#include "Remote/SocketDef.c"


TYesNo SocketListen( TSocket *Socket) {
int Iface;
   for(Iface = 0 ; Iface < SOCKET_INTERFACE_COUNT ; Iface++) {
      if(Iface == SOCKET_INTERFACE_MSD){
         continue;
      }
      if( Interface[Iface].Listen( Socket)) {
         Socket->Interface = Iface;
         return YES;
      }
   }
   return NO;
}


//------------------------------------------------------------------------------
//  Permission
//------------------------------------------------------------------------------

byte SocketPermission( TSocket *Socket)
// Permission
{
   return Interface[Socket->Interface].Permission(Socket);
} // SocketIfMsdPermission

//------------------------------------------------------------------------------
//  State
//------------------------------------------------------------------------------

byte SocketState( TSocket *Socket)
// Gets state of socket
{
   return Interface[Socket->Interface].State(Socket);
} // SocketState

//------------------------------------------------------------------------------
//  Receive
//------------------------------------------------------------------------------

TYesNo SocketReceive( TSocket *Socket, void *Buffer, int Size)
// Receive into <Buffer> with <Size>
{
   return Interface[Socket->Interface].Receive(Socket, Buffer, Size);
} // SocketReceive

//------------------------------------------------------------------------------
//  Receive size
//------------------------------------------------------------------------------

int SocketReceiveSize( TSocket *Socket)
// Gets number of received bytes
{
   return Interface[Socket->Interface].ReceiveSize(Socket);
} // SocketReceiveSize

//------------------------------------------------------------------------------
//  Send
//------------------------------------------------------------------------------

TYesNo SocketSend( TSocket *Socket, const void *Buffer, int Size)
// Send <Buffer> with <Size>
{
   return Interface[Socket->Interface].Send(Socket, Buffer, Size);
} // SocketSend

//------------------------------------------------------------------------------
//  Interface ID
//------------------------------------------------------------------------------

byte SocketInterfaceId( TSocket *Socket)
// Get interface id
{
   return Socket->Interface;
} // SocketInterfaceId

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void SocketClose( TSocket *Socket)
// Close socket
{
   return Interface[Socket->Interface].Close(Socket);
} // SocketClose