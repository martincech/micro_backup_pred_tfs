//*****************************************************************************
//
//   DataFlash.h    Atmel DataFlash memory access
//   Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __DataFlash_H__
   #define __DataFlash_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FlashSpi_H__
   #include "Flash/FlashSpi.h"
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void FlashInit();
// Initialisation

byte FlashStatus( void);
// Read status byte

TYesNo FlashWaitForReady( void);
// Wait for ready

//------- Common read functions (buffer/flash array) :

#define FlashReadData()                spiFlashByteRead()
// Read current address data

#define FlashReadStop()                spiFlashRelease()
// Terminate data read

//-----------------------------------------------------------------------------
// Block
//-----------------------------------------------------------------------------

void FlashBlockReadStart( word Page, word Offset);
// Start sequential read from flash array

#define FlashBlockReadData()           FlashReadData()
// Read flash array data

#define FlashBlockReadStop()           FlashReadStop()
// Terminate flash array read

//-----------------------------------------------------------------------------
// Buffer
//-----------------------------------------------------------------------------

void FlashBufferSave( word Page);
// Write buffer to flash

void FlashBufferLoad( word Page);
// Read buffer from flash

//-----------------------------------------------------------------------------

void FlashBufferWriteStart( word Offset);
// Start sequential write

#define FlashBufferWriteData( Value)   spiFlashByteWrite( Value)
// Write buffer data

#define FlashBufferWriteStop()         spiFlashRelease()
// Write done

//-----------------------------------------------------------------------------

void FlashBufferReadStart( word Offset);
// Start sequential read from buffer

#define FlashBufferReadData()          FlashReadData()
// Read buffer data

#define FlashBufferReadStop()          FlashReadStop()
// Terminate buffer read

//-----------------------------------------------------------------------------
// Tranparent data access
//-----------------------------------------------------------------------------

// Warning : user must check for flash buffer operations

TYesNo FlashSave( dword Address, const void *Data, int Size);
// Save <Data> with <Size> at <Address>

void FlashLoad( dword Address, void *Data, int Size);
// Load <Data> with <Size> from <Address>

TYesNo FlashFill( dword Address, byte Pattern, int Size);
// Write <Pattern> with <Size> at <Address>

TYesNo FlashMatch( dword Address, const void *Data, int Size);
// Compare <Data> with <Size> at <Address>

dword FlashSum( dword Address, int Size);
// Sum NVM contents at <Address> with <Size>

#endif
