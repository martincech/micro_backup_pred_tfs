//*****************************************************************************
//
//   NorFlash.h     Atmel NOR Flash memory access
//   Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __NorFlash_H__
   #define __NorFlash_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FlashSpi_H__
   #include "Flash/FlashSpi.h"
#endif

// Device identification :
typedef struct {
   byte Manufacturer;             // manufacturer code
   byte DeviceId1;                // device ID1
   byte DeviceId2;                // device ID2
   byte ExtendedLenght;           // extended device information string length
} TFlashDevice;

#ifdef __cplusplus
   extern "C" {
#endif
//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void FlashInit();
// Initialisation

TYesNo FlashWaitForReady( void);
// Wait for ready

void FlashIdentification( TFlashDevice *Device);
// Read <Device> identification

//-----------------------------------------------------------------------------

void FlashBlockReadStart( dword Address);
// Start sequential read from flash array

#define FlashBlockReadData()           spiFlashByteRead()
// Read flash array data

#define FlashBlockReadStop()           spiFlashRelease()
// Terminate flash array read

//-----------------------------------------------------------------------------

void FlashBlockWriteStart( dword Address);
// Start block write

#define FlashBlockWriteData( Value)    spiFlashByteWrite( Value)
// Write buffer data

#define FlashBlockWritePerform()       spiFlashRelease()
// Write done

//-----------------------------------------------------------------------------

void FlashBlockErase( dword Address);
// Erase block at <Address>

TYesNo FlashEraseAll( void);
// Erase all device

//-----------------------------------------------------------------------------
// Tranparent data access
//-----------------------------------------------------------------------------

// Warning : user must check for page / sector erase

TYesNo FlashSave( dword Address, const void *Data, int Size);
// Save <Data> with <Size> at <Address>

void FlashLoad( dword Address, void *Data, int Size);
// Load <Data> with <Size> from <Address>

TYesNo FlashFill( dword Address, byte Pattern, int Size);
// Write <Pattern> with <Size> at <Address>

TYesNo FlashMatch( dword Address, const void *Data, int Size);
// Compare <Data> with <Size> at <Address>

dword FlashSum( dword Address, int Size);
// Sum NVM contents at <Address> with <Size>

//-----------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
