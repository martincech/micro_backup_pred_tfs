//*****************************************************************************
//
//    Dac.h        Internal D/A convertor functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Dac_H__
   #define __Dac_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void DacInit( void);
// Initialize convertor

void DacValueSet( byte Channel, word Value);
// Set output <Value>

#endif
