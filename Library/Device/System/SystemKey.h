//******************************************************************************
//
//    SystemKey.h  System events
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __SystemKey_H__
   #define __SystemKey_H__

// system events :   
typedef enum {
   K_IDLE = _K_SYSTEM_BASE,
   K_TIMER_FAST,
   K_TIMER_SLOW,
   K_FLASH1,
   K_FLASH2,
   K_TIMEOUT,
   K_POWER_CONNECTED,
   K_POWER_FAILURE,
   K_POWER_OK,
   K_WAKE_UP,
   K_SHUTDOWN,                         // power shutdown
   _K_SYSTEM_LAST
} ESystemKey;
   
#endif
