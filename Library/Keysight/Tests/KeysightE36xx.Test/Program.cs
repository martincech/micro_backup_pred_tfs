﻿using System;
using Keysight;

namespace KeysightE36xxTest
{
   class Program
   {
      static void Main(string[] args)
      {
         var powerSupply = new KeysightE36xx("ASRL10::INSTR");
         var myOutput = powerSupply.Outputs[0];
         while (true)
         {
            myOutput.Current = 0.01;
            myOutput.Voltage = 4;
            powerSupply.Outputs.TurnOn();

            Console.WriteLine("Voltage: " + myOutput.Voltage + ", Current: " + myOutput.Current);
         }
      }
   }
}
