//******************************************************************************
//
//   CommunicationDef.h  Communication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CommunicationDef_H__
   #ifndef _MANAGED
      #define __CommunicationDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#include "fnet_ip.h"
#include "Time/uTime.h"

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------
#ifndef _MANAGED
#define DATA_PUBLICATION_USERNAME_SIZE 16
#define DATA_PUBLICATION_USERNAME_DEFAULT "Pid_123"

#define DATA_PUBLICATION_PASSWORD_SIZE 16
#define DATA_PUBLICATION_PASSWORD_DEFAULT "Cid_123"

#define DATA_PUBLICATION_URL_SIZE 512/2
#ifdef __UDEBUG__
#define DATA_PUBLICATION_URL_DEFAULT "http://188.95.58.102:8801"
#else
#define DATA_PUBLICATION_URL_DEFAULT ""
#endif

#define DATA_PUBLICATION_START_FROM_DAY_MIN 1
#define DATA_PUBLICATION_START_FROM_DAY_MAX 999
#define DATA_PUBLICATION_START_FROM_DAY_DEFAULT 1

#define DATA_PUBLICATION_PERIOD_MIN 1
#define DATA_PUBLICATION_PERIOD_MAX 999
#define DATA_PUBLICATION_PERIOD_DEFAULT 1

#define DATA_PUBLICATION_ACCELERATE_FROM_DAY_MIN 1
#define DATA_PUBLICATION_ACCELERATE_FROM_DAY_MAX 999
#define DATA_PUBLICATION_ACCELERATE_FROM_DAY_DEFAULT 30

#define DATA_PUBLICATION_ACCELERATED_PERIOD_MIN 1
#define DATA_PUBLICATION_ACCELERATED_PERIOD_MAX 999
#define DATA_PUBLICATION_ACCELERATED_PERIOD_DEFAULT 1

#define DATA_PUBLICATION_SEND_AT_MIN 0
#define DATA_PUBLICATION_SEND_AT_MAX 0
#define DATA_PUBLICATION_SEND_AT_DEFAULT 0

#define CELLULAR_DATA_APN_SIZE 63
#define CELLULAR_DATA_APN_DEFAULT ""

#define CELLULAR_DATA_USERNAME_SIZE 15
#define CELLULAR_DATA_USERNAME_DEFAULT ""

#define CELLULAR_DATA_PASSWORD_SIZE 15
#define CELLULAR_DATA_PASSWORD_DEFAULT ""

#define ETHERNET_IP_DEFAULT            0x0201A8C0
#define ETHERNET_SUBNET_MASK_DEFAULT   0x00FFFFFF
#define ETHERNET_GATEWAY_DEFAULT       0x0101A8C0
#define ETHERNET_PRIMARY_DNS_DEFAULT   0x0101A8C0
#define ETHERNET_SECONDARY_DNS_DEFAULT 0
#endif

//------------------------------------------------------------------------------
//  Data publication interface
//------------------------------------------------------------------------------

#ifdef _MANAGED
namespace Bat2Library{
   [Flags]
   public enum class DataPublicationInterfaceE{
#else
typedef enum {
#endif
   DATA_PUBLICATION_INTERFACE_DISABLED = 0x0000,
   DATA_PUBLICATION_INTERFACE_INTERNET = 0x0001,
   DATA_PUBLICATION_INTERFACE_SMS      = 0x0002,
   DATA_PUBLICATION_INTERFACE_BOTH     = DATA_PUBLICATION_INTERFACE_INTERNET | DATA_PUBLICATION_INTERFACE_SMS
#ifndef _MANAGED
   ,
   _DATA_PUBLICATION_INTERFACE_LAST
} EDataPublicationInterface;
#else
};
}
#endif
#ifndef _MANAGED
//------------------------------------------------------------------------------
//  Data publication
//------------------------------------------------------------------------------

typedef struct {
   byte Interface;
   char Username[ DATA_PUBLICATION_USERNAME_SIZE + 1];
   char Password[ DATA_PUBLICATION_PASSWORD_SIZE + 1];
   char Url[ DATA_PUBLICATION_URL_SIZE + 1]; // URL address

   TDayNumber StartFromDay;                 // Start sending from day
   TDayNumber Period;                       // Send with period [days]
   TDayNumber AccelerateFromDay;            // Accelerate sending  from day
   TDayNumber AcceleratedPeriod;            // Accelerated sending period [days]
   UTimeGauge SendAt;                       // Send message at this time
} TDataPublication;

//------------------------------------------------------------------------------
//  Cellular data
//------------------------------------------------------------------------------

typedef struct {
   char Apn[ CELLULAR_DATA_APN_SIZE + 1];
   char Username[ CELLULAR_DATA_USERNAME_SIZE + 1];
   char Password[ CELLULAR_DATA_PASSWORD_SIZE + 1];
} TCellularData;

//------------------------------------------------------------------------------
//  Ethernet
//------------------------------------------------------------------------------

typedef struct {
   TYesNo Dhcp;
   fnet_ip4_addr_t Ip;
   fnet_ip4_addr_t SubnetMask;
   fnet_ip4_addr_t Gateway;
   fnet_ip4_addr_t PrimaryDns;
} TEthernet;

//******************************************************************************

//------------------------------------------------------------------------------
//  Data communication context
//------------------------------------------------------------------------------

#include "Archive/ArchiveDef.h"
#include "Message/GsmMessageDef.h"
#include "SmsGate/SmsGateDef.h"
#define CLOCK_INVALID 0xFFFFFFFF            // invalid clock data

typedef struct {
   // statistics
   UClockGauge   SendTime;                  // Time to send statistics at
   TDayNumber    ClosedDay;                 // Closed day number
   TDayNumber    SendDay;                   // Actual send day number
   TArchiveIndex ClosedIndex;               // Archive marker of closed day
   TArchiveIndex SendIndex;                 // Archive marker of day actualy send
   TContactIndex StatisticContact;          // Contact for actual statistic send
   // commands
   byte          CommandUnsend;
   // events
   TContactIndex EventContact;              // Contact for actual event send
   TLogIndex     LogIndex;                  // index to log for events
   UClockGauge   LogTimestamp;              // last log timestamp
   byte          EventUnsend;
      // common for SMS
   byte          ContactType;               // actual send contact type (EGsmSmsFormat)
   char          Message[ GSM_SMS_SIZE + 1];               // Message to send
   char          PhoneNumber[ GSM_PHONE_NUMBER_SIZE + 1];  // Phone number of the message
   TSmsGateSlotNumber  SendSmsSlot;
   // common for internet
//   TDayNumber    ISendDay;                   // Actual send day number
//   TArchiveIndex ISendIndex;                 // Archive marker of day actualy send
//   dword         IPublishStartTime;          // for timeout of active sending when it is not possible

} TCommunicationContext;

#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "CommunicationDef.h"
   #define _MANAGED

   namespace Bat2Library{
      public ref class CommunicationC abstract sealed{
         static CommunicationC()
         {
            STATISTICS_SEND_AT_DEFAULT = TimeSpan(
               DATA_PUBLICATION_SEND_AT_DEFAULT / 3600 % 24,
               DATA_PUBLICATION_SEND_AT_DEFAULT / 60 % 60,
               DATA_PUBLICATION_SEND_AT_DEFAULT % 60);
         }
     public:
         /// <summary>
         /// Minimum start from day number
         /// </summary>
         literal int STATISTICS_START_FROM_DAY_MIN = DATA_PUBLICATION_START_FROM_DAY_MIN;
         /// <summary>
         /// Maximum start from day number
         /// </summary>
         literal int STATISTICS_START_FROM_DAY_MAX = DATA_PUBLICATION_START_FROM_DAY_MAX;
         literal int STATISTICS_START_FROM_DAY_DEFAULT = DATA_PUBLICATION_START_FROM_DAY_DEFAULT;

         /// <summary>
         /// Period of statistics (in days) -  minimum
         /// </summary>
         literal int STATISTICS_PERIOD_MIN  = DATA_PUBLICATION_PERIOD_MIN;
         /// <summary>
         /// Period of statistics (in days) -  maximum
         /// </summary>
         literal int STATISTICS_PERIOD_MAX = DATA_PUBLICATION_PERIOD_MAX;
         literal int STATISTICS_PERIOD_DEFAULT = DATA_PUBLICATION_PERIOD_DEFAULT;

         /// <summary>
         /// Accelerate from day (in days) - minimum
         /// </summary>
         literal int STATISTICS_ACCELERATE_FROM_DAY_MIN  = DATA_PUBLICATION_ACCELERATE_FROM_DAY_MIN;
         /// <summary>
         /// Accelerate from day (in days) - maximum
         /// </summary>
         literal int STATISTICS_ACCELERATE_FROM_DAY_MAX = DATA_PUBLICATION_ACCELERATE_FROM_DAY_MAX;
         literal int STATISTICS_ACCELERATE_FROM_DAY_DEFAULT = DATA_PUBLICATION_ACCELERATE_FROM_DAY_DEFAULT;

         /// <summary>
         /// Acceleration period (in days) - minimum
         /// </summary>
         literal int STATISTICS_ACCELERATED_PERIOD_MIN  = DATA_PUBLICATION_ACCELERATED_PERIOD_MIN;
         /// <summary>
         /// Acceleration period (in days) - maximum
         /// </summary>
         literal int STATISTICS_ACCELERATED_PERIOD_MAX = DATA_PUBLICATION_ACCELERATED_PERIOD_MAX;
         literal int STATISTICS_ACCELERATED_PERIOD_DEFAULT = DATA_PUBLICATION_ACCELERATED_PERIOD_DEFAULT;

         initonly static const TimeSpan STATISTICS_SEND_AT_DEFAULT;
      };
   }
#endif

//------------------------------------------------------------------------------
#endif
