//******************************************************************************
//
//   MenuEthernet.c  Ethernet menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuEthernet.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DInput.h"        // Input
#include "Gadget/DIp.h"           // IP labels
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Communication.h"
#include "Ethernet/MenuEthernetInfo.h"


static DefMenu( EthernetMenu)
   STR_DHCP,
   STR_IP,
   STR_SUBNET_MASK,
   STR_GATEWAY,
   STR_PRIMARY_DNS,
   STR_INFO,
EndMenu()

typedef enum {
   MI_DHCP,
   MI_IP,
   MI_SUBNET_MASK,
   MI_GATEWAY,
   MI_PRIMARY_DNS,
   MI_INFO
} EEthernetMenu;

// Local functions :

static void EthernetParameters( int Index, int y, TEthernet *Parameters);
// Draw ethernet parameters

//------------------------------------------------------------------------------
//  Menu Ethernet
//------------------------------------------------------------------------------

void MenuEthernet( void)
// Edit ethernet parameters
{
TMenuData MData;
int       i;

   forever {
      DMenuClear( MData);
      MData.Mask = 0;
      if(Ethernet.Dhcp == YES){
          MData.Mask = (1 << MI_IP) | (1 << MI_SUBNET_MASK)
                  | (1 << MI_GATEWAY)| (1 << MI_PRIMARY_DNS);
      }
      // selection :
      if( !DMenu( STR_ETHERNET, EthernetMenu, (TMenuItemCb *)EthernetParameters, &Ethernet, &MData)){
         ConfigEthernetSave();
         return;
      }
      switch( MData.Item){
         case MI_DHCP :
            i = Ethernet.Dhcp;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Ethernet.Dhcp = (byte)i;
            break;

         case MI_IP :
            i = (int) Ethernet.Ip;
            if( !DInputIp( STR_IP, STR_IP, (fnet_ip4_addr_t *) &i)){
               break;
            }
            Ethernet.Ip = (fnet_ip4_addr_t) i;
            break;

         case MI_SUBNET_MASK :
            i = (int) Ethernet.SubnetMask;
            if( !DInputIp( STR_SUBNET_MASK, STR_SUBNET_MASK, (fnet_ip4_addr_t *) &i)){
               break;
            }
            Ethernet.SubnetMask = (fnet_ip4_addr_t) i;
            break;

         case MI_GATEWAY :
            i = (int) Ethernet.Gateway;
            if( !DInputIp( STR_GATEWAY, STR_GATEWAY, (fnet_ip4_addr_t *) &i)){
               break;
            }
            Ethernet.Gateway = (fnet_ip4_addr_t) i;
            break;

         case MI_PRIMARY_DNS :
            i = (int) Ethernet.PrimaryDns;
            if( !DInputIp( STR_PRIMARY_DNS, STR_PRIMARY_DNS, (fnet_ip4_addr_t *) &i)){
               break;
            }
            Ethernet.PrimaryDns = (fnet_ip4_addr_t) i;
            break;

         case MI_INFO :
            MenuEthernetInfo();
            break;

      }
   }
} // MenuEthernet

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void EthernetParameters( int Index, int y, TEthernet *Parameters)
// Draw ethernet parameters
{
   switch( Index){
      case MI_DHCP :
         DLabelEnum( Parameters->Dhcp, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_IP :
         DIpShortRight( Parameters->Ip, DMENU_PARAMETERS_X, y);
         break;

      case MI_SUBNET_MASK :
         DIpShortRight( Parameters->SubnetMask, DMENU_PARAMETERS_X, y);
         break;

      case MI_GATEWAY :
         DIpShortRight( Parameters->Gateway, DMENU_PARAMETERS_X, y);
         break;

      case MI_PRIMARY_DNS :
         DIpShortRight( Parameters->PrimaryDns, DMENU_PARAMETERS_X, y);
         break;

      case MI_INFO :
         break;

   }
} // EthernetParameters
