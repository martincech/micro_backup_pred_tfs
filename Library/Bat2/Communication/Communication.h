//******************************************************************************
//
//   Communication.h
//   Version 1.0                 (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Communication_H__
   #define __Communication_H__

#ifndef __CommunicationDef_H__
   #include "CommunicationDef.h"
#endif

extern TDataPublication DataPublication;
extern TCellularData CellularData;
extern TEthernet Ethernet;

extern const TDataPublication DataPublicationDefault;
extern const TCellularData CellularDataDefault;
extern const TEthernet EthernetDefault;

extern       TCommunicationContext CommunicationContext;
extern const TCommunicationContext CommunicationContextDefault;

#endif
