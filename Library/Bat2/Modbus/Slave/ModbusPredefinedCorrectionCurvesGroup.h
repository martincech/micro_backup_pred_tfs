//******************************************************************************
//
//   ModbusPredefinedCorrectionCurvesGroup.h  Modbus Predefined Correction Curves register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusPredefinedCorrectionCurvesGroup_H__
   #define __ModbusPredefinedCorrectionCurvesGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadPredefinedCorrectionCurves( EModbusRegNum R);
// Read Predefined Correction Curves register group


TYesNo ModbusRegWritePredefinedCorrectionCurves( EModbusRegNum R, word D);
// Write Predefined Correction Curves register group



#endif
