//******************************************************************************
//
//   ModbusCharacterBufferGroup.h  Modbus Character buffer register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusCharacterBufferGroup_H__
   #define __ModbusCharacterBufferGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadCharacterBuffer( EModbusRegNum R);
// Read Character buffer register group


TYesNo ModbusRegWriteCharacterBuffer( EModbusRegNum R, word D);
// Write Character buffer register group



#endif
