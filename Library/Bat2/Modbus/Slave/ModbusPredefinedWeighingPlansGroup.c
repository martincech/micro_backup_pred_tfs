//******************************************************************************
//
//   ModbusPredefinedWeighingPlansGroup.c      Modbus Predefined Weighing Plans register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusPredefinedWeighingPlansGroup.h"
#include "Scheduler/WeighingSchedulerDef.h"
#include "Scheduler/WeighingPlanList.h"
#include "Scheduler/WeighingPlanList.h"
#include "Time/uTime.h"
#include "ModbusRegRangeCheck.h"
#include <string.h>


static TWeighingPlanIndex   _PredefinedWeighingPlansPlanIndex = 0; // Current weighing plan index, 0 means no plan
static TWeighingPlan WeighingPlan;


// Locals :


//------------------------------------------------------------------------------
//  Read Predefined Weighing Plans register
//------------------------------------------------------------------------------
word ModbusRegReadPredefinedWeighingPlans( EModbusRegNum R)
// Read Predefined Weighing Plans register group
{
int i;
TWeighingPlanList PlanList;
TWeighingTime WeighingTime;
UTime Time;

   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_20){
      i = R - MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_1;
      WeighingPlanTimeListLoad( &WeighingTime, &WeighingPlan, i);
      uTime( &Time, WeighingTime.From);
      return Time.Hour;
   }
   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_20){
      i = R - MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_1;
      WeighingPlanTimeListLoad( &WeighingTime, &WeighingPlan, i);
      uTime( &Time, WeighingTime.From);
      return Time.Min;
   }
   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_20){
      i = R - MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_1;
      WeighingPlanTimeListLoad( &WeighingTime, &WeighingPlan, i);
      uTime( &Time, WeighingTime.To);
      return Time.Hour;
   }
   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_20){
      i = R - MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_1;
      WeighingPlanTimeListLoad( &WeighingTime, &WeighingPlan, i);
      uTime( &Time, WeighingTime.To);
      return Time.Min;
   }
   switch ( R){
      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TYPE :
      {
         return WeighingPlan.Days.Mode;
      }
      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_OF_WEEK :
      {
         return WeighingPlan.Days.Days;
      }
      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_WEIGHING :
      {
         return WeighingPlan.Days.WeighingDays;
      }
      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_SUSPEND :
      {
         return WeighingPlan.Days.SuspendedDays;
      }
      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_START :
      {
         return WeighingPlan.Days.StartDay;
      }
      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_NAME :
      {
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, WeighingPlan.Name, WEIGHING_PLAN_NAME_SIZE);
         return 1;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Predefined Weighing Plans register
//------------------------------------------------------------------------------
TYesNo ModbusRegWritePredefinedWeighingPlans( EModbusRegNum R, word D)
// Write Predefined Weighing Plans register group
{
int i;
TWeighingPlanList PlanList;
TWeighingTime WeighingTime;
UTime Time;

   // Check value correctness
   if( !ModbusRegRangeCheckPredefinedWeighingPlans( R, D)){
      return NO;
   }
   // String NAME
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_NAME){
      strncpy( WeighingPlan.Name, (void *)WriteValuesBuffer.CharacterBuffer, WEIGHING_PLAN_NAME_SIZE);
      return YES;
   }
   // Command CREATE
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_CREATE){
      if( !(WeighingPlanListCount(&PlanList) < WeighingPlanListCapacity())){
         return NO;
      }
      while( !WeighingPlanListOpen( &PlanList));
      _PredefinedWeighingPlansPlanIndex = WeighingPlanListAdd( &PlanList, &WeighingPlan);
      WeighingPlanListClose( &PlanList);

      return YES;
   }
   // Command SAVE
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_SAVE){
      if( !(_PredefinedWeighingPlansPlanIndex > 0)){
         return NO;
      }
      while( !WeighingPlanListOpen( &PlanList));
      WeighingPlanListSave( &PlanList, &WeighingPlan, _PredefinedWeighingPlansPlanIndex);
      WeighingPlanListClose( &PlanList);

      return YES;
   }
   // Command DELETE
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DELETE){
      if( !(_PredefinedWeighingPlansPlanIndex > 0)){
         return NO;
      }
      while( !WeighingPlanListOpen( &PlanList));
      WeighingPlanListDelete( &PlanList, _PredefinedWeighingPlansPlanIndex);
      _PredefinedWeighingPlansPlanIndex--;
      WeighingPlanListClose( &PlanList);

      return YES;
   }
   // Global variable _PredefinedWeighingPlansPlanIndex
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_PLAN_INDEX){
      if( !(D < WeighingPlanListCount(&PlanList))){
         return NO;
      }
      _PredefinedWeighingPlansPlanIndex = D;
      while( !WeighingPlanListOpen( &PlanList));
      WeighingPlanListLoad( &PlanList, &WeighingPlan, _PredefinedWeighingPlansPlanIndex);
      WeighingPlanListClose( &PlanList);
      return YES;
   }
   // R/W registers 
   // register array TIME_FROM_HOUR
   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_20){
      i = R - MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_1; 
      WeighingPlanTimeListLoad( &WeighingTime, &WeighingPlan, i);uTime( &Time, WeighingTime.From);
      Time.Hour = D;
      WeighingTime.From = uTimeGauge( &Time);WeighingPlanTimeListSave( &WeighingTime, &WeighingPlan, i);
      return YES;
   }
   // register array TIME_FROM_MIN
   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_20){
      i = R - MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_1; 
      WeighingPlanTimeListLoad( &WeighingTime, &WeighingPlan, i);uTime( &Time, WeighingTime.From);
      Time.Min = D;
      WeighingTime.From = uTimeGauge( &Time);WeighingPlanTimeListSave( &WeighingTime, &WeighingPlan, i);
      return YES;
   }
   // register array TIME_TO_HOUR
   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_20){
      i = R - MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_1; 
      WeighingPlanTimeListLoad( &WeighingTime, &WeighingPlan, i);uTime( &Time, WeighingTime.To);
      Time.Hour = D;
      WeighingTime.To = uTimeGauge( &Time);WeighingPlanTimeListSave( &WeighingTime, &WeighingPlan, i);
      return YES;
   }
   // register array TIME_TO_MIN
   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_20){
      i = R - MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_1; 
      WeighingPlanTimeListLoad( &WeighingTime, &WeighingPlan, i);uTime( &Time, WeighingTime.To);
      Time.Min = D;
      WeighingTime.To = uTimeGauge( &Time);WeighingPlanTimeListSave( &WeighingTime, &WeighingPlan, i);
      return YES;
   }
   // register DAY_TYPE
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TYPE){    
            
      WeighingPlan.Days.Mode = D;      
      
      return YES;
   }
   // register DAY_OF_WEEK
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_OF_WEEK){    
            
      WeighingPlan.Days.Days = D;      
      
      return YES;
   }
   // register DAY_TECHNOLOGICAL_WEIGHING
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_WEIGHING){    
            
      WeighingPlan.Days.WeighingDays = D;      
      
      return YES;
   }
   // register DAY_TECHNOLOGICAL_SUSPEND
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_SUSPEND){    
            
      WeighingPlan.Days.SuspendedDays = D;      
      
      return YES;
   }
   // register DAY_TECHNOLOGICAL_START
   if( R == MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_START){    
            
      WeighingPlan.Days.StartDay = D;      
      
      return YES;
   }
   return NO;
}




