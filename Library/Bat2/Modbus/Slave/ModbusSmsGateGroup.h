//******************************************************************************
//
//   ModbusSmsGateGroup.h  Modbus SMS Gate register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusSmsGateGroup_H__
   #define __ModbusSmsGateGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadSmsGate( EModbusRegNum R);
// Read SMS Gate register group


TYesNo ModbusRegWriteSmsGate( EModbusRegNum R, word D);
// Write SMS Gate register group



#endif
