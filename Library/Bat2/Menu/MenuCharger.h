//******************************************************************************
//
//   MenuCharger.h    Menu charger
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuCharger_H__
   #define __MenuCharger_H__

#ifndef __Menu_H__
   #include "Menu/Menu.h"
#endif

typedef enum {
   MENU_CHARGER_EXIT_POWER_REMOVED = _MENU_EXIT_LAST
} EMenuChargerExit;

void MenuCharger( void);
// Menu charger

#endif
