//******************************************************************************
//
//   MenuConfigurationWeighing.c  Configuration weighing menu
//   Version 1.0                  (c) VEIT Electronics
//
//******************************************************************************

#include "MenuConfigurationWeighing.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DLabel.h"        // Display label
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Predefined/MenuPredefinedWeighing.h"
#include "Curve/MenuGrowthCurve.h"
#include "Curve/MenuCorrectionCurve.h"

#include "Weighing/WeighingConfiguration.h"
#include "Predefined/PredefinedList.h"
#include "Curve/Curvelist.h"
#include "Scheduler/MenuWeighingPlans.h"

static TPredefinedList PredefinedList;

static DefMenu( ConfigurationWeighingMenu)
   STR_DEFAULT_WEIGHING,
   STR_PREDEFINED_WEIGHINGS,
   STR_WEIGHING_PLAN,
   STR_GROWTH_CURVES,
   STR_CORRECTION_CURVES,
EndMenu()

typedef enum {
   MI_DEFAULT_WEIGHING,
   MI_PREDEFINED_WEIGHINGS,
   MI_WEIGHING_PLAN,
   MI_GROWTH_CURVES,
   MI_CORRECTION_CURVES
} EConfigurationWeighingMenu;

// Local functions :

static void ConfigurationWeighingParameters( int Index, int y, void *Parameters);
// Draw configuration weighing parameters

//------------------------------------------------------------------------------
//  Menu ConfigurationWeighing
//------------------------------------------------------------------------------

void MenuConfigurationWeighing( void)
// Edit configuration weighing parameters
{
TMenuData MData;
TPredefinedWeighingIdentifier Identifier;
TCurveList CurveList;
TCorrectionList CorrectionList;
   if(!PredefinedListOpen( &PredefinedList)) {
      return;
   }
   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_WEIGHING, ConfigurationWeighingMenu, (TMenuItemCb *)ConfigurationWeighingParameters, 0, &MData)){
         PredefinedListClose( &PredefinedList);
         return;
      }
      switch( MData.Item){
         case MI_PREDEFINED_WEIGHINGS :
            MenuPredefinedWeighing( &PredefinedList);
            break;
         case MI_WEIGHING_PLAN :
            MenuWeighingPlans();
            break;
         case MI_DEFAULT_WEIGHING :
            Identifier = DefaultWeighingGet();
            if( !MenuPredefinedWeighingSelect( &PredefinedList, STR_DEFAULT_WEIGHING, &Identifier, STR_WEIGHING_DISABLED)){
               break;
            }
            DefaultWeighingSet( &PredefinedList, Identifier);
            break;

         case MI_GROWTH_CURVES :
            if(!CurveListOpen( &CurveList)) {
               break;
            }
            MenuGrowthCurve( &CurveList);
            CurveListClose( &CurveList);
            break;

         case MI_CORRECTION_CURVES :
            if(!CorrectionListOpen( &CorrectionList)) {
               break;
            }
            MenuCorrectionCurve( &CorrectionList);
            CorrectionListClose( &CorrectionList);
            break;

      }
   }
} // MenuConfigurationWeighing

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void ConfigurationWeighingParameters( int Index, int y, void *Parameters)
// Draw configuration weighing parameters
{
char Name[ PREDEFINED_WEIGHING_NAME_SIZE + 1];
TPredefinedWeighingIdentifier Identifier;
   switch( Index){
      case MI_PREDEFINED_WEIGHINGS :
         break;

      case MI_DEFAULT_WEIGHING :
         Identifier = DefaultWeighingGet();

         if( !PredefinedListName( &PredefinedList, Identifier, Name)){
            DLabelNarrow( STR_WEIGHING_DISABLED, DMENU_PARAMETERS_X, y);
            break;
         }
         DLabelNarrow( Name, DMENU_PARAMETERS_X, y);
         break;

      case MI_GROWTH_CURVES :
         break;

      case MI_CORRECTION_CURVES :
         break;

   }
} // ConfigurationWeighingParameters


