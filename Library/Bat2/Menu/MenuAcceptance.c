//******************************************************************************
//
//   MenuAcceptance.c  Acceptance menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuAcceptance.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings

#include "Menu/MenuAcceptanceData.h"

static DefMenu( AcceptanceMenu)
   STR_MARGIN_ABOVE,
   STR_MARGIN_BELOW,
   STR_MALES,
   STR_FEMALES,
EndMenu()

typedef enum {
   MI_MARGIN_ABOVE,
   MI_MARGIN_BELOW,
   MI_MALES,
   MI_FEMALES
} EAcceptanceMenu;

// Local functions :

static void WeighingAcceptanceParameters( int Index, int y, TWeighingAcceptance *Parameters);
// Draw acceptance parameters

//------------------------------------------------------------------------------
//  Menu Acceptance
//------------------------------------------------------------------------------

void MenuAcceptance( TWeighingConfiguration *Configuration)
// Menu acceptance
{
TMenuData MData;
int       i;
TWeighingAcceptance *Acceptance;

   Acceptance = &Configuration->Acceptance;
   DMenuClear( MData);
   forever {
      //>>> item selection
      MData.Mask = 0;
      if( Configuration->TargetWeights.SexDifferentiation == SEX_DIFFERENTIATION_YES){
         MData.Mask |= (1 << MI_MARGIN_ABOVE) | (1 << MI_MARGIN_BELOW);
      } else {
         MData.Mask |= (1 << MI_MALES) | (1 << MI_FEMALES);
      }
      //<<< item selection
      // selection :
      if( !DMenu( STR_ACCEPTANCE, AcceptanceMenu, (TMenuItemCb *)WeighingAcceptanceParameters, Acceptance, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_MARGIN_ABOVE :
            i = Acceptance->Male.MarginAbove;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MIN, WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MAX, "%")){
               break;
            }
            Acceptance->Male.MarginAbove = (byte)i;
            break;

         case MI_MARGIN_BELOW :
            i = Acceptance->Male.MarginBelow;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_ACCEPTANCE_MARGIN_BELOW_MIN, WEIGHING_ACCEPTANCE_MARGIN_BELOW_MAX, "%")){
               break;
            }
            Acceptance->Male.MarginBelow = (byte)i;
            break;

         case MI_MALES :
            MenuAcceptanceData( STR_MALES,   &Acceptance->Male);
            break;

         case MI_FEMALES :
            MenuAcceptanceData( STR_FEMALES, &Acceptance->Female);
            break;

      }
   }
} // MenuAcceptance

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingAcceptanceParameters( int Index, int y, TWeighingAcceptance *Parameters)
// Draw acceptance parameters
{
   switch( Index){
      case MI_MARGIN_ABOVE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->Male.MarginAbove, "%");
         break;

      case MI_MARGIN_BELOW :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->Male.MarginBelow, "%");
         break;

      case MI_MALES :
         break;

      case MI_FEMALES :
         break;

   }
} // WeighingAcceptanceParameters
