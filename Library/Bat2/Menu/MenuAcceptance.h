//******************************************************************************
//
//   MenuAcceptance.h  Acceptance menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuAcceptance_H__
   #define __MenuAcceptance_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "Weighing/WeighingConfiguration.h"
#endif


void MenuAcceptance( TWeighingConfiguration *Configuration);
// Menu acceptance

#endif
