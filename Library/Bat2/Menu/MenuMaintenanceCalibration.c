//******************************************************************************
//
//   MenuCalibration.c  Calibration menu
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenanceCalibration.h"

#include "System/System.h"        // Operating system
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DMsg.h"          // Message box
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform/Zone.h"        // Platform data
#include "Weighing/Weighing.h"    // Weighing utility

//------------------------------------------------------------------------------
//   Main loop
//------------------------------------------------------------------------------

void MenuMaintenanceCalibration( void)
// Calibration menu
{
TWeightGauge Range;

   Range = PlatformCalibration.FullRange;
   if( !DInputWeight( STR_CALIBRATION, STR_ENTER_WEIGHT, &Range)){
      return;
   }
   PlatformCalibration.FullRange = Range;
   ConfigCalibrationSave();
   WeighingCalibration();
} // MenuMaintenanceCalibration
