//******************************************************************************
//
//   MenuRs485.h  Rs485 menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuRs485_H__
   #define __MenuRs485_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuRs485( void);
// Menu rs485

#endif
