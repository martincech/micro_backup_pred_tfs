//******************************************************************************
//
//   MenuMaintenanceSimulation.c  Maintenance simulation menu
//   Version 1.0                 (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenanceSimulation.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Gadget/DInput.h"        // Input box
#include "Gadget/DTime.h"         // Display time
#include "Gadget/DMsg.h"          // Message box
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Scheduler/WeighingScheduler.h"
#include "Weighing/WeighingConfiguration.h"

static DefMenu( MaintenanceSimulationMenu)
   STR_DAY_DURATION,
   STR_NEXT_DAY,
   STR_TARGET_WEIGHT_MALE,
   STR_TARGET_WEIGHT,
   STR_TARGET_WEIGHT_FEMALE,
EndMenu()

typedef enum {
   MI_DAY_DURATION,
   MI_NEXT_DAY,
   MI_TARGET_WEIGHT_MALE,
   MI_TARGET_WEIGHT,
   MI_TARGET_WEIGHT_FEMALE
} EMaintenanceSimulationMenu;

// Local functions :

static void SimulationParameters( int Index, int y, void *Parameters);
// Draw maintenance simulation parameters

//------------------------------------------------------------------------------
//  Menu MaintenanceSimulation
//------------------------------------------------------------------------------

void MenuMaintenanceSimulation( void)
// Edit maintenance simulation parameters
{
TMenuData MData;
UDateTime DateTime;
int       i;


   DMenuClear( MData);
   forever {
      //>>> item enable
      MData.Mask = 0;
      switch( WeighingSexDifferentation()){
         case SEX_DIFFERENTIATION_NO :
            MData.Mask |= (1 << MI_TARGET_WEIGHT_MALE) | (1 << MI_TARGET_WEIGHT_FEMALE);
            break;

         case SEX_DIFFERENTIATION_YES :
            MData.Mask |= (1 << MI_TARGET_WEIGHT);
            break;

         case SEX_DIFFERENTIATION_STEP_ONLY :
            MData.Mask |= (1 << MI_TARGET_WEIGHT_MALE) | (1 << MI_TARGET_WEIGHT_FEMALE);
            MData.Mask |= (1 << MI_TARGET_WEIGHT);
            break;

      }
      //<<< item enable
      // selection :
      if( !DMenu( STR_SIMULATION, MaintenanceSimulationMenu, (TMenuItemCb *)SimulationParameters, 0, &MData)){
         ConfigWeighingConfigurationSave();
         ContextWeighingContextSave();
         return;
      }
      switch( MData.Item){
         case MI_DAY_DURATION :
            if( WeighingStatus() != WEIGHING_STATUS_STOPPED){
               break;
            }
            uTime( &DateTime.Time, WeighingDayDuration());
            if( !DInputTime( STR_DAY_DURATION, STR_DAY_DURATION, &DateTime.Time)){
               break;
            }
            WeighingDayDurationSet( uTimeGauge( &DateTime.Time));
            break;

         case MI_NEXT_DAY :
            if( WeighingStatus() != WEIGHING_STATUS_WEIGHING){
                break;
            }
            if( !DMsgYesNo( STR_CONFIRMATION, STR_NEXT_DAY_CONFIRM, 0)){
               break;
            }
            WeighingSchedulerDayNext();
            break;

         case MI_TARGET_WEIGHT_MALE :
         case MI_TARGET_WEIGHT :
            i = WeighingContext.TargetWeight;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            WeighingContext.TargetWeight = (TWeightGauge)i;
            break;

         case MI_TARGET_WEIGHT_FEMALE :
            i = WeighingContext.TargetWeightFemale;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            WeighingContext.TargetWeightFemale = (TWeightGauge)i;
            break;

      }
   }
} // MenuMaintenanceSimulation

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void SimulationParameters( int Index, int y, void *Parameters)
// Draw maintenance simulation parameters
{
   switch( Index){
      case MI_DAY_DURATION :
         DTimeShortRight( WeighingDayDuration(), DMENU_PARAMETERS_X, y);
         break;

      case MI_NEXT_DAY :
         break;

      case MI_TARGET_WEIGHT_MALE :
      case MI_TARGET_WEIGHT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, WeighingContext.TargetWeight);
         break;

      case MI_TARGET_WEIGHT_FEMALE :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, WeighingContext.TargetWeightFemale);
         break;

   }
} // SimulationParameters
