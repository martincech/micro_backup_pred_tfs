//******************************************************************************
//
//   MenuWeighingStartLater.h   Weighing start later menu
//   Version 1.0                (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingStartLater_H__
   #define __MenuWeighingStartLater_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

TYesNo MenuWeighingStartLater( void);
// Menu weighing start later

#endif
