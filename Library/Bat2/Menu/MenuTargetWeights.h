//******************************************************************************
//
//   MenuTargetWeights.h  Target weights menu
//   Version 1.0          (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuTargetWeights_H__
   #define __MenuTargetWeights_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "Weighing/WeighingConfiguration.h"
#endif


void MenuTargetWeights( TWeighingTargetWeights *TargetWeights);
// Menu target weights

#endif
