//******************************************************************************
//
//   MenuPower.c             Menu Power
//   Version 1.0             (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPower.h"
#include "System/System.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Power/Power.h"
#include "Accu/Accu.h"
#include "Usb/Usb.h"
#include "Gui.h"

static void PrintScreen( void);
// Print screen

//------------------------------------------------------------------------------
//  Menu Power
//------------------------------------------------------------------------------

void MenuPower( void)
// Menu Power
{
int Key;
   forever {
      Key = GuiScheduler();

      switch( Key){
         case K_TIMER_SLOW :
            PrintScreen();            
            break;

         case K_SHUTDOWN:
            return;

         case K_ESC:
            return;
      }
   }
} // MenuAccuCharger

//******************************************************************************

//------------------------------------------------------------------------------
//  Print screen
//------------------------------------------------------------------------------

static void PrintScreen( void)
// Print screen
{
   GClear();
   GTextAt(0, 0);
   cprintf("Accu capacity remaining: %d %%\n", AccuCapacityRemaining());
   cprintf("Accu voltage: %d mV\n", AccuVoltage());
   cprintf("Accu current: %d mA\n", AccuCurrent());
   cprintf("Accu temp: %d oC\n", AccuTemperature());
   cputs("USB state: ");
   switch( UsbStatusGet()){
      case USB_STATE_OFF :
         cputs("Off");
         break;

      case USB_STATE_DEVICE_CHARGER_DETECTION :
         cputs("Charger detection");
         break;

      case USB_STATE_CHARGER :
         cputs("Charger");
         break;

      case USB_STATE_DEVICE_ATTACHED :
         cputs("Attached");
         break;

      case USB_STATE_DEVICE_CONNECTED :
         cputs("Connected to PC");
         break;

      case USB_STATE_HOST_ENABLED :
         cputs("Host enabled");
         break;

      case USB_STATE_HOST_CONNECTED :
         cputs("Host connected");
         break;

      default :
         cputs("Unknown");
         break;
   }

   cputs("\n");
   cprintf("Current limit: %d mA", Power.InputCurrentLimit);
   cputs("\n");
   cputs("Enough for GSM: ");

   if(Power.EnoughForGsm) {
      cputs("YES");
   } else {
      cputs("NO");
   }

   GFlush();
} // PrintScreen