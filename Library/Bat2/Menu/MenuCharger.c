//******************************************************************************
//
//   MenuCharger.c      Menu charger
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "MenuCharger.h"
#include "System/System.h"
#include "Graphic/Graphic.h"      // graphic
#include "Gadget/DLayout.h"       // Layout
#include "Gadget/DEvent.h"        // Events
#include "Str.h"                  // Strings
#include "Menu/MenuPower.h"       
#include "Menu/Screen.h"
#include "Power/Power.h"
#include "Kbd/Kbd.h"
#include "Sleep/Sleep.h"
#include "Usb/Usb.h"
#include "Display/Backlight.h"

//------------------------------------------------------------------------------
//  Menu charger
//------------------------------------------------------------------------------

void MenuCharger( void)
// Menu charger
{
int Key;
   forever {
      DEventDiscard();
      switch( SysEventWait()){
         case K_TIMER_FAST:
            if(!PowerExternal() || !Power.Accu.Present) {
               MenuExitSet(MENU_CHARGER_EXIT_POWER_REMOVED);
            }
            break;

         case K_FLASH1:
            ScreenCharger();
            break;

         case K_SHUTDOWN:
            return;

         case K_POWER_ON_OFF:
            MenuExitSet( MENU_EXIT_REBOOT);

         case K_ENTER:
            MenuPower();
            break;

         default:
            break;
      }
      GFlush();
   }
} // MenuMain
