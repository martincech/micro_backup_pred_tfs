//******************************************************************************
//
//   MenuAcceptanceData.h  Acceptance male/female menu
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuAcceptanceData_H__
   #define __MenuAcceptanceData_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "Weighing/WeighingConfiguration.h"
#endif


void MenuAcceptanceData( TUniStr Title, TWeighingAcceptanceData *AcceptanceData);
// Menu acceptance male/female

#endif
