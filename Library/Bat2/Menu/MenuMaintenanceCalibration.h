//******************************************************************************
//                                                                            
//   MenuMaintenanceCalibration.h  Calibration menu
//   Version 1.0                   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenanceCalibration_H__
   #define __MenuMaintenanceCalibration_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuMaintenanceCalibration( void);
// Menu calibration

#endif
