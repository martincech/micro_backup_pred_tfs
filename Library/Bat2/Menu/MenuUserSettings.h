//******************************************************************************
//
//   MenuUserSettings.h  User settings menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuUserSettings_H__
   #define __MenuUserSettings_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuUserSettings( void);
// Menu user settings

#endif
