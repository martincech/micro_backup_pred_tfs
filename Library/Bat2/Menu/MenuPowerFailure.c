//******************************************************************************
//
//   MenuPowerFailure.c    Menu power failure
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPowerFailure.h"
#include "Menu/Menu.h"
#include "System/System.h"
#include "Menu/Screen.h"          // Display screen
#include "Display/Backlight.h"
#include "Kbd/Kbd.h"
#include "Power/Power.h"
#include "Sleep/Sleep.h"
#include "Usb/Usb.h"
#include "Memory/Nvm.h" 
#include "Graphic/Graphic.h"
#include "Gui.h"
//------------------------------------------------------------------------------
//  Menu Main
//------------------------------------------------------------------------------

void MenuPowerFailure( void)
// Menu power failure
{
   UsbEnumerationEnable( NO);
   NvmShutdown();
   BacklightOff();
   ScreenPowerFailure();         // display power failure

   forever {
      switch( GuiScheduler()){
         case K_POWER_ON_OFF:
            MenuExitSet(MENU_EXIT_POWER_OFF);
            return;

         case K_POWER_OK:
            return;

         default:
            break;
      }
      GFlush();
   }
} // MenuPowerFailure
