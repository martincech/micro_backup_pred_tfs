//******************************************************************************
//
//   MenuRs485Interface.h  Rs485 interface 1 menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuRs485Interface_H__
   #define __MenuRs485Interface_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif


void MenuRs485Interface( byte Interface);
// Menu rs485 interface

#endif
