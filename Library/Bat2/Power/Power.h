//*****************************************************************************
//
//    Power.h      Power management
//    Version 1.1  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Power_H__
   #define __Power_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#include "Config/State.h"

#define Power        State.Power

#ifdef __cplusplus
   extern "C" {
#endif

void PowerInit( void);
// Initialize

void PowerExecute( void);
// Power task

TYesNo PowerSwitchedOnByUser( void);
// Power up reason

TYesNo PowerExternal( void);
// External power present

TYesNo PowerFailure( void);
// Check for power failure

TYesNo PowerEsdEvent( void);
// Check for esd event

void PowerOff( void);
// Power off

#ifdef __cplusplus
   }
#endif

#endif
