//*****************************************************************************
//
//    SuperCap.h    Bat2 supercap utility
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SuperCap_H__
   #define __SuperCap_H__

#include "Unisys/Uni.h"

void SuperCapInit( void);
// Init

void SuperCapExecute( void);
// Execute

TYesNo SuperCapOk( void);
// Supercap voltage valid

#endif