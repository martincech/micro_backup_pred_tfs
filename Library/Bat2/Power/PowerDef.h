//*****************************************************************************
//
//    PowerDef.h      Power definitions
//    Version 1.1  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __PowerDef_H__
   #define __PowerDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Accu_H__
   #include "Accu/Accu.h"
#endif

typedef struct {
   TYesNo             Present;
   byte               CapacityRemaining;
   word               Voltage;
   int16              Current;
   int8               Temperature;
} TAccuState;

typedef enum {
   CHARGER_STATUS_CHARGING,
   CHARGER_STATUS_DONE,
   CHARGER_STATUS_OFF,
   CHARGER_STATUS_ERROR
} EChargerStatus;

typedef struct {
   TYesNo     EthernetPowered;
   TYesNo     UsbPowered;
   TYesNo     Rs485Powered;
   TYesNo     EnoughForGsm;
   word       InputCurrentLimit;
   byte       ChargerStatus;
   TAccuState Accu;
} TPowerState;

#endif
