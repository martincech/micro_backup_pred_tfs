//******************************************************************************
//
//   StateDef.h    Bat2 state
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __StateDef_H__
   #define __StateDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#include "Power/PowerDef.h"

typedef struct {
   TPowerState Power;
} TState;

#endif
