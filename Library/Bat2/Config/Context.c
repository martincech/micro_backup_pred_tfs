//******************************************************************************
//
//   Config.c     Bat2 configuration
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Context.h"
#include "Config/ConfigDef.h"
#include "Data/uStorage.h"
#include "Menu/Menu.h"
#include "Weighing/WeighingConfiguration.h"
#include "Communication/Communication.h"
#include "Calibration/Calibration.h"
#include "Multitasking/Multitasking.h"
#include "SmsGate/SmsGate.h"
#include "Platform/Zone.h"
static UStorage Storage;

//------------------------------------------------------------------------------
//  Storage definition
//------------------------------------------------------------------------------

#define CONTEXT_SPARE   128

uStorageStart( ContextDescriptor, CONTEXT_COUNT)
   // !!! UPDATE EContextItem when items changed (added or rearanged)
   uStorageItem( &MenuContext),
   uStorageItem( &WeighingContext),
   uStorageItem( &CommunicationContext),
   uStorageItem( &SmsGateQueue),
   uStorageItem( &Calibration),
   uStorageItem( &PlatformCalibration),
   uStorageSpare( CONTEXT_SPARE)
uStorageEnd()

static void ContextSave();
//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo ContextRemoteLoad( void)
// Load from remote device
{
   if (!uStorageCopy(&ContextDescriptor, FILE_CONTEXT_LOCAL, FILE_CONTEXT_REMOTE)){
      return NO;
   }
   return YES;
} // ContextRemoteLoad

//------------------------------------------------------------------------------
//  Remote save
//------------------------------------------------------------------------------

TYesNo ContextRemoteSave( void)
// Save to remote device
{
   return uStorageCopy(&ContextDescriptor, FILE_CONTEXT_REMOTE, FILE_CONTEXT_LOCAL);
} // ContextRemoteSave

void ContextFactory( void)
// Set factory config on demand
{
   uStorageInit(&ContextDescriptor, FILE_CONTEXT_LOCAL);
   while(!uStorageOpen( &Storage, &ContextDescriptor, FILE_CONTEXT_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageDefault( &ContextDescriptor);
   uStorageSave( &Storage);
   uStorageClose( &Storage);
}
//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

void ContextLoad( void)
// Load configuration
{
#ifdef STORAGE_ALLWAYS_DEFAULT
   ContextFactory();
#endif
   uStorageInit( &ContextDescriptor, FILE_CONTEXT_LOCAL);
   while(!uStorageOpen( &Storage, &ContextDescriptor, FILE_CONTEXT_LOCAL)){
      MultitaskingReschedule();
   }

#ifndef STORAGE_ALLWAYS_DEFAULT
   // check for storage integrity :
   if( !uStorageCheck( &Storage)) {
      uStorageRestore( &Storage);
      if( !uStorageCheck( &Storage)) {
         uStorageDefault( &ContextDescriptor);
         uStorageSave( &Storage);
      }
   }
#endif
   // load storage data :
   uStorageLoad( &Storage);
   uStorageClose( &Storage);
} // ContextLoad

void ContextSave(void)
{
   ContextWeighingContextSave();
   ContextCommunicationSave();
   ContextCalibrationCoefficientsSave();
   ContextMenuContextSave();
   ContextCalibrationSave();
}
//------------------------------------------------------------------------------
//  Weighing context save
//------------------------------------------------------------------------------

void ContextWeighingContextSave( void)
// Save weighing context
{
   while(!uStorageOpen( &Storage, &ContextDescriptor, FILE_CONTEXT_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONTEXT_WEIGHING);
   uStorageClose( &Storage);
} // ContextWeighingContextSave

//------------------------------------------------------------------------------
//   Data publication context
//------------------------------------------------------------------------------

void ContextCommunicationSave( void)
// Save Communication subsystem context
{
   while(!uStorageOpen( &Storage, &ContextDescriptor, FILE_CONTEXT_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONTEXT_COMMUNICATION);
   uStorageClose( &Storage);
} // ContextGsmContextSave


//------------------------------------------------------------------------------
//   SMS context
//------------------------------------------------------------------------------

void ContextSmsGateSave( void)
// Save SMS gate subsystem context
{
   while(!uStorageOpen( &Storage, &ContextDescriptor, FILE_CONTEXT_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONTEXT_SMS_GATE);
   uStorageClose( &Storage);
} // ContextGsmContextSave

//------------------------------------------------------------------------------
//   Menu context
//------------------------------------------------------------------------------

void ContextMenuContextSave( void)
// Save Menu context
{
   while(!uStorageOpen( &Storage, &ContextDescriptor, FILE_CONTEXT_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONTEXT_MENU);
   uStorageClose( &Storage);
} // ContextMenuContextSave

//------------------------------------------------------------------------------
//  Platform calibration save
//------------------------------------------------------------------------------

void ContextCalibrationCoefficientsSave( void)
// Save Calibration configuration
{
   while(!uStorageOpen( &Storage, &ContextDescriptor, FILE_CONTEXT_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONTEXT_CALIBRATION_COEFFICIENTS);
   uStorageClose( &Storage);
} // ConfigCalibrationSave

//------------------------------------------------------------------------------
//  Calibration save
//------------------------------------------------------------------------------

void ContextCalibrationSave( void)
// Save Calibration configuration
{
   while(!uStorageOpen( &Storage, &ContextDescriptor, FILE_CONTEXT_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONTEXT_CALIBRATION);
   uStorageClose( &Storage);
} // ConfigCalibrationSave
