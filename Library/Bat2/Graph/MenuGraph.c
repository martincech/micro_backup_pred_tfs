//******************************************************************************
//
//   MenuGraph.c  Bat2 graph viewer page
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGraph.h"
#include "Sound/Beep.h"                   // Sounds
#include "Gadget/DEvent.h"                // Event manager
#include "Gadget/DLayout.h"               // Display layout
#include "Gadget/DGraph.h"                // Display graph
#include "Gadget/DMenu.h"                 // Display menu
#include "Gadget/DEdit.h"                 // Display edit value
#include "Gadget/DLabel.h"                // Display label
#include "Weight/DWeight.h"
#include "Console/conio.h"
#include "Archive/Archive.h"              // Archive items
#include "Graph/GraphDef.h"
#include "Str.h"                          // project directory strings
#include "Fonts.h"                        // Project fonts
#include "Bitmap.h"                       // Bitmaps
#include "Weighing/WeighingConfiguration.h"  // Weighing configuration data


static TYesNo         ArchiveHasFemales;
static TYesNo         ArchiveHasHourly;
static TYesNo         ShowHourly;
static word           CurrentHour[24];

static void MenuGraphSetDataLines(int *Line);
// Menu graph set data lines
static void GraphSetDataLinesParameters(  int Index, int y, void *Line);
// Menu graph set data lines parameters callback
static void LoadArchive(int *LinesData, TGraphLine *Lines, int LineCount, int *HourlyDayPos, TDayNumber *HourlyDayNumber);
// Load archive and fill data lines according to selected values

//------------------------------------------------------------------------------
//   Statistics graph menu
//------------------------------------------------------------------------------
#include "Console/Conio.h"        // Console output

static void XValueDayFormater( char *buf, int32 index){
   bprintf(buf, "%s: %d", StrGet(STR_DAY), index + 1);
}

static void XValueHourFormater( char *buf, int32 index){
   bprintf(buf, "%s: %d-%d", StrGet(STR_HOURS), (CurrentHour[index] >> 8) & 0xFF, CurrentHour[index] & 0xFF);
}

static void UniformityFormater( char *buf, int32 val){
   bprintf(buf, "%3.1f%%", val);
}

static void LinesCvFormater( char *buf, int32 val){
   bprintf(buf, "%3.1f", val);
}

#define POINTS_COUNT_MAX      100

void MenuGraph( void)
//Display page with graps
{   
static int     LinesData[GRAPH_LINE_COUNT] = {-1}; // static to stay selected
static int32   CursorPos = 0;
static int     DayPos = 0;
TDayNumber     DayNumber;
TYesNo         Redraw;
TYesNo         Reload;
TGraph         Graph;
TGraphLine     Lines[GRAPH_LINE_COUNT];
int32          Points[GRAPH_LINE_COUNT][POINTS_COUNT_MAX>>1];
int            i;
int32 u,v;


   Redraw = YES;
   Reload = YES;
   Graph.Lines = Lines;
   Graph.LineCount = GRAPH_LINE_COUNT;
   Lines[0].Points = Points[0];
   Lines[1].Points = Points[1];
   Lines[1].PointCount = Lines[0].PointCount = 0;
   Lines[0].LegendImage = Lines[1].LegendImage = 0;
   Lines[0].YValueFormater = Lines[1].YValueFormater = 0;
   if( LinesData[0] == -1){
      // daily and male with first two items in lines types
      ShowHourly = NO;
      for( i = _GRAPH_DATA_LINES_FIRST; i < _GRAPH_DATA_LINES_FIRST + GRAPH_LINE_COUNT; i++){
         LinesData[i] = i % _GRAPH_DATA_LINES_LAST;
      }
   }

   forever {
      if( Reload){
         LoadArchive( LinesData, Lines, GRAPH_LINE_COUNT, &DayPos, &DayNumber);
         Graph.XValueFormater = (ArchiveHasHourly && ShowHourly)? XValueHourFormater : XValueDayFormater;
         Graph.LinesSameYAxis = YES;

         for( i = 0; i < GRAPH_LINE_COUNT; i++){
            u = GraphDataGetType( LinesData[i]);
            v = GraphDataGetType(LinesData[0]);
            if( GraphDataGetType( LinesData[i]) != GraphDataGetType(LinesData[0])){
               Graph.LinesSameYAxis = NO;
            }
            switch( GraphDataGetType( LinesData[i])) {
               case GRAPH_DATA_LINES_AVERAGE_WEIGHT:
                  Lines[i].LegendImage = (TBitmap*) &BmpStatisticsWeight;
                  Lines[i].YValueFormater = BWeightWithUnits;
                  break;
               case GRAPH_DATA_LINES_TARGET_WEIGHT:
                  Lines[i].LegendImage = (TBitmap*) &BmpStatisticsTarget;
                  Lines[i].YValueFormater = BWeightWithUnits;
                  break;
               case GRAPH_DATA_LINES_CV:
                  Lines[i].LegendImage = (TBitmap*) &BmpStatisticsSigma;
                  Lines[i].YValueFormater = LinesCvFormater;
                  break;
               case GRAPH_DATA_LINES_COUNT:
                  Lines[i].LegendImage = (TBitmap*) &BmpStatisticsCount;
                  Lines[i].YValueFormater = 0;
                  break;
               case GRAPH_DATA_LINES_UNIFORMITY:
                  Lines[i].LegendImage = (TBitmap*) &BmpStatisticsUni;
                  Lines[i].YValueFormater = UniformityFormater;
                  break;
               default:
                  Lines[i].LegendImage = 0;
                  Lines[i].YValueFormater = 0;
                  break;
            }
         }
         Reload = NO;
      }
      if( Redraw){
         Graph.CursorOnPoint = CursorPos;
         GClear();
         if( ArchiveHasHourly && ShowHourly){
            char buf[20];
            bprintf(buf, "%s %d", StrGet(STR_DAY), DayNumber);
            DLayoutTitleLTRT( STR_GRAPHS, buf, 0);
         } else {
            DLayoutTitle( STR_GRAPHS);
         }
         DLayoutStatus( STR_BTN_OK, STR_BTN_SELECT, 0); // display status line
         DGraph( &Graph, DLAYOUT_TITLE_H, G_HEIGHT - DLAYOUT_TITLE_H - DLAYOUT_STATUS_H);
         if( Lines[0].PointCount == 0){
            DLabelCenter(STR_NO_ITEMS_FOUND, 0, (G_HEIGHT - DLAYOUT_STATUS_H - DLAYOUT_TITLE_H)/2, G_WIDTH, 0);
         }
         GFlush();
         Redraw = NO;
      }

      switch( DEventWait()){
         case K_UP :
            if( DayPos > 0 && ArchiveHasHourly && ShowHourly){
               DayPos--;
               Redraw = YES;
               Reload = YES;
               CursorPos = 0;
            }
            break;

         case K_DOWN :
            if( ArchiveHasHourly && ShowHourly){
               DayPos++;
               Redraw = YES;
               Reload = YES;
               CursorPos = 0;
            }
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            if( CursorPos > 0){
               CursorPos--;
               Redraw = YES;
            }
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            if( CursorPos < Lines[0].PointCount - 1){
               CursorPos++;
               Redraw = YES;
            }
            break;

         case K_ENTER :
            MenuGraphSetDataLines( LinesData);
            Redraw = YES;
            Reload = YES;
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return;
      }
   }
}// MenuGraph

static void LoadArchive(int *LinesData, TGraphLine *Lines, int LineCount, int *HourlyDayPos, TDayNumber *HourlyDayNumber)
// Load archive and fill data lines according to selected values
{
TArchive       Archive;
TArchiveIndex  ArchCount;
TArchiveIndex  ArchIndex;
TArchiveItem   ArchItem;
TArchiveItem   PrevItem;
TStatistic     Statistic;
TYesNo         EmptyDay;
TWeightGauge   Target, TargetF; // special case of data - part of marker instead of data
int            i, j;

   //archive items:
   //           H  O  U  R        H  O  U  R         D  A  Y
   // Marker | Male | Female | Male | Female ..... Male | Female
   // Marker | Male ....
   //reload archive to graph
   while( !ArchiveOpen( &Archive));
   ArchCount = ArchiveCount();
   if( ArchCount > 2){
      // check for females and hourly statistics in archive
      ArchiveHasFemales = NO;
      ArchiveHasHourly = NO;
      ArchiveGet( &Archive, 0, &PrevItem);
      for(ArchIndex = 1; ArchIndex < ArchCount; ArchIndex++){
         ArchiveGet( &Archive, ArchIndex, &ArchItem);
         if(ArchiveIsMarker( &ArchItem) && !ArchiveIsMarker(&PrevItem)){
            break;
         }
         else {
            if( !ArchiveIsDaily( &ArchItem.Data)){
               ArchiveHasHourly = YES;
            }else {
               if(ArchItem.Data.Sex == SEX_FEMALE){
                  ArchiveHasFemales = YES;
                  break;
               }
            }
         }
         PrevItem = ArchItem;
      }
   }else {
      ArchiveClose( &Archive);
      return;
   }


   ArchIndex = 0;
   if( ArchiveHasHourly && ShowHourly){
      // daily statistics - show from currently selected day only
      int i = -1;
      if( *HourlyDayPos != -1){
         for(; ArchIndex < ArchCount; ArchIndex++){
            if( i == *HourlyDayPos){
               ArchIndex--;
               break;
            }
            ArchiveGet( &Archive, ArchIndex, &ArchItem);
            if( ArchiveIsMarker( &ArchItem) && (ArchIndex != ArchCount - 1)){
               i++;
            }
         }
         if( i < *HourlyDayPos){
            *HourlyDayPos = 0;
            ArchIndex = 0;
         }
      } else {
         *HourlyDayPos = 0;
      }
      ArchiveGet( &Archive, ArchIndex, &ArchItem);
      *HourlyDayNumber = ArchItem.Marker.Day;
   }

   j = -1;
   EmptyDay = NO;
   for( ; ArchIndex < ArchCount; ArchIndex++){
      ArchiveGet( &Archive, ArchIndex, &ArchItem);
      if( ArchiveIsMarker( &ArchItem)){
         Target = ArchItem.Marker.TargetWeight;
         TargetF = ArchItem.Marker.TargetWeightFemale;
         if( !ArchiveHasHourly || !ShowHourly){
            // show daily statistics only
            j++;
            for( i = 0; i < LineCount; i++){
               Lines[i].Points[j] = DGRAPH_INVALID_POINT;
            }
         } else if( j >=0 ){
            //hourly statistics finished
            break;
         }

         if( EmptyDay){
           if( ArchiveHasHourly && ShowHourly){
              j = 0;
              break;
           }
         }
         EmptyDay = YES;
         continue;
      } else {
         if( ArchiveHasHourly && ShowHourly){
            // show hourly statistics only
            if( ArchiveIsDaily(&ArchItem.Data)){
               continue;
            } else {
               TYesNo contNext = YES;
               if( ArchItem.Data.Sex == SEX_FEMALE){
                  for( i = 0; i < LineCount; i++){
                     if( GraphDataGetGender( LinesData[i]) == SEX_FEMALE){
                        contNext = NO;
                        break;
                     }
                  }
               }
               if( contNext){
                  j++;
                  CurrentHour[j] = ArchItem.Data.HourFrom << 8 | ArchItem.Data.HourTo;
                  for( i = 0; i < LineCount; i++){
                     Lines[i].Points[j] = DGRAPH_INVALID_POINT;
                  }
               }
            }
         } else if (!ArchiveIsDaily(&ArchItem.Data)){
            continue;
         }
      }
      EmptyDay = NO;

      // get actual data to show
      ArchiveStatistic(&Statistic, &ArchItem);
      for( i = 0; i < LineCount; i++){
         // check gender type
         if(GraphDataGetGender( LinesData[i]) != ArchItem.Data.Sex) {
            continue;
         }
         switch( GraphDataGetType( LinesData[i])) {
            case GRAPH_DATA_LINES_AVERAGE_WEIGHT:
               Lines[i].Points[j] = Statistic.Average;
               break;
            case GRAPH_DATA_LINES_TARGET_WEIGHT:
               if( GraphDataGetGender( LinesData[i]) == SEX_FEMALE){
                  Lines[i].Points[j] = TargetF;
               }else {
                  Lines[i].Points[j] = Target;
               }
               break;
            case GRAPH_DATA_LINES_CV:
               Lines[i].Points[j] = Statistic.Cv;
               break;
            case GRAPH_DATA_LINES_COUNT:
               Lines[i].Points[j] = Statistic.Count;
               break;
            case GRAPH_DATA_LINES_UNIFORMITY:
               Lines[i].Points[j] = Statistic.Uniformity;
               break;
         }
      }
   }

   if( !ArchiveIsMarker( &ArchItem) && !EmptyDay){
      j++;
   }

   for( i = 0; i < LineCount; i++){
      Lines[i].PointCount = j;
   }

   ArchiveClose( &Archive);
}

//------------------------------------------------------------------------------
//  Menu for graph lines setting
//------------------------------------------------------------------------------

static void MenuGraphSetDataLines( int *Line)
// Menu graph set data lines
{
TMenuData MData;
int i;
TUniStr menu[GRAPH_LINE_COUNT*2+1+1]; // gender and type per line, daily/short + strnull
char buf[GRAPH_LINE_COUNT*2+1][20];


   DMenuClear( MData);
   for( i = 0; i < GRAPH_LINE_COUNT; i++){
      menu[i] = buf[i];
      bprintf(buf[i], "%s %d", StrGet(STR_LINE), i + 1);
   }
   for( ; i < GRAPH_LINE_COUNT*2; i++){
      menu[i] = buf[i];
      bprintf(buf[i], "%s %d %s", StrGet(STR_LINE), i - GRAPH_LINE_COUNT + 1, StrGet(STR_SEX));
      if( !ArchiveHasFemales){
         MData.Mask |= (1 << i);
      }
   }
   menu[i] = buf[i];
   bprintf(buf[i], "%s", StrGet(STR_HOURS));
   if (!ArchiveHasHourly){
      MData.Mask |= (1 << i);
   }

   menu[++i] = STR_NULL;
   forever {
      // selection :
      if( !DMenu( STR_SET_DATA_LINES, (const TUniStr *)&menu, GraphSetDataLinesParameters, Line, &MData)){
         return;
      }
      if( MData.Item < GRAPH_LINE_COUNT){
         // type settings
         i = GraphDataGetType(Line[MData.Item]);
         if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_GRAPH_DATA_LINES, _GRAPH_DATA_LINES_LAST)){
            continue;
         }
         GraphDataSetType(Line[MData.Item], i);
      } else if( MData.Item < GRAPH_LINE_COUNT*2){
         // gender settings
         i = GraphDataGetGender(Line[MData.Item % GRAPH_LINE_COUNT]);
         if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_SEX, SEX_UNDEFINED)){
            continue;
         }
         GraphDataSetGender(Line[MData.Item % GRAPH_LINE_COUNT], i);
      } else {
         i = ShowHourly;
         if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
            continue;
         }
         ShowHourly = i;
      }
   }
} // MenuGraphSetDataLines

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void GraphSetDataLinesParameters(  int Index, int y, void *Line)
// Menu graph set data lines parameters callback
{
   if( Index < GRAPH_LINE_COUNT){
      // type settings
      DLabelEnum( GraphDataGetType(((int*)Line)[Index]), ENUM_GRAPH_DATA_LINES, DMENU_PARAMETERS_X, y);
   } else if( Index < GRAPH_LINE_COUNT*2){
      // gender settings
      DLabelEnum( GraphDataGetGender(((int*)Line)[Index % GRAPH_LINE_COUNT]), ENUM_SEX, DMENU_PARAMETERS_X, y);
   } else {
      DLabelEnum( ShowHourly, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
   }
}
