//******************************************************************************
//
//   DacsTask.c     Dacs service task routines
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "DacsTask.h"
#include "Dacs/Dacs.h"
#include "Statistic/Statistic.h"          // Statistics
#include "Platform/Zone.h"                // Weighing zone
#include "Config/Config.h"                // Bat2 version
#include "Dacs/DacsConfig.h"
#include "Weighing/WeighingConfiguration.h"
#include "Archive/Archive.h"
#include "System/System.h"        // Operating system
#include <string.h>

static byte               _DacsTaskStatus;
//------------------------------------------------------------------------------
//  Task state
//------------------------------------------------------------------------------

typedef enum {
   DACS_TASK_STATUS_OFF,
   DACS_TASK_STATUS_IDLE,
   DACS_TASK_STATUS_LISTENING,
   DACS_TASK_STATUS_INCOMMING,
   DACS_TASK_STATUS_SENDING,
   _DACS_TASK_STATUS_LAST
} EDacsTaskStatus;

#define DacsTaskActive()     (_DacsTaskStatus != DACS_TASK_STATUS_OFF)
#define DacsTaskStart()      _DacsTaskStatus = DACS_TASK_STATUS_IDLE
#define DacsTaskStop()       _DacsTaskStatus = DACS_TASK_STATUS_OFF

// Local functions :

static TYesNo _SetReplyAccordingToRequest( TDacsRequest *Request, TDacsReply *Reply);
// Set reply message according to incomming command
static void _StatisticsForGender( TDacsGenderDataReply *GenderData, TStatistic *Statistics);
// Fill data for one gender
static void _CreateStatistics(word DayNumber, TStatistic *Male, TStatistic *Female);
// create statistics for current day in ACS (can be different from our day)
static int32 _ReadData(byte Address);
// DACS5 - read data from address
static TYesNo _WriteData(byte Address, int32 Value);
// DACS5 - write data to address

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void DacsTaskInit( int uartPort)
// Init
{
   DacsInit(uartPort);
   _DacsTaskStatus = DACS_TASK_STATUS_OFF;
}

//------------------------------------------------------------------------------
//  Free
//------------------------------------------------------------------------------

void DacsTaskFree( void)
// Deinit - free resources
{
   DacsDeinit();
   DacsTaskStop();
}

//------------------------------------------------------------------------------
//  Executive
//------------------------------------------------------------------------------

void DacsTaskExecute( void)
// Executive
{
TDacsStatus    Status;
TDacsRequest   *Request;
TDacsReply     *Reply;

   if( !DacsTaskActive()){
      DacsTaskStart();
   }

   forever{
      Status = DacsStatus();
      switch( _DacsTaskStatus){
         case DACS_TASK_STATUS_IDLE :
            if( Status != DACS_RECEIVE_LISTENING){
               DacsListenStart();
            }
            _DacsTaskStatus = DACS_TASK_STATUS_LISTENING;
            break;

         case DACS_TASK_STATUS_LISTENING :
            if( Status == DACS_RECEIVE_ERROR){
               DacsListenStart();
            }
            if( Status != DACS_RECEIVE_DATA_WAITING){
               return;
            }
            _DacsTaskStatus = DACS_TASK_STATUS_INCOMMING;
            break;

         case DACS_TASK_STATUS_INCOMMING :
            if( (Request = DacsReceive()) == NULL){
               _DacsTaskStatus = DACS_TASK_STATUS_IDLE;
               break;
            }
            Reply = DacsGetReplyBuffer();
            if(!_SetReplyAccordingToRequest( Request, Reply)){
               _DacsTaskStatus = DACS_TASK_STATUS_IDLE;
            }
            if(!DacsSend()){
               _DacsTaskStatus = DACS_TASK_STATUS_IDLE;
            }
            _DacsTaskStatus = DACS_TASK_STATUS_SENDING;
            break;

         case DACS_TASK_STATUS_SENDING :
            if( Status == DACS_SEND_ACTIVE){
               return;
            }
            _DacsTaskStatus =  DACS_TASK_STATUS_LISTENING;
            break;
      }
   }
}

//------------------------------------------------------------------------------
//   Create reply message according to incomming request
//------------------------------------------------------------------------------
static TYesNo _SetReplyAccordingToRequest( TDacsRequest *Request, TDacsReply *Reply)
// Set reply message according to incomming command
{
TStatistic Male;
TStatistic Female;

   switch( Request->RequestCode){
      case DACS_REQUEST_DATA :
         if( WeighingStatus() != WEIGHING_STATUS_WEIGHING){
            //weighing not in progress, time can be changed
            UDateTime dt;
            UDateTimeGauge dtGauge;
            dt.Date.Day = Request->Data.Day;
            dt.Date.Year = uYearSet(Request->Data.Year);
            dt.Date.Month = Request->Data.Month;
            dt.Time.Hour = Request->Data.Hour;
            dt.Time.Min = Request->Data.Min;
            dt.Time.Sec = Request->Data.Sec;
            dtGauge = uDateTimeGauge( &dt);
            SysDateTimeSet( dtGauge);
         }
         Reply->Data.ReplyCode = DACS_REPLY_DATA;
         Reply->Data.TypeCode = DACS_DATA_TYPE_STATISTICS;
         Reply->Data.Reserved = 0;
         _CreateStatistics( Request->Data.ProductionDay, &Male, &Female);
         _StatisticsForGender( &(Reply->Data.Female), &Female);
         _StatisticsForGender( &(Reply->Data.Male), &Male);
         if( ZoneLastFlag() & WEIGHT_FLAG_FEMALE ){
            Reply->Data.Male.WeightNow = 0;
         }  else {
            Reply->Data.Female.WeightNow = 0;
         }
         break;

      case DACS_REQUEST_VERSION :
         Reply->Version.ReplyCode = DACS_REPLY_VERSION;
         Reply->Version.Reserved = 0;
         Reply->Version.Major = DeviceVersionMajorGet(Bat2Version.Software);
         Reply->Version.Minor = DeviceVersionMinorGet(Bat2Version.Software);
         break;

      default :
         return NO;
   }
   return YES;
}

//------------------------------------------------------------------------------
//   Dacs6 helpers
//------------------------------------------------------------------------------

static void _StatisticsForGender( TDacsGenderDataReply *GenderData, TStatistic *Statistics)
// Fill data for one gender
{
   GenderData->TargetWeight   = Statistics->Target;
   GenderData->Average        = Statistics->Average;
   GenderData->Count          = Statistics->Count;
   GenderData->WeightNow      = ZoneLastWeight();
   GenderData->Gain           = Statistics->Gain;
   // since TStatistic uniformity is in 0.1% accuracy and we need only 1%
   GenderData->Uniformity     = Statistics->Uniformity/10;
   GenderData->Sigma          = Statistics->Sigma;
   GenderData->Cv             = Statistics->Cv;
}

static void _CreateStatistics( word DayNumber, TStatistic *Male, TStatistic *Female)
// create statistics for current day in ACS (can be different from our day)
{
TArchiveItem archive;
TArchiveIndex i;
TArchiveIndex archiveMarker;
TArchive Archive;

   memset( Female, 0, sizeof(TStatistic));
   memset( Male, 0, sizeof(TStatistic));
   Female->Uniformity = 1000;
   Male->Uniformity = 1000;

   // ACS before SCALE, or weighing stopped
   if( WeighingStatus() != WEIGHING_STATUS_WEIGHING ||
       DayNumber > WeighingContext.Day){
      // when weighing stopped return empty statistic
      // same when ACS before to scale
      return;
   }

   // ON time
   if( DayNumber == WeighingContext.Day){
      // ACS is on time, send current statistics
      memcpy( Male, ZoneStatistic(), sizeof(TStatistic));
      if(WeighingHasFemale()){
         memcpy( Female, ZoneStatisticFemale(), sizeof(TStatistic));
      }
      return;
   }

   // SCALE before ACS, user archive
   archiveMarker = -1;
   ArchiveOpen( &Archive);
   for(i = 0; i < ArchiveCount(); i++){
      ArchiveGet( &Archive, i, &archive);
      if( ArchiveIsMarker( &archive)){
         if(archive.Marker.Day == DayNumber){
            //daynumber i need
            archiveMarker = i;
            break;
         }
      }
   }
   if(archiveMarker != i){
      //no archive for requested day, return empty stats
      ArchiveClose( &Archive);
      return;
   }

   for(i= i+1; i < ArchiveCount(); i++){
      //go through data after marker
      ArchiveGet( &Archive, i, &archive);
      if( ArchiveIsMarker( &archive)){
         break;
      }
      if(archive.Data.Day == DayNumber){
         if( archive.Data.Sex == SEX_FEMALE){
            ArchiveStatistic( Female, &archive);
         } else {
            ArchiveStatistic( Male, &archive);
         }
      }
   }
   //update with values from marker
   ArchiveGet( &Archive, archiveMarker, &archive);
   Male->Target = archive.Marker.TargetWeight;
   if(WeighingHasFemale()){
      Female->Target = archive.Marker.TargetWeightFemale;
   }
   ArchiveClose( &Archive);
}

