//******************************************************************************
//
//   MenuWeighingPlan.h  Weighing plan menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingPlan_H__
   #define __MenuWeighingPlan_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingSchedulerDef_H__
   #include "Scheduler/WeighingSchedulerDef.h"
#endif

void MenuWeighingPlan( TWeighingPlan *WeighingPlan);
// Menu weighing plan

#endif
