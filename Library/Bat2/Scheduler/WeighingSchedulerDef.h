//******************************************************************************
//
//   WeighingSchedulerDef.h  Weighing scheduler data
//   Version 1.0             (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeighingSchedulerDef_H__
   #ifndef _MANAGED
   #define __WeighingSchedulerDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __uTime_H__
   #include "Time/uTime.h"
#endif

#ifndef __uNamedListDef_H__
   #include "Data/uNamedListDef.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------
#ifndef _MANAGED
#define WEIGHING_DAYS_DAYS_DEFAULT           0xFF

#define WEIGHING_DAYS_MIN                    1
#define WEIGHING_DAYS_MAX                    99
#define WEIGHING_DAYS_WEIGHING_DAYS_DEFAULT  1
#define WEIGHING_DAYS_SUSPENDED_DAYS_DEFAULT 1

#define WEIGHING_DAYS_START_DAY_MIN          DAY_NUMBER_MIN
#define WEIGHING_DAYS_START_DAY_MAX          DAY_NUMBER_MAX
#define WEIGHING_DAYS_START_DAY_DEFAULT      DAY_NUMBER_DEFAULT

#define WEIGHING_PLAN_NAME_SIZE              15
#define WEIGHING_PLAN_NAME_DEFAULT           "PLAN_"
#endif
//------------------------------------------------------------------------------
//  Weighing days mode
//------------------------------------------------------------------------------

#ifdef _MANAGED
namespace Bat2Library{
   public enum class WeighingDaysModeE{
#else
typedef enum {
#endif
   WEIGHING_DAYS_MODE_DAY_NUMBER,
   WEIGHING_DAYS_MODE_DAY_OF_WEEK,
#ifndef _MANAGED
   _WEIGHING_DAYS_MODE_LAST
} EWeighingDaysMode;
#else
   };
}
#endif

#ifdef _MANAGED
namespace Bat2Library{
   using namespace System;
   [Flags]
   public enum class WeighingDaysMaskE{
#else
typedef enum {
#endif
   WEIGHING_DAYS_MONDAY = 0x01,
   WEIGHING_DAYS_TUESDAY = 0x02,
   WEIGHING_DAYS_WEDNESDAY = 0x04,
   WEIGHING_DAYS_THURSDAY = 0x08,
   WEIGHING_DAYS_FRIDAY = 0x10,
   WEIGHING_DAYS_SATURDAY = 0x20,
   WEIGHING_DAYS_SUNDAY = 0x40
#ifndef _MANAGED
} EWeighingDaysMask;
#else
   };
}
#endif

#ifndef _MANAGED
typedef TTimeRange TWeighingTime;     // weighing single time range
typedef byte TWeighingTimeIndex;      // Weighing time index

#define WEIGHING_TIME_INDEX_INVALID  UDIRECTORY_INDEX_INVALID

//------------------------------------------------------------------------------
//  Weighing days
//------------------------------------------------------------------------------
typedef struct {
   byte       Mode;                         // Weighing days mode
   byte       Days;                         // Day of week mask
   TDayNumber WeighingDays;                 // Weighing days of pattern
   TDayNumber SuspendedDays;                // Suspended days of pattern
   TDayNumber StartDay;                     // Start day of pattern
} TWeighingDays;

//------------------------------------------------------------------------------
//  Weighing plan
//------------------------------------------------------------------------------

typedef struct {
   char                 Name[ WEIGHING_PLAN_NAME_SIZE + 1];     // Curve name (must be first item !)
   byte                 SyncWithDayStart;             // Synchronize scheduler with Day Start/Closed time
   word                 _Dummy;
   TWeighingDays        Days;                         // Weighing days configuration
   TWeighingTime        Times[ WEIGHING_TIME_COUNT];  // Weighing times list
   TWeighingTimeIndex   TimesCount;                   // Weighing times list index
} TWeighingPlan;

typedef UNamedListIdentifier TWeighingPlanIndex;      // Weighing plan index

#define WEIGHING_PLAN_INDEX_INVALID  UDIRECTORY_INDEX_INVALID
#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "WeighingSchedulerDef.h"
   #define _MANAGED
namespace Bat2Library{
	public ref class WeighingSchedulerC abstract sealed{
	public:		
		literal int DAY_MIN = WEIGHING_DAYS_MIN;
		literal int DAY_MAX = WEIGHING_DAYS_MAX;
		literal int ONLINE_DAYS_DEFAULT = WEIGHING_DAYS_WEIGHING_DAYS_DEFAULT;
		literal int SUSPENDED_DAYS_DEFAULT = WEIGHING_DAYS_SUSPENDED_DAYS_DEFAULT;
		literal int START_DAY_MIN = WEIGHING_DAYS_START_DAY_MIN;
		literal int START_DAY_MAX = WEIGHING_DAYS_START_DAY_MAX;
		literal int START_DAY_DEFAULT = WEIGHING_DAYS_START_DAY_DEFAULT;
		literal int NAME_SIZE = WEIGHING_PLAN_NAME_SIZE;		
	};
}
#endif
#endif
