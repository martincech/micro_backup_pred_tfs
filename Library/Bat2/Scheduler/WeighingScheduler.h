//******************************************************************************
//
//   WeighingScheduler.h   Weighing scheduler services
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeighingScheduler_H__
   #define __WeighingScheduler_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif
//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void WeighingSchedulerInit( void);
// Initialize

void WeighingSchedulerResume( void);
// Power on initialization

void WeighingSchedulerExecute( void);
// Executive

//------------------------------------------------------------------------------

void WeighingSchedulerStart( void);
// Start weighing

void WeighingSchedulerStartAt( UClockGauge Time);
// Start weighing

void WeighingSchedulerStop( void);
// Stop weighing

void WeighingSchedulerSuspend( void);
// Suspend weighing

void WeighingSchedulerRelease( void);
// Relase weighing

void WeighingSchedulerCalibration( void);
// Start weighing calibration

void WeighingSchedulerDiagnostics( void);
// Start diagnostic weighing

void WeighingSchedulerDayNext( void);
// Next technological day for simulation only

//------------------------------------------------------------------------------

UDateTimeGauge WeighingDayStartDateTime( TDayNumber Day);
// Calculate start date time of technological <Day>

#ifdef __cplusplus
   }
#endif
#endif
