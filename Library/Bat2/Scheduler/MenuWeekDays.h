//******************************************************************************
//
//   MenuWeekDays.h   Days of week selector
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeekDays_H__
   #define __MenuWeekDays_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeekDays( byte *WeekDays);
// Select week days

#endif
