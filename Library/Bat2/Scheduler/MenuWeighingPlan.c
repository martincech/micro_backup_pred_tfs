//******************************************************************************
//
//   MenuWeighingPlan.c  Weighing plan menu
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingPlan.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Gadget/DNamedList.h"    // Display named list operations
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration
#include "Gadget/DInput.h"        // Input string

#include "Scheduler/MenuWeighingDays.h"
#include "Scheduler/MenuWeighingTimes.h"

static DefMenu( WeighingPlanMenu)
   STR_NAME,
   STR_SYNC_WITH_DAY_START,
   STR_WEIGHING_DAYS,
   STR_WEIGHING_TIME,
EndMenu()

typedef enum {
   MI_NAME,
   MI_SYNC_WITH_DAY_START,
   MI_WEIGHING_DAYS,
   MI_WEIGHING_TIME
} EWeighingPlanMenu;

// Local functions :

static void WeighingPlanParameters( int Index, int y, TWeighingPlan *Parameters);
// Draw weighing plan parameters

//------------------------------------------------------------------------------
//  Menu WeighingPlan
//------------------------------------------------------------------------------
void MenuWeighingPlan( TWeighingPlan *WeighingPlan)
// Edit weighing plan parameters
{
TMenuData MData;
char      Name[ WEIGHING_PLAN_NAME_SIZE + 1];
int       i;

   DMenuClear( MData);
   forever {
      //>>> item enable
      //!!!MData.Mask = 0;
      MData.Mask = (1 << MI_SYNC_WITH_DAY_START);
      //<<< item enable
      // selection :
      if( !DMenu( STR_WEIGHING_PLAN, WeighingPlanMenu, (TMenuItemCb *)WeighingPlanParameters, WeighingPlan, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_NAME :
            strcpy( Name, WeighingPlan->Name);
            if( !DInputText( STR_NAME, STR_ENTER_NAME, Name, WEIGHING_PLAN_NAME_SIZE, YES)){
               break;
            }
            strcpy( WeighingPlan->Name, Name);
            break;
         case MI_SYNC_WITH_DAY_START :
            i = WeighingPlan->SyncWithDayStart;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            WeighingPlan->SyncWithDayStart = (byte)i;
            break;

         case MI_WEIGHING_DAYS :
            MenuWeighingDays( WeighingPlan);
            break;

         case MI_WEIGHING_TIME :
            MenuWeighingTimes( WeighingPlan);
            break;

      }
   }
} // MenuWeighingPlan

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingPlanParameters( int Index, int y, TWeighingPlan *Parameters)
// Draw weighing plan parameters
{
   switch( Index){
      case MI_NAME :
         DLabelNarrow( Parameters->Name, DMENU_PARAMETERS_X, y);
         break;

      case MI_SYNC_WITH_DAY_START :
         DLabelEnum( Parameters->SyncWithDayStart, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_WEIGHING_DAYS :
         break;

      case MI_WEIGHING_TIME :
         break;

   }
} // WeighingPlanParameters
