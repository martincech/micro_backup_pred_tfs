//******************************************************************************
//
//   MenuWeighingTimes.c  Weighing times menu
//   Version 1.0          (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingTimes.h"
#include "Gadget/DMenu.h"                // Display menu
#include "Str.h"                         // Strings

#include "Gadget/DTimeRange.h"           // Time range edit
#include "Data/uList.h"                  // Sequential list
#include "Scheduler/WeighingPlanList.h"  // Weighing plan list
#include "Scheduler/WeighingPlan.h"      // Default weighing time
#include <string.h>

static DefMenu( WeighingTimesMenu)
   STR_EDIT,
   STR_CREATE,
   STR_DELETE,
EndMenu()

typedef enum {
   MI_EDIT,
   MI_CREATE,
   MI_DELETE
} EWeighingTimesMenu;

// Local functions :

//------------------------------------------------------------------------------
//  Menu Weighing Times
//------------------------------------------------------------------------------

void MenuWeighingTimes( TWeighingPlan *WeighingPlan)
// Menu weighing times
{
TMenuData          MData;
TWeighingTime      WeighingTime;
int                TimesCount;

   DMenuClear( MData);
   TimesCount = WeighingPlanTimeListCount( WeighingPlan);
   forever {
      // check for directory capacity :
      MData.Mask = 0;
      if( TimesCount == WeighingPlanTimeListCapacity()){
         MData.Mask |= (1 << MI_CREATE);              // disable create
         if( MData.Item == MI_CREATE){
            MData.Item = MI_EDIT;
         }
      }
      // check for empty list :
      if( TimesCount <= 1){
         MData.Mask |= (1 << MI_DELETE);              // disable delete
         if( MData.Item == MI_DELETE){
            MData.Item = MI_EDIT;
         }
      }
      // selection :
      if( !DMenu( STR_WEIGHING_TIME, WeighingTimesMenu, 0, 0, &MData)){
         return;
      }

      switch( MData.Item){
         case MI_EDIT :
            DTimeRangeEdit( STR_WEIGHING_TIME, WeighingPlan->Times, TimesCount);
            break;

         case MI_CREATE :
            memcpy( &WeighingTime, &TimeRangeDefault, sizeof( TWeighingTime)); // fill with defaults
            DTimeRangeCreate( &WeighingTime);
            if( memequ( &WeighingTime, &TimeRangeDefault, sizeof( TWeighingTime))){
               break;                                          // not modified by user
            }
            WeighingPlanTimeListAdd( WeighingPlan, &WeighingTime);        // append item
            TimesCount = WeighingPlanTimeListCount( WeighingPlan);
            break;

         case MI_DELETE :
            DTimeRangeDelete( STR_WEIGHING_TIME, WeighingPlan->Times, &TimesCount);
            if( WeighingPlanTimeListCount( WeighingPlan) != TimesCount){
               WeighingPlan->TimesCount = TimesCount;
            }
            break;
      }
   }
} // MenuWeighingTimes

//******************************************************************************




