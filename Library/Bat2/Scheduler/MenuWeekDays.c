//******************************************************************************
//
//   MenuWeekDays.c   Days of week selector
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeekDays.h"
#include "Gadget/DCheckList.h"         // Display checklist
#include "Str.h"                       // Strings


static DefCheckList( WeekDaysList)
   STR_MONDAY,
   STR_TUESDAY,
   STR_WEDNESDAY,
   STR_THURSDAY,
   STR_FRIDAY,
   STR_SATURDAY,
   STR_SUNDAY,
EndCheckList()

//------------------------------------------------------------------------------
//  Menu weighing menu configuration selector
//------------------------------------------------------------------------------

void MenuWeekDays( byte *WeekDays)
// Select week days
{
dword Mask;

   Mask = *WeekDays;
   DCheckList( STR_DAYS, WeekDaysList, &Mask);
   *WeekDays = (byte)Mask;
} // MenuWeighingMenuSelection
