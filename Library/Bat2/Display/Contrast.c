//******************************************************************************
//                                                                            
//   Contrast.c     Display contrast
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Contrast.h"
#include "Lcd/St7259.h"                     // GPU contrast
#include "Display/DisplayConfiguration.h"

//------------------------------------------------------------------------------
//  Contrast Set
//------------------------------------------------------------------------------

void ContrastSet( void)
// Set contrast
{
   ContrastTest( DisplayConfiguration.Contrast);
} // ContrastSet

//------------------------------------------------------------------------------
//  Contrast Test
//------------------------------------------------------------------------------

void ContrastTest( int Contrast)
// Test contrast
{
   GpuContrast( GPU_EC_BASE - CONTRAST_MAX / 2 + Contrast);
} // ContrastTest
