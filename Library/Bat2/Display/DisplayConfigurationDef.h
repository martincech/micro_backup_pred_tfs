//*****************************************************************************
//
//    DisplayConfigurationDef.h  Display configuration definition
//    Version 1.0               (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __DisplayConfigurationDef_H__
   #ifndef _MANAGED
      #define __DisplayConfigurationDef_H__
   #endif


#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//-----------------------------------------------------------------------------
//  Backlight
//-----------------------------------------------------------------------------

// Backlight mode
#ifdef _MANAGED
namespace Bat2Library{
   public enum class BacklightModeE{
#else
typedef enum {
#endif
      BACKLIGHT_MODE_AUTO,                // Backlight in automatic mode
      BACKLIGHT_MODE_ON,	               // Backlight is always on
      BACKLIGHT_MODE_OFF,	               // Backlight is always off
#ifndef _MANAGED
      _BACKLIGHT_MODE_COUNT
   } EBacklightMode;
#else
   };
}
#endif

#ifndef _MANAGED
// Backlight configuration
typedef struct {
   byte Mode;		               // EBacklightMode Backlight mode   
   byte Intensity;	               // Backlight intensity
   word Duration;                      // Backlight duration [s]
} TBacklightConfiguration;

#endif
//-----------------------------------------------------------------------------
// Display configuration
//-----------------------------------------------------------------------------

// Display mode
#ifdef _MANAGED
namespace Bat2Library{
   public enum class DisplayModeE{
#else
typedef enum {
#endif
   DISPLAY_MODE_BASIC,                 // Basic display mode
   DISPLAY_MODE_ADVANCED,              // Advanced display mode
   DISPLAY_MODE_LARGE,                 // Large display mode
#ifndef _MANAGED
   _DISPLAY_MODE_COUNT
} EDisplayMode;
#else
   };
}
#endif

#ifndef _MANAGED

typedef struct {
   byte                    Mode;       // EDisplayMode Display mode
   byte                    Contrast;   // Display contrast
   byte                    PowerSave;
   byte                    _Spare;
   TBacklightConfiguration Backlight;  // Backlight configuration
} TDisplayConfiguration;

//-----------------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------------

#define BACKLIGHT_INTENSITY_MIN        0      // min. backlight intensity
#define BACKLIGHT_INTENSITY_MAX        9      // max. backlight intensity
#define BACKLIGHT_INTENSITY_DEFAULT    8      // default backlight intensity

#define BACKLIGHT_DURATION_MIN         1      // min. backlight duration [s]
#define BACKLIGHT_DURATION_MAX        99      // max. backlight duration [s]
#define BACKLIGHT_DURATION_DEFAULT    20      // default backlight duration [s]

#define CONTRAST_MIN                   0      // min. contrast intensity
#define CONTRAST_MAX                  64      // max. contrast intensity
#define CONTRAST_DEFAULT              32      // default contrast intensity

#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "DisplayConfigurationDef.h"
   #define _MANAGED
   namespace Bat2Library{
      public ref class DisplayConfigurationC abstract sealed{
      public:
         literal byte DISPLAY_CONTRAST_MIN = CONTRAST_MIN;
         literal byte DISPLAY_CONTRAST_MAX = CONTRAST_MAX;
         literal byte DISPLAY_CONTRAST_DEFAULT = CONTRAST_DEFAULT;

         static property byte DisplayContrastMin{byte get(){return DISPLAY_CONTRAST_MIN;}}
         static property byte DisplayContrastMax{byte get(){return DISPLAY_CONTRAST_MAX;}}
         static property byte DisplayContrastDefault{byte get(){return DISPLAY_CONTRAST_DEFAULT;}}
      };

      public ref class BacklightConfigurationC abstract sealed{
      public:
         literal byte INTENSITY_MIN = BACKLIGHT_INTENSITY_MIN;
         literal byte INTENSITY_MAX = BACKLIGHT_INTENSITY_MAX;
         literal byte INTENSITY_DEFAULT = BACKLIGHT_INTENSITY_DEFAULT;

         literal word DURATION_MIN = BACKLIGHT_DURATION_MIN;
         literal word DURATION_MAX = BACKLIGHT_DURATION_MAX;
         literal word DURATION_DEFAULT = BACKLIGHT_DURATION_DEFAULT;

         static property byte IntensityMin{byte get(){return INTENSITY_MIN;}}
         static property byte IntensityMax{byte get(){return INTENSITY_MAX;}}
         static property byte IntensityDefault{byte get(){return INTENSITY_DEFAULT;}}

         static property word DurationMin{word get(){return DURATION_MIN;}}
         static property word DurationMax{word get(){return DURATION_MAX;}}
         static property word DurationDefault{word get(){return DURATION_DEFAULT;}}
      };

   }
#endif

#endif // __DisplayConfigurationDef_H__
