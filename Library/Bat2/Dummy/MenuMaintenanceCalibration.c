//******************************************************************************
//
//   MenuCalibration.c  Calibration menu for uBat2
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "Menu/MenuMaintenanceCalibration.h"

#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DMsg.h"          // Message box
#include "Str.h"                  // Strings

//------------------------------------------------------------------------------
//   Main loop
//------------------------------------------------------------------------------

void MenuMaintenanceCalibration( void)
// Calibration menu
{
   DMsgOk( "Calibration", "Calibration not supported", "");
   DMsgWait();
} // MenuMaintenanceCalibration
