//*****************************************************************************
//
//    Multitasking.c       Dummy Multitasking
//    Version 1.0          (c) Veit Electronics
//
//*****************************************************************************

#include "Multitasking/Multitasking.h"

//-----------------------------------------------------------------------------
// Reschedule
//-----------------------------------------------------------------------------

void MultitaskingReschedule( void)
// Reschedule
{
}

//-----------------------------------------------------------------------------
// Mutex
//-----------------------------------------------------------------------------

void MultitaskingMutexInit(TMutex *Mutex)
// Init mutex
{
   MultitaskingMutexRelease(Mutex);
} // MultitaskingMutexInit

void MultitaskingMutexSet( TMutex *Mutex)
// Set mutex
{
   forever {
      if(!*Mutex) {
         *Mutex = 1;
         break;
      }
   }
} // MultitaskingMutexSet

TYesNo MultitaskingMutexTrySet( TMutex *Mutex)
// Set mutex
{
   if(!*Mutex) {
      *Mutex = 1;
      return YES;
   }
   return NO;
} // MultitaskingMutexTrySet

void MultitaskingMutexRelease( TMutex *Mutex)
// Release mutex
{
   *Mutex = 0;
} // MultitaskingMutexRelease
