//******************************************************************************
//
//   Efs.c        External File System
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "File/Efs.h"

//-----------------------------------------------------------------------------
//   Initialize
//-----------------------------------------------------------------------------

void EfsInit( void)
// Initialisation
{
} // EfsInit

//-----------------------------------------------------------------------------
//   Directory exists
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryExists( const char *Path)
// Check for directory <Name>
{
} // EfsDirectoryExists

//-----------------------------------------------------------------------------
//   Directory create
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryCreate( const char *Name)
// Create new directory with <Name>
{
   return( YES);
} // EfsDirectoryCreate

//-----------------------------------------------------------------------------
//   Directory change
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryChange( const char *Name)
// Change current directory to <Name>
{
   return( YES);
} // EfsDirectoryChange

//-----------------------------------------------------------------------------
//   Directory delete
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryDelete( const char *Name)
// Delete directory <Name>
{
   return( YES);
} // EfsDirectoryDelete

//-----------------------------------------------------------------------------
//   Directory rename
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryRename( const char *Path, const char *NewName)
// Renames selected directory
{
} // EfsDirectoryRename

//-----------------------------------------------------------------------------
//   File create
//-----------------------------------------------------------------------------

TYesNo EfsFileCreate( const char *FileName, dword Size)
// Create <FileName>
{
   return( YES);
} // EfsFileCreate

//-----------------------------------------------------------------------------
//   File open
//-----------------------------------------------------------------------------

TYesNo EfsFileOpen( const char *FileName)
// Open <FileName> for read/write
{
   return( YES);
} // EfsFileOpen

//-----------------------------------------------------------------------------
//   File delete
//-----------------------------------------------------------------------------

TYesNo EfsFileDelete( const char *FileName)
// Delete <FileName>
{
   return( YES);
} // EfsFileDelete

//-----------------------------------------------------------------------------
//   File close
//-----------------------------------------------------------------------------

void EfsFileClose( void)
// Close opened file
{
} // EfsFileClose

//-----------------------------------------------------------------------------
//   File write
//-----------------------------------------------------------------------------

word EfsFileWrite( void *Data, word Count)
// Write <Data> with <Count> bytes, returns number of bytes written
{
   return( Count);
} // EfsFileWrite

//-----------------------------------------------------------------------------
//   File read
//-----------------------------------------------------------------------------

word EfsFileRead( void *Data, word Count)
// Read <Data> with <Count> bytes, returns number of bytes read
{
   return( Count);
} // EfsFileRead

//-----------------------------------------------------------------------------
//   File seek
//-----------------------------------------------------------------------------

TYesNo EfsFileSeek( int32 Pos, EEfsSeekMode Whence)
// Seek at <Pos> starting from <Whence>
{
   return( YES);
} // EfsFileSeek

//-----------------------------------------------------------------------------
//   Efs Unmount
//-----------------------------------------------------------------------------

void EfsUnmount( void)
{
} // Safely unmount drive