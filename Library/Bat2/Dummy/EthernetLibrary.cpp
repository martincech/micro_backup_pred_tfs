//******************************************************************************
//
//   EthernetLibrary.cpp   Ethernet dummy implementation
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#include "Ethernet/EthernetLibrary.h"
#include "Debug/uDebug.h"
#include <string.h>
#include <stdio.h>
#include "Modbus/TimerSlot.h"
#include <QTimer>
#include <QNetworkInterface>

static dword upTimeS;
static QNetworkInterface netIface;

static void timerFunc(){
   upTimeS++;
}

static QTimer timer;
#define TIMER_INTERVAL_MS 1000
static TimerSlot ts(timerFunc);


void EthernetLibraryExecute()
// main library executive
{
}

//------------------------------------------------------------------------------
//  stack routines
//------------------------------------------------------------------------------

TYesNo EthernetLibraryInitNetworkInterface()
// init stack and network interface
{
   TRACE( "Ethernet library : Init stack");

   foreach (QNetworkInterface iface, QNetworkInterface::allInterfaces()) {
    if(!iface.isValid() || (iface.flags() & QNetworkInterface::IsLoopBack) != 0 ||
          (iface.flags() & QNetworkInterface::IsUp) == 0 || (iface.flags() & QNetworkInterface::IsRunning)==0 ){
        continue;
    }
    netIface = iface;
   }
   if(!netIface.isValid()){return NO;}

   upTimeS = 0;
   timer.setInterval(TIMER_INTERVAL_MS);
   QObject::connect(&timer, SIGNAL(timeout()), &ts, SLOT(timerExpired()));
   timer.start();
   return YES;
}

void EthernetLibraryFreeNetworkInterface()
// free stack
{
   netIface = QNetworkInterface();
   TRACE( "Ethernet library : Free stack");
}

//------------------------------------------------------------------------------
//  service routines
//------------------------------------------------------------------------------

TYesNo EthernetLibraryInitDHCPService()
// init DHCP service
{
   TRACE( "Ethernet library : DHCP started");
   return YES;
}

void EthernetLibraryFreeDHCPService()
// free DHCP service and resources
{
   TRACE( "Ethernet library : DHCP released");
}

//------------------------------------------------------------------------------
//  info routines
//------------------------------------------------------------------------------

void EthernetLibraryEthName( char *buffer, int bufferSize)
// return zero terminated interface name string
{
   strncpy(buffer, netIface.humanReadableName().toStdString().c_str(), bufferSize);
}

void EthernetLibraryIPv4( char *buffer, int bufferSize)
// return zero terminated IPv4 address with automatic or manual subscript(based on DHCP service)
{
   snprintf(buffer, bufferSize, netIface.allAddresses().first().toString().toStdString().c_str());
}
void EthernetLibraryDnsServer( char *buffer, int bufferSize)
// return zero terminated IPv4 dns server address
{
    snprintf(buffer, bufferSize, "unknown");
}


void EthernetLibraryLinkStatus( char *buffer, int bufferSize)
// return zero terminated link status string
{
    snprintf(buffer, bufferSize, netIface.isValid()? "connected" : "disconnected");
}

dword EthernetLibraryUpTime()
// return time for which is library used
{
   return upTimeS;
}

dword EthernetLibraryRxPacketCount()
// return received packet count
{
static int dummyCount = 0;

   return dummyCount++;
}

dword EthernetLibraryTxPacketCount()
// return transmit packet count
{
static int dummyCount = 0;

   return dummyCount++;
}

void EthernetSleepEnable( TYesNo Enable)
{
}

TYesNo EthernetIsLink( void)
{
   return netIface.isValid()? YES:NO;
}

TYesNo EthernetLibraryIpAssigned()
{
   foreach (QNetworkAddressEntry address, netIface.addressEntries()) {
      if(!address.ip().isNull()){
         return YES;
      }
   }
   return NO;
}
