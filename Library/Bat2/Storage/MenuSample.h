//******************************************************************************
//                                                                            
//   MenuSample.h  Bat2 samples memory viewer
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuSample_H__
   #define __MenuSample_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuSample( void);
// Display samples memory

#endif


