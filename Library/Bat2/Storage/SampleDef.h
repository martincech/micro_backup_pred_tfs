//******************************************************************************
//
//   SampleDef.h   Weighing sample definition
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __SampleDef_H__
   #define __SampleDef_H__

#ifndef __PlatformDef_H__
   #include "Platform/PlatformDef.h"
#endif

#ifndef __WeightFlagDef_H__
   #include "Weight/WeightFlagDef.h"
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define SAMPLE_SIZE   sizeof( TSample)

//------------------------------------------------------------------------------
//  Data
//------------------------------------------------------------------------------

typedef word  TSampleOrigin;
typedef word  TSampleTimestamp;
typedef byte  TSampleFlag;
typedef dword TSampleWeight;
typedef dword TSampleIndex;

//------------------------------------------------------------------------------
//  Sample
//------------------------------------------------------------------------------

typedef struct {
   TSampleOrigin    Origin;            // sample originator (platform or box address)
   TSampleTimestamp Timestamp;         // sample timestamp
   TSampleWeight    Weight;            // sample weight & flags
} TSample;

#define _SampleWeightGet( w)             ((int32)((w) & 0xFFFFFF))
#define _SampleFlagGet( w)               ((byte)((w) >> 24))
#define _SampleWeightSet( w, f)          (((w) & 0xFFFFFF) | ((dword)(f) << 24))
#define _SampleWeightSignumExtension( w) ((w) & 0x00800000L) ? ((w) | 0xFF000000L) : (w)

//------------------------------------------------------------------------------

#endif
