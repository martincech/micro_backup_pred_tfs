//******************************************************************************
//
//   ZoneConfig.h     Weighing Zone permanent configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "Zone.h"

TPlatformVersion     PlatformVersion;
TPlatformSigmaDelta  PlatformSigmaDelta;
TPlatformPicostrain  PlatformPicostrain;
TPlatformDetection   PlatformDetection;
TPlatformAcceptance  PlatformAcceptance;
TPlatformCalibration PlatformCalibration;

const TPlatformSigmaDelta PlatformSigmaDeltaDefault = {
   /* Rate */      PLATFORM_SIGMA_DELTA_RATE_DEFAULT,
   /* Prefilter */ PLATFORM_SIGMA_DELTA_PREFILTER_DEFAULT,
   /* Filter */    PLATFORM_SIGMA_DELTA_FILTER_SINC4,
   /* Chop */      NO,
   /* _Dummy */    0
};

const TPlatformPicostrain PlatformPicostrainDefault = {
   /* Rate */           PLATFORM_PICOSTRAIN_RATE_DEFAULT,
   /* Prefilter */      PLATFORM_PICOSTRAIN_PREFILTER_DEFAULT,
   /* Filter */         PLATFORM_PICOSTRAIN_FILTER_DEFAULT,
   /* Mode */           PLATFORM_PICOSTRAIN_MODE_DEFAULT,
   /* AccuracySwitch */ PLATFORM_PICOSTRAIN_ACCURACY_SWITCH_DEFAULT,
   /* _Dummy1 */        0,
   /* AccuracyFine */   PLATFORM_PICOSTRAIN_ACCURACY_FINE_DEFAULT,
   /* AccuracyCoarse */ PLATFORM_PICOSTRAIN_ACCURACY_COARSE_DEFAULT,
   /* _Dummy2 */        0
};

const TPlatformCalibration PlatformCalibrationDefault = {
   /* Points */    PLATFORM_CALIBRATION_POINTS_DEFAULT,
   /* Weight    */ {0, 10000, 20000, 30000, 40000},
   /* Delay */     PLATFORM_CALIBRATION_DELAY_DEFAULT,
   /* Duration */  PLATFORM_CALIBRATION_DURATION_DEFAULT
};
