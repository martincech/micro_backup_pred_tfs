//******************************************************************************
//
//   Diagnostic.c   Diagnostic
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Diagnostic.h"
#include "DiagnosticFifo.h"
#include "Weighing/Memory.h"

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void DiagnosticInit( void)
// Initialize
{
} // DiagnosticInit

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void DiagnosticStart( void)
// Start weighing
{
   DiagnosticFifoInit();
   MemoryDiagnosticStart();
} // DiagnosticStart

//------------------------------------------------------------------------------
//  Append
//------------------------------------------------------------------------------

void DiagnosticStop( void)
// Stop weighing
{
   MemoryDiagnosticStop();
} // DiagnosticStop

//------------------------------------------------------------------------------
//  Read
//------------------------------------------------------------------------------

TYesNo DiagnosticRead( TDiagnosticSample *Sample)
// Read sample
{
dword Overrun = DiagnosticFifoOverrunReset();
   if(Overrun) {
      Sample->Type = DIAGNOSTIC_LOST_SAMPLES;
      Sample->LostSamples = Overrun;
      return YES;
   }
   return DiagnosticFifoGet( Sample);
} // DiagnosticRead

//------------------------------------------------------------------------------
//  Save data
//------------------------------------------------------------------------------

void DiagnosticAppend( TDiagnosticFrame *Frame)
// Save data
{
static dword LastTimestamp;
TDiagnosticSample Sample;
dword FifoCount;
int i;

   if(Frame->LostSamples) {
      Sample.Type = DIAGNOSTIC_LOST_SAMPLES;
      Sample.LostSamples = Frame->LostSamples;
      DiagnosticFifoPut(Sample);
   }

   if(LastTimestamp != Frame->Timestamp) {
      Sample.Type = DIAGNOSTIC_TIMESTAMP;
      Sample.Timestamp = Frame->Timestamp;
      DiagnosticFifoPut(Sample);
      LastTimestamp = Sample.Timestamp;
   }

   for(i = 0 ; i < Frame->Count ; i++) {
      FifoCount = DiagnosticFifoCount();
      if(FifoCount >= DIAGNOSTIC_FIFO_SIZE - 2) { // -2 = room for lost and timestamp sample
         MemoryDiagnosticWrite();
         FifoCount = DiagnosticFifoCount();
         if(FifoCount != 0) {
            DiagnosticFifoInit();
            MemoryDiagnosticStart();
            // fatal error
         } else {
            DiagnosticFifoInit();
         }
      }

      Sample.Type = DIAGNOSTIC_WEIGHT;
      Sample.Weight = DiagnosticWeightDecompose(Frame->Weight[i]);
      DiagnosticFifoPut(Sample);
   }
} // DiagnosticAppend

//*****************************************************************************