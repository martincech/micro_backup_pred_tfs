//******************************************************************************
//
//   Diagnostic.h   Diagnostic
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Diagnostic_H__
   #define __Diagnostic_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __DiagnosticDef_H__
   #include "Diagnostic/DiagnosticDef.h"
#endif

typedef enum {
   DIAGNOSTIC_WEIGHT,
   DIAGNOSTIC_TIMESTAMP,
   DIAGNOSTIC_LOST_SAMPLES
} EDiagnosticSampleType;

#ifdef __WIN32__
   #pragma pack( push, 1)                   // byte alignment
#endif

typedef struct {
   byte Type;
   union {
      TWeightGauge Weight;
      dword Timestamp;
      dword LostSamples;
   };
} __packed TDiagnosticSample;

#ifdef __WIN32__
   #pragma pack( pop)                       // original alignment
#endif

#define DIAGNOSTIC_SAMPLE_SIZE         sizeof(TDiagnosticSample)

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void DiagnosticInit( void);
// Initialize

void DiagnosticStart( void);
// Start weighing

void DiagnosticStop( void);
// Stop weighing

TYesNo DiagnosticRead(TDiagnosticSample *Sample);
// Read sample

void DiagnosticAppend( TDiagnosticFrame *Frame);
// Save data

#endif
