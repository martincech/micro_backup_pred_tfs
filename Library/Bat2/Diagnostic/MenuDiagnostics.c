//******************************************************************************
//                                                                            
//   MenuUsbLoadConfiguration.c   Load configuration from USB
//   Version 1.0                  (c) VEIT Electronics
//
//******************************************************************************

#include "MenuUsbLoadConfiguration.h"

#include "System/System.h"        // Operating system
#include "Graphic/Graphic.h"      // graphic
#include "Gadget/DMsg.h"          // Message box
#include "Str.h"                  // Strings

#include "File/Efs.h"
#include "Memory/Nvm.h"
#include "Memory/NvmLayout.h"
#include "Data/uStorage.h"

#if NVM_CONFIG_SIZE > NVM_DIAGNOSTIC_SIZE
   #error Diagnostic area used to backup config
#endif

#define CONFIGURATION_FILE       "Nvm.bin"
#define FRAGMENT_SIZE            4096 // copy size

#define STR_USB_NOT_PRESENT_INCOMPATIBLE     "Drive not present/incompatible"
#define STR_USB_CONFIG_LOADED                "Configuration loaded"
#define STR_USB_CONFIG_NONEXISTENT           "File doesn't exist"
#define STR_USB_CONFIG_CORRUPTED_BAD_VERSION "Corrupted or bad version"
#define STR_USB_ERROR                        "USB error"

//------------------------------------------------------------------------------
//   Main loop
//------------------------------------------------------------------------------

void MenuUsbLoadConfiguration( void)
// Calibration menu
{
byte Fragment[FRAGMENT_SIZE];
dword Size;

   DMsgWait();
   if(!EfsInit()) {
      DMsgOk( STR_USB, STR_USB_NOT_PRESENT_INCOMPATIBLE, 0);
      return;
   }

   if(!EfsFileOpen( CONFIGURATION_FILE)) {
      EfsSwitchOff();
      DMsgOk( STR_USB, STR_USB_CONFIG_NONEXISTENT, 0);
      return;
   }
   // Seek to config position in file
   if(!EfsFileSeek( NVM_CONFIG_START, EFS_SEEK_START)) {
      EfsFileClose();
      EfsSwitchOff();
      DMsgOk( STR_USB, STR_USB_ERROR, 0);
      return;
   }
   // Load config with backup
   for(Size = 0; Size < NVM_CONFIG_SIZE; Size += FRAGMENT_SIZE) {
      if(EfsFileRead( Fragment, FRAGMENT_SIZE) != FRAGMENT_SIZE) {
         EfsFileClose();
         EfsSwitchOff();
         NvmMove( NVM_CONFIG_START, NVM_DIAGNOSTIC_START, Size); // restore
         DMsgOk( STR_USB, STR_USB_ERROR, 0);
         return;
      }

      NvmMove( NVM_DIAGNOSTIC_START + Size, NVM_CONFIG_START + Size, FRAGMENT_SIZE); // backup
      NvmSave( NVM_CONFIG_START + Size, Fragment, FRAGMENT_SIZE);

      switch(SysSchedulerBasic()) {
         case K_SHUTDOWN:
            EfsFileClose();
            EfsSwitchOff();
            NvmMove( NVM_CONFIG_START, NVM_DIAGNOSTIC_START, Size); // restore
            return;

         default:
            break;
      }
   }
   EfsFileClose();
   EfsSwitchOff();
   // Check
   if(!uStorageCheck()) {
      NvmMove( NVM_CONFIG_START, NVM_DIAGNOSTIC_START, NVM_CONFIG_SIZE); // restore
      DMsgOk( STR_USB, STR_USB_CONFIG_CORRUPTED_BAD_VERSION, 0);
      return;
   }
   uStorageLoad();
   DMsgOk( STR_USB, STR_USB_CONFIG_LOADED, 0);
} // MenuUsbLoadConfiguration
