//******************************************************************************
//
//   pDiagnosticFifo.c     Diagnostic Samples FIFO
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#include "DiagnosticFifo.h"
#include "Memory/Nvm.h"
#include "Config/Bat2Def.h"

static dword _ReadIndex;
static dword _WriteIndex;
static dword _Overrun;

#define FifoFull()    ((_ReadIndex + DIAGNOSTIC_FIFO_SIZE) == _WriteIndex)
#define FifoDelete()   (_ReadIndex++)
#define FifoPush( w)     //NvmSave(DIAGNOSTIC_DATA_ADDRESS + (_WriteIndex++ & (DIAGNOSTIC_FIFO_SIZE - 1)) * DIAGNOSTIC_SAMPLE_SIZE, &w, DIAGNOSTIC_SAMPLE_SIZE)
#define FifoPop( w)      //NvmLoad(DIAGNOSTIC_DATA_ADDRESS +  (_ReadIndex++ & (DIAGNOSTIC_FIFO_SIZE - 1)) * DIAGNOSTIC_SAMPLE_SIZE,  w, DIAGNOSTIC_SAMPLE_SIZE)
#define FifoFlush( c)   _ReadIndex += c

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

void DiagnosticFifoInit( void)
// Initialize
{
   _ReadIndex  = 0;
   _WriteIndex = 0;
   _Overrun = 0;
} // FifoInit

//------------------------------------------------------------------------------
// Put
//------------------------------------------------------------------------------

void DiagnosticFifoPut( TDiagnosticSample Weight)
// Save <Weight>
{
   if( FifoFull()){
      FifoDelete();                    // remove oldest item
      _Overrun++;
   }
   FifoPush( Weight);
} // FifoPut

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

dword DiagnosticFifoCount( void)
// Returns samples count
{
   return( _WriteIndex - _ReadIndex);
} // FifoCount

//------------------------------------------------------------------------------
// Overrun
//------------------------------------------------------------------------------

dword DiagnosticFifoOverrunReset( void)
// Reset and return lost samples
{
dword RetOverrun;
   RetOverrun = _Overrun;
   _Overrun = 0;
   return( RetOverrun);
} // DiagnosticFifoOverrunReset

//------------------------------------------------------------------------------
// Get
//------------------------------------------------------------------------------

TYesNo DiagnosticFifoGet( TDiagnosticSample *Weight)
// Returns <Weight> at <Index>, removes item from Fifo
{
   if(!DiagnosticFifoCount()) {
      return NO;
   }
   FifoPop(Weight);
   return( YES);
} // FifoGet
