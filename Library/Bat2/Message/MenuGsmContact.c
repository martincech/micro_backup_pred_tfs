//******************************************************************************
//
//   MenuGsmContact.c  GSM contact menu
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmContact.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Gadget/DInput.h"        // Input string
#include "Str.h"                  // Strings
#include "Gsm/GsmDef.h"           // GSM module services

#include "Message/GsmMessage.h"

static DefMenu( GsmContactMenu)
   STR_NAME,
   STR_PHONE_NUMBER,
   STR_SMS_FORMAT,
   STR_STATISTICS,
   STR_SEND_HISTOGRAM,
   STR_COMMANDS,
   STR_EVENTS,
EndMenu()

typedef enum {
   MI_NAME,
   MI_PHONE_NUMBER,
   MI_SMS_FORMAT,
   MI_STATISTICS,
   MI_SEND_HISTOGRAM,
   MI_COMMANDS,
   MI_EVENTS
} EGsmContactMenu;

// Local functions :

static void GsmContactParameters( int Index, int y, TGsmContact *Parameters);
// Draw gsm contact parameters

//------------------------------------------------------------------------------
//  Menu GsmContact
//------------------------------------------------------------------------------

void MenuGsmContact( TGsmContact *Contact)
// Edit gsm contact parameters
{
TMenuData MData;
int       i, j;
char      Name[ GSM_CONTACT_NAME_SIZE + 1];
char      PhoneNumber[ GSM_PHONE_NUMBER_SIZE];


   DMenuClear( MData);
   forever {
      //>>> item enable
      MData.Mask = (1 << MI_SEND_HISTOGRAM);
      if( Contact->SmsFormat == GSM_SMS_FORMAT_PC_ONLY){
         MData.Mask = (1 << MI_COMMANDS) | (1 << MI_EVENTS);
      }
      if( !Contact->Statistics){
         MData.Mask |= (1 << MI_SEND_HISTOGRAM);
      }
      //<<< item enable
      // selection :
      if( !DMenu( STR_CONTACT, GsmContactMenu, (TMenuItemCb *)GsmContactParameters, Contact, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_NAME :
            strcpy( Name, Contact->Name);
            if( !DInputText( STR_NAME, STR_ENTER_NAME, Name, GSM_CONTACT_NAME_SIZE, YES)){
               break;
            }
            strcpy( Contact->Name, Name);
            break;

         case MI_PHONE_NUMBER :
            strcpy( PhoneNumber, Contact->PhoneNumber+1);
            if( !DInputTextL( STR_PHONE_NUMBER, STR_ENTER_PHONE_NUMBER, PhoneNumber, GSM_PHONE_NUMBER_SIZE-1, NO, " 0123456789")){
               break;
            }
            PhoneNumber[GSM_PHONE_NUMBER_SIZE-1] = '\0'; //add trailing zero
            //remove spaces
            i = 0;
            while( i < strlen(PhoneNumber)){
               if(PhoneNumber[i] == ' '){
                  for(j = i+1; j < strlen(PhoneNumber); j++){
                     PhoneNumber[j-1] = PhoneNumber[j];
                  }
                  PhoneNumber[j-1] = '\0';
                  continue;
               }
               i++;
            }
            strcpy( Contact->PhoneNumber+1, PhoneNumber);
            break;

         case MI_STATISTICS :
            i = Contact->Statistics;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Contact->Statistics = (byte)i;
            break;

         case MI_COMMANDS :
            i = Contact->Commands;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Contact->Commands = (byte)i;
            break;

         case MI_EVENTS :
            i = Contact->Events;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Contact->Events = (byte)i;
            break;
         case MI_SMS_FORMAT :
            i = Contact->SmsFormat;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_GSM_SMS_FORMAT, _GSM_SMS_FORMAT_LAST)){
               break;
            }
            Contact->SmsFormat = (byte)i;
            break;

         case MI_SEND_HISTOGRAM :
            i = Contact->SendHistogram;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Contact->SendHistogram = (byte)i;
            break;

        /* case MI_TEST_SMS :
            i = Contact->SendHistogram;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Contact->SendHistogram = (byte)i;
            break;*/
      }
   }
} // MenuGsmContact

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void GsmContactParameters( int Index, int y, TGsmContact *Parameters)
// Draw gsm contact parameters
{
   switch( Index){
      case MI_NAME :
         DLabelNarrow( Parameters->Name, DMENU_PARAMETERS_X, y);
         break;

      case MI_PHONE_NUMBER :
         DLabelNarrow( Parameters->PhoneNumber, DMENU_PARAMETERS_X, y);
         break;

      case MI_STATISTICS :
         DLabelEnum( Parameters->Statistics, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_COMMANDS :
         DLabelEnum( Parameters->Commands, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_EVENTS :
         DLabelEnum( Parameters->Events, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_SMS_FORMAT :
         DLabelEnum( Parameters->SmsFormat, ENUM_GSM_SMS_FORMAT, DMENU_PARAMETERS_X, y);
         break;

      case MI_SEND_HISTOGRAM :
         DLabelEnum( Parameters->SendHistogram, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

   }
} // GsmContactParameters
