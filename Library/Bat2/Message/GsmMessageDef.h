//******************************************************************************
//
//   GsmMessageDef.h GSM message data
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GsmMessageDef_H__
   #ifndef _MANAGED
      #define __GsmMessageDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

#ifndef __uListDef_H__
   #include "Data/uListDef.h"
#endif

#ifndef _GsmDef_H__
   #include "Gsm/GsmDef.h"
#endif

#ifndef __LogDef_H__
   #include "Log/LogDef.h"
#endif
//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------
#ifndef _MANAGED
#define GSM_CONTACT_NAME_SIZE    GSM_PHONE_NUMBER_SIZE
#define GSM_CONTACT_NAME_DEFAULT ""

#define GSM_CONTACT_PHONE_NUMBER_SIZE    GSM_PHONE_NUMBER_SIZE
#define GSM_CONTACT_PHONE_NUMBER_DEFAULT "+"

#define GSM_COMMANDS_EXPIRATION_MIN 1
#define GSM_COMMANDS_EXPIRATION_MAX 99
#define GSM_COMMANDS_EXPIRATION_DEFAULT 10

#define GSM_EVENTS_WEIGHT_GREATER_MIN 0
#define GSM_EVENTS_WEIGHT_GREATER_MAX 0
#define GSM_EVENTS_WEIGHT_GREATER_DEFAULT 0

#define GSM_EVENTS_WEIGHT_COUNT_LESS_MIN 0
#define GSM_EVENTS_WEIGHT_COUNT_LESS_MAX 1000
#define GSM_EVENTS_WEIGHT_COUNT_LESS_DEFAULT 0

#define GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MIN 2
#define GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MAX 60
#define GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_DEFAULT 10

#define GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MIN 1
#define GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MAX 60
#define GSM_POWER_OPTIONS_SWITCH_ON_DURATION_DEFAULT 2

#define GSM_POWER_OPTIONS_SWITCH_ON_TIME_MIN 0
#define GSM_POWER_OPTIONS_SWITCH_ON_TIME_MAX 0
#define GSM_POWER_OPTIONS_SWITCH_ON_TIME_DEFAULT

//------------------------------------------------------------------------------
//  GSM contact
//------------------------------------------------------------------------------

typedef struct {
   char Name[ GSM_CONTACT_NAME_SIZE + 1];         // Contact name
   char PhoneNumber[ GSM_PHONE_NUMBER_SIZE + 1];  // Phone number
   byte SmsFormat;                                // SMS format
   byte Statistics;                               // Send daily statistics
   byte Commands;                                 // Accept commands
   byte Events;                                   // Send events
   byte SendHistogram;                            // Send histogram also
} TGsmContact;

typedef UListIndex TContactIndex;                 // Contact list index

#define CONTACT_INDEX_INVALID  ULIST_INDEX_INVALID

#endif

//------------------------------------------------------------------------------
//  Gsm channel mode
//------------------------------------------------------------------------------

#ifdef _MANAGED
namespace Bat2Library{
   public enum class GsmChannelModeE{
#else
typedef enum {
#endif
   GSM_CHANNEL_MODE_OFF,
   GSM_CHANNEL_MODE_ON,
#ifndef _MANAGED
   _GSM_CHANNEL_MODE_LAST
} EGsmChannelMode;
#else
   };
}
#endif

//------------------------------------------------------------------------------
//  GSM statistics
//------------------------------------------------------------------------------

#ifdef _MANAGED
namespace Bat2Library{
   public enum class GsmSmsFormatE{
#else
typedef enum {
#endif
   GSM_SMS_FORMAT_MOBILE_PHONE,
   GSM_SMS_FORMAT_PC_ONLY,
#ifndef _MANAGED
   _GSM_SMS_FORMAT_LAST
} EGsmSmsFormat;
#else
   };
}
#endif

#ifndef _MANAGED

//------------------------------------------------------------------------------
//  GSM commands
//------------------------------------------------------------------------------

typedef struct {
   byte Enabled;                            // Enable commands processing
   byte CheckPhoneNumber;                   // Check originator phone number
   word Expiration;                         // Command expiration time
} TGsmCommands;

#endif

//------------------------------------------------------------------------------
//  GSM events - flagged enum (bitfields)
//------------------------------------------------------------------------------
#ifdef _MANAGED
namespace Bat2Library{
   using namespace System;
   [Flags]
   public enum class GsmEventMaskE{
#else
typedef enum {
#endif
   GSM_EVENT_STATUS_CHANGE    = 0x0001,
   GSM_EVENT_DEVICE_FAILURE   = 0x0002
#ifndef _MANAGED
} EGsmEventMasks;
#else
   };
}
#endif

#ifndef _MANAGED
typedef struct {
   dword    EventMask;
} TGsmEvents;

////------------------------------------------------------------------------------
////  GSM channels
////------------------------------------------------------------------------------

//typedef struct {
//   byte         Enabled;                    // Enable channels processing
//   byte         CheckPhoneNumber;           // Check originator phone number
//} TGsmSmsDataTransfer;


#endif

//------------------------------------------------------------------------------
//  GSM power options
//------------------------------------------------------------------------------

#ifdef _MANAGED
namespace Bat2Library{
   public enum class GsmPowerModeE{
#else
typedef enum {
#endif
   GSM_POWER_MODE_OFF,
   GSM_POWER_MODE_ON,
   GSM_POWER_MODE_PERIODIC,
   GSM_POWER_MODE_TIME_PLAN,
#ifndef _MANAGED
   _GSM_POWER_MODE_LAST
} EGsmPowerMode;
#else
   };
}
#endif

#ifndef _MANAGED
#define GSM_POWER_OPTION_TIME_COUNT 10

typedef struct {
   byte        Mode;                               // GSM power on mode
   byte        SwitchOnPeriod;                     // GSM power period
   byte        SwitchOnDuration;                   // GSM power on duration
   TTimeRange  Times[GSM_POWER_OPTION_TIME_COUNT];
   byte        TimesCount;
} TGsmPowerOptions;

//------------------------------------------------------------------------------
//  GSM message
//------------------------------------------------------------------------------

typedef struct {
   TGsmCommands        Commands;
   TGsmEvents          Events;
   TGsmPowerOptions    PowerOptions;
} TGsmMessage;

//------------------------------------------------------------------------------

#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "GsmMessageDef.h"
   #define _MANAGED
   namespace Bat2Library{
      public ref class GsmMessageC abstract sealed{
      public:
         /// <summary>
         /// Maximum contact name length
         /// </summary>
         literal int CONTACT_NAME_SIZE = GSM_CONTACT_NAME_SIZE;
         /// <summary>
         /// Maximum contact phone number name length
         /// </summary>
         literal int CONTACT_PHONE_NUMBER_SIZE = GSM_CONTACT_PHONE_NUMBER_SIZE;

         /// <summary>
         /// Command expiration (in mins) - minimum
         /// </summary>
         literal int COMMANDS_EXPIRATION_MIN  = GSM_COMMANDS_EXPIRATION_MIN;
         /// <summary>
         /// Command expiration (in mins) - maximum
         /// </summary>
         literal int COMMANDS_EXPIRATION_MAX = GSM_COMMANDS_EXPIRATION_MAX;
         literal int COMMANDS_EXPIRATION_DEFAULT = GSM_COMMANDS_EXPIRATION_DEFAULT;

         literal int EVENTS_WEIGHT_GREATER_DEFAULT  = GSM_EVENTS_WEIGHT_GREATER_DEFAULT;

         /// <summary>
         /// Event weight count less minimum - unit set by configuration
         /// </summary>
         literal int EVENTS_WEIGHT_COUNT_LESS_MIN  = GSM_EVENTS_WEIGHT_COUNT_LESS_MIN;
         literal int EVENTS_WEIGHT_COUNT_LESS_MAX = GSM_EVENTS_WEIGHT_COUNT_LESS_MAX;
         literal int EVENTS_WEIGHT_COUNT_LESS_DEFAULT = GSM_EVENTS_WEIGHT_COUNT_LESS_DEFAULT;

         /// <summary>
         /// GSM switch on period (in mins)
         /// </summary>
         literal int POWER_OPTIONS_SWITCH_ON_PERIOD_MIN   = GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MIN;
         literal int POWER_OPTIONS_SWITCH_ON_PERIOD_MAX = GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MAX;
         literal int POWER_OPTIONS_SWITCH_ON_PERIOD_DEFAULT = GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_DEFAULT;

         /// <summary>
         /// GSM switch on duration (in mins)
         /// </summary>
         literal int POWER_OPTIONS_SWITCH_ON_DURATION_MIN   = GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MIN;
         literal int POWER_OPTIONS_SWITCH_ON_DURATION_MAX = GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MAX;
         literal int POWER_OPTIONS_SWITCH_ON_DURATION_DEFAULT = GSM_POWER_OPTIONS_SWITCH_ON_DURATION_DEFAULT;
      };
   }
#endif
#endif
