//******************************************************************************
//
//   MenuGsm.c     GSM menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsm.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DLabel.h"
#include "Gadget/DEdit.h"
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "MenuGsmPowerOptions.h"
#include "MenuGsmContacts.h"
#include "MenuGsmCommands.h"
#include "MenuGsmEvents.h"
#include "MenuGsmInfo.h"
#include "MenuGsmPinCode.h"

#include "Message/GsmMessage.h"

static DefMenu( GsmMenu)
   STR_POWER_OPTIONS,
   STR_CONTACTS,
   STR_COMMANDS,
   STR_EVENTS,
   STR_INFO,
   STR_PIN_CODE,
EndMenu()

typedef enum {
   MI_POWER_OPTIONS,
   MI_CONTACTS,
   MI_COMMANDS,
   MI_EVENTS,
   MI_INFO,
   MI_PIN_CODE
} EGsmMenu;

// Local functions :
static void MenuGsmParameters( int Index, int y, void *udata);
// Draw Gsm parameters

//------------------------------------------------------------------------------
//  Menu Gsm
//------------------------------------------------------------------------------

void MenuGsm( void)
// Menu gsm
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      //>>> item enable
      //<<< item enable
      // selection :
      if( !DMenu( STR_GSM, GsmMenu, (TMenuItemCb *) MenuGsmParameters, 0, &MData)){
         ConfigGsmMessageSave();
         return;
      }
      switch( MData.Item){
         case MI_POWER_OPTIONS :
            MenuGsmPowerOptions();
            break;

         case MI_CONTACTS :
            MenuGsmContacts();
            break;

         case MI_COMMANDS :
            MenuGsmCommands();
            break;

         case MI_EVENTS :
            MenuGsmEvents();
            break;

         case MI_INFO :
            MenuGsmInfo();
            break;

         case MI_PIN_CODE :
            MenuGsmPinCode();
            break;

      }
   }
} // MenuGsm

static void MenuGsmParameters( int Index, int y, void *udata)
// Draw Gsm parameters
{
   switch( Index){
      case MI_POWER_OPTIONS :
         DLabelEnum( GsmMessage.PowerOptions.Mode, ENUM_GSM_POWER_MODE, DMENU_PARAMETERS_X, y);
         break;
      case MI_COMMANDS :
         DLabelEnum( GsmMessage.Commands.Enabled, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;
      case MI_EVENTS :
         DLabelEnum( GsmMessage.Events.EventMask ? YES:NO, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;
   }
}
