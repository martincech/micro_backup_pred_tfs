//******************************************************************************
//
//   MenuGsm.h     GSM menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsm_H__
   #define __MenuGsm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuGsm( void);
// Menu gsm

#endif
