//******************************************************************************
//
//   MenuGsmPowerOptionsTimes.h     Menu for GSM power options time plan settings
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmPowerOptionsTimes_H__
#define __MenuGsmPowerOptionsTimes_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#include "GsmMessageDef.h"

void MenuGsmPowerOptionsTimes( TGsmPowerOptions *PowerOpt);
// Menu weighing times


#endif // __MenuGsmPowerOptionsTimes_H__


