//******************************************************************************
//
//   Message.c      GSM messaging
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Message.h"
#include "Console/conio.h"
#include "System/System.h"
#include "Parse/uParse.h"
#include "Weight/Weight.h"                  // Weight functions
#include "Scheduler/WeighingScheduler.h"    // Weighing timing
#include "Weighing/WeighingConfiguration.h" // Weighing configuration
#include "Weighing/Statistics.h"            // Weighing statistics
#include "Message/ContactList.h"            // Contacts
#include "Message/GsmMessage.h"             // GSM messaging configuration
#include "Platform/Zone.h"                  // Weighing zone / actual statistics only
#include "Config/Config.h"                  // Bat2Device only
#include "Country/bTime.h"                  // Print Date/Time to buffer
#include "Archive/Archive.h"                // Archive data

#include "Megavi/MegaviTask.h"              // MEgavi services
#include "SmsGate/SmsGate.h"                // Sms gate services
#include "Message/MGsm.h"                   // Sms read service
#include "Str.h"
#include "Communication/Communication.h"
#include "Communication/InternetPublication.h"
#include <string.h>
#include "Timer/Timer.h"                   // Systimer
#include "SensorPack/Sensors/Sensors.h"

// contact iteration :
#define _ContactStatisticStop()         CommunicationContext.StatisticContact = CONTACT_INDEX_INVALID
#define _ContactStatisticDone()        (CommunicationContext.StatisticContact == CONTACT_INDEX_INVALID)
#define _HasDataToSend()               (CommunicationContext.SendDay != DAY_NUMBER_INVALID)

#define _GsmEnabled()       (GsmMessage.PowerOptions.Mode != GSM_POWER_MODE_OFF)

#define INET_SEND_TIMEOUT         120  // s
#define INET_SEND_TIMEOUT_INVALID -1
#define _InetSendTimeout()        (!_InetSendTimeoutRunning() || TimeAfter(SysTime(), CommunicationContext.IPublishStartTime))
#define _InetSendTimeoutSet()     (CommunicationContext.IPublishStartTime = SysTime() + INET_SEND_TIMEOUT)
#define _InetSendTimeoutReset()   (CommunicationContext.IPublishStartTime  = INET_SEND_TIMEOUT_INVALID)
#define _InetSendTimeoutRunning() (CommunicationContext.IPublishStartTime  != INET_SEND_TIMEOUT_INVALID)

// Local functions :
extern char *strupr( char *s);
// String to upper case - defined elsewhere

static TYesNo _CommandExecute( void);
// Execute SMS command

static TYesNo _CommandExecuteStat( char *PhoneNumber, const char *Text);
// Execute STAT command

static TYesNo _CommandExecuteMegavi( char *PhoneNumber, const char *Text);
// Execute MEGAVI command

static TYesNo _StatisticExecute( void);
// Execute daily statistics through SMS

//static TYesNo _IStatisticExecute( void);
//// Execute daily statistics through internet

static void _StatisticSendStop( void);
// Stop send current statistic through sms

//static void _IStatisticSendStop( void);
//// Stop send current statistic through internet

static void _StatisticCloseWait( void);
// Wait for a new statistic

static void _StatisticDayClose( UClockGauge SendAt);
// Close a day and schedule <SendAt> time

static void _StatisticSendSchedule( TDayNumber Day);
// Schedule statistic send of <Day>

static UClockGauge _StatisticSendAt( UClockGauge Clock);
// Calculate statistic send time by <Clock>

static TYesNo _EventExecute( void);
// Execute events

static TYesNo _SendWeighingStatus( char *PhoneNumber);
// Send weighing status

static TYesNo _SendWrongDay( char *PhoneNumber, TDayNumber Day);
// Send wrong <Day> message

static TYesNo _SendStatisticNow( char *PhoneNumber);
// Send actual statistic

static TYesNo _SendStatisticDay( char *PhoneNumber, TDayNumber Day);
// Send statistic of <Day>

static TYesNo _SendStatisticIndex(char *PhoneNumber, TArchiveIndex Index, EGsmSmsFormat smsFormat);
// Send statistic by marker <Index>

static TYesNo _SendStatistic( char *PhoneNumber, TDayNumber Day, TStatistic *StatisticCalculate, TStatistic *StatisticFemale);
// Send <Statistic>/<StatisticFemale>

static TYesNo _SendStatisticNewFormat( char *PhoneNumber, TDayNumber Day, TStatistic *Statistic, TStatistic *StatisticFemale);
// Send <Statistic>/<StatisticFemale> by new data format together with sensor data

//static TYesNo _SendStatisticBinary( char *PhoneNumber, TDayNumber Day, TStatistic *Statistic, TStatistic *StatisticFemale);
//// Send binary formated <Statistic>/<StatisticFemale>

static TYesNo _SendEvent( TLogItem *LogItem);
// Send event by log code to current event contact

static TYesNo _PhoneNumberCheck( char *Number);
// Check for command accept

static TYesNo _ContactStatisticFirst( void);
// Check for first statistic contact

static TYesNo _ContactStatisticNext( void);
// Check for next statistic contact

static TYesNo _ContactEventNext( void);
// Check for next event contact

static TYesNo _ContactStatisticFind( TContactIndex From);
// Check for statistic contact starting from <From>

static TYesNo _SendSms( void);
// Send Sms

//static TYesNo _SendBinarySms( int32 size);
//// Send binary SMS

//------------------------------------------------------------------------------
//  Message compose
//------------------------------------------------------------------------------

#define DATE_TEXT_SIZE 12

void MessageCompose(char *Buffer, TDayNumber Day, TStatistic *Statistic, TStatistic *StatisticFemale)
// Message compose both
{
   char           DateText[ DATE_TEXT_SIZE + 1];
   UDateTimeGauge DateTime;

      DateTime = WeighingDayStartDateTime( Day);
      bDate( DateText, uDateTimeDate( DateTime));
      if(StatisticFemale == NULL){
         bprintf( Buffer, "SCALE %s DAY %03d %s Cnt %04d Avg %6.3f Gain %6.3f Sig %5.3f Cv %03d Uni %03d",
               Bat2Device.Name,
               Day,
               DateText,
               // unisex/male :
               Statistic->Count,
               WeightNumber( Statistic->Average),
               WeightNumber( Statistic->Gain),
               WeightNumber( Statistic->Sigma),
               Statistic->Cv / 10,                    // convert to [1%]
               Statistic->Uniformity / 10);           // convert to [1%]
      } else {
         bprintf( Buffer, "SCALE %s DAY %03d %s FEMALES: Cnt %04d Avg %6.3f Gain %6.3f Sig %5.3f Cv %03d Uni %03d"
                          " MALES: Cnt %04d Avg %6.3f Gain %6.3f Sig %5.3f Cv %03d Uni %03d",
                  Bat2Device.Name,
                  Day,
                  DateText,
                  // female :
                  StatisticFemale->Count,
                  WeightNumber( StatisticFemale->Average),
                  WeightNumber( StatisticFemale->Gain),
                  WeightNumber( StatisticFemale->Sigma),
                  StatisticFemale->Cv / 10,              // convert to [1%]
                  StatisticFemale->Uniformity / 10,      // convert to [1%]
                  // male :
                  Statistic->Count,
                  WeightNumber( Statistic->Average),
                  WeightNumber( Statistic->Gain),
                  WeightNumber( Statistic->Sigma),
                  Statistic->Cv / 10,                    // convert to [1%]
                  Statistic->Uniformity / 10);           // convert to [1%]
      }

} // MessageCompose

//------------------------------------------------------------------------------
//  Message compose for new format
//------------------------------------------------------------------------------

void MessageComposeNewFormat(char *Buffer, TDayNumber Day, TStatistic *Statistic, TStatistic *StatisticFemale)
{
char           DateText[ DATE_TEXT_SIZE + 1];
UDateTimeGauge DateTime;
dword temp,co2,humidity;


   DateTime = WeighingDayStartDateTime( Day);
   temp = TemperatureAverageRead();
   co2 = CO2AverageRead();
   humidity = HumidityAverageRead();
   bDateFormated(DateText, uDateTimeDate( DateTime), DATE_FORMAT_MMDDYYYY, '/', '/');
   if(StatisticFemale == NULL){
      bprintf( Buffer,  "%08X %d %s "                     // header
                        "%d %.3f %.3f %.3f %.1f %.1f "     // males
                        "%.3f %d - %.3f",               // Sensors data
            ConfigurationSerialNumber(),
            Day,
            DateText,
            // male :
            Statistic->Count,
            WeightNumber( Statistic->Average),
            WeightNumber( Statistic->Gain),
            WeightNumber( Statistic->Sigma),
            Statistic->Cv,
            Statistic->Uniformity,
            // sensor data
            temp*1000,
            co2,
            humidity*1000);
   } else {
      bprintf( Buffer,  "%08X %d %s "                     // header
                        "%d %.3f %.3f %.3f %.1f %.1f "     // females
                        "%d %.3f %.3f %.3f %.1f %.1f "     // males
                        "%.3f %d - %.3f",               // Sensors data
            ConfigurationSerialNumber(),
            Day,
            DateText,
            // female :
            StatisticFemale->Count,
            WeightNumber( StatisticFemale->Average),
            WeightNumber( StatisticFemale->Gain),
            WeightNumber( StatisticFemale->Sigma),
            StatisticFemale->Cv,
            StatisticFemale->Uniformity,
            // male :
            Statistic->Count,
            WeightNumber( Statistic->Average),
            WeightNumber( Statistic->Gain),
            WeightNumber( Statistic->Sigma),
            Statistic->Cv,
            Statistic->Uniformity,
            // sensor data
            temp*1000,
            co2,
            humidity*1000);
   }
} // MessageComposeNewFormat

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void MessageInit( void)
// Initialize
{
   ContactListInit();
} // MessageInit

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void MessageExecute( void)
// Execute
{
static TYesNo LastGsmEnabled = NO;
//   if((DataPublication.Interface & DATA_PUBLICATION_INTERFACE_INTERNET) == DATA_PUBLICATION_INTERFACE_INTERNET){
//      _IStatisticExecute();
//   }
   if( !_GsmEnabled()) { // GSM disabled
      if(LastGsmEnabled) { // GSM switched off, flush
         LastGsmEnabled = NO;
         _StatisticSendStop();
      }
      return;
   }
   if( !LastGsmEnabled){
      CommunicationContext.EventContact = CONTACT_INDEX_INVALID;
      CommunicationContext.LogIndex = LogCount();
      CommunicationContext.LogTimestamp = 0;
      CommunicationContext.EventUnsend = 0;
      ContextCommunicationSave();
   }
   LastGsmEnabled = YES;
   if( (CommunicationContext.SendSmsSlot != SMS_GATE_SLOT_ERROR) &&
       (SmsGateSmsStatus( CommunicationContext.SendSmsSlot) == SMS_GATE_SMS_STATUS_PENDING)){
      // wait till previous sms send
      return;
   }
   if(CommunicationContext.SendSmsSlot != SMS_GATE_SLOT_ERROR){
      CommunicationContext.SendSmsSlot = SMS_GATE_SLOT_ERROR;
      ContextCommunicationSave();
   }

   // check for command
   if( _CommandExecute()){             // execute SMS commands
      return;
   }
   if((DataPublication.Interface & DATA_PUBLICATION_INTERFACE_SMS) == DATA_PUBLICATION_INTERFACE_SMS){
      // check for messaging :
      if( _StatisticExecute()){           // execute daily statistics
         _ContactStatisticNext();
         ContextCommunicationSave();
         return;
      }
   }
   // check for events
   if( _EventExecute()){
      ContextCommunicationSave();
      return;
   }
} // MessageExecute


//------------------------------------------------------------------------------
//  Resume
//------------------------------------------------------------------------------

void MessageResume( void)
// Resume weighing after power on
{
   if( CommunicationContext.SendTime == CLOCK_INVALID){
      return;                          // no data to send schedule
   }
   if( SysClock() < CommunicationContext.SendTime){
      return;                          // waiting for send
   }
   // send expiration, start again
   MessageStart();
} // MessageResume

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void MessageStart( void)
// Start weighing
{
   _StatisticSendStop();
//   _IStatisticSendStop();
   _StatisticCloseWait();
   ContextCommunicationSave();
} // MessageStart

//------------------------------------------------------------------------------
//  Next
//------------------------------------------------------------------------------

void MessageDayClose( void)
// Close day
{
UClockGauge SendAt;
   SendAt = _StatisticSendAt( WeighingDayCloseAt());
   _StatisticDayClose( SendAt);
   ContextCommunicationSave();
} // MessageDayClose

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void MessageStop( void)
// Stop weighing
{
   _StatisticSendStop();               // stop old send
   _StatisticDayClose( SysClock());    // send a new day immediately
   ContextCommunicationSave();
} // MessageStop

//******************************************************************************

//------------------------------------------------------------------------------
//   Command execute
//------------------------------------------------------------------------------

#define SMS_COMMAND_STAT        "STAT"
#define SMS_COMMAND_STAT_SIZE   sizeof( SMS_COMMAND_STAT)
#define SMS_COMMAND_MEGAVI      "MEGAVI"
#define SMS_COMMAND_MEGAVI_SIZE sizeof( SMS_COMMAND_MEGAVI)

static TYesNo _CommandExecute( void)
// Execute SMS command
{
char        PhoneNumber[ GSM_PHONE_NUMBER_SIZE + 1];
char        Text[ GSM_SMS_SIZE + 1];
char        TextCommand[ GSM_SMS_SIZE + 1]; // uprcase text - array length greatest from commands

UClockGauge ReceivedTime;

   if( CommunicationContext.CommandUnsend){
      if( _SendSms()){
         CommunicationContext.EventUnsend = CommunicationContext.CommandUnsend = 0;
         ContextCommunicationSave();
         return YES;
      }
      return NO;
   }
   // check for received messages :
   if( !MGsmReceive( PhoneNumber, Text, &ReceivedTime)){
      return( NO);                     // unable read SMS
   }

   // check for valid phone number :
   if( !_PhoneNumberCheck( PhoneNumber)){
      return( NO);                     // wrong SMS number or commands dissabled
   }

   if( (ReceivedTime + GsmMessage.Commands.Expiration * TIME_MIN) < SysClock()){
      return( NO);                     // message expiration
   }

   strncpy( TextCommand, Text, SMS_COMMAND_MEGAVI_SIZE); // copy command part of sms
   TextCommand[SMS_COMMAND_MEGAVI_SIZE] = '\0';          // set last index in case zero is missing
   strupr( TextCommand);                                 // convert it to upcase
   // check for STAT command :
   if( strnequ( TextCommand, SMS_COMMAND_STAT, SMS_COMMAND_STAT_SIZE)){
      return( _CommandExecuteStat( (char *)&PhoneNumber, (const char *)&Text));
   }
   // check for MEGAVI command
   if( strnequ( TextCommand, SMS_COMMAND_MEGAVI, SMS_COMMAND_MEGAVI_SIZE)){
      return( _CommandExecuteMegavi( (char *)&PhoneNumber, (const char *)&Text));
   }
   return( NO); // NO valid command
} // _CommandExecute

//------------------------------------------------------------------------------
//   STAT command execute
//------------------------------------------------------------------------------

static TYesNo _CommandExecuteStat( char *PhoneNumber, const char *Text)
// Execute STAT command
{
unsigned    Day;

   // check for weighing status :
   if( (WeighingStatus() != WEIGHING_STATUS_WEIGHING)){
      if( !_SendWeighingStatus( PhoneNumber)){
         CommunicationContext.CommandUnsend = 1;
         ContextCommunicationSave();
         return YES;
      }
      CommunicationContext.CommandUnsend = 0;
      return YES;
   }
   // check for requested day :
   if( strlen( Text) == SMS_COMMAND_STAT_SIZE){
      // actual day only
      if( !_SendStatisticNow( PhoneNumber)){
         CommunicationContext.CommandUnsend = 1;
         ContextCommunicationSave();
         return YES;
      }
      CommunicationContext.CommandUnsend = 0;
      return YES;
   }
   // stat command with a day
   uParseNumber( (const char *) &Text[ SMS_COMMAND_STAT_SIZE + 1], &Day);
   if( !_SendStatisticDay( PhoneNumber, Day)){
      CommunicationContext.CommandUnsend = 1;
      ContextCommunicationSave();
      return YES;
   }
   CommunicationContext.CommandUnsend = 0;
   return YES;
}// _CommandExecuteStat

//------------------------------------------------------------------------------
//   MEGAVI command execute
//------------------------------------------------------------------------------

static TYesNo _CommandExecuteMegavi( char *PhoneNumber, const char *Text)
// Execute MEGAVI command
{
byte Len;

   Len = strlen( Text);
   if( Len == SMS_COMMAND_MEGAVI_SIZE){
      MegaviTaskSetSmsRequest(PhoneNumber, MEGAVI_SMS_REQUEST_SHORT);
   } else {
      MegaviTaskSetSmsRequest(PhoneNumber, Text[Len-1]);
   }
   return YES;
}// _CommandExecuteMegavi

//------------------------------------------------------------------------------
//   Statistic execute
//------------------------------------------------------------------------------
//static TYesNo _IStatisticExecute( void)
//// Execute daily statistics through internet
//{
//TArchive Archive;

//   // check send time
//   if( CommunicationContext.SendTime != CLOCK_INVALID){
//      dword Now = SysClock();
//      if( Now < CommunicationContext.SendTime){
//         return NO;
//      }
//   }

//   if( CommunicationContext.ClosedIndex == ARCHIVE_INDEX_INVALID ||
//       (CommunicationContext.ISendDay != DAY_NUMBER_INVALID &&
//        CommunicationContext.ISendDay >= CommunicationContext.ClosedDay)){
//      // no data to send
//      return NO;
//   }

//   if( CommunicationContext.ISendDay == DAY_NUMBER_INVALID){
//      CommunicationContext.ISendDay = 0;
//   }

//   while((CommunicationContext.ISendDay <= CommunicationContext.ClosedDay) &&
//       CommunicationContext.ISendIndex == ARCHIVE_INDEX_INVALID){
//      // get index of archive item to send as next
//      while(!ArchiveOpen( &Archive));
//      CommunicationContext.ISendIndex = ArchiveSearch( &Archive, CommunicationContext.ISendDay);
//      ArchiveClose(&Archive);
//      CommunicationContext.ISendDay++;
//   }
//   // data successfully send or timetout
//   if(_SendStatisticIndexRoutine( NULL, CommunicationContext.ISendIndex)){
//      CommunicationContext.ISendIndex = ARCHIVE_INDEX_INVALID;
//      ContextCommunicationSave();
//      _InetSendTimeoutReset();
//      return YES;
//   } else if(!_InetSendTimeoutRunning()){
//      // timer not running, start it
//      _InetSendTimeoutSet();
//   } else if( _InetSendTimeout()){
//      // timer timeout out, not try to send this data anymore
//      CommunicationContext.ISendIndex = ARCHIVE_INDEX_INVALID;
//      _InetSendTimeoutReset();
//   }

//   ContextCommunicationSave();
//   return NO;
//}

static TYesNo _StatisticExecute( void)
// Execute daily statistics
{
char PhoneNumber[ GSM_PHONE_NUMBER_SIZE + 1];
TContactList ContactList;
TGsmContact  Contact;

   // check for a day closed :
   if( CommunicationContext.SendTime != CLOCK_INVALID){
      // check for send time expiration :
      dword Now = SysClock();
      if( Now >= CommunicationContext.SendTime){
         // prepare next statistics
         CommunicationContext.SendTime = CLOCK_INVALID;
         _StatisticSendSchedule( CommunicationContext.ClosedDay);
         _StatisticCloseWait();        // wait for a next closed day
         ContextCommunicationSave();
         return( NO);
      }
   }
   if( !_HasDataToSend()){
      return( NO);                     // no data for send
   }

   if( _ContactStatisticDone()){
      _StatisticSendStop();
      ContextCommunicationSave();
      return( NO);                     // all contacts served
   }

   // send current statistics at phone number :
   while(!ContactListOpen( &ContactList));
   ContactListPhoneNumber( &ContactList, PhoneNumber, CommunicationContext.StatisticContact);   // get actual contact number
   ContactListLoad( &ContactList, &Contact, CommunicationContext.StatisticContact);
   CommunicationContext.ContactType = Contact.SmsFormat;
   ContactListClose( &ContactList);

   return( _SendStatisticIndex( PhoneNumber, CommunicationContext.SendIndex, Contact.SmsFormat));   // send statistic data
} // _StatisticExecute

//------------------------------------------------------------------------------
//  Statistic stop
//------------------------------------------------------------------------------
//static void _IStatisticSendStop()
//{
//   CommunicationContext.ISendDay = DAY_NUMBER_INVALID;
//   CommunicationContext.ISendIndex = ARCHIVE_INDEX_INVALID;
//}

static void _StatisticSendStop( void)
// Stop send current statistic
{
   _ContactStatisticStop();                           // stop contact
   CommunicationContext.SendSmsSlot = SMS_GATE_SLOT_ERROR;
   CommunicationContext.SendIndex   = ARCHIVE_INDEX_INVALID;
   CommunicationContext.SendDay     = DAY_NUMBER_INVALID;
} // _StatisticSendStop

//------------------------------------------------------------------------------
//  Statistic wait
//------------------------------------------------------------------------------

static void _StatisticCloseWait( void)
// Wait for a new statistic
{
   CommunicationContext.SendTime    = CLOCK_INVALID;
   CommunicationContext.ClosedDay   = DAY_NUMBER_INVALID;
   CommunicationContext.ClosedIndex = ARCHIVE_INDEX_INVALID;
} // _StatisticCloseWait

//------------------------------------------------------------------------------
//   Statistic day close
//------------------------------------------------------------------------------

static void _StatisticDayClose( UClockGauge SendAt)
// Close a day and schedule <SendAt> time
{
   CommunicationContext.SendTime    = SendAt;              // start send after close time
   CommunicationContext.ClosedDay   = WeighingDay();       // closed day number
   CommunicationContext.ClosedIndex = StatisticsLastDay(); // closed day archive marker
} // _StatisticDayClose

//------------------------------------------------------------------------------
//   Statistic schedule
//------------------------------------------------------------------------------

#define _DayAccept( Day, From, Period)   ((((Day) - (From)) % (Period)) == 0)

static void _StatisticSendSchedule( TDayNumber Day)
// Schedule statistic send of <Day>
{
   _StatisticSendStop();               // stop old send
   if( Day == DAY_NUMBER_INVALID){
      return;                          // wrong day
   }
   // check for send start :
   if( Day < DataPublication.StartFromDay){
      return;                          // start later
   }
   if( Day < DataPublication.AccelerateFromDay){
      // slow send
      if( !_DayAccept( Day, DataPublication.StartFromDay, DataPublication.Period)){
         return;                       // out of send interval
      }
   } else {
      // fast send
      if( !_DayAccept( Day, DataPublication.AccelerateFromDay, DataPublication.AcceleratedPeriod)){
         return;                       // out of send interval
      }
   }

   if((DataPublication.Interface & DATA_PUBLICATION_INTERFACE_SMS) == DATA_PUBLICATION_INTERFACE_SMS){
      if(!_ContactStatisticFirst()) {
         return;
      }
   }
   // schedule send :
   CommunicationContext.SendDay   = CommunicationContext.ClosedDay;
   CommunicationContext.SendIndex = CommunicationContext.ClosedIndex;
} // _StatisticSendSchedule

//------------------------------------------------------------------------------
//  From time
//------------------------------------------------------------------------------

static UClockGauge _StatisticSendAt( UClockGauge Clock)
// Calculate statistic send time by <Clock>
{
UDateTimeGauge DateTime;
UDateGauge     Date;
UTimeGauge     Time;

   if( (WeighingDayDuration() > 0) &&
       (WeighingDayDuration() < TIME_DAY)){
      return( Clock + 1* TIME_SEC);    // simulated day, send now
   }
   DateTime = uClockDateTime( Clock);  // get wall clock time
   Date     = uDateTimeDate( DateTime);
   Time     = uDateTimeTime( DateTime);
   if( Time > DataPublication.SendAt){
      Date++;                          // tomorrow
   }
   DateTime = uDateTimeCompose( Date, DataPublication.SendAt);
   return( uDateTimeClock( DateTime));
} // _StatisticSendAt

//------------------------------------------------------------------------------
//   Event execute
//------------------------------------------------------------------------------
#include "Log/Log.h"
#include "Log/Logger.h"

static TYesNo _EventExecute( void)
// Execute events
{
TLogItem  item;
TLogIndex Count;

   Count = LogCount();
   if(CommunicationContext.LogIndex == LOG_INDEX_INVALID){
      if( Count > 0){
         CommunicationContext.LogIndex = 0;
      } else {
         return NO;
      }
   }

   // go through log
   for( ; CommunicationContext.LogIndex < Count; CommunicationContext.LogIndex++){
      if( !LogGet(CommunicationContext.LogIndex, &item)){
         CommunicationContext.LogIndex = 0;
         return NO;
      }
      if(item.Timestamp < CommunicationContext.LogTimestamp){
         return NO;
      }

      CommunicationContext.LogTimestamp = item.Timestamp;
      // check event request
      switch( LogModuleGet(item.Code)){
         case LOG_MODULE_WEIGHING_STATUS_CHANGED :
            if( !(GsmMessage.Events.EventMask & GSM_EVENT_STATUS_CHANGE)){
               continue;
            }
            break;

         case LOG_FAILURE :
            if( !(GsmMessage.Events.EventMask & GSM_EVENT_DEVICE_FAILURE)){
               continue;
            }
            break;

         default:
            continue;
      }
      // send sms
      if( !CommunicationContext.EventUnsend){
         if( !_ContactEventNext()){
            continue;
         }
      }
      CommunicationContext.EventUnsend = !_SendEvent( &item);
      return YES;
   }
   // log is circle buffer, go to begining when at end
   if( CommunicationContext.LogIndex == Count && LogFull()){
      CommunicationContext.LogIndex = 0;
   }
   return( NO);
} // _EventExecute

//------------------------------------------------------------------------------
//   Send status
//------------------------------------------------------------------------------

static TYesNo _SendWeighingStatus( char *PhoneNumber)
// Send weighing status
{
   bprintf( CommunicationContext.Message, "SCALE %s : %s", Bat2Device.Name, ENUM_WEIGHING_STATUS + WeighingStatus());
   strcpy( CommunicationContext.PhoneNumber, PhoneNumber);
   return( _SendSms());
} // _SendWeighingStatus

//------------------------------------------------------------------------------
//   Send wrong day
//------------------------------------------------------------------------------

static TYesNo _SendWrongDay( char *PhoneNumber, TDayNumber Day)
// Send wrong <Day> message
{
   bprintf( CommunicationContext.Message, "SCALE %s DAY %03d NO DATA", Bat2Device.Name, Day);
   strcpy( CommunicationContext.PhoneNumber, PhoneNumber);
   return( _SendSms());
} // _SendWrongDay

//------------------------------------------------------------------------------
//   Send actual statistics
//------------------------------------------------------------------------------

static TYesNo _SendStatisticNow( char *PhoneNumber)
// Send actual statistic
{
TStatistic *Statistic;
TStatistic *StatisticFemale;

   Statistic       = ZoneStatistic();
   StatisticFemale = ZoneStatisticFemale();
   return( _SendStatistic( PhoneNumber, WeighingDay(), Statistic, StatisticFemale));
} // _SendStatisticNow

//------------------------------------------------------------------------------
//   Send day statistics
//------------------------------------------------------------------------------

static TYesNo _SendStatisticDay( char *PhoneNumber, TDayNumber Day)
// Send statistic of <Day>
{
TArchiveIndex Index;
TArchive Archive;
TContactList ContactList;
EGsmSmsFormat format;

   // search for day marker :
   while(!ArchiveOpen( &Archive));
   Index = ArchiveSearch( &Archive, Day);
   ArchiveClose( &Archive);
   if( Index == ARCHIVE_INDEX_INVALID){
      // requested day not found
      return( _SendWrongDay( PhoneNumber, Day));
   }

   while(!ContactListOpen( &ContactList));
   format = ContactListSmsFormat( &ContactList, PhoneNumber);
   ContactListClose( &ContactList);
   if( !_SendStatisticIndex( PhoneNumber, Index, format)){
      return( _SendWrongDay( PhoneNumber, Day));
   }
   return( YES);
} // _SendStatisticDay

//------------------------------------------------------------------------------
//   Send statistics by index
//------------------------------------------------------------------------------

static TYesNo _SendStatisticIndex( char *PhoneNumber, TArchiveIndex Index, EGsmSmsFormat smsFormat)
// Send statistic by marker <Index>
{
TDayNumber    Day;
TArchiveItem  Item;
TWeightGauge  TargetWeight;
TWeightGauge  TargetWeightFemale;
TStatistic    Statistic;
TStatistic    StatisticFemale;
TArchive      Archive;

   while(!ArchiveOpen( &Archive));
   if( !ArchiveGet( &Archive, Index, &Item)){
      // archive access error
      ArchiveClose( &Archive);
      return( NO);
   }
   if( !ArchiveIsMarker( &Item)){
      // archive access error
      ArchiveClose( &Archive);
      return( NO);
   }
   // remember Targets :
   Day                = Item.Marker.Day;
   TargetWeight       = Item.Marker.TargetWeight;
   TargetWeightFemale = Item.Marker.TargetWeightFemale;
   memset( &Statistic, 0, sizeof( TStatistic));
   memset( &StatisticFemale, 0, sizeof( TStatistic));
   // get unisex/male data :
   Index++;
   if( ArchiveGet( &Archive, Index, &Item) && !ArchiveIsMarker( &Item)){
      // Non empty archive for this day
      ArchiveStatistic( &Statistic, &Item);
      Statistic.Target = TargetWeight;
      memset( &StatisticFemale, 0, sizeof( TStatistic));
      if( WeighingHasFemale()){
         // get female data :
         Index++;
         if( !ArchiveGet( &Archive, Index, &Item)){
            // archive access error
            ArchiveClose( &Archive);
            return( NO);
         }
         if( ArchiveIsMarker( &Item)){
            // archive access error
            ArchiveClose( &Archive);
            return( NO);
         }
         ArchiveStatistic( &StatisticFemale, &Item);
         StatisticFemale.Target = TargetWeightFemale;
      }
   }
   ArchiveClose( &Archive);
   if(smsFormat == GSM_SMS_FORMAT_PC_ONLY){
     return( _SendStatisticNewFormat(PhoneNumber, Day, &Statistic, &StatisticFemale));
   } else {
      return( _SendStatistic( PhoneNumber, Day, &Statistic, &StatisticFemale));
   }
} // _SendStatisticIndex

//------------------------------------------------------------------------------
//   Send statistics
//------------------------------------------------------------------------------

static TYesNo _SendStatistic( char *PhoneNumber, TDayNumber Day, TStatistic *Statistic, TStatistic *StatisticFemale)
// Send <Statistic>/<StatisticFemale>
{
   if( WeighingHasFemale()){
      MessageCompose(CommunicationContext.Message, Day, Statistic, StatisticFemale);
   } else {
      MessageCompose(CommunicationContext.Message, Day, Statistic, NULL);
   }
   strcpy( CommunicationContext.PhoneNumber, PhoneNumber);

   return( _SendSms());
} // _SendStatistic

static TYesNo _SendStatisticNewFormat( char *PhoneNumber, TDayNumber Day, TStatistic *Statistic, TStatistic *StatisticFemale)
// Send data for males and females together with values from sensors
{
   if( WeighingHasFemale()){
      MessageComposeNewFormat(CommunicationContext.Message, Day, Statistic, StatisticFemale);
   } else {
      MessageComposeNewFormat(CommunicationContext.Message, Day, Statistic, NULL);
   }
   strcpy( CommunicationContext.PhoneNumber, PhoneNumber);

   return( _SendSms());
} // _SendStatisticNewFormat

//------------------------------------------------------------------------------
//   Send statistics binary format
//------------------------------------------------------------------------------
#include "Crc/Crc.h"

static void ToBinaryGenderStats(TStatistic *Statistic, TBinaryGenderStats *GenderStats)
{
   GenderStats->Target = Statistic->Target;
   GenderStats->Count = Statistic->Count;
   GenderStats->Average = Statistic->Average;
   GenderStats->Gain = Statistic->Gain;
   GenderStats->Sigma = Statistic->Sigma;
   GenderStats->Cv = Statistic->Cv;
   GenderStats->Uniformity = Statistic->Uniformity;
}

//static TYesNo _SendStatisticBinary( char *PhoneNumber, TDayNumber Day, TStatistic *Statistic, TStatistic *StatisticFemale)
//// Send binary formated <Statistic>/<StatisticFemale>
//{
//TBinaryStatistics Stats;
//int Size;
//word *crcPtr;

//   Stats.DateTime = WeighingDayStartDateTime( Day);
//   Stats.Day = Day;
//   ToBinaryGenderStats(Statistic, &Stats.Male);
//   if( WeighingHasFemale()){
//      ToBinaryGenderStats(StatisticFemale, &Stats.Female.Female);
//      crcPtr = &Stats.Crc16;
//   } else {
//      crcPtr = &Stats.Female.Crc16;
//   }
//   Size = (unsigned)crcPtr - (unsigned)&Stats;
//   *crcPtr = CalculateCrc16( (byte*)&Stats, Size, 0);
//   Size += sizeof(Stats.Crc16);

//   if(PhoneNumber != NULL){
//      strcpy( CommunicationContext.PhoneNumber, PhoneNumber);
//      memcpy( CommunicationContext.Message, &Stats, Size);
//      return( _SendBinarySms( Size));
//   } else {
//      InternetPublicationSettings settings = {
//        DataPublication.Url,      // url
//        DataPublication.Username, //username
//        DataPublication.Password  //password
//      };
//      return InternetPublicationPublish(settings, &Stats, Size);
//   }
//}

//------------------------------------------------------------------------------
//  Send event
//------------------------------------------------------------------------------
#define DATE_TIME_TEXT_SIZE 24

static TYesNo _SendEvent( TLogItem *LogItem)
// Send event by log code to current event contact
{
char  DateTime[ DATE_TIME_TEXT_SIZE + 1];
TContactList ContactList;
TGsmContact  Contact;

   while(!ContactListOpen( &ContactList));
   ContactListLoad( &ContactList, &Contact, CommunicationContext.EventContact);
   CommunicationContext.ContactType = Contact.SmsFormat;
   ContactListPhoneNumber( &ContactList, CommunicationContext.PhoneNumber, CommunicationContext.EventContact);
   ContactListClose( &ContactList);
   // create text sms or return according to codes
   switch( LogModuleGet(LogItem->Code)){
      case LOG_MODULE_WEIGHING_STATUS_CHANGED :
         switch( LogWeighingStatusToGet(LogItem->Code)){
            case WEIGHING_STATUS_WEIGHING :
            case WEIGHING_STATUS_STOPPED :
            case WEIGHING_STATUS_SUSPENDED :
            case WEIGHING_STATUS_CALIBRATION :
            case WEIGHING_STATUS_DIAGNOSTICS :
               break;
            default:
               return YES; // dont send this event
         }

         bDateTime( DateTime, (UDateTimeGauge)LogItem->Timestamp);
         bprintf( CommunicationContext.Message, "SCALE %s\n%s WEIGHING STATUS CHANGED\n From: %s\n To: %s",
                  Bat2Device.Name,
                  DateTime,
                  ENUM_WEIGHING_STATUS + LogWeighingStatusFromGet(LogItem->Code),
                  ENUM_WEIGHING_STATUS +  LogWeighingStatusToGet(LogItem->Code));
         break;
      case LOG_FAILURE :
         bDateTime( DateTime, (UDateTimeGauge)LogItem->Timestamp);
         bprintf( CommunicationContext.Message, "SCALE %s\n%s DEVICE FAILURE\n%s",
                  Bat2Device.Name,
                  DateTime,
                  ENUM_FAILURE + LogFailureCodeGet(LogItem->Code));
         break;
      default:
         return YES;
   }

   return( _SendSms());
}

//------------------------------------------------------------------------------
//   Phone number check
//------------------------------------------------------------------------------

static TYesNo _PhoneNumberCheck( char *Number)
// Check for command accept
{
TYesNo Enabled;
TContactList ContactList;
   if( !GsmMessage.Commands.Enabled){
      return( NO);                     // commands disabled
   }
   if( !GsmMessage.Commands.CheckPhoneNumber){
      return( YES);                    // always accept
   }
   while(!ContactListOpen( &ContactList));
   Enabled = ContactListCommandEnabled( &ContactList, Number);
   ContactListClose( &ContactList);
   return Enabled;
} // _PhoneNumberCheck

//------------------------------------------------------------------------------
//   Statistic contact first
//------------------------------------------------------------------------------

static TYesNo _ContactStatisticFirst( void)
// Check for first statistic contact
{
   return _ContactStatisticFind(0); // find first
} // _ContactStatisticFirst

//------------------------------------------------------------------------------
//   Statistic contact next
//------------------------------------------------------------------------------

static TYesNo _ContactStatisticNext( void)
// Check for next statistic contact
{
   if( _ContactStatisticDone()){
      return( NO);                     // already done
   }

   return _ContactStatisticFind(CommunicationContext.StatisticContact + 1); // find next
} // _ContactStatisticNext

//------------------------------------------------------------------------------
//   Statistic contact find
//------------------------------------------------------------------------------

static TYesNo _ContactStatisticFind( TContactIndex From)
// Check for statistic contact starting from <From>
{
TContactIndex i, Count;
TContactList ContactList;

   while(!ContactListOpen( &ContactList));
   Count = ContactListCount( &ContactList);
   for( i = From; i < Count; i++){
      if( !ContactListStatisticEnabled( &ContactList, i)){
         continue;
      }
      CommunicationContext.StatisticContact = i;
      ContactListClose( &ContactList);
      return( YES);
   }
   _ContactStatisticStop();            // stop send
   ContactListClose( &ContactList);
   return( NO);
} // _ContactStatisticFind

//------------------------------------------------------------------------------
//   Event contact find
//------------------------------------------------------------------------------

static TYesNo _ContactEventNext( void)
// Check for next event contact
{
TContactList ContactList;

   if( CommunicationContext.EventContact == CONTACT_INDEX_INVALID){
      CommunicationContext.EventContact = 0;
   } else {
      CommunicationContext.EventContact++;
   }

   while(!ContactListOpen( &ContactList));
   while( CommunicationContext.EventContact < ContactListCount( &ContactList)){
      if( ContactListEventEnabled( &ContactList, CommunicationContext.EventContact)){
         ContactListClose( &ContactList);
         return YES;
      }
      CommunicationContext.EventContact++;
   }
   ContactListClose( &ContactList);
   CommunicationContext.EventContact = CONTACT_INDEX_INVALID;
   return NO;
}

//------------------------------------------------------------------------------
//   Send SMS
//------------------------------------------------------------------------------

static TYesNo _SendSms( void)
// Send Sms
{
   CommunicationContext.SendSmsSlot = SmsGateSmsSend(  CommunicationContext.Message, CommunicationContext.PhoneNumber);
   if( CommunicationContext.SendSmsSlot < SMS_GATE_SMS_SLOTS){
      return YES;
   }
   CommunicationContext.SendSmsSlot = SMS_GATE_SLOT_ERROR;
   return NO;
} // _SendSms

//------------------------------------------------------------------------------
//   Send SMS
//------------------------------------------------------------------------------

//static TYesNo _SendBinarySms( int32 size)
//// Send binary SMS
//{
//   CommunicationContext.SendSmsSlot = SmsGateSmsSendBinary(CommunicationContext.Message, size, CommunicationContext.PhoneNumber);
//   if( CommunicationContext.SendSmsSlot < SMS_GATE_SMS_SLOTS){
//      return YES;
//   }
//   CommunicationContext.SendSmsSlot = SMS_GATE_SLOT_ERROR;
//   return NO;
//}
