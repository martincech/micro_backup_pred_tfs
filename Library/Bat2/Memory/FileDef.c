//*****************************************************************************
//
//   FileDef.c       Bat2 file definitions
//   Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#include "FileDef.h"
#include "Memory/FileInternal.h"
#include "Config/Bat2Def.h"

TFileDescriptor FileDescriptor[_FILE_COUNT] = {
   { NVM_SANDBOX_START,               NVM_SANDBOX_SIZE},
   { CONFIG_ADDRESS,                  CONFIG_SIZE},
   { NVM_SAMPLES_START,               NVM_SAMPLES_SIZE},
   { NVM_SAMPLES_BACKUP_START,        NVM_SAMPLES_BACKUP_SIZE},

   { NVM_ARCHIVE_START,               NVM_ARCHIVE_SIZE},
   { NVM_FIFO_START,                  NVM_FIFO_SIZE},
   { CONTACT_ADDRESS,                 CONTACT_SIZE},
   { WEIGHING_PLAN_ADDRESS,           WEIGHING_PLAN_SIZE},
   { CONTEXT_ADDRESS,                 CONTEXT_SIZE},
   { PREDEFINED_WEIGHING_ADDRESS,     PREDEFINED_WEIGHING_SIZE},
   { GROWTH_CURVE_ADDRESS,            GROWTH_CURVE_SIZE},
   { CORRECTION_CURVE_ADDRESS,        CORRECTION_CURVE_SIZE},
   { NVM_LOG_START,                   NVM_LOG_SIZE},
   { NVM_STATE_START,                 NVM_STATE_SIZE},
   { NVM_FIRMWARE_START,              NVM_FIRMWARE_SIZE}
};

#include "FileDef.h"
#include "Memory/FileInternal.h"

#include "Memory/FileNvm.h"
#include "Memory/FileRemote.h"

const TFileInterface FileInterface[_FILE_INTERFACE_COUNT] = {
   {
      FileLocalPermission,
      FileLocalOpen,
      FileLocalClose,
      FileLocalSave,
      FileLocalLoad,
      FileLocalFill,
      FileLocalMatch,
      FileLocalSum,
      FileLocalMove,
      FileLocalCommit
   },
   {
      FileRemotePermission,
      FileRemoteOpen,
      FileRemoteClose,
      FileRemoteSave,
      FileRemoteLoad,
      FileRemoteFill,
      FileRemoteMatch,
      FileRemoteSum,
      FILE_UNUSED_PTR,
      FileRemoteCommit
   }
};

const TFileItem FileItem[_FILE_LIST_COUNT] = {
   {FILE_SANDBOX,                FILE_INTERFACE_LOCAL},
   {FILE_CONFIG,                 FILE_INTERFACE_LOCAL},
   {FILE_SAMPLES,                FILE_INTERFACE_LOCAL},
   {FILE_SAMPLES_BACKUP,         FILE_INTERFACE_LOCAL},
   {FILE_ARCHIVE,                FILE_INTERFACE_LOCAL},
   {FILE_FIFO,                   FILE_INTERFACE_LOCAL},
   {FILE_CONTACT_LIST,           FILE_INTERFACE_LOCAL},
   {FILE_WEIGHING_PLAN,          FILE_INTERFACE_LOCAL},
   {FILE_CONTEXT,                FILE_INTERFACE_LOCAL},
   {FILE_PREDEFINED_WEIGHING,    FILE_INTERFACE_LOCAL},
   {FILE_GROWTH_CURVE,           FILE_INTERFACE_LOCAL},
   {FILE_CORRECTION_CURVE,       FILE_INTERFACE_LOCAL},
   {FILE_LOG,                    FILE_INTERFACE_LOCAL},
   {FILE_STATE,                  FILE_INTERFACE_LOCAL},
   {FILE_FIRMWARE,               FILE_INTERFACE_LOCAL},
   {FILE_CONFIG,                 FILE_INTERFACE_REMOTE},
   {FILE_SAMPLES,                FILE_INTERFACE_REMOTE},
   {FILE_SAMPLES_BACKUP,         FILE_INTERFACE_REMOTE},
   {FILE_ARCHIVE,                FILE_INTERFACE_REMOTE},
   {FILE_FIFO,                   FILE_INTERFACE_REMOTE},
   {FILE_CONTACT_LIST,           FILE_INTERFACE_REMOTE},
   {FILE_WEIGHING_PLAN,          FILE_INTERFACE_REMOTE},
   {FILE_CONTEXT,                FILE_INTERFACE_REMOTE},
   {FILE_PREDEFINED_WEIGHING,    FILE_INTERFACE_REMOTE},
   {FILE_GROWTH_CURVE,           FILE_INTERFACE_REMOTE},
   {FILE_CORRECTION_CURVE,       FILE_INTERFACE_REMOTE},
   {FILE_LOG,                    FILE_INTERFACE_REMOTE},
   {FILE_STATE,                  FILE_INTERFACE_REMOTE},
   {FILE_FIRMWARE,               FILE_INTERFACE_REMOTE},
};
