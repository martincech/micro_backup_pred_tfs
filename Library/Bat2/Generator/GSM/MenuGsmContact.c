//******************************************************************************
//
//   MenuGsmContact.c  Gsm contact menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmContact.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "MenuGsm.h"


static DefMenu( GsmContactMenu)
   STR_NAME,
   STR_PHONE_NUMBER,
   STR_SMS_FORMAT,
   STR_STATISTICS,
   STR_SEND_HISTOGRAM,
   STR_COMMANDS,
   STR_EVENTS,

EndMenu()

typedef enum {
   MI_NAME,
   MI_PHONE_NUMBER,
   MI_SMS_FORMAT,
   MI_STATISTICS,
   MI_SEND_HISTOGRAM,
   MI_COMMANDS,
   MI_EVENTS,

} EGsmContactMenu;

// Local functions :

static void GsmContactParameters( int Index, int y, TGsmContact *Parameters);
// Draw gsm contact parameters

//------------------------------------------------------------------------------
//  Menu GsmContact
//------------------------------------------------------------------------------

void MenuGsmContact( void)
// Edit gsm contact parameters
{
TMenuData MData;
int       i;
char Name[ GSM_CONTACT_NAME_SIZE + 1];
char PhoneNumber[ GSM_CONTACT_PHONE_NUMBER_SIZE + 1];


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_CONTACT, GsmContactMenu, (TMenuItemCb *)GsmContactParameters, &GsmContact, &MData)){
         ConfigGsmContactSave();
         return;
      }
      switch( MData.Item){
         case MI_NAME :
            strcpy( Name, GsmContact.Name);
            if( !DInputText( STR_NAME, STR_ENTER_NAME, Name, GSM_CONTACT_NAME_SIZE)){
               break;
            }
            strcpy( GsmContact.Name, Name);
            break;

         case MI_PHONE_NUMBER :
            strcpy( PhoneNumber, GsmContact.PhoneNumber);
            if( !DInputText( STR_PHONE_NUMBER, STR_ENTER_PHONE_NUMBER, PhoneNumber, GSM_CONTACT_PHONE_NUMBER_SIZE)){
               break;
            }
            strcpy( GsmContact.PhoneNumber, PhoneNumber);
            break;

         case MI_SMS_FORMAT :
            i = GsmContact.SmsFormat;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_GSM_SMS_FORMAT, _GSM_SMS_FORMAT_LAST)){
               break;
            }
            GsmContact.SmsFormat = (byte)i;
            break;

         case MI_STATISTICS :
            i = GsmContact.Statistics;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmContact.Statistics = (byte)i;
            break;

         case MI_SEND_HISTOGRAM :
            i = GsmContact.SendHistogram;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmContact.SendHistogram = (byte)i;
            break;

         case MI_COMMANDS :
            i = GsmContact.Commands;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmContact.Commands = (byte)i;
            break;

         case MI_EVENTS :
            i = GsmContact.Events;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmContact.Events = (byte)i;
            break;

      }
   }
} // MenuGsmContact

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void GsmContactParameters( int Index, int y, TGsmContact *Parameters)
// Draw gsm contact parameters
{
   switch( Index){
      case MI_NAME :
         DLabelNarrow( Parameters->Name, DMENU_PARAMETERS_X, y);
         break;

      case MI_PHONE_NUMBER :
         DLabelNarrow( Parameters->PhoneNumber, DMENU_PARAMETERS_X, y);
         break;

      case MI_SMS_FORMAT :
         DLabelEnum( Parameters->SmsFormat, ENUM_GSM_SMS_FORMAT, DMENU_PARAMETERS_X, y);
         break;

      case MI_STATISTICS :
         DLabelEnum( Parameters->Statistics, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_SEND_HISTOGRAM :
         DLabelEnum( Parameters->SendHistogram, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_COMMANDS :
         DLabelEnum( Parameters->Commands, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_EVENTS :
         DLabelEnum( Parameters->Events, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

   }
} // GsmContactParameters
