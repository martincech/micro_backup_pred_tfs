//******************************************************************************
//
//   MenuGsmDef.h  GSM menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmDef_H__
   #define __MenuGsmDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define GSM_CONTACT_NAME_SIZE 16
#define GSM_CONTACT_NAME_DEFAULT "?"

#define GSM_CONTACT_PHONE_NUMBER_SIZE 16
#define GSM_CONTACT_PHONE_NUMBER_DEFAULT "0"

#define GSM_COMMANDS_EXPIRATION_MIN 1
#define GSM_COMMANDS_EXPIRATION_MAX 99
#define GSM_COMMANDS_EXPIRATION_DEFAULT 10

#define GSM_EVENTS_WEIGHT_GREATER_MIN 0
#define GSM_EVENTS_WEIGHT_GREATER_MAX 0
#define GSM_EVENTS_WEIGHT_GREATER_DEFAULT 0

#define GSM_EVENTS_WEIGHT_COUNT_LESS_MIN 0
#define GSM_EVENTS_WEIGHT_COUNT_LESS_MAX 1000
#define GSM_EVENTS_WEIGHT_COUNT_LESS_DEFAULT 0

#define GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MIN 2
#define GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MAX 60
#define GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_DEFAULT 10

#define GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MIN 1
#define GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MAX 60
#define GSM_POWER_OPTIONS_SWITCH_ON_DURATION_DEFAULT 2

#define GSM_POWER_OPTIONS_SWITCH_ON_TIME_MIN 0
#define GSM_POWER_OPTIONS_SWITCH_ON_TIME_MAX 0
#define GSM_POWER_OPTIONS_SWITCH_ON_TIME_DEFAULT 


//------------------------------------------------------------------------------
//  Gsm power mode
//------------------------------------------------------------------------------

typedef enum {
   GSM_POWER_MODE_OFF,
   GSM_POWER_MODE_ON,
   GSM_POWER_MODE_PERIODIC,
   GSM_POWER_MODE_TIME_PLAN,
   _GSM_POWER_MODE_LAST
} EGsmPowerMode;

//------------------------------------------------------------------------------
//  Gsm sms format
//------------------------------------------------------------------------------

typedef enum {
   GSM_SMS_FORMAT_MOBILE_PHONE,
   GSM_SMS_FORMAT_PC_ONLY,
   _GSM_SMS_FORMAT_LAST
} EGsmSmsFormat;

//------------------------------------------------------------------------------
//  Enter name
//------------------------------------------------------------------------------

typedef enum {
   ENTER_NAME_ENTER_NAME,
   _ENTER_NAME_LAST
} EEnterName;

//------------------------------------------------------------------------------
//  Enter phone number
//------------------------------------------------------------------------------

typedef enum {
   ENTER_PHONE_NUMBER_ENTER_PHONE_NUMBER,
   _ENTER_PHONE_NUMBER_LAST
} EEnterPhoneNumber;

//------------------------------------------------------------------------------
//  Gsm info
//------------------------------------------------------------------------------

typedef enum {
   GSM_INFO_GSM_INFO,
   GSM_INFO_PIN_CODE_ALREADY_ENTERED,
   GSM_INFO_REGISTERED,
   GSM_INFO_UNABLE_SET_PIN,
   GSM_INFO_PIN,
   GSM_INFO_VALID,
   GSM_INFO_INVALID,
   GSM_INFO_OPERATOR,
   GSM_INFO_SIGNAL_STRENGTH,
   GSM_INFO_GSM_MODEM_ERROR,
   GSM_INFO_WRONG_PIN_NUMBER,
   _GSM_INFO_LAST
} EGsmInfo;



//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef word TDayNumber;

//------------------------------------------------------------------------------
//  Gsm contact
//------------------------------------------------------------------------------

typedef struct {
   char Name[ GSM_CONTACT_NAME_SIZE + 1]; // Contact name
   char PhoneNumber[ GSM_CONTACT_PHONE_NUMBER_SIZE + 1]; // Phone number
   byte SmsFormat; // SMS format
   byte Statistics; // Send daily statistics
   byte SendHistogram; // Send histogram also
   byte Commands; // Accept commands
   byte Events; // Send events
   byte _Dummy;
} TGsmContact;

//------------------------------------------------------------------------------
//  Gsm commands
//------------------------------------------------------------------------------

typedef struct {
   byte Enabled; // Enable commands processing
   byte CheckPhoneNumber; // Check originator phone number
   word Expiration; // Command expiration time
} TGsmCommands;

//------------------------------------------------------------------------------
//  Gsm events
//------------------------------------------------------------------------------

typedef struct {
   TWeightGauge WeightGreater; // Average weight greater than
   word WeightCountLess; // Weight count after day closed is less than
   byte StatusChanged; // Weighing status changed (stopped/suspended,...)
   byte DeviceFailure; // Some device failure
} TGsmEvents;

//------------------------------------------------------------------------------
//  Gsm power options
//------------------------------------------------------------------------------

typedef struct {
   byte Mode; // GSM power on mode
   byte SwitchOnPeriod; // GSM power period
   byte SwitchOnDuration; // GSM power on duration
   byte SwitchOnTime;
} TGsmPowerOptions;



//------------------------------------------------------------------------------
#endif
