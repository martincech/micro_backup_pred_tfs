//******************************************************************************
//
//   MenuGsmContact.h  Gsm contact menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmContact_H__
   #define __MenuGsmContact_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MenuGsm_H__
   #include "MenuGsm.h"
#endif


void MenuGsmContact( void);
// Menu gsm contact

#endif
