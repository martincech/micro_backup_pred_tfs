//******************************************************************************
//
//   MenuGsmPowerOptions.c  Gsm power options menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmPowerOptions.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "MenuGsm.h"


static DefMenu( GsmPowerOptionsMenu)
   STR_MODE,
   STR_SWITCH_ON_PERIOD,
   STR_SWITCH_ON_DURATION,
   STR_SWITCH_ON_TIME,
EndMenu()

typedef enum {
   MI_MODE,
   MI_SWITCH_ON_PERIOD,
   MI_SWITCH_ON_DURATION,
   MI_SWITCH_ON_TIME
} EGsmPowerOptionsMenu;

// Local functions :

static void GsmPowerOptionsParameters( int Index, int y, TGsmPowerOptions *Parameters);
// Draw gsm power options parameters

//------------------------------------------------------------------------------
//  Menu GsmPowerOptions
//------------------------------------------------------------------------------

void MenuGsmPowerOptions( void)
// Edit gsm power options parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_POWER_OPTIONS, GsmPowerOptionsMenu, (TMenuItemCb *)GsmPowerOptionsParameters, &GsmPowerOptions, &MData)){
         ConfigGsmPowerOptionsSave();
         return;
      }
      switch( MData.Item){
         case MI_MODE :
            i = GsmPowerOptions.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_GSM_POWER_MODE, _GSM_POWER_MODE_LAST)){
               break;
            }
            GsmPowerOptions.Mode = (byte)i;
            break;

         case MI_SWITCH_ON_PERIOD :
            i = GsmPowerOptions.SwitchOnPeriod;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MIN, GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_MAX, "min")){
               break;
            }
            GsmPowerOptions.SwitchOnPeriod = (byte)i;
            break;

         case MI_SWITCH_ON_DURATION :
            i = GsmPowerOptions.SwitchOnDuration;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MIN, GSM_POWER_OPTIONS_SWITCH_ON_DURATION_MAX, "min")){
               break;
            }
            GsmPowerOptions.SwitchOnDuration = (byte)i;
            break;

         case MI_SWITCH_ON_TIME :
            i = GsmPowerOptions.SwitchOnTime;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_POWER_OPTIONS_SWITCH_ON_TIME_MIN, GSM_POWER_OPTIONS_SWITCH_ON_TIME_MAX, 0)){
               break;
            }
            GsmPowerOptions.SwitchOnTime = (byte)i;
            break;

      }
   }
} // MenuGsmPowerOptions

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void GsmPowerOptionsParameters( int Index, int y, TGsmPowerOptions *Parameters)
// Draw gsm power options parameters
{
   switch( Index){
      case MI_MODE :
         DLabelEnum( Parameters->Mode, ENUM_GSM_POWER_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_SWITCH_ON_PERIOD :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->SwitchOnPeriod, "min");
         break;

      case MI_SWITCH_ON_DURATION :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->SwitchOnDuration, "min");
         break;

      case MI_SWITCH_ON_TIME :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->SwitchOnTime, 0);
         break;

   }
} // GsmPowerOptionsParameters
