//******************************************************************************
//
//   MenuGsmEvents.c  Gsm events menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmEvents.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "MenuGsm.h"


static DefMenu( GsmEventsMenu)
   STR_WEIGHT_GREATER,
   STR_WEIGHT_COUNT_LESS,
   STR_STATUS_CHANGED,
   STR_DEVICE_FAILURE,
EndMenu()

typedef enum {
   MI_WEIGHT_GREATER,
   MI_WEIGHT_COUNT_LESS,
   MI_STATUS_CHANGED,
   MI_DEVICE_FAILURE
} EGsmEventsMenu;

// Local functions :

static void GsmEventsParameters( int Index, int y, TGsmEvents *Parameters);
// Draw gsm events parameters

//------------------------------------------------------------------------------
//  Menu GsmEvents
//------------------------------------------------------------------------------

void MenuGsmEvents( void)
// Edit gsm events parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_EVENTS, GsmEventsMenu, (TMenuItemCb *)GsmEventsParameters, &GsmEvents, &MData)){
         ConfigGsmEventsSave();
         return;
      }
      switch( MData.Item){
         case MI_WEIGHT_GREATER :
            i = GsmEvents.WeightGreater;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmEvents.WeightGreater = (TWeightGauge)i;
            break;

         case MI_WEIGHT_COUNT_LESS :
            i = GsmEvents.WeightCountLess;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_EVENTS_WEIGHT_COUNT_LESS_MIN, GSM_EVENTS_WEIGHT_COUNT_LESS_MAX, 0)){
               break;
            }
            GsmEvents.WeightCountLess = (word)i;
            break;

         case MI_STATUS_CHANGED :
            i = GsmEvents.StatusChanged;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmEvents.StatusChanged = (byte)i;
            break;

         case MI_DEVICE_FAILURE :
            i = GsmEvents.DeviceFailure;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmEvents.DeviceFailure = (byte)i;
            break;

      }
   }
} // MenuGsmEvents

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void GsmEventsParameters( int Index, int y, TGsmEvents *Parameters)
// Draw gsm events parameters
{
   switch( Index){
      case MI_WEIGHT_GREATER :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->WeightGreater);
         break;

      case MI_WEIGHT_COUNT_LESS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->WeightCountLess, 0);
         break;

      case MI_STATUS_CHANGED :
         DLabelEnum( Parameters->StatusChanged, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_DEVICE_FAILURE :
         DLabelEnum( Parameters->DeviceFailure, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

   }
} // GsmEventsParameters
