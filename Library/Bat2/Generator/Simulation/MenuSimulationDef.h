//******************************************************************************
//
//   MenuSimulationDef.h  Simulation menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuSimulationDef_H__
   #define __MenuSimulationDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define SIMULATION_DAY_DURATION_MIN 60
#define SIMULATION_DAY_DURATION_MAX 86400
#define SIMULATION_DAY_DURATION_DEFAULT 86400

#define SIMULATION_TARGET_WEIGHT_MIN 0
#define SIMULATION_TARGET_WEIGHT_MAX 0
#define SIMULATION_TARGET_WEIGHT_DEFAULT ?

#define SIMULATION_TARGET_WEIGHT_FEMALE_MIN 0
#define SIMULATION_TARGET_WEIGHT_FEMALE_MAX 0
#define SIMULATION_TARGET_WEIGHT_FEMALE_DEFAULT ?






//------------------------------------------------------------------------------
//  Simulation
//------------------------------------------------------------------------------

typedef struct {
   UTimeGauge DayDuration; // Day duration (24h or other for simulation)
   TWeightGauge TargetWeight; // Current target weight (male)
   TWeightGauge TargetWeightFemale; // Current target weight female
} TSimulation;



//------------------------------------------------------------------------------
#endif
