//******************************************************************************
//
//   Weighing SchedulerDef.h  Weighing scheduler
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Weighing SchedulerDef_H__
   #define __Weighing SchedulerDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define WEIGHING_DAYS_DAYS_MIN 0
#define WEIGHING_DAYS_DAYS_MAX 10
#define WEIGHING_DAYS_DAYS_DEFAULT 0

#define WEIGHING_DAYS_WEIGHING_DAYS_MIN 0
#define WEIGHING_DAYS_WEIGHING_DAYS_MAX 0
#define WEIGHING_DAYS_WEIGHING_DAYS_DEFAULT 1

#define WEIGHING_DAYS_SUSPENDED_DAYS_MIN 0
#define WEIGHING_DAYS_SUSPENDED_DAYS_MAX 0
#define WEIGHING_DAYS_SUSPENDED_DAYS_DEFAULT 1

#define WEIGHING_DAYS_STARTDAY_MIN 1
#define WEIGHING_DAYS_STARTDAY_MAX 999
#define WEIGHING_DAYS_STARTDAY_DEFAULT 1

#define WEIGHING_TIME_WEIGHING_FROM_MIN 0
#define WEIGHING_TIME_WEIGHING_FROM_MAX 0
#define WEIGHING_TIME_WEIGHING_FROM_DEFAULT 0

#define WEIGHING_TIME_WEIGHING_TO_MIN 0
#define WEIGHING_TIME_WEIGHING_TO_MAX 0
#define WEIGHING_TIME_WEIGHING_TO_DEFAULT 0


//------------------------------------------------------------------------------
//  Weighing days mode
//------------------------------------------------------------------------------

typedef enum {
   WEIGHING_DAYS_MODE_DAY_NUMBER,
   WEIGHING_DAYS_MODE_DAY_OF_WEEK,
   _WEIGHING_DAYS_MODE_LAST
} EWeighingDaysMode;



//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef word TDayNumber;

//------------------------------------------------------------------------------
//  Weighing plan
//------------------------------------------------------------------------------

typedef struct {
   byte Planning; // Weighing planning enabled
   byte SyncWithDayStart; // Synchronize scheduler with Day Start/Closed time
} TWeighingPlan;

//------------------------------------------------------------------------------
//  Weighing days
//------------------------------------------------------------------------------

typedef struct {
   byte Mode; // Weighing days mode
   byte Days; // Day of week mask
   byte WeighingDays; // Weighing days of pattern
   byte SuspendedDays; // Suspended days of pattern
   TDayNumber Startday; // Start of weighing days
   word _Dummy;
} TWeighingDays;

//------------------------------------------------------------------------------
//  Weighing time
//------------------------------------------------------------------------------

typedef struct {
   UTimeGauge WeighingFrom; // Weighing from time
   UTimeGauge WeighingTo; // Weighing to time
} TWeighingTime;



//------------------------------------------------------------------------------
#endif
