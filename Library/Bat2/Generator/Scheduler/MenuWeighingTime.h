//******************************************************************************
//
//   MenuWeighingTime.h  Weighing time menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingTime_H__
   #define __MenuWeighingTime_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Weighing Scheduler_H__
   #include "Weighing Scheduler.h"
#endif


void MenuWeighingTime( void);
// Menu weighing time

#endif
