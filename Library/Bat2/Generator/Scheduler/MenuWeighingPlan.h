//******************************************************************************
//
//   MenuWeighingPlan.h  Weighing plan menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingPlan_H__
   #define __MenuWeighingPlan_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Weighing Scheduler_H__
   #include "Weighing Scheduler.h"
#endif


void MenuWeighingPlan( void);
// Menu weighing plan

#endif
