//******************************************************************************
//
//   MenuDataPublication.c  Data publication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuDataPublication.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Menu.h"
#include "MenuGsmContacts.h"


static DefMenu( DataPublicationMenu)
   STR_INTERFACE,
   STR_USERNAME,
   STR_PASSWORD,
   STR_IP,
   STR_URL,
   STR_PORT,
   STR_CONTACTS,
EndMenu()

typedef enum {
   MI_INTERFACE,
   MI_USERNAME,
   MI_PASSWORD,
   MI_IP,
   MI_URL,
   MI_PORT,
   MI_CONTACTS
} EDataPublicationMenu;

// Local functions :

static void DataPublicationParameters( int Index, int y, TDataPublication *Parameters);
// Draw data publication parameters

//------------------------------------------------------------------------------
//  Menu DataPublication
//------------------------------------------------------------------------------

void MenuDataPublication( void)
// Edit data publication parameters
{
TMenuData MData;
int       i;
char Username[ DATA_PUBLICATION_USERNAME_SIZE + 1];
char Password[ DATA_PUBLICATION_PASSWORD_SIZE + 1];
char Url[ DATA_PUBLICATION_URL_SIZE + 1];


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DATA_PUBLICATION, DataPublicationMenu, (TMenuItemCb *)DataPublicationParameters, &DataPublication, &MData)){
         ConfigDataPublicationSave();
         return;
      }
      switch( MData.Item){
         case MI_INTERFACE :
            i = DataPublication.Interface;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_DATA_PUBLICATION_INTERFACE, _DATA_PUBLICATION_INTERFACE_LAST)){
               break;
            }
            DataPublication.Interface = (byte)i;
            break;

         case MI_USERNAME :
            strcpy( Username, DataPublication.Username);
            if( !DInputText( STR_USERNAME, STR_ENTER_USERNAME, Username, DATA_PUBLICATION_USERNAME_SIZE)){
               break;
            }
            strcpy( DataPublication.Username, Username);
            break;

         case MI_PASSWORD :
            strcpy( Password, DataPublication.Password);
            if( !DInputText( STR_PASSWORD, STR_ENTER_PASSWORD, Password, DATA_PUBLICATION_PASSWORD_SIZE)){
               break;
            }
            strcpy( DataPublication.Password, Password);
            break;

         case MI_IP :
            i = (int) DataPublication.Ip;
            if( !DEditIp( DMENU_EDIT_X, MData.y, (TIpAddress *) &i)){
               break;
            }
            DataPublication.Ip = (TIpAddress) i;
            break;

         case MI_URL :
            strcpy( Url, DataPublication.Url);
            if( !DInputText( STR_URL, STR_ENTER_URL, Url, DATA_PUBLICATION_URL_SIZE)){
               break;
            }
            strcpy( DataPublication.Url, Url);
            break;

         case MI_PORT :
            i = DataPublication.Port;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, DATA_PUBLICATION_PORT_MIN, DATA_PUBLICATION_PORT_MAX, 0)){
               break;
            }
            DataPublication.Port = (word)i;
            break;

         case MI_CONTACTS :
            MenuGsmContacts();
            break;

      }
   }
} // MenuDataPublication

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void DataPublicationParameters( int Index, int y, TDataPublication *Parameters)
// Draw data publication parameters
{
   switch( Index){
      case MI_INTERFACE :
         DLabelEnum( Parameters->Interface, ENUM_DATA_PUBLICATION_INTERFACE, DMENU_PARAMETERS_X, y);
         break;

      case MI_USERNAME :
         DLabelNarrow( Parameters->Username, DMENU_PARAMETERS_X, y);
         break;

      case MI_PASSWORD :
         DLabelNarrow( Parameters->Password, DMENU_PARAMETERS_X, y);
         break;

      case MI_IP :
         DIpShortRight( Parameters->Ip, DMENU_PARAMETERS_X, y);
         break;

      case MI_URL :
         DLabelNarrow( Parameters->Url, DMENU_PARAMETERS_X, y);
         break;

      case MI_PORT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Port, 0);
         break;

      case MI_CONTACTS :
         break;

   }
} // DataPublicationParameters
