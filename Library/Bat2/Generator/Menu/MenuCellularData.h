//******************************************************************************
//
//   MenuCellularData.h  Cellular data menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuCellularData_H__
   #define __MenuCellularData_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Menu_H__
   #include "Menu.h"
#endif


void MenuCellularData( void);
// Menu cellular data

#endif
