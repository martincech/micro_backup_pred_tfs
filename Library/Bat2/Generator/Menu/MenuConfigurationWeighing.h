//******************************************************************************
//
//   MenuConfigurationWeighing.h  Configuration weighing menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuConfigurationWeighing_H__
   #define __MenuConfigurationWeighing_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Menu_H__
   #include "Menu.h"
#endif


void MenuConfigurationWeighing( void);
// Menu configuration weighing

#endif
