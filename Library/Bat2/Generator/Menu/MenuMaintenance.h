//******************************************************************************
//
//   MenuMaintenance.h  Maintenance menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenance_H__
   #define __MenuMaintenance_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Menu_H__
   #include "Menu.h"
#endif


void MenuMaintenance( void);
// Menu maintenance

#endif
