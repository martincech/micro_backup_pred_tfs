//******************************************************************************
//
//   GrowthCurveDef.h  Bat2 Growth curve
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GrowthCurveDef_H__
   #define __GrowthCurveDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define CURVE_POINT_WEIGHT_MIN 0
#define CURVE_POINT_WEIGHT_MAX 0
#define CURVE_POINT_WEIGHT_DEFAULT 0

#define GROWTH_CURVE_NAME_SIZE 15
#define GROWTH_CURVE_NAME_DEFAULT "Curve000"

#define GROWTH_CURVE_DELETED_MIN 0
#define GROWTH_CURVE_DELETED_MAX 0
#define GROWTH_CURVE_DELETED_DEFAULT 

#define GROWTH_CURVE_STANDARD_MIN 0
#define GROWTH_CURVE_STANDARD_MAX 0
#define GROWTH_CURVE_STANDARD_DEFAULT 

#define GROWTH_CURVE_POINT_SIZE 30






//------------------------------------------------------------------------------
//  Curve point
//------------------------------------------------------------------------------

typedef struct {
   Day Number Day; // Technological day
   word _Dummy;
   TWeightGauge Weight; // Target weight
} TCurvePoint;

//------------------------------------------------------------------------------
//  Growth curve
//------------------------------------------------------------------------------

typedef struct {
   char Name[ GROWTH_CURVE_NAME_SIZE + 1]; // Curve name
   byte Deleted; // Deleted curve
   byte Standard; // Standard growth curve
   word _Dummy;
   TCurvePoint Point[ GROWTH_CURVE_POINT_SIZE]; // Growth curve points
} TGrowthCurve;



//------------------------------------------------------------------------------
#endif
