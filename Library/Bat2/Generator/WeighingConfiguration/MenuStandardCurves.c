//******************************************************************************
//
//   MenuStandardCurves.c  Standard curves menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuStandardCurves.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "WeighingConfiguration.h"


static DefMenu( StandardCurvesMenu)
   STR_MALES,
   STR_FEMALES,
EndMenu()

typedef enum {
   MI_MALES,
   MI_FEMALES
} EStandardCurvesMenu;

// Local functions :

static void WeighingStandardCurvesParameters( int Index, int y, TWeighingStandardCurves *Parameters);
// Draw standard curves parameters

//------------------------------------------------------------------------------
//  Menu StandardCurves
//------------------------------------------------------------------------------

void MenuStandardCurves( void)
// Edit standard curves parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_STANDARD_CURVES, StandardCurvesMenu, (TMenuItemCb *)WeighingStandardCurvesParameters, &WeighingStandardCurves, &MData)){
         ConfigWeighingStandardCurvesSave();
         return;
      }
      switch( MData.Item){
         case MI_MALES :
            i = WeighingStandardCurves.Males;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_STANDARD_CURVES_MALES_MIN, WEIGHING_STANDARD_CURVES_MALES_MAX, 0)){
               break;
            }
            WeighingStandardCurves.Males = (TCurveIdentifier)i;
            break;

         case MI_FEMALES :
            i = WeighingStandardCurves.Females;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_STANDARD_CURVES_FEMALES_MIN, WEIGHING_STANDARD_CURVES_FEMALES_MAX, 0)){
               break;
            }
            WeighingStandardCurves.Females = (TCurveIdentifier)i;
            break;

      }
   }
} // MenuStandardCurves

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingStandardCurvesParameters( int Index, int y, TWeighingStandardCurves *Parameters)
// Draw standard curves parameters
{
   switch( Index){
      case MI_MALES :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Males, 0);
         break;

      case MI_FEMALES :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Females, 0);
         break;

   }
} // WeighingStandardCurvesParameters
