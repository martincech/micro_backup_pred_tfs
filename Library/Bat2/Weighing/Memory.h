//******************************************************************************
//
//   Memory.h       External memory
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Memory_H__
   #define __Memory_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void MemoryInit( void);
// Initialize

TYesNo MemoryTest( void);
// Memory test

void MemoryResume( void);
// Resume weighing after power on

void MemoryStart( void);
// Start weighing

void MemoryDayClose( void);
// Close day

void MemoryStop( void);
// Stop weighing

void MemoryDiagnosticStart();
// Start diagnostic

void MemoryDiagnosticStop();
// Stop diagnostic

void MemoryDiagnosticWrite();
// Write diagnostic

dword MemoryDiagnosticSamples( void);

dword MemoryDiagnosticLostSamples( void);

#endif
