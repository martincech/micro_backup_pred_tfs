//******************************************************************************
//
//   Correction.h  Weighing correction utility
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Correction_H__
   #define __Correction_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __SexDef_H__
   #include "Weight/SexDef.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __CorrectionCurveDef_H__
   #include "Curve/CorrectionCurveDef.h"
#endif   

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void CorrectionInit( void);
// Initialize

void CorrectionResume( void);
// Reload current curve

void CorrectionStart( void);
// Initialization after weighing start

void CorrectionNew( void);
// Update weighing correction for actual day

TWeightGauge CorrectionWeight( TWeightGauge Weight);
// Returns corrected <Weight>

TWeightGauge CorrectionWeightSet( TWeightGauge Weight);
// Returns inverted correction of <Weight>

#endif
