//******************************************************************************
//
//   Correction.c  Weighing correction utility
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Correction.h"
#include "Curve/CorrectionList.h"
#include "Curve/CorrectionCurve.h"
#include "Weighing/WeighingConfiguration.h"
#include "Math/uGeometry.h"

#define CORRECTION_ZERO   1000

#define _Curve    WeighingContext.CorrectionCurve
// Local functions :

static TCorrectionFactor _CorrectionFactor( TDayNumber Day);
// Returns correction factor for <Day>

static TCorrectionFactor _PointInterpolation( TDayNumber Day, byte LowIndex, byte HighIndex);
// Returns interpolated value for <Day> between <LowIndex>..<HighIndex> points

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void CorrectionInit( void)
// Initialize
{
   CorrectionListInit();
} // CorrectionInit

//------------------------------------------------------------------------------
//   Resume
//------------------------------------------------------------------------------

void CorrectionResume( void)
// Reload current curve
{
} // _CorrectionResume

//------------------------------------------------------------------------------
//   Start
//------------------------------------------------------------------------------

void CorrectionStart( void)
// Initialization after weighing start
{
   CorrectionResume();                 // load active curve
   CorrectionNew();                   // update correction for active day
} // CurveStart

//------------------------------------------------------------------------------
//   Next
//------------------------------------------------------------------------------

void CorrectionNew( void)
// Update weighing correction for actual day
{
   WeighingContext.Correction = _CorrectionFactor( WeighingDay());
} // CorrectionNew

//------------------------------------------------------------------------------
//   Weight
//------------------------------------------------------------------------------

TWeightGauge CorrectionWeight( TWeightGauge Weight)
// Returns corrected <Weight>
{
TCorrectionFactor Factor;

   Factor  = WeighingContext.Correction;
   Weight *= Factor;
   Weight /= CORRECTION_ZERO;
   return( Weight);
} // CorrectionWeight

//------------------------------------------------------------------------------
//   Weight set
//------------------------------------------------------------------------------

TWeightGauge CorrectionWeightSet( TWeightGauge Weight)
// Returns inverted correction of <Weight>
{
TCorrectionFactor Factor;

   Factor = WeighingContext.Correction;
   if( Factor == 0){
      return( Weight);                 // zero division
   }
   Weight *= CORRECTION_ZERO;
   Weight /= Factor;
   return( Weight);
} // CorrectionWeightSet

//******************************************************************************

//------------------------------------------------------------------------------
//   Factor
//------------------------------------------------------------------------------

static TCorrectionFactor _CorrectionFactor( TDayNumber Day)
// Returns correction factor for <Day>
{
byte PointIndex;

   if( WeighingConfiguration.CorrectionCurve == ULIST_IDENTIFIER_INVALID){
      return( CORRECTION_ZERO);
   }
   if( _Curve.Count == 0){
      return( CORRECTION_ZERO);        // empty curve
   }
   // check for short curve :
   if( _Curve.Count < 2){
      return( _Curve.Point[ 0].Factor + CORRECTION_ZERO);
   }
   // check for curve start :
   if( Day <= _Curve.Point[ 0].Day){
      return( _Curve.Point[ 0].Factor + CORRECTION_ZERO);
   } // else Day > Point[ 0].Day
   // check for interval :
   for( PointIndex = 1; PointIndex < _Curve.Count; PointIndex++){
      if( _Curve.Point[ PointIndex].Day >= Day){
         return( _PointInterpolation( Day, PointIndex - 1, PointIndex));
      }
   }
   // end of _Correction :
   return( _Curve.Point[ _Curve.Count - 1].Factor + CORRECTION_ZERO); // last point
} // _CorrectionFactor

//------------------------------------------------------------------------------
//   Interpolation
//------------------------------------------------------------------------------

static TCorrectionFactor _PointInterpolation( TDayNumber Day, byte LowIndex, byte HighIndex)
// Returns interpolated value for <Day> between <LowIndex>..<HighIndex> points
{
TCorrectionFactor LowFactor, HighFactor;
TDayNumber        LowDay, HighDay;

   LowFactor   = _Curve.Point[ LowIndex].Factor;
   HighFactor  = _Curve.Point[ HighIndex].Factor;
   LowDay      = _Curve.Point[ LowIndex].Day;
   HighDay     = _Curve.Point[ HighIndex].Day;
   return( (TCorrectionFactor)uInterpolationShort( LowDay, HighDay, LowFactor, HighFactor, Day) + CORRECTION_ZERO);
} // _PointInterpolation
