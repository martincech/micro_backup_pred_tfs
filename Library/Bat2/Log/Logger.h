//******************************************************************************
//
//   Logger.h       Logger
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Logger_H__
   #define __Logger_H__

#ifndef __Log_H__
   #include "Log/Log.h"
#endif

typedef enum {
   LOG_MODULE_EFS,
   LOG_MODULE_WEIGHING_STATUS_CHANGED,
   LOG_MODULE_GSM,
   LOG_FAILURE
} ELogModule;

// Get module code
#define LogModuleGet( Code)    (Code & 0xFF)

// log efs module
#define LogEfs(C)                      LogAppend(&(TLogItem){.Code = LOG_MODULE_EFS | ((C) << 8)})

// log weighing status changed module
#define LogWeighingStatusChanged(FROM, TO)\
   LogAppend(&(TLogItem){.Code = LOG_MODULE_WEIGHING_STATUS_CHANGED | ((FROM) << 8) | ((TO) << 16)})
#define LogWeighingStatusFromGet( Code) (((Code) >> 8) & 0xFF)
#define LogWeighingStatusToGet( Code)   (((Code) >> 16) & 0xFF)

// log gsm module
#define LogGsm(C)                      LogAppend(&(TLogItem){.Code = LOG_MODULE_GSM | ((C) << 8)})

// log failures
typedef enum {
   LOG_FAILURE_POWER_FAILURE
} ELogFailureCodes;

#define LogFailure(C)                  LogAppend(&(TLogItem){.Code = LOG_FAILURE | ((C) << 8)})
#define LogFailureCodeGet( Code)       (((Code) >> 8) & 0xFF)

#endif
