//******************************************************************************
//
//   LogDef.h      Log
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __LogDef_H__
   #define __LogDef_H__

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

typedef dword TLogCode;

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define LOG_INDEX_INVALID ((dword)-1)

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef dword TLogIndex;

//------------------------------------------------------------------------------
//  Archive item
//------------------------------------------------------------------------------

typedef struct {
   UClockGauge Timestamp;
   TLogCode    Code;
} TLogItem;

#define LOG_ITEM_SIZE sizeof( TLogItem)

#endif
