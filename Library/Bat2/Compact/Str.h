//******************************************************************************
//
//  Str.h          String codes
//  Version 1.0    (c) Robot
//
//******************************************************************************

#ifndef __Str_H__
   #define __Str_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#define STR_NULL (char *)0 // undefined string
//>>> #include '/MICRO/Library/Bat/Gadget/BoxTitle.str'
//-----------------------------------------------------------------------------
// Message Box Title
//-----------------------------------------------------------------------------

#define STR_CONFIRMATION     (char *)1
#define STR_INFORMATION      (char *)2
#define STR_WARNING          (char *)3
#define STR_ERROR            (char *)4

//-----------------------------------------------------------------------------
// Wait box
//-----------------------------------------------------------------------------

#define STR_BOX_WAIT         (char *)5

//<<< '/MICRO/Library/Bat/Gadget/BoxTitle.str'
//>>> #include '/MICRO/Library/Bat/Gadget/Button.str'
//-----------------------------------------------------------------------------
// Message Box Buttons
//-----------------------------------------------------------------------------

#define STR_BTN_OK           (char *)6
#define STR_BTN_YES          (char *)7
#define STR_BTN_NO           (char *)8
#define STR_BTN_CANCEL       (char *)9
#define STR_BTN_EXIT         (char *)10
#define STR_BTN_SELECT       (char *)11

//<<< '/MICRO/Library/Bat/Gadget/Button.str'
//>>> #include '/MICRO/Library/Bat/Gadget/Edit.str'
//-----------------------------------------------------------------------------
// Input box
//-----------------------------------------------------------------------------

#define STR_OUT_OF_LIMITS    (char *)12

//-----------------------------------------------------------------------------
// Text box
//-----------------------------------------------------------------------------

#define STR_STRING_EMPTY     (char *)13

//<<< '/MICRO/Library/Bat/Gadget/Edit.str'
//>>> #include '/MICRO/Library/Bat/Gadget/YesNo.str'
//-----------------------------------------------------------------------------
// Yes/No enum
//-----------------------------------------------------------------------------

#define ENUM_YES_NO          (char *)14
#define STR_NO               (char *)14
#define STR_YES              (char *)15

//<<< '/MICRO/Library/Bat/Gadget/YesNo.str'
//>>> #include '/MICRO/Library/Bat/Gadget/List.str'
//-----------------------------------------------------------------------------
// List
//-----------------------------------------------------------------------------

#define STR_EDIT             (char *)16
#define STR_CREATE           (char *)17
#define STR_COPY             (char *)18
#define STR_DELETE           (char *)19
#define STR_RENAME           (char *)20

#define STR_ENTER_NAME       (char *)21
#define STR_NAME_EXISTS      (char *)22
#define STR_REALLY_DELETE    (char *)23
#define STR_NO_ITEMS_FOUND   (char *)24
#define STR_LIST_IS_EMPTY    (char *)25
//<<< '/MICRO/Library/Bat/Gadget/List.str'
//>>> #include '/MICRO/Library/Bat/Gadget/Range.str'
//-----------------------------------------------------------------------------
// Range
//-----------------------------------------------------------------------------

#define STR_FROM             (char *)26
#define STR_TO               (char *)27
//<<< '/MICRO/Library/Bat/Gadget/Range.str'

//>>> #include '/MICRO/Library/Bat/Country/Time.str'
//-----------------------------------------------------------------------------
// Time units
//-----------------------------------------------------------------------------

#define STR_MINUTES          (char *)28
#define STR_SECONDS          (char *)29
#define STR_HOUR             (char *)30

//<<< '/MICRO/Library/Bat/Country/Time.str'
//>>> #include '/MICRO/Library/Bat/Country/DayOfWeek.str'
//-----------------------------------------------------------------------------
// Day of Week
//-----------------------------------------------------------------------------

#define ENUM_DAY_OF_WEEK     (char *)31
#define STR_MONDAY           (char *)31
#define STR_TUESDAY          (char *)32
#define STR_WEDNESDAY        (char *)33
#define STR_THURSDAY         (char *)34
#define STR_FRIDAY           (char *)35
#define STR_SATURDAY         (char *)36
#define STR_SUNDAY           (char *)37
//<<< '/MICRO/Library/Bat/Country/DayOfWeek.str'
//>>> #include '/MICRO/Library/Bat/Country/Country.str'
//-----------------------------------------------------------------------------
// Country enum
//-----------------------------------------------------------------------------

#define ENUM_COUNTRY         (char *)38
#define STR_COUNTRY_INTERNATIONAL (char *)38
#define STR_COUNTRY_ALBANIA  (char *)39
#define STR_COUNTRY_ALGERIA  (char *)40
#define STR_COUNTRY_ARGENTINA (char *)41
#define STR_COUNTRY_AUSTRALIA (char *)42
#define STR_COUNTRY_AUSTRIA  (char *)43
#define STR_COUNTRY_BANGLADESH (char *)44
#define STR_COUNTRY_BELARUS  (char *)45
#define STR_COUNTRY_BELGIUM  (char *)46
#define STR_COUNTRY_BOLIVIA  (char *)47
#define STR_COUNTRY_BRAZIL   (char *)48
#define STR_COUNTRY_BULGARIA (char *)49
#define STR_COUNTRY_CANADA   (char *)50
#define STR_COUNTRY_CHILE    (char *)51
#define STR_COUNTRY_CHINA    (char *)52
#define STR_COUNTRY_COLOMBIA (char *)53
#define STR_COUNTRY_CYPRUS   (char *)54
#define STR_COUNTRY_CZECH    (char *)55
#define STR_COUNTRY_DENMARK  (char *)56
#define STR_COUNTRY_ECUADOR  (char *)57
#define STR_COUNTRY_EGYPT    (char *)58
#define STR_COUNTRY_ESTONIA  (char *)59
#define STR_COUNTRY_FINLAND  (char *)60
#define STR_COUNTRY_FRANCE   (char *)61
#define STR_COUNTRY_GERMANY  (char *)62
#define STR_COUNTRY_GREECE   (char *)63
#define STR_COUNTRY_HUNGARY  (char *)64
#define STR_COUNTRY_INDIA    (char *)65
#define STR_COUNTRY_INDONESIA (char *)66
#define STR_COUNTRY_IRAN     (char *)67
#define STR_COUNTRY_IRELAND  (char *)68
#define STR_COUNTRY_ISRAEL   (char *)69
#define STR_COUNTRY_ITALY    (char *)70
#define STR_COUNTRY_JAPAN    (char *)71
#define STR_COUNTRY_JORDAN   (char *)72
#define STR_COUNTRY_LATVIA   (char *)73
#define STR_COUNTRY_LEBANON  (char *)74
#define STR_COUNTRY_LITHUANIA (char *)75
#define STR_COUNTRY_LUXEMBOURG (char *)76
#define STR_COUNTRY_MALAYSIA (char *)77
#define STR_COUNTRY_MALTA    (char *)78
#define STR_COUNTRY_MEXICO   (char *)79
#define STR_COUNTRY_MONGOLIA (char *)80
#define STR_COUNTRY_MOROCCO  (char *)81
#define STR_COUNTRY_NEPAL    (char *)82
#define STR_COUNTRY_NETHERLANDS (char *)83
#define STR_COUNTRY_NEW_ZEALAND (char *)84
#define STR_COUNTRY_NIGERIA  (char *)85
#define STR_COUNTRY_NORWAY   (char *)86
#define STR_COUNTRY_PAKISTAN (char *)87
#define STR_COUNTRY_PARAGUAY (char *)88
#define STR_COUNTRY_PERU     (char *)89
#define STR_COUNTRY_PHILIPPINES (char *)90
#define STR_COUNTRY_POLAND   (char *)91
#define STR_COUNTRY_PORTUGAL (char *)92
#define STR_COUNTRY_ROMANIA  (char *)93
#define STR_COUNTRY_RUSSIA   (char *)94
#define STR_COUNTRY_SLOVAKIA (char *)95
#define STR_COUNTRY_SLOVENIA (char *)96
#define STR_COUNTRY_SOUTH_AFRICA (char *)97
#define STR_COUNTRY_SOUTH_KOREA (char *)98
#define STR_COUNTRY_SPAIN    (char *)99
#define STR_COUNTRY_SWEDEN   (char *)100
#define STR_COUNTRY_SWITZERLAND (char *)101
#define STR_COUNTRY_SYRIA    (char *)102
#define STR_COUNTRY_THAILAND (char *)103
#define STR_COUNTRY_TUNISIA  (char *)104
#define STR_COUNTRY_TURKEY   (char *)105
#define STR_COUNTRY_UKRAINE  (char *)106
#define STR_COUNTRY_UK       (char *)107
#define STR_COUNTRY_USA      (char *)108
#define STR_COUNTRY_URUGUAY  (char *)109
#define STR_COUNTRY_VENEZUELA (char *)110
#define STR_COUNTRY_VIETNAM  (char *)111

//-----------------------------------------------------------------------------
// Language enum
//-----------------------------------------------------------------------------

#define ENUM_LANGUAGE        (char *)112
#define STR_LNG_CZECH        (char *)112
#define STR_LNG_DUTCH        (char *)113
#define STR_LNG_ENGLISH      (char *)114
#define STR_LNG_FINNISH      (char *)115
#define STR_LNG_FRENCH       (char *)116
#define STR_LNG_GERMAN       (char *)117
#define STR_LNG_HUNGARIAN    (char *)118
#define STR_LNG_JAPANESE     (char *)119
#define STR_LNG_POLISH       (char *)120
#define STR_LNG_PORTUGUESE   (char *)121
#define STR_LNG_RUSSIAN      (char *)122
#define STR_LNG_SPANISH      (char *)123
#define STR_LNG_TURKISH      (char *)124

//-----------------------------------------------------------------------------
// Date format enum
//-----------------------------------------------------------------------------

#define ENUM_DATE_FORMAT     (char *)125
#define STR_DATE_FORMAT_DDMMYYYY (char *)125
#define STR_DATE_FORMAT_MMDDYYYY (char *)126
#define STR_DATE_FORMAT_YYYYMMDD (char *)127
#define STR_DATE_FORMAT_YYYYDDMM (char *)128
#define STR_DATE_FORMAT_DDMMMYYYY (char *)129
#define STR_DATE_FORMAT_MMMDDYYYY (char *)130
#define STR_DATE_FORMAT_YYYYMMMDD (char *)131
#define STR_DATE_FORMAT_YYYYDDMMM (char *)132

//-----------------------------------------------------------------------------
// Time format enum
//-----------------------------------------------------------------------------

#define ENUM_TIME_FORMAT     (char *)133
#define STR_TIME_FORMAT_24   (char *)133
#define STR_TIME_FORMAT_12   (char *)134

//-----------------------------------------------------------------------------
// Daylight saving time enum
//-----------------------------------------------------------------------------

#define ENUM_DAYLIGHT_SAVING_TYPE (char *)135
#define STR_DAYLIGHT_SAVING_TYPE_OFF (char *)135
#define STR_DAYLIGHT_SAVING_TYPE_EU (char *)136
#define STR_DAYLIGHT_SAVING_TYPE_US (char *)137
//<<< '/MICRO/Library/Bat/Country/Country.str'
//>>> #include '/MICRO/Library/Bat/Country/MenuCountry.str'
//-----------------------------------------------------------------------------
// Menu Country
//-----------------------------------------------------------------------------

//STR_COUNTRY             "Country"
#define STR_LANGUAGE         (char *)138
#define STR_DATE_FORMAT      (char *)139
#define STR_TIME_FORMAT      (char *)140
#define STR_DAYLIGHT_SAVING_TIME (char *)141

// Country menu messages :
#define STR_ENTER_DATE_FORMAT (char *)142
#define STR_ENTER_DATE_SEPARATOR1 (char *)143
#define STR_ENTER_DATE_SEPARATOR2 (char *)144
#define STR_ENTER_TIME_FORMAT (char *)145
#define STR_ENTER_TIME_SEPARATOR (char *)146
//<<< '/MICRO/Library/Bat/Country/MenuCountry.str'


//>>> #include '/MICRO/Library/Bat/Weight/Weight.str'
//-----------------------------------------------------------------------------
// Units enum
//-----------------------------------------------------------------------------

#define ENUM_WEIGHT_UNITS    (char *)147
#define STR_WEIGHT_UNITS_KG  (char *)147
#define STR_WEIGHT_UNITS_G   (char *)148
#define STR_WEIGHT_UNITS_LB  (char *)149
//<<< '/MICRO/Library/Bat/Weight/Weight.str'
//>>> #include '/MICRO/Library/Bat/Weight/Sex.str'
//-----------------------------------------------------------------------------
// Sex enum
//-----------------------------------------------------------------------------

#define ENUM_SEX             (char *)150
#define STR_SEX_MALE         (char *)150
#define STR_SEX_FEMALE       (char *)151
#define STR_SEX_UNDEFINED    (char *)152
//<<< '/MICRO/Library/Bat/Weight/Sex.str'
//>>> #include '/MICRO/Library/Bat/Weight/MenuWeightUnits.str'
//-----------------------------------------------------------------------------
// Weight menu
//-----------------------------------------------------------------------------

#define STR_WEIGHT_UNITS     (char *)153
#define STR_WEIGHT_DIVISION  (char *)154
#define STR_WEIGHT_RANGE     (char *)155

//-----------------------------------------------------------------------------
// Weight capacity enum
//-----------------------------------------------------------------------------

#define ENUM_WEIGHT_CAPACITY (char *)156
#define STR_WEIGHT_CAPACITY_NORMAL (char *)156
#define STR_WEIGHT_CAPACITY_EXTENDED (char *)157
//<<< '/MICRO/Library/Bat/Weight/MenuWeightUnits.str'

//>>> #include '/MICRO/Library/Bat2/Graph/Graphs.str'
//******************************************************************************
//
//   Graphs.str  Graph data select
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************
//------------------------------------------------------------------------------
//  Graph data lines enum
//------------------------------------------------------------------------------

#define ENUM_GRAPH_DATA_LINES (char *)158
#define STR_AVERAGE_WEIGHT   (char *)158
#define STR_TARGET_WEIGHT    (char *)159
#define STR_DEVIATION        (char *)160
#define STR_COUNT            (char *)161
#define STR_UNIFORMITY       (char *)162

//------------------------------------------------------------------------------
//  Set data lines
//------------------------------------------------------------------------------

#define STR_SET_DATA_LINES   (char *)163
#define STR_LINE             (char *)164
#define STR_ARCHIVE_EMPTY    (char *)165
#define STR_DAY              (char *)166
#define STR_HOURS            (char *)167
//<<< '/MICRO/Library/Bat2/Graph/Graphs.str'

//>>> #include '/MICRO/Library/Bat2/Menu/MenuMain.str'
//******************************************************************************
//
//   Menu.str  Bat2  menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

//------------------------------------------------------------------------------
//  Statistics
//------------------------------------------------------------------------------

#define STR_ARCHIVE_CLEAR_CONFIRM (char *)168

//------------------------------------------------------------------------------
//  User settings
//------------------------------------------------------------------------------

#define STR_ENTER_DATE       (char *)169

//------------------------------------------------------------------------------
//  Maintenance
//------------------------------------------------------------------------------
#define STR_PASSWORD         (char *)170
#define STR_ENTER_SCALE_NAME (char *)171

//------------------------------------------------------------------------------
//  Configuration Weighing
//------------------------------------------------------------------------------

#define STR_WEIGHING_DISABLED (char *)172
#define STR_GROWTH_CURVE     (char *)173

//------------------------------------------------------------------------------
//  Simulation
//------------------------------------------------------------------------------

//STR_SIMULATION "Simulation"
#define STR_DAY_DURATION     (char *)174
#define STR_NEXT_DAY         (char *)175
#define STR_TARGET_WEIGHT_MALE (char *)176
#define STR_TARGET_WEIGHT_FEMALE (char *)177

#define STR_NEXT_DAY_CONFIRM (char *)178

//------------------------------------------------------------------------------
//  Growth curve
//------------------------------------------------------------------------------

#define STR_NEW_ITEM         (char *)179
#define STR_BTN_DELETE       (char *)180
#define STR_ENTER_TIME       (char *)181
//<<< '/MICRO/Library/Bat2/Menu/MenuMain.str'
//>>> #include '/MICRO/Library/Bat2/Menu/MenuWeighing.str'
//------------------------------------------------------------------------------
//  Menu Weighing...
//------------------------------------------------------------------------------

#define STR_WEIGHING_START_CONFIRM (char *)182
#define STR_WEIGHING_STOP_CONFIRM (char *)183
#define STR_WEIGHING_SUSPEND_CONFIRM (char *)184
#define STR_WEIGHING_RELEASE_CONFIRM (char *)185


//<<< '/MICRO/Library/Bat2/Menu/MenuWeighing.str'
//>>> #include '/MICRO/Library/Bat2/Menu/MenuWeighingAdjust.str'
//------------------------------------------------------------------------------
//  Menu Weighing target adjust
//------------------------------------------------------------------------------

#define STR_NEW_TARGET       (char *)186


//<<< '/MICRO/Library/Bat2/Menu/MenuWeighingAdjust.str'
//>>> #include '/MICRO/Library/Bat2/Menu/Password.str'
//-----------------------------------------------------------------------------
// Password :
//-----------------------------------------------------------------------------

#define STR_PROTECT_BY_PASSWORD (char *)187
#define STR_ENTER_PASSWORD   (char *)188
#define STR_ENTER_NEW_PASSWORD (char *)189
#define STR_CONFIRM_PASSWORD (char *)190
#define STR_PASSWORDS_DONT_MATCH (char *)191
#define STR_INVALID_PASSWORD (char *)192
//<<< '/MICRO/Library/Bat2/Menu/Password.str'
//>>> #include '/MICRO/Library/Bat2/Menu/Usb.str'
//******************************************************************************
//
//   Usb.str       Usb strings
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#define STR_USB_NOT_PRESENT_INCOMPATIBLE (char *)193
#define STR_USB_FILE_NEXISTS_CORRUPTED (char *)194
#define STR_USB_CONFIG_LOADED (char *)195

#define STR_USB_CONFIG_NONEXISTENT (char *)196
#define STR_USB_CONFIG_CORRUPTED_BAD_VERSION (char *)197
#define STR_USB_ERROR        (char *)198

#define STR_USB_DUMP_DONE    (char *)199

#define STR_CANCEL_CONFIRM   (char *)200
#define STR_PROCEED_CONFIRM  (char *)201
//<<< '/MICRO/Library/Bat2/Menu/Usb.str'
//>>> #include '/MICRO/Library/Bat2/Menu/MenuRs485.str'

#define ENUM_RS485_MODE      (char *)202
#define STR_SENSOR_PACK      (char *)202
#define STR_MODBUS           (char *)203
#define STR_MEGAVI           (char *)204
#define STR_DACS             (char *)205


#define ENUM_MODBUS_PROTOCOL (char *)206
#define STR_RTU              (char *)206
#define STR_ASCII            (char *)207


#define ENUM_MODBUS_PARITY   (char *)208
#define STR_NONE             (char *)208
#define STR_ODD              (char *)209
#define STR_EVEN             (char *)210


#define ENUM_DACS_VERSION    (char *)211
#define STR_6                (char *)211


#define STR_RS485            (char *)212
#define STR_INTERFACE_1      (char *)213
#define STR_INTERFACE_2      (char *)214


#define STR_INTERFACE        (char *)215
#define STR_ENABLED          (char *)216
#define STR_MODE             (char *)217
#define STR_MODULE_ADDRESS   (char *)218
#define STR_MODULE_CODE      (char *)219
#define STR_PROTOCOL         (char *)220
#define STR_ADDRESS          (char *)221
#define STR_BAUD_RATE        (char *)222
#define STR_DATA_BITS        (char *)223
#define STR_PARITY           (char *)224
#define STR_REPLY_DELAY      (char *)225
#define STR_DACS_VERSION     (char *)226

//<<< '/MICRO/Library/Bat2/Menu/MenuRs485.str'
//>>> #include '/MICRO/Library/Bat2/Menu/WeighingConfiguration.str'
//******************************************************************************
//
//   WeighingConfiguration.str  Bat2 weighing configuration
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************
//------------------------------------------------------------------------------
//  Prediction mode enum
//------------------------------------------------------------------------------

#define ENUM_PREDICTION_MODE (char *)227
#define STR_AUTOMATIC        (char *)227
#define STR_GROWTH_CURVE     (char *)228

//------------------------------------------------------------------------------
//  Prediction growth enum
//------------------------------------------------------------------------------

#define ENUM_PREDICTION_GROWTH (char *)229
#define STR_SLOW             (char *)229
#define STR_FAST             (char *)230

//------------------------------------------------------------------------------
//  Sex differentiation enum
//------------------------------------------------------------------------------

#define ENUM_SEX_DIFFERENTIATION (char *)231
#define STR_NO               (char *)231
#define STR_YES              (char *)232
#define STR_STEP_ONLY        (char *)233

//------------------------------------------------------------------------------
//  Weighing status enum
//------------------------------------------------------------------------------

#define ENUM_WEIGHING_STATUS (char *)234
#define STR_UNDEFINED        (char *)234
#define STR_STOPPED          (char *)235
#define STR_WAIT             (char *)236
#define STR_SLEEP            (char *)237
#define STR_WEIGHING         (char *)238
#define STR_SUSPENDED        (char *)239
#define STR_RELEASED         (char *)240
#define STR_CALIBRATION      (char *)241
#define STR_DIAGNOSTICS      (char *)242

//------------------------------------------------------------------------------
//  Adjust target weights enum
//------------------------------------------------------------------------------

#define ENUM_ADJUST_TARGET_WEIGHTS (char *)243
#define STR_NONE             (char *)243
#define STR_SINGLE           (char *)244
#define STR_RECOMPUTE        (char *)245

//------------------------------------------------------------------------------
//  Configuration
//------------------------------------------------------------------------------

#define STR_CONFIGURATION    (char *)246
#define STR_MENU_SELECTION   (char *)247
#define STR_TYPE             (char *)248
#define STR_FLOCK            (char *)249
#define STR_INITIAL_DAY      (char *)250
#define STR_INITIAL_WEIGHT   (char *)251
#define STR_INITIAL_WEIGHTS  (char *)252
#define STR_GROWTH_CURVES    (char *)253
#define STR_STANDARD_CURVE   (char *)254
#define STR_STANDARD_CURVES  (char *)255
#define STR_CORRECTION_CURVE (char *)256
#define STR_DETECTION        (char *)257
#define STR_TARGET_WEIGHTS   (char *)258
#define STR_ACCEPTANCE       (char *)259
#define STR_STATISTICS       (char *)260
#define STR_DAY_START        (char *)261
#define STR_START_NOW        (char *)262
#define STR_START_LATER      (char *)263

//------------------------------------------------------------------------------
//  Initial weights
//------------------------------------------------------------------------------

#define STR_MALES            (char *)264
#define STR_FEMALES          (char *)265

//------------------------------------------------------------------------------
//  Growth curves
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  Standard curves
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  Target weights
//------------------------------------------------------------------------------

#define STR_GROWTH           (char *)266
#define STR_SEX_DIFFERENTIATION (char *)267
#define STR_SEX              (char *)268
#define STR_ADJUST_TARGET_WEIGHTS (char *)269

//------------------------------------------------------------------------------
//  Acceptance
//------------------------------------------------------------------------------

#define STR_MARGIN_ABOVE     (char *)270
#define STR_MARGIN_BELOW     (char *)271

//------------------------------------------------------------------------------
//  Acceptance data
//------------------------------------------------------------------------------

#define STR_ACCEPTANCE_DATA  (char *)272

//------------------------------------------------------------------------------
//  Detection
//------------------------------------------------------------------------------

#define STR_FILTER           (char *)273
#define STR_STABILIZATION_TIME (char *)274
#define STR_STABILIZATION_RANGE (char *)275
#define STR_STEP             (char *)276

//<<< '/MICRO/Library/Bat2/Menu/WeighingConfiguration.str'

//>>> #include '/MICRO/Library/Bat2Platform/Platform/Platform.str'
//-----------------------------------------------------------------------------
// Acceptance mode enum :
//-----------------------------------------------------------------------------

#define ENUM_ACCEPTANCE_MODE (char *)277
#define STR_ACCEPTANCE_MODE_SINGLE (char *)277
#define STR_ACCEPTANCE_MODE_MIXED (char *)278
#define STR_ACCEPTANCE_MODE_STEP (char *)279

//-----------------------------------------------------------------------------
// Step mode enum :
//-----------------------------------------------------------------------------

#define ENUM_STEP            (char *)280
#define STR_STEP_ENTER       (char *)280
#define STR_STEP_LEAVE       (char *)281
#define STR_STEP_BOTH        (char *)282

//-----------------------------------------------------------------------------
// Calibration menu :
//-----------------------------------------------------------------------------

// calibration menu messages :
#define STR_ENTER_WEIGHT     (char *)283
#define STR_RELEASE_WEIGHT   (char *)284
#define STR_UNLOAD_SCALE     (char *)285
#define STR_LOAD_SCALE       (char *)286
#define STR_CALIBRATION_SAVED (char *)287
//<<< '/MICRO/Library/Bat2Platform/Platform/Platform.str'
//>>> #include '/MICRO/Library/Bat2Platform/Platform/PlatformStatus.str'
//-----------------------------------------------------------------------------
// Operation enum
//-----------------------------------------------------------------------------

#define ENUM_PLATFORM_OPERATION (char *)288
#define STR_OPERATION_UNDEFINED (char *)288
#define STR_OPERATION_STOP   (char *)289
#define STR_OPERATION_WEIGHING (char *)290
#define STR_OPERATION_CALIBRATION (char *)291
#define STR_OPERATION_DIAGNOSTICS (char *)292
#define STR_OPERATION_SLEEP  (char *)293
#define STR_OPERATION_MATCHING (char *)294

//-----------------------------------------------------------------------------
// Error enum
//-----------------------------------------------------------------------------

#define ENUM_PLATFORM_ERROR  (char *)295
#define STR_ERROR_OK         (char *)295
#define STR_ERROR_BREAK      (char *)296
#define STR_ERROR_OVERRUN    (char *)297
#define STR_ERROR_CONFIGURATION (char *)298
#define STR_ERROR_CALIBRATION (char *)299
//<<< '/MICRO/Library/Bat2Platform/Platform/PlatformStatus.str'

//>>> #include '/MICRO/Library/Bat2/Display/MenuDisplay.str'
//******************************************************************************
//                                                                            
//   MenuDisplay.str Display configuration strings
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

//-----------------------------------------------------------------------------
// Backlight mode enum :
//-----------------------------------------------------------------------------

#define ENUM_BACKLIGHT_MODE  (char *)300
#define STR_BACKLIGHT_MODE_AUTO (char *)300
#define STR_BACKLIGHT_MODE_ON (char *)301
#define STR_BACKLIGHT_MODE_OFF (char *)302

//-----------------------------------------------------------------------------
// Power save enum :
//-----------------------------------------------------------------------------

#define ENUM_DISPLAY_POWERSAVE (char *)303
#define STR_DISPLAY_POWERSAVE_NO (char *)303
#define STR_DISPLAY_POWERSAVE_YES (char *)304

//-----------------------------------------------------------------------------
// Display mode enum :
//-----------------------------------------------------------------------------

#define ENUM_DISPLAY_MODE    (char *)305
#define STR_DISPLAY_MODE_BASIC (char *)305
#define STR_DISPLAY_MODE_ADVANCED (char *)306
#define STR_DISPLAY_MODE_STRONG (char *)307

//-----------------------------------------------------------------------------
// Configuration/Backlight menu :
//-----------------------------------------------------------------------------

#define STR_BACKLIGHT_MODE   (char *)308
#define STR_BACKLIGHT_DURATION (char *)309
#define STR_BACKLIGHT_INTENSITY (char *)310

//-----------------------------------------------------------------------------
// Configuration/Display menu :
//-----------------------------------------------------------------------------

#define STR_DISPLAY_POWERSAVE (char *)311
#define STR_DISPLAY_MODE     (char *)312
#define STR_BACKLIGHT        (char *)313
#define STR_CONTRAST         (char *)314
//<<< '/MICRO/Library/Bat2/Display/MenuDisplay.str'
//>>> #include '/MICRO/Library/Bat2/Statistic/MenuStatisticParameters.str'
//******************************************************************************
//                                                                            
//   MenuStatisticParameters.str  Statistic Parameters strings
//   Version 1.0                  (c) VEIT Electronics
//
//******************************************************************************

//-----------------------------------------------------------------------------
// Histogram mode enum
//-----------------------------------------------------------------------------

#define ENUM_HISTOGRAM_MODE  (char *)315
#define STR_HISTOGRAM_MODE_RANGE (char *)315
#define STR_HISTOGRAM_MODE_STEP (char *)316

//-----------------------------------------------------------------------------
// Statistic parameters menu
//-----------------------------------------------------------------------------

#define STR_UNIFORMITY_RANGE (char *)317
#define STR_HISTOGRAM_MODE   (char *)318
#define STR_HISTOGRAM_RANGE  (char *)319
#define STR_HISTOGRAM_STEP   (char *)320

#define STR_SHORT_PERIOD     (char *)321
#define STR_SHORT_TYPE       (char *)322
#define STR_SHORT_PERIOD_DISABLED (char *)323

#define ENUM_STATISTIC_SHORT_TYPE (char *)324
#define STR_STATISTIC_SHORT_TYPE_INTERVAL (char *)324
#define STR_STATISTIC_SHORT_TYPE_CUMMULATIVE (char *)325
//<<< '/MICRO/Library/Bat2/Statistic/MenuStatisticParameters.str'
//>>> #include '/MICRO/Library/Bat2/Message/MenuGsm.str'
//******************************************************************************
//
//   MenuGsm.str  GSM menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************
//------------------------------------------------------------------------------
//  Gsm power mode enum
//------------------------------------------------------------------------------

#define ENUM_GSM_POWER_MODE  (char *)326
#define STR_OFF              (char *)326
#define STR_ON               (char *)327
#define STR_PERIODIC         (char *)328
#define STR_TIME_PLAN        (char *)329

//------------------------------------------------------------------------------
//  Gsm sms format enum
//------------------------------------------------------------------------------

#define ENUM_GSM_SMS_FORMAT  (char *)330
#define STR_MOBILE_PHONE     (char *)330
#define STR_PC_ONLY          (char *)331

//------------------------------------------------------------------------------
//  Enter name enum
//------------------------------------------------------------------------------

#define ENUM_ENTER_NAME      (char *)332
#define STR_ENTER_NAME       (char *)332

//------------------------------------------------------------------------------
//  Enter phone number enum
//------------------------------------------------------------------------------

#define ENUM_ENTER_PHONE_NUMBER (char *)333
#define STR_ENTER_PHONE_NUMBER (char *)333

//------------------------------------------------------------------------------
//  Gsm info enum
//------------------------------------------------------------------------------

#define ENUM_GSM_INFO        (char *)334
#define STR_GSM_INFO         (char *)334
#define STR_PIN_CODE_ALREADY_ENTERED (char *)335
#define STR_REGISTERED       (char *)336
#define STR_UNABLE_SET_PIN   (char *)337
#define STR_PIN              (char *)338
#define STR_VALID            (char *)339
#define STR_INVALID          (char *)340
#define STR_OPERATOR         (char *)341
#define STR_SIGNAL_STRENGTH  (char *)342
#define STR_GSM_MODEM_ERROR  (char *)343
#define STR_WRONG_PIN_NUMBER (char *)344

//------------------------------------------------------------------------------
//  Gsm
//------------------------------------------------------------------------------

#define STR_GSM              (char *)345
#define STR_POWER_OPTIONS    (char *)346
#define STR_CONTACTS         (char *)347
#define STR_COMMANDS         (char *)348
#define STR_EVENTS           (char *)349
#define STR_INFO             (char *)350
#define STR_PIN_CODE         (char *)351

//------------------------------------------------------------------------------
//  Contacts
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  Contact
//------------------------------------------------------------------------------

#define STR_CONTACT          (char *)352
#define STR_NAME             (char *)353
#define STR_PHONE_NUMBER     (char *)354
#define STR_SMS_FORMAT       (char *)355
#define STR_SEND_HISTOGRAM   (char *)356

//------------------------------------------------------------------------------
//  Commands
//------------------------------------------------------------------------------

#define STR_CHECK_PHONE_NUMBER (char *)357
#define STR_EXPIRATION       (char *)358

//------------------------------------------------------------------------------
//  Events
//------------------------------------------------------------------------------

#define STR_WEIGHT_GREATER   (char *)359
#define STR_WEIGHT_COUNT_LESS (char *)360
#define STR_STATUS_CHANGED   (char *)361
#define STR_DEVICE_FAILURE   (char *)362

//------------------------------------------------------------------------------
//  Power options
//------------------------------------------------------------------------------

#define STR_SWITCH_ON_PERIOD (char *)363
#define STR_SWITCH_ON_DURATION (char *)364
#define STR_SWITCH_ON_TIME   (char *)365

//<<< '/MICRO/Library/Bat2/Message/MenuGsm.str'
//>>> #include '/MICRO/Library/Bat2/Scheduler/Scheduler.str'
//******************************************************************************
//
//   Scheduler.str  Scheduler
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************
//------------------------------------------------------------------------------
//  Weighing days mode enum
//------------------------------------------------------------------------------

#define ENUM_WEIGHING_DAYS_MODE (char *)366
#define STR_DAY_NUMBER       (char *)366
#define STR_DAY_OF_WEEK      (char *)367

//------------------------------------------------------------------------------
//  Weighing plan
//------------------------------------------------------------------------------

//STR_WEIGHING_PLAN "Weighing plan"
#define STR_PLANNING         (char *)368
#define STR_SYNC_WITH_DAY_START (char *)369
#define STR_WEIGHING_DAYS    (char *)370
#define STR_WEIGHING_TIME    (char *)371

//------------------------------------------------------------------------------
//  Weighing days
//------------------------------------------------------------------------------

//STR_WEIGHING_DAYS "Weighing days"
//STR_MODE         "Mode"
#define STR_DAYS             (char *)372
//STR_WEIGHING_DAYS  "Weighing days"
#define STR_SUSPENDED_DAYS   (char *)373
#define STR_START_DAY        (char *)374
//<<< '/MICRO/Library/Bat2/Scheduler/Scheduler.str'
//>>> #include '/MICRO/Library/Bat2/Log/Log.str'
#define ENUM_FAILURE         (char *)375
#define STR_POWER_FAILURE    (char *)375


//<<< '/MICRO/Library/Bat2/Log/Log.str'
//>>> #include '/MICRO/Library/Bat2/Communication/Communication.str'
//******************************************************************************
//
//   Communication.str  Communication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************
//------------------------------------------------------------------------------
//  Data publication interface enum
//------------------------------------------------------------------------------

#define ENUM_DATA_PUBLICATION_INTERFACE (char *)376
#define STR_DISABLED         (char *)376
#define STR_INTERNET         (char *)377
#define STR_SMS              (char *)378
#define STR_BOTH             (char *)379

//------------------------------------------------------------------------------
//  Day number enum
//------------------------------------------------------------------------------

#define ENUM_DAY_NUMBER      (char *)380

//------------------------------------------------------------------------------
//  Data publication
//------------------------------------------------------------------------------

#define STR_DATA_PUBLICATION (char *)380
#define STR_USERNAME         (char *)381
#define STR_URL              (char *)382
#define STR_START_FROM_DAY   (char *)383
#define STR_PERIOD           (char *)384
#define STR_ACCELERATE_FROM_DAY (char *)385
#define STR_ACCELERATED_PERIOD (char *)386
#define STR_SEND_AT          (char *)387

//------------------------------------------------------------------------------
//  Cellular data
//------------------------------------------------------------------------------

#define STR_CELLULAR_DATA    (char *)388
#define STR_APN              (char *)389

//------------------------------------------------------------------------------
//  Ethernet
//------------------------------------------------------------------------------

#define STR_ETHERNET         (char *)390
#define STR_DHCP             (char *)391
#define STR_IP               (char *)392
#define STR_SUBNET_MASK      (char *)393
#define STR_GATEWAY          (char *)394
#define STR_PRIMARY_DNS      (char *)395
#define STR_SECONDARY_DNS    (char *)396

//------------------------------------------------------------------------------
//  Wifi
//------------------------------------------------------------------------------

#define STR_WIFI             (char *)397
#define STR_NETWORK          (char *)398

//<<< '/MICRO/Library/Bat2/Communication/Communication.str'
//>>> #include '/MICRO/Library/Bat2/Menu/Menu.str'
//******************************************************************************
//
//   Menu.str  Bat2  menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************
//------------------------------------------------------------------------------
//  Main
//------------------------------------------------------------------------------

#define STR_MAIN             (char *)399
#define STR_USER_SETTINGS    (char *)400
#define STR_MAINTENANCE      (char *)401

//------------------------------------------------------------------------------
//  Weighing
//------------------------------------------------------------------------------

#define STR_START            (char *)402
#define STR_STOP             (char *)403
#define STR_SUSPEND          (char *)404
#define STR_RELEASE          (char *)405
#define STR_DIAGNOSTICS_START (char *)406

//------------------------------------------------------------------------------
//  Statistics
//------------------------------------------------------------------------------

#define STR_ARCHIVE          (char *)407
#define STR_GRAPHS           (char *)408
#define STR_TODAYS_WEIGHINGS (char *)409

//------------------------------------------------------------------------------
//  User settings
//------------------------------------------------------------------------------

#define STR_DISPLAY_BACKLIGHT (char *)410
#define STR_DISPLAY_CONTRAST (char *)411
#define STR_DATE_AND_TIME    (char *)412

//------------------------------------------------------------------------------
//  Configuration
//------------------------------------------------------------------------------

#define STR_COMMUNICATION    (char *)413
#define STR_DISPLAY          (char *)414

//------------------------------------------------------------------------------
//  Maintenance
//------------------------------------------------------------------------------

#define STR_SCALE_NAME       (char *)415
#define STR_COUNTRY          (char *)416
#define STR_PLATFORM         (char *)417
#define STR_USB              (char *)418
#define STR_POWER            (char *)419
#define STR_SIMULATION       (char *)420
#define STR_BOOTLOADER       (char *)421
#define STR_FACTORY_DEFAULTS (char *)422

//------------------------------------------------------------------------------
//  Weighing
//------------------------------------------------------------------------------

#define STR_PREDEFINED_WEIGHINGS (char *)423
#define STR_DEFAULT_WEIGHING (char *)424
#define STR_WEIGHING_PLAN    (char *)425
#define STR_CORRECTION_CURVES (char *)426

//------------------------------------------------------------------------------
//  Communication
//------------------------------------------------------------------------------

#define STR_CELLULAR         (char *)427
#define STR_INTERNET_CONNECTION (char *)428

//<<< '/MICRO/Library/Bat2/Menu/Menu.str'

//MenuWeighing.c

// MenuUsb.c
#define STR_LOAD_CONFIGURATION (char *)429
#define STR_SAVE_CONFIGURATION (char *)430
#define STR_TEST_MEMORY      (char *)431
#define STR_FORMAT           (char *)432
#define STR_STATUS           (char *)433

#define STR_DONE             (char *)434
#define STR_USB_OK           (char *)435

#define STR_ALL_DATA_LOST    (char *)436

#define STR_UNABLE_WRITE_USB (char *)437
#define STR_CONTINUE_ANYWAY  (char *)438


// MenuRemote.c
#define STR_REMOTE_CLIENT_CONNECTED (char *)439
#define STR_REMOTE_TERMINATE_SESSION (char *)440
// Menu.c
#define STR_SET_DATE_TIME    (char *)441
#define STR_CHECK_RTC_BATTERY (char *)442

#define STR_POWER_OFF        (char *)443
#define STR_REALLY_POWER_OFF (char *)444

// MenuCharger
#define STR_CHARGING         (char *)445
#define STR_CHARGING_OFF     (char *)446

// Screen.c
#define STR_CHARGED          (char *)447

// MenuMaintenance

#define STR_KIND_UNKNOWN     (char *)448
#define STR_CURVE_DISABLED   (char *)449
#define STR_NO_CURVES        (char *)450
#define STR_WRONG_DATE_TIME  (char *)451

#define STR_BTN_HISTOGRAM    (char *)452
#define STR_BTN_STATISTICS   (char *)453

#define STR_HISTOGRAM        (char *)454

// Ethernet module
#define STR_ETHERNET_INFO    (char *)455
#define STR_CONNECTED        (char *)456
#define STR_NOT_CONNECTED    (char *)457
#define STR_PACKETS_COUNT    (char *)458
#define STR_ETHERNET_TURNED_OFF (char *)459
#define STR_ETHERNET_UP_TIME (char *)460
#define STR_IPV4_ADDRESS     (char *)461

// Internet publication
#define STR_INVALID_URL      (char *)462
#define STR_UNKNOWN_ERROR    (char *)463
#define STR_INVALID_HOSTNAME (char *)464
#define STR_CONNECTIONG_ERROR (char *)465
#define STR_TO_REMOTE_SERVER (char *)466
#define STR_COMMUNICATING_ERROR (char *)467
#define STR_WITH_REMOTE_SERVER (char *)468
#define STR_AUTHENTICATION_ERROR (char *)469
#define STR_CHECK_CREDENTIALS (char *)470
#define STR_INTERNET_CONNECTION_ERROR (char *)471
#define STR_UNAUTHORIZED_ACCESS (char *)472
#define STR_INSUFICIENT_PRIVILEGES (char *)473
#define STR_DETAILS          (char *)474
#define STR_TOTAL            (char *)475

// Factory defaults
#define STR_REALLY_RESET_TO_FACTORY_DEFAULTS (char *)476
#define STR_BROILERS         (char *)477
#define STR_BROILERS_1_WEEK  (char *)478
#define STR_BREEDERS         (char *)479
#define STR_OTHER            (char *)480
#define STR_MANUAL           (char *)481
#define STR_NEVER_ENDING     (char *)482
#define STR_EVERY_SECOND_DAY (char *)483
#define STR_NO_NIGHTS        (char *)484

// system :
#define _STR_LAST (char *)485 // strings count

#endif
