//******************************************************************************
//
//   Gui.c   Bat2 Scheduler
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Gui.h"
#include "Hardware.h"
#include "System/System.h"
#include "Menu/MenuExit.h"
#include "Cpu/Cpu.h"
#include "Sleep/Sleep.h"
#include "Display/Backlight.h"
#include "Power/Power.h"
#include "Usb/Usb.h"
#include "Remote/Remote.h"
#include "Multitasking/Multitasking.h"
#include "Rs485/Rs485Config.h"
#include "Multitasking/Tasks.h"
#include "Tasks/TasksDef.h"
#include "Usb/Usb.h"
#include "Graphic/Graphic.h"
#include "Display/Contrast.h"

//-----------------------------------------------------------------------------
// System keyboard control
//-----------------------------------------------------------------------------

#if KBD_NUMERIC
#include <string.h>
static byte _SysKeyMask = 0;
static int _LastKeyCode = -1;
static int _LastCharIndex = -1;
static const char _K_0_Chars[] = "0 ";
static const char _K_1_Chars[] = "1_+-*/=!?.,:;%#&";
static const char _K_2_Chars[] = "2ABCabc";
static const char _K_3_Chars[] = "3DEFdef";
static const char _K_4_Chars[] = "4GHIghi";
static const char _K_5_Chars[] = "5JKLjkl";
static const char _K_6_Chars[] = "6MNOmno";
static const char _K_7_Chars[] = "7PQRSpqrs";
static const char _K_8_Chars[] = "8TUVtuv";
static const char _K_9_Chars[] = "9WXYZwxyz";
static const char *_SysKeyChars[]={
   _K_0_Chars, _K_1_Chars, _K_2_Chars,
   _K_3_Chars, _K_4_Chars, _K_5_Chars,
   _K_6_Chars, _K_7_Chars, _K_8_Chars,
   _K_9_Chars
};

#endif

void SysKeyAscii(byte SysKeyMask)
// System keyboard semantics control
{
#if KBD_NUMERIC
   _LastKeyCode = K_NULL;
   _LastCharIndex = -1;
   _SysKeyMask = SysKeyMask;
#endif
}
#include <stdio.h>
#if KBD_NUMERIC
static int _ProcessKey( int Key)
// process key by allowed ascii leters
{
int curKey;
int i, len;
char c;
int keyAgain;


   curKey = Key & ~(K_REPEAT | K_RELEASED);

   switch( curKey){
      case K_0:
         if(!(_SysKeyMask & (SYS_KEY_NUMBERS|SYS_KEY_LETTERS|SYS_KEY_SPECIAL))){
            return K_RELEASED;
         }
         break;
      case K_1:
         if(!(_SysKeyMask & (SYS_KEY_NUMBERS|SYS_KEY_SPECIAL))){
            return K_RELEASED;
         }
         break;
      case K_2: case K_4: case K_6: case K_8: case K_7: case K_9:
         if( (Key & K_REPEAT) |
             !(_SysKeyMask & (SYS_KEY_NUMBERS|SYS_KEY_LETTERS))) {
            _LastKeyCode = Key;
            return Key & ~K_REPEAT;
         }
         if( _LastKeyCode & K_REPEAT){
            _LastKeyCode = Key;
            return K_RELEASED;
         }
         break;
      break;
      case K_3: case K_5:
         if(!(_SysKeyMask & (SYS_KEY_NUMBERS|SYS_KEY_LETTERS))){
            return K_IDLE;
         }
         break;
      default:
         return Key;
   }

   if( !(Key & K_RELEASED)){
      return K_RELEASED;
   }

   i = curKey - K_0;
   len = strlen( _SysKeyChars[i]);
   keyAgain = _LastKeyCode == curKey? K_REPEAT: 0;
   if( (_LastKeyCode != curKey) || (_LastCharIndex + 1) == len){
      // get index from begining of array according to allowed chars
      _LastCharIndex = 0;
      if( !(_SysKeyMask & SYS_KEY_NUMBERS)){
         // numbers not allowed, move to uppercase letters
         _LastCharIndex  = 1;
      }

      if( (_LastCharIndex == 1) &&
          !(_SysKeyMask & SYS_KEY_LETTER_UC) && (curKey != K_1)){
         // uppercase letters not allowed, move to lowercase
         while( _SysKeyChars[i][_LastCharIndex++] < 'a');
      }

   } else {
      // get next index
      if( curKey == K_1){
         // special letters
         _LastCharIndex++;
         if( !(_SysKeyMask & SYS_KEY_SPECIAL)){
            _LastCharIndex = 0;
         }
      }else {
         forever {
            _LastCharIndex++;
            if( _LastCharIndex == len){
               _LastCharIndex = 0;
            }
            c = _SysKeyChars[i][_LastCharIndex];
            // number but numbers not allowed
            if(!(_SysKeyMask & SYS_KEY_NUMBERS) &&( c < 'A')){
               continue;
            }
            // upper case but upper case not allowed
            if(!(_SysKeyMask & SYS_KEY_LETTER_UC) && (c >= 'A') && (c < 'a') ){
               continue;
            }
            // lower case, but lower case not allowed
            if(!(_SysKeyMask & SYS_KEY_LETTER_LC) && (c >= 'a')){
               continue;
            }
            break;
         }
      }
   }
   _LastKeyCode = curKey;
   return K_ASCII | keyAgain | _SysKeyChars[i][_LastCharIndex];
}

#endif

//------------------------------------------------------------------------------
//  Event wait
//------------------------------------------------------------------------------

int GuiEventWait( void)
// Scheduler loop, returns nonempty event
{
int Key;

   forever {
      Key = GuiScheduler();            // scheduler step
      if( Key != K_IDLE) {
          return( Key);                // nonempty event
      }
   }
} // GuiEventWait

TYesNo GuiIdle( void) {
   return !BacklightIsOn();
}
#include "Display/DisplayConfiguration.h"        // Project configuration

//------------------------------------------------------------------------------
//  Scheduler
//------------------------------------------------------------------------------

int GuiScheduler( void)
// Gui scheduler
{
static TYesNo GOff = NO;
int Key;
   if(MenuExitGet()) {
      Key = K_SHUTDOWN;
   } else {
      Key = GuiTaskScheduler();
   }

   if(Key == K_POWER_FAILURE) {
      MenuExitSet(MENU_EXIT_POWER_FAILURE);
      Key = K_SHUTDOWN;
   }
   switch( Key) {
         case K_TIMER_SLOW:
            BacklightTimer();
            break;

         case K_0:case K_0 | K_RELEASED:case K_0 | K_REPEAT:
         case K_1:case K_1 | K_RELEASED:case K_1 | K_REPEAT:
         case K_2:case K_2 | K_RELEASED:case K_2 | K_REPEAT:
         case K_3:case K_3 | K_RELEASED:case K_3 | K_REPEAT:
         case K_4:case K_4 | K_RELEASED:case K_4 | K_REPEAT:
         case K_5:case K_5 | K_RELEASED:case K_5 | K_REPEAT:
         case K_6:case K_6 | K_RELEASED:case K_6 | K_REPEAT:
         case K_7:case K_7 | K_RELEASED:case K_7 | K_REPEAT:
         case K_8:case K_8 | K_RELEASED:case K_8 | K_REPEAT:
         case K_9:case K_9 | K_RELEASED:case K_9 | K_REPEAT:
         case K_RELEASED:
            #if KBD_NUMERIC
            Key = _ProcessKey( Key);
            #endif
         case K_ENTER :case K_ESC :
         case K_WAKE_UP:
            SysTimeoutReset();
            BacklightOn();
            if(GOff) {
               GInit();
               ContrastSet();
               GOff = NO;
            }
            break;

         case K_TIMEOUT:
            BacklightOff();
            if(DisplayConfiguration.PowerSave) {
               GShutdown();
               GOff = YES;
            }
            break;

         case K_POWER_CONNECTED:
            SysTimeoutReset();
            break;

         case K_SHUTDOWN:
            SysTimeoutReset();
            BacklightOff();
            break;

         case K_IDLE:
            MultitaskingReschedule();
            break;

         default:
            break;
   }

   return Key;
} // GuiScheduler
