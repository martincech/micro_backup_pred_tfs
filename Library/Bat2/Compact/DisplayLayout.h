//*****************************************************************************
//
//    DisplayLayout.h  Bat2 display layout
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __DisplayLayout_H__
   #define __DisplayLayout_H__

//------------------------------------------------------------------------------
//  String
//------------------------------------------------------------------------------

// String codes are placed at _STR_LOW_LIMIT .. _STR_HIGH_LIMIT addresses
#define _STR_LOW_LIMIT   (char *)0                // low resource limit
#define _STR_HIGH_LIMIT  (char *)1000             // high resource limit

#define STR_LANGUAGE_COUNT  1                     // don't translate strings

//-----------------------------------------------------------------------------
// Display colors
//-----------------------------------------------------------------------------

#define DCOLOR_BACKGROUND        COLOR_WHITE        // standard background
#define DCOLOR_DEFAULT           COLOR_BLACK        // default color
#define DCOLOR_TITLE             COLOR_WHITE        // title text
#define DCOLOR_TITLE_BG          COLOR_BLACK        // title background
#define DCOLOR_CURSOR            COLOR_DARKGRAY     // cursor color
#define DCOLOR_STATUS            COLOR_BLACK        // status line text
#define DCOLOR_STATUS_BG         COLOR_LIGHTGRAY    // status line background
#define DCOLOR_STATUS_LINE       COLOR_DARKGRAY     // status line separator line
#define DCOLOR_ENTER             COLOR_WHITE        // enter area text
#define DCOLOR_ENTER_BG          COLOR_BLACK        // enter area background
#define DCOLOR_ENTER_CURSOR      COLOR_BLACK        // enter area cursor
#define DCOLOR_PROGRESS          COLOR_BLACK        // progress bar frame
#define DCOLOR_PROGRESS_BAR      COLOR_DARKGRAY     // progress bar filler
#define DCOLOR_SHADOW            COLOR_LIGHTGRAY    // shadow color
#define DCOLOR_HISTOGRAM_CURSOR  COLOR_DARKGRAY     // histogram cursor

//-----------------------------------------------------------------------------
// Display commons
//-----------------------------------------------------------------------------

#define DCURSOR_R            2              // coursor round corners

#define DENTER_YEAR_YYYY     1              // use YYYY year for edit
#define DENTER_CALLBACK      1              // enable action callback
#define DENTER_H             19             // enter field height

#define DINPUT_CAPTION_Y     45             // input field caption
#define DINPUT_EDIT_Y        80             // input field
#define DINPUT_EDIT_H        19             // input field height
#define DINPUT_RANGE_ENABLE  1              // enable DInputNumber range display

#define DMENU_PARAMETERS_X   G_WIDTH - 5    // menu parameters start x
#define DMENU_EDIT_X         G_WIDTH - 5    // edit parameters start x

#define DMENU_FONT           TAHOMA16       // menu font
#define DINPUT_FONT          TAHOMA16       // input dialogs font
#define DEDIT_FONT           TAHOMA16       // edit field font
#define DLIST_FONT           TAHOMA16       // view list font
#define DMSG_FONT            TAHOMA16       // message box font
#define DLAYOUT_FONT         TAHOMA16       // title/status line font
#define DHISTOGRAM_FONT      TAHOMA16       // histogram font

#define DALIGN_RIGHT         1              // edit fields aligned right
#define DLABEL_LENGTH_MAX    63             // max. label length

//------------------------------------------------------------------------------

#define DEDIT_TEXT          1
#define DEDIT_IP            1
//#define DINPUT_DATE_DISABLE 1
//#define DINPUT_TIME_DISABLE 1
#define DINPUT_DATE_TIME_DISABLE 1
//#define DATE_YEAR_YYYY
//#define TIME_ACCEPT_MIDNIGHT    1         // accept 24:00:00 input as valid time

//------------------------------------------------------------------------------

#endif
