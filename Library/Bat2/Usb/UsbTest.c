//******************************************************************************
//
//   MemoryTest.c     Memory test
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "UsbTest.h"
#include "Efs/Efs.h"

//------------------------------------------------------------------------------
//  Test memory
//------------------------------------------------------------------------------

#define TEST_PATTERN             "MemTest"
#define TEST_PATTERN_SIZE        sizeof(TEST_PATTERN) - 1
#define TEST_DIRECTORY           "MemTest"
#define TEST_DIRECTORY_NEW_NAME  "MemTestNew"
#define TEST_FILE                "MemTest"
#define TEST_REPEAT              1000

TYesNo UsbTestMemory( void)
// Test USB memory
{
   if(!EfsInit()) {
      return NO;
   }

   TYesNo DirectoryExisted = EfsDirectoryExists(TEST_DIRECTORY);

   if(!EfsDirectoryCreate(TEST_DIRECTORY)) {
      EfsSwitchOff();
      return NO;
   }

   if(!EfsFileCreate(TEST_FILE, 0)) {
      EfsSwitchOff();
      return NO;
   }
   int i = TEST_REPEAT;
   while(i--) {
      if(EfsFileWrite( TEST_PATTERN, TEST_PATTERN_SIZE) != TEST_PATTERN_SIZE) {
         EfsSwitchOff();
         return NO;
      }
   }
   
   EfsFileClose();
   if(!EfsFileOpen(TEST_FILE)) {
      EfsSwitchOff();
      return NO;
   }

   i = TEST_REPEAT;
   byte Buffer[TEST_PATTERN_SIZE];
   while(i--) {
      if(EfsFileRead( Buffer, TEST_PATTERN_SIZE) != TEST_PATTERN_SIZE) {
         EfsSwitchOff();
         return NO;
      }

      if(memcmp(Buffer, TEST_PATTERN, TEST_PATTERN_SIZE)) {
         EfsSwitchOff();
         return NO;
      }
   }

   EfsFileClose();
   
   if(!EfsFileDelete(TEST_FILE)) {
      EfsSwitchOff();
      return NO;
   }

   if(!EfsDirectoryChange("..")) {
      EfsSwitchOff();
      return NO;
   }

   if(!DirectoryExisted) {
      if(!EfsDirectoryRename(TEST_DIRECTORY, TEST_DIRECTORY_NEW_NAME)) {
         EfsSwitchOff();
         return NO;
      }

      if(!EfsDirectoryDelete(TEST_DIRECTORY_NEW_NAME)) {
         EfsSwitchOff();
         return NO;
      }
   }
   EfsSwitchOff();
   return YES;
} // UsbTestMemory