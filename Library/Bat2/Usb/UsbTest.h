//******************************************************************************
//
//   UsbTest.h       Usb test
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef _Uni_H__
   #include "Unisys/Uni.h"
#endif

TYesNo UsbTestMemory( void);
// Test USB memory

TYesNo UsbTestDevice( void);
// Test USB device