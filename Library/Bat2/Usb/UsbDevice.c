//******************************************************************************
//
//   UsbDevice.c      Usb device
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#include "Usb/UsbDevice.h"
#include "Remote/SocketIfUsb.h"

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void UsbDeviceInit( void)
// Initialisation
{
   SocketIfUsbInit();
} // UsbDeviceInit

//------------------------------------------------------------------------------
//  Deinitialisation
//------------------------------------------------------------------------------

void UsbDeviceDeinit( void)
// Deinitialisation
{
   SocketIfUsbDeinit();
} // UsbDeviceDeinit

//------------------------------------------------------------------------------
//  Connected
//------------------------------------------------------------------------------

TYesNo UsbDeviceIsConnected( void)
// Returns YES if connected to host
{
   return SocketIfUsbReady();
} // UsbDeviceIsConnected