//*****************************************************************************
//
//    UsbKey.h     Bat2 Usb codes
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __UsbKey_H__
   #define __UsbKey_H__

// key definition :
typedef enum {
   K_USB_ENABLE = _K_USB_BASE,
   K_USB_DISABLE,
//-------------------------------------------------------------------
   _K_USB_LAST,
} EUsbKey;
  
#endif
