//******************************************************************************
//
//   MenuGrowthCurve.c  Growth curve viewer
//   Version 1.1        (c) VEIT Electronics
//
//******************************************************************************

/*
   June 6, 2013 - v 1.1 day duplication check added
*/

#include "MenuGrowthCurve.h"
#include "Graphic/Graphic.h"              // Graphic display
#include "Console/conio.h"                // Console
#include "Sound/Beep.h"                   // Sounds
#include "Gadget/DCursor.h"               // List Cursor
#include "Gadget/DList.h"                 // List drawings
#include "Gadget/DEvent.h"                // Event manager
#include "Gadget/DLayout.h"               // Display layout
#include "Gadget/DMsg.h"                  // Message box
#include "Weight/DWeight.h"               // Display weight
#include "Gadget/DEdit.h"                 // Display edit value
#include "Gadget/DNamedList.h"            // Display named list operations
#include "Gadget/DDirectory.h"            // Display list directory
#include "Curve/CurveList.h"              // Growth curve list
#include "Curve/GrowthCurve.h"            // Growth curve
#include "Str.h"                          // project directory strings
#include "Bitmap.h"                       // Bitmaps

// Constants :
#define DCURVE_DAY_X           (35)
#define DCURVE_LINE_X          (60)
#define DCURVE_WEIGHT_X        (G_WIDTH / 2 + 65)

// Local functions :

static void EditItem( TListCursor *Cursor, TGrowthCurve *Curve);
// Edit item

static void DeleteItem( TListCursor *Cursor, TGrowthCurve *Curve);
// Delete item

static void DisplayPage( TListCursor *Cursor, TGrowthCurve *Curve);
// Display curve page

//------------------------------------------------------------------------------
//  Growth curve
//------------------------------------------------------------------------------

void MenuGrowthCurve( TCurveList *CurveList)
// Growth curve menu
{
TCurveIndex  Index;
TGrowthCurve Curve;

   forever {
      GrowthCurveDefault( &Curve);
      switch( DNamedListOperation( STR_GROWTH_CURVES, CurveList, &Index, GrowthCurveName( &Curve))){
         case DLIST_OPERATION_EDIT :
         case DLIST_OPERATION_COPY :
            CurveListLoad( CurveList, &Curve, Index);          // load curve
            MenuGrowthCurveEdit( &Curve);           // edit curve
            CurveListSave( CurveList, &Curve, Index);          // save curve
            break;

         case DLIST_OPERATION_CREATE :
            MenuGrowthCurveEdit( &Curve);           // edit default curve
            CurveListSave( CurveList, &Curve, Index);          // save curve
            break;

         case DLIST_OPERATION_RENAME :
            break;

         case DLIST_OPERATION_DELETE :
            break;

         case DLIST_OPERATION_UNDEFINED:
            break;

         default :
            return;
      }
   }
} // MenuGrowthCurve

//------------------------------------------------------------------------------
//  Select growth curve
//------------------------------------------------------------------------------

TYesNo MenuGrowthCurveSelect( TCurveList *CurveList, TUniStr Title, TCurveIdentifier *Identifier, TUniStr SpecialItem)
// Select growth curve, add <SpecialItem> at end of list
{
TCurveIdentifier id;
TCurveIndex      Index;
   // find directory index :
   id    = *Identifier;
   Index = 0;
   if( id != ULIST_IDENTIFIER_INVALID){
      Index = uDirectoryFindIdentifier(( UDirectory *) uNamedListDirectory( CurveList), id);
      if( Index == UDIRECTORY_INDEX_INVALID){
         Index = 0;
      }
   }
   // select directory item :
   Index = DDirectorySelect( Title, uNamedListDirectory( CurveList), Index, SpecialItem);
   if( Index == UDIRECTORY_INDEX_INVALID){
      return( NO);                     // no selection
   }
   // check for special item :
   if( DDirectorySpecial( Index)){
      // set special item as invalid identifier
      *Identifier = ULIST_IDENTIFIER_INVALID;
      return( YES);
   }
   *Identifier = uDirectoryItemIdentifier(( UDirectory *) uNamedListDirectory( CurveList), Index);
   return( YES);
} // MenuGrowthCurveSelect

//------------------------------------------------------------------------------
//  Edit growth curve
//------------------------------------------------------------------------------

void MenuGrowthCurveEdit( TGrowthCurve *Curve)
// Display/edit growth curve
{
TListCursor Cursor;

   DCursorInit( &Cursor, GrowthCurveCount( Curve) + 1, DLIST_ROWS_COUNT_MAX);
   forever {
      if( DCursorPageChanged( &Cursor)){
         // redraw page
         GClear();                                // clear display
         DLayoutTitle( GrowthCurveName( Curve));  // display title
         DLayoutStatus( STR_BTN_CANCEL, STR_BTN_DELETE, &BmpButtonRight); // display status line
      }
      if( DCursorRowChanged( &Cursor)){
         DisplayPage( &Cursor, Curve);
         DCursorRowUpdate( &Cursor);
         GFlush();                               // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            DCursorRowUp( &Cursor);
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            DCursorRowDown( &Cursor);
            break;

         case K_RIGHT :
            DeleteItem( &Cursor, Curve);
            DCursorCountSet( &Cursor, GrowthCurveCount( Curve) + 1);// update items count
            break;

         case K_ENTER :
            EditItem( &Cursor, Curve);
            DCursorCountSet( &Cursor, GrowthCurveCount( Curve) + 1);// update items count
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return;
      }
   }
} // MenuGrowthCurveEdit

//******************************************************************************

//------------------------------------------------------------------------------
//   Edit
//------------------------------------------------------------------------------

static void EditItem( TListCursor *Cursor, TGrowthCurve *Curve)
// Edit item
{
TCurvePoint  Point;
int          RowY;
int          Value;
dword        Index;

   Index = DCursorIndex( Cursor);                // get active row index
   // check for last row :
   if( Index == GrowthCurveCount( Curve)){
      if( GrowthCurveCount( Curve) == GROWTH_CURVE_POINT_COUNT){
         BeepError();                            // maximum capacity reached
         return;
      }
      Index = GrowthCurveAppend( Curve);         // append a new point
      DisplayPage( Cursor, Curve);
      GFlush();
   }
   GrowthCurvePoint( Curve, Index, &Point);
   RowY  = DListY( DCursorRow( Cursor));
   // edit day :
   Value = Point.Day;
   if( !DEditNumber( DCURVE_DAY_X, RowY, &Value, 0, DAY_NUMBER_MIN, DAY_NUMBER_MAX, 0)){
      DisplayPage( Cursor, Curve);
      GFlush();
      return;
   }
   if(GrowthCurvePointDayExists(Curve, Index, Value)) {
      BeepError();
      DCursorPageRedraw( Cursor);
      GFlush();
      return;
   }
   Point.Day = (TDayNumber)Value;
   GrowthCurveUpdate( Curve, Index, &Point);    // update point
   // redraw page :
   DisplayPage( Cursor, Curve);
   GFlush();
   // edit weight :
   Value = Point.Weight;
   if( DEditWeight( DCURVE_WEIGHT_X, RowY, &Value)){
      Point.Weight = (TWeightGauge)Value;
   }
   // update point :
   GrowthCurveUpdate( Curve, Index, &Point);    // update point
   GrowthCurveSort( Curve, Index);              // sort curve
   DCursorPageRedraw( Cursor);                  // redraw items
} // EditItem

//------------------------------------------------------------------------------
//  Delete item
//------------------------------------------------------------------------------

static void DeleteItem( TListCursor *Cursor, TGrowthCurve *Curve)
// Delete item
{
char        Buffer[ 16];
TCurvePoint Point;
dword       Index;

   Index = DCursorIndex( Cursor);                // get active row index
   // check for last row :
   if( Index == GrowthCurveCount( Curve)){
      BeepError();
      return;
   }
   GrowthCurvePoint( Curve, Index, &Point);
   bprintf( Buffer, "Day %d", Point.Day);
   if( !DMsgYesNo( STR_CONFIRMATION, STR_REALLY_DELETE, Buffer)){
      DCursorPageRedraw( Cursor);                // redraw items
      return;
   }
   GrowthCurveDelete( Curve, Index);
} // DeleteItem

//------------------------------------------------------------------------------
//  Display page
//------------------------------------------------------------------------------

static void DisplayPage( TListCursor *Cursor, TGrowthCurve *Curve)
// Display database page
{
int         RowY;
int         i;
TCurvePoint Point;
dword       Index;

   DListClear( Cursor);                // clear list area
   // draw points
   for( i = 0; i < DCursorRowCount( Cursor); i++){
      RowY = DListY( i);
      if( !DListCursor( Cursor, i)){
         continue;
      }
      // get item data :
      Index = DCursorPage( Cursor) + i;
      if( Index >= GrowthCurveCount( Curve)){
         // empty/last row, check for cursor position :
         if( i == DCursorRow( Cursor)){
            GTextAt( DCURVE_DAY_X, RowY);
            cputs( STR_NEW_ITEM);
         }
         continue;                     // empty row
      }
      GrowthCurvePoint( Curve, Index, &Point);
      // day number :
      GTextAt( DCURVE_DAY_X, RowY);
      DLabelFormat( DCURVE_DAY_X, RowY, "%d", Point.Day, 0);
      // weight :
      DWeightWithUnitsNarrow( DCURVE_WEIGHT_X, RowY, Point.Weight);
   }
   GFlush();                           // redraw rows
   DListLine( DCURVE_LINE_X);          // vertical separator line
} // DisplayPage
