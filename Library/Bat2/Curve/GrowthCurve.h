//******************************************************************************
//
//   GrowthCurve.h  Growth curve utility
//   Version 1.1    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GrowthCurve_H__
   #define __GrowthCurve_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __GrowthCurveDef_H__
   #include "Curve/GrowthCurveDef.h"
#endif   

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void GrowthCurveDefault( TGrowthCurve *Curve);
// Initialize

char *GrowthCurveName( TGrowthCurve *Curve);
// Growth curve name

TDayNumber GrowthCurveCount( TGrowthCurve *Curve);
// Growth curve points count

void GrowthCurvePoint( TGrowthCurve *Curve, int Index, TCurvePoint *Point);
// Returns curve point at <Index>

TYesNo GrowthCurvePointDayExists( TGrowthCurve *Curve, int Index, int Day);
// Checks if <Point>.day already exists, <Curve>.Point[Index] is not checked

//------------------------------------------------------------------------------

int GrowthCurveAppend( TGrowthCurve *Curve);
// Append default point, returns index

void GrowthCurveDelete( TGrowthCurve *Curve, int Index);
// Delete point at <Index>

TYesNo GrowthCurveUpdate( TGrowthCurve *Curve, int Index, TCurvePoint *Point);
// Update curve, with a new <Point> at <Index>

void GrowthCurveSort( TGrowthCurve *Curve, int Index);
// Sort curve, with a new data at <Index>

#endif
