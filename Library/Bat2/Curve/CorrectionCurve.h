//******************************************************************************
//
//   CorrectionCurve.h Correction curve utility
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CorrectionCurve_H__
   #define __CorrectionCurve_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __CorrectionCurveDef_H__
   #include "Curve/CorrectionCurveDef.h"
#endif   

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void CorrectionCurveDefault( TCorrectionCurve *Curve);
// Initialize

char *CorrectionCurveName( TCorrectionCurve *Curve);
// Correction curve name

TDayNumber CorrectionCurveCount( TCorrectionCurve *Curve);
// Correction curve points count

void CorrectionCurvePoint( TCorrectionCurve *Curve, int Index, TCorrectionPoint *Point);
// Returns curve point at <Index>

TYesNo CorrectionCurvePointDayExists( TCorrectionCurve *Curve, int Index, int Day);
// Checks if <Point>.day already exists, <Curve>.Point[Index] is not checked

//------------------------------------------------------------------------------

int CorrectionCurveAppend( TCorrectionCurve *Curve);
// Append default point, returns index

void CorrectionCurveDelete( TCorrectionCurve *Curve, int Index);
// Delete point at <Index>

TYesNo CorrectionCurveUpdate( TCorrectionCurve *Curve, int Index, TCorrectionPoint *Point);
// Update curve, with a new <Point> at <Index>

void CorrectionCurveSort( TCorrectionCurve *Curve, int Index);
// Sort curve, with a new data at <Index>

#endif
