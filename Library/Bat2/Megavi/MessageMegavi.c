//******************************************************************************
//
//   MessageMegavi.c  Megavi message management
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#include "Megavi/MessageMegavi.h"
#include "SmsGate/SmsGate.h"
#include "Message/ContactList.h"           // Phonebook
#include <string.h>                        // string routines

static TMegaviSmsRequest         _SmsRequest;
static char                      _SmsRequestPhoneNumber[GSM_PHONE_NUMBER_SIZE + 1];

//------------------------------------------------------------------------------
//  Slots init
//------------------------------------------------------------------------------

void MessageMegaviInit( void)
// Initialize
{
   _SmsRequest = MEGAVI_SMS_REQUEST_NONE;
   memset(_SmsRequestPhoneNumber, 0, GSM_PHONE_NUMBER_SIZE + 1);
}

//------------------------------------------------------------------------------
//  Set sms request field
//------------------------------------------------------------------------------

void MessageMegaviSetSmsRequest( TMegaviSmsRequest SmsRequest, char *PhoneNumber)
// Set sms request field to be this one
{
   _SmsRequest = SmsRequest;

   if(SmsRequest == MEGAVI_SMS_REQUEST_NONE){
      return;
   }

   strncpy( (char *) &_SmsRequestPhoneNumber, PhoneNumber, GSM_PHONE_NUMBER_SIZE + 1);
}

//------------------------------------------------------------------------------
//  Get sms request field
//------------------------------------------------------------------------------

TMegaviSmsRequest MessageMegaviGetSmsRequest( void)
// Get actual sms request field
{
   return _SmsRequest;
}

//------------------------------------------------------------------------------
//  Save sms send request
//------------------------------------------------------------------------------

TSmsGateSlotNumber MessageMegaviSmsSend( TMegaviSmsTarget SmsTarget, char *SmsText)
// Send sms to SmsTarget
{
TContactIndex  ListCount;
TContactIndex  ListIndex;
TContactList   ContactList;
TGsmContact    ListContact;
   
   if(( SmsTarget < MEGAVI_SMS_TARGET_MIN) && ( SmsTarget != MEGAVI_SMS_TARGET_BROADCAST)){
      return SMS_GATE_PHONENUMBER_ERROR;
   }
   if( SmsTarget > MEGAVI_SMS_TARGET_MAX){
      return SMS_GATE_PHONENUMBER_ERROR;
   }
   // broadcast message
   if( SmsTarget == MEGAVI_SMS_TARGET_BROADCAST){
      return SmsGateSmsSend(SmsText, NULL);
   }
   //single target message
   if( SmsTarget != _SmsRequest){
      return SMS_GATE_PHONENUMBER_ERROR;
   }

   //target in phonebook?
   ContactListOpen( &ContactList);
   for( ListIndex = 0; ListIndex < ContactListCount( &ContactList); ListIndex++){
      ContactListLoad( &ContactList, &ListContact, ListIndex);
      if( strequ(_SmsRequestPhoneNumber, ListContact.PhoneNumber)){
         break;
      }
   }
   ContactListClose( &ContactList);
   if( ListIndex > ListCount){
      return SMS_GATE_PHONENUMBER_ERROR;
   }

   return SmsGateSmsSend(SmsText, _SmsRequestPhoneNumber);
}

//------------------------------------------------------------------------------
//  Get Sms status
//------------------------------------------------------------------------------

ESmsGateSmsStatus MessageMegaviSmsStatus( TSmsGateSlotNumber SlotNumber)
// Get status of SMS on slot
{
   return SmsGateSmsStatus(SlotNumber);
}

