//******************************************************************************
//
//   MegaviTask.c  Megavi task routines
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MegaviTask.h"
#include "MegaviConfig.h"                 // Megavi options
#include "MessageMegavi.h"                // Megavi SMS services
#include "Megavi/Megavi.h"                // Megavi module
#include "Statistic/Statistic.h"          // Statistics
#include "Platform/Zone.h"                // Weighing zone
#include "Storage/Sample.h"               // Samples
#include "System/System.h"                // Timer only

#if MEGAVI_SMS_REQUEST_PERIOD <= 1
   // debug mode
   #define MEGAVI_SMS_REQUEST_COUNTER     1
#else
   #define MEGAVI_SMS_REQUEST_COUNTER     TimerSlowCount( MEGAVI_SMS_REQUEST_PERIOD)
#endif

static byte               _MegaviTaskStatus;
static byte               _MegaviTaskTimer;
static byte               _MegaviTaskSmsRequetTimer;

#define MegaviTaskSmsRequestTimerSet()    _MegaviTaskSmsRequetTimer = MEGAVI_SMS_REQUEST_COUNTER
#define MegaviTaskSmsRequestTimerTick()  (--_MegaviTaskSmsRequetTimer == 0)
#define MegaviTaskSmsRequestTimerIsSet() (_MegaviTaskSmsRequetTimer != 0)
#define MegaviTaskSmsRequestTimerReset() _MegaviTaskSmsRequetTimer = 0

//------------------------------------------------------------------------------
//  Task state
//------------------------------------------------------------------------------

typedef enum {
   MEGAVI_TASK_STATUS_OFF,
   MEGAVI_TASK_STATUS_IDLE,
   MEGAVI_TASK_STATUS_LISTENING,
   MEGAVI_TASK_STATUS_INCOMMING,
   MEGAVI_TASK_STATUS_SENDING,
   _MEGAVI_TASK_STATUS_LAST
} EMegaviTaskStatus;

#define MegaviTaskActive()     (_MegaviTaskStatus != MEGAVI_TASK_STATUS_OFF)

// Local functions :

static  void _StatisticsToMegaviFormat(TMegaviReplyStatistics *MegaviStatistic, TStatistic *Statistic);
// Change internal statistic format to format suitable for Megavi reply
static void _SetReplyAccordingToCommand( TMegaviCommand *Command, TMegaviReply *Reply);
// Set reply message according to incomming command
static void _FillReplyWithWeights(TMegaviReply *Reply, TSampleFlag SexFlag);
// Fill reply message with weights of males/females according to SexFlag
static void _MegaviTaskMessageExecute( void);
// Execute sms sending
static void _MegaviTaskRequestFieldExecute( void);
// Execute field clearing
static void _MegaviTaskClearSmsRequest( void);
// Clear SMS request field
static void _MegaviTaskStart( void);
// Start megavi task - command and reply processing
static void _MegaviTaskStop( void);
// Stop megavi task

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void MegaviTaskInit( int uartPort)
// Initialize
{
   MegaviInit(uartPort);
   MegaviTaskSmsRequestTimerReset();
   MessageMegaviInit();                // initialize megavi SMS messaging
   _MegaviTaskStatus = MEGAVI_TASK_STATUS_OFF;
}

void MegaviTaskFree( void)
// Deinit - free resources
{
   MegaviDeinit();
   _MegaviTaskStop();
}

//------------------------------------------------------------------------------
//   Megavi task executive
//------------------------------------------------------------------------------

void MegaviTaskExecute( void)
// Execute
{
TMegaviStatus MStatus;
TMegaviCommand *Command;
TMegaviReply *Reply;
   // check for megavi task activity
   if( !MegaviTaskActive()){
      _MegaviTaskStart();
   }

   forever{
      MStatus = MegaviStatus();
      switch( _MegaviTaskStatus){
         case MEGAVI_TASK_STATUS_IDLE :
            if( MStatus != MEGAVI_RECEIVE_LISTENING){
               MegaviListenStart();
            }
            _MegaviTaskStatus = MEGAVI_TASK_STATUS_LISTENING;
            break;

         case MEGAVI_TASK_STATUS_LISTENING :            
            if( MStatus == MEGAVI_RECEIVE_ERROR){
               MegaviListenStart();
               break;
            }
            if( MStatus != MEGAVI_RECEIVE_DATA_WAITING){
               return;
            }
            _MegaviTaskStatus = MEGAVI_TASK_STATUS_INCOMMING;
            break;

         case MEGAVI_TASK_STATUS_INCOMMING :
         {
            if( (Command = MegaviReceive()) == NULL){
               _MegaviTaskStatus = MEGAVI_TASK_STATUS_IDLE;
               break;
            }
            Reply = MegaviGetReplyBuffer();
            _SetReplyAccordingToCommand(Command, Reply);
            if( !MegaviSend()){
               _MegaviTaskStatus = MEGAVI_TASK_STATUS_IDLE;
               break;
            }
            _MegaviTaskStatus =  MEGAVI_TASK_STATUS_SENDING;
            break;
         }

         case MEGAVI_TASK_STATUS_SENDING :
            if( MStatus == MEGAVI_SEND_ACTIVE){
               return;
            }
            _MegaviTaskStatus =  MEGAVI_TASK_STATUS_LISTENING;
            break;

         default:
            return;
      }
   }
}

//------------------------------------------------------------------------------
//   Sms request validity executive
//------------------------------------------------------------------------------

void MegaviTaskExecuteSmsRequestClear( void)
// Execute sms request field clearing
{
   if( !MegaviTaskSmsRequestTimerIsSet()){
      return;
   }

   if( !MegaviTaskSmsRequestTimerTick()){
      return;
   }

   _MegaviTaskClearSmsRequest();
}

//------------------------------------------------------------------------------
//   Start megavi task
//------------------------------------------------------------------------------

static void _MegaviTaskStart( void)
// Start megavi task - command and reply processing
{
   if( !MegaviTaskActive()){
      _MegaviTaskStatus = MEGAVI_TASK_STATUS_IDLE;
   }
}

//------------------------------------------------------------------------------
//   Stop megavi task
//------------------------------------------------------------------------------

static void _MegaviTaskStop( void)
// Stop megavi task
{
   if( MegaviTaskActive()){
      _MegaviTaskStatus = MEGAVI_TASK_STATUS_OFF;
      _MegaviTaskClearSmsRequest();
   }
}

//------------------------------------------------------------------------------
//   Set Sms Request setting
//------------------------------------------------------------------------------

void MegaviTaskSetSmsRequest( char *PhoneNumber, TMegaviSmsRequest SmsRequest)
{
   if( !MegaviTaskActive() || MegaviTaskSmsRequestTimerIsSet()){
      return;
   }

   MessageMegaviSetSmsRequest( SmsRequest, PhoneNumber);
   if( SmsRequest == MEGAVI_SMS_REQUEST_NONE){
      return;
   }
   MegaviTaskSmsRequestTimerSet();
}

//------------------------------------------------------------------------------
//   Clear Sms Request setting
//------------------------------------------------------------------------------

static void _MegaviTaskClearSmsRequest( void)
// Clear SMS request field
{
   MegaviTaskSmsRequestTimerReset();
   MessageMegaviSetSmsRequest( MEGAVI_SMS_REQUEST_NONE, 0);
}

//------------------------------------------------------------------------------
//   Create reply message according to incomming command
//------------------------------------------------------------------------------

static void _SetReplyAccordingToCommand( TMegaviCommand *Command, TMegaviReply *Reply)
// Set reply message according to incomming command
{
   switch( Command->CommandCode){
      case MEGAVI_COMMAND_VERSION :
         Reply->ReplyCode = MEGAVI_REPLY_VERSION;
         Reply->Version.Major = MEGAVI_MAJOR;
         Reply->Version.Minor = MEGAVI_MINOR;
         break;

      case MEGAVI_COMMAND_MALE_DATA_GET :
         Reply->ReplyCode = MEGAVI_REPLY_MALE_DATA;
         Reply->Data.SmsRequest = MessageMegaviGetSmsRequest();
         _StatisticsToMegaviFormat( &(Reply->Data.Statistics), ZoneStatistic());
         _FillReplyWithWeights(Reply, WEIGHT_FLAG_MALE);
         break;

      case MEGAVI_COMMAND_FEMALE_DATA_GET :
         Reply->ReplyCode = MEGAVI_REPLY_FEMALE_DATA;
         Reply->Data.SmsRequest = MessageMegaviGetSmsRequest();
         _StatisticsToMegaviFormat( &(Reply->Data.Statistics), ZoneStatisticFemale());
         _FillReplyWithWeights(Reply, WEIGHT_FLAG_FEMALE);
         break;

      case MEGAVI_COMMAND_SMS_SEND :
         Reply->ReplyCode = MEGAVI_REPLY_SMS_SEND;
         Reply->SmsSendData.SlotNumber = MessageMegaviSmsSend(Command->SmsSendData.SmsTarget,
                                                              Command->SmsSendData.SmsText);
         if( Command->SmsSendData.SmsTarget == MessageMegaviGetSmsRequest()){
            _MegaviTaskClearSmsRequest();
         }
         break;

      case MEGAVI_COMMAND_SMS_STATUS_GET :
         Reply->ReplyCode = MEGAVI_REPLY_SMS_STATUS;
         Reply->SmsStatusData.Status = MessageMegaviSmsStatus(Command->SmsStatusData.SlotNumber);
         break;
   }
}

//------------------------------------------------------------------------------
//   Statistic format
//------------------------------------------------------------------------------

static  void _StatisticsToMegaviFormat(TMegaviReplyStatistics *MegaviStatistic, TStatistic *Statistic)
// Change internal statistic format to format suitable for Megavi reply
{
   MegaviStatistic->Average = Statistic->Average;
   MegaviStatistic->Count =  Statistic->Count;
   MegaviStatistic->Gain = Statistic->Gain;
   MegaviStatistic->Sigma = Statistic->Sigma;
   MegaviStatistic->Target = Statistic->Target;
   MegaviStatistic->Uniformity = Statistic->Uniformity;
}

//------------------------------------------------------------------------------
//   Fill weights
//------------------------------------------------------------------------------

static void _FillReplyWithWeights(TMegaviReply *Reply, TSampleFlag SexFlag)
{
byte i;
TSampleIndex j;
TSample Sample;
int ZoneSamples;
TSamples Samples;
   while(!SampleOpen( &Samples));
   ZoneSamples = ZoneSamplesCount();
   j = ZoneSamples;
   for( i = 0; i < MEGAVI_WEIGHT_COUNT; i++){
      Reply->Data.Weight[i] = 0;
      while(j > 0){
         j--;
         SampleGet( &Samples, j, &Sample);
         if(!( SampleFlag( &Sample) & SexFlag)){
            continue;
         }
         Reply->Data.Weight[i] = SampleWeight( &Sample);
      }
   }
   SampleClose( &Samples);
}
