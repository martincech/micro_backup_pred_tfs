//*****************************************************************************
//
//    Rc.c         Remote commands
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Rc.h"
#include "System/System.h"
#include "Cpu/Cpu.h"
#include "Memory/File.h"
#include "Timer/Timer.h"
#include "Action/Action.h"
#include <string.h>

#define RC_FILES_COUNT        3

typedef struct {
   TFile  File;
   byte   Name;
   TYesNo Opened;
   byte   OwnerSessionId;
} TRcFile;

static word LastActivityTime;
static byte ActiveSessionId;

static TRcFile RcFile[RC_FILES_COUNT];

static TFile *Check( TFileName FileName, byte OwnerSessionId);
// 

void CloseFiles( byte SessionId);
// Close <SessionId> files

void CloseAllFiles( void);
// Close all files

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void RcInit( void)
// Init
{
   ActiveSessionId = RC_SESSION_ID_INVALID;
} // RcInit

//------------------------------------------------------------------------------
//  Session
//------------------------------------------------------------------------------

#define SINGLE_ID       1

byte RcSessionStart( void)
// Start session - check for RC_SESSION_ID_INVALID 
{
   if(ActiveSessionId != RC_SESSION_ID_INVALID) {
      return RC_SESSION_ID_INVALID;
   }
   LastActivityTime = SysTimer();
   ActiveSessionId = SINGLE_ID;
   return SINGLE_ID;
} // RcSessionStart

TYesNo RcSessionIsActive( byte SessionId)
// Checks for session validity
{
   if(SessionId == RC_SESSION_ID_INVALID) {
      return NO;
   }
   if(SessionId != ActiveSessionId) {
      return NO;
   }
   LastActivityTime = SysTimer();
   return YES;
} // RcSessionIsActive

void RcSessionTerminate( byte SessionId)
// Terminates session
{
   if(SessionId != SINGLE_ID) {
      return;
   }
   LastActivityTime = SysTimer();
   ActiveSessionId = RC_SESSION_ID_INVALID;
   CloseFiles( SessionId);
} // RcSessionStop

void RcSessionTerminateForce()
// Terminate session by user intervention
{
   LastActivityTime = SysTimer();
   ActiveSessionId = RC_SESSION_ID_INVALID;
   CloseAllFiles();
}

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void RcExecute( void)
// Execute - call periodically
{
//   if(TimerBefore(SysTimer(), LastActivityTime + SESSION_TIMEOUT)) {
//      return;
//   }
//   RcSessionTerminate( ActiveSessionId);
} // RcExecute

//------------------------------------------------------------------------------
//  Command execution
//------------------------------------------------------------------------------

TYesNo RcCommand(TRcId *RcId, TRcCmd *Cmd, word CmdSize, TRcReply *Reply, word *ReplySize)
// Execute command
{
TYesNo Success = YES;
int ActionReplySize;
int i;
TFile *File;
   if(!RcSessionIsActive( RcId->SessionId)) {
      return NO;
   }

   *ReplySize = RcReplySimpleSize();

   LastActivityTime = SysTimer();

   switch( Cmd->Cmd) {
      case RC_CMD_ACTION:
#ifndef __WIN32__
         for(i = 0 ; i < RC_FILES_COUNT ; i++) { // no open file
            if(RcFile[i].Opened) {
               return NO;
            }
         }
         if(!ActionExecute(&Cmd->Data.Action.Action, RcRcToActionSize( CmdSize), &Reply->Data.Action.Action, &ActionReplySize)) {
            return NO;
         }
         *ReplySize = RcActionToRcSize( ActionReplySize);
#endif
         break;

      case RC_CMD_FILE_OPEN:
         if(CmdSize != RcCmdSize( FileOpen)) {
            return NO;
         }
         Success = NO;
         for(i = 0 ; i < RC_FILES_COUNT ; i++) {
            if(RcFile[i].Opened) {
               continue;
            }
            if(!FileOpen( &RcFile[i].File, Cmd->Data.FileOpen.FileName, Cmd->Data.FileOpen.Mode)) {
               break;
            }
            RcFile[i].Opened = YES;
            RcFile[i].Name = Cmd->Data.FileOpen.FileName;
            RcFile[i].OwnerSessionId = RcId->SessionId;
            Success = YES;
            break;
         }
         break;

      case RC_CMD_FILE_CLOSE:
         if(CmdSize != RcCmdSize( FileClose)) {
            return NO;
         }
         File = Check( Cmd->Data.FileName, RcId->SessionId);
         if(!File) {
            Success = NO;
            break;
         }
         FileClose( File);
         Success = NO;
         for( i = 0 ; i < RC_FILES_COUNT ; i++) {
            if(RcFile[i].Name == Cmd->Data.FileName) {
               RcFile[i].Opened = NO;
               Success = YES;
               break;
            }
         }
         break;

      case RC_CMD_FILE_SAVE:
         if(CmdSize != RcCmdFileSaveSize( Cmd->Data.FileSave.Size)) {
            return NO;
         }
         File = Check( Cmd->Data.FileName, RcId->SessionId);
         if(!File) {
            Success = NO;
            break;
         }
         Success = FileSave( File, Cmd->Data.FileSave.Address, Cmd->Data.FileSave.Data, Cmd->Data.FileSave.Size);
         break;

      case RC_CMD_FILE_LOAD:
         if(CmdSize != RcCmdSize( FileLoad)) {
            return NO;
         }
         File = Check( Cmd->Data.FileName, RcId->SessionId);
         if(!File) {
            Success = NO;
            break;
         }
         *ReplySize = RcReplyFileLoadSize( Cmd->Data.FileLoad.Size);
         FileLoad( File, Cmd->Data.FileLoad.Address, Reply->Data.FileLoad.Data, Cmd->Data.FileLoad.Size);
         break;

      /*case RC_CMD_FILE_MATCH:
         if(CmdSize != RcCmdFileMatchSize( Cmd->Data.FileMatch.Size)) {
            return NO;
         }
         File = Check( Cmd->Data.FileName, RcId->SessionId);
         if(!File) {
            Success = NO;
            break;
         }
         *ReplySize = RcReplySize( FileMatch);
         Reply->Data.FileMatch.Match  = FileMatch( File, Cmd->Data.FileMatch.Address, Cmd->Data.FileMatch.Data, Cmd->Data.FileMatch.Size);
         break;

      case RC_CMD_FILE_FILL:
         if(CmdSize != RcCmdSize( FileFill)) {
            return NO;
         }
         File = Check( Cmd->Data.FileName, RcId->SessionId);
         if(!File) {
            Success = NO;
            break;
         }
         Success = FileFill( File, Cmd->Data.FileFill.Address, Cmd->Data.FileFill.Pattern, Cmd->Data.FileFill.Size);
         break;
*/
      case RC_CMD_FILE_SUM:
         if(CmdSize != RcCmdSize( FileSum)) {
            return NO;
         }
         File = Check( Cmd->Data.FileName, RcId->SessionId);
         if(!File) {
            Success = NO;
            break;
         }
         *ReplySize = RcReplySize( FileSum);
         Reply->Data.FileSum.Sum = FileSum( File, Cmd->Data.FileSum.Address, Cmd->Data.FileSum.Size);
         break;
/*
      case RC_CMD_FILE_MOVE:
         if(CmdSize != RcCmdSize( FileMove)) {
            return NO;
         }
         File = Check( Cmd->Data.FileName, RcId->SessionId);
         if(!File) {
            Success = NO;
            break;
         }
         Success = FileMove( File, Cmd->Data.FileMove.ToAddress, Cmd->Data.FileMove.FromAddress, Cmd->Data.FileMove.Size);
         break;*/

      case RC_CMD_FILE_COMMIT:
         if(CmdSize != RcCmdSize( FileCommit)) {
            return NO;
         }
         File = Check( Cmd->Data.FileName, RcId->SessionId);
         if(!File) {
            Success = NO;
            break;
         }
         Success = FileCommit( File);
         break;

      default:
         return NO;
   }
   Reply->Reply = RcReply( Cmd->Cmd, Success);
   return YES;
} // RcExecute

#ifndef __WIN32__
//------------------------------------------------------------------------------
//  No session command execution
//------------------------------------------------------------------------------
#include "SmsGate/SmsGate.h"

TYesNo RcNoSessionCommand( TRcCmd *Cmd, word CmdSize, TRcReply *Reply, word *ReplySize)
// Execute command without the need of session
{
TYesNo Success = YES;

   if( Cmd->Cmd < _RC_CMD_NO_SESSION){
      return NO;
   }

   *ReplySize = RcReplySimpleSize();

   switch( Cmd->Cmd) {
      case RC_CMD_GSM_SEND :
         Reply->Data.GsmSend.SlotNumber = SmsGateSmsSend( Cmd->Data.GsmSend.SmsText, NULL);
         *ReplySize = RcReplySize(GsmSend);
         break;
      case RC_CMD_GSM_SEND_TO :
         Reply->Data.GsmSendTo.SlotNumber = SmsGateSmsSend( Cmd->Data.GsmSendTo.SmsText, Cmd->Data.GsmSendTo.PhoneNumber);
         *ReplySize = RcReplySize(GsmSendTo);
         break;
      case RC_CMD_GSM_STATUS :
         if( Cmd->Data.GsmStatus.SlotNumber >= SMS_GATE_SMS_SLOTS){
            Success = NO;
            break;
         }
         Reply->Data.GsmStatus.SmsStatus = SmsGateSmsStatus( Cmd->Data.GsmStatus.SlotNumber);
         *ReplySize = RcReplySize(GsmStatus);
         break;
      default :
         Success = NO;
         break;
   }

   Reply->Reply = RcReply(Cmd->Cmd, Success);
   return YES;

}
#endif
//******************************************************************************

//------------------------------------------------------------------------------
//  Check handle
//------------------------------------------------------------------------------

static TFile *Check( TFileName FileName, byte OwnerSessionId)
// 
{
int i;
TYesNo Found = NO;
   for( i = 0 ; i < RC_FILES_COUNT ; i++) {
      if(RcFile[i].Name == FileName) {
         Found = YES;
         break;
      }
   }
   if(!Found) {
      return 0;
   }
   if( !RcFile[i].Opened) {
      return 0;
   }
   if( RcFile[i].OwnerSessionId != OwnerSessionId) {
      return 0;
   }
   return &RcFile[i].File;
} // CheckHandle

//------------------------------------------------------------------------------
//  Close files
//------------------------------------------------------------------------------

void CloseFiles( byte SessionId)
// Close <SessionId> files
{
   CloseAllFiles();
} // CloseFiles

//------------------------------------------------------------------------------
//  Close all files
//------------------------------------------------------------------------------

void CloseAllFiles( void)
// Close all files
{
int i;
   for(i = 0 ; i < RC_FILES_COUNT ; i++) {
      FileClose( &RcFile[i].File);
      memset(&RcFile[i], 0, sizeof(RcFile[i]));
   }
} // CloseAllFiles
