//******************************************************************************
//
//   PredefinedList.c  Predefined weighings list
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "PredefinedList.h"
#include "Data/uNamedList.h"
#include "Weighing/WeighingConfiguration.h"
#include "Multitasking/Multitasking.h"
#include <string.h>

// data without name :
#define _PredefinedWeighingDataSize()       (sizeof( TPredefinedWeighing) - PREDEFINED_WEIGHING_NAME_SIZE - 1)
#define _PredefinedWeighingDataAddress( c)  ((char *)c + PREDEFINED_WEIGHING_NAME_SIZE + 1)

uNamedListAlloc( _PredefinedListDescriptor, PREDEFINED_WEIGHING_COUNT, _PredefinedWeighingDataSize());

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo PredefinedListRemoteLoad(void)
// Load from remote device
{
   if (!uNamedListRemCopy((UNamedListDescriptor *)&_PredefinedListDescriptor, FILE_PREDEFINED_WEIGHING_LOCAL, FILE_PREDEFINED_WEIGHING_REMOTE)){
      return NO;
   }

   return YES;
} // PredefinedListRemoteLoad

//------------------------------------------------------------------------------
//  Remote save
//------------------------------------------------------------------------------

TYesNo PredefinedListRemoteSave( void)
// Save to remote device
{
   return uNamedListRemCopy((UNamedListDescriptor *)&_PredefinedListDescriptor, FILE_PREDEFINED_WEIGHING_REMOTE, FILE_PREDEFINED_WEIGHING_LOCAL);
} // PredefinedListRemoteSave

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void PredefinedListInit( void)
// Initialize
{
   uNamedListInit( &_PredefinedListDescriptor, FILE_PREDEFINED_WEIGHING_LOCAL);
} // PredefinedListInit

TYesNo PredefinedListOpen( TPredefinedList *PredefinedList) {
   TYesNo Success;
   Success = uNamedListOpen( PredefinedList, &_PredefinedListDescriptor, FILE_PREDEFINED_WEIGHING_LOCAL);
   if(!Success) {
      MultitaskingReschedule();
   }
   return Success;
}

void PredefinedListClose( TPredefinedList *PredefinedList) {
   uNamedListClose( PredefinedList);
}

//------------------------------------------------------------------------------
//   Identifier
//------------------------------------------------------------------------------

TPredefinedWeighingIdentifier PredefinedListIdentifier( TPredefinedList *PredefinedList, TPredefinedWeighingIndex Index)
// Returns identifier of predefined weighing at <Index>
{
   return( uDirectoryItemIdentifier( PredefinedList, Index));
} // PredefinedListIdentifier

//------------------------------------------------------------------------------
//   Index
//------------------------------------------------------------------------------

TPredefinedWeighingIndex PredefinedListIndex( TPredefinedList *PredefinedList, TPredefinedWeighingIdentifier Identifier)
// Returns index of predefined weighing by predefined weighing <Identifier>
{
   return( uDirectoryFindIdentifier( PredefinedList, Identifier));
} // PredefinedListIndex

//------------------------------------------------------------------------------
//   Name
//------------------------------------------------------------------------------

TYesNo PredefinedListName( TPredefinedList *PredefinedList, TPredefinedWeighingIdentifier Identifier, char *Name)
// Returns predefined weighing <Name>
{
TPredefinedWeighingIndex Index;

   Index = uDirectoryFindIdentifier( PredefinedList, Identifier);
   if( Index == UDIRECTORY_INDEX_INVALID){
      strcpy( Name, "?");
      return( NO);
   }
   uDirectoryItemName( PredefinedList, Index, Name);
   return( YES);
} // PredefinedListName

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

void PredefinedListLoad( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIndex Index)
// Load <Predefined> by <Index>
{
TPredefinedWeighingIdentifier Identifier;

   Identifier = uDirectoryItemIdentifier( PredefinedList, Index);
   uNamedListLoad( PredefinedList, Identifier, _PredefinedWeighingDataAddress( Predefined));
   uDirectoryItemName( PredefinedList, Index, Predefined->Name);
   Predefined->Predefined = Identifier;          // set own list identifier
} // PredefinedListLoad

//------------------------------------------------------------------------------
//   Save
//------------------------------------------------------------------------------

void PredefinedListSave( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIndex Index)
// Save <Predefined> at <Index>
{
TPredefinedWeighingIdentifier Identifier;

   Identifier = uDirectoryItemIdentifier( PredefinedList, Index);
   uDirectoryItemName( PredefinedList, Index, Predefined->Name);
   Predefined->Predefined = Identifier;          // save own list identifier
   uNamedListSave( PredefinedList, Identifier, _PredefinedWeighingDataAddress( Predefined));
} // PredefinedListSave

//------------------------------------------------------------------------------
//   Changed
//------------------------------------------------------------------------------

TYesNo PredefinedListChanged( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIndex Index)
// Returns YES if <Predefined> don't match data at <Index>
{
TPredefinedWeighingIdentifier Identifier;
char Name[ PREDEFINED_WEIGHING_NAME_SIZE - 1];

   Identifier = uDirectoryItemIdentifier( PredefinedList, Index);
   uDirectoryItemName( PredefinedList, Index, Name);
   if( !strequ( Predefined->Name, Name)){
      return( YES);                    // item name changed
   }
   return( uNamedListChanged( PredefinedList, Identifier, _PredefinedWeighingDataAddress( Predefined)));
} // PredefinedListChanged

//------------------------------------------------------------------------------
//   Load by identifier
//------------------------------------------------------------------------------

TYesNo PredefinedListIdentifierLoad( TPredefinedList *PredefinedList, TPredefinedWeighing *Predefined, TPredefinedWeighingIdentifier Identifier)
// Load <Predefined> by <Identifier>
{
TPredefinedWeighingIndex Index;

   // initialize with defaults :
   PredefinedListDefault( Predefined);
   if( Identifier == ULIST_IDENTIFIER_INVALID){
      return( NO);
   }
   // get directory index :
   Index = uDirectoryFindIdentifier( PredefinedList, Identifier);
   if( Index == UDIRECTORY_INDEX_INVALID){
      return( NO);
   }
   PredefinedListLoad( PredefinedList, Predefined, Index);
   return( YES);
} // PredefinedListIdentifierLoad

/*
//------------------------------------------------------------------------------
//   Save by identifier
//------------------------------------------------------------------------------

TYesNo PredefinedListIdentifierSave( TPredefinedWeighing *Predefined, TPredefinedWeighingIdentifier Identifier)
// Save <Predefined> by <Identifier>
{
TPredefinedWeighingIndex Index;

   if( Identifier == ULIST_IDENTIFIER_INVALID){
      return( NO);
   }
   // get directory index :
   Index = uDirectoryFindIdentifier( &_PredefinedList, Identifier);
   if( Index == UDIRECTORY_INDEX_INVALID){
      return( NO);
   }
   PredefinedListSave( Predefined, Index);
   return( YES);
} // PredefinedListIdentifierSave
*/

//------------------------------------------------------------------------------
//   Default
//------------------------------------------------------------------------------

void PredefinedListDefault( TPredefinedWeighing *Predefined)
// Load <Predefined> with defaults
{
   *Predefined = PredefinedWeighingDefault;
} // PredefinedListDefault
