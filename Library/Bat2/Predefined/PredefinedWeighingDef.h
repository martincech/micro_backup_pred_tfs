//******************************************************************************
//
//   PredefinedWeighingDef.h Predefined weighing data
//   Version 1.0             (c) VEIT Electronics
//
//******************************************************************************

#ifndef __PredefinedWeighingDef_H__
   #define __PredefinedWeighingDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __WeighingConfigurationDef_H__
   #include "Weighing/WeighingConfigurationDef.h"
#endif

#ifndef __uNamedListDef_H__
   #include "Data/uNamedListDef.h"
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define PREDEFINED_WEIGHING_NAME_SIZE          WEIGHING_CONFIGURATION_NAME_SIZE

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef UDirectoryIndex                  TPredefinedWeighingIndex;

//------------------------------------------------------------------------------
//  Predefined weighing
//------------------------------------------------------------------------------

typedef TWeighingConfiguration           TPredefinedWeighing;
typedef TWeighingConfigurationIdentifier TPredefinedWeighingIdentifier;

#define PredefinedWeighingDefault        WeighingConfigurationDefault

#endif
