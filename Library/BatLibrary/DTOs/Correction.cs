﻿namespace DTOs
{
   public class Correction
   {
      public bool UseCurve { get; set; }
      public short Day1 { get; set; }
      public short Day2 { get; set; }
      public double UniformityRange { get; set; }
   }
}
