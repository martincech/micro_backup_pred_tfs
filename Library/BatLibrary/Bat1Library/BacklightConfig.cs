﻿namespace Bat1Library
{
   /// <summary>
   /// Backlight configuration
   /// </summary>
   public struct BacklightConfig {
      public BacklightMode Mode;		               // TBacklightMode Backlight mode   
      public int           Intensity;	               // Backlight intensity
      public int           Duration;                   // Backlight duration [s]
   }
}