namespace Bat1Library
{
   /// <summary>
   /// Constants
   /// </summary>
   public class ScaleConfigConst {
      /// <summary>
      /// Maximum length of the scale name
      /// </summary>
      public const int SCALE_NAME_LENGTH	= 15;

      /// <summary>
      /// Number of password keys
      /// </summary>
      public const int PASSWORD_LENGTH = 4;
        
      /// <summary>
      /// volume 0..MAX
      /// </summary>
      public const int VOLUME_MAX = 10;

      /// <summary>
      /// backlight intensity 0..MAX
      /// </summary>
      public const int BACKLIGHT_MAX = 20;
        
      /// <summary>
      /// contrast intensity 0..MAX
      /// </summary>
      public const int CONTRAST_MAX = 64;
        
      /// <summary>
      /// backlight duration [s]
      /// </summary>
      public const int BACKLIGHT_DURATION_MAX = 300;

      /// <summary>
      /// Print constants - Max. paper width
      /// </summary>
      public const int PAPER_WIDTH_MAX = 99;

      /// <summary>
      /// Max. power off timeout in minutes
      /// </summary>
      public const int POWER_OFF_TIMEOUT_MAX = (99 * 60);

      /// <summary>
      /// maximal number of birds
      /// </summary>
      public const int NUMBER_OF_BIRDS_MAX = 99;

      /// <summary>
      /// Maximum filter * 0.1s
      /// </summary>
      public const int FILTER_MAX = 50;

      /// <summary>
      /// Maximum stabilisation * 0.1s
      /// </summary>
      public const int STABILISATION_TIME_MAX = 50;
   }
}