namespace Bat1Library
{
   public enum SampleFlag
   {
      NONE,
      LIGHT,
      OK,
      HEAVY,
      MALE,
      FEMALE,
   };
}