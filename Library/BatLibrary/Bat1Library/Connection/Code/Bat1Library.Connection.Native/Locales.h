#pragma once
#include "Bat1Dll.h"

namespace Bat1Library
{
   namespace Connection
   {
      namespace Native
      {
         using namespace System;
         using namespace System::Text;

         static public ref class Locales
         {
         public:
            static String^ GetCountryName(int CountryCode);
            static int GetLanguage(int Country);         
            static int GetCodePage(int Language);       
            static int GetDateFormat(int Country);       
            static wchar_t GetDateSeparator1(int Country);       
            static wchar_t GetDateSeparator2(int Country);
            static int GetTimeFormat(int Country);       
            static wchar_t GetTimeSeparator(int Country);
            static int GetDaylightSavingType(int Country);
         };
      }
   }
}