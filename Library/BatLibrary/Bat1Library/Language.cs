namespace Bat1Library
{
   public enum Language
   {
      CZECH,
      DUTCH,
      ENGLISH,
      FINNISH,
      FRENCH,
      GERMAN,
      HUNGARIAN,
      ITALIAN,
      JAPANESE,
      POLISH,
      PORTUGUESE,
      RUSSIAN,
      SPANISH,
      TURKISH
   };
}