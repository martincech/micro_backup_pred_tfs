namespace Bat1Library
{
   public enum TimeFormat
   {
      HOUR24,
      HOUR12,
   };
}