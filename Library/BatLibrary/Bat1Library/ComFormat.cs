namespace Bat1Library
{
   public enum ComFormat
   {
      COM_8BITS,
      COM_8BITS_EVEN,
      COM_8BITS_ODD,
      COM_8BITS_MARK,
      COM_8BITS_SPACE,
      COM_7BITS,
      COM_7BITS_EVEN,
      COM_7BITS_ODD,
      COM_7BITS_MARK,
      COM_7BITS_SPACE,
   };
}