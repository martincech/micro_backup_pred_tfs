﻿namespace Bat1Library
{
   /// <summary>
   /// Backlight mode
   /// </summary>
   public enum BacklightMode {
      BACKLIGHT_MODE_AUTO,                // Backlight in automatic mode
      BACKLIGHT_MODE_ON,	                // Backlight is always on
      BACKLIGHT_MODE_OFF,	                // Backlight is always off
      _BACKLIGHT_MODE_COUNT
   }
}