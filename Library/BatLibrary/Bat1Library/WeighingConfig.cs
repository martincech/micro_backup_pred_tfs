namespace Bat1Library
{
   /// <summary>
   /// Weighing configuration
   /// </summary>
   public struct WeighingConfig
   {
      public WeightSortingConfig WeightSorting; // Weight limits mode and weights
      public SavingConfig Saving; // Saving parameters
   }
}