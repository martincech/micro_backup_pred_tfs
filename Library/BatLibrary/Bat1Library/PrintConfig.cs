namespace Bat1Library
{
   /// <summary>
   /// Print configuration
   /// </summary>
   public struct PrintConfig {
      public int       PaperWidth;                    // Paper width [mm]
      public ComFormat CommunicationFormat;           // Serial format TComFormat enum
      public int       CommunicationSpeed;            // Serial speed [Bd]
   }
}