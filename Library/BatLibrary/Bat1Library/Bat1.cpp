//******************************************************************************
//
//   Bat1.cpp         Bat1 advanced services
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include <windows.h>
#include <stdio.h>

#include "Hardware.h"
#include "Bat1.h"
#include "ARM/Inc/Dt.h"

#include "ARM/Bat1/Bat1.h"        // default constants
#include "ARM/Bat1/BeepDef.h"     // beep constants
#include "ARM/Bat1/ConfigSet.c"   // configuration defaults

#ifndef BAT1_NOFS
   #include "ARM/Inc/File/Fd.h"
   #include "ARM/Inc/File/Fs.h"
#endif

// Read/Write large memory constants
#define MEMORY_CHUNK_BLOCKS 50                                     // chunk USB blocks count
#define MEMORY_CHUNK_SIZE   (MEMORY_CHUNK_BLOCKS * USB_WRITE_SIZE) // chunk size
#define MEMORY_RETRY        3                                      // trials count

//******************************************************************************
// Constructor
//******************************************************************************

TBat1::TBat1() : TBatDevice()
// Constructor
{
   // clear files/groups
   memset( File,  0, sizeof( File));
   memset( Group, 0, sizeof( Group));   
   // initialize
   Dispose();
} // TBat1

//******************************************************************************
// Destructor
//******************************************************************************

TBat1::~TBat1()
// Destructor
{
   Dispose();
} // ~TBat1

//******************************************************************************
// New device
//******************************************************************************

void TBat1::NewDevice()
// Create empty device
{
   Dispose();
} // NewDevice

//******************************************************************************
// Load
//******************************************************************************

BOOL TBat1::Load( BOOL ReadSamples)
// Load data from Bat1
{
   Dispose();                          // empty device
   // Load configuration
   if( !ReadMemory( offsetof( TEeprom, Config), &Config, sizeof( Config))){
      return( FALSE);
   }
   // Load directory
   TUsbDirectoryItem Item;
   int               Count;
   if( !DirectoryBegin( Count)){
      return( FALSE);
   }
   for( int i = 0; i < Count; i++){
      if( !DirectoryNext( &Item)){
         return( FALSE);
      }
      if( Item.Class == SDB_CLASS){
         File[ FilesCount].Info = Item;
         FilesCount++;         
      } else if( Item.Class == GRP_CLASS){
         Group[ GroupsCount].Info = Item;
         GroupsCount++;         
      } else {
         return( FALSE);               // unknown class
      }
   }
   int Size;
   // Load file data
   for( int i = 0; i < FilesCount; i++){
      Size  = File[ i].Info.Info.Size; // file size
      if( Size < sizeof( TSdbConfig)){
         // corrupted file
         File[ i].SamplesCount = 0;                                               // clear samples count
         File[ i].Current      = File[ i].Info.Handle == Config.Config.LastFile;  // mark as working file
         File[ i].Config       = Config.Config.WeighingParameters;                // fill with defaults
         continue;
      }
      Size -= sizeof( TSdbConfig);     // remove config
      int SamplesCount = Size / sizeof( TSdbRecord);       // samples count
      if( Size % sizeof( TSdbRecord) != 0){
         IERROR;                       // wrong file size
      }
      if( !ReadSamples){
         SamplesCount = 0;
      }
      File[ i].SamplesCount = SamplesCount;                // set samples count
      File[ i].Current      = File[ i].Info.Handle == Config.Config.LastFile;  // mark as working file
      AllocSamples( i, SamplesCount);                      // allocate samples memory
      if( !FileOpen( File[ i].Info.Handle)){
          FileClose();
          return( FALSE);
      }
      if( !FileRead( &File[ i].Config, sizeof( TSdbConfig))){
          FileClose();
          return( FALSE);
      }
      if( SamplesCount){
         if( !FileRead( File[ i].Sample, SamplesCount * sizeof( TSdbRecord))){
             FileClose();
             return( FALSE);
         }
      }
      FileClose();
   }
   // Load group data
   for( int i = 0; i < GroupsCount; i++){
      Size  = Group[ i].Info.Info.Size; // file size
      if( Size != sizeof( TGroup)){
         IERROR;
      }
      if( !FileOpen( Group[ i].Info.Handle)){
          FileClose();
          return( FALSE);
      }
      if( !FileRead( &Group[ i].Group, sizeof( TGroup))){
          FileClose();
          return( FALSE);
      }
      FileClose();
      LoadGroup( i);                   // decode data
   }
   return( TRUE);
} // Load

//******************************************************************************
// Save
//******************************************************************************

BOOL TBat1::Save()
// Save data to Bat1
{
   // filesystem first
   if( TouchFilesystem){
      if( !FormatFilesystem()){
         return( FALSE);
      }
      // save files
      for( int i = 0; i < FilesCount; i++){
         int Handle;
         if( !FileCreate( File[ i].Info.Info.Name, File[ i].Info.Info.Note, SDB_CLASS, Handle)){
            FileClose();
            return( FALSE);
         }
         if( !FileWrite( &File[ i].Config, sizeof( TSdbConfig))){
             FileClose();
             return( FALSE);
         }
         int SamplesCount = File[ i].SamplesCount;
         if( SamplesCount){
            if( !FileWrite( File[ i].Sample, SamplesCount * sizeof( TSdbRecord))){
                FileClose();
                return( FALSE);
            }
         }
         FileClose();
         File[ i].Info.Handle = Handle;          // remember handle
      }
      // save groups
      for( int i = 0; i < GroupsCount; i++){
         SaveGroup( i);                          // to internal representation
         int Handle;
         if( !FileCreate( Group[ i].Info.Info.Name, Group[ i].Info.Info.Note, GRP_CLASS, Handle)){
            FileClose();
            return( FALSE);
         }
         if( !FileWrite( &Group[ i].Group, sizeof( TGroup))){
             FileClose();
             return( FALSE);
         }
         FileClose();
         Group[ i].Info.Handle = Handle;          // remember handle
      }
   }
   // update working file
   for( int i = 0; i < FilesCount; i++){
      if( File[ i].Current){
         if( Config.Config.LastFile != File[ i].Info.Handle){
            Config.Config.LastFile = (TFdirHandle)File[ i].Info.Handle;
            TouchConfig = TRUE;
         }
         break;
      }
   }
   // save config
   if( TouchConfig){
      CalcConfigCrc();
      if( !WriteMemory( offsetof( TEeprom, Config), &Config, sizeof( Config))){
         return( FALSE);
      }
      ReloadConfig();                  // update working configuration
   }
   return( TRUE);
} // Save

//******************************************************************************
// Load estimation
//******************************************************************************

BOOL TBat1::LoadEstimation( int &Promile)
// Load size estimation [%%]
{
   int Size = 0;
   TUsbDirectoryItem Item;
   int               Count;
   // search for directory items
   if( !DirectoryBegin( Count)){
      return( FALSE);
   }
   for( int i = 0; i < Count; i++){
      if( !DirectoryNext( &Item)){
         return( FALSE);
      }
      if( Item.Class == SDB_CLASS){
         Size += Item.Info.Size;
      }
   }
   // promile of total file space size
   Promile = (Size * 1000) / (FAT_SIZE * FS_BLOCK_SIZE);
   return( TRUE);
} // LoadEstimation

//******************************************************************************
// Save estimation
//******************************************************************************

void TBat1::SaveEstimation( int &Promile)
// Save size estimation [%%]
{
   // get total byte size
   int Size = 0;
   for( int i = 0; i < FilesCount; i++){
      Size += File[ i].SamplesCount * sizeof( TSdbRecord) + sizeof( TSdbConfig);
   }
   // promile of total file space size
   Promile = (Size * 1000) / (FAT_SIZE * FS_BLOCK_SIZE);
} // SaveEstimation

//******************************************************************************
// Crash
//******************************************************************************

BOOL TBat1::LoadCrashInfo()
// Read exception & watchdog info
{
   if( !ReadMemory( offsetof( TEeprom, Exception), &ExceptionInfo, sizeof( TExceptionInfo))){
      return( FALSE);
   }
   if( !ReadMemory( offsetof( TEeprom, WatchDog),  &WatchDogInfo,  sizeof( TWatchDogInfo))){
      return( FALSE);
   }
   return( TRUE);
} // LoadCrashInfo

//******************************************************************************
// Read Memory
//******************************************************************************

BOOL TBat1::ReadLargeMemory( int Address, void *Buffer, int Size)
// Read large chunk of the EEPROM
{
   int  ChunkCount = Size / MEMORY_CHUNK_SIZE;
   int  LastSize   = Size % MEMORY_CHUNK_SIZE;
   int  Offset     = 0;
   byte *p         = (byte *)Buffer;
   BOOL Done       = FALSE;
   // read chunks
   for( int i = 0; i < ChunkCount; i++){
      Done = FALSE;
      for( int j = 0; j < MEMORY_RETRY; j++){
         if( ReadMemory( Address + Offset, &p[ Offset], MEMORY_CHUNK_SIZE)){
            Done = TRUE;
            break;
         }
      }
      if( !Done){
         return( FALSE);               // read error
      }
      Offset += MEMORY_CHUNK_SIZE;
   }
   // read remainder
   if( !LastSize){
      return( TRUE);                   // done
   }
   Done = FALSE;
   for( int j = 0; j < MEMORY_RETRY; j++){
      if( ReadMemory( Address + Offset, &p[ Offset], LastSize)){
         Done = TRUE;
         break;
      }
   }
   return( Done);
} // ReadLargeMemory

//******************************************************************************
// Write Memory
//******************************************************************************

BOOL TBat1::WriteLargeMemory( int Address, void *Buffer, int Size)
// Write large chunk of the EEPROM data
{
   int  ChunkCount = Size / MEMORY_CHUNK_SIZE;
   int  LastSize   = Size % MEMORY_CHUNK_SIZE;
   int  Offset     = 0;
   byte *p         = (byte *)Buffer;
   BOOL Done       = FALSE;
   // read chunks
   for( int i = 0; i < ChunkCount; i++){
      Done = FALSE;
      for( int j = 0; j < MEMORY_RETRY; j++){
         if( WriteMemory( Address + Offset, &p[ Offset], MEMORY_CHUNK_SIZE)){
            Done = TRUE;
            break;
         }
      }
      if( !Done){
         return( FALSE);               // read error
      }
      Offset += MEMORY_CHUNK_SIZE;
   }
   // read remainder
   if( !LastSize){
      return( TRUE);                   // done
   }
   Done = FALSE;
   for( int j = 0; j < MEMORY_RETRY; j++){
      if( WriteMemory( Address + Offset, &p[ Offset], LastSize)){
         Done = TRUE;
         break;
      }
   }
   return( Done);
} // WriteLargeMemory

//******************************************************************************
// Files Delete
//******************************************************************************

void TBat1::FilesDelete()
// Delete all files
{
   // deallocate samples :
   for( int i = 0; i < FDIR_SIZE; i++){
      if( File[ i].Sample){
         delete [] File[i].Sample;
      }
   }
   FilesCount = 0;
   memset( File,  0, sizeof( File));
} // FilesDelete

//******************************************************************************
// Groups Delete
//******************************************************************************

void TBat1::GroupsDelete()
// Delete all groups
{
   GroupsCount = 0;
   memset( Group, 0, sizeof( Group));
} // GroupsDelete

//******************************************************************************
// Alloc Samples
//******************************************************************************

void TBat1::AllocSamples( int FileIndex, int SamplesCount)
// Alloc memory for samples
{
   ClearSamples( FileIndex);
   TFileEntry *Fe = &File[ FileIndex];
   if( SamplesCount){
      Fe->Sample = new TSdbRecord[ SamplesCount];  // samples memory
   }
   Fe->SamplesCount = SamplesCount;
} // AllocSamples

//******************************************************************************
// Clear Samples
//******************************************************************************

void TBat1::ClearSamples( int FileIndex)
// Clear all samples
{
   TFileEntry *Fe = &File[ FileIndex];
   if( Fe->Sample){
      delete [] Fe->Sample;
      Fe->Sample       = 0;
   }
   Fe->SamplesCount = 0;
} // ClearSamples

//******************************************************************************
// Decode Time
//******************************************************************************

BOOL TBat1::DeviceByEeprom( void *Buffer)
// Load device by EEPROM contents in <Buffer>
{
   Dispose();
   // load config
   byte *p = (byte *)Buffer;
   p += offsetof( TEeprom, Config);
   Config = *(TConfigUnion *)p;
   // load crash data
   p  = (byte *)Buffer;
   p += offsetof( TEeprom, Exception);
   ExceptionInfo = *(TExceptionInfo *)p;
   p  = (byte *)Buffer;
   p += offsetof( TEeprom, WatchDog);
   WatchDogInfo = *(TWatchDogInfo *)p;
#ifndef BAT1_NOFS
   _Eeprom = (byte *)Buffer;               // set memory array
   FsInit();                               // initialize filesystem
   // read directory
   FdSetClass( FDIR_CLASS_WILDCARD);       // any file
   int Count = FdCount();                  // nuber of directory entries
   FdFindBegin();                          // start directory read
   for( int i = 0; i < Count; i++){
      TFdirHandle Handle = FdFindNext();
      TFdirClass  Class  = FdGetClass( Handle);
      if( Class == SDB_CLASS){
         File[ FilesCount].Info.Handle = Handle;
         File[ FilesCount].Info.Class  = Class;
         FdLoad( Handle, &File[ FilesCount].Info.Info);
         FilesCount++;         
      } else if( Class == GRP_CLASS){
         Group[ GroupsCount].Info.Handle = Handle;
         Group[ GroupsCount].Info.Class  = Class;
         FdLoad( Handle, &Group[ GroupsCount].Info.Info);
         GroupsCount++;         
      } else {
         return( FALSE);               // unknown class
      }
   }
   // Load file data
   int Size;
   for( int i = 0; i < FilesCount; i++){
      Size  = File[ i].Info.Info.Size; // file size
      if( Size == 0){
         // corrupted file
         File[ i].SamplesCount = 0;                                               // clear samples count
         File[ i].Current      = File[ i].Info.Handle == Config.Config.LastFile;  // mark as working file
         File[ i].Config       = Config.Config.WeighingParameters;                // fill with defaults
         continue;
      }
      Size -= sizeof( TSdbConfig);     // remove config
      if( Size < 0){
         IERROR;                       // wrong file config size
      }
      if( Size % sizeof( TSdbRecord) != 0){
         IERROR;                       // wrong file size
      }
      int SamplesCount = Size / sizeof( TSdbRecord);       // samples count
      File[ i].SamplesCount = SamplesCount;                // set samples count
      File[ i].Current      = File[ i].Info.Handle == Config.Config.LastFile;  // mark as working file
      AllocSamples( i, SamplesCount);                      // allocate samples memory
      FsOpen( (TFdirHandle)File[ i].Info.Handle, YES);
      if( !FsRead( &File[ i].Config, sizeof( TSdbConfig))){
          FsClose();
          return( FALSE);
      }
      if( SamplesCount){
         if( !FsRead( File[ i].Sample, SamplesCount * sizeof( TSdbRecord))){
             FsClose();
             return( FALSE);
         }
      }
      FsClose();
   }
   // Load group data
   for( int i = 0; i < GroupsCount; i++){
      Size  = Group[ i].Info.Info.Size; // file size
      if( Size != sizeof( TGroup)){
         IERROR;
      }
      FsOpen( (TFdirHandle)Group[ i].Info.Handle, YES);
      if( !FsRead( &Group[ i].Group, sizeof( TGroup))){
          FsClose();
          return( FALSE);
      }
      FsClose();
      LoadGroup( i);                   // decode data
   }
#endif
   return( TRUE);
} // DeviceByEeprom

//******************************************************************************
// Decode Time
//******************************************************************************

void TBat1::DecodeTime( TTimestamp Timestamp, int &Day, int &Month, int &Year, int &Hour, int &Min, int &Sec)
// Decode <Timestamp> to items
{
   TLocalTime Local;
   DtDecode( Timestamp, &Local);
   Day   = Local.Day;
   Month = Local.Month;
   Year  = Local.Year + 2000;
   Hour  = Local.Hour;
   Min   = Local.Min;
   Sec   = Local.Sec;
} // DecodeTime

//******************************************************************************
// Encode Time
//******************************************************************************

TTimestamp TBat1::EncodeTime( int Day, int Month, int Year, int Hour, int Min, int Sec)
// Returns timestamp by items
{
   TLocalTime Local;
   Local.Day   = Day;
   Local.Month = Month;
   Local.Year  = Year - 2000;
   Local.Hour  = Hour;
   Local.Min   = Min;
   Local.Sec   = Sec;
   return( DtEncode( &Local));
} // EncodeTime

//-----------------------------------------------------------------------------

//******************************************************************************
// Dispose
//******************************************************************************

void TBat1::Dispose()
// Dispose allocated memory
{
   FilesDelete();
   GroupsDelete();
   TouchConfig      = FALSE;
   TouchFilesystem  = FALSE;
   // cleanup config :
   memset( &Config, 0, sizeof( Config));
   Config.Config = _DefaultConfig;               // fill with defaults
} // Dispose

//******************************************************************************
// Config CRC
//******************************************************************************

void TBat1::CalcConfigCrc()
// Recalculate Config CRC
{
byte       *p;
TConfigCrc Crc;

   Crc  = 0;
   p   = (byte *)&Config;
   for( int i = 0; i < CONFIG_DATA_SIZE; i++){
      Crc += *p;
      p++;
   }
   Config.Spare.CheckSum = ~Crc;
} // CalcConfigCrc


//******************************************************************************
// Load group
//******************************************************************************

void TBat1::LoadGroup( int GroupIndex)
// Decode group data
{
   TGroupEntry *Ge = &Group[ GroupIndex];
   for( int i = 0; i < FilesCount; i++){
      if( !GroupContains( Ge->Group, File[ i].Info.Handle)){
         continue;
      }
      // add file index to group
      Ge->FileIndex[ Ge->FilesCount] = i;
      Ge->FilesCount++;
   }
} // LoadGroup


//******************************************************************************
// Save group
//******************************************************************************

void TBat1::SaveGroup( int GroupIndex)
// Encode group data
{
   TGroupEntry *Ge = &Group[ GroupIndex];
   for( int i = 0; i < Ge->FilesCount; i++){
      GroupAdd( Ge->Group, File[ Ge->FileIndex[ i]].Info.Handle);
   }
} // SaveGroup

//******************************************************************************
// Group add
//******************************************************************************

void TBat1::GroupAdd( TGroup Group, int Handle)
// Add <Handle> to <Group>
{
   Group[ Handle / 8] |= (byte)(1 << (Handle % 8));
} // GroupAdd

//******************************************************************************
// Group contains
//******************************************************************************

BOOL TBat1::GroupContains( TGroup Group, int Handle)
// Returns TRUE if <Handle> is in the <Group>
{
   return( Group[ Handle / 8] & (byte)(1 << (Handle % 8)));
} // GroupContains
