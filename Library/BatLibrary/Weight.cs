﻿using System;
using System.Runtime.Serialization;

namespace BatLibrary
{  
   [DataContract]
   public class Weight : IComparable
   {
      /// <summary>
      /// Weighing units
      /// </summary>
      public enum WeightUnits
      {
// ReSharper disable once InconsistentNaming
         KG,
         G,
// ReSharper disable once InconsistentNaming
         LB,
      };

      public static int GDecimalPrecision = 2;

      #region Private

      private const string DIVIDE_BY_ZERO = "Divide by zero";
      private const double LB_TO_G_MULTIPLIER = 453.59237;
      private const double KG_TO_G_MULTIPLIER = 1000;

      // ReSharper disable once InconsistentNaming
      private static double PRECISION
      {
         get { return Math.Pow(10, -GDecimalPrecision); }
      }
           
      private double internalWeight;
      

      private double InternalWeight
      {
         get { return internalWeight; }
         set
         {
            internalWeight = Math.Round(value, GDecimalPrecision, MidpointRounding.AwayFromZero);
         }
      }

      #endregion

      #region Constructors

      public Weight(double weight, WeightUnits units)
      {
         switch (units)
         {
            case WeightUnits.KG:
               InternalWeight = weight*KG_TO_G_MULTIPLIER;
               break;
            case WeightUnits.G:
               InternalWeight = weight;
               break;
            case WeightUnits.LB:
               InternalWeight = weight*LB_TO_G_MULTIPLIER;
               break;
            default:
               throw new ArgumentOutOfRangeException("units");
         }
      }
      /// <summary>
      /// Init weight in grams
      /// </summary>
      /// <param name="grams">initialization value of weight as grams</param>
      public Weight(double grams = 0)
         : this(grams, WeightUnits.G)
      {
         
      }

      public Weight(double? grams)
         : this(grams ?? 0)
      {
         
      }
      public Weight(Weight weight)
         : this(weight.AsG)
      {
      }
      #endregion

      public DateTime TimeStamp { get; set; }

      public double As(WeightUnits unit)
      {
         switch (unit)
         {
            case WeightUnits.KG:
               return AsG/KG_TO_G_MULTIPLIER;
            case WeightUnits.G:
               return AsG;
            case WeightUnits.LB:
               return AsG/LB_TO_G_MULTIPLIER;
            default:
               throw new ArgumentOutOfRangeException("unit");
         }
      }

      [DataMember]
      public double AsG
      {
         get { return InternalWeight; }
      }

      public double AsKg
      {
         get { return As(WeightUnits.KG); }
      }

      public double AsLb
      {
         get { return As(WeightUnits.LB); }
      }

      #region Operators

      public static Weight operator +(Weight c1, Weight c2)
      {
         return new Weight(c1.InternalWeight + c2.InternalWeight);
      }

      public static Weight operator +(Weight c1, double c2)
      {
         return new Weight(c1.InternalWeight + c2);
      }

      public static Weight operator +(double c1, Weight c2)
      {
         return c2 + c1;
      }

      public static Weight operator -(Weight c1, Weight c2)
      {
         return new Weight(c1.InternalWeight - c2.InternalWeight);
      }

      public static Weight operator -(Weight c1, double c2)
      {
         return new Weight(c1.InternalWeight - c2);
      }

      public static Weight operator -(double c1, Weight c2)
      {
         return new Weight(c1 - c2.InternalWeight);
      }

      public static Weight operator *(Weight c1, Weight c2)
      {
         return new Weight(c1.InternalWeight*c2.InternalWeight);
      }

      public static Weight operator *(Weight c1, double c2)
      {
         return new Weight(c1.InternalWeight * c2);
      }

      public static Weight operator *(double c1, Weight c2)
      {
         return c2*c1;
      }

      public static Weight operator /(Weight c1, Weight c2)
      {
         if (Math.Abs(c2.InternalWeight) < PRECISION) throw new ArgumentException(DIVIDE_BY_ZERO);
         return new Weight(c1.InternalWeight/c2.InternalWeight);
      }

      public static Weight operator /(Weight c1, double c2)
      {
         if (Math.Abs(c2) < PRECISION) throw new ArgumentException(DIVIDE_BY_ZERO);
         return new Weight(c1.InternalWeight/c2);
      }

      public static Weight operator /(double c1, Weight c2)
      {
         if (Math.Abs(c2.InternalWeight) < PRECISION) throw new ArgumentException(DIVIDE_BY_ZERO);
         return new Weight(c1 / c2.InternalWeight);
      }

      public static bool operator <=(Weight c1, Weight c2)
      {
         return c1.InternalWeight <= c2.InternalWeight;
      }

      public static bool operator <=(Weight c1, double c2)
      {
         return c1.InternalWeight <= Round(c2);
      }

      public static bool operator <=(double c1, Weight c2)
      {
         return Round(c1) <= c2.InternalWeight;
      }

      public static bool operator >=(Weight c1, Weight c2)
      {
         return c1.InternalWeight >= c2.InternalWeight;
      }

      public static bool operator >=(Weight c1, double c2)
      {
         return c1.InternalWeight >= Round(c2);
      }

      public static bool operator >=(double c1, Weight c2)
      {
         return Round(c1) >= c2.InternalWeight;
      }

      public static bool operator >(Weight c1, Weight c2)
      {
         return c1.InternalWeight > c2.InternalWeight;
      }

      public static bool operator >(Weight c1, double c2)
      {
         return c1.InternalWeight > Round(c2);
      }

      public static bool operator >(double c1, Weight c2)
      {
         return c2 < c1;
      }

      public static bool operator <(Weight c1, Weight c2)
      {
         return c1.InternalWeight < c2.InternalWeight;
      }

      public static bool operator <(Weight c1, double c2)
      {
         return c1.InternalWeight < Round(c2);
      }

      public static bool operator <(double c1, Weight c2)
      {
         return c2 > c1;
      }

      public static bool operator ==(Weight c1, Weight c2)
      {
         if (ReferenceEquals(c1, c2)) return true;
         if (ReferenceEquals(null, c1)) return false;
         if (ReferenceEquals(null, c2)) return false;
         return Math.Abs(c1.InternalWeight - c2.InternalWeight) < PRECISION;
      }

      public static bool operator ==(Weight c1, double c2)
      {
         if (ReferenceEquals(null, c1)) return false;
         return Math.Abs(c1.InternalWeight - c2) < PRECISION;
      }

      public static bool operator ==(double c1, Weight c2)
      {
         return c2 == c1;
      }

      public static bool operator !=(Weight c1, Weight c2)
      {
         return !(c1 == c2);
      }

      public static bool operator !=(Weight c1, double c2)
      {
         return !(c1 == c2);
      }

      public static bool operator !=(double c1, Weight c2)
      {
         return !(c1 == c2);
      }

      #endregion

      #region Overrides of Object
      #region Equality members

      protected bool Equals(Weight other)
      {
         return InternalWeight.Equals(other.InternalWeight) && TimeStamp.Equals(other.TimeStamp);
      }

      /// <summary>
      /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
      /// </summary>
      /// <returns>
      /// true if the specified object  is equal to the current object; otherwise, false.
      /// </returns>
      /// <param name="obj">The object to compare with the current object. </param>
      public override bool Equals(object obj)
      {
         if (ReferenceEquals(null, obj)) return false;
         if (ReferenceEquals(this, obj)) return true;
         return obj.GetType() == GetType() && Equals((Weight)obj);
      }

      /// <summary>
      /// Serves as a hash function for a particular type. 
      /// </summary>
      /// <returns>
      /// A hash code for the current <see cref="T:System.Object"/>.
      /// </returns>
      public override int GetHashCode()
      {
         unchecked
         {
            return (InternalWeight.GetHashCode() * 397) ^ TimeStamp.GetHashCode();
         }
      }

      #endregion

      public override string ToString()
      {
         return InternalWeight.ToString();
      }

      public int CompareTo(object obj)
      {
         var objT = obj.GetType();
         var objTIsDouble = objT == typeof(double);
         if (objT != GetType() && !objTIsDouble)
         {
            throw new InvalidOperationException(String.Format("Cannot compare weight with type %1", objT));
         }
         var cmpValue = objTIsDouble ? obj : ((Weight) obj).AsG;
         var weightCmp = ((IComparable)InternalWeight).CompareTo(cmpValue);
         if (objTIsDouble)
         {
            return weightCmp;
         }
         return weightCmp == 0 ? ((IComparable)TimeStamp).CompareTo(((Weight)obj).TimeStamp) : weightCmp;
      }

      private static double Round(double c)
      {
         return Math.Round(c, GDecimalPrecision);
      }

      #endregion
   }
}
