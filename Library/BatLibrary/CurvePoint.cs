namespace BatLibrary
{
   public class CurvePoint
   {
      public CurvePoint(int x, Weight y)
      {
         X = x;
         Y = y;
      }

      public int X { get; set; }
      public Weight Y { get; set; }
   }
}