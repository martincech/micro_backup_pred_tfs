#pragma once
#include "Message\Message.h"
#include "Crc\Crc.h"
#include "Time\uDateTime.h"

namespace Bat2Library
{
	using namespace System::Runtime::InteropServices;
	using namespace System;
	using namespace Bat2Library;
	public ref class MessageParser
	{
	internal:
		literal int ExpectedMaleMessageSize = (sizeof(TBinaryStatistics)-sizeof(TBinaryGenderStats));
		literal int ExpectedCompleteMessageSize = sizeof(TBinaryStatistics);

	public:
		static bool Parse(array<System::Byte> ^data, [Out] PublishedData^% parsedData);
		static array<System::Byte>^ GenerateData(PublishedData ^dataVariable);
	};
}

