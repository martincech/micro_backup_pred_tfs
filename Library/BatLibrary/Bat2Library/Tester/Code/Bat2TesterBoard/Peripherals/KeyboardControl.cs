using Bat2TesterBoard.Interfaces;

namespace Bat2TesterBoard.Peripherals
{
   /// <summary>
   ///  Represents Bat2 keyboard simulator
   /// </summary>
   public class KeyboardControl
   {
      #region Private fields

      private readonly IOController gpio;
      private bool escValue;
      private bool upValue;
      private bool enterValue;
      private bool leftValue;
      private bool downValue;
      private bool rightValue;

      #endregion


      /// <summary>
      ///  Constructor
      /// Pressed keys are with value true, released keys are value false
      /// TODO generate docu for each property
      /// </summary>
      public KeyboardControl(IOController gpio)
      {
         this.gpio = gpio;
         foreach (var key in gpio.Keys)
         {
            key.Release();
         }
      }

      public bool Esc
      {
         get { return escValue; }
         set
         {
            escValue = value;
            if (value)
            {
               gpio.Keys[(int)KeyNames.Esc].Push();
            }
            else
            {
               gpio.Keys[(int)KeyNames.Esc].Release();
            }
         }
      }

      public bool Up
      {
         get { return upValue; }
         set
         {
            upValue = value;
            if (value)
            {
               gpio.Keys[(int)KeyNames.Up].Push();
            }
            else
            {
               gpio.Keys[(int)KeyNames.Up].Release();
            }
         }
      }

      public bool Enter
      {
         get { return enterValue; }
         set
         {
            enterValue = value;
            if (value)
            {
               gpio.Keys[(int)KeyNames.Enter].Push();
            }
            else
            {
               gpio.Keys[(int)KeyNames.Enter].Release();
            }
         }
      }

      public bool Left
      {
         get { return leftValue; }
         set
         {
            leftValue = value;
            if (value)
            {
               gpio.Keys[(int)KeyNames.Left].Push();
            }
            else
            {
               gpio.Keys[(int)KeyNames.Left].Release();
            }
         }
      }

      public bool Down
      {
         get { return downValue; }
         set
         {
            downValue = value;
            if (value)
            {
               gpio.Keys[(int)KeyNames.Down].Push();
            }
            else
            {
               gpio.Keys[(int)KeyNames.Down].Release();
            }
         }
      }

      public bool Right
      {
         get { return rightValue; }
         set
         {
            rightValue = value;
            if (value)
            {
               gpio.Keys[(int)KeyNames.Right].Push();
            }
            else
            {
               gpio.Keys[(int)KeyNames.Right].Release();
            }
         }
      }

   }
}