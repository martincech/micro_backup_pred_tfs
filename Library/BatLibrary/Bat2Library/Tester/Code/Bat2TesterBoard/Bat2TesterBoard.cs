//******************************************************************************
//
//   Bat2TesterBoard.cs    Bat2 Tester Board
//   Version 1.0           (c) Veit Electronics
//
//******************************************************************************

using Bat2TesterBoard.Controllers;
using Bat2TesterBoard.Peripherals;

namespace Bat2TesterBoard
{
   /// <summary>
   ///  This class controls Bat2 tester board HW
   /// </summary>
   public class Bat2TesterBoard
   {
      #region Private fields

      private const string GPIO_DEVICE_NAME = "VEIT USB Reader";
      public readonly KeyboardControl Keyboard;
      public readonly LoadCellControl LoadCell;
      public readonly UsbPcControl UsbPc;
      public readonly AccuControl Accu;

      #endregion
      /// <summary>
      /// Constructor
      /// </summary>
      public Bat2TesterBoard()
      {
         var ftdi = new Ftdi.FtxxGpio(GPIO_DEVICE_NAME);
         var gpio = new FtdiIoController(ftdi);
         UsbPc = new UsbPcControl(gpio);
         Keyboard = new KeyboardControl(gpio);
         LoadCell = new LoadCellControl(gpio);
         Accu = new AccuControl(gpio);
      }
   }
}