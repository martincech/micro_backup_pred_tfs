﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Bat2Library.ModbusWorker.Worker;

namespace Bat2Library.ModbusWorker
{
   internal class ModbusElo
   {
      /// <summary>
      /// Elo settings
      /// </summary>
      private Setup setup;

      /// <summary>
      /// Specific scale adresses
      /// </summary>
      private List<int> adresses;

      /// <summary>
      /// Function to handle result of reading
      /// </summary>
      private Func<WorkerData, bool> read;

         /// <summary>
      /// Worker for Modbus scales
      /// </summary>
      Bat2ModbusOnlineWorker modbusWorker;

      /// <summary>
      /// List of current scale state
      /// </summary>
      List<ScaleState> scaleStateCollection;

      /// <summary>
      /// List of scale state from previous step (needed for midnight detection)
      /// </summary>
      List<CurrentState> lastStateCollection;

      /// <summary>
      /// Index of scale in the list
      /// </summary>
      int currentScaleIndex;

      private EloOperation operation;

      /// <summary>
      /// Action that should be performed after the Modbus worker is cancelled
      /// </summary>
      ActionAfterCancel actionAfterCancel;

      public ModbusElo(Setup setup, List<int> adresses = null, EloOperation operation = EloOperation.Read)
      {
         this.setup = setup;
         this.adresses = adresses;
         this.operation = operation;
      }

      public Setup Setup
      {
         get { return setup; }
      }

      public void Start(Func<WorkerData, bool> read)
      {
         this.read = read;
         StartWorkers();
      }

      public void Stop()
      {
         StopWorkers(actionAfterCancel);
      }

      /// <summary>
      /// Create list of Modbus scales and worker
      /// </summary>
      private void CreateModbusWorker()
      {
         // Vytvorim seznam vah
         scaleStateCollection = new List<ScaleState>();

         //Pokud jsou definovány specifické adresy načítají se pouze tyto
         if (adresses != null)
         {
            foreach (var adress in adresses)
            {
               scaleStateCollection.Add(new ScaleState(setup.ModbusScales.PortNumber, adress));
            }
         }
         else
         {
            for (int i = setup.ModbusScales.FirstAddress; i <= setup.ModbusScales.LastAddress; i++)
            {
               scaleStateCollection.Add(new ScaleState(setup.ModbusScales.PortNumber, i));
            }
         }

         // Vytvorim seznam poslednich stavu vsech vah (poradi odpovida seznamu vah scaleStateCollection)
         lastStateCollection = new List<CurrentState>();
         for (int i = setup.ModbusScales.FirstAddress; i <= setup.ModbusScales.LastAddress; i++)
         {
            lastStateCollection.Add(null);      // Default neznam zadny posledni stav
         }
         // Vytvorim worker
         List<Bat2Slave> scaleCollection = new List<Bat2Slave>();
         foreach (ScaleState state in scaleStateCollection)
         {
            scaleCollection.Add(state.Slave);
         }
         modbusWorker = new Bat2ModbusOnlineWorker(ModbusWorker_ProgressChanged,
                                                   ModbusWorker_RunWorkerCompleted,
                                                   setup.ModbusProtocol.Speed,
                                                   setup.ModbusProtocol.Parity,
                                                   8,
                                                   setup.ModbusProtocol.ReplyTimeout,
                                                   setup.ModbusProtocol.SilentInterval,
                                                   setup.ModbusProtocol.NumberOfAttempts,
                                                   setup.ModbusProtocol.Protocol,
                                                   scaleCollection);

         actionAfterCancel = ActionAfterCancel.NONE;
      }

      /// <summary>
      /// Start Modbus worker
      /// </summary>
      private void StartModbusWorker()
      {
         currentScaleIndex = 0;
         modbusWorker.Start(operation);
      }

      /// <summary>
      /// Check if any worker is running
      /// </summary>
      /// <returns>True if running</returns>
      private bool IsWorkerRunning()
      {
         if (modbusWorker.IsBusy)
         {
            return true;
         }
         return false;
      }

      /// <summary>
      /// Create all workers and start workers for idle operation
      /// </summary>
      private void StartWorkers()
      {
         // Vytvorim seznam vah a worker, automaticky rozjedu
         CreateModbusWorker();
         StartModbusWorker();
      }

      /// <summary>
      /// Stop all workers
      /// </summary>
      /// <param name="actionAfterCancel">Action to perform after the workers are stopped</param>
      private void StopWorkers(ActionAfterCancel actionAfterCancel)
      {
         this.actionAfterCancel = actionAfterCancel;     // Akci provede ten worker, ktery se ukonci jako posledni
         modbusWorker.Stop();

      }

      /// <summary>
      /// Perform action after all workers are cancelled
      /// </summary>
      private void PerformActionAfterCancel()
      {
         // Akci provede vzdy az ten worker, ktery se zastavi jako posledni
         if (IsWorkerRunning())
         {
            return;     // Nektery worker jeste bezi, cekam dal
         }

         switch (actionAfterCancel)
         {
            case ActionAfterCancel.SETUP:
               actionAfterCancel = ActionAfterCancel.NONE;    // Snuluju hned, pak by mohla byt vyjimka
               ShowSetup();
               break;

            case ActionAfterCancel.QUIT:
               break;
         }
      }

      /// <summary>
      /// Show setup form. After it is closed, the Modbus worker is started again.
      /// </summary>
      private void ShowSetup()
      {
         // Vytvorim a rozjedu vsechny workery
         StartWorkers();
      }

      private void ModbusWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
      {
         WorkerData data = (WorkerData)e.UserState;
         if (!read(data))
         {
            StopWorkers(ActionAfterCancel.NONE);
         }
      }

      private void ModbusWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
      {
         if (!e.Cancelled)
         {
            return;     // Worker nemuze skoncit sam od sebe, jede v nekonecne smycce
         }
         
         PerformActionAfterCancel();
      }
   }

   internal enum ActionAfterCancel
   {
      NONE,
      SETUP,      // Zobrazi setup
      QUIT,       // Ukonci aplikaci
      SEND_SMS    // Odesle SMS
   }
}
