﻿using System.Windows.Threading;
using TestApp.IViews;

namespace Connection.Desktop.Tests
{
   public class MockIConnectedDevicesView : IConnectedDevicesView
   {
      #region Implementation of IView

      /// <summary>
      /// Gets or sets the data context of the view.
      /// This will be set to ViewModel of the view.
      /// </summary>
      public object DataContext { get; set; }
      public Dispatcher Dispatcher { get { return Dispatcher.CurrentDispatcher; } }
      public void ShowMessage(string message)
      {
      }

      #endregion
   }
}