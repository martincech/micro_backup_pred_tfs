﻿using System.Collections.Specialized;
using System.Threading;
using Common.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestApp.ViewModels;

namespace Connection.Desktop.Tests
{
   [TestClass]
   public class ConnectedDevicesViewModelTests
   {
      private ConnectedDevicesViewModel module;

      [TestInitialize]
      public void Init()
      {
         var view = new MockIConnectedDevicesView();
         module = new ConnectedDevicesViewModel(view);
      }

      [TestCleanup]
      public void Clean()
      {
         module.Shutdown(); 
         Assert.IsFalse(module.IsRunning);
      }

      [TestMethod]
      public void IsRunningChanged_AfterRun()
      {
         var raised = new ManualResetEvent(false);
         Assert.IsFalse(module.IsRunning);
         module.PropertyChanged += (sender, args) =>
         {
            if (args.PropertyName == "IsRunning")
            {
               raised.Set();
            }
         };
         module.Run();
         Assert.IsTrue(raised.WaitOne(1000));
         Assert.IsTrue(module.IsRunning);
      }

      [TestMethod]
      public void ConnectedDevicesRaised()
      {
         var addRaised = new ManualResetEvent(false);
         var delRaised = new ManualResetEvent(false);
         module.Models.CollectionChanged += (sender, args) =>
         {
            switch (args.Action)
            {
               case NotifyCollectionChangedAction.Add:
                  addRaised.Set();
                  break;
               case NotifyCollectionChangedAction.Remove:
                  delRaised.Set();
                  break;
            }
         };

         module.Run();
         // temporary sleep thread to do threadpool queued items
         Assert.IsFalse(addRaised.WaitOne(500));
         DispatcherUtil.DoEvents();
         Assert.IsTrue(addRaised.WaitOne(1000), "Add device not raised, check BAT2 connection!");
         module.Shutdown();
         // temporary sleep thread to do threadpool queued items
         Assert.IsFalse(delRaised.WaitOne(500));
         DispatcherUtil.DoEvents();
         Assert.IsTrue(delRaised.WaitOne(1000), "Del device not raised!");
      }
   }
}
