﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using Common.Services.Hosting;

namespace Connection.Client
{
   /// <summary>
   /// Represents generic client for communication with service.
   /// </summary>
   /// <typeparam name="S">Service type to be created for client</typeparam>
   /// <typeparam name="I">Contract to be communicated through.</typeparam>
   public abstract class ServiceClient<S, I> 
      where S : class, I
      where I : class
   {
      static ServiceClient()
      {
         Debug.Assert(typeof(I).IsInterface, "{0} must be an interface!", typeof(I).Name);
         Debug.Assert(typeof(I).GetCustomAttributes(typeof(ServiceContractAttribute), false).Any(), string.Format("{0} is not valid contract!", typeof(I).Name));
         Debug.Assert(!typeof(S).IsInterface, "{0} must be a class!", typeof(S).Name);
         Debug.Assert(typeof (S).GetCustomAttributes(typeof (ServiceBehaviorAttribute), false).Any(), string.Format("{0} is not a valid service implementation!", typeof(S).Name));
      }

      public object ClientCallback { get; set; }
      #region Command executive

      /// <summary>
      /// Execute method on remote service of type <see cref="I"/>
      /// </summary>
      protected void ExecuteServiceMethod(Action<I> action)
      {
         var factory = InProcFactory.CreateClientFactory<S, I>();
         I client;
         if (ClientCallback != null)
         {
            client = factory.CreateInstance(ClientCallback);
         }
         else
         {
            client = factory.CreateInstance();
         }
         ThreadPool.QueueUserWorkItem(param =>
         {
            try
            {
               action.Invoke(client);
               if (ClientCallback == null)
               {
                  factory.CloseProxy(client);
               }
            }
            catch (Exception)
            {
               factory.CloseProxy(client);
            }
         });


      }

      #endregion
   }
}
