﻿using Connection.Interface.Contract;
using Connection.Server;

namespace Connection.Client
{
   public class Bat2EventsSubscribtionClient :
      ServiceClient<Bat2EventsSubscribtionService, IBat2EventsSubscribtionContract>,
      IBat2EventsSubscribtionContract
   {
      #region Implementation of ISubscriptionContract

      /// <summary>
      /// Subscribe for all events
      /// </summary>
      public void SubscribeAll()
      {
         ExecuteServiceMethod(x => x.SubscribeAll());
      }

      /// <summary>
      /// Subscribe for concrete event
      /// </summary>
      /// <param name="eventOperation">event operation name</param>
      public void Subscribe(string eventOperation)
      {
         ExecuteServiceMethod(x => x.Subscribe(eventOperation));
      }

      /// <summary>
      /// Unsubscribe for concrete event
      /// </summary>
      /// <param name="eventOperation">event operation name</param>
      public void Unsubscribe(string eventOperation)
      {
         ExecuteServiceMethod(x => x.Unsubscribe(eventOperation));
      }

      /// <summary>
      /// Unsubscribe for all events
      /// </summary>
      public void UnsubscribeAll()
      {
         ExecuteServiceMethod(x => x.UnsubscribeAll());
      }

      #endregion
   }
}