﻿using System;
using Connection.Interface.Contract;
using Connection.Interface.Domain;
using Connection.Server;

namespace Connection.Client
{
   public class Bat2CommandContractClient : 
      ServiceClient<Bat2CommandContractService, IBat2CommandContract>,
      IBat2CommandContract
   {
      #region Implementation of IBat2CommandContract

      /// <summary>
      /// Send command to device to read data from it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void ReadData(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.ReadData(deviceData));
      }

      public void ReadConfig(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.ReadConfig(deviceData));
      }

      public void ReadContext(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.ReadContext(deviceData));
      }

      public void ReadArchive(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.ReadArchive(deviceData));
      }

      public void ReadGrowthCurves(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.ReadGrowthCurves(deviceData));
      }

      public void ReadCorrectionCurves(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.ReadCorrectionCurves(deviceData));
      }

      public void ReadContactList(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.ReadContactList(deviceData));
      }

      public void ReadWeighingPlans(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.ReadWeighingPlans(deviceData));
      }

      /// <summary>
      /// Send command to device to write data to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WriteData(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.WriteData(deviceData));
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void TimeGet(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.TimeGet(deviceData));
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      /// <param name="at"></param>
      public void TimeSet(Bat2DeviceData deviceData, DateTime at)
      {
         ExecuteServiceMethod(x => x.TimeSet(deviceData, at));
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      /// <param name="at"></param>
      public void WeighingStart(Bat2DeviceData deviceData, DateTime? at)
      {
         ExecuteServiceMethod(x => x.WeighingStart(deviceData, at));
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WeighingStop(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.WeighingStop(deviceData));
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WeighingSuspend(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.WeighingSuspend(deviceData));
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      public void WeighingRelease(Bat2DeviceData deviceData)
      {
         ExecuteServiceMethod(x => x.WeighingRelease(deviceData));
      }

      #endregion
   }
}
