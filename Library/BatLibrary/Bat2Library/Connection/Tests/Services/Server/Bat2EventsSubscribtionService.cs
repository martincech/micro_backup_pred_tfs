﻿using System.ServiceModel;
using Common.Services.PublishSubscribe;
using Connection.Interface.Contract;

namespace Connection.Server
{
   /// <summary>
   /// Subscription service, holds information about subscribted client which want to be informed about
   /// <see cref="IBat2EventsContract"/>  events.
   /// </summary>
   [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
   public class Bat2EventsSubscribtionService : SubscriptionManager<IBat2EventsContract>,
      IBat2EventsSubscribtionContract
   {

   }
}
