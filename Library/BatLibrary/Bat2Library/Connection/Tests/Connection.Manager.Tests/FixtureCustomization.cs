using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.Domain.Old;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Kernel;
using Utilities;
using Utilities.Extensions;

namespace Connection.Manager.Tests
{
   public static class FixtureCustomization
   {
      /// <summary>
      /// Customize created data
      /// </summary>
      /// <param name="fixture"></param>
      public static void CustomizeData(this Fixture fixture)
      {
         fixture.RepeatCount = 2;

         fixture.Customize<IPAddress>(ob => ob.Without(p => p.ScopeId));
         fixture.Customize<CellularData>(comp => comp
            .With(x => x.Apn, "Apn")
            .With(x => x.Password, GetString(fixture, "Password", 10))
            .With(x => x.Username, GetString(fixture, "UserName", 10))
            );

         fixture.Customize<VersionInfo>(comp => comp
            .With(x => x.HardwareBuild, (byte) 1)
            .With(x => x.HardwareMajor, (byte) 2)
            .With(x => x.HardwareMinor, (byte) 3)
            .With(x => x.SoftwareBuild, (byte) 4)
            .With(x => x.SoftwareMajor, (byte) 6)
            .With(x => x.SoftwareMinor, (byte) 6)
            );

         fixture.Customize<Contact>(comp => comp
            .With(x => x.Name, GetString(fixture, "Name", 10))
            .With(x => x.PhoneNumber, "777444333")
            );

         fixture.Customize<DataPublication>(comp => comp
            .With(x => x.Username, GetString(fixture, "UserName", 10))
            .With(x => x.Password, GetString(fixture, "Password", 10))
            .With(x => x.SendAt, GetTime(fixture))
            );

         fixture.Customize<MegaviOptions>(comp => comp
            .With(x => x.Address, (byte) 14)
            .With(x => x.Code, (byte) 10)
            );

         fixture.Customize<Ethernet>(comp => comp
            .With(x => x.Ip, IPAddress.Parse("255.244.112.123"))
            .With(x => x.PrimaryDns, IPAddress.Parse("192.168.1.133"))
            .With(x => x.SubnetMask, IPAddress.Parse("255.255.255.0"))
            .With(x => x.Gateway, IPAddress.Parse("135.14.17.0"))
            );

         fixture.Customize<DeviceInfo>(comp => comp
            .With(x => x.Password, "1524")
            .With(x => x.Name, GetString(fixture, "Name", 10))
            );

         fixture.Customize<GsmMessage>(comp => comp
            .With(x => x.SwitchOnTimes, new List<TimeRange>
            {
               new TimeRange {From = GetTime(fixture), To = GetTime(fixture)},
               new TimeRange {From = GetTime(fixture), To = GetTime(fixture)}
            })
            );

         fixture.Customize<WeighingConfiguration>(comp => comp
            .With(x => x.Flock, GetString(fixture, "Flock", 10))
            .With(x => x.Name, GetString(fixture, "Name", 10))
            .With(x => x.DayStart, GetTime(fixture))
            );

         fixture.Customize<Curve>(comp => comp
            .With(x => x.Name, GetString(fixture, "Name", 10))
            );

         fixture.Customize<Country>(comp => comp
            .With(x => x.DaylightSavingType, DaylightSavingE.DAYLIGHT_SAVING_OFF)
            );

         fixture.Customize<WeighingPlan>(comp => comp
            .With(x => x.Name, GetString(fixture, "Name", 10))
            );

         fixture.Customize<WeighingContext>(comp => comp
            .With(x => x.StartAt, GetTime(fixture))
            .With(x => x.DayZero, GetTime(fixture))
            .With(x => x.DayCloseAt, GetTime(fixture))
            .With(x => x.DayDuration, new TimeSpan(23, 59, 59))
            );

         fixture.Customize<TimeRange>(comp => comp
            .With(x => x.From, GetTime(fixture))
            .With(x => x.To, GetTime(fixture))
            );

         fixture.Customize<CalibrationContext>(comp => comp
            .With(x => x.Time, GetTime(fixture))
            );

         fixture.Customize<BaseVersionInfo>(comp => comp
            .Without(x => x.SerialNumberArray)
            .With(x => x.SerialNumber, (uint) 100));

         fixture.Customize<VersionInfo>(comp => comp
            .Without(x => x.SerialNumberArray)
            .With(x => x.SerialNumber, (uint) 100));

         fixture.Customize<List<WeighingConfiguration>>(comp => comp
            .Do(x => x.ForEach(i => { i.PredefinedIndex = x.IndexOf(i); }))
            );

         // fix.Customize<OldConfiguration>(x => x.Without(a => a.VersionInfo));
         fixture.Customize<OldWeighingStart>(x => x.With(a => a.DateTime, new DateTime(2005, 4, 3, 2, 4, 0)));
         fixture.Customize<OldArchive>(x => x.With(a => a.DateTime, new DateTime(2003, 5, 2, 1, 55, 0)));
         fixture.Customize<OldGsm>(x =>
            x.Without(a => a.DayMidnight1)
               .Without(a => a.MidnightSendHour)
               .Without(a => a.MidnightSendMin)
               .Without(a => a.PeriodMidnight1)
               .Without(a => a.PeriodMidnight2)
               .With(a => a.Numbers, new List<string>
               {
                  "420111222333   ",
                  "999888666      ",
                  "123456789      ",
                  "111222555666   ",
                  "4556872045     "
               }));
         fixture.Customize<OldFlock>(x =>
            x.With(a => a.GrowthCurveFemale, new List<OldCurvePoint>
            {
               new OldCurvePoint {Day = 1, Weight = 1},
               new OldCurvePoint {Day = 2, Weight = 3},
               new OldCurvePoint {Day = 3, Weight = 5},
            }).With(a => a.GrowthCurveMale, new List<OldCurvePoint>
            {
               new OldCurvePoint {Day = 1, Weight = 2},
               new OldCurvePoint {Day = 2, Weight = 4},
               new OldCurvePoint {Day = 4, Weight = 6},
            }));

         fixture.Customize<OldFlockHeader>(x => x.With(a => a.Title, "Title   ").With(a => a.Number, (byte) 0));
         fixture.Customize<OldConfiguration>(x =>
            x.With(a => a.Units, WeightUnitsE.WEIGHT_UNITS_LB)
               .With(a => a.VersionInfo, new BaseVersionInfo
               {
                  SoftwareBuild = 1,
                  SoftwareMajor = 1,
                  SoftwareMinor = 1,
                  SerialNumber = 157,
                  Modification = 0
               })
               .With(c => c.Flocks, FlockList(fixture)));
         fixture.Customize<List<WeighingConfiguration>>(x => x.With(a => a.Capacity, 0));
      }

      public static IEnumerable<OldFlock> FlockList(Fixture fixture)
      {
         var fList = fixture.Create<List<OldFlock>>();
         if (fList.Count() == 1)
         {
            var f = fList.First();
            f.Header.WeighFrom = f.Header.WeighTill = 0;
            fList.Add(f);
         }
         else
         {
            var i = 0;
            foreach (var flock in fList)
            {
               flock.Header.Title = flock.Header.Title + i;
               flock.Header.Number = (byte) i;
               flock.Header.WeighTill = (byte) (flock.Header.WeighFrom + 1);
               i++;
            }
         }
         return fList;
      }

      public static void Compare(this object o1, object o2)
      {
         Assert.IsTrue(o1.Same(o2));
      }

      private static string GetString(ISpecimenBuilder fixture, string name, int size)
      {
         return fixture.Create(name).Substring(0, size);
      }

      private static DateTime GetTime(ISpecimenBuilder fixture)
      {
         var t = fixture.Create<DateTime>();
         return new DateTime(2001, 1, 1, t.Hour, t.Minute, t.Second);
      }
   }
}