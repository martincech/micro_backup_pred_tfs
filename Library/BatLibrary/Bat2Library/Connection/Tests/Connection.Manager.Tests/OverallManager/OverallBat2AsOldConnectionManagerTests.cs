﻿using Bat2Library.Connection.Interface.Domain.Old;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.OverallManager
{
   [TestClass]
   public class OverallBat2AsOldConnectionManagerTests :
      BaseOverallBat2ConnectionManagerTests<
         BaseVersionInfo,
         OldConfiguration,
         OldBat2DeviceData,
         IBat2OldDataReader,
         IBat2OldDataWriter,
         IBat2ActionCommander>
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public OverallBat2AsOldConnectionManagerTests()
         : base(Bat2ConnectionManager.Instance)
      {
      }
   }
}