﻿using System;
using System.IO;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Connection.Manager.Tests.Bat2New.Sockets.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Sockets.UsbSocket
{
   [TestClass]
   public class UsbBat2DataWriteTests : Bat2DataWriteTests
   {
      private const string FILE_NAME = "tmpFile.txt";

      #region Overrides of Bat2DataWriteTests

      protected override void SaveMethod_ReturnFalse_WhenNotConnected<T>(Func<T, IBat2NewDataWriter, bool> saveAction)
      {
         using (var file = new FileStream(FILE_NAME, FileMode.Create, FileAccess.ReadWrite))
         {
            using (var socket = new Bat2UsbSocket(file))
            {
               using (var b2D = new UsbBat2Device(socket))
               {
                  SaveMethod_ReturnFalse_WhenNotConnected(saveAction, b2D);
               }
            }
         }
      }

      protected override void SaveMethod_ReturnTrue_WhenConnected<T>(Func<T, IBat2NewDataWriter, bool> saveAction)
      {
         using (var socket = new Bat2UsbSocket(Bat2UsbSocketTests.VID, Bat2UsbSocketTests.PID))
         {
            using (var b2D = new UsbBat2Device(socket))
            {
               SaveMethod_ReturnTrue_WhenConnected(saveAction, b2D);
            }
         }
      }

      #endregion
   }
}