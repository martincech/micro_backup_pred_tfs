﻿using System;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Connection.Manager.Tests.Bat2New.Sockets.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Sockets.FlashSocket
{
   [TestClass]
   public class FlashBat2DataActionTests : Bat2DataActionTests
   {
      [TestInitialize]
      public void Init()
      {
         FlashFiles.Init();
      }

      [TestCleanup]
      public void Clean()
      {
         FlashFiles.Free();
      }

      #region Overrides of Bat2DataActionTests

      internal override void ActionMethod_ReturnFalse_WhenNotConnected(Func<IBat2ActionCommander, bool> action)
      {
         using (var socket = new Bat2FlashSocket("w:\\", FlashFiles.FirstSn.Value))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               ActionMethod_ReturnFalse_WhenNotConnected(action, b2D);
            }
         }
         FlashFiles.FilesCheck();
      }

      internal override void ActionMethod_ReturnTrue_WhenConnected(Func<IBat2ActionCommander, bool> action)
      {
         using (var socket = new Bat2FlashSocket(FlashFiles.BasePath, FlashFiles.FirstSn.Value))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               ActionMethod_ReturnFalse_WhenNotConnected(action, b2D);
            }
         }
         FlashFiles.FilesCheck();
      }

      #endregion
   }
}