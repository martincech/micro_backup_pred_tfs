﻿using System;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Connection.Manager.Tests.Bat2New.Sockets.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Sockets.FlashSocket
{
   [TestClass]
   public class FlashBat2DataReadTests : Bat2DataReadTests
   {
      [TestInitialize]
      public void Init()
      {
         FlashFiles.Init();
      }

      [TestCleanup]
      public void Clean()
      {
         FlashFiles.Free();
      }

      #region Overrides of Bat2DataReadTests

      protected override void LoadMethod_ReturnNull_WhenNotConnected<T>(Func<IBat2NewDataReader, T> loadAction)
      {
         using (var socket = new Bat2FlashSocket("w:\\", FlashFiles.FirstSn.Value))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               LoadMethod_ReturnNull_WhenNotConnected(loadAction, b2D);
            }
         }
         FlashFiles.FilesCheck();
      }

      protected override void LoadMethod_ReturnNonNull_WhenConnected<T>(Func<IBat2NewDataReader, T> loadAction)
      {
         using (var socket = new Bat2FlashSocket(FlashFiles.BasePath, FlashFiles.FirstSn.Value))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               LoadMethod_ReturnNonNull_WhenConnected(loadAction, b2D);
            }
         }
         FlashFiles.FilesCheck();
      }

      #endregion
   }
}