﻿using System.IO;
using System.Linq;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Sockets.FlashSocket
{
   [TestClass]
   public class EfsTests
   {
      private EfsTest uut;
      private string basePath;
      private const string SUB1 = "Sub1";
      private const string SUB2 = "Sub2";

      [TestInitialize]
      public void Init()
      {
         var info = Directory.CreateDirectory("TestDir");
         Assert.IsTrue(info.Exists);
         basePath = info.FullName;
         uut = new EfsTest(info.FullName);
         Assert.IsTrue(uut.EfsInit());
      }

      private void DeleteDirectory(string path)
      {
         foreach (var directory in Directory.GetDirectories(path))
         {
            DeleteDirectory(directory);
         }
         foreach (var file in Directory.GetFiles(path))
         {
            File.Delete(file);
         }
      }

      [TestCleanup]
      public void Clean()
      {
         DeleteDirectory(basePath);
         Directory.Delete(basePath, true);
      }

      private string Path(string name)
      {
         return System.IO.Path.Combine(basePath, name);
      }

      private string Path(string name, string name2)
      {
         return System.IO.Path.Combine(basePath, name, name2);
      }

      [TestMethod]
      public void DirectoryCreate_Succesfully()
      {
         Assert.IsTrue(uut.EfsDirectoryCreate(SUB1));
         Assert.IsTrue(uut.EfsDirectoryCreate(SUB1));
         Assert.IsTrue(Directory.Exists(Path(SUB1)), "SubDirectory not exists");
         Assert.IsTrue(Directory.Exists(Path(SUB1, SUB1)), "SubSubDirectory not exists");

         Assert.AreEqual(Directory.GetDirectories(Path("")).Count(), 1);
         Assert.AreEqual(Directory.GetDirectories(Path(SUB1)).Count(), 1);
      }

      [TestMethod]
      public void ExistsIsOk()
      {
         Assert.AreEqual(Directory.Exists(Path(SUB1)), uut.EfsDirectoryExists(Path(SUB1)));
         Directory.CreateDirectory(Path(SUB1));
         Assert.AreEqual(Directory.Exists(Path(SUB1)), uut.EfsDirectoryExists(Path(SUB1)));
         Assert.IsTrue(uut.EfsDirectoryCreate(Path(SUB1, SUB1)));
         Assert.AreEqual(Directory.Exists(Path(SUB1, SUB1)), uut.EfsDirectoryExists(Path(SUB1, SUB1)));
      }

      [TestMethod]
      public void DirectoryChange_Succesfully()
      {
         Assert.IsFalse(uut.EfsDirectoryChange(".."));
         Assert.IsTrue(uut.EfsDirectoryCreate(SUB1));
         Assert.IsTrue(uut.EfsDirectoryChange(".."), "Change directory problem");
         Assert.IsTrue(uut.EfsDirectoryCreate(SUB2));
         Assert.IsTrue(Directory.Exists(Path(SUB1)), "SubDirectory not exists");
         Assert.IsTrue(Directory.Exists(Path(SUB2)), "SubDirectory not exists");
         Assert.IsFalse(Directory.GetDirectories(Path(SUB1)).Any());
         Assert.IsFalse(Directory.GetDirectories(Path(SUB2)).Any());
         Assert.AreEqual(Directory.GetDirectories(Path("")).Count(), 2);
      }

      [TestMethod]
      public void FormatIsOk()
      {
         Directory.CreateDirectory(Path(SUB1));
         Directory.CreateDirectory(Path(SUB1, SUB2));
         Assert.IsTrue(Directory.Exists(Path(SUB1)));
         Assert.IsTrue(uut.EfsFormat());
         Assert.IsFalse(Directory.Exists(Path(SUB1)));
         Assert.IsTrue(Directory.Exists(Path("")));
      }

      [TestMethod]
      public void DirectoryDelete_isOk()
      {
         Assert.IsTrue(uut.EfsDirectoryCreate(SUB1));
         Assert.IsTrue(Directory.Exists(Path(SUB1)));
         Assert.IsFalse(uut.EfsDirectoryDelete(SUB1));
         Assert.IsTrue(Directory.Exists(Path(SUB1)));
         Assert.IsTrue(uut.EfsDirectoryChange(".."));
         Assert.IsTrue(uut.EfsDirectoryDelete(SUB1));
         Assert.IsFalse(Directory.Exists(Path(SUB1)));
      }

      [TestMethod]
      public void DirectoryRename_isOk()
      {
         Assert.IsTrue(uut.EfsDirectoryCreate(SUB1));
         Assert.IsTrue(Directory.Exists(Path(SUB1)));
         Assert.IsTrue(uut.EfsDirectoryChange(".."));
         Assert.IsTrue(uut.EfsDirectoryRename(SUB1, SUB2));
         Assert.IsFalse(Directory.Exists(Path(SUB1)));
         Assert.IsTrue(Directory.Exists(Path(SUB2)));
      }

      [TestMethod]
      public void FileExists_isOk()
      {
         Assert.IsTrue(uut.EfsDirectoryCreate(SUB1));
         Assert.AreEqual(uut.EfsFileExists(SUB1), File.Exists(Path(SUB1)));
         var fs = File.Create(Path(SUB1, SUB2));
         Assert.AreEqual(uut.EfsFileExists(SUB2), File.Exists(Path(SUB1, SUB2)));
         fs.Close();
         File.Delete(Path(SUB1, SUB2));
         Assert.AreEqual(uut.EfsFileExists(SUB2), File.Exists(Path(SUB1, SUB2)));
      }

      [TestMethod]
      public void FileCreate_isOk()
      {
         var descriptor = 0;
         Assert.IsTrue(uut.EfsFileCreate(ref descriptor, SUB1, 20));
         Assert.IsTrue(File.Exists(Path(SUB1)));
         uut.EfsFileClose(ref descriptor);
      }

      [TestMethod]
      public void FileDelete_isOk()
      {
         var fs = File.Create(Path(SUB1));
         fs.Close();
         var descriptor = 0;
         Assert.IsTrue(uut.EfsFileDelete(SUB1));
         Assert.IsFalse(File.Exists(Path(SUB1)));
      }

      [TestMethod]
      public void FileCreateDelete_isOk()
      {
         var descriptor = 0;
         Assert.IsTrue(uut.EfsFileCreate(ref descriptor, SUB1, 20));
         Assert.IsTrue(uut.EfsFileDelete(SUB1));
      }

      [TestMethod]
      public void RW_Ok()
      {
         var buf = new byte[] {1, 2, 3};
         var bufRead = new byte[3];
         var descriptor = 0;
         Assert.IsTrue(uut.EfsFileCreate(ref descriptor, SUB1, 0));
         var len = uut.EfsFileWrite(ref descriptor, buf, (ushort) buf.Length);
         uut.EfsFileSeek(ref descriptor, 0, SeekOrigin.Begin);
         Assert.AreEqual(uut.EfsFileRead(ref descriptor, ref bufRead, (ushort) buf.Length), len);
         CollectionAssert.AreEqual(buf, bufRead);
         uut.EfsFileDelete(SUB1);
      }

      [TestMethod]
      public void Format_Imposible_When_File_Opened()
      {
         var descriptor = 0;
         Assert.IsTrue(uut.EfsFileCreate(ref descriptor, SUB1, 0));
         Assert.IsFalse(uut.EfsFormat());
         uut.EfsFileClose(ref descriptor);
         Assert.IsTrue(uut.EfsFormat());
      }

      [TestMethod]
      public void SocketIfMsdTest()
      {
         Assert.IsTrue(uut.TestSocket());
      }
   }
}