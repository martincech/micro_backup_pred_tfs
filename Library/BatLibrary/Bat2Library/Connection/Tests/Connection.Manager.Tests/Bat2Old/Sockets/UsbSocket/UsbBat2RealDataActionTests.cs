using System;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2Old.Sockets.UsbSocket
{
   [TestClass]
   public class UsbBat2RealDataActionTests : UsbBat2FakeDataActionTests
   {
      internal override void ActionMethod_ReturnTrue_WhenConnected(Func<IBat2ActionCommander, bool> action)
      {
         //real device
         using (var socket = new Bat2OldUsbSocket(Bat2Library.Bat2Old.Constants.Bat2Ftdi.USB_DEVICE_NAME))
         {
            using (var b2D = new UsbBat2OldDevice(socket))
            {
               ActionMethod_ReturnTrue_WhenConnected(action, b2D);
            }
         }
      }
   }
}