using System;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2Old.Sockets.UsbSocket
{
   [TestClass]
   public class UsbBat2RealDataWriteTests : UsbBat2FakeDataWriteTests
   {
      protected override void SaveMethod_ReturnTrue_WhenConnected<T>(Func<T, IBat2OldDataWriter, bool> saveAction)
      {
         //real device
         using (var socket = new Bat2OldUsbSocket(Bat2Library.Bat2Old.Constants.Bat2Ftdi.USB_DEVICE_NAME))
         {
            using (var b2D = new UsbBat2OldDevice(socket))
            {
               SaveMethod_ReturnTrue_WhenConnected(saveAction, b2D);
            }
         }
      }
   }
}