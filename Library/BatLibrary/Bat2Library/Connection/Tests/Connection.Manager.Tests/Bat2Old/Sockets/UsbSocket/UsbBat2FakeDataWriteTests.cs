﻿using System;
using System.IO;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Connection.Manager.Tests.Bat2Old.Sockets.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2Old.Sockets.UsbSocket
{
   [TestClass]
   public class UsbBat2FakeDataWriteTests : Bat2OldDataWriteTests
   {
      private const string FILE_NAME = "tmpFile.txt";
      private const string MEMORY_FILE_NAME_V111 = "Bat2Old_v1.11_scale.bin";
      private const string MEMORY_FILE_NAME_V150 = "Bat2Old_v1.50_scale.bin";

      [TestInitialize]
      public void Init()
      {
         Assert.IsTrue(File.Exists(MEMORY_FILE_NAME_V111));
         if (File.Exists(MEMORY_FILE_NAME_V111 + "tmp"))
         {
            File.Delete(MEMORY_FILE_NAME_V111 + "tmp");
         }
         File.Copy(MEMORY_FILE_NAME_V111, MEMORY_FILE_NAME_V111 + "tmp");

         Assert.IsTrue(File.Exists(MEMORY_FILE_NAME_V150));
         if (File.Exists(MEMORY_FILE_NAME_V150 + "tmp"))
         {
            File.Delete(MEMORY_FILE_NAME_V150 + "tmp");
         }
         File.Copy(MEMORY_FILE_NAME_V150, MEMORY_FILE_NAME_V150 + "tmp");
      }

      [TestCleanup]
      public void Clean()
      {
         File.Delete(MEMORY_FILE_NAME_V111);
         File.Copy(MEMORY_FILE_NAME_V111 + "tmp", MEMORY_FILE_NAME_V111);
         File.Delete(MEMORY_FILE_NAME_V150);
         File.Copy(MEMORY_FILE_NAME_V150 + "tmp", MEMORY_FILE_NAME_V150);
      }
      #region Overrides of Bat2DataWriteTests

      protected override void SaveMethod_ReturnFalse_WhenNotConnected<T>(Func<T, IBat2OldDataWriter, bool> saveAction)
      {
         using (var file = new FileStream(FILE_NAME, FileMode.Create, FileAccess.ReadWrite))
         {
            using (var socket = new Bat2OldUsbSocket(file))
            {
               using (var b2D = new UsbBat2OldDevice(socket))
               {
                  SaveMethod_ReturnFalse_WhenNotConnected(saveAction, b2D);
               }
            }
         }
      }

      protected override void SaveMethod_ReturnTrue_WhenConnected<T>(Func<T, IBat2OldDataWriter, bool> saveAction)
      {
         // simulated device v1.11
         using (var file = new FileStream(MEMORY_FILE_NAME_V111, FileMode.Open, FileAccess.ReadWrite))
         {
            Assert.AreNotEqual(0, file.Length);
            using (var socket = new Bat2OldUsbSocket(file))
            {
               using (var b2D = new UsbBat2OldDevice(socket))
               {
                  SaveMethod_ReturnTrue_WhenConnected(saveAction, b2D);
               }
            }
         }

         // simulated device 1.50
         using (var file = new FileStream(MEMORY_FILE_NAME_V150, FileMode.Open, FileAccess.ReadWrite))
         {
            Assert.AreNotEqual(0, file.Length);
            using (var socket = new Bat2OldUsbSocket(file))
            {
               using (var b2D = new UsbBat2OldDevice(socket))
               {
                  SaveMethod_ReturnTrue_WhenConnected(saveAction, b2D);
               }
            }
         }
      }

      #endregion
   }
}