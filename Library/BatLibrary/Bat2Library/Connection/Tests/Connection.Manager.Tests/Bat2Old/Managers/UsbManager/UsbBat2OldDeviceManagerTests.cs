﻿using System.Threading;
using Bat2Library.Connection.Interface.Domain.Old;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2Old.Managers.UsbManager
{
   [TestClass]
   public class UsbBat2OldDeviceManagerTests :
      BaseIBat2DeviceManagerTests<
         BaseVersionInfo,
         OldConfiguration,
         OldBat2DeviceData,
         IBat2OldDataReader,
         IBat2OldDataWriter,
         IBat2ActionCommander>
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public UsbBat2OldDeviceManagerTests()
         : base(UsbBat2OldDeviceManager.Instance)
      {
         Thread.Sleep(2000);
      }
   }
}