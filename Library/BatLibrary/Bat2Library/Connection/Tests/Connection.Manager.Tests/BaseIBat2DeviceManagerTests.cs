﻿using System.Linq;
using System.Threading;
using Bat2Library.Connection.Interface.Domain.Old;
using Bat2Library.Connection.Interface.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests
{
   /// <summary>
   /// For this tests any BAT2 must be connected to some port
   /// </summary>
   [TestClass]
   public abstract class BaseIBat2DeviceManagerTests<S, T, D, R, W, C>
      where S : BaseVersionInfo
      where T : BaseConfiguration<S>
      where D : BaseBat2DeviceData<S, T>
      where R : IBat2DataReader<S, T>
      where W : IBat2DataWriter<S, T>
      where C : IBat2ActionCommander
   {
      public IBat2DeviceManager<S, T, D, R, W, C> Manager { get; private set; }

      protected BaseIBat2DeviceManagerTests(IBat2DeviceManager<S, T, D, R, W, C> manager)
      {
         Manager = manager;
         Thread.Sleep(3000);
      }

      [TestMethod]
      public void CanFindAnyDevices()
      {
         Assert.IsTrue(Manager.ConnectedDevices.Count() != 0, "Device count is 0 - try connect BAT2");
      }

      [TestMethod]
      public void ConnectedDeviceHasReaderWriterActionObjects()
      {
         var device = Manager.ConnectedDevices.First();
         Assert.IsNotNull(Manager.GetDataReader(device), "Data reader don't exists!");
         Assert.IsNotNull(Manager.GetDataWriter(device), "Data writer don't exists!");
         Assert.IsNotNull(Manager.GetActionCommander(device), "Action commander don't exists!");
      }

      [TestMethod]
      public void ConnectedDevice_ReadValidConfiguration()
      {
         var device = Manager.ConnectedDevices.First();
         var reader = Manager.GetDataReader(device);
         var configuration = device.Configuration ?? reader.LoadConfig();

         Assert.IsNotNull(configuration, "Device configuration not loaded (is NULL)!");
         Assert.IsTrue(device.IsValid, "Device is invalid!");
      }

      [TestMethod]
      public void ConnectedDevice_DevDataConfigurationChanged_WhenLoadedSeparatelly()
      {
         var devData = Manager.ConnectedDevices.First();
         var reader = Manager.GetDataReader(devData);
         var newConfiguration = reader.LoadConfig();
         if (newConfiguration == null)
         {
            Assert.IsNotNull(devData.Configuration);
         }
         else
         {
            Assert.AreSame(devData.Configuration, newConfiguration);
         }
      }
   }
}