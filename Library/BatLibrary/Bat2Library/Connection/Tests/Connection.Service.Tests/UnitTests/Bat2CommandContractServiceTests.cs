﻿using System.IO;
using System.ServiceModel;
using Bat2Library.Connection;
using Bat2Library.Connection.Interface;
using Services.FileUpload;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace Connection.Service.Tests.UnitTests
{
   [TestClass]
   public class Bat2CommandContractServiceTests
   {
      private IKernel kernel;

      [TestInitialize]
      public void Init()
      {
         kernel = new StandardKernel();
         // Load modules
         kernel.Load(new[] { new ConnectedDevicesModule()});
         var controller = kernel.Get<IConnectedDevicesController>();
         controller.Run();
      }

      [TestCleanup]
      public void Clean()
      {
         var controller = kernel.Get<IConnectedDevicesController>();
         controller.Shutdown();
         if (File.Exists("ahoj.txt"))
         {
            File.Delete("ahoj.txt");   
         }
         
      }

      [TestMethod]
      public void FileTransfer_Test()
      {
         var fileFactory = new ChannelFactory<IDataUpload>("IDataUpload");
         var uploadRequestInfo = new RemoteFileInfo();
         var buffer = System.Text.Encoding.UTF8.GetBytes("Nazdar, čo si ma nepametáš?");
         using (var stream = new MemoryStream(buffer))
         {
            uploadRequestInfo.InputStream = stream;
            fileFactory.CreateChannel().LoadFile(uploadRequestInfo);
         }
         
         Assert.IsTrue(File.Exists("ahoj.txt"));
         
      }

   }
}
