﻿using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager;
using Ninject.Modules;

namespace Bat2Library.Connection
{
   public class ConnectedDevicesModule : NinjectModule
   {
      #region Overrides of NinjectModule

      /// <summary>
      /// Loads the module into the kernel.
      /// </summary>
      public override void Load()
      {
         //Bind<IBat2OldDeviceManager>().ToMethod(context => Bat2ConnectionManager.Instance).InSingletonScope();
         Bind<IBat2NewDeviceManager>().ToMethod(context => Bat2ConnectionManager.Instance).InSingletonScope();
      }

      #endregion
   }
}