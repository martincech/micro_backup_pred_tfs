﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.Domain.Old;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;

namespace Bat2Library.Connection.Manager
{
   /// <summary>
   /// Device manager is responsible to raise events when device is connected/disconnected.
   /// It also gives interfaces for data access to underlying connected devices. 
   /// </summary>
   public class Bat2ConnectionManager :
      IBat2NewDeviceManager, IBat2OldDeviceManager
   {
      #region Fields

      private static Bat2ConnectionManager _instance;

      private event EventHandler<OldBat2DeviceData> DeviceConnectedOld;
      private event EventHandler<OldBat2DeviceData> DeviceDisconnectedOld;
      private event EventHandler<Bat2DeviceData> DeviceConnectedNew;
      private event EventHandler<Bat2DeviceData> DeviceDisconnectedNew;

      /// <summary>
      /// Existing managers
      /// </summary>
      private List<Object> deviceManagers;

      #endregion

      #region Constructor

      public static Bat2ConnectionManager Instance
      {
         get
         {
            lock (typeof (Bat2ConnectionManager))
            {
               return _instance ?? (_instance = new Bat2ConnectionManager());
            }
         }
      }

      private Bat2ConnectionManager()
      {
         deviceManagers = new List<Object>
         {
            UsbBat2NewDeviceManager.Instance,
            FlashBat2NewDeviceManager.Instance,
            //UsbBat2OldDeviceManager.Instance
         };

         foreach (IBat2NewDeviceManager bat2Manager in deviceManagers.Where(m => m is IBat2NewDeviceManager))
         {
            bat2Manager.DeviceConnected += Bat2DeviceConnected;
            bat2Manager.DeviceDisconnected += Bat2DeviceDisconnected;
         }
         foreach (IBat2OldDeviceManager bat2Manager in deviceManagers.Where(m => m is IBat2OldDeviceManager))
         {
            bat2Manager.DeviceConnected += Bat2DeviceConnected;
            bat2Manager.DeviceDisconnected += Bat2DeviceDisconnected;
         }
      }

      public void Dispose()
      {
         if (deviceManagers == null)
         {
            return;
         }
         foreach (IBat2NewDeviceManager bat2Manager in deviceManagers.Where(m => m is IBat2NewDeviceManager))
         {
            bat2Manager.DeviceConnected -= Bat2DeviceConnected;
            bat2Manager.DeviceDisconnected -= Bat2DeviceDisconnected;
            bat2Manager.Dispose();
         }
         foreach (IBat2OldDeviceManager bat2Manager in deviceManagers.Where(m => m is IBat2OldDeviceManager))
         {
            bat2Manager.DeviceConnected -= Bat2DeviceConnected;
            bat2Manager.DeviceDisconnected -= Bat2DeviceDisconnected;
            bat2Manager.Dispose();
         }
         deviceManagers = null;
      }

      #endregion

      public event EventHandler<IBaseBat2DeviceData> DeviceConnected;

      public event EventHandler<IBaseBat2DeviceData> DeviceDisconnected;

      public IEnumerable<IBaseBat2DeviceData> ConnectedDevices
      {
         get
         {
            var oldD = ((IDeviceConnectionManager<OldBat2DeviceData>) this).ConnectedDevices;
            var newD = ((IDeviceConnectionManager<Bat2DeviceData>) this).ConnectedDevices;
            return oldD.Cast<IBaseBat2DeviceData>().Concat(newD);
         }
      }

      /// <summary>
      /// Gets adequate datareader object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get data reader for</param>
      /// <returns>data reader object</returns>
      public IBat2DataReader<S, T> GetDataReader<S, T>(BaseBat2DeviceData<S, T> forDevice)
         where S : BaseVersionInfo
         where T : BaseConfiguration<S>
      {
         if (forDevice is Bat2DeviceData)
         {
            return GetDataReader(forDevice as Bat2DeviceData) as IBat2DataReader<S, T>;
         }
         if (forDevice is OldBat2DeviceData)
         {
            return GetDataReader(forDevice as OldBat2DeviceData) as IBat2DataReader<S, T>;
         }
         Debug.Assert(false, "Imposible type!");
         return null;
      }

      /// <summary>
      /// Gets adequate datawriter object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get data writer for</param>
      /// <returns>data writer object</returns>
      public IBat2DataWriter<S, T> GetDataWriter<S, T>(BaseBat2DeviceData<S, T> forDevice)
         where S : BaseVersionInfo
         where T : BaseConfiguration<S>
      {
         if (forDevice is Bat2DeviceData)
         {
            return GetDataWriter(forDevice as Bat2DeviceData) as IBat2DataWriter<S, T>;
         }
         if (forDevice is OldBat2DeviceData)
         {
            return GetDataWriter(forDevice as OldBat2DeviceData) as IBat2DataWriter<S, T>;
         }
         Debug.Assert(false, "Imposible type!");
         return null;
      }

      /// <summary>
      /// Gets apropriate action writer object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get action commander for</param>
      /// <returns>action writer object</returns>
      public IBat2ActionCommander GetActionCommander<S, T>(BaseBat2DeviceData<S, T> forDevice)
         where S : BaseVersionInfo
         where T : BaseConfiguration<S>
      {
         if (forDevice is Bat2DeviceData)
         {
            return GetActionCommander(forDevice as Bat2DeviceData);
         }
         if (forDevice is OldBat2DeviceData)
         {
            return GetActionCommander(forDevice as OldBat2DeviceData);
         }
         Debug.Assert(false, "Imposible type!");
         return null;
      }

      #region Implementation of IBat2OldDeviceManager

      event EventHandler<OldBat2DeviceData> IDeviceConnectionManager<OldBat2DeviceData>.DeviceConnected
      {
         add { DeviceConnectedOld += value; }
         remove { DeviceConnectedOld -= value; }
      }

      event EventHandler<OldBat2DeviceData> IDeviceConnectionManager<OldBat2DeviceData>.DeviceDisconnected
      {
         add { DeviceDisconnectedOld += value; }
         remove { DeviceDisconnectedOld -= value; }
      }

      /// <summary>
      /// Return collection of all connected devices
      /// </summary> 
      IEnumerable<OldBat2DeviceData> IDeviceConnectionManager<OldBat2DeviceData>.ConnectedDevices
      {
         get
         {
            return
               deviceManagers.Where(m => m is IBat2OldDeviceManager)
                  .Cast<IBat2OldDeviceManager>()
                  .SelectMany(manager => manager.ConnectedDevices);
         }
      }

      /// <summary>
      /// Gets adequate datareader object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get data reader for</param>
      /// <returns>data reader object</returns>
      public IBat2OldDataReader GetDataReader(OldBat2DeviceData forDevice)
      {
         return
            deviceManagers.Where(m => m is IBat2OldDeviceManager)
               .Cast<IBat2OldDeviceManager>()
               .Select(manager => manager.GetDataReader(forDevice))
               .FirstOrDefault(m => m != null);
      }

      /// <summary>
      /// Gets adequate datawriter object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get data writer for</param>
      /// <returns>data writer object</returns>
      public IBat2OldDataWriter GetDataWriter(OldBat2DeviceData forDevice)
      {
         return
            deviceManagers.Where(m => m is IBat2OldDeviceManager)
               .Cast<IBat2OldDeviceManager>()
               .Select(manager => manager.GetDataWriter(forDevice))
               .FirstOrDefault(m => m != null);
      }

      /// <summary>
      /// Gets apropriate action writer object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get action commander for</param>
      /// <returns>action writer object</returns>
      public IBat2ActionCommander GetActionCommander(OldBat2DeviceData forDevice)
      {
         return
            deviceManagers.Where(m => m is IBat2OldDeviceManager)
               .Cast<IBat2OldDeviceManager>()
               .Select(manager => manager.GetActionCommander(forDevice))
               .FirstOrDefault(m => m != null);
      }

      #endregion

      #region Implementation of IBat2NewDeviceManager

      event EventHandler<Bat2DeviceData> IDeviceConnectionManager<Bat2DeviceData>.DeviceConnected
      {
         add { DeviceConnectedNew += value; }
         remove { DeviceConnectedNew -= value; }
      }

      event EventHandler<Bat2DeviceData> IDeviceConnectionManager<Bat2DeviceData>.DeviceDisconnected
      {
         add { DeviceDisconnectedNew += value; }
         remove { DeviceDisconnectedNew -= value; }
      }

      /// <summary>
      /// Return collection of all connected devices
      /// </summary> 
      IEnumerable<Bat2DeviceData> IDeviceConnectionManager<Bat2DeviceData>.ConnectedDevices
      {
         get
         {
            return
               deviceManagers.Where(m => m is IBat2NewDeviceManager)
                  .Cast<IBat2NewDeviceManager>()
                  .SelectMany(manager => manager.ConnectedDevices);
         }
      }

      /// <summary>
      /// Gets adequate datareader object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get data reader for</param>
      /// <returns>data reader object</returns>
      public IBat2NewDataReader GetDataReader(Bat2DeviceData forDevice)
      {
         return
            deviceManagers.Where(m => m is IBat2NewDeviceManager)
               .Cast<IBat2NewDeviceManager>()
               .Select(manager => manager.GetDataReader(forDevice))
               .FirstOrDefault(m => m != null);
      }

      /// <summary>
      /// Gets adequate datawriter object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get data writer for</param>
      /// <returns>data writer object</returns>
      public IBat2NewDataWriter GetDataWriter(Bat2DeviceData forDevice)
      {
         return
            deviceManagers.Where(m => m is IBat2NewDeviceManager)
               .Cast<IBat2NewDeviceManager>()
               .Select(manager => manager.GetDataWriter(forDevice))
               .FirstOrDefault(m => m != null);
      }

      /// <summary>
      /// Gets apropriate action writer object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get action commander for</param>
      /// <returns>action writer object</returns>
      public IBat2ActionCommander GetActionCommander(Bat2DeviceData forDevice)
      {
         return
            deviceManagers.Where(m => m is IBat2NewDeviceManager)
               .Cast<IBat2NewDeviceManager>()
               .Select(manager => manager.GetActionCommander(forDevice))
               .FirstOrDefault(m => m != null);
      }

      #endregion

      #region Helpers

      #region Event handlers

      /// <summary>
      /// Event handler for USB device connection
      /// </summary>
      /// <param name="sender">UsbDeviceManager</param>
      /// <param name="e">Bat device identification</param>
      private void Bat2DeviceConnected<T>(object sender, T e)
         where T : IBaseBat2DeviceData
      {
         OnDeviceConnected(e);
      }

      /// <summary>
      /// Event handler for USB device disconnection
      /// </summary>
      /// <param name="sender">UsbDeviceManager</param>
      /// <param name="e">Bat device identification</param>
      private void Bat2DeviceDisconnected<T>(object sender, T e)
         where T : IBaseBat2DeviceData
      {
         OnDeviceDisconnected(e);
      }

      #endregion

      #region Event invocators

      protected virtual void OnDeviceConnected<T>(T e)
         where T : IBaseBat2DeviceData
      {
         if (typeof (T) == typeof (Bat2DeviceData))
         {
            var handler = DeviceConnectedNew;
            if (handler != null) handler(this, e as Bat2DeviceData);
         }

         if (typeof (T) == typeof (OldBat2DeviceData))
         {
            var handler = DeviceConnectedOld;
            if (handler != null) handler(this, e as OldBat2DeviceData);
         }
         OnDeviceConnected(e as IBaseBat2DeviceData);
      }

      protected virtual void OnDeviceDisconnected<T>(T e)
         where T : IBaseBat2DeviceData
      {
         if (typeof (T) == typeof (Bat2DeviceData))
         {
            var handler = DeviceDisconnectedNew;
            if (handler != null) handler(this, e as Bat2DeviceData);
         }

         if (typeof (T) == typeof (OldBat2DeviceData))
         {
            var handler = DeviceDisconnectedOld;
            if (handler != null) handler(this, e as OldBat2DeviceData);
         }
         OnDeviceDisconnected(e as IBaseBat2DeviceData);
      }

      protected virtual void OnDeviceConnected(IBaseBat2DeviceData e)

      {
         var handler = DeviceConnected;
         if (handler != null) handler(this, e);
      }

      protected virtual void OnDeviceDisconnected(IBaseBat2DeviceData e)

      {
         var handler = DeviceDisconnected;
         if (handler != null) handler(this, e);
      }

      #endregion

      #endregion
   }
}
