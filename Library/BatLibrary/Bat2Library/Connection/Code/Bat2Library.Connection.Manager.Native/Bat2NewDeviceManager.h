#pragma once
#include "Bat2DeviceManager.h"
#include "Bat2NewDataRW.h"
#include "Bat2NewDevice.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System::Diagnostics;
            using namespace Bat2Library::Connection::Interface::IO;

            public ref class Bat2NewDeviceManager abstract :
               Bat2DeviceManager <
                  Bat2NewINativeSocket ^,
                  VersionInfo ^,
                  Configuration ^,
                  Bat2DeviceData ^,
                  IBat2NewDataReader ^,
                  IBat2NewDataWriter ^,
                  IBat2ActionCommander ^>,
               IBat2NewDeviceManager
            {            
            protected:
               virtual Bat2DataRW<VersionInfo ^, Configuration ^, Bat2NewINativeSocket ^, Bat2DeviceData ^> ^CreateDataRwObject(Bat2Device<VersionInfo ^, Configuration ^, Bat2NewINativeSocket ^, Bat2DeviceData^> ^dev) override sealed
               {
                  Bat2NewDevice ^b2nd = static_cast<Bat2NewDevice ^>(dev);
                  Debug::Assert(b2nd != nullptr);
                  return CreateDataRwObject(b2nd);
               }

               virtual Bat2NewDataRW ^CreateDataRwObject(Bat2NewDevice ^dev) override sealed
               {
                  return gcnew Bat2NewDataRW(dev);
               }
            };
         }
      }
   }
}