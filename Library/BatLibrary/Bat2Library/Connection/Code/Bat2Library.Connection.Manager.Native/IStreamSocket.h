#pragma once

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System::IO;

            public interface class IStreamSocket
            {
               property Stream ^Socket
               {
                  Stream ^get();
                  void set(Stream ^value);
               }
            };
         }
      }
   }
}