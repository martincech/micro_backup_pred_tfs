#include "FlashBat2NewDeviceManager.h"
#include "FlashBat2Device.h"
#include "Bat2FlashSocket.h"

using namespace Bat2Library::Connection::Manager::Native;
using namespace System::Text::RegularExpressions;
using namespace Usb;
using namespace Usb::UMS;

FlashBat2NewDeviceManager::FlashBat2NewDeviceManager()
{
   existingDevices = gcnew Dictionary<UmsDevice ^, IEnumerable<Bat2NewDevice^> ^>();
   usbWatcher = UsbDeviceManager::Instance;
   usbWatcher->DeviceConnected += gcnew System::EventHandler<Usb::UsbDevice ^>(this, &Connection::Manager::Native::FlashBat2NewDeviceManager::OnDeviceConnected);
   usbWatcher->DeviceDisconnected += gcnew System::EventHandler<Usb::UsbDevice ^>(this, &Connection::Manager::Native::FlashBat2NewDeviceManager::OnDeviceDisconnected);
}

static void DirectoryCopy(String ^sourceDirName, String ^destDirName, bool copySubDirs = true)
{
   // Get the subdirectories for the specified directory.
   DirectoryInfo ^dir = gcnew DirectoryInfo(sourceDirName);
   if (!dir->Exists)
   {
      return;
   }
   array<DirectoryInfo^> ^dirs = dir->GetDirectories();
   // If the destination directory  doesn't exist, create it. 
   if (!Directory::Exists(destDirName))
   {
      Directory::CreateDirectory(destDirName);
   }
   
   // Get the files in the directory and copy them to the new location.
   array<FileInfo^> ^files = dir->GetFiles();
   for each(FileInfo ^file in files)
   {
      String ^temppath = Path::Combine(destDirName, file->Name);
      file->CopyTo(temppath, true);
   }

   // If copying subdirectories, copy them and their contents to new location. 
   if (copySubDirs)
   {
      for each(DirectoryInfo ^subdir in dirs)
      {
         String ^temppath = Path::Combine(destDirName, subdir->Name);
         DirectoryCopy(subdir->FullName, temppath, copySubDirs);
      }
   }
}

void FlashBat2NewDeviceManager::OnDeviceConnected(System::Object ^sender, UsbDevice ^e)
{
   UmsDevice ^dev = dynamic_cast<UMS::UmsDevice ^>(e);
   if (dev == nullptr){ return; }
   if (existingDevices->ContainsKey(dev)){ return; }

   List<Bat2NewDevice^> ^b2devs = gcnew List<Bat2NewDevice^>();
   for each (String ^basePath in dev->BasePaths)
   {
      String ^baseDirectory = Path::Combine(basePath, BaseDirectoryName);
      if (!Directory::Exists(baseDirectory)){
         continue;
      }
      // TODO identify and load each file
      array<String ^> ^serials = Directory::GetDirectories(baseDirectory);
      Regex ^dirRegex = gcnew Regex("[\da-fA-F]*");
      for each (String ^deviceDirectory in serials)
      {
         String ^directoryName = Path::GetFileName(deviceDirectory);
         if (!dirRegex->IsMatch(directoryName)){ continue; }
         int serialNumber;
         try{
            serialNumber = int::Parse(directoryName);
         } catch(Exception ^){
            continue;
         }
          
         String ^backupDirectory = deviceDirectory + "_BACKUP";
         DirectoryCopy(deviceDirectory, backupDirectory);
         
         Bat2FlashSocket ^socket = gcnew Bat2FlashSocket(basePath, serialNumber);
         FlashBat2Device ^b2d = gcnew FlashBat2Device(socket);
         Bat2NewDataRW dLoader(b2d);
         Bat2DeviceData ^data;
         while ((data = dLoader.LoadAll()) != nullptr){
         }
         b2devs->Add(b2d);
         socket->EverythingRead = true;
         Directory::Delete(deviceDirectory);
         Directory::Move(backupDirectory, deviceDirectory);
      }
   }

   if (b2devs->Count != 0){
      existingDevices->Add(dev, b2devs);
      for each (Bat2NewDevice ^b2d in b2devs)
      {
		  Bat2DeviceData ^data = b2d->DeviceData;
		  if (data == nullptr){ continue; }

		  if (data->Configuration->VersionInfo->IsValid){
            Devices->Add(b2d);
			   DeviceConnected(this, data);
         }
      }
   }
}


void FlashBat2NewDeviceManager::OnDeviceDisconnected(System::Object ^sender, UsbDevice ^e)
{
   UmsDevice ^dev = dynamic_cast<UMS::UmsDevice ^>(e);
   if (dev == nullptr){ return; }
   if (existingDevices->ContainsKey(dev)){
      for each (Bat2NewDevice ^b2d in existingDevices[dev])
      {
		  Bat2DeviceData ^data = b2d->DeviceData;
		  if (data == nullptr){ continue; }

		 DeviceDisconnected(this, data);
         Devices->Remove(b2d);
         delete b2d;
      }
      existingDevices->Remove(dev);
   }
}