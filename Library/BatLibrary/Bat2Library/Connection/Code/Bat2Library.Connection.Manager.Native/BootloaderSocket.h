#pragma once

#undef _MANAGED
#include "Remote\Frame.h"
#include "Remote\SocketIf.h"

private class BootloaderSocketIf : SocketIf
{
public:
   BootloaderSocketIf(System::String ^efsRoot);

   virtual byte State(void);

   virtual TYesNo Receive(void *Buffer, int Size);

   virtual int ReceiveSize(void);

   virtual TYesNo Send(const void *Buffer, int Size);

   virtual void Close(void);

   virtual byte Permission(void);

   bool OpenForRead();
   bool OpenForWrite();

private:
   bool IsOpenedForRead();
   bool IsOpenedForWrite();
};
