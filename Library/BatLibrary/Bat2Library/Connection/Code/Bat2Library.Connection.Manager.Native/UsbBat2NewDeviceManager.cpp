#include "UsbBat2NewDeviceManager.h"

using namespace Bat2Library::Connection::Manager::Native;
using namespace System::Runtime::Remoting::Messaging;
using namespace System::Diagnostics;
using namespace System::Threading;
using namespace Usb;

UsbBat2NewDeviceManager::UsbBat2NewDeviceManager()
{
   existingDevices = gcnew Dictionary<HidDevice ^, Bat2NewDevice ^>();
   usbWatcher = UsbDeviceManager::Instance;
   usbWatcher->DeviceConnected += gcnew System::EventHandler<Usb::UsbDevice ^>(this, &Connection::Manager::Native::UsbBat2NewDeviceManager::OnDeviceConnected);
   usbWatcher->DeviceDisconnected += gcnew System::EventHandler<Usb::UsbDevice ^>(this, &Connection::Manager::Native::UsbBat2NewDeviceManager::OnDeviceDisconnected);
}

void UsbBat2NewDeviceManager::OnDeviceConnected(System::Object ^sender, Usb::UsbDevice ^e)
{
   HidDevice ^dev = dynamic_cast<HID::HidDevice ^>(e);
   if (dev == nullptr || !UsbBat2Device::IsValid(dev->VendorID, dev->ProductID)){ return; }
   if (existingDevices->ContainsKey(dev)){ return; }
   UsbBat2Device ^b2d = CreateUsbBat2Device(dev);
   Bat2NewDataRW ^dLoader = CreateDataRwObject(b2d);
   bool validLoad = false;
   for (int i = 0; i < 3; i++)
   {
      Configuration ^config = dLoader->LoadConfig();
      // try to load identification with 3 tries
	  if (config != nullptr && config->VersionInfo->IsValid){
         validLoad = true;
         break;
      }
      // sleep for awhile
      Thread::CurrentThread->Sleep(20);
   }
   if (!validLoad)
   {
      return;
   }

   Bat2DeviceData ^data = b2d->DeviceData;
   if (data == nullptr){ return; }

#ifdef _DEBUG

   if (data->Configuration->VersionInfo->SerialNumber == 0){
	   data->Configuration->VersionInfo->SerialNumber = b2d->UsbSocket->GetHashCode();
   }
#endif
   existingDevices->Add(dev, b2d);
   if (data->Configuration->VersionInfo->IsValid){
      Devices->Add(b2d);
	  DeviceConnected(this, data);
   }
}

void UsbBat2NewDeviceManager::OnDeviceDisconnected(System::Object ^sender, Usb::UsbDevice ^e)
{
   HidDevice ^dev = dynamic_cast<HidDevice ^>(e);
   if (dev == nullptr || !UsbBat2Device::IsValid(dev->VendorID, dev->ProductID)){ return; }
   if (existingDevices->ContainsKey(dev)){
      Bat2NewDevice ^b2d = existingDevices[dev];
      Bat2DeviceData ^data = b2d->DeviceData;
	   if (data == nullptr){ return; }

	   DeviceDisconnected(this, data);
      Devices->Remove(b2d);
      delete b2d;
   }
}
