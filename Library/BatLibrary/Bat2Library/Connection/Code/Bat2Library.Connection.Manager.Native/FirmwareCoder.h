#pragma once
using namespace System;

namespace Bat2Library
{
   using namespace System::Runtime::InteropServices;
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            public ref class FirmwareCoder abstract sealed
            {
            public:
               // Encode InputFile to firmware file and save this file to OutputFilePath
               static bool Encode(String ^InputFile, String ^OutputFilePath);

               // encode byte array of data to output array of data
               static bool Encode(array<System::Byte> ^inputData, [Out]array<System::Byte> ^%encodedData);
            };
         }
      }
   }
}
