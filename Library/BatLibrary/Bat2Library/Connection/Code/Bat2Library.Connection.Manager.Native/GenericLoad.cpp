#include "Bat2NewDataRW.h"
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Net;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::Domain;
using namespace Bat2Library::Connection::Interface;
#include <assert.h>

#undef _MANAGED
#include "Config\Config.h"
#include "Config\ConfigDef.h"
#include "Display\DisplayConfigurationDef.h"
#include "Weighing\WeighingConfigurationDef.h"
#include "Message\GsmMessageDef.h"
#include "Rs485\Rs485Config.h"
#include "Modbus\ModbusConfigDef.h"
#include "Megavi\MegaviConfigDef.h"
#include "SmsGate\SmsGateConfigDef.h"
#include "Dacs\DacsConfigDef.h"
#include "CurvesPlan.h"
#include "Communication\CommunicationDef.h"

bool fromNative(Bat2Library::Connection::Interface::Domain::Configuration ^%config);
extern VersionInfo ^fromNativeDeviceVersion();
extern DeviceInfo ^fromNativeDevice();
extern Bat2Library::Connection::Interface::Domain::Country ^fromNativeCountry();
extern WeightUnits ^fromNativeWeightUnits();
extern PlatformCalibration ^fromNativeCalibration();
extern DisplayConfiguration ^fromNativeDisplayConfiguration();
extern WeighingConfiguration ^fromNativeWeighingConfiguration();
extern List<Domain::Rs485Options^> ^fromNativeRs485Configuration();
extern ModbusOptions ^fromNativeModbusConfiguration();
extern MegaviOptions ^fromNativeMegaviConfiguration();
extern DacsOptions ^fromNativeDacsConfiguration();
extern GsmMessage ^fromNativeGsmMessage();
extern DataPublication ^fromNativeDataPublication();
extern CellularData ^fromNativeCellularData();
extern Ethernet ^fromNativeEthernet();

extern bool LoadList(List<WeighingPlan^> ^%plans);
extern bool LoadList(List<WeighingConfiguration^> ^%predefined);
extern bool LoadList(List<Contact^> ^%contacts);

extern Context^ LoadContextData();
extern Domain::MenuContext ^fromNativeMenuContext();
extern CalibrationContext ^fromNativeCalibrationContext();
extern WeighingContext ^fromNativeWeighingContext();

#ifdef DeviceVersion
#undef DeviceVersion
#endif

bool Bat2NewDataRW::fromNativeGeneric(Object ^%o)
{
	Type ^type = o->GetType();   
	if (type == VersionInfo::typeid){
		o = fromNativeDeviceVersion();
	}
	else if (type == Domain::Configuration::typeid){
		Domain::Configuration ^data = safe_cast<Domain::Configuration^>(o);
		if (!fromNative(data))
		{
			return false;
		}
		o = data;
		return true;
	}
	else if (type == Context::typeid){
		o = LoadContextData();
	}
	else if (type == DeviceInfo::typeid){
		o = fromNativeDevice();
	}
	else if (type == Country::typeid){
		o = fromNativeCountry();
	}
	else if (type == WeightUnits::typeid){
		o = fromNativeWeightUnits();
	}
	else if (type == PlatformCalibration::typeid){
		o = fromNativeCalibration();
	}
	else if (type == WeighingConfiguration::typeid){
		o = fromNativeWeighingConfiguration();
	}
	else if (type == DisplayConfiguration::typeid){
		o = fromNativeDisplayConfiguration();
	}
	else if (type == ModbusOptions::typeid){
		o = fromNativeModbusConfiguration();
	}
	else if (type == MegaviOptions::typeid){
		o = fromNativeMegaviConfiguration();
	}
	else if (type == DacsOptions::typeid){
		o = fromNativeDacsConfiguration();
	}
	else if (type == GsmMessage::typeid){
		o = fromNativeGsmMessage();
	}
	else if (type == DataPublication::typeid){
		o = fromNativeDataPublication();
	}
	else if (type == CellularData::typeid){
		o = fromNativeCellularData();
	}
	else if (type == Ethernet::typeid){
		o = fromNativeEthernet();
	}
	else if (type == List<WeighingPlan^>::typeid){
		List<WeighingPlan^> ^data = (List<WeighingPlan^>^)o;
		LoadList(data);
	}
	else if (type == List<Domain::Rs485Options^>::typeid){
		 o = fromNativeRs485Configuration();
	}
	else if (type == List<WeighingConfiguration^>::typeid){
		List<WeighingConfiguration^> ^data = (List<WeighingConfiguration^>^)o;
		LoadList(data);
	}
	else if (type == List<Contact^>::typeid){
		List<Contact^> ^data = (List<Contact^>^)o;
		LoadList(data);
	}
	else if (type == Domain::MenuContext::typeid){
		o = fromNativeMenuContext();
	}
	else if (type == CalibrationContext::typeid){
		o = fromNativeCalibrationContext();
	}
	else if (type == WeighingContext::typeid){
		o = fromNativeWeighingContext();
	}
	else {
		return false;
	}
	return true;
}