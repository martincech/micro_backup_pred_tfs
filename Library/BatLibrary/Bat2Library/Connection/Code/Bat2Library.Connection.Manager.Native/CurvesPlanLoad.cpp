#include "Bat2NewDataRW.h"
using namespace System;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface;
using namespace Bat2Library::Connection::Interface::Domain;
#include <assert.h>

#undef _MANAGED
#include "CurvesPlan.h"
#include "Predefined\PredefinedList.h"
#include "Data\uDirectory.h"

//bool LoadGrowthList(List<Curve^> ^%curves);
//bool LoadCorrectionList(List<Curve^> ^%curves);
//bool LoadList(List<WeighingPlan^> ^%plans);
//bool LoadList(List<WeighingConfiguration^> ^%predefined);

bool Bat2NewDataRW::LoadGrowth(List<Curve^> ^%curves)
{
   if (device->Socket->CurveListRemoteLoad() == NO){
      return false;
   }
   return LoadGrowthList(curves);
}



bool Bat2NewDataRW::LoadGrowthList(List<Curve^> ^%curves)
{
	TCurveList list;
	TCurveIndex itemCount;
	TGrowthCurve item;
	CurveListInit();
	if (CurveListOpen(&list) == NO){
		return false;
	}
	itemCount = uDirectoryCount(&list);
	for (TCurveIndex i = 0; i < itemCount; i++){
		CurveListLoad(&list, &item, i);
		curves->Add(fromNativeGrowthCurve(&item));
	}
	CurveListClose(&list);
	return true;
}

bool Bat2NewDataRW::LoadCorrection(List<Curve^> ^%curves)
{
   if (device->Socket->CorrectionListRemoteLoad() == NO){
      return false;
   }
   return LoadCorrectionList(curves);
}

bool Bat2NewDataRW::LoadCorrectionList(List<Curve^> ^%curves)
{
	TCorrectionList list;
	TCorrectionIndex itemCount;
	TCorrectionCurve item;
	CorrectionListInit();
	if (CorrectionListOpen(&list) == NO){
		return false;
	}
	itemCount = uDirectoryCount(&list);
	for (TCurveIndex i = 0; i < itemCount; i++){
		CorrectionListLoad(&list, &item, i);
		curves->Add(fromNativeCorrectionCurve(&item));
	}
	CorrectionListClose(&list);
	return true;
}

bool Bat2NewDataRW::Load(List<WeighingPlan^> ^%plans)
{
	if (device->Socket->WeighingPlanListRemoteLoad() == NO){
      return false;
   }
   return LoadList(plans);
}

bool Bat2NewDataRW::LoadList(List<WeighingPlan^> ^%plans)
{
	TWeighingPlanList list;
	TWeighingPlanIndex itemCount;
	TWeighingPlan item;
	WeighingPlanListInit();
	if (WeighingPlanListOpen(&list) == NO){
		return false;
	}
	itemCount = WeighingPlanListCount(&list);
	for (TCurveIndex i = 0; i < itemCount; i++){
		WeighingPlanListLoad(&list, &item, i);
		plans->Add(fromNativeWeighingPlan(&item));
	}
	WeighingPlanListClose(&list);
	return true;
}

bool Bat2NewDataRW::Load(List<WeighingConfiguration^> ^%predefined)
{
	if (device->Socket->PredefinedListRemoteLoad() == NO){
      return false;
   }
	return LoadList(predefined);
}

bool Bat2NewDataRW::LoadList(List<WeighingConfiguration^> ^%predefined)
{	
	TPredefinedList list;
	TPredefinedWeighingIndex itemCount;
	TPredefinedWeighing item;
	PredefinedListInit();
	if (PredefinedListOpen(&list) == NO){
		return false;
	}
	itemCount = uDirectoryCount(&list);
	for (TPredefinedWeighingIndex i = 0; i < itemCount; i++){
		PredefinedListLoad(&list, &item, i);
		predefined->Add(fromNativeWeighingConfiguration(&item));
	}
	PredefinedListClose(&list);
	return true;
}