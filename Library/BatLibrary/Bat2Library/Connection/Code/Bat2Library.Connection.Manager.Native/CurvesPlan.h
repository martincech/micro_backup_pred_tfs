#pragma once
#undef _MANAGED
#include "Curve\CurveList.h"
#include "Curve\CorrectionList.h"
#include "Scheduler\WeighingPlanList.h"
#include "Predefined\PredefinedWeighingDef.h"

using namespace Bat2Library::Connection::Interface::Domain;

Curve ^fromNativeGrowthCurve(TGrowthCurve *curve);
Curve ^fromNativeCorrectionCurve(TCorrectionCurve *curve);
WeighingPlan ^fromNativeWeighingPlan(TWeighingPlan *plan);
WeighingConfiguration ^fromNativeWeighingConfiguration(TWeighingConfiguration *configuration);

void toNativePlan(TWeighingPlan *plan, WeighingPlan ^d);
void toNativeGrowthCurve(TGrowthCurve *curve, Curve ^d);
void toNativeCorrectionCurve(TCorrectionCurve *curve, Curve ^d);
void toNativeWeighingConfiguration(TWeighingConfiguration *configuration, WeighingConfiguration ^d);