#pragma once
#include "File\Efs.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;
            using namespace System::Runtime::InteropServices;
            using namespace System::IO;
            using namespace System::Collections::Generic;

            private ref class Efs abstract sealed
            {
            private:
               static Dictionary<TEfsFile, FileInfo^> ^fileDescriptors;
               static Dictionary<FileInfo ^, FileStream ^> ^openedFiles;
               static TEfsFile descriptor;

            public:

               static Efs()
               {
                  fileDescriptors = gcnew Dictionary<TEfsFile, FileInfo ^>();
                  openedFiles = gcnew Dictionary<FileInfo ^, FileStream ^>();
                  descriptor = 0;
               }

               static bool AddFile(String ^file, TEfsFile %fileDesc)
               {
                  if (!File::Exists(file)){
                     return false;
                  }
                  for each (KeyValuePair<TEfsFile, FileInfo ^> ^value in fileDescriptors)
                  {
                     if (value->Value->FullName->Equals(file)){
                        fileDesc = value->Key;
                        return true;
                     }
                  }
                  FileInfo ^info = gcnew FileInfo(file);
                  fileDescriptors->Add(++descriptor, info);
                  fileDesc = descriptor;
                  return true;
               }

               static bool DeleteFile(String ^file)
               {
                  TEfsFile fileDesc;
                  bool found = false;
                  for each (KeyValuePair<TEfsFile, FileInfo ^> ^value in fileDescriptors)
                  {
                     if (value->Value->FullName->Equals(file)){
                        fileDesc = value->Key;
                        found = true;
                        break;
                     }
                  }
                  if (found){
                     RemoveFile(fileDesc);
                  }
                  try{
                     File::Delete(file);
                  }
                  catch (Exception ^){
                     return false;
                  }
                  return true;
               }

               static dword FileSize(TEfsFile fileDesc)
               {
                  if (!fileDescriptors->ContainsKey(fileDesc)){
                     return 0;
                  }
                  FileInfo ^file = fileDescriptors[fileDesc];
                  return file->Length;
               }

               static bool RemoveFile(TEfsFile fileDesc){
                  if (!fileDescriptors->ContainsKey(fileDesc)){
                     return false;
                  }
                  FileInfo ^file = fileDescriptors[fileDesc];
                  FileStream ^fs;
                  if (openedFiles->TryGetValue(file, fs)){
                     fs->Close();
                     openedFiles->Remove(file);
                  }
                  fileDescriptors->Remove(fileDesc);
                  return true;

               }

               static FileStream ^GetFile(TEfsFile fileDesc){
                  FileInfo ^file;
                  if (!fileDescriptors->TryGetValue(fileDesc, file)){
                     return nullptr;
                  }
                  FileStream ^fs;
                  if (!openedFiles->TryGetValue(file, fs)){
                     fs = file->Open(FileMode::Open, FileAccess::ReadWrite);
                     openedFiles->Add(file, fs);
                  }
                  return fs;
               }

            };

#ifdef _DEBUG
            public ref class EfsTest sealed{
            public:
               bool TestSocket();

               EfsTest(System::String ^Directory);

               bool EfsInit(void);

               bool EfsFormat(void);

               bool EfsDirectoryExists(System::String ^Path);

               bool EfsDirectoryCreate(System::String ^Path);
               bool EfsDirectoryChange(System::String ^Path);
               bool EfsDirectoryDelete(System::String ^Path);
               bool EfsDirectoryRename(System::String ^Path, System::String ^NewName);
               bool EfsFileExists(System::String ^Name);
               bool EfsFileCreate(TEfsFile %File, System::String ^FileName, dword Size);
               bool EfsFileOpen(TEfsFile %File, System::String ^FileName);
               bool EfsFileDelete(System::String ^FileName);
               void EfsFileClose(TEfsFile %File);
               word EfsFileWrite(TEfsFile %File, array<byte> ^Data, word Count);
               word EfsFileRead(TEfsFile %File, array<byte> ^%Data, word Count);
               bool EfsFileSeek(TEfsFile %File, int32 Pos, SeekOrigin Whence);
            };
#endif
         }
      }
   }
}
