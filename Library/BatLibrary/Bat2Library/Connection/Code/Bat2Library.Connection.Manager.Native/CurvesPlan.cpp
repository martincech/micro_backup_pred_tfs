#include "CurvesPlan.h"
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;
using namespace System::Diagnostics;
using namespace Bat2Library;

#include <assert.h>
#undef _MANAGED
#include "Curve\CurveList.h"
#include "Curve\CorrectionList.h"
#include "Scheduler\WeighingPlanList.h"
#include "Time\uClock.h"
#include "Weight\Weight.h"

//------------------------------------------------------------------------------
//  Load Curves from native
//------------------------------------------------------------------------------

Curve ^fromNativeGrowthCurve(TGrowthCurve *curve)
{
   Debug::Assert(curve != NULL);
   Curve ^d = gcnew Curve();
   for (int i = 0; i < curve->Count; i++)
   {
      CurvePoint ^p = gcnew CurvePoint();
      p->ValueX = curve->Point[i].Day;
      p->ValueY = curve->Point[i].Weight;
      d->Points->Add(p);
   }
   d->Name = gcnew String(curve->Name);
   return d;
}

Curve ^fromNativeCorrectionCurve(TCorrectionCurve *curve)
{
   Debug::Assert(curve != NULL);
   Curve ^d = gcnew Curve();
   for (int i = 0; i < curve->Count; i++)
   {
      CurvePoint ^p = gcnew CurvePoint();
      p->ValueX = curve->Point[i].Day;
      p->ValueY = curve->Point[i].Factor;
      d->Points->Add(p);
   }
   d->Name = gcnew String(curve->Name);
   return d;
}

WeighingPlan ^fromNativeWeighingPlan(TWeighingPlan *plan)
{
   Debug::Assert(plan != NULL);
   WeighingPlan ^d = gcnew WeighingPlan();
   UTime dt;

   d->Name = gcnew String(plan->Name);
   d->SyncWithDayStart = plan->SyncWithDayStart == YES;
   d->WeighingDaysMode = (WeighingDaysModeE)plan->Days.Mode;
   d->WeighingDaysDays = (WeighingDaysMaskE)plan->Days.Days;
   d->WeighingDaysOnlineDays = plan->Days.WeighingDays;
   d->WeighingDaysSuspendedDays = plan->Days.SuspendedDays;
   d->WeighingDaysStartDay = plan->Days.StartDay;
   
   d->WeighingTimes = gcnew List<TimeRange^>();
   if (plan->TimesCount > WEIGHING_TIME_COUNT){ return d; }
   for (int i = 0; i < plan->TimesCount; i++)
   {
      TimeRange ^t = gcnew TimeRange();
      uTime(&dt, plan->Times[i].From);
      t->From = DateTime(uYearGet(1), 1, 1, dt.Hour, dt.Min, dt.Sec);
      uTime(&dt, plan->Times[i].To);
      t->To = DateTime(uYearGet(1), 1, 1, dt.Hour, dt.Min, dt.Sec);
      ((List<TimeRange^>^)d->WeighingTimes)->Add(t);
   }
   return d;
}

WeighingConfiguration ^fromNativeWeighingConfiguration(TWeighingConfiguration *weighingConfiguration)
{
   UDateTime dt;
   IntPtr str;
   Debug::Assert(weighingConfiguration != NULL);
   WeighingConfiguration ^d = gcnew WeighingConfiguration();

   d->Name = gcnew String(weighingConfiguration->Name);
   d->Flock = gcnew String(weighingConfiguration->Flock);	
	d->MenuMask = (WeighingConfigurationMenuMaskE)(0x1FFF - weighingConfiguration->MenuMask);
   d->InitialDay = weighingConfiguration->InitialDay;
   d->CorrectionCurveIndex = weighingConfiguration->CorrectionCurve;
   d->PredefinedIndex = weighingConfiguration->Predefined;

   d->MaleInitialWeight = WeightNumber(weighingConfiguration->Male.InitialWeight);
   d->MaleGrowthCurveIndex = weighingConfiguration->Male.GrowthCurve;
   d->FemaleInitialWeight = WeightNumber(weighingConfiguration->Female.InitialWeight);
   d->FemaleGrowthCurveIndex = weighingConfiguration->Female.GrowthCurve;

   d->Mode = weighingConfiguration->TargetWeights.Mode;
   d->Growth = (PredictionGrowthE)weighingConfiguration->TargetWeights.Growth;
   d->SexDifferentiation = (SexDifferentiationE)weighingConfiguration->TargetWeights.SexDifferentiation;
   d->Sex = (SexE)weighingConfiguration->TargetWeights.Sex;
   d->AdjustTargetWeights = (OnlineAdjustmentE)weighingConfiguration->TargetWeights.AdjustTargetWeights;

   d->Filter = weighingConfiguration->Detection.Filter;
   d->StabilizationRange = weighingConfiguration->Detection.StabilizationRange;
   d->StabilizationTime = weighingConfiguration->Detection.StabilizationTime;
   d->Step = (PlatformStepModeE)weighingConfiguration->Detection.Step;

   d->MaleMarginAbove = weighingConfiguration->Acceptance.Male.MarginAbove;
   d->MaleMarginBelow = weighingConfiguration->Acceptance.Male.MarginBelow;
   d->FemaleMarginAbove = weighingConfiguration->Acceptance.Female.MarginAbove;
   d->FemaleMarginBelow = weighingConfiguration->Acceptance.Female.MarginBelow;

   d->UniformityRange = weighingConfiguration->Statistic.UniformityRange;
   d->ShortPeriod = weighingConfiguration->Statistic.ShortPeriod;
   d->ShortType = (StatisticShortTypeE)weighingConfiguration->Statistic.ShortType;
   d->HistogramMode = (HistogramModeE)weighingConfiguration->Statistic.Histogram.Mode;
   d->HistogramRange = weighingConfiguration->Statistic.Histogram.Range;
   d->HistogramStep = weighingConfiguration->Statistic.Histogram.Step;

   uClock(&dt, weighingConfiguration->DayStart);
	int hour = dt.Time.Hour;
	int min = dt.Time.Min;
	int sec = dt.Time.Sec;
   d->DayStart = DateTime(uYearGet(1), 1, 1, dt.Time.Hour, dt.Time.Min, dt.Time.Sec);

   d->Planning = weighingConfiguration->Planning ? YES : NO;
   d->WeighingPlanIndex = weighingConfiguration->WeighingPlan;
   return d;
}

void toNativePlan(TWeighingPlan *plan, WeighingPlan ^d)
{
   Debug::Assert(plan != NULL);
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "WeighingPlan can't be null!");
   }
   if (String::IsNullOrEmpty(d->Name)){
      throw gcnew ArgumentOutOfRangeException("d", "WeighingPlan Name have to be non-zero length string!");
   }
   UTime dt;
   IntPtr str = Marshal::StringToHGlobalAnsi(d->Name);
   strcpy(plan->Name, (char*)str.ToPointer());
   Marshal::FreeHGlobal(str);
   plan->Days.Mode = (EWeighingDaysMode)d->WeighingDaysMode;
   plan->Days.Days = (EWeighingDaysMask)d->WeighingDaysDays;
   plan->Days.WeighingDays = d->WeighingDaysOnlineDays;
   plan->Days.SuspendedDays = d->WeighingDaysSuspendedDays;
   plan->Days.StartDay = d->WeighingDaysStartDay;
   plan->SyncWithDayStart = d->SyncWithDayStart ? YES : NO;
   int i = 0;
   for each (TimeRange ^t in d->WeighingTimes)
   {
      dt.Hour = t->From.Hour;
      dt.Min = t->From.Minute;
      dt.Sec = t->From.Second;
      plan->Times[i].From = uTimeGauge(&dt);

      dt.Hour = t->To.Hour;
      dt.Min = t->To.Minute;
      dt.Sec = t->To.Second;
      plan->Times[i].To = uTimeGauge(&dt);
      i++;
      if (i == WEIGHING_TIME_COUNT) break;
   }
   plan->TimesCount = i;
}
//------------------------------------------------------------------------------
// Save Curves to native
//------------------------------------------------------------------------------

void toNativeGrowthCurve(TGrowthCurve *curve, Curve ^d)
{
   Debug::Assert(curve != NULL);
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "Curve can't be null!");
   }
   if (String::IsNullOrEmpty(d->Name)){
      throw gcnew ArgumentOutOfRangeException("d", "Curve Name have to be non-zero length string!");
   }
   int i = 0;
   for each (CurvePoint ^p in d->Points)
   {
      curve->Point[i].Day = p->ValueX;
      curve->Point[i].Weight = p->ValueY;
      i++;
      if (i >= GROWTH_CURVE_POINT_COUNT) break;
   }
   curve->Count = i; 
   IntPtr str = Marshal::StringToHGlobalAnsi(d->Name);
   strncpy(curve->Name, (char*)str.ToPointer(), GROWTH_CURVE_NAME_SIZE);
   Marshal::FreeHGlobal(str);
}

void toNativeCorrectionCurve(TCorrectionCurve *curve, Curve ^d)
{
   Debug::Assert(curve != NULL);
   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "Curve can't be null!");
   }
   if (String::IsNullOrEmpty(d->Name)){
      throw gcnew ArgumentOutOfRangeException("d", "Curve Name have to be non-zero length string!");
   }
   int i = 0;
   for each (CurvePoint ^p in d->Points)
   {
      curve->Point[i].Day = p->ValueX;
      curve->Point[i].Factor = p->ValueY;
      i++;
      if (i >= CORRECTION_CURVE_POINT_COUNT) break;
   }
   curve->Count = i;
   IntPtr str = Marshal::StringToHGlobalAnsi(d->Name);
   strncpy(curve->Name, (char*)str.ToPointer(),CORRECTION_CURVE_NAME_SIZE);
   Marshal::FreeHGlobal(str);
}

void toNativeWeighingConfiguration(TWeighingConfiguration *weighingConfiguration, WeighingConfiguration ^d)
{
   UDateTime dt;
   IntPtr str;

   if (d == nullptr){
      throw gcnew ArgumentNullException("d", "WeighingConfiguration can't be null!");
   }
   if (d->Name == nullptr){
      d->Name = "";
   }
   if (d->Flock == nullptr){
      d->Flock = "";
   }

   str = Marshal::StringToHGlobalAnsi(d->Name);
   strncpy(weighingConfiguration->Name, (char*)str.ToPointer(),WEIGHING_CONFIGURATION_NAME_SIZE);
   Marshal::FreeHGlobal(str);

   str = Marshal::StringToHGlobalAnsi(d->Flock);
   strncpy(weighingConfiguration->Flock, (char*)str.ToPointer(),WEIGHING_CONFIGURATION_FLOCK_SIZE);
   Marshal::FreeHGlobal(str);

	weighingConfiguration->MenuMask = 0x1FFF - (unsigned int)d->MenuMask;
   weighingConfiguration->InitialDay = d->InitialDay;
   weighingConfiguration->CorrectionCurve = d->CorrectionCurveIndex;
   weighingConfiguration->Predefined = d->PredefinedIndex;

   weighingConfiguration->Male.InitialWeight = WeightGauge(d->MaleInitialWeight);
   weighingConfiguration->Male.GrowthCurve = d->MaleGrowthCurveIndex;
   weighingConfiguration->Female.InitialWeight = WeightGauge(d->FemaleInitialWeight);
   weighingConfiguration->Female.GrowthCurve = d->FemaleGrowthCurveIndex;

   weighingConfiguration->TargetWeights.Mode = d->Mode;
   weighingConfiguration->TargetWeights.Growth = (byte)d->Growth;
   weighingConfiguration->TargetWeights.SexDifferentiation = (byte)d->SexDifferentiation;
   weighingConfiguration->TargetWeights.Sex = (byte)d->Sex;
   weighingConfiguration->TargetWeights.AdjustTargetWeights = (byte)d->AdjustTargetWeights;

   weighingConfiguration->Detection.Filter = d->Filter;
   weighingConfiguration->Detection.StabilizationRange = d->StabilizationRange;
   weighingConfiguration->Detection.StabilizationTime = d->StabilizationTime;
   weighingConfiguration->Detection.Step = (byte)d->Step;

   weighingConfiguration->Acceptance.Male.MarginAbove = d->MaleMarginAbove;
   weighingConfiguration->Acceptance.Male.MarginBelow = d->MaleMarginBelow;
   weighingConfiguration->Acceptance.Female.MarginAbove = d->FemaleMarginAbove;
   weighingConfiguration->Acceptance.Female.MarginBelow = d->FemaleMarginBelow;

   weighingConfiguration->Statistic.UniformityRange = d->UniformityRange;
   weighingConfiguration->Statistic.ShortPeriod = d->ShortPeriod;
   weighingConfiguration->Statistic.ShortType = (byte)d->ShortType;
   weighingConfiguration->Statistic.Histogram.Mode = (byte)d->HistogramMode;
   weighingConfiguration->Statistic.Histogram.Range = d->HistogramRange;
   weighingConfiguration->Statistic.Histogram.Step = d->HistogramStep*10;

   dt.Time.Hour = d->DayStart.Hour;
   dt.Time.Min = d->DayStart.Minute;
   dt.Time.Sec = d->DayStart.Second;
   weighingConfiguration->DayStart = uTimeGauge(&dt.Time);

   weighingConfiguration->Planning = d->Planning ? YES : NO;
   weighingConfiguration->WeighingPlan = d->WeighingPlanIndex;
}