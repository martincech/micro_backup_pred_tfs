#include "Bat2NewINativeSocket.h"

#include "Memory\FileRemote.h"
#include "Action\ActionRemote.h"
#include "Memory\Nvm.h"

// remote load/save methods
#include "Config\Config.h"
#include "Config\Context.h"
#include "Archive\Archive.h"
#include "Message\ContactList.h"
#include "Curve\CurveList.h"
#include "Curve\CorrectionList.h"
#include "Scheduler\WeighingPlanList.h"
#include "Predefined\PredefinedList.h"
#include "Action\ActionRemote.h"

using namespace Bat2Library::Connection::Manager::Native;

void Bat2NewINativeSocket::ExceptionCheck(array<byte> ^Buffer)
{
   if (Buffer == nullptr)
   {
      throw gcnew ArgumentNullException("Buffer", "Buffer have to be non-null.");
   }
   if (NativeSocket == NULL)
   {
      throw gcnew System::IO::IOException("Internal communication socket not available!");
   }
}
Bat2Library::SocketStateE Bat2NewINativeSocket::State(void)
{ 
   return (Bat2Library::SocketStateE)NativeSocket->State(); 
}

bool Bat2NewINativeSocket::Receive(array<byte> ^%Buffer)
{
   ExceptionCheck(Buffer);
   pin_ptr<byte> buf = &Buffer[0];
   return NativeSocket->Receive(buf, Buffer->Length) == YES;
}

int Bat2NewINativeSocket::ReceiveSize(void) 
{ 
   return NativeSocket->ReceiveSize(); 
}

bool Bat2NewINativeSocket::Send(array<byte> ^Buffer)
{
   ExceptionCheck(Buffer);
   pin_ptr<byte> buf = &Buffer[0];
   return NativeSocket->Send(buf, Buffer->Length) == YES;
}

void Bat2NewINativeSocket::Close(void)
{ 
   NativeSocket->Close(); 
}

Bat2Library::FileModeE Bat2NewINativeSocket::Permission(void)
{ 
   return (Bat2Library::FileModeE)NativeSocket->Permission(); 
}

bool Bat2NewINativeSocket::SetupConnection()
{
   if (!Bat2INativeSocket::SetupConnection()){
      return false;
   }
   FileRemoteSetup(NativeSocket);
   ActionRemoteSetup(NativeSocket);
   NvmFormat();
   ConfigLoad();
   ContextLoad();
   return true;
}

bool Bat2NewINativeSocket::FreeConnection()
{
   if (!Bat2INativeSocket::FreeConnection()){
      return false;
   }

   FileRemoteSetup(NULL);
   ActionRemoteSetup(NULL);
   return true;
}

TYesNo Bat2NewINativeSocket::ConfigRemoteLoad() {
   return ::ConfigRemoteLoad();
}
TYesNo Bat2NewINativeSocket::ConfigRemoteSave() {
   return ::ConfigRemoteSave();
}

TYesNo Bat2NewINativeSocket::ArchiveRemoteLoad() {
   return ::ArchiveRemoteLoad();
}

TYesNo Bat2NewINativeSocket::ContactListRemoteLoad(){
   return ::ContactListRemoteLoad();
}
TYesNo Bat2NewINativeSocket::ContactListRemoteSave(){
   return ::ContactListRemoteSave();
}

TYesNo Bat2NewINativeSocket::ContextRemoteLoad(){
   return ::ContextRemoteLoad();
}

TYesNo Bat2NewINativeSocket::CurveListRemoteLoad(){
   return ::CurveListRemoteLoad();
}
TYesNo Bat2NewINativeSocket::CurveListRemoteSave(){
   return ::CurveListRemoteSave();
}

TYesNo Bat2NewINativeSocket::CorrectionListRemoteLoad(){
   return ::CorrectionListRemoteLoad();
}
TYesNo Bat2NewINativeSocket::CorrectionListRemoteSave(){
   return ::CorrectionListRemoteSave();
}

TYesNo Bat2NewINativeSocket::WeighingPlanListRemoteLoad(){
   return ::WeighingPlanListRemoteLoad();
}
TYesNo Bat2NewINativeSocket::WeighingPlanListRemoteSave(){
   return ::WeighingPlanListRemoteSave();
}

TYesNo Bat2NewINativeSocket::PredefinedListRemoteLoad(){
   return ::PredefinedListRemoteLoad();
}
TYesNo Bat2NewINativeSocket::PredefinedListRemoteSave(){
   return ::PredefinedListRemoteSave();
}

TYesNo Bat2NewINativeSocket::ActionRemoteWeighingSchedulerStart(void) 
{
   return ::ActionRemoteWeighingSchedulerStart();
}
TYesNo Bat2NewINativeSocket::ActionRemoteWeighingSchedulerStartAt(UClockGauge DateTime) 
{
   return ::ActionRemoteWeighingSchedulerStartAt(DateTime);
}
TYesNo Bat2NewINativeSocket::ActionRemoteWeighingSchedulerStop(void) 
{
   return ::ActionRemoteWeighingSchedulerStop();
}
TYesNo Bat2NewINativeSocket::ActionRemoteWeighingSchedulerSuspend(void) 
{
   return ::ActionRemoteWeighingSchedulerSuspend();
}
TYesNo Bat2NewINativeSocket::ActionRemoteWeighingSchedulerRelease(void) 
{
   return ::ActionRemoteWeighingSchedulerRelease();
}
TYesNo Bat2NewINativeSocket::ActionRemoteTimeSet(UDateTimeGauge DateTime) 
{
   return ::ActionRemoteTimeSet(DateTime);
}
TYesNo Bat2NewINativeSocket::ActionRemoteTimeGet(UDateTimeGauge *DateTime) 
{
   return ::ActionRemoteTimeGet(DateTime);
}
TYesNo Bat2NewINativeSocket::ActionRemoteReboot() 
{
   return ::ActionRemoteReboot();
}