#pragma once
#include "Bat2DataRW.h"
#include "Bat2NewDevice.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace Bat2Library::Connection::Interface::IO;
            using namespace Bat2Library::Connection::Interface::Domain;
            using namespace System::Collections::Generic;
            using namespace System::Reflection;
            using namespace Bat2Library;
            using namespace System::Runtime::InteropServices;
            
            private ref class Bat2NewDataRW :
               Bat2DataRW<VersionInfo ^, Configuration ^, Bat2NewINativeSocket ^, Bat2DeviceData ^>,
               IBat2NewDataReader,
               IBat2NewDataWriter
            {
            public:
               Bat2NewDataRW(Bat2NewDevice ^device) : Bat2DataRW(device){};

					//Load default values
					virtual Bat2DeviceData ^LoadDefault() sealed;

               //Load methods
               virtual Configuration ^LoadConfig() override sealed{ return LoadConfig(true); }
               virtual Context ^LoadContext() sealed{ return LoadContext(true); }
               virtual IEnumerable<ArchiveItem^> ^LoadArchive() sealed{ return LoadArchive(true); }
               virtual IEnumerable<Curve^> ^LoadGrowthCurves() sealed{ return LoadGrowthCurves(true); }
               virtual IEnumerable<Curve^> ^LoadCorrectionCurves() sealed{ return LoadCorrectionCurves(true); }
               virtual IEnumerable<WeighingPlan ^> ^LoadWeighingPlans() sealed{ return LoadWeighingPlans(true); }
               virtual IEnumerable<Contact ^> ^LoadContactList() sealed{ return LoadContactList(true); }
               virtual IEnumerable<WeighingConfiguration ^> ^LoadPredefinedWeighings() sealed{ return LoadPredefinedWeighings(true); }

               // Write methods
               virtual bool SaveConfig(Configuration ^config) override sealed;
               virtual bool SaveGrowthCurves(IEnumerable<Curve^> ^growthCurves) sealed;
               virtual bool SaveCorrectionCurves(IEnumerable<Curve^> ^correctionCurves) sealed;
               virtual bool SaveContactList(IEnumerable<Contact^> ^contacts) sealed;
               virtual bool SaveWeighingPlans(IEnumerable<WeighingPlan^>^ weighingPlans) sealed;
               virtual bool SavePredefinedWeighings(IEnumerable<WeighingConfiguration^>^ predefinedWeighings) sealed;


            internal:
               virtual Bat2DeviceData ^LoadAll() sealed;
               virtual bool WriteAll(Bat2DeviceData ^)sealed;
			      static bool fromNative(Configuration ^%config);
			      static void toNative(Configuration ^config);
			      static bool toNativeGeneric(Object ^o);
			      static bool fromNativeGeneric(Object ^%o);

					
					
            private:               	
               // Actual load functions
               bool Load(Configuration ^%config);
               bool Load(Context ^%context);
               bool Load(List<ArchiveItem^> ^%archive);
               bool Load(List<WeighingPlan^> ^%plans);
               bool LoadGrowth(List<Curve^> ^%curves);
               bool LoadCorrection(List<Curve^> ^%curves);
               bool Load(List<Contact^> ^%contacts);
               bool Load(List<WeighingConfiguration^> ^%predefined);

               // Actual save functions
               bool Save(Configuration ^config);
               bool Save(IEnumerable<WeighingPlan^> ^plans);
               bool SaveGrowth(IEnumerable<Curve^> ^curves);
               bool SaveCorrection(IEnumerable<Curve^> ^curves);
               bool Save(IEnumerable<Contact^> ^contacts);
               bool Save(IEnumerable<WeighingConfiguration^> ^predefinedWeighings);

               virtual Configuration ^LoadConfig(bool withSetup) sealed;
               virtual Context ^LoadContext(bool withSetup) sealed;
               virtual IEnumerable<ArchiveItem^> ^LoadArchive(bool withSetup) sealed;
               virtual IEnumerable<Curve^> ^LoadGrowthCurves(bool withSetup) sealed;
               virtual IEnumerable<Curve^> ^LoadCorrectionCurves(bool withSetup) sealed;
               virtual IEnumerable<WeighingPlan ^> ^LoadWeighingPlans(bool withSetup) sealed;
               virtual IEnumerable<Contact ^> ^LoadContactList(bool withSetup) sealed;
               virtual IEnumerable<WeighingConfiguration ^> ^LoadPredefinedWeighings(bool withSetup) sealed;

					static bool Bat2NewDataRW::LoadList(List<Contact^> ^%contacts);
					static bool Bat2NewDataRW::LoadGrowthList(List<Curve^> ^%curves);
					static bool Bat2NewDataRW::LoadCorrectionList(List<Curve^> ^%curves);
					static bool Bat2NewDataRW::LoadList(List<WeighingPlan^> ^%plans);
					static bool Bat2NewDataRW::LoadList(List<WeighingConfiguration^> ^%predefined);

            };
         }
      }
   }
}

