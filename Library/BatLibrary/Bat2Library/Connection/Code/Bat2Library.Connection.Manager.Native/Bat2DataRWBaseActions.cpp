#include "Bat2DataRW.h"
#undef _MANAGED
#include "Time\uDateTime.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::Domain;
using namespace System::Runtime::InteropServices;
using namespace Bat2Library;
#include <assert.h>


static UDateTime ConvertToDt(Object ^param)
{
   UDateTime dt;

   if (param == nullptr){
      throw gcnew ArgumentNullException("param", "Start at time can't be null!");
   }
   if (param->GetType() != DateTime::typeid){
      throw gcnew ArgumentException("Invalid time specified!");
   }
   DateTime ^d = (DateTime)param;
   dt.Date.Year = uYearSet(d->Year);
   dt.Date.Month = d->Month;
   dt.Date.Day = d->Day;
   dt.Time.Hour = d->Hour;
   dt.Time.Min = d->Minute;
   dt.Time.Sec = d->Second;
   return dt;
}

generic<typename R, typename S, typename U, typename V>
bool Bat2DataRW<R, S, U, V>::Action(ActionCmdE action, Object ^param, [Out] Object^%outValue)
{
   outValue = nullptr;
   bool actionExecuted = false;

   switch (action)
   {
      case ActionCmdE::ACTION_WEIGHING_SCHEDULER_START:
         actionExecuted = device->Socket->ActionRemoteWeighingSchedulerStart() == YES;
         break;

      case ActionCmdE::ACTION_WEIGHING_SCHEDULER_START_AT:
      {
         UDateTime dt = ConvertToDt(param);
         actionExecuted = device->Socket->ActionRemoteWeighingSchedulerStartAt(uClockGauge(&dt)) == YES;
      } break;

      case ActionCmdE::ACTION_WEIGHING_SCHEDULER_STOP:
         actionExecuted = device->Socket->ActionRemoteWeighingSchedulerStop() == YES;
         break;

      case ActionCmdE::ACTION_WEIGHING_SCHEDULER_SUSPEND:
         actionExecuted = device->Socket->ActionRemoteWeighingSchedulerSuspend() == YES;
         break;

      case ActionCmdE::ACTION_WEIGHING_SCHEDULER_RELEASE:
         actionExecuted = device->Socket->ActionRemoteWeighingSchedulerRelease() == YES;
         break;

      case ActionCmdE::ACTION_TIME_SET:
      {
         UDateTime dt = ConvertToDt(param);
         actionExecuted = device->Socket->ActionRemoteTimeSet(uClockDateTime(uClockGauge(&dt))) == YES;
      } break;

      case ActionCmdE::ACTION_TIME_GET:
      {
         UDateTimeGauge dtg;
         UDateTime dt;
         if (device->Socket->ActionRemoteTimeGet(&dtg) == NO){
            actionExecuted = false;
            break;
         }
         uDateTime(&dt, dtg);
         outValue = DateTime(uYearGet(dt.Date.Year), dt.Date.Month, dt.Date.Day, dt.Time.Hour, dt.Time.Min, dt.Time.Sec);
         actionExecuted = true;
      } break;

      default:
         return false;
   }
   return actionExecuted;
}