#pragma once
#include "Bat2NewDeviceManager.h"
#include "UsbBat2Device.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;
            using namespace System::Collections::Generic;
            using namespace System::Collections::ObjectModel;
            using namespace Bat2Library::Connection::Interface::IO;
            using namespace Usb;


            /// <summary>
            /// Manager for usb connected devices
            /// </summary>
            public ref class UsbBat2NewDeviceManager : Bat2NewDeviceManager
            {
            public:
               static property UsbBat2NewDeviceManager ^Instance{
                  UsbBat2NewDeviceManager ^get(){
                     Lock l(UsbBat2NewDeviceManager::typeid);
                     if (_instance == nullptr){
                        _instance = gcnew UsbBat2NewDeviceManager();
                     }
                     return _instance;
                  }
               }

            protected:
               virtual Bat2NewDataRW ^CreateDataRwObject(HidDevice ^forDevice)
               {
                  UsbBat2Device ^b2d = CreateUsbBat2Device(forDevice);
                  return CreateDataRwObject(b2d);
               }

               static UsbBat2Device ^CreateUsbBat2Device(HidDevice ^forDevice)
               {
                  Bat2UsbSocket ^socket = gcnew Bat2UsbSocket(forDevice->Open());
#ifdef _DEBUG
                  if (TimeConstant.HasValue){
                     socket->TimeConstant = TimeConstant.Value;
                  }
#endif
                  return gcnew UsbBat2Device(socket);
               }

            private:
               UsbBat2NewDeviceManager();
               static UsbBat2NewDeviceManager ^_instance = nullptr;
               Dictionary<HidDevice ^, Bat2NewDevice ^> ^existingDevices;

               UsbDeviceManager ^usbWatcher;
               void OnDeviceConnected(System::Object ^sender, UsbDevice ^e);
               void OnDeviceDisconnected(System::Object ^sender, UsbDevice ^e);

#ifdef _DEBUG
            internal:
               IBat2NewDataReader ^GetDataReader(HidDevice ^forDevice)
               {
                  return CreateDataRwObject(forDevice);
               }

               IBat2NewDataWriter ^GetDataWriter(HidDevice ^forDevice)
               {
                  return CreateDataRwObject(forDevice);
               }

               IBat2ActionCommander ^GetActionCommander(HidDevice ^forDevice)
               {
                  return CreateDataRwObject(forDevice);
               }

               static void SetTimeConstant(int constant)
               {
                  TimeConstant = constant;
               }

            private:
               static Nullable<int> TimeConstant;
#endif        
            };


         }
      }
   }
}