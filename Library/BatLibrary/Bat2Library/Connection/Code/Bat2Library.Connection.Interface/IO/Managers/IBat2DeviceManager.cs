﻿using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Connection.Interface.IO
{

   public interface IBat2OldDeviceManager :
      IBat2DeviceManager<
         BaseVersionInfo,
         OldConfiguration,
         OldBat2DeviceData,
         IBat2OldDataReader,
         IBat2OldDataWriter,
         IBat2ActionCommander>
   {
   }
   public interface IBat2NewDeviceManager :
      IBat2DeviceManager<
         VersionInfo,
         Configuration,
         Bat2DeviceData, 
         IBat2NewDataReader, 
         IBat2NewDataWriter, 
         IBat2ActionCommander>
   {      
   }

   public interface IBat2DeviceManager<S, T, D, out R, out W, out C> :
      IDeviceConnectionManager<D>
      where S : BaseVersionInfo
      where T : BaseConfiguration<S>
      where D : BaseBat2DeviceData<S, T>
      where R : IBat2DataReader<S, T>
      where W : IBat2DataWriter<S, T>
      where C : IBat2ActionCommander 
   {
      /// <summary>
      /// Gets adequate datareader object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get data reader for</param>
      /// <returns>data reader object</returns>
      R GetDataReader(D forDevice);

      /// <summary>
      /// Gets adequate datawriter object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get data writer for</param>
      /// <returns>data writer object</returns>
      W GetDataWriter(D forDevice);

      /// <summary>
      /// Gets apropriate action writer object for selected Bat device (specified by serialNumber)
      /// </summary>
      /// <param name="forDevice">device to get action commander for</param>
      /// <returns>action writer object</returns>
      C GetActionCommander(D forDevice);
   }
}
