﻿using System;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Connection.Interface.IO
{
   public interface IBat2DataWriter<S, in T> : IDisposable
      where S : BaseVersionInfo
      where T : BaseConfiguration<S>
   {
      /// <summary>
      /// Save configuration to device
      /// </summary>
      /// <param name="config">Data description object of data to be writen, <see cref="BaseConfiguration{S}"/></param>
      /// <returns>true/false whether save was succesfull or not</returns>
      bool SaveConfig(T config);
   }
}
