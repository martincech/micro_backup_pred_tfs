﻿using System;

namespace Bat2Library.Connection.Interface.IO
{
   public interface IBat2ActionCommander : IDisposable
   {
      /// <summary>
      /// Start weighing right now
      /// </summary>
      /// <returns>true/false on operation validly/invalidly executed</returns>
      bool WeighingStart();
      /// <summary>
      /// Start weighing at specific time
      /// </summary>
      /// <param name="at"><see cref="DateTime"/> to specify time when to start</param>
      /// <returns>true/false on operation validly/invalidly executed</returns>
      bool WeighingStart(DateTime at);
      /// <summary>
      /// Stop currently running weighing
      /// </summary>
      /// <returns>true/false on operation validly/invalidly executed</returns>
      bool WeighingStop();
      /// <summary>
      /// Pause currently running weighing
      /// </summary>
      /// <returns>true/false on operation validly/invalidly executed</returns>
      bool WeighingSuspend();
      /// <summary>
      /// Release previously paused weighing
      /// </summary>
      /// <returns>true/false on operation validly/invalidly executed</returns>
      bool WeighingRelease();

      /// <summary>
      /// Set system date time on device
      /// </summary>
      /// <param name="time">time to be set as current time in device</param>
      /// <returns>true/false on operation validly/invalidly executed</returns>
      bool TimeSet(DateTime time);

      /// <summary>
      /// Return current time in device
      /// </summary>
      /// <returns><see cref="DateTime"/> which is set in device</returns>
      bool TimeGet(out DateTime? dt);

      //bool Reboot();
   };
}
