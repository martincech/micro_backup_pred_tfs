using System;
using System.Runtime.Serialization;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Version of device
   /// </summary>
   [DataContract]
   public class VersionInfo : BaseVersionInfo
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="VersionInfo"/> class.
      /// </summary>
      public VersionInfo()
      {
         IsValid = false;
      }

      /// <summary>
      /// HW build number
      /// </summary>
      [DataMember(IsRequired = true)]
      public byte HardwareBuild { get; set; }

       /// <summary>
      /// HW minor number
      /// </summary>
      [DataMember(IsRequired = true)]
      public byte HardwareMinor { get; set; }

       /// <summary>
      /// HW major number
      /// </summary>
      [DataMember(IsRequired = true)]
      public byte HardwareMajor { get; set; }

       /// <summary>
      /// Specific class of device
      /// </summary>
      [DataMember(IsRequired = true)]
      public short Class { get; set; }
   }
}