﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
    /// <summary>
    /// SmsGate protocol options
    /// </summary>
    [DataContract]
    public class Ethernet
    {
        #region Private fields

        #endregion

        [DataMember]
        public bool Dhcp { get; set; }

        [DataMember]
        public IPAddress Ip { get; set; }

        [DataMember]
        public IPAddress SubnetMask { get; set; }

        [DataMember]
        public IPAddress Gateway { get; set; }

        [DataMember]
        public IPAddress PrimaryDns { get; set; }
    }
}