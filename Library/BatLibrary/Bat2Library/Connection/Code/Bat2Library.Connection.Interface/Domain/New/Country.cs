﻿using System.Runtime.Serialization;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Language and country settings
   /// </summary>
   [DataContract]
   public class Country
   {
      #region Private fields

       #endregion

       /// <summary>
       /// Language to be used
       /// </summary>
       [DataMember]
       public LanguageE Language { get; set; }

      /// <summary>
      /// Code of country
      /// </summary>
      [DataMember]
      public CountryE CountryCode { get; set; }

       /// <summary>
      /// Code page of texts
      /// </summary>
      [DataMember]
      public CodePageE CodePage { get; set; }

       /// <summary>
      /// Format of date
      /// </summary>
      [DataMember]
      public DateFormatE DateFormat { get; set; }

       /// <summary>
      /// Separator between days and months
      /// </summary>
      [DataMember]
      public char DateSeparator1 { get; set; }

       /// <summary>
      /// Separator between months and years
      /// </summary>
      [DataMember]
      public char DateSeparator2 { get; set; }

       /// <summary>
      /// Format of time
      /// </summary>
      [DataMember]
      public TimeFormatE TimeFormat { get; set; }

       /// <summary>
      /// Separator betwenn hours, mins, secs
      /// </summary>
      [DataMember]
      public char TimeSeparator { get; set; }

       /// <summary>
      /// Type of dayling savings (winter/summer time switching)
      /// </summary>
      [DataMember]
      public DaylightSavingE DaylightSavingType { get; set; }
   }
}