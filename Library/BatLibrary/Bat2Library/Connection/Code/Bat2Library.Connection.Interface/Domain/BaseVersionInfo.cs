﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    [DataContract]
    public class BaseVersionInfo
    {
        private const int SERIAL_NUMBER_LENGTH = 10;

        #region Private fields

        private byte[] serialNumber;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionInfo"/> class.
        /// </summary>
        public BaseVersionInfo()
        {
            IsValid = false;
            serialNumber = new byte[SERIAL_NUMBER_LENGTH];
        }

        /// <summary>
        /// SW build number
        /// </summary>
        [DataMember(IsRequired = true)]
        public byte SoftwareBuild { get; set; }

        /// <summary>
        /// SW minor number
        /// </summary>
        [DataMember(IsRequired = true)]
        public byte SoftwareMinor { get; set; }

        /// <summary>
        /// SW major number
        /// </summary>
        [DataMember(IsRequired = true)]
        public byte SoftwareMajor { get; set; }

        /// <summary>
        /// Modification of device
        /// </summary>
        [DataMember(IsRequired = true)]
        public short Modification { get; set; }


        /// <summary>
        /// Serial number of device as number
        /// </summary>
        [DataMember(IsRequired = true)]
        public UInt32 SerialNumber
        {
            get { return BitConverter.ToUInt32(serialNumber.Take(sizeof (UInt32)).ToArray(), 0); }
            set
            {
                serialNumber = new byte[SERIAL_NUMBER_LENGTH];
                Array.Copy(BitConverter.GetBytes(value), serialNumber, sizeof (UInt32));
                IsValid = true;
            }
        }

        /// <summary>
        /// Serial number of device as number
        /// </summary>
        [DataMember(IsRequired = true)]
        public byte[] SerialNumberArray
        {
            get { return serialNumber; }
            set
            {
                serialNumber = new byte[SERIAL_NUMBER_LENGTH];
                if (value == null) return;
                Array.Copy(value, serialNumber, value.Length > serialNumber.Length ? SERIAL_NUMBER_LENGTH : value.Length);
            }
        }


        /// <summary>
        /// Whether this info is valid
        /// </summary>
        public bool IsValid { get; internal set; }
    }
}
