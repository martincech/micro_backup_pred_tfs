﻿namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldCalibration
    {
        // Kalibrace nuly
        public int ZeroCalibration { get; set; }

        // Kalibrace rozsahu
        public int RangeCalibration { get; set; }

        // Rozsah 0..NOSNOST_NASLAPNYCH_VAH   
        public int Range { get; set; }

        // Dilek vah                  
        public byte Division { get; set; }                          
    }
}