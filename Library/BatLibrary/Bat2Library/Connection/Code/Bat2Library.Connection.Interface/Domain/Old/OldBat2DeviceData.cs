﻿using System.Collections.Generic;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldBat2DeviceData : BaseBat2DeviceData<BaseVersionInfo, OldConfiguration>
    {
        public OldBat2DeviceData()
        {
           Configuration = new OldConfiguration();
        }

        public IEnumerable<OldArchiveDailyInfo> Archive { get; set; }
    }
}