﻿using System;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldWeightCorrection
    {
        // Den prvniho zlomu, do tohoto dne je korekce nulova
        public UInt16 Day1 { get; set; }

        // Den druheho zlomu, v tomto dni a dale se uplatni zadana korekce <Correction>       
        public UInt16 Day2 { get; set; }

        // Korekce v desetinach procenta (max. 25.5%) v den Day2. Pokud je hodnota 0, korekce se neuplatnuje.
        public byte Correction { get; set; }          
    }
}