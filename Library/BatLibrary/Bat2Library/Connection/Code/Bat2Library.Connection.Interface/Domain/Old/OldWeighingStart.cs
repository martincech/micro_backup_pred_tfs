﻿using System;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    // Parametry, ktere zadal pri zahajeni vykrmu. Tyto parametry se v celem prubehu vazeni nemeni.
    // Po skonceni vykrmu se zde drzi vsechny parametry posledniho vykrmu az do doby odstartovani dalsiho vykrmu. Parametry odtut muze tedy vyuzivat
    // historie i po ukonceni vykrmu.
    public class OldWeighingStart
    {
        // Flag, ze prave probiha krmeni
        public byte Running { get; set; }

        // Flag, ze se prave ceka na opozdeny start krmeni. Ostatni polozky TWeighingStart jsou vyplneny, staci inicializovat archiv atd.
        public byte WaitingForStart { get; set; }

        // Flag, zda zvolil krmeni podle urciteho hejna. Pokud ne, zadal zakladni parametry primo a vazi se bez pouziti hejna, za pouziti parametru v TRychlyVykrm.
        public byte UseFlock { get; set; }

        // Pokud se krmi podle hejna, je zde cislo hejna, podle ktereho se prave krmi. Pri ukonceni vykrmu zde zustane cislo hejna, podle ktereho se krmilo.
        public byte CurrentFlock { get; set; }

        // V kolikatem dnu vykrm zacal minus 1, tj. o kolik se ma krivka posunout. Standardne je to 0.
        public UInt16 CurveDayShift { get; set; }

        // Datum zahajeni vykrmu
        public DateTime DateTime { get; set; }

        // Zde jsou parametry vykrmu pri PouzivatHejno=NO
        public OldQuickWeighing QuickWeighing { get; set; } 
     
        // Rozsah uniformity v +- %
        public byte UniformityRange { get; set; }

        // Flag, ze probiha online mereni
        public byte Online { get; set; }             
    }
}
