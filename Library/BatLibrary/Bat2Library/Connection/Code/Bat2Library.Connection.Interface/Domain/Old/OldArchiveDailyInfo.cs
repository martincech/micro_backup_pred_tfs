﻿using System.Collections.Generic;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldArchiveDailyInfo : BaseArchiveItem
    {
        public OldArchive Archive { get; set; }
        public IEnumerable<OldLoggerSample> Samples { get; set; }                             
    }
}