﻿using System;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    // Parametry RS-485
    public class OldRs485
    {
        // Adresa vahy v siti RS-485
        public byte Address { get; set; }

        // Rychlost komunikace v Baudech za sekundu
        public UInt32 Speed { get; set; }

        // Typ parity
        public TParity Parity { get; set; }

        // Zpozdeni mezi prijmem prikazu a odpovedi v milisekundach, kvantovani s TIMER0_PERIOD
        public UInt16 ReplyDelay { get; set; }

        // Mezera mezi dvema pakety MODBUS v milisekundach
        public byte SilentInterval { get; set; }

        // Typ komunikacniho protokolu
        public TProtocol Protocol { get; set; }       
    }   // 10 bajtu

    // Parita pri komunikaci RS-485, POZOR, musi odpovidat parite definovane v MODBUS (alespon prvni mody)
    public enum TParity : byte
    {
        RS485_PARITY_NONE,        // Zadna parita (paritni bit se nevysila)
        RS485_PARITY_EVEN,        // Suda parita
        RS485_PARITY_ODD,         // Licha parita
        RS485_PARITY_MARK,        // Vysilaji se 2 stop bity
        _RS485_PARITY_COUNT
    }

    // Komunikacni protokol
    public enum TProtocol : byte
    {
        RS485_PROTOCOL_MODBUS_RTU,
        RS485_PROTOCOL_MODBUS_ASCII,
        _RS485_PROTOCOL_COUNT
    }
}
