﻿using System;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldArchiveItem
    {
        // statistika 
        public OldStatistic Stat { get; set; }

        // histogram 
        public OldHistogram Hist { get; set; }

        // Prumerna vcerejsi hmotnost (pro vypocet daily gain)
        public UInt16 LastAverage { get; set; }

        // Normovana hmotnost pro tento den
        public UInt16 TargetWeight { get; set; }

        // Uniformita vypoctena presne z jednotlivych vzorku
        public byte RealUniformity { get; set; }
    }
}
