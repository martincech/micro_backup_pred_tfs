﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Configuration for GSM message module
   /// </summary>
   [DataContract]
   public class GsmMessage
   {
      /// <summary>
      /// Start sending from day
      /// </summary>
      [DataMember]
      public short StartFromDay { get; set; }
      /// <summary>
      /// Sending period
      /// </summary>
      [DataMember]
      public short Period { get; set; }
      /// <summary>
      /// Accelerated period start from day
      /// </summary>
      [DataMember]
      public short AccelerateFromDay { get; set; }
      /// <summary>
      /// Accelerated period
      /// </summary>
      [DataMember]
      public short AcceleratedPeriod { get; set; }
      /// <summary>
      /// Time to send at
      /// </summary>
      [DataMember]
      public DateTime SendAt { get; set; }
      /// <summary>
      /// Enable commands input
      /// </summary>
      [DataMember]
      public bool CommandsEnabled { get; set; }
      /// <summary>
      /// Check allowed phone number on commands
      /// </summary>
      [DataMember]
      public bool CommandsCheckPhoneNumber { get; set; }
      /// <summary>
      /// Check command expiration
      /// </summary>
      [DataMember]
      public short CommandsExpiration { get; set; }
      /// <summary>
      /// Mask for events to be send
      /// </summary>
      [DataMember]
      public UInt32 EventMask { get; set; }
      /// <summary>
      /// Enable remote control through sms
      /// </summary>
      [DataMember]
      public bool RemoteControlEnabled { get; set; }
      /// <summary>
      /// Check allowed phone numbers on remote control
      /// </summary>
      [DataMember]
      public bool RemoteControlCheckPhoneNumber { get; set; }
      /// <summary>
      /// Mode of operation
      /// </summary>
      [DataMember]
      public byte Mode { get; set; }
      /// <summary>
      /// Period during which is module switched on
      /// </summary>
      [DataMember]
      public byte SwitchOnPeriod { get; set; }
      /// <summary>
      /// Duration of switch on 
      /// </summary>
      [DataMember]
      public byte SwitchOnDuration { get; set; }
      /// <summary>
      /// Specific times to switch on
      /// </summary>
      [DataMember]
      public List<TimeRange> SwitchOnTimes { get; set; }
   }
}