﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// 
   /// </summary>
   [DataContract]
   public class Configuration
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public Configuration()
      {
         VersionInfo = new VersionInfo();
         DeviceInfo = new DeviceInfo();
      }

      /// <summary>
      /// Is this configuration valid? Invalid configurations are posible when data are not loaded validly from device
      /// </summary>
      public bool IsValid { get { return VersionInfo.IsValid; } }
      
      /// <summary>
      /// Info about device version, <see cref="VersionInfo"/>
      /// </summary>
      [DataMember(IsRequired = true, Name = "Version", Order = 1)]
      public VersionInfo VersionInfo { get; set; }
      
      /// <summary>
      /// Info about current device, <see cref="DeviceInfo"/>
      /// </summary>
      [DataMember(IsRequired = true, Order = 2)]
      public DeviceInfo DeviceInfo { get; set; }

      /// <summary>
      /// Country and Language settings in device, <see cref="Country"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 3)]
      public Country Country { get; set; }
      
      /// <summary>
      /// Weighing units selection, <see cref="WeightUnits"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 4)]
      public WeightUnits WeightUnits { get; set; }

      /// <summary>
      /// Calibration settings of platform, <see cref="PlatformCalibration"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 5)]
      public PlatformCalibration PlatformCalibration { get; set; }

      /// <summary>
      /// Configuration of display, <see cref="DisplayConfiguration"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 6)]
      public DisplayConfiguration DisplayConfiguration { get; set; }

      /// <summary>
      /// Configuration of current weighing, <see cref="WeighingConfiguration"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 7)]
      public WeighingConfiguration WeighingConfiguration { get; set; }

      /// <summary>
      /// Configuration of RS485 lines, <see cref="Rs485Options"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 8)]
      public List<Rs485Options> Rs485Options { get; set; }

      /// <summary>
      /// Configuration of modbus line, <see cref="ModbusOptions"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 9)]
      public ModbusOptions ModbusOptions { get; set; }

      /// <summary>
      /// Configuration of megavi line, <see cref="MegaviOptions"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 10)]
      public MegaviOptions MegaviOptions { get; set; }

      /// <summary>
      /// Configuration of smsgate line, <see cref="SmsGateOptions"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 11)]
      public SmsGateOptions SmsGateOptions { get; set; }

      /// <summary>
      /// Configuration of dacs line, <see cref="DacsOptions"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 12)]
      public DacsOptions DacsOptions { get; set; }

      /// <summary>
      /// Configuration of ethernet, <see cref="EthernetOptions"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 13)]
      public EthernetOptions EthernetOptions { get; set; }

      /// <summary>
      /// Configuration of GSM module, <see cref="GsmMessage"/>
      /// </summary>
      [DataMember(IsRequired = false, Order = 14)]
      public GsmMessage GsmMessage { get; set; }
   }
}
