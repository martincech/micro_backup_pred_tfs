﻿using System;
using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Time range with times from and to
   /// </summary>
   [DataContract]
   public class TimeRange
   {
      /// <summary>
      /// Begining of range
      /// </summary>
      [DataMember]
      public DateTime From { get; set; }
      /// <summary>
      /// End of range
      /// </summary>
      [DataMember]
      public DateTime To { get; set; }
   }
}