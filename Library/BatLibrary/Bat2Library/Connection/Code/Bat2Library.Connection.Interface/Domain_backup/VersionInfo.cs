using System;
using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Version of device
   /// </summary>
   [DataContract]
   public class VersionInfo
   {
      private uint serialNumber;

      /// <summary>
      /// Initializes a new instance of the <see cref="VersionInfo"/> class.
      /// </summary>
      public VersionInfo()
      {
         IsValid = false;
      }

      /// <summary>
      /// HW build number
      /// </summary>
      [DataMember]
      public byte HardwareBuild { get; set; }
      /// <summary>
      /// HW minor number
      /// </summary>
      [DataMember]
      public byte HardwareMinor { get; set; }
      /// <summary>
      /// HW major number
      /// </summary>
      [DataMember]
      public byte HardwareMajor { get; set; }
      /// <summary>
      /// SW build number
      /// </summary>
      [DataMember]
      public byte SoftwareBuild { get; set; }
      /// <summary>
      /// SW minor number
      /// </summary>
      [DataMember]
      public byte SoftwareMinor { get; set; }
      /// <summary>
      /// SW major number
      /// </summary>
      [DataMember]
      public byte SoftwareMajor { get; set; }
      /// <summary>
      /// Modification of device
      /// </summary>
      [DataMember]
      public short Modification { get; set; }
      /// <summary>
      /// Specific class of device
      /// </summary>
      [DataMember]
      public short Class { get; set; }
      /// <summary>
      /// Serial number of device
      /// </summary>
      [DataMember]
      public UInt32 SerialNumber
      {
         get { return serialNumber; }
         set
         {
            serialNumber = value;
            IsValid = true;
         }
      }

      /// <summary>
      /// Whether this info is valid
      /// </summary>
      public bool IsValid { get; private set; }
   }
}