﻿using System.ServiceModel;
using Services.PublishSubscribe.Contracts;

namespace Bat2Library.Connection.Interface.Contract
{

   [ServiceContract(CallbackContract = typeof(IBat2ConnectionEventsContract))]
   public interface IBat2ConnectionEventsSubscribtionContract :
      ISubscriptionContract
   {      
   }

   [ServiceContract(CallbackContract = typeof(IBat2DataReadEventsContract))]
   public interface IBat2DataReadEventsSubscribtionContract :
      ISubscriptionContract
   {
   }

   [ServiceContract(CallbackContract = typeof(IBat2DataWriteEventsContract))]
   public interface IBat2DataWriteEventsSubscribtionContract :
      ISubscriptionContract
   {
   }

   [ServiceContract(CallbackContract = typeof(IBat2ActionEventsContract))]
   public interface IBat2ActionEventsSubscribtionContract :
      ISubscriptionContract
   {
   }
   /// <summary>
   /// Subscribtion contract for temporary subscribtions of <see cref="IBat2EventsContract"/>
   /// (subscribtion live only as long as service lives)
   /// </summary>
   [ServiceContract(CallbackContract = typeof(IBat2EventsContract))]
   public interface IBat2EventsSubscribtionContract : 
      IBat2ConnectionEventsSubscribtionContract,
      IBat2DataReadEventsSubscribtionContract,
      IBat2DataWriteEventsSubscribtionContract,
      IBat2ActionEventsSubscribtionContract
   {
   }
}
