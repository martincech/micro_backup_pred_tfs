﻿using System;
using System.ServiceModel;
using Bat2Library.Connection.Interface.Domain;

namespace Bat2Library.Connection.Interface.Contract
{
   /// <summary>
   /// This services serves as data command sender to BAT2 devices
   /// </summary>
   [ServiceContract]
   public interface IBat2DataCommandContract
   {
      /// <summary>
      /// Send command to device to read all data from it - same as calling 
      /// all other read methods from this interface separatelly
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void ReadData(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to read just configuration from it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void ReadConfig(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to read just context from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void ReadContext(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to read whole archive from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void ReadArchive(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to read growth curves from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void ReadGrowthCurves(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to read correction curves from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void ReadCorrectionCurves(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to read contact list from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void ReadContactList(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to read weighing plans from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void ReadWeighingPlans(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to read predefined weighing configurations from it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void ReadPredefinedWeighings(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to write data to it - same as calling
      /// all write methods from this interface separatelly
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WriteData(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to write just configuration to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WriteConfig(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to write just growth curves to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WriteGrowthCurves(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to write just correction curves to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WriteCorrectionCurves(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to write just contact list to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WriteContactList(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to write just weighing plans to it.
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WriteWeighingPlans(Bat2DeviceData deviceData);

      /// <summary>
      /// Send command to device to write just predefined weighing configurations to it
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WritePredefinedWeighings(Bat2DeviceData deviceData);

   }

   /// <summary>
   /// This services serves as action command sender to BAT2 devices
   /// </summary>
   [ServiceContract]
   public interface IBat2ActionCommandContract
   {
      /// <summary>
      /// Read time from device
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void TimeGet(Bat2DeviceData deviceData);

      /// <summary>
      /// Write time to device
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      /// <param name="at">time to write to device</param>
      [OperationContract(IsOneWay = true)]
      void TimeSet(Bat2DeviceData deviceData, DateTime at);

      /// <summary>
      /// Start weighing on device
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      /// <param name="at">when has value plan weighing at this time, otherwise start immediatelly</param>
      [OperationContract(IsOneWay = true)]
      void WeighingStart(Bat2DeviceData deviceData, DateTime? at);

      /// <summary>
      /// Stop weighing on device
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WeighingStop(Bat2DeviceData deviceData);

      /// <summary>
      /// Pause weighing on device
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WeighingSuspend(Bat2DeviceData deviceData);

      /// <summary>
      /// Release previously paused weighing on device
      /// </summary>
      /// <param name="deviceData">device descriptor</param>
      [OperationContract(IsOneWay = true)]
      void WeighingRelease(Bat2DeviceData deviceData);
   }

   /// <summary>
   /// This services serves as action to to get information about BAT2 connected devices
   /// </summary>
   [ServiceContract]
   public interface IBat2DeviceCommandContract 
   {
       /// <summary>
       /// Get all connected devices
       /// </summary>
       [OperationContract(IsOneWay = true)]
       void ConnectedDevicesGet();
   }

   /// <summary>
   /// This services serves as Data + Action command sender to BAT2 devices
   /// It is just composition of interfaces above, 
   /// <see cref="IBat2DataCommandContract"/>
   /// <see cref="IBat2ActionCommandContract"/>
   /// <see cref="IBat2DeviceCommandContract"/>
   /// </summary>
   [ServiceContract]
   public interface IBat2CommandContract : IBat2DataCommandContract, IBat2ActionCommandContract, IBat2DeviceCommandContract
   {
   }
}