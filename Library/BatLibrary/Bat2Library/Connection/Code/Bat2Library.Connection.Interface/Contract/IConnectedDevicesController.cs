﻿using Utilities.Modules;

namespace Bat2Library.Connection.Interface
{
   /// <summary>
   /// Controler for connected devices watching
   /// </summary>
   public interface IConnectedDevicesController : IModuleController
   {
   }
}