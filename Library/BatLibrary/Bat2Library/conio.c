#include "Console\conio.h"
#include <stdio.h>

void bprintf(char *Buffer, const char *Format, ...)
// formatovany vystup do <Buffer>
{
   va_list Arg;
   va_start(Arg, Format);
   vsprintf(Buffer, Format, Arg);
   va_end(Arg);
}
