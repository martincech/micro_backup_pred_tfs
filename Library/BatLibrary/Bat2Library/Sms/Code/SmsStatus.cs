﻿using System;
using Usb.MODEM;
using Utilities.Observable;

namespace Bat2Library.Sms
{
   /// <summary>
   /// Current status
   /// </summary>
   public class SmsStatus : ObservableObject
   {
      /// <summary>
      /// COM port number, so I can recognize the modem
      /// </summary>
      //public int PortNumber;
      public string PortName;

      /// <summary>
      /// Modem name
      /// </summary>
      public string ModemName;

      /// <summary>
      /// Current event of the GSM modem
      /// </summary>
      private SmsEvent _event;

      public SmsEvent Event
      {
         get { return _event; }
         set { SetProperty(ref _event, value); }
      }

      /// <summary>
      /// Operator name
      /// </summary>
      public string OperatorName;

      /// <summary>
      /// Signal strength in percents
      /// </summary>
      private int _signalStrength;
      public int SignalStrength
      {
         get { return _signalStrength; }
         set { SetProperty(ref _signalStrength, value); }
      }

      /// <summary>
      /// Phone number of received SMS
      /// </summary>
      public string SmsNumber;

      /// <summary>
      /// Text of received SMS
      /// </summary>
      public string SmsText;

      /// <summary>
      /// Indicates if message was synced
      /// </summary>
      public bool Synced;

      /// <summary>
      /// Masasage date
      /// </summary>
      public string Date;

      private ModemDevice _device;
      public ModemDevice Device { get { return _device; } set { _device = value; } }

      /// <summary>
      /// Constructor
      /// </summary>
      public SmsStatus()
      {
         Init();
      }

      public SmsStatus(ModemDevice device)
      {
         Init();
         _device = device;
      }

      private void Init()
      {
         ModemName = "";
         Event = SmsEvent.INIT;
         OperatorName = "";
         SignalStrength = 0;
         Synced = false;
         Date = String.Format("{0:G}", DateTime.Now);
      }

      /// <summary>
      /// Copy constructor
      /// </summary>
      /// <param name="source">Source</param>
      public SmsStatus(SmsStatus source)
      {
         PortName = source.PortName;
         ModemName = source.ModemName;
         Event = source.Event;
         OperatorName = source.OperatorName;
         SignalStrength = source.SignalStrength;
         SmsNumber = source.SmsNumber;
         SmsText = source.SmsText;
         Synced = source.Synced;
         Date = source.Date;
      }
   }
}
