﻿using System.Collections.Generic;
using Usb.MODEM;

namespace Bat2Library.Sms
{
    public class SmsInitData
    {
       /// <summary>
       /// COM port number
       /// </summary>
       //public string PortNumber;
       public ModemDevice Device;

        /// <summary>
        /// Mode of operation
        /// </summary>
        public SmsOperationMode Mode;

        /// <summary>
        /// List of phone numbers for sending
        /// </summary>
        public List<string> SendPhoneNumberCollection;

        /// <summary>
        /// SMS text for sending
        /// </summary>
        public string SendSmsText;

        /// <summary>
        /// Phone number for requests
        /// </summary>
        public string RequestPhoneNumber;

        /// <summary>
        /// Day of request or -1 for today
        /// </summary>
        public int RequestDay;
    }
}