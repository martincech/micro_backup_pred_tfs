﻿using System.Collections.Generic;

namespace Bat2Library.Utilities.Interfaces
{
   public interface IParser
   {
      IEnumerable<Report> Parse();
   }
}
