﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bat2Library.Utilities.Interfaces;

namespace Bat2Library.Utilities
{
   public class CsvParser : Parser, IParser
   { 
      public char delimiter = DEFAULT_DELIMITER;
   
      private const char DEFAULT_DELIMITER = ';';
      private const string ERROR_MSG_UNEXPECTED = "Unexcepted file structure";

      #region Constructors

      public CsvParser(byte[] file) : base(file)
      {
      }
     
      public CsvParser(string filename) : base(filename)
      {
      }

      #endregion

      public IEnumerable<Report> Parse()
      {
         try
         {
            Reports = new List<Report>();
            FoundRecords = 0;
            var stream = new MemoryStream(byteArray);
            var reader = new StreamReader(stream);
            ParseCsvDoc(reader);
            if (Reports.Count == 0)
            {
               Reports = null;
            }
            return Reports;
         }
         catch (Exception)
         {
            Reports = null;
            return Reports;
         }
      }

      #region Private helpers

      private void ParseCsvDoc(StreamReader reader)
      {      
         Reports.Clear();
         var sex = SexE.SEX_UNDEFINED;

         while (!reader.EndOfStream)
         {
            var line = reader.ReadLine();
            if (line != null)
            {
               var values = line.Split(delimiter);
               if (values.Count() < HeaderFormat.REPORT_MINIMUM_LENGTH || values.Count() > HeaderFormat.REPORT_MAXIMUM_LENGTH)
               {
                  throw new Exception(ERROR_MSG_UNEXPECTED);
               }

               if (Reports.Count == 0 && IsMaleSex(values))
               {
                  sex = SexE.SEX_MALE;
               }

               DateTime date;
               int day, count;
               double avg, gain, sigma, cv, uni;
               var parsingList = new List<bool>
               {
                  DateTime.TryParse(values[HeaderFormat.DATE_INDEX], out date),
                  int.TryParse(values[HeaderFormat.DAY_INDEX], out day),
                  int.TryParse(values[HeaderFormat.COUNT_INDEX], out count),
                  double.TryParse(values[HeaderFormat.AVERAGE_INDEX], out avg),
                  double.TryParse(values[HeaderFormat.GAIN_INDEX], out gain),
                  double.TryParse(values[HeaderFormat.SIGMA_INDEX], out sigma),
                  double.TryParse(values[HeaderFormat.CV_INDEX], out cv),
                  double.TryParse(values[HeaderFormat.UNIFORMITY_INDEX], out uni)
               };

               var parsingResult = parsingList.Any(i => i == false);
               if (parsingResult)
               {
                  if (CheckIsData(parsingList))
                  {
                     FoundRecords++;
                  }
                  continue;
               }

               AddReport(date, sex, values[HeaderFormat.SCALE_INDEX], day, count, avg, gain, sigma, cv, uni, values.Last());
               FoundRecords++;
            }
         }
      }

      /// <summary>
      /// Check data if contains male's information.
      /// </summary>
      /// <param name="list"></param>
      /// <returns></returns>
      private bool IsMaleSex(string[] list)
      {
         // Data must be only in first 2 columns
         var flag = !(list[0].Contains(" ") || list[1].Contains(" ") || 
                      list[1].Contains("( +") || list[0].Equals("") || list[1].Equals(""));

         if (flag)
         {  //other columns must be empty
            for (var i = 2; i < list.Count(); i++)
            {
               if (!list[i].Equals(""))
               {
                  return false;
               }
            }
         }
         return flag;
      }      

      #endregion
   }
}
