﻿namespace Bat2Library.Utilities
{
   internal class HeaderFormat
   {
      public static int DATE_INDEX = 0;
      public static int SCALE_INDEX = 1;
      public static int DAY_INDEX = 2;
      public static int COUNT_INDEX = 3;
      public static int AVERAGE_INDEX = 4;
      public static int GAIN_INDEX = 5;
      public static int SIGMA_INDEX = 6;
      public static int CV_INDEX = 7;
      public static int UNIFORMITY_INDEX = 8;
      public static int REPORT_MINIMUM_LENGTH = 9;
      public static int REPORT_MAXIMUM_LENGTH = 12;
   }
}
