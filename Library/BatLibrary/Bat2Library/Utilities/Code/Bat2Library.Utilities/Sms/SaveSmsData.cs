﻿using System;
using System.Text;
using Bat2Library.Utilities.Sms.Models;
using Utilities;

namespace Bat2Library.Utilities.Sms
{
    public class SaveSmsData
    {
        public static bool Save(string fileName, SmsData decodedSms)
        {

            Csv csv = new Csv(",", ".", Encoding.Default);
            int column = 0;
            csv.AddDateTime(column++, DateTime.Now);
            csv.AddString(column++, decodedSms.PhoneNumber);
            csv.AddLong(column++, decodedSms.ScaleNumber);
            csv.AddInteger(column++, decodedSms.DayNumber);
            csv.AddDateTime(column++, decodedSms.Date);
            csv.AddInteger(column++, decodedSms.FemaleData.Count);
            csv.AddDouble(column++, double.Parse(decodedSms.FemaleData.Average.ToString("0.000")));
            csv.AddDouble(column++, double.Parse(decodedSms.FemaleData.Gain.ToString("0.000")));
            csv.AddDouble(column++, double.Parse(decodedSms.FemaleData.Sigma.ToString("0.000")));
            csv.AddDouble(column++, double.Parse(decodedSms.FemaleData.Cv.ToString("0.0")));
            csv.AddDouble(column++, double.Parse(decodedSms.FemaleData.Uniformity.ToString("0.0")));
            csv.AddInteger(column++, decodedSms.MaleData == null ? 2 : 1); // undefined or female
            csv.SaveLine();

            if (decodedSms.MaleData != null)
            {
                column = 0;
                csv.AddDateTime(column++, DateTime.Now);
                csv.AddString(column++, decodedSms.PhoneNumber);
                csv.AddLong(column++, decodedSms.ScaleNumber);
                csv.AddInteger(column++, decodedSms.DayNumber);
                csv.AddDateTime(column++, decodedSms.Date);
                csv.AddInteger(column++, decodedSms.MaleData.Count);
                csv.AddDouble(column++, double.Parse(decodedSms.MaleData.Average.ToString("0.000")));
                csv.AddDouble(column++, double.Parse(decodedSms.MaleData.Gain.ToString("0.000")));
                csv.AddDouble(column++, double.Parse(decodedSms.MaleData.Sigma.ToString("0.000")));
                csv.AddDouble(column++, double.Parse(decodedSms.MaleData.Cv.ToString("0.0")));
                csv.AddDouble(column++, double.Parse(decodedSms.MaleData.Uniformity.ToString("0.0")));
                csv.AddInteger(column++, 0); // male
                csv.SaveLine();
            }
            try
            {
                return csv.SaveToFile(fileName, true);      // Append
            }
            catch
            {
                return false;
            }
        }

        public static bool SaveGarbage(string phoneNumber, string text, string fileName)
        {
            Csv csv = new Csv(",", ".", Encoding.Default);
            int column = 0;

            csv.AddDateTime(column++, DateTime.Now);
            csv.AddString(column++, phoneNumber);
            csv.AddString(column++, text);
            csv.SaveLine();

            try
            {
                return csv.SaveToFile(fileName, true);      // Append
            }
            catch
            {
                return false;
            }
        }
    }
}
