﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bat2Library.Utilities.Interfaces;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Bat2Library.Utilities
{
   public class ExcelParser : Parser, IParser
   {     
      private const int ACTIVE_SHEET = 0;   

      #region Constructors

      public ExcelParser(byte[] file) : base(file)
      {
      }

      public ExcelParser(string filename) : base(filename)
      {     
      }

      #endregion

      public IEnumerable<Report> Parse()
      {
         try
         {
            FoundRecords = 0;
            Reports = new List<Report>();
            IWorkbook workbook;
            using (var stream = new MemoryStream(byteArray))
            {
               workbook = InitWorkbook(stream);               
            }
            ParseExcelDoc(workbook);
            if (Reports.Count == 0)
            {
               Reports = null;
            }
            return Reports;
         }
         catch (Exception e)
         {
            Reports = null;
            return Reports;
         }
      }

      #region Private helpers

      private IWorkbook InitWorkbook(Stream stream)
      {       
         try
         {  // new excel format *.xlsx
            return new XSSFWorkbook(stream);
         }
         catch (ICSharpCode.SharpZipLib.Zip.ZipException ex)
         {  // old excel format *.xls
            return new HSSFWorkbook(stream);
         }        
      }
      
      private void ParseExcelDoc(IWorkbook workbook)
      {
         Reports.Clear();      
         var sex = SexE.SEX_UNDEFINED;
         var sheet = workbook.GetSheetAt(ACTIVE_SHEET);      
         
         for (var rowIndex = 0; rowIndex <= sheet.LastRowNum; rowIndex++)
         {
            var actualRow = sheet.GetRow(rowIndex);
            if (actualRow != null) //null is when the row only contains empty cells 
            {
               if (IsMaleSex(actualRow))
               {
                  sex = SexE.SEX_MALE;
               }

               if (actualRow.Cells.Count >= HeaderFormat.REPORT_MINIMUM_LENGTH)
               {
                  var data = new List<string>();
                  for (var i = 0; i < actualRow.Cells.Count; i++)
                  {
                     data.Add(actualRow.GetCell(i).ToString());
                  }

                  var note = "";
                  if (data.Count() == HeaderFormat.REPORT_MINIMUM_LENGTH + 1 ||
                      data.Count() == HeaderFormat.REPORT_MAXIMUM_LENGTH)
                  {  //data contains note
                     note = data.Last();
                  }

                  var date = new DateTime();
                  int day, count;
                  double avg, gain, sigma, cv, uni;
                  var parsingList = new List<bool>
                  {
                     int.TryParse(data[HeaderFormat.DAY_INDEX], out day),
                     int.TryParse(data[HeaderFormat.COUNT_INDEX], out count),
                     double.TryParse(data[HeaderFormat.AVERAGE_INDEX], out avg),
                     double.TryParse(data[HeaderFormat.GAIN_INDEX], out gain),
                     double.TryParse(data[HeaderFormat.SIGMA_INDEX], out sigma),
                     double.TryParse(data[HeaderFormat.CV_INDEX], out cv),
                     double.TryParse(data[HeaderFormat.UNIFORMITY_INDEX], out uni)
                  };

                  try
                  {
                     date = actualRow.GetCell(0).DateCellValue;
                     parsingList.Add(true);
                  }
                  catch
                  {  // value is not DateTime
                     parsingList.Add(false);
                  }
     
                  var parsingResult = parsingList.Any(i => i == false);
                  if (parsingResult)
                  {
                     if (CheckIsData(parsingList))
                     {
                        FoundRecords++;
                     }
                     continue;
                  }

                  AddReport(date, sex, data[HeaderFormat.SCALE_INDEX], day, count, avg, gain, sigma, cv, uni, note);
                  FoundRecords++;
               }
               else if (Reports.Count > 0)
               {  // Bad row, but it is included to data
                  FoundRecords++;
               }
               else if (Reports.Count == 0)
               {  // too few arguments, but check, if row contains data (to update statistic)
                  if (CheckIsData(actualRow))
                  {
                     FoundRecords++;
                  }
               }
            }
         }
      }

      /// <summary>
      /// Check if this row represent data.
      /// </summary>
      /// <param name="row">Tested row</param>
      /// <returns>true - row contains minimum required items</returns>
      private bool CheckIsData(IRow row)
      {
         var count = 0;
         try
         {
            var date = row.GetCell(0).DateCellValue;
            count++;
         }
         catch { } // value is not DateTime

         for (var i = 1; i < row.Cells.Count; i++)
         {
            if (count >= MINIMUM_PARSE_DATA)
            {
               return true;
            }

            double value;
            if (double.TryParse(row.GetCell(i).ToString(), out value))
            {
               count++;
            }
         }

         return false;
      }
     
      private bool IsMaleSex(IRow row)
      {
         return Reports.Count == 0 &&
                row.Cells.Count == 2 &&
                !row.GetCell(0).ToString().Contains(" ") &&
                !row.GetCell(1).ToString().Contains(" ") &&
                !row.GetCell(1).ToString().Contains("( +");
      }  

      #endregion
   }
}
