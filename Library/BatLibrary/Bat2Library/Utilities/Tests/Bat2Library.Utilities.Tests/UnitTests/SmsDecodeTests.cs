﻿using System;
using Bat2Library.Utilities.Sms;
using Bat2Library.Utilities.Sms.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bat2Library.Utilities.Tests.UnitTests
{
   [TestClass]
   public class SmsDecodeTests
   {
      private const string BASIC_NO_SEX =
         "SCALE 123456789012345 DAY 999 31.12.9999 11:59:59PM Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999";

      private const string BASIC_MALE_FEMALE =
         "SCALE 123456789012345 DAY 999 31.12.9999 11:59:59PM FEMALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999 MALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999";

      private const string EXTENDED_NO_SEX =
         "7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999";

      private const string EXTENDED_MALE_FEMALE =
         "7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999";

      private const string BASIC_NO_SEX_NOT_OK =
         "SCALE 123456789012345 DAY 999 Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999";

      private const string BASIC_MALE_FEMALE_NOT_OK =
         "SCALE 123456789012345 DAY 999 31.12.9999 11:59:59PM FEMALES: 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999 MALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999";

      private const string EXTENDED_NO_SEX_NOT_OK =
         "7FFFFFFF 999 12/31/9999 99999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999"; //Missing one attribute

      private const string EXTENDED_MALE_FEMALE_NOT_OK =
         "7FXFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999"; //Wrong character in serial number

      [TestMethod]
      public void Basic_NoSex_WhenOK()
      {
         SmsData data = null;
         Assert.IsTrue(SmsDecode.Decode(BASIC_NO_SEX, out data));
      }

      [TestMethod]
      public void Basic_NoSex_WhenNotOK()
      {
         SmsData data = null;
         Assert.IsFalse(SmsDecode.Decode(BASIC_NO_SEX_NOT_OK, out data));
      }

      [TestMethod]
      public void Basic_MaleFemale_WhenOK()
      {
         SmsData data = null;
         Assert.IsTrue(SmsDecode.Decode(BASIC_MALE_FEMALE, out data));
      }

      [TestMethod]
      public void Basic_MaleFemale_WhenNotOK()
      {
         SmsData data = null;
         Assert.IsFalse(SmsDecode.Decode(BASIC_MALE_FEMALE_NOT_OK, out data));
      }

      [TestMethod]
      public void Extended_NoSex_WhenOK()
      {
         SmsData data = null;
         Assert.IsTrue(SmsDecode.Decode(EXTENDED_NO_SEX, out data));
      }

      [TestMethod]
      public void Extended_NoSex_WhenNotOK()
      {
         SmsData data = null;
         Assert.IsFalse(SmsDecode.Decode(EXTENDED_NO_SEX_NOT_OK, out data));
      }

      [TestMethod]
      public void Extended_MaleFemale_WhenOK()
      {
         SmsData data = null;
         Assert.IsTrue(SmsDecode.Decode(EXTENDED_MALE_FEMALE, out data));
      }

      [TestMethod]
      public void Extended_MaleFemale_WhenNotOK()
      {
         SmsData data = null;
         Assert.IsFalse(SmsDecode.Decode(EXTENDED_MALE_FEMALE_NOT_OK, out data));
      }
   }
}

