﻿using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bat2Library.Utilities.Tests.UnitTests
{
   [TestClass]
   public class CsvParserTests
   {
      [TestMethod]
      public void Females_WithHeader_OpenByByteArray()
      {
         const string filename = "Resources/csvFemales.csv";
         var parser = new CsvParser(File.ReadAllBytes(filename)); // byte array
         parser.Parse();

         CheckReports(parser);      
      }

      [TestMethod]
      public void Females_WithHeader_OpenByName()
      {
         const string filename = "Resources/csvFemales.csv";
         var parser = new CsvParser(filename);                  // filename
         parser.Parse();

         CheckReports(parser);
      }

      private void CheckReports(CsvParser parser)
      {
         // total count of records
         Assert.AreEqual(5, parser.Reports.Count);

         //exact data for second item
         var index = 1;
         Assert.AreEqual(new DateTime(2015, 3, 10), parser.Reports[index].Date);
         Assert.AreEqual("456", parser.Reports[index].ScaleName);
         Assert.AreEqual(10, parser.Reports[index].Day);
         Assert.AreEqual(25, parser.Reports[index].Count);
         Assert.AreEqual(1, parser.Reports[index].Average);
         Assert.AreEqual(1, parser.Reports[index].Gain);
         Assert.AreEqual(1, parser.Reports[index].Sigma);
         Assert.AreEqual(1, parser.Reports[index].Cv);
         Assert.AreEqual(1, parser.Reports[index].Uniformity);
         Assert.AreEqual("", parser.Reports[index].Note);

         Assert.AreEqual(SexE.SEX_UNDEFINED, parser.Reports[index].Sex);
         Assert.AreEqual(5, parser.FoundRecords);
      }

      [TestMethod]
      public void Males_WithHeader()
      {
         const string filename = "Resources/csvMales.csv";
         var parser = new CsvParser(filename);
         parser.Parse();

         // total count of records
         Assert.AreEqual(3, parser.Reports.Count);

         //exact data for third item
         var index = 2;
         Assert.AreEqual(new DateTime(2015, 3, 31), parser.Reports[index].Date);
         Assert.AreEqual("999", parser.Reports[index].ScaleName);
         Assert.AreEqual(1, parser.Reports[index].Day);
         Assert.AreEqual(10, parser.Reports[index].Count);
         Assert.AreEqual(1, parser.Reports[index].Average);
         Assert.AreEqual(11, parser.Reports[index].Gain);
         Assert.AreEqual(1, parser.Reports[index].Sigma);
         Assert.AreEqual(1, parser.Reports[index].Cv);
         Assert.AreEqual(25, parser.Reports[index].Uniformity);
         Assert.AreEqual("", parser.Reports[index].Note);

         Assert.AreEqual(SexE.SEX_MALE, parser.Reports[index].Sex);
         Assert.AreEqual(3, parser.FoundRecords);
      }

      [TestMethod]
      public void File_WithoutHeader()
      {
         const string filename = "Resources/csvWithoutHeader.csv";
         var parser = new CsvParser(filename);
         parser.Parse();

         // total count of records
         Assert.AreEqual(2, parser.Reports.Count);

         //exact data for third item
         var index = 0;
         Assert.AreEqual(new DateTime(2015, 3, 10), parser.Reports[index].Date);
         Assert.AreEqual("1111", parser.Reports[index].ScaleName);
         Assert.AreEqual(1, parser.Reports[index].Day);
         Assert.AreEqual(10, parser.Reports[index].Count);
         Assert.AreEqual(1, parser.Reports[index].Average);
         Assert.AreEqual(1, parser.Reports[index].Gain);
         Assert.AreEqual(1, parser.Reports[index].Sigma);
         Assert.AreEqual(1, parser.Reports[index].Cv);
         Assert.AreEqual(1, parser.Reports[index].Uniformity);
         Assert.AreEqual("", parser.Reports[index].Note);

         Assert.AreEqual(SexE.SEX_UNDEFINED, parser.Reports[index].Sex);
         Assert.AreEqual(2, parser.FoundRecords);
      }

      [TestMethod]
      public void File_AdditionalParameters()
      {
         const string filename = "Resources/csvFemalesAdditionalParamOtherLang.csv";
         var parser = new CsvParser(filename);
         parser.Parse();

         // total count of records
         Assert.AreEqual(13, parser.Reports.Count);

         //exact data for third item
         var index = 10;
         Assert.AreEqual(new DateTime(2015, 4, 29), parser.Reports[index].Date);
         Assert.AreEqual("101", parser.Reports[index].ScaleName);
         Assert.AreEqual(30, parser.Reports[index].Day);
         Assert.AreEqual(7, parser.Reports[index].Count);
         Assert.AreEqual(0.099, parser.Reports[index].Average);
         Assert.AreEqual(0.099, parser.Reports[index].Gain);
         Assert.AreEqual(0.001, parser.Reports[index].Sigma);
         Assert.AreEqual(1, parser.Reports[index].Cv);
         Assert.AreEqual(100, parser.Reports[index].Uniformity);
         Assert.AreEqual("", parser.Reports[index].Note);

         Assert.AreEqual(SexE.SEX_UNDEFINED, parser.Reports[index].Sex);
         Assert.AreEqual(13, parser.FoundRecords);
      }

      [TestMethod]
      public void File_Note()
      {
         const string filename = "Resources/csvFemalesAdditionalParamOtherLang.csv";
         var parser = new CsvParser(filename);
         var reports = parser.Parse().ToList();

         // total count of records
         Assert.AreEqual(13, parser.Reports.Count);
         var index = 11;
         Assert.AreEqual("Test poznamky", reports[index].Note);
         Assert.AreEqual(13, parser.FoundRecords);
      }

      [TestMethod]
      public void File_Header_Error()
      {
         const string filename = "Resources/csvHeaderError.csv";
         var parser = new CsvParser(filename);
         var reports = parser.Parse().ToList();

         // total count of records
         Assert.AreEqual(3, reports.Count);

         //exact data for third item
         var index = 1;
         Assert.AreEqual(new DateTime(2015, 3, 11), reports[index].Date);
         Assert.AreEqual("123", reports[index].ScaleName);
         Assert.AreEqual(2, reports[index].Day);
         Assert.AreEqual(3, reports[index].Count);
         Assert.AreEqual(1, reports[index].Average);
         Assert.AreEqual(1, reports[index].Gain);
         Assert.AreEqual(1, reports[index].Sigma);
         Assert.AreEqual(1, reports[index].Cv);
         Assert.AreEqual(1, reports[index].Uniformity);
         Assert.AreEqual("", reports[index].Note);

         Assert.AreEqual(SexE.SEX_MALE, reports[index].Sex);
         Assert.AreEqual(3, parser.FoundRecords);
      }

      [TestMethod]
      public void File_DataError_MissingColumn()
      {
         const string filename = "Resources/csvDataError1.csv";
         var parser = new CsvParser(filename);
         var reports = parser.Parse();
         Assert.IsNull(reports);
         Assert.AreEqual(3, parser.FoundRecords);
      }

      [TestMethod]
      public void File_DataError_MissingValue()
      {
         const string filename = "Resources/csvDataError2.csv";
         var parser = new CsvParser(filename);
         parser.Parse();

         // total count of records which are OK
         Assert.AreEqual(1, parser.Reports.Count);

         //exact data for third item
         var index = 0;
         Assert.AreEqual(new DateTime(2015, 3, 11), parser.Reports[index].Date);
         Assert.AreEqual("999", parser.Reports[index].ScaleName);
         Assert.AreEqual(1, parser.Reports[index].Day);
         Assert.AreEqual(10, parser.Reports[index].Count);
         Assert.AreEqual(1, parser.Reports[index].Average);
         Assert.AreEqual(11, parser.Reports[index].Gain);
         Assert.AreEqual(1, parser.Reports[index].Sigma);
         Assert.AreEqual(1, parser.Reports[index].Cv);
         Assert.AreEqual(25, parser.Reports[index].Uniformity);
         Assert.AreEqual("", parser.Reports[index].Note);

         Assert.AreEqual(SexE.SEX_UNDEFINED, parser.Reports[index].Sex);
         Assert.AreEqual(4, parser.FoundRecords);
      }

      [TestMethod]
      public void File_WrongDelimiter()
      {
         const string filename = "Resources/csvMales.csv";
         var parser = new CsvParser(filename)
         {
            delimiter = '-'
         };
         var reports = parser.Parse();
         Assert.IsNull(reports);
         Assert.AreEqual(0, parser.FoundRecords);
      }

      [TestMethod]
      public void File_OnlyData_WithoutHeader()
      {
         const string filename = "Resources/csvOnlyData.csv";
         var parser = new CsvParser(filename);
         var reports = parser.Parse();
         Assert.AreEqual(3, reports.Count());
         Assert.AreEqual(3, parser.FoundRecords);
      }
   }
}
