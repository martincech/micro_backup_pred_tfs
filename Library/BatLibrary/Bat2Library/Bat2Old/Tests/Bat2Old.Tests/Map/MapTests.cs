﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Bat2Library;
using Bat2Library.Bat2Old;
using Bat2Library.Bat2Old.Flash;
using Bat2Library.Bat2Old.Map;
using Bat2Library.Connection.Interface.Domain.Old;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using Utilities;

namespace Bat2Old.Tests.Map
{
    [TestClass]
    public class MapTests
    {
        private Fixture fixture;
        public Fixture Fixture {
            get { return fixture; }
        }

        [TestInitialize]
        public void Init()
        {
            fixture = new Fixture();
            fixture.RepeatCount = 2;
         

            // fix.Customize<OldConfiguration>(x => x.Without(a => a.VersionInfo));
            fixture.Customize<OldWeighingStart>(x => x.With(a => a.DateTime, new DateTime(2005, 4, 3, 2, 4, 0)));
            fixture.Customize<OldArchive>(x => x.With(a => a.DateTime, new DateTime(2003, 5, 2, 1, 55, 0)));
            fixture.Customize<OldGsm>(x => 
                x.Without(a => a.DayMidnight1)
                 .Without(a => a.MidnightSendHour)
                 .Without(a => a.MidnightSendMin)
                 .Without(a => a.PeriodMidnight1)
                 .Without(a => a.PeriodMidnight2)
                 .With(a=>a.Numbers, new List<string>()
                 {
                     "420111222333   ",
                     "999888666      ",
                     "123456789      ",
                     "111222555666   ",
                     "4556872045     "
                 }));
            fixture.Customize<OldFlock>(x =>
                x.With(a => a.GrowthCurveFemale, new List<OldCurvePoint>()
                {
                    new OldCurvePoint() {Day = 1, Weight = 1},
                    new OldCurvePoint() {Day = 2, Weight = 3},
                    new OldCurvePoint() {Day = 3, Weight = 5},
                }).With(a => a.GrowthCurveMale, new List<OldCurvePoint>()
                {
                    new OldCurvePoint() {Day = 1, Weight = 2},
                    new OldCurvePoint() {Day = 2, Weight = 4},
                    new OldCurvePoint() {Day = 4, Weight = 6},
                }));

            fixture.Customize<OldConfiguration>(x =>
                x.With(a => a.Units, WeightUnitsE.WEIGHT_UNITS_LB)
                    .With(a => a.VersionInfo, new BaseVersionInfo()
                    {
                        SoftwareBuild = 1,
                        SoftwareMajor = 1,
                        SoftwareMinor = 1,
                        SerialNumber = 157,
                        Modification = 0
                    }));
            fixture.Customize<OldConfiguration>(x => x.With(a => a, fixture.Create<OldConfiguration>()));
            fixture.Customize<OldFlockHeader>(x => x.With(a=>a.Title, "Title   ").With(a=>a.Number,(byte)0));

        

        }

        #region Archive

        [TestMethod]
        public void Archive_When_NotNull()
        {
           
            var oldArchive = fixture.Create<OldArchive>();

            var tArchive = oldArchive.Map();
            Assert.IsFalse(tArchive.Equals(new TArchive()));

            var oldArchiveNew = tArchive.Map();
            Assert.IsTrue(oldArchive.Same(oldArchiveNew));
        }

        [TestMethod]
        public void Archive_When_Null()
        {
            OldArchive oldArchive = null;
            var tArchive = oldArchive.Map();
            Assert.IsTrue(tArchive.Equals(new TArchive()));
            var oldArchiveNew = tArchive.Map();
            Assert.IsNull(oldArchiveNew);
        }

        #endregion

        #region ArchiveDailyInfo

        [TestMethod]
        public void ArchiveDailyInfo_When_NotNull()
        {
           
            var oldArchiveDailyInfo = fixture.Create<OldArchiveDailyInfo>();

            var tArchiveDailyInfo = oldArchiveDailyInfo.Map();
            Assert.IsFalse(tArchiveDailyInfo.Equals(new TArchiveDailyInfo()));

            var oldArchiveDailyInfoNew = tArchiveDailyInfo.Map();
            Assert.IsTrue(oldArchiveDailyInfo.Same(oldArchiveDailyInfoNew));
        }

        [TestMethod]
        public void ArchiveDailyInfo_When_Null()
        {
            OldArchiveDailyInfo oldArchiveDailyInfo = null;
            var tArchiveDailyInfo = oldArchiveDailyInfo.Map();
            Assert.IsTrue(tArchiveDailyInfo.Equals(new TArchiveDailyInfo()));
            var oldArchiveDailyInfoNew = tArchiveDailyInfo.Map();
            Assert.IsNull(oldArchiveDailyInfoNew);
        }

        #endregion

        #region Calibration

        [TestMethod]
        public void Calibration_When_NotNull()
        {
            var oldCalibration = fixture.Create<OldCalibration>();

            var tCalibration = oldCalibration.Map();
            Assert.IsFalse(tCalibration.Equals(new TCalibration()));

            var oldCalibrationNew = tCalibration.Map();
            Assert.IsTrue(oldCalibration.Same(oldCalibrationNew));
        }

        [TestMethod]
        public void Calibration_When_Null()
        {
            OldCalibration oldCalibration = null;
            var tCalibration = oldCalibration.Map();
            Assert.IsTrue(tCalibration.Equals(new TCalibration()));
            var oldCalibrationNew = tCalibration.Map();
            Assert.IsNull(oldCalibrationNew);
        }

        #endregion

        #region ConfigSection

        [TestMethod]
        public void ConfigSection_When_NotNull()
        {
            var oldConfigSection = fixture.Create<OldConfiguration>();
            oldConfigSection.Flocks.Last().Header.Number = 1;

            var tConfigSection = oldConfigSection.MapV111();
            Assert.IsFalse(tConfigSection.Equals(new Bat2Library.Bat2Old.Flash.v111.TConfigSection()));

            var oldConfigSectionNew = tConfigSection.Map();
            Assert.IsTrue(oldConfigSection.Same(oldConfigSectionNew));
        }

        [TestMethod]
        public void ConfigSection_When_Null()
        {
            OldConfiguration oldConfigSection = null;
            var tConfigSection = oldConfigSection.MapV111();
            Assert.IsTrue(tConfigSection.Equals(new Bat2Library.Bat2Old.Flash.v111.TConfigSection()));
            var oldConfigSectionNew = tConfigSection.Map();
            Assert.IsNull(oldConfigSectionNew);
        }

        #endregion

        #region Configuration

        [TestMethod]
        public void Configuration_When_NotNull()
        {

            var oldConfiguration = fixture.Create<OldConfiguration>();

            var tConfiguration = oldConfiguration.MapV111();
            Assert.IsFalse(tConfiguration.Equals(new Bat2Library.Bat2Old.Flash.v111.TConfig()));

            var oldConfigurationNew = tConfiguration.Map();
            Assert.IsTrue(oldConfiguration.Same(oldConfigurationNew));
        }

        [TestMethod]
        public void Configuration_When_Null()
        {
            OldConfiguration oldConfiguration = null;
            var tConfiguration = oldConfiguration.MapV111();
            Assert.IsTrue(tConfiguration.Equals(new Bat2Library.Bat2Old.Flash.v111.TConfig()));
            var oldConfigurationNew = tConfiguration.Map();
            Assert.IsNull(oldConfigurationNew);
        }

        #endregion

        #region Flash

        [TestMethod]
        public void Flash_When_NotNull()
        {

            var oldFlash = fixture.Create<OldBat2DeviceData>();
            oldFlash.Configuration.Flocks.Last().Header.Number = 1;
            var tFlash = oldFlash.MapV111();
            Assert.IsFalse(tFlash.Equals(new Bat2Library.Bat2Old.Flash.v111.TFlash()));

            var oldFlashNew = tFlash.Map();
            Assert.IsTrue(oldFlash.Same(oldFlashNew));
        }

        [TestMethod]
        public void Flash_When_Null()
        {
            OldBat2DeviceData oldFlash = null;
            var tFlash = oldFlash.MapV111();
            Assert.IsTrue(tFlash.Equals(new Bat2Library.Bat2Old.Flash.v111.TFlash()));
            var oldFlashNew = tFlash.Map();
            Assert.IsNull(oldFlashNew);
        }

        #endregion

        #region Flock

        [TestMethod]
        public void Flock_When_NotNull()
        {

            var oldFlock = fixture.Create<OldFlock>();
            
            var tFlock = oldFlock.Map();
            Assert.IsFalse(tFlock.Equals(new TFlock()));

            var oldFlockNew = tFlock.Map();
            Assert.IsTrue(oldFlock.Same(oldFlockNew));
        }

        [TestMethod]
        public void Flock_When_Null()
        {
            OldFlock oldFlock = null;
            var tFlock = oldFlock.Map();
            Assert.IsTrue(tFlock.Equals(new TFlock()));
            var oldFlockNew = tFlock.Map();
            Assert.IsNull(oldFlockNew);
        }

        #endregion

        #region FlockHeader

        [TestMethod]
        public void FlockHeader_When_NotNull()
        {

            var oldFlockHeader = fixture.Create<OldFlockHeader>();

            var tFlockHeader = oldFlockHeader.Map();
            Assert.IsFalse(tFlockHeader.Equals(new TFlockHeader()));

            var oldFlockHeaderNew = tFlockHeader.Map();
            Assert.IsTrue(oldFlockHeader.Same(oldFlockHeaderNew));
        }

        [TestMethod]
        public void FlockHeader_When_Null()
        {
            OldFlockHeader oldFlockHeader = null;
            var tFlockHeader = oldFlockHeader.Map();
            Assert.IsTrue(tFlockHeader.Equals(new TFlockHeader()));
            var archiveNew = tFlockHeader.Map();
            Assert.IsNull(archiveNew);
        }

        #endregion

        #region Gsm

        [TestMethod]
        public void Gsm_When_NotNull()
        {

            var oldGsm = fixture.Create<OldGsm>();

            var tGsm = oldGsm.MapV111();
            Assert.IsFalse(tGsm.Equals(new Bat2Library.Bat2Old.Flash.v111.TGsm()));

            var oldGsmNew = tGsm.Map();
            Assert.IsTrue(oldGsm.Same(oldGsmNew));
        }

        [TestMethod]
        public void Gsm_When_Null()
        {
            OldGsm oldGsm = null;
            var tGsm = oldGsm.MapV111();
            Assert.IsTrue(tGsm.Equals(new Bat2Library.Bat2Old.Flash.v111.TGsm()));
            var oldGsmNew = tGsm.Map();
            Assert.IsNull(oldGsmNew);
        }

        #endregion

        #region Histogram

        [TestMethod]
        public void Histogram_When_NotNull()
        {

            var oldHistogram = fixture.Create<OldHistogram>();

            var tHistogram = oldHistogram.Map();
            Assert.IsFalse(tHistogram.Equals(new THistogram()));

            var oldHistogramNew = tHistogram.Map();
            Assert.IsTrue(oldHistogram.Same(oldHistogramNew));
        }

        [TestMethod]
        public void Histogram_When_Null()
        {
            OldHistogram oldHistogram = null;
            var tHistogram = oldHistogram.Map();
            Assert.IsTrue(tHistogram.Equals(new THistogram()));
            var oldHistogramNew = tHistogram.Map();
            Assert.IsNull(oldHistogramNew);
        }

        #endregion

        #region LoggerSample

        [TestMethod]
        public void LoggerSample_When_NotNull()
        {

            var oldLoggerSample = fixture.Create<OldLoggerSample>();

            var tLoggerSample = oldLoggerSample.Map();
            Assert.IsFalse(tLoggerSample.Equals(new TLoggerSample()));

            var oldLoggerSampleNew = tLoggerSample.Map();
            Assert.IsTrue(oldLoggerSample.Same(oldLoggerSampleNew));
        }

        [TestMethod]
        public void LoggerSample_When_Null()
        {
            OldLoggerSample oldLoggerSample = null;
            var tLoggerSample = oldLoggerSample.Map();
            Assert.IsTrue(tLoggerSample.Equals(new TLoggerSample()));
            var oldLoggerSampleNew = tLoggerSample.Map();
            Assert.IsNull(oldLoggerSampleNew);
        }

        #endregion

        #region OnlineSample

        [TestMethod]
        public void OnlineSample_When_NotNull()
        {

            var oldOnlineSample = fixture.Create<OldOnlineSample>();

            var tOnlineSample = oldOnlineSample.Map();
            Assert.IsFalse(tOnlineSample.Equals(new TOnlineSample()));

            var oldOnlineSampleNew = tOnlineSample.Map();
            Assert.IsTrue(oldOnlineSample.Same(oldOnlineSampleNew));
        }

        [TestMethod]
        public void OnlineSample_When_Null()
        {
            OldOnlineSample oldOnlineSample = null;
            var tOnlineSample = oldOnlineSample.Map();
            Assert.IsTrue(tOnlineSample.Equals(new TOnlineSample()));
            var oldOnlineSampleNew = tOnlineSample.Map();
            Assert.IsNull(oldOnlineSampleNew);
        }

        #endregion

        #region QuickWeighing

        [TestMethod]
        public void QuickWeighing_When_NotNull()
        {

            var oldQuickWeighing = fixture.Create<OldQuickWeighing>();

            var tQuickWeighing = oldQuickWeighing.Map();
            Assert.IsFalse(tQuickWeighing.Equals(new TQuickWeighing()));

            var oldQuickWeighingNew = tQuickWeighing.Map();
            Assert.IsTrue(oldQuickWeighing.Same(oldQuickWeighingNew));
        }

        [TestMethod]
        public void QuickWeighing_When_Null()
        {
            OldQuickWeighing oldQuickWeighing = null;
            var tQuickWeighing = oldQuickWeighing.Map();
            Assert.IsTrue(tQuickWeighing.Equals(new TQuickWeighing()));
            var oldQuickWeighingNew = tQuickWeighing.Map();
            Assert.IsNull(oldQuickWeighingNew);
        }

        #endregion

        #region Rs485

        [TestMethod]
        public void Rs485_When_NotNull()
        {

            var oldRs485 = fixture.Create<OldRs485>();

            var tRs485 = oldRs485.Map();
            Assert.IsFalse(tRs485.Equals(new TRs485()));

            var oldRs485New = tRs485.Map();
            Assert.IsTrue(oldRs485.Same(oldRs485New));
        }

        [TestMethod]
        public void Rs485_When_Null()
        {
            OldRs485 oldRs485 = null;
            var tRs485 = oldRs485.Map();
            Assert.IsTrue(tRs485.Equals(new TRs485()));
            var oldRs485New = tRs485.Map();
            Assert.IsNull(oldRs485New);
        }

        #endregion

        #region Statistic

        [TestMethod]
        public void Statistic_When_NotNull()
        {

            var oldStatistic = fixture.Create<OldStatistic>();

            var tStatistic = oldStatistic.Map();
            Assert.IsFalse(tStatistic.Equals(new TStatistic()));

            var oldStatisticNew = tStatistic.Map();
            Assert.IsTrue(oldStatistic.Same(oldStatisticNew));
        }

        [TestMethod]
        public void Statistic_When_Null()
        {
            OldStatistic oldStatistic = null;
            var tStatistic = oldStatistic.Map();
            Assert.IsTrue(tStatistic.Equals(new TStatistic()));
            var oldStatisticNew = tStatistic.Map();
            Assert.IsNull(oldStatisticNew);
        }

        #endregion

        #region WeighingStart

        [TestMethod]
        public void WeighingStart_When_NotNull()
        {

            var oldWeighingStart = fixture.Create<OldWeighingStart>();

            var tWeighingStart = oldWeighingStart.Map();
            Assert.IsFalse(tWeighingStart.Equals(new TWeighingStart()));

            var oldWeighingStartNew = tWeighingStart.Map();
            Assert.IsTrue(oldWeighingStart.Same(oldWeighingStartNew));
        }

        [TestMethod]
        public void WeighingStart_When_Null()
        {
            OldWeighingStart oldWeighingStart = null;
            var tWeighingStart = oldWeighingStart.Map();
            Assert.IsTrue(tWeighingStart.Equals(new TWeighingStart()));
            var oldWeighingStartNew = tWeighingStart.Map();
            Assert.IsNull(oldWeighingStartNew);
        }

        #endregion

        #region WeightCorrection

        [TestMethod]
        public void WeightCorrection_When_NotNull()
        {

            var oldWeightCorrection = fixture.Create<OldWeightCorrection>();

            var tWeightCorrection = oldWeightCorrection.Map();
            Assert.IsFalse(tWeightCorrection.Equals(new TWeightCorrection()));

            var oldWeightCorrectionNew = tWeightCorrection.Map();
            Assert.IsTrue(oldWeightCorrection.Same(oldWeightCorrectionNew));
        }

        [TestMethod]
        public void WeightCorrection_When_Null()
        {
            OldWeightCorrection oldWeightCorrection = null;
            var tWeightCorrection = oldWeightCorrection.Map();
            Assert.IsTrue(tWeightCorrection.Equals(new TWeightCorrection()));
            var oldWeightCorrectionNew = tWeightCorrection.Map();
            Assert.IsNull(oldWeightCorrectionNew);
        }

        #endregion
    }
}
