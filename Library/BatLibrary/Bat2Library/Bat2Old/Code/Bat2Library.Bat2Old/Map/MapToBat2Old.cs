﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bat2Library.Bat2Old.Constants;
using Bat2Library.Bat2Old.Flash;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Bat2Old.Map
{
   public static class MapToBat2Old
   {
      #region Bat v1.11

      public static OldBat2DeviceData Map(this Flash.v111.TFlash flash)
      {
         if (flash.Equals(new Flash.v111.TFlash()))
         {
            return null;
         }

         try
         {
            var oldFlash = new OldBat2DeviceData
            {
               Archive = flash.Archive.Select(x => x.Map()),
               Configuration = flash.ConfigSection.Map()
            };

            return oldFlash;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldConfiguration Map(this Flash.v111.TConfigSection config)
      {
         if (config.Equals(new Flash.v111.TConfigSection()))
         {
            return null;
         }

         try
         {
            var oldConfig = config.Config.Map();

            oldConfig.Calibration = config.Calibration.Map();
            oldConfig.Flocks = config.Flocks.Where(w => CheckTFlockHeader(w.Header)).Select(x => x.Map());

            return oldConfig;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldConfiguration Map(this Flash.v111.TConfig config)
      {
         if (config.Equals(new Flash.v111.TConfig()))
         {
            return null;
         }
         try
         {
            var oldConfig = new OldConfiguration
            {
               AutoMode = config.AutoMode,
               Backlight = config.Backlight.Map(),
               Language = config.Language,
               Battery = config.Battery,
               Build = config.Build,
               ComparisonFlock = config.ComparisonFlock,
               Filter = config.Filter,
               Gsm = config.Gsm.Map(),
               HistogramRange = config.HistogramRange,
               HwVersion = config.HwVersion,
               JumpMode = (PlatformStepModeE) (byte) config.JumpMode,
               MarginAboveFemale = config.MarginAbove.First(),
               MarginAboveMale = config.MarginAbove.Last(),
               MarginBelowFemale = config.MarginBelow.First(),
               MarginBelowMale = config.MarginBelow.Last(),
               Rs485 = config.Rs485.Map(),
               StabilizationRange = config.StabilizationRange,
               StabilizationTime = config.StabilizationTime,
               UniformityRange = config.UniformityRange,
               Units = config.Units.Map(),
               VersionInfo = new BaseVersionInfo
               {
                  SerialNumber = config.IdentificationNumber,
                  SoftwareBuild = (byte) ((config.Version >> 8) & 15),
                  SoftwareMajor = (byte) ((config.Version >> 4) & 15),
                  SoftwareMinor = (byte) ((config.Version >> 0) & 15),
               },
               WeighingStart = config.WeighingStart.Map(),
               WeightCorrection = config.WeightCorrection.Map(),
            };
            return oldConfig;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldGsm Map(this Flash.v111.TGsm gsm)
      {
         if (gsm.Equals(new Flash.v111.TGsm()))
         {
            return null;
         }

         try
         {
            var oldGsm = new OldGsm
            {
               ExecuteOnRequest = gsm.ExecuteOnRequest == 1,
               CheckNumbers = gsm.CheckNumbers == 1,
               NumberCount = gsm.NumberCount,
               Numbers = gsm.Numbers.GetNumbers(),
               PeriodMidnight = gsm.PeriodMidnight,
               SendMidnight = gsm.SendMidnight == 1,
               SendOnRequest = gsm.SendOnRequest == 1,
               Use = gsm.Use == 1
            };

            return oldGsm;
         }
         catch (Exception)
         {
            return null;
         }
      }

      #endregion

      #region Bat v1.50

      public static OldBat2DeviceData Map(this Flash.v150.TFlash flash)
      {
         if (flash.Equals(new Flash.v150.TFlash()))
         {
            return null;
         }

         try
         {
            var oldFlash = new OldBat2DeviceData
            {
               Archive = flash.Archive.Select(x => x.Map()),
               Configuration = flash.ConfigSection.Map()
            };

            return oldFlash;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldConfiguration Map(this Flash.v150.TConfigSection config)
      {
         if (config.Equals(new Flash.v150.TConfigSection()))
         {
            return null;
         }

         try
         {
            var oldConfig = config.Config.Map();
            oldConfig.Calibration = config.Calibration.Map();
            oldConfig.Flocks = config.Flocks.Where(w => CheckTFlockHeader(w.Header)).Select(x => x.Map());
            return oldConfig;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldConfiguration Map(this Flash.v150.TConfig config)
      {
         if (config.Equals(new Flash.v150.TConfig()))
         {
            return null;
         }
         try
         {
            var oldConfig = new OldConfiguration
            {
               AutoMode = config.AutoMode,
               Backlight = config.Backlight.Map(),
               Language = config.Language,
               Build = config.Build,
               ComparisonFlock = config.ComparisonFlock,
               Filter = config.Filter,
               Gsm = config.Gsm.Map(),
               HistogramRange = config.HistogramRange,
               HwVersion = config.HwVersion,
               JumpMode = (PlatformStepModeE) (byte) config.JumpMode,
               MarginAboveFemale = config.MarginAbove.First(),
               MarginAboveMale = config.MarginAbove.Last(),
               MarginBelowFemale = config.MarginBelow.First(),
               MarginBelowMale = config.MarginBelow.Last(),
               Rs485 = config.Rs485.Map(),
               StabilizationRange = config.StabilizationRange,
               StabilizationTime = config.StabilizationTime,
               UniformityRange = config.UniformityRange,
               Units = config.Units.Map(),
               VersionInfo = new BaseVersionInfo
               {
                  SerialNumberArray = config.IdentificationNumber,
                  SoftwareBuild = (byte) ((config.Version >> 8) & 15),
                  SoftwareMajor = (byte) ((config.Version >> 4) & 15),
                  SoftwareMinor = (byte) ((config.Version >> 0) & 15),
               },
               WeighingStart = config.WeighingStart.Map(),
               WeightCorrection = config.WeightCorrection.Map()
            };
            return oldConfig;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldGsm Map(this Flash.v150.TGsm gsm)
      {
         if (gsm.Equals(new Flash.v150.TGsm()))
         {
            return null;
         }

         try
         {
            var oldGsm = new OldGsm
            {
               CheckNumbers = gsm.CheckNumbers == 1,
               NumberCount = gsm.NumberCount,
               Numbers = gsm.Numbers.GetNumbers(),
               SendMidnight = gsm.SendMidnight == 1,
               SendOnRequest = gsm.SendOnRequest == 1,
               Use = gsm.Use == 1,
               DayMidnight1 = gsm.DayMidnight1,
               MidnightSendHour = gsm.MidnightSendHour,
               MidnightSendMin = gsm.MidnightSendMin,
               PeriodMidnight1 = gsm.PeriodMidnight1,
               PeriodMidnight2 = gsm.PeriodMidnight2
            };

            return oldGsm;
         }
         catch (Exception)
         {
            return null;
         }
      }

      #endregion

      public static OldArchive Map(this TArchive archive)
      {
         if (archive.Equals(new TArchive()))
         {
            return null;
         }
         try
         {
            var oldArchive = new OldArchive
            {
               DateTime = archive.DateTime.Map(),
               ArchiveItemFemale = new OldArchiveItem
               {
                  Hist = archive.Hist != null ? archive.Hist.First().Map() : null,
                  LastAverage = archive.LastAverage != null ? archive.LastAverage.First() : (ushort) 0,
                  RealUniformity = archive.RealUniformity != null ? archive.RealUniformity.First() : (byte) 0,
                  Stat = archive.Stat != null ? archive.Stat.First().Map() : null,
                  TargetWeight = archive.TargetWeight != null ? archive.TargetWeight.First() : (ushort) 0,
               },
               ArchiveItemMale = new OldArchiveItem
               {
                  Hist = archive.Hist != null ? archive.Hist.Last().Map() : null,
                  LastAverage = archive.LastAverage != null ? archive.LastAverage.Last() : (ushort) 0,
                  RealUniformity = archive.RealUniformity != null ? archive.RealUniformity.Last() : (byte) 0,
                  Stat = archive.Stat != null ? archive.Stat.Last().Map() : null,
                  TargetWeight = archive.TargetWeight != null ? archive.TargetWeight.Last() : (ushort) 0,
               },
               DayNumber = archive.DayNumber,
               RealUniformityUsed = archive.RealUniformityUsed == 1
            };

            return oldArchive;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldArchiveDailyInfo Map(this TArchiveDailyInfo archive)
      {
         if (archive.Equals(new TArchiveDailyInfo()))
         {
            return null;
         }

         try
         {
            var oldarchive = new OldArchiveDailyInfo
            {
               Archive = archive.Archive.Map(),
               Samples = archive.Samples.Select(x => x.Map())
            };

            return oldarchive;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldLoggerSample Map(this TLoggerSample sample)
      {
         if (sample.Equals(new TLoggerSample()))
         {
            return null;
         }

         try
         {
            var oldSample = new OldLoggerSample
            {
               Flag = sample.Flag,
               Value = sample.Value
            };

            return oldSample;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static DateTime Map(this TLongDateTime date)
      {
         if (date.Equals(new TLongDateTime()))
         {
            return new DateTime();
         }

         try
         {
            var dateTime = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Min, 0);

            return dateTime;
         }
         catch (Exception)
         {
            return new DateTime();
         }
      }

      public static OldStatistic Map(this TStatistic stat)
      {
         if (stat.Equals(new TStatistic()))
         {
            return null;
         }

         try
         {
            var oldStat = new OldStatistic
            {
               Count = stat.Count,
               X2Suma = stat.X2Suma,
               XSuma = stat.XSuma
            };
            return oldStat;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldHistogram Map(this THistogram hist)
      {
         if (hist.Equals(new THistogram()))
         {
            return null;
         }

         try
         {
            var oldHist = new OldHistogram
            {
               Center = hist.Center,
               Slot = hist.Slot,
               Step = hist.Step
            };

            return oldHist;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldCalibration Map(this TCalibration calibration)
      {
         if (calibration.Equals(new TCalibration()))
         {
            return null;
         }

         try
         {
            var oldCalibration = new OldCalibration
            {
               Division = calibration.Division,
               Range = calibration.Range,
               RangeCalibration = calibration.RangeCalibration,
               ZeroCalibration = calibration.ZeroCalibration
            };

            return oldCalibration;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldWeightCorrection Map(this TWeightCorrection weightCorr)
      {
         if (weightCorr.Equals(new TWeightCorrection()))
         {
            return null;
         }

         try
         {
            var oldWeightCorr = new OldWeightCorrection
            {
               Correction = weightCorr.Correction,
               Day1 = weightCorr.Day1,
               Day2 = weightCorr.Day2
            };

            return oldWeightCorr;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldWeighingStart Map(this TWeighingStart weighingStart)
      {
         if (weighingStart.Equals(new TWeighingStart()))
         {
            return null;
         }

         try
         {
            var oldWeighingStart = new OldWeighingStart
            {
               CurrentFlock = weighingStart.CurrentFlock,
               CurveDayShift = weighingStart.CurveDayShift,
               DateTime = weighingStart.DateTime.Map(),
               Online = weighingStart.Online,
               QuickWeighing = weighingStart.QuickWeighing.Map(),
               Running = weighingStart.Running,
               UniformityRange = weighingStart.UniformityRange,
               UseFlock = weighingStart.UseFlock,
               WaitingForStart = weighingStart.WaitingForStart
            };

            return oldWeighingStart;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldQuickWeighing Map(this TQuickWeighing weighing)
      {
         if (weighing.Equals(new TQuickWeighing()))
         {
            return null;
         }

         try
         {
            var oldWeighing = new OldQuickWeighing
            {
               InitialWeightFemale = weighing.InitialWeight.First(),
               InitialWeightMale = weighing.InitialWeight.Last(),
               UseBothGenders = weighing.UseBothGenders == 1
            };

            return oldWeighing;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldRs485 Map(this TRs485 rs485)
      {
         if (rs485.Equals(new TRs485()))
         {
            return null;
         }

         try
         {
            var oldRs485 = new OldRs485
            {
               Address = rs485.Address,
               Parity = rs485.Parity,
               Protocol = rs485.Protocol,
               ReplyDelay = rs485.ReplyDelay,
               SilentInterval = rs485.SilentInterval,
               Speed = rs485.Speed
            };

            return oldRs485;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldFlock Map(this TFlock flock)
      {
         if (flock.Equals(new TFlock()))
         {
            return null;
         }

         try
         {
            //TODO: How to separate male from female points in collection growthcurve 

            var femaleCurve = new List<TCurvePoint>();
            var maleCurve = new List<TCurvePoint>();

            var lastDay = -1;
            var isFemale = true;

            foreach (var tCurvePoint in flock.GrowthCurve)
            {
               if (lastDay < tCurvePoint.Day && isFemale)
               {
                  femaleCurve.Add(tCurvePoint);
                  lastDay = tCurvePoint.Day;
               }
               else
               {
                  maleCurve.Add(tCurvePoint);
                  isFemale = false;
               }
            }

            if (maleCurve.Count == 0)
            {
               maleCurve = femaleCurve;
            }

            var oldFlock = new OldFlock
            {
               GrowthCurveFemale = femaleCurve.Select(x => x.Map()),
               GrowthCurveMale = maleCurve.Select(x => x.Map()),
               Header = flock.Header.Map()
            };

            return oldFlock;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldCurvePoint Map(this TCurvePoint point)
      {
         if (point.Equals(new TCurvePoint()))
         {
            return null;
         }

         try
         {
            var oldPoint = new OldCurvePoint
            {
               Day = point.Day,
               Weight = point.Weight
            };

            return oldPoint;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldFlockHeader Map(this TFlockHeader header)
      {
         if (header.Equals(new TFlockHeader()))
         {
            return null;
         }

         try
         {
            var oldHeader = new OldFlockHeader
            {
               Number = header.Number,
               Title = Encoding.UTF8.GetString(header.Title),
               UseBothGenders = header.UseBothGenders == 1,
               UseCurves = header.UseCurves == 1,
               WeighFrom = header.WeighFrom,
               WeighTill = header.WeighTill
            };
            return oldHeader;
         }
         catch (Exception)
         {
            return null;
         }
      }

      private static IEnumerable<string> GetNumbers(this byte[] numbersBytes)
      {
         const int size = TConstants.GSM_NUMBER_MAX_COUNT*TConstants.GSM_NUMBER_MAX_LENGTH;

         var numbers = new List<string>();

         if (numbersBytes.Length > size)
         {
            numbersBytes = numbersBytes.Take(size).ToArray();
         }

         for (var i = 0; i < size; i += TConstants.GSM_NUMBER_MAX_LENGTH)
         {
            if (numbersBytes.Skip(i).Count() < TConstants.GSM_NUMBER_MAX_LENGTH)
            {
               break;
            }

            var number =
               Encoding.UTF8.GetString(numbersBytes.Skip(i).Take(TConstants.GSM_NUMBER_MAX_LENGTH).ToArray());
            if (number.Any(char.IsDigit))
            {
               numbers.Add(number);
            }
         }

         return numbers;
      }

      public static OldOnlineSample Map(this TOnlineSample sample)
      {
         if (sample.Equals(new TOnlineSample()))
         {
            return null;
         }

         try
         {
            var oldOnlineSample = new OldOnlineSample
            {
               Flag = sample.Flag,
               Value = sample.Value
            };

            return oldOnlineSample;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public static OldWeighing Map(this TWeighing weighing)
      {
         if (weighing.Equals(new TWeighing()))
         {
            return null;
         }

         try
         {
            var oldWeighing = new OldWeighing();

            return oldWeighing;
         }
         catch (Exception)
         {
            return null;
         }
      }

      #region Enum mapping

      public static BacklightModeE Map(this TBacklight backlight)
      {
         var oldBacklight = BacklightModeE.BACKLIGHT_MODE_ON;

         switch (backlight)
         {
            case TBacklight.BACKLIGHT_AUTO:
               oldBacklight = BacklightModeE.BACKLIGHT_MODE_AUTO;
               break;
            case TBacklight.BACKLIGHT_OFF:
               oldBacklight = BacklightModeE.BACKLIGHT_MODE_OFF;
               break;
            case TBacklight.BACKLIGHT_ON:
               oldBacklight = BacklightModeE.BACKLIGHT_MODE_ON;
               break;
         }
         return oldBacklight;
      }

      public static WeightUnitsE Map(this TUnits tUnits)
      {
         var weightUnits = WeightUnitsE.WEIGHT_UNITS_KG;

         switch (tUnits)
         {
            case TUnits.UNITS_KG:
               weightUnits = WeightUnitsE.WEIGHT_UNITS_KG;
               break;
            case TUnits.UNITS_LB:
               weightUnits = WeightUnitsE.WEIGHT_UNITS_LB;
               break;
         }
         return weightUnits;
      }

      #endregion

      #region Helpers

      private static bool CheckTFlockHeader(TFlockHeader header)
      {
         return header.Number < 10 && !(header.Number == 0 && header.WeighFrom == 0 && header.WeighTill == 0);
      }

      #endregion
   }
}