using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;

namespace Bat2Library.Bat2Old.Flash
{
   // Zahlavi hejna
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TFlockHeader
   {
      public byte Number; // Cislo hejna - pokud je hejno definovane, je rovno indexu hejna (0 - 9)

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.FLOCK_NAME_MAX_LENGTH)] public byte[] Title;
         // Nazev hejna (bez ukoncovaci nuly, prazdne znaky se doplni mezerami)

      public byte UseCurves;
         // Zda se maji pouzivat rustove krivky nebo automaticke zjistovani cilove hmotnosti bez pouziti rustovych krivek

      // Pokud krivky nepouziva, jsou zadane pocatecni hmotnosti ulozene jako 1. bod ve krivce
      public byte UseBothGenders; // Flag, zda se ma pouzivat rozliseni na samce a samice

      public byte WeighFrom;
         // Umoznuje omezit ukladani az od zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se

      public byte WeighTill;
         // Umoznuje omezit ukladani pouze do zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se
   } // 13 bajtu
}