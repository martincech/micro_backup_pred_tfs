﻿namespace Bat2Library.Bat2Old.Flash
{
   public enum TJumpMode : byte
   {
      JUMPMODE_ENTER, // Vyhodnocuje se pouze naskok na vahu
      JUMPMODE_LEAVE, // Vyhodnocuje se pouze seskok z vahy
      JUMPMODE_BOTH, // Vyhodnocuje se naskok i seskok z vahy
      _JUMPMODE_COUNT
   }
}