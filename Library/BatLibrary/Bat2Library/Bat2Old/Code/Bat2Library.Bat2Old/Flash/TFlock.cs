using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;

namespace Bat2Library.Bat2Old.Flash
{
   // Struktura jednoho hejna
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TFlock
   {
      public TFlockHeader Header;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.CURVE_MAX_POINTS*TConstants.GENDER_COUNT)] public
         TCurvePoint[] GrowthCurve; // Rustova krivka pro samice (pripadne oboje) a pro samce

      public void Swap()
      {
         for (var i = 0; i < GrowthCurve.Length; i++)
         {
            GrowthCurve[i].Swap();
         }
      }
   } // 253 bajtu
}