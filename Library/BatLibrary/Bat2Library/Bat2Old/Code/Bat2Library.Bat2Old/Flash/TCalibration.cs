using System.Runtime.InteropServices;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   //-----------------------------------------------------------------------------
   // Kalibrace
   //-----------------------------------------------------------------------------

   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TCalibration
   {
      public int ZeroCalibration; // Kalibrace nuly
      public int RangeCalibration; // Kalibrace rozsahu
      public int Range; // Rozsah 0..NOSNOST_NASLAPNYCH_VAH
      public byte Division; // Dilek vah

      public void Swap()
      {
         ZeroCalibration = Endian.SwapInt32(ZeroCalibration);
         RangeCalibration = Endian.SwapInt32(RangeCalibration);
         Range = Endian.SwapInt32(Range);
      }
   } // 13 bajtu
}