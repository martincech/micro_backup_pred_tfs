using System.Runtime.InteropServices;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   // Popisovac statistiky :
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TStatistic
   {
      public System.Single XSuma;
      public System.Single X2Suma;
      public System.UInt16 Count;

      public void Swap()
      {
         XSuma = Endian.SwapFloat(XSuma);
         X2Suma = Endian.SwapFloat(X2Suma);
         Count = Endian.SwapUInt16(Count);
      }
   } // 10 bajtu
}