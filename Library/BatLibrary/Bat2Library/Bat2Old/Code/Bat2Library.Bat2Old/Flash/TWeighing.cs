using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   // Struktura s provoznimi promennymi pro aktualni den vykrmu - tato je v RAM, jsou zde jiz predem predpocitane veci (meze z rustove krivky atd.) a dalsi
   // veci, ktere jsou stale potreba a v prubehu dne se nemeni, napr. pro zobrazeni. Je to jakasi cache dat z EEPROM, do RAM se to nacte vzdy po nejake
   // zmene v EEPROM, pri prechodu na dalsi den atd.
   // Meze musi byt long, kdyby zadal napr. 65.535kg +- 10%, tak horni mez se nevejde do 2 bajtu
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TWeighing
   {
      public TFlockHeader Header;
      public System.UInt16 DayNumber; // Cislo dne od pocatku vykrmu

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public System.UInt16[] TargetWeight;
         // Normovana hmotnost nactena z rustove krivky pro samice a samce

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public System.Single[] MarginAbove;
         // Horni mez hmotnosti samic a samcu

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public System.Single[] MarginBelow;
         // Dolni mez hmotnosti samic a samcu

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public System.UInt16[] ComparisonWeight;
         // Hmotnost pro porovnani nactena z rustove porovnavaci krivky pro samice a samce

      public void Swap()
      {
         DayNumber = Endian.SwapUInt16(DayNumber);
         for (var i = 0; i < TConstants.GENDER_COUNT; i++)
         {
            TargetWeight[i] = Endian.SwapUInt16(TargetWeight[i]);
            MarginAbove[i] = Endian.SwapFloat(MarginAbove[i]);
            MarginBelow[i] = Endian.SwapFloat(MarginBelow[i]);
            ComparisonWeight[i] = Endian.SwapUInt16(ComparisonWeight[i]);
         }
      }
   } // 273 bajtu
}