using System.Runtime.InteropServices;
using Bat2Library.Connection.Interface.Domain.Old;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   // Parametry RS-485
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TRs485
   {
      public byte Address; // Adresa vahy v siti RS-485
      public System.UInt32 Speed; // Rychlost komunikace v Baudech za sekundu
      public TParity Parity; // Typ parity

      public System.UInt16 ReplyDelay;
         // Zpozdeni mezi prijmem prikazu a odpovedi v milisekundach, kvantovani s TIMER0_PERIOD

      public byte SilentInterval; // Mezera mezi dvema pakety MODBUS v milisekundach
      public TProtocol Protocol; // Typ komunikacniho protokolu

      public void Swap()
      {
         Speed = Endian.SwapUInt32(Speed);
         ReplyDelay = Endian.SwapUInt16(ReplyDelay);
      }
   } // 10 bajtu
}