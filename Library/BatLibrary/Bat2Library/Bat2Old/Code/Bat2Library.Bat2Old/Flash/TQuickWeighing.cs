using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   //-----------------------------------------------------------------------------
   // Konfiguracni data
   //-----------------------------------------------------------------------------

   // Rychle zadani parametru vykrmu rovnou pri startu, bez pouziti nektereho preddefinovaneho hejna.
   // Pri pouziti hejna se tyto parametry berou primo z hejna.
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TQuickWeighing
   {
      public byte UseBothGenders; // Flag, zda se ma pouzivat rozliseni na samce a samice

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public System.UInt16[] InitialWeight;
         // Zadana normovana hmotnost pro pocatecni den vykrmu pro samice a samce

      public void Swap()
      {
         for (var i = 0; i < TConstants.GENDER_COUNT; i++)
         {
            InitialWeight[i] = Endian.SwapUInt16(InitialWeight[i]);
         }
      }
   } // 5 bajtu
}