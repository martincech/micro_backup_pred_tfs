using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;
using Bat2Library.Connection.Interface.Domain.Old;
using Utilities;

namespace Bat2Library.Bat2Old.Flash.v111
{
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TConfig
   {
      // Interni data, pro ulozeni v EEPROM
      public System.UInt16 Version; // VERSION
      public byte Build; // BUILD
      public THwVersion HwVersion; // Verze hardware (GSM, Dacs LW1, Modbus, ...)
      public System.UInt16 IdentificationNumber; // identifikace zarizeni
      public byte Language; // Jazykova verze + jazyk

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public byte[] MarginAbove;
         // Okoli nad prumerem pro samice a samce

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GENDER_COUNT)] public byte[] MarginBelow;
         // Okoli pod prumerem pro samice a samce

      public byte Filter; // Filtr prevodniku
      public byte StabilizationRange; // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
      public byte StabilizationTime; // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)

      public TAutoMode AutoMode;
         // Typ rezimu automatickeho hledani cilove hmotnosti (bez nebo s pouzitim denniho prirustku)

      public TJumpMode JumpMode; // Typ naskoku/seskoku na vahu, ktery se vyhodnocuje

      public TUnits Units; // Zobrazovane jednotky

      public byte HistogramRange; // Rozsah histogramu v +- % stredni hodnoty
      public byte UniformityRange; // Rozsah uniformity v +- %

      public TWeighingStart WeighingStart; // Parametry vazeni

      public TGsm Gsm; // Parametry GSM modulu

      public TBacklight Backlight; // Nastaveny rezim podsvitu
      public byte Battery; // Vaha je napajena z baterie - zatim se nepouziva

      public TRs485 Rs485; // Parametry linky RS-485

      public byte ComparisonFlock;
         // Cislo hejna, se kterym se porovnavaji vysledky, pripadne FLOCK_EMPTY_NUMBER pokud se neporovnava

      public TWeightCorrection WeightCorrection; // Korekce hmotnosti

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.CONFIG_RESERVED_SIZE_V111)] public byte[] Reserved;
         // Pro budouci pouziti

      public byte Checksum; // Kontrolni soucet

      public void Swap()
      {
         Version = Endian.SwapUInt16(Version);
         IdentificationNumber = Endian.SwapUInt16(IdentificationNumber);
         WeighingStart.Swap();
         Rs485.Swap();
         WeightCorrection.Swap();
      }
   } // 150 bajtu, pri provedeni zmen zkontrolovat fci CfgSaveConfigFromModule()
}