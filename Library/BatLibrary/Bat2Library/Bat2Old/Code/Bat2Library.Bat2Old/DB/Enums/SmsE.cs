﻿namespace Bat2Library.Bat2Old.DB.Enums
{
   public enum SmsE
   {
      GSM_NUMBER,
      INSERT_DATE_TIME,
      EDIT_DATE_TIME,
      ID,
      DAY_NUMBER,
      DAY_DATE,
      FEMALE_COUNT,
      FEMALE_AVERAGE,
      FEMALE_GAIN,
      FEMALE_SIGMA,
      FEMALE_CV,
      FEMALE_UNI,
      MALE_COUNT,
      MALE_AVERAGE,
      MALE_GAIN,
      MALE_SIGMA,
      MALE_CV,
      MALE_UNI,
      NOTE
   }
}
