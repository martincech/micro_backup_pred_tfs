﻿using System;

namespace Bat2Library.Bat2Old.DB
{
   public class Filter
   {
      public string Name { get; set; }
      public short Id { get; set; }
      public DateTime DateFrom { get; set; }
      public DateTime DateTill { get; set; }
      public string Note { get; set; }
   }
}
