﻿using System.Collections.Generic;

namespace Bat2Library.Bat2Old.DB
{
   public class Flock
   {
      public Flock()
      {
         FemaleRecords = new Dictionary<int, double>();
         MaleRecords = new Dictionary<int, double>();
      }

      public short Number { get; set; }
      public string Name { get; set; }
      public bool UseCurves { get; set; }
      public bool UseGender { get; set; }
      public short WeighFrom { get; set; }
      public short WeighTo { get; set; }

      /// <summary>
      /// Key - FemaleDay00 - FemaleDay29
      /// Value - FemaleWeight00 - FemaleWeight29
      /// </summary>
      public IDictionary<int, double> FemaleRecords { get; set; }

      /// <summary>
      /// Key - MaleDay00 - MaleDay29
      /// Value - MaleWeight00 - MaleWeight29
      /// </summary>
      public IDictionary<int, double> MaleRecords { get; set; }
   }
}
