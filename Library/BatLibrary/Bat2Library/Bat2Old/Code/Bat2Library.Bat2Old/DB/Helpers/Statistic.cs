﻿namespace Bat2Library.Bat2Old.DB.Helpers
{
   public class Statistic
   {
      public short Count { get; set; }
      public double Average { get; set; }
      public double Gain { get; set; }
      public double Sigma { get; set; }
      public short Cv { get; set; }
      public short Uni { get; set; }
   }
}
