﻿namespace Bat2Library.Bat2Old.DB
{
   public class Online
   {
      public short DayNumber { get; set; }
      public short TimeHour { get; set; }
      public double Weight { get; set; }
      public bool Saved { get; set; }
      public bool Stable { get; set; }
   }
}
