//*****************************************************************************
//
//    uStorageDef.h  Persistent storage description
//    Version 1.0    (c) VymOs
//
//*****************************************************************************

#ifndef __uStorageDef_H__
   #define __uStorageDef_H__

/*
#ifndef __Hardware_H__
   #include "Hardware.h"
#endif
*/
#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __File_H__
   #include "Memory/File.h"
#endif

//------------------------------------------------------------------------------
//  Persistent storage descriptor
//------------------------------------------------------------------------------

// persistent item descriptor :
typedef struct {
   void       *Data;                   // persistent data RAM buffer
   const void *DefaultData;            // persistent data FLASH defaults
   word        Size;                   // persistent data size
} UStorageItem;

// persistent item descriptor index :
typedef word UStorageIndex;
typedef dword UStorageAddress;

typedef struct {
   const byte Count;
   const UStorageItem Items[];
} UStorageDescriptor;

typedef struct {
   UStorageDescriptor *Descriptor;
   TFile File;
} UStorage;

// persistent storage definition :
#define uStorageStart( Name, Cnt)     const UStorageDescriptor Name = { Cnt, {
#define uStorageItem( Data)               uStorageDefine( Data, Data##Default, sizeof( *(Data)))
#define uStorageData( Data)               uStorageDefine( Data, 0, sizeof( *(Data)))
#define uStorageSpare( Size)              uStorageDefine( 0, 0, Size)
#define uStorageEnd()                  }};

#define uStorageDefine( Data, DefaultData, Size)  { Data, DefaultData, Size}

//------------------------------------------------------------------------------
//  Persistent storage frame
//------------------------------------------------------------------------------

#define USTORAGE_MAGIC      0xAA55     // storage magic identification
#define USTORAGE_MAGIC_END  0xCC33     // storage magic end indentification

// frame header :
typedef struct {
   word  Magic;
   word  Version;
   dword Size;
} UStorageHeader;

// frame CRC :
typedef word UStorageCrc;

// frame footer :
typedef struct {
   word        Magic;
   UStorageCrc Crc;
} UStorageFooter;

// fake frame definition :
typedef struct {
   UStorageHeader Header;
   byte           Data[ 1];
   UStorageFooter Footer;
} UStorageFrame;

// storage frame addresses :
#define uStorageAddress( Storage)                (0)
#define uStorageHeaderAddress( Storage)           uStorageAddress( Storage->Descriptor)
#define uStorageDataAddress( Storage)            (uStorageHeaderAddress( Storage->Descriptor) + offsetof( UStorageFrame, Data))
// storage frame addresses by total frame <Size> :
#define uStorageFooterAddress( Storage, Size)    (uStorageHeaderAddress( Storage->Descriptor) + (Size) - sizeof( UStorageFooter))
#define uStorageMagicEndAddress( Storage, Size)  (uStorageFooterAddress( Storage->Descriptor, Size) + offsetof( UStorageFooter, Magic))
#define uStorageCrcAddress( Storage, Size)       (uStorageFooterAddress( Storage->Descriptor, Size) + offsetof( UStorageFooter, Crc))
// total frame size with <DataSize> :
#define uStorageSize( DataSize)         (sizeof( UStorageHeader) + (DataSize) + sizeof( UStorageFooter))

#endif
