//*****************************************************************************
//
//    uNamedListDef.h  Named list data description
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __uNamedListDef_H__
   #define __uNamedListDef_H__

#ifndef __uDirectoryDef_H__
   #include "Data/uDirectoryDef.h"
#endif

//------------------------------------------------------------------------------
//  Named list layout
//------------------------------------------------------------------------------

typedef struct {
   UDirectoryLayout Directory;         // directory layout data
// byte             Data[];            // list items
} UNamedListLayout;

//------------------------------------------------------------------------------
//  Named list
//------------------------------------------------------------------------------

typedef UDirectory           UNamedList;
typedef UDirectoryDescriptor UNamedListDescriptor;
typedef UDirectoryIdentifier UNamedListIdentifier;

#endif
