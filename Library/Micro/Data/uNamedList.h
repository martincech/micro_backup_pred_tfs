//*****************************************************************************
//
//    uNamedList.h  Named list utility
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __uNamedList_H__
   #define __uNamedList_H__

#ifndef __uNamedListDef_H__
   #include "Data/uNamedListDef.h"
#endif

#ifndef __uDirectory_H__
   #include "Data/uDirectory.h"
#endif

// Other operations : see uDirectory.h

//------------------------------------------------------------------------------
//  Declaration
//------------------------------------------------------------------------------

#define uNamedListAlloc( Name, Capacity, ItemSize)\
                         uDirectoryAlloc( Name, Capacity, ItemSize)

#define uNamedListSize( Capacity, ItemSize) \
                        (uDirectorySize( Capacity) + (Capacity) * (ItemSize))

#define uNamedListDirectory( List)   ((UDirectory *)(List))

#ifdef __cplusplus
   extern "C" {
#endif

TYesNo uNamedListRemCopy(UNamedListDescriptor *Descriptor, TFileName FilenameDest, TFileName FilenameSrc);
// Load from remote device

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void uNamedListInit( const UNamedListDescriptor *Descriptorm, TFileName FileName);
// Initialize <List>

TYesNo uNamedListOpen( UNamedList *List, const UNamedListDescriptor *Descriptor, TFileName FileName);
// Open

void uNamedListClose( UNamedList *List);
// Close

UDirectoryIndex uNamedListAdd( UNamedList *List, const char *Name, const void *Data);
// Add item with <Name> and <Data> to <List>

UDirectoryIndex uNamedListCopy( UNamedList *List, const char *NewName, UDirectoryIndex FromIndex);
// Copy item <FromIndex> with a <NewName> to <List>

void uNamedListDelete( UNamedList *List, UDirectoryIndex Index);
// Delete item at <Index> from <List>

#define uNamedListRename( List, Index, NewName) uDirectoryRename((UDirectory *) uNamedListDirectory( List), Index, NewName)
// Rename item at <Index> with <NewName>

#define uNamedListCount( List)                  uDirectoryCount((UDirectory *) uNamedListDirectory( List))
// Returns list items count

//------------------------------------------------------------------------------

void uNamedListLoad( UNamedList *List, UNamedListIdentifier Identifier, void *Data);
// Load <Data> from <List> by <Identifier>

void uNamedListSave( UNamedList *List, UNamedListIdentifier Identifier, const void *Data);
// Save <Data> with <Identifier> to <List>

TYesNo uNamedListChanged( UNamedList *List, UNamedListIdentifier Identifier, const void *Data);
// Returns YES if  <Data> don't match <List> <Identifier> item

#ifdef __cplusplus
   }
#endif

#endif
