//*****************************************************************************
//
//    uStorage.c    Persistent data storage with NVM
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Data/uStorage.h"
#include "Memory/Nvm.h"
#include <string.h>
#include "Storage.h"           // project storage definiton
#include "Memory/File.h"

// Local functions :
static dword _StorageSize( const UStorageDescriptor *Descriptor);
// returns storage total size

static dword _StorageAddress( const UStorageDescriptor *Descriptor, UStorageIndex ItemIndex);
// returns storage address of item at <ItemIndex>

static void _StorageCrcUpdate( UStorage *Storage);
// update storage CRC

static TYesNo _uStorageOpen( UStorage *Storage, const UStorageDescriptor *Descriptor, TFileName Filename, TFileMode FileMode);
// Open

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo uStorageCopy( const UStorageDescriptor *Descriptor, TFileName FilenameDest, TFileName FilenameSrc)
// Load from remote device
{
UStorage StorageDestInt;
UStorage *StorageDest = &StorageDestInt;
UStorage StorageSrcInt;
UStorage *StorageSrc = &StorageSrcInt;
TFileAddress Address;
dword Size;

   StorageSrc->Descriptor = (UStorageDescriptor *)Descriptor;
   StorageDest->Descriptor = (UStorageDescriptor *)Descriptor;

   Address = 0;
   Size = _StorageSize( StorageDest->Descriptor);

   if(!FileOpen(&StorageSrc->File, FilenameSrc, FILE_MODE_READ_ONLY)) {
      return NO;
   }

   if(FileIsOffline(&StorageSrc->File)) {
      FileLoad(&StorageSrc->File, Address, 0, Size);
      FileClose(&StorageSrc->File);
      return YES;
   }

   if(!uStorageCheck( StorageSrc)) {
      FileClose( &StorageSrc->File);
      return NO;
   }

   if(!FileOpen(&StorageDest->File, FilenameDest, FILE_MODE_WRITE_ONLY | FILE_MODE_SANDBOX)) {
      FileClose( &StorageSrc->File);
      return NO;
   }

   if(!FileClone( &StorageDest->File, &StorageSrc->File, Address, Size)) {
      FileClose( &StorageSrc->File);
      FileClose( &StorageDest->File);
      return NO;
   }
   FileCommit( &StorageDest->File);
   FileClose( &StorageSrc->File);
   FileClose( &StorageDest->File);
   return YES;
} // uStorageRemoteLoad

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void uStorageInit( const UStorageDescriptor *Descriptor, TFileName FileName)
// Initialize <Storage>
{
} // uStorageInit

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo uStorageOpen( UStorage *Storage, const UStorageDescriptor *Descriptor, TFileName FileName)
// Open
{
   if(!FileOpen(&Storage->File, FileName, FILE_MODE_READ_WRITE)) {
      return NO;
   }
   Storage->Descriptor = (UStorageDescriptor *)Descriptor;
   return YES;
} // uStorageOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void uStorageClose( UStorage *Storage)
// Close
{
   FileClose( &Storage->File);
} // uStorageClose

//------------------------------------------------------------------------------
//  Check
//------------------------------------------------------------------------------

TYesNo uStorageCheck( UStorage *Storage)
// Check persistent data integrity
{
UStorageHeader Header;
UStorageCrc    Crc;
UStorageCrc    NvmCrc;
dword          Size;
word           Magic;
   // check for header data :
   FileLoad( &Storage->File, uStorageHeaderAddress( Storage), &Header, sizeof( UStorageHeader));
   if( Header.Magic != USTORAGE_MAGIC){
      return( NO);
   }
   if( Header.Version != USTORAGE_VERSION){
      return( NO);
   }
   Size = _StorageSize( Storage->Descriptor);
   if( Header.Size != Size){
      return( NO);
   }
   // check for CRC :
   Crc = (UStorageCrc)FileSum( &Storage->File, uStorageHeaderAddress( Storage), Size - sizeof( UStorageFooter));
   Crc = ~Crc;
   NvmCrc = FileWordRead( &Storage->File, uStorageCrcAddress( Storage, Size));
   if( Crc != NvmCrc){
      return( NO);
   }
   // check for magic end :
   Magic = FileWordRead( &Storage->File, uStorageMagicEndAddress( Storage, Size));
   if( Magic != USTORAGE_MAGIC_END){
      return( NO);
   }
   return( YES);
} // uStorageCheck

//------------------------------------------------------------------------------
//  Restore
//------------------------------------------------------------------------------

void uStorageRestore( UStorage *Storage)
// restores persistent data
{
/*TNvmAddress         Address;
   Address    = uStorageAddress( Storage);
   NvmRollback( Address);*/
} // uStorageRestore

//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

TYesNo uStorageLoad( UStorage *Storage)
// Load all persistent data
{
int                 i;
const UStorageItem *Item;
TNvmAddress         Address;

   Address    = uStorageDataAddress( Storage);
   for( i = 0; i < Storage->Descriptor->Count; i++){
      Item = &Storage->Descriptor->Items[ i];
      if( Item->Data){
         FileLoad( &Storage->File, Address, Item->Data, Item->Size);
      } // else no data buffer
      Address += Item->Size;
   }
   return( YES);
} // uStorageLoad

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

TYesNo uStorageSave( UStorage *Storage)
// Save all persistent data
{
int                 i;
const UStorageItem *Item;
TNvmAddress         Address;
UStorageHeader      Header;
// Rem : always save all - dont' check for data equality

   // save header data :
   Header.Magic   = USTORAGE_MAGIC;
   Header.Version = USTORAGE_VERSION;
   Header.Size = _StorageSize( Storage->Descriptor);
   FileSave( &Storage->File, uStorageHeaderAddress( Storage), &Header, sizeof( UStorageHeader));
   // save persistent data :
   Address    = uStorageDataAddress( Storage);
   for( i = 0; i < Storage->Descriptor->Count; i++){
      Item = &Storage->Descriptor->Items[ i];
      if( Item->Data){
         FileSave( &Storage->File, Address, Item->Data, Item->Size);
      } // else no data buffer
      Address += Item->Size;
   }
   _StorageCrcUpdate( Storage);
   //NvmCommit();                        // permanently save - flush cached data
   return( YES);
} // uStorageSave

//------------------------------------------------------------------------------
//  Defaults
//------------------------------------------------------------------------------

void uStorageDefault( const UStorageDescriptor *Descriptor)
// Initialize all persistent data with defaults
{
int                 i;
const UStorageItem *Item;
TNvmAddress         Address;

   Address    = uStorageDataAddress( Storage);
   for( i = 0; i < Descriptor->Count; i++){
      Item = &Descriptor->Items[ i];
      if( Item->Data){
         if( Item->DefaultData){
            memcpy( Item->Data, Item->DefaultData, Item->Size);
         } else {
            memset( Item->Data, 0, Item->Size);
         }
      } // else no data buffer
      Address += Item->Size;
   }
} // uStorageDefault

//------------------------------------------------------------------------------
//  Load item
//------------------------------------------------------------------------------

TYesNo uStorageItemLoad( UStorage *Storage, UStorageIndex Index)
// Load persistent data at item <Index>
{
TNvmAddress         Address;
const UStorageItem *Item;

   Address = _StorageAddress( Storage->Descriptor, Index);
   Item    = &Storage->Descriptor->Items[ Index];
   if( !Item->Data){
      return( NO);
   }
   FileLoad( &Storage->File, Address, Item->Data, Item->Size);
   return( YES);
} // uStorageItemLoad

//------------------------------------------------------------------------------
//  Save item
//------------------------------------------------------------------------------

TYesNo uStorageItemSave( UStorage *Storage, UStorageIndex Index)
// Save persistent data at item <Index>
{
TNvmAddress         Address;
const UStorageItem *Item;

   Address = _StorageAddress( Storage->Descriptor, Index);
   Item    = &Storage->Descriptor->Items[ Index];
   if( !Item->Data){
      return( NO);
   }
   // check for data equality :
   if( FileMatch( &Storage->File, Address, Item->Data, Item->Size)){
       return( YES);
   }
   // nonvolatile save :
   FileSave( &Storage->File, Address, Item->Data, Item->Size);
   _StorageCrcUpdate( Storage);
   return( YES);
} // uStorageItemSave

//------------------------------------------------------------------------------
//  Item defaults
//------------------------------------------------------------------------------

void uStorageItemDefault( const UStorageDescriptor *Descriptor, UStorageIndex Index)
// Initialize persistent data at item <Index> with defaults
{
TNvmAddress         Address;
const UStorageItem *Item;

   Address = _StorageAddress( Descriptor, Index);
   Item    = &Descriptor->Items[ Index];
   if( !Item->Data){
      return;
   }
   if( Item->DefaultData){
      memcpy( Item->Data, Item->DefaultData, Item->Size);
   } else {
      memset( Item->Data, 0, Item->Size);
   }
} // uStorageItemDefault

//------------------------------------------------------------------------------
//  Item changed
//------------------------------------------------------------------------------

TYesNo uStorageItemChanged( UStorage *Storage, UStorageIndex Index)
// Returns YES if persistent data don't match actual data
{
TNvmAddress         Address;
const UStorageItem *Item;

   Address = _StorageAddress( Storage->Descriptor, Index);
   Item    = &Storage->Descriptor->Items[ Index];
   if( !Item->Data){
      return( NO);
   }
   // check for data equality :
   return( !FileMatch( &Storage->File, Address, Item->Data, Item->Size));
} // uStorageItemChanged

//******************************************************************************

//------------------------------------------------------------------------------
//  Size
//------------------------------------------------------------------------------

static dword _StorageSize( const UStorageDescriptor *Descriptor)
// returns storage total size
{
int                 i;
const UStorageItem *Item;
dword               DataSize;

   DataSize = 0;
   for( i = 0; i < Descriptor->Count; i++){
      Item = &Descriptor->Items[ i];
      DataSize += Item->Size;
   }
   return( uStorageSize( DataSize));      // add header & footer
} // _StorageSize

//------------------------------------------------------------------------------
//  Data size
//------------------------------------------------------------------------------

static dword _StorageAddress( const UStorageDescriptor *Descriptor, UStorageIndex ItemIndex)
// returns storage address of item at <ItemIndex>
{
int                 i;
const UStorageItem *Item;
TNvmAddress         Address;

   Address = uStorageDataAddress( Descriptor);
   for( i = 0; i < ItemIndex; i++){
      Item = &Descriptor->Items[ i];
      Address += Item->Size;
   }
   return( Address);
} // _StorageAddress

//------------------------------------------------------------------------------
//  Data size
//------------------------------------------------------------------------------

static void _StorageCrcUpdate( UStorage *Storage)
// update storage CRC
{
dword          Size;
UStorageFooter Footer;

   Size         = _StorageSize( Storage->Descriptor);
   Footer.Magic = USTORAGE_MAGIC_END;
   Footer.Crc   = ~(UStorageCrc)FileSum( &Storage->File, uStorageHeaderAddress( Storage), Size - sizeof( UStorageFooter));
   FileSave( &Storage->File, uStorageFooterAddress( Storage, Size), &Footer, sizeof( UStorageFooter));
} // _StorageCrcUpdate
