//*****************************************************************************
//
//    User.c        User interface for Unity
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "User.h"
#include "unity_fixture.h"
#include <stdio.h>

//-----------------------------------------------------------------------------
// Confirm
//-----------------------------------------------------------------------------

void UserConfirm( void)
// Prompts user to confirm previous action
{
   puts("Confirm");

   if(getchar() == '\n') {
      return;
   }

   while(getchar() != '\n');
   TEST_FAIL_MESSAGE("Failed by user!");
}

//-----------------------------------------------------------------------------
// Continue
//-----------------------------------------------------------------------------

void UserContinue( const char *Message)
// Continue when user press anything + \n
{
   puts(Message);
   while(getchar() != '\n');
}