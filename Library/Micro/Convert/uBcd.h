//*****************************************************************************
//
//    uBcd.h       BCD utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __uBcd_H__
   #define __uBcd_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//-----------------------------------------------------------------------------
// BCD digits
//-----------------------------------------------------------------------------

byte uBcdDigitGet( dword Value, int Order);
// Returns BCD digit at <Order>

dword uBcdDigitSet( dword Value, int Order, byte Digit);
// Sets BCD <Digit> at <Order>

int uBinaryWidth( dword Number);
// Returns digits count of the binary <Number>

int uBcdWidth( dword Number);
// Returns digits count of the BCD/hexadecimal <Number>

//-----------------------------------------------------------------------------
// Binary to BCD
//-----------------------------------------------------------------------------

dword uBinaryToBcd( dword x);
// Converts binary 0..99 999 999 to BCD

//-----------------------------------------------------------------------------
// BCD to Binary
//-----------------------------------------------------------------------------

dword uBcdToBinary( dword x);
// Converts 0..99 999 999 to binary

//-----------------------------------------------------------------------------
// Nibble operations
//-----------------------------------------------------------------------------

#define uLowNibble( x)       ((byte)(x) & 0x0F)
#define uHighNibble( x)      ((byte)(x) >> 4)

//-----------------------------------------------------------------------------
// To character
//-----------------------------------------------------------------------------

#define uNibbleToDec( x)     uNibbleToHex( x)
// Converts low nibble to '0'..'9'

char uNibbleToHex( byte x);
// Converts low nibble to '0'..'F'

//-----------------------------------------------------------------------------
// To digit
//-----------------------------------------------------------------------------

#define uCharToDec( ch)      ((ch) - '0')
// Converts ASCII '0'..'9' to digit

byte uCharToHex( char ch);
// Converts ASCII '0'..'9' and 'A'..'F' to digit

#endif
