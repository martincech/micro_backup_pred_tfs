//*****************************************************************************
//
//    uEndian.h    Endian conversions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __uEndian_H__
   #define __uEndian_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define uEndianByteSwap( Value)       (Value)
// Swap byte <Value> (as placeholder only)

word uEndianWordSwap( word Value);
// Swap word <Value>

dword uEndianDwordSwap( dword Value);
// Swap dword <Value>

#endif
