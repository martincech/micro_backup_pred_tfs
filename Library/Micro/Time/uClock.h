//******************************************************************************
//
//   uClock.h     Date & Time with daylight saving
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __uClock_H__
   #ifndef _MANAGED
   #define __uClock_H__
   #endif

#ifndef __uDateTime_H__
   #include "Time/uDateTime.h"
#endif

#ifndef __uPersistent_H__
   #include "Data/uPersistent.h"
#endif

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

// Daylight saving :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class DaylightSavingE{
#else
typedef enum {
#endif
   DAYLIGHT_SAVING_OFF,           // no daylight saving
   DAYLIGHT_SAVING_EU,            // european daylight saving
   DAYLIGHT_SAVING_US,            // US daylight saving
#ifndef _MANAGED
   _DAYLIGHT_SAVING_COUNT
} EDaylightSaving;
#else
   };
}
#endif

#ifndef _MANAGED
//------------------------------------------------------------------------------
// Data types
//------------------------------------------------------------------------------

// Linear date & time representation (LSB = 1s) with daylight saving
typedef int32 UClockGauge;

// Rem : internal representation is UDateTimeGauge always as winter time

//------------------------------------------------------------------------------
// Persistent configuration
//------------------------------------------------------------------------------

#ifdef UCLOCK_PARENT // embedded
   uPersistentChild( UCLOCK_PARENT, ClockDaylightSavingType, byte);
   #define uClockDaylightSavingTypeSet( DstType) uPersistentChildSet( UCLOCK_PARENT, DaylightSavingType, DstType)
   #define uClockDaylightSavingType()            uPersistentChildGet( UCLOCK_PARENT, DaylightSavingType)
#else // singleton
   uPersistent( ClockDaylightSavingType, byte);
   #define uClockDaylightSavingTypeSet( DstType) uPersistentSet( ClockDaylightSavingType, DstType)
   #define uClockDaylightSavingType()            uPersistentGet( ClockDaylightSavingType)
#endif


#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

UClockGauge uClockGauge( UDateTime *DateTime);
// Encode <DateTime> to internal representation

void uClock(UDateTime *DateTime, UClockGauge ClockGauge);
// Decode <DateTime> from <ClockGauge>


//------------------------------------------------------------------------------
// Calculations
//------------------------------------------------------------------------------

UDateTimeGauge uClockDateTime( UClockGauge ClockGauge);
// Returns DateTimeGauge of <ClockGauge>

UClockGauge uDateTimeClock( UDateTimeGauge DateTimeGauge);
// Returns ClockGauge of <DateTimeGauge>

UDow uClockDow( UClockGauge ClockGauge);
// Returns day of week of <ClockGauge>

TYesNo uClockDaylightSaving( UDateTime *DateTime);
// Returns YES if the <DateTime> is in summertime

#ifdef __cplusplus
}
#endif

#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "uClock.h"
   #define _MANAGED
#endif

#endif // __uClock_H__

