//******************************************************************************
//
//    Delay.c      System delay loops
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "Cpu/core_cm4.h"
#include "UniSys/Uni.h"

#if F_CPU < 1000000
   #error Too slow F_CPU
#endif

#define PIPELINE_REFILL_CPU_CYCLES           1    // Branch instruction execution time varies
#define CPU_CYCLES_PER_LOOP_ITERATION_FIXED  ((1) + (1 + PIPELINE_REFILL_CPU_CYCLES)) // = (subs) + (b/bne including pipeliny refill cycles)
#define CPU_CYCLES_PER_US                    (F_CPU / 1000000)
#define CPU_CYCLES_PER_MS                    (F_CPU / 1000)

//------------------------------------------------------------------------------
//   Delay ms
//------------------------------------------------------------------------------

void SysDelay( word ms)
// Milisecond delay
{
   #define INNER_LOOP_CYCLES  (CPU_CYCLES_PER_MS / CPU_CYCLES_PER_LOOP_ITERATION_FIXED)
   dword InnerLoopCounter;

__asm__ __volatile__ (
   "MsLoop:\n\t"
      "cbz %[Ms],End\n\t"
      "mov %[InnerLoopCounter], %[InnerLoopCycles]\n\t"
   "InnerLoop:\n\t"
      "subs %[InnerLoopCounter], #1\n\t"
      "bne InnerLoop\n\t"
      "subs %[Ms], #1\n\t" 
      "b MsLoop\n\t"
   "End:\n\t" : : [InnerLoopCounter] "r" (InnerLoopCounter), [InnerLoopCycles] "r" (INNER_LOOP_CYCLES), [Ms] "r" (ms)
);
} // SysDelay

//------------------------------------------------------------------------------
//   Delay us
//------------------------------------------------------------------------------

void SysUDelay( word us)
// Microsecond delay
{
#if CPU_CYCLES_PER_US > CPU_CYCLES_PER_LOOP_ITERATION_FIXED
   #define NOP_COUNT (CPU_CYCLES_PER_US - CPU_CYCLES_PER_LOOP_ITERATION_FIXED)
#else 
   #define NOP_COUNT 0
#endif
   #undef INNER_LOOP_CYCLES
   #define INNER_LOOP_CYCLES  (CPU_CYCLES_PER_US / CPU_CYCLES_PER_LOOP_ITERATION_FIXED)
   dword InnerLoopCounter;
   dword InnerLoopCycles = INNER_LOOP_CYCLES;
   __asm__ __volatile__ (
      "UsLoop:\n\t"
#if NOP_COUNT < 10
// fill with nop
   #if (NOP_COUNT-0)
         "nop\n\t"
   #if (NOP_COUNT-1)
         "nop\n\t"
   #if (NOP_COUNT-2)
         "nop\n\t"
   #if (NOP_COUNT-3)
         "nop\n\t"
   #if (NOP_COUNT-4)
         "nop\n\t"
   #if (NOP_COUNT-5)
         "nop\n\t"
   #if (NOP_COUNT-6)
         "nop\n\t"
   #if (NOP_COUNT-7)
         "nop\n\t"
   #if (NOP_COUNT-8)
         "nop\n\t"
   #if (NOP_COUNT-9)
         "nop\n\t"
   #endif
   #endif
   #endif
   #endif
   #endif
   #endif
   #endif
   #endif
   #endif
   #endif
         "subs %[Us], #1\n\t" 
         "bne UsLoop\n\t"
#else
// use inner loop
       "cbz %[Us],EndUs\n\t"
       "mov %[InnerLoopCounterUs], %[InnerLoopCyclesUs]\n\t"
      "InnerLoopUs:\n\t"
         "subs %[InnerLoopCounterUs], #1\n\t"
         "bne InnerLoopUs\n\t"
         "subs %[Us], #1\n\t" 
         "b UsLoop\n\t"
#endif
      "EndUs:\n\t" : : [InnerLoopCounterUs] "r" (InnerLoopCounter), [InnerLoopCyclesUs] "r" (InnerLoopCycles), [Us] "r" (us)
   );
} // SysUDelay