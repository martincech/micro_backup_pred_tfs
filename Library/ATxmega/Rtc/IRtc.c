//*****************************************************************************
//
//    IRtc.c       AtXmega Real Time Clock
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Rtc/Rtc.h"
#include "Hardware.h"
#include "System/System.h"

#define PRESCALER    256
#define CLOCK        1000

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void RtcInit( void)
// Initialize bus and RTC
{
   sysclk_enable_module(SYSCLK_PORT_GEN, SYSCLK_RTC);
   CLK.RTCCTRL = CLK_RTCSRC_ULP_gc | CLK_RTCEN_bm;
   RTC.PER = RTC_PERIOD * CLOCK / PRESCALER / 1000 - 1;
   RTC.CNT = 0;
   RTC.INTCTRL = RTC_OVFINTLVL_LO_gc;
   RTC.CTRL = RTC_PRESCALER_DIV256_gc;
   do { } while (RTC.STATUS & RTC_SYNCBUSY_bm);
} // RtcInit