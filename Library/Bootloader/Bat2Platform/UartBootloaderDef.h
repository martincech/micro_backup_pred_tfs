//*****************************************************************************
//
//    UartBootloaderDef.h  Uart Bootloader
//    Version 1.0         (c) Veit Electronics
//
//*****************************************************************************

#ifndef __UartBootloaderDef_H__
   #define __UartBootloaderDef_H__
   
#include "Unisys/Uni.h"
#include "Device/VersionDef.h"

//#define BOOTLOADER_SW_VERSION   BootloaderVersion(1, 0)

typedef enum {
   BOOTLOADER_CMD_VERSION,
   BOOTLOADER_CMD_DOWNLOAD,
   BOOTLOADER_CMD_START_APP
} EBootloaderCmd;

typedef struct {
   byte Cmd;
} TBootloaderReportCmd;

//-----------------------------------------------------------------------------
// Replies by IN REPORTS
//-----------------------------------------------------------------------------

typedef union {
   TDeviceVersion Version;
} UBootloaderReportReply;

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef struct {
   byte Reply;
   UBootloaderReportReply Data;
} __packed TBootloaderReportReply;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif
#endif