//*****************************************************************************
//
//    Bootloader.h   Bootloader
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Bootloader_H__
   #define __Bootloader_H__

#include "Unisys/Uni.h"

typedef enum {
   BOOT_CMD_APP,
   BOOT_CMD_BOOTLOADER,
   BOOT_CMD_INVALID
} EBootloaderCmd;

typedef enum {
   BOOT_STATUS_NORMAL,
   BOOT_STATUS_ERROR_UPDATE,
} EBootStatus;

typedef struct {
  byte   Cmd;
  byte   Status;
} __packed TBootloaderData;

extern volatile TBootloaderData BootloaderData;

void BootloaderTrigger( void);
// Trigger bootloader







typedef enum {
   BOOTLOADER_IDLE,
   BOOTLOADER_BUSY,
   BOOTLOADER_DONE,
   BOOTLOADER_ERROR,
} EBootloaderStatus;

void BootloaderInit( void);
// Initialization

void BootloaderExecute( void);
// Execute

int BootloaderState( void);
// State


#endif