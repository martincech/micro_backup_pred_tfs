﻿using System;
using System.Linq;
using BatLibrary;
using Recognition.Types;

// ReSharper disable InconsistentNaming

namespace RecognitionTests
{
   public class TestData
   {
      public static readonly double[] StableSamplesG = { 54, 52, 54, 49, 53, 48, 52, 48, 51, 50, 50, 52, 51, 51.5, 52, 51.4, 55.5 };
      public static readonly double[] UnstableSamplesG = { 55, 45, 54, 46, 53, 47, 52, 48, 51, 105, 60, 52, 51, 11.5, 52 };
      public static readonly double[] SamplesG = { 55, 53, 54, 52, 53, 55, 56, 52, 51, 54, 55, 52, 51, 54,
                                                   150, 160, 151, 165, 152, 163, 168, 170, 155, 154, 155, 168, 170, 155, 154, 155,
                                                   255, 253, 254, 252, 253, 255, 256, 252, 251, 254, 255, 253, 254, 252, 253 };
      public static readonly double[] CalibrationSamplesG =
      {
         100.0, 101, 100, 100.4, 99.8, 102, 101.3, 100.7, 100, 100.2, 99.9, 101, 100.4, 100.5, 100.1,
         5.0, 5.8, 5.1, 6.1, 6, 5.1, 5.2, 5.1, 5, 5, 5.1, 5.5, 5.4, 5, 5.0
      };

      public static readonly Weight[] STABLE_SAMPLES = StableSamplesG.Select(s=>new Weight(s)).ToArray();
      public static readonly Weight[] UNSTABLE_SAMPLES = UnstableSamplesG.Select(s=>new Weight(s)).ToArray();
      public static readonly Weight[] SAMPLES = SamplesG.Select(s => new Weight(s)).ToArray();
      public static readonly Weight[] CALIBRATION_SAMPLES = CalibrationSamplesG.Select(s => new Weight(s)).ToArray();


      public static FilterWeight[] UnStablePreFilteredData(double stableRange)
      {
         return UnstableSamplesG.Select(s => new FilterWeight(s, NextDouble(random, stableRange + 10, stableRange + 50))).ToArray();
      }

      public static FilterWeight[] StablePreFilteredData(double stableRange)
      {
         var data = StableSamplesG.Select(s => new FilterWeight(s, NextDouble(random, 0, stableRange - 0.5))).ToArray();
         data[0] = new FilterWeight(StableSamplesG[0], 100);
         return data;
      }

      public static FilterWeight[] ExactData(double value)
      {
         const int dataCount = 15;
         var data = new FilterWeight[dataCount];
         data[0] = new FilterWeight(value, 100);
         for (var i = 1; i < dataCount; i++)
         {
            data[i] = new FilterWeight(value, 0.1);
         }
         return data;
      }

      #region Private

      private static readonly Random random = new Random();

      private static double NextDouble(Random rnd, double min, double max)
      {
         return min + (rnd.NextDouble()*(max - min));
      }

      #endregion
   }
}