﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recognition;
using Recognition.RecognitionParts;
using Recognition.Types;

namespace RecognitionTests.IntegrationTests
{
   [TestClass]
   public class RecognitionTests
   {
      private FCFDARecognition recognizer;

      [TestInitialize]
      public void Init()
      {
         recognizer = new FCFDARecognition();
      }


      [TestMethod]
      public void RecognitionTest()
      {
         CalibrationParams calibration;
         FilterParams filter;
         AcceptanceParams parameters;
         CreateParametersAndInputValues(out calibration, out filter, out parameters);
         recognizer = new FCFDARecognition
         {
            Calibration = { CalibrationParams = calibration},
            AveragingFiltration = { Params = filter},
            StableFiltration = { Params = filter },
            AcceptanceParams = parameters
         };
         TestRecognizerForValidValues(TestData.STABLE_SAMPLES);
      }      

      [TestMethod]
      public void NullParameters_SameValuesAtEndAsAtBegining()
      {
         recognizer.AveragingFiltration.Params = null;
         recognizer.Calibration.CalibrationParams = null;
         recognizer.StableFiltration.Params = null;
         recognizer.Acceptance.Params = null;

         const double value = 5;
         var date = DateTime.Now;
         var recognizedValue = 0.0;
         var recognizedTime = new DateTime();
         recognizer.Recognized += (sender, args) =>
         {
            recognizedValue = args.AsG;
            recognizedTime = args.TimeStamp;
         };

         recognizer.Add(new RawWeight(value) { TimeStamp = date });
         Assert.AreEqual(value, recognizedValue);
         Assert.AreEqual(date, recognizedTime);
      }

      [TestMethod]
      public void SetParametersToNullAndThenToNonNull()
      {
         NullParameters_SameValuesAtEndAsAtBegining();
         CalibrationParams calibration;
         FilterParams filter;
         AcceptanceParams parametrs;
         CreateParametersAndInputValues(out calibration, out filter, out parametrs);
         recognizer.Acceptance.Params = parametrs;
         recognizer.AveragingFiltration.Params = filter;
         recognizer.StableFiltration.Params = filter;
         TestRecognizerForValidValues(TestData.STABLE_SAMPLES);
      }

      [TestMethod]
      public void CalibrationWorksWithFilterBefore()
      {
         CalibrationParams calibration;
         FilterParams filter;
         AcceptanceParams parameters;
         CreateParametersAndInputValues(out calibration, out filter, out parameters);
         recognizer = new FCFDARecognition
         {
            Calibration = { CalibrationParams = calibration },
            AveragingFiltration = { Params = filter },
            StableFiltration = { Params = filter },
            AcceptanceParams = parameters
         };

         var readValues = new List<BirdWeight>();
         recognizer.Recognized += (sender, weight) =>
         {
            readValues.Add(weight);
         };

         Assert.AreEqual(1.0, recognizer.Calibration.CalibrationParams.ValueMultiplier);
         Assert.AreEqual(0.0, recognizer.Calibration.CalibrationParams.ValueOffset.AsG);
         Assert.IsTrue(recognizer.AveragingFiltration.Successor == recognizer.Calibration);

         recognizer.Calibration.StartCalibration(TestData.CALIBRATION_SAMPLES.First());
         foreach (var sample in TestData.CALIBRATION_SAMPLES)
         {
            recognizer.Add(new RawWeight(sample));
         }

         Assert.AreNotEqual(1.0, recognizer.Calibration.CalibrationParams.ValueMultiplier);
         Assert.AreNotEqual(0.0, recognizer.Calibration.CalibrationParams.ValueOffset.AsG);
         Assert.AreEqual(0, readValues.Count);
      }

      [TestMethod]
      public void CalibratedAndThenRecognizedValidValue()
      {
         CalibrationWorksWithFilterBefore();

         var readValues = new List<BirdWeight>();
         recognizer.Recognized += (sender, weight) =>
         {
            readValues.Add(weight);
         };

         Assert.AreEqual(0, readValues.Count);
         foreach (var sample in TestData.STABLE_SAMPLES)
         {
            recognizer.Add(new RawWeight(sample));
         }
         Assert.AreEqual(1, readValues.Count);
      }

      #region Private helpers

      private static void CreateParametersAndInputValues(out CalibrationParams calibration, out FilterParams filter,
         out AcceptanceParams parametrs)
      {
         const double valueMultiplier = 1;
         var valueOffset = new StableWeight();

         const int avergingWindow = 10;
         const int stableWindow = 5;
         const int stableRange = 5;
       
         calibration = new CalibrationParams
         {
            ValueMultiplier = valueMultiplier,
            ValueOffset = valueOffset
         };

         filter = new FilterParams
         {
            AveragingWindow = avergingWindow,
            StableRange = new Weight(stableRange),
            StableWindow = stableWindow
         };

         parametrs = new AcceptanceParams
         {
            MaleLow = new Weight(45),
            MaleHigh = new Weight(55),
            FemaleLow = new Weight(34),
            FemaleHigh = new Weight(44),
            AcceptanceSexMode = AcceptanceSexMode.Mixed,
            StepMode = AcceptanceStepMode.Both
         };
      }

      private void TestRecognizerForValidValues(Weight[] stableSamples)
      {
         var values = new List<BirdWeight>();
         recognizer.Recognized += (sender, args) =>
         {
            values.Add(args);
         };
         RecognizerAddSamples(stableSamples);
         Assert.AreEqual(1, values.Count);

         RecognizerAddSamples(stableSamples, 54.5);
         Assert.AreEqual(2, values.Count);
        
         RecognizerAddSamples(stableSamples, 75);
         Assert.AreEqual(2, values.Count);

         RecognizerAddSamples(stableSamples, 115.2);
         Assert.AreEqual(3, values.Count);

         RecognizerAddSamples(stableSamples, 80.1);
         Assert.AreEqual(4, values.Count);
      }

      private void RecognizerAddSamples(Weight[] stableSamples,  double baseValue = 0.0)
      {
         foreach (var sample in stableSamples)
         {
            recognizer.Add(new RawWeight(sample.AsG + baseValue));
         }
      }

      #endregion
   }
}