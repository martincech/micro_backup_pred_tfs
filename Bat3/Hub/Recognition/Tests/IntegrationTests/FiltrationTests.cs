﻿using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recognition.RecognitionParts;
using Recognition.Types;

namespace RecognitionTests.IntegrationTests
{
   [TestClass]
   public class FiltrationTests
   {
      #region Private fields

      internal const int AVERGING_WINDOW = 10;
      internal const int STABLE_WINDOW = 5;
      // ReSharper disable once InconsistentNaming
      internal static Weight STABLE_RANGE = new Weight(5);
      private StableFiltration stableFiltration;
      private AveragingFiltration averagingFiltration;

      #endregion

      [TestInitialize]
      public void Init()
      {
         var @params = new FilterParams
         {
            StableRange = STABLE_RANGE,
            StableWindow = STABLE_WINDOW,
            AveragingWindow = AVERGING_WINDOW
         };
         stableFiltration = new StableFiltration(@params);
         averagingFiltration = new AveragingFiltration(@params);
      }


      [TestMethod]
      public void Filter_AccordingToParamsWell()
      {
         averagingFiltration.Params.AveragingWindow = 2;
         Assert.AreEqual(2, FilterProcess());

         stableFiltration.Params.StableRange = new Weight(10);
         averagingFiltration.Params.AveragingWindow = 5;
         Assert.AreEqual(3, FilterProcess());

         stableFiltration.Params.StableRange = new Weight(5);
         averagingFiltration.Params.AveragingWindow = 5;
         stableFiltration.Params.StableWindow = 1;
         Assert.AreEqual(3, FilterProcess());
      }

      #region Private helpers

      private int FilterProcess()
      {
         var samples = TestData.SAMPLES;
         var avgValues = new List<AverageWeight>();
         averagingFiltration.Recognized += (sender, eventArgs) =>
         {
            avgValues.Add(eventArgs);
         };
         var stableValues = new List<StableWeight>();
         stableFiltration.Recognized += (sender, eventArgs) =>
         {
            stableValues.Add(eventArgs);
         };

         foreach (var sample in samples)
         {
            averagingFiltration.Add(new RawWeight(sample.AsG));
         }
         Assert.AreEqual(samples.Count(), avgValues.Count);
         foreach (var sample in avgValues)
         {
            stableFiltration.Add(new CalibratedWeight(sample.AsG, sample.HighPass.AsG));
         }
         return stableValues.Count;
      }

      #endregion
   }
}
