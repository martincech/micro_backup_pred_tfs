﻿using BatLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recognition.RecognitionParts;
using Recognition.Types;

namespace RecognitionTests.UnitTests
{
   [TestClass]
   public class AcceptanceTest
   {
      #region Default parameters

      private readonly AcceptanceParams defaultParams = new AcceptanceParams
      {
         MaleLow = new Weight(45),
         MaleHigh = new Weight(55),
         FemaleLow = new Weight(34.9),
         FemaleHigh = new Weight(44.9),
         AcceptanceSexMode = AcceptanceSexMode.Mixed,
         StepMode = AcceptanceStepMode.Both
      };

      private readonly AcceptanceParams maleSingleParameters = new AcceptanceParams
      {
         MaleLow = new Weight(27),
         MaleHigh = new Weight(37),
         AcceptanceSexMode = AcceptanceSexMode.Single,
         StepMode = AcceptanceStepMode.Both,
         SexMode = Sex.Male
      };

      #endregion

      [TestMethod]
      public void Acceptance_Accepted_WhenMixed()
      {
         const int expectMale = 54;
         const double expectFemale = 34.9;

         var parameters = defaultParams;
         Acceptance_OK(parameters, expectMale, expectFemale, null);
      }

      [TestMethod]
      public void Acceptance_NotAccepted_WhenMixed()
      {
         const int expectMale = 56;
         const int expectFemale = 34;

         var parameters = defaultParams;
         Acceptance_NotOK(parameters, new Weight(expectMale), new Weight(expectFemale));
      }

      [TestMethod]
      public void Acceptance_Accepted_WhenSingle()
      {
         const int expectMale = 46;

         var parameters = defaultParams;
         parameters.AcceptanceSexMode = AcceptanceSexMode.Single;
         parameters.SexMode = Sex.Male;
         
         Acceptance_OK(parameters, expectMale, null, null);
      }

      [TestMethod]
      public void Acceptance_Accepted_WhenSingleUndefinedSex()
      {
         const int expectUndefined = 46;
         var parameters = new AcceptanceParams
         {
            UndefinedLow = new Weight(45),
            UndefinedHigh = new Weight(55),
            AcceptanceSexMode = AcceptanceSexMode.Single,
            StepMode = AcceptanceStepMode.Both,
            SexMode = Sex.Undefined
         };

         Acceptance_OK(parameters, null, null, expectUndefined);
      }

      [TestMethod]
      public void Acceptance_NotAccepted_WhenSingle()
      {
         const double expectMale = 26.9;

         var parameters = maleSingleParameters;
         parameters.SexMode = Sex.Female;
         Acceptance_NotOK(parameters, new Weight(expectMale), null);
      }

      [TestMethod]
      public void Acceptance_Step()
      {
         var stepMode = DifferenceMode.Increased;
         var parameters = maleSingleParameters;
         var acceptance = new Acceptance(parameters);

         var called = 0;
         acceptance.Recognized += (sender, args) =>
         {
            // ReSharper disable once AccessToModifiedClosure
            Assert.AreEqual(stepMode, args.Difference);
            Assert.AreEqual(Sex.Male, args.Sex);
            called++;
         };

         stepMode = DifferenceMode.Increased;
         acceptance.Add(new DifferenceWeight(30));

         stepMode = DifferenceMode.Decreased;
         acceptance.Add(new DifferenceWeight(-30));

         stepMode = DifferenceMode.Increased;
         acceptance.Add(new DifferenceWeight(30));

         Assert.AreEqual(3, called);
      }


      [TestMethod]
      public void Acceptance_StepMode_Enter()
      {
         var parameters = maleSingleParameters;
         parameters.StepMode = AcceptanceStepMode.Enter;
         var acceptance = new Acceptance(parameters);

         var called = 0;
         acceptance.Recognized += (sender, args) =>
         {
            Assert.AreEqual(args.Difference, DifferenceMode.Increased);
            Assert.AreEqual(args.Sex, Sex.Male);
            called++;
         };

         acceptance.Add(new DifferenceWeight(30));
         acceptance.Add(new DifferenceWeight(-30));
         acceptance.Add(new DifferenceWeight(30));
         Assert.AreEqual(2, called);
      }

      #region Private helpers

      private void Acceptance_OK(AcceptanceParams parameters, double? maleValue, double? femaleValue,
         double? undefinedSexValue)
      {
         BirdWeight result = null;
         var acceptance = new Acceptance(parameters);

         acceptance.Recognized += (sender, args) =>
         {
            result = args;
         };

         if (maleValue != null &&
             (parameters.SexMode != Sex.Female || parameters.AcceptanceSexMode == AcceptanceSexMode.Mixed))
         {
            acceptance.Add(new DifferenceWeight(maleValue));
            Assert.IsNotNull(result);
            Assert.AreEqual(maleValue, result.AsG);
            Assert.AreEqual(Sex.Male, result.Sex);
         }

         result = null;

         if (femaleValue != null &&
             (parameters.SexMode != Sex.Male || parameters.AcceptanceSexMode == AcceptanceSexMode.Mixed))
         {
            acceptance.Add(new DifferenceWeight(femaleValue));
            Assert.IsNotNull(result);
            Assert.AreEqual(femaleValue, result.AsG);
            Assert.AreEqual(Sex.Female, result.Sex);
         }

         if (undefinedSexValue != null && (parameters.SexMode == Sex.Undefined))
         {
            acceptance.Add(new DifferenceWeight(undefinedSexValue));
            Assert.IsNotNull(result);
            Assert.AreEqual(undefinedSexValue, result.AsG);
            Assert.AreEqual(Sex.Undefined, result.Sex);
         }
      }

      private void Acceptance_NotOK(AcceptanceParams parameters, Weight maleValue, Weight femaleValue)
      {
         var sexAdded = Sex.Undefined;
         var acceptance = new Acceptance(parameters);

         acceptance.Recognized += (sender, args) =>
         {
            Assert.Fail("This test should not pass.");
         };

         acceptance.Recognized += (sender, args) =>
         {
            // ReSharper disable once AccessToModifiedClosure
            if (sexAdded == Sex.Male)
            {
               Assert.Fail("Male weight {0} shouldn't pass acceptance filter. With min: {1} max: {2}.", maleValue,
                  parameters.MaleLow, parameters.MaleHigh);
            }

            // ReSharper disable once AccessToModifiedClosure
            if (sexAdded == Sex.Female)
            {
               Assert.Fail("Female weight {0} shouldn't pass acceptance filter. With min: {1} max: {2}.", maleValue,
                  parameters.FemaleLow, parameters.FemaleHigh);
            }
         };


         if (maleValue != null &&
             (parameters.SexMode != Sex.Female || parameters.AcceptanceSexMode == AcceptanceSexMode.Mixed))
         {
            sexAdded = Sex.Male;
            acceptance.Add(new DifferenceWeight(maleValue));
         }

         if (femaleValue != null &&
             (parameters.SexMode != Sex.Male || parameters.AcceptanceSexMode == AcceptanceSexMode.Mixed))
         {
            sexAdded = Sex.Female;
            acceptance.Add(new DifferenceWeight(femaleValue));
         }
      }

      #endregion
   }
}
