﻿using System;
using BatLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recognition.RecognitionParts;
using Recognition.Types;
using RecognitionTests.IntegrationTests;

namespace RecognitionTests.UnitTests
{
   [TestClass]
   public class CalibrationTest
   {
      #region Private fields

      private Calibration calibration;
      private CalibrationParams calibrationParams;
      private static readonly Weight CALIBRATION_WEIGHT = new Weight(500);
      private const double VALID_HIGH_PASS = 0.1;

      #endregion

      [TestInitialize]
      public void Init()
      {
         /*
         * Weight used for calibration 2kg => 20000
         * Scale + weight => 1920
         * Scale => 1420 => ValueOffset
         * 20000/(1920-1420) = 40 => ValueMultiplier
         */
         calibrationParams = new CalibrationParams
         {
            ValueMultiplier = 40,
            ValueOffset = new StableWeight(142)
         };
         calibration = new Calibration(calibrationParams)
         {
            FilterParams = new FilterParams
            {
               StableRange = FiltrationTests.STABLE_RANGE,
               StableWindow = FiltrationTests.STABLE_WINDOW
            }
         };
      }

      #region Calibration process

      [TestMethod]
      [ExpectedException(typeof(NullReferenceException))]
      public void CalibrationFail_NoFiltrationParameters()
      {
         calibration.FilterParams = null;
         calibration.StartCalibration(CALIBRATION_WEIGHT);

         var data = TestData.StablePreFilteredData(FiltrationTests.STABLE_RANGE.AsG);
         foreach (var sample in data)
         {
            calibration.Add(new AverageWeight(sample.AsG, sample.HighPass.AsG));
         }
      }

      [TestMethod]
      [ExpectedException(typeof(NullReferenceException))]
      public void CalibrationFail_WeightWithoutHighPass()
      {
         calibration.StartCalibration(CALIBRATION_WEIGHT);
         Assert.IsNotNull(calibration.FilterParams);

         var data = TestData.STABLE_SAMPLES;
         foreach (var sample in data)
         {
            calibration.Add(new AverageWeight(sample));
         }
      }

      [TestMethod]
      public void CalibratedAndStabilizedEventRaised_RecognizedNotRaised()
      {
         var calibrated = false;
         var calibrationWeightStabilized = false;
         var recognized = false;
         calibration.Calibrated += (sender, args) =>
         {
            calibrated = true;
         };
         calibration.CalibrationWeightStabilized += (sender, args) =>
         {
            calibrationWeightStabilized = true;
         };
         calibration.Recognized += (sender, weight) =>
         {
            recognized = true;
         };
         calibration.StartCalibration(CALIBRATION_WEIGHT);

         Assert.IsFalse(calibrated);
         Assert.IsFalse(calibrationWeightStabilized);
         Assert.IsFalse(recognized);
         
         // first calibration value
         var stable1 = TestData.StablePreFilteredData(FiltrationTests.STABLE_RANGE.AsG);
         foreach (var sample in stable1)
         {
            calibration.Add(new AverageWeight(sample.AsG, sample.HighPass.AsG));
         }
         Assert.IsFalse(calibrated);
         Assert.IsTrue(calibrationWeightStabilized);
         Assert.IsFalse(recognized);

         // second calibration value
         calibrationWeightStabilized = false;
         var stable2 = TestData.ExactData(100);
         foreach (var sample in stable2)
         {
            if (calibrated) break;
            calibration.Add(new AverageWeight(sample.AsG, sample.HighPass.AsG));
         }
         Assert.IsTrue(calibrated);
         Assert.IsFalse(calibrationWeightStabilized);
         Assert.IsFalse(recognized);

         // recognized value
         calibrated = false;
         foreach (var sample in stable2)
         {
            calibration.Add(new AverageWeight(sample.AsG, sample.HighPass.AsG));
         }
         Assert.IsFalse(calibrated);
         Assert.IsFalse(calibrationWeightStabilized);
         Assert.IsTrue(recognized);
      }

      [TestMethod]
      public void CalibrationParametersAreCorrectAfterCalibration_WithWeightFirst()
      {
         ExecuteCalibration(CALIBRATION_WEIGHT.AsG, 100);
      }

      [TestMethod]
      public void CalibrationParametersAreCorrectAfterCalibration_WithWeightLast()
      {
         ExecuteCalibration(100, CALIBRATION_WEIGHT.AsG);
      }

      [TestMethod]
      public void CalibrationParamsObjectIsSameAfterCalibration()
      {
         var pars = calibration.CalibrationParams;
         ExecuteCalibration(100, CALIBRATION_WEIGHT.AsG);
         Assert.AreSame(pars, calibration.CalibrationParams);
      }

      [TestMethod]
      public void CalibratedAndThenRecognizedValidValue()
      {
         var sample1 = CALIBRATION_WEIGHT.AsG;
         var sample2 = 100.0;
         ExecuteCalibration(sample1, sample2);

         Weight recognizedValue = null;
         calibration.Recognized += (sender, weight) =>
         {
            recognizedValue = weight;
         };
         calibration.Add(new AverageWeight(sample1, VALID_HIGH_PASS));
         Assert.AreEqual(GetExpectedRecognizedWeight(sample1), recognizedValue.AsG);
         calibration.Add(new AverageWeight(sample2, VALID_HIGH_PASS));
         Assert.AreEqual(GetExpectedRecognizedWeight(sample2), recognizedValue.AsG);
      }

      #endregion

      [TestMethod]
      public void Add_WhenValueIsPositive()
      {
         var sampleValue = 157.1;
         /*
          * (157.1 -142)*40 = 604g
         */
         var expectedWeight = 604;

         CheckCalibrationResult(sampleValue, expectedWeight);
      }

      [TestMethod]
      public void Add_WhenValueIsNegative()
      {

         var sampleValue = 97.1;
         /*
          * (97.1 - 142)*40 = -1796g
         */
         var expectedWeight = -1796;

         CheckCalibrationResult(sampleValue, expectedWeight);
      }


      [TestMethod]
      public void Add_WhenValueIsEqualToValueOffset()
      {

         var sampleValue = 142;
         /*
          * (142 -142)*4 = 0 => 0g 
         */
         var expectedWeight = 0;

         CheckCalibrationResult(sampleValue, expectedWeight);
      }


      #region Private helpers

      private void CheckCalibrationResult(double sampleValue, double expectedWeight)
      {
         var expextedDate = DateTime.Now;

         calibration.Recognized += (sender, e) =>
         {
            Assert.AreEqual(expectedWeight, e.AsG);
            Assert.AreEqual(expextedDate, e.TimeStamp);
         };
         calibration.Add(new AverageWeight(sampleValue, VALID_HIGH_PASS)
         {
            TimeStamp = expextedDate
         });
      }

      private double GetExpectedRecognizedWeight(double weight)
      {
         return ((weight - calibration.CalibrationParams.ValueOffset)*calibration.CalibrationParams.ValueMultiplier).AsG;
      }

      private void CalibrationAddValues(double value)
      {
         foreach (var sample in TestData.ExactData(value))
         {
            calibration.Add(new AverageWeight(sample.AsG, sample.HighPass.AsG));
         }
      }

      private void ExecuteCalibration(double stableSample1, double stableSample2)
      {
         calibration.StartCalibration(CALIBRATION_WEIGHT);
         CalibrationAddValues(stableSample1);
         CalibrationAddValues(stableSample2);

         if (stableSample1 < stableSample2)
         {
            Assert.AreEqual(stableSample2/Math.Abs(stableSample2 - stableSample1),
               calibration.CalibrationParams.ValueMultiplier);
            Assert.AreEqual(stableSample1, calibration.CalibrationParams.ValueOffset.AsG);
         }
         else
         {
            Assert.AreEqual(stableSample1/Math.Abs(stableSample1 - stableSample2),
               calibration.CalibrationParams.ValueMultiplier);
            Assert.AreEqual(stableSample2, calibration.CalibrationParams.ValueOffset.AsG);
         }
      }

      #endregion
   }
}
