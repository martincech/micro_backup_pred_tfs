﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recognition.RecognitionParts;
using Recognition.Types;
using RecognitionTests.IntegrationTests;

namespace RecognitionTests.UnitTests
{
   [TestClass]
   public class AveragingFiltrationTest
   {
      private AveragingFiltration filtration;

      [TestInitialize]
      public void Init()
      {
         filtration = new AveragingFiltration(new FilterParams
         {
            AveragingWindow = FiltrationTests.AVERGING_WINDOW,
            StableRange = FiltrationTests.STABLE_RANGE
         });
      }

      [TestMethod]
      public void Filter_ReturnAverageValues()
      {
         filtration.Params.AveragingWindow = 5;
         var recognized = new AverageWeight();
         var recognizedCount = 0;
         filtration.Recognized += (sender, weight) =>
         {
            recognized = weight;
            recognizedCount++;
         };

         var data = TestData.SAMPLES.ToList().GetRange(0, filtration.Params.AveragingWindow);
         foreach (var sample in data)
         {
            filtration.Add(new RawWeight(sample));   
         }

         Assert.AreEqual(data.Count, recognizedCount);
         Assert.AreEqual(data.Sum(s => s.AsG) / data.Count, recognized.AsG);
      }

      [TestMethod]
      public void Filter_SetHighPass()
      {
         var recognized = new List<AverageWeight>();
         var unstableCount = 0;
         filtration.Recognized += (sender, weight) =>
         {
            recognized.Add(weight);
            if (weight.HighPass >= filtration.Params.StableRange)
            {
               // ReSharper disable once AccessToModifiedClosure
               unstableCount++;
            }
         };

         // stable data
         foreach (var sample in TestData.STABLE_SAMPLES)
         {
            filtration.Add(new RawWeight(sample));
         }
         for (var i = 1; i < recognized.Count; i++)
         {
            Assert.IsTrue(recognized[i].HighPass > 0.0 && 
               recognized[i].HighPass < filtration.Params.StableRange);
         }

         // unstable data
         unstableCount = 0;
         foreach (var sample in TestData.UNSTABLE_SAMPLES)
         {
            filtration.Add(new RawWeight(sample));
         }
         Assert.IsTrue(unstableCount > 1);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentOutOfRangeException))]
      public void ParamsOutOfRange_AveragingWindow()
      {
         filtration.Params.AveragingWindow = filtration.Params.AveragingWindowMinimum - 1;
      }

      [TestMethod]
      public void NullParameters_NoFiltration()
      {
         filtration.Params = null;
         var samples = TestData.STABLE_SAMPLES;
         var recognizedIndex = 0;
         var recognizedValues = new List<Weight>();
         filtration.Recognized += (sender, eventArgs) =>
         {
            recognizedIndex++;
            recognizedValues.Add(new Weight(eventArgs.AsG));
         };

         foreach (var sample in samples)
         {
            filtration.Add(new RawWeight(sample));
         }
         Assert.AreEqual(samples.Count(), recognizedIndex);
         CollectionAssert.AreEqual(samples, recognizedValues);
      }  
   }
}
