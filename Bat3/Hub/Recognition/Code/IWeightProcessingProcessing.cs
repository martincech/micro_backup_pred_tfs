using BatLibrary;

namespace Recognition
{
   public interface IWeightProcessingProcessing<in T, U> : IWeightProcessingAdministrator<T>, IWeightProcessingEventer<U>
      where T : Weight
      where U : Weight
   {
   }
}