﻿using System;
using BatLibrary;

namespace Recognition.Types
{
   public class DifferenceWeight : Weight
   {
      /// <summary>
      /// Init weight in grams
      /// </summary>
      /// <param name="grams">initialization value of weight as grams</param>
      public DifferenceWeight(double grams = 0) : 
         this(grams, WeightUnits.G)
      {
         
      }

      public DifferenceWeight(Weight weight) 
         : this(Math.Abs(weight.AsG))
      {
      }

      public DifferenceWeight(double? grams)
         : this(grams ?? 0)
      {
      }

      public DifferenceWeight(double weight, WeightUnits units) 
         : base(Math.Abs(weight), units)
      {
         Difference = weight >= 0 ? DifferenceMode.Increased : DifferenceMode.Decreased;
      }

      public DifferenceMode Difference { get; private set; }
   }
}
