﻿using BatLibrary;

namespace Recognition.Types
{
   public class FilterWeight : Weight
   {
      /// <summary>
      /// Init weight in grams
      /// </summary>
      /// <param name="grams">initialization value of weight as grams</param>
      public FilterWeight(double grams = 0) : base(grams)
      {
      }

      public FilterWeight(Weight weight) : base(weight)
      {
      }

      public FilterWeight(double? grams) : base(grams)
      {
      }

      public FilterWeight(double weight, WeightUnits units)
         : base(weight, units)
      {
      }

      public FilterWeight(double? grams, double highPassGrams)
         : base(grams)
      {
         HighPass = new Weight(highPassGrams);
      }

      public FilterWeight(double? grams, Weight highPass)
         : base(grams)
      {
         HighPass = highPass;
      }

      public Weight HighPass { get; private set; }
   }
}
