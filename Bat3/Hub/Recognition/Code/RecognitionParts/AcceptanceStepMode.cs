namespace Recognition.RecognitionParts
{
   public enum AcceptanceStepMode
   {
      Enter,
      Leave,
      Both
   }
}