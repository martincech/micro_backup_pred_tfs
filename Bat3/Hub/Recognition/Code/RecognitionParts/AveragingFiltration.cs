﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Recognition.Types;

namespace Recognition.RecognitionParts
{
   /// <summary>
   /// Filtering of measured values to get stabilized value
   /// </summary>
   public class AveragingFiltration : RecognitionChainPart<RawWeight, AverageWeight>
   {
      #region Private
   
      private readonly List<RawWeight> fifo;
      private const int PARAMS_AVERAGING_WINDOW_MINIMUM = 2;
      
      #endregion

      /// <summary>
      /// Filtration constructor
      /// </summary>
      /// <param name="fParams">filtration parametrs</param>
      public AveragingFiltration(FilterParams fParams)
      {
         Params = fParams;
         SetParametersLimits();

         fifo = new List<RawWeight>();  
      }

      public FilterParams Params { get; set; }

      /// <summary>
      /// Add measured value into buffer
      /// </summary>
      /// <param name="rawWeight">measured value</param>
      public override void Add(RawWeight rawWeight)
      {
         if (Params == null)
         {
            OnRecognized(new AverageWeight(rawWeight.AsG) { TimeStamp = rawWeight.TimeStamp });
            return;
         }
         while (fifo.Count >= Params.AveragingWindow && fifo.Count > 0)
         {
            fifo.RemoveAt(0);
         }
         fifo.Add(rawWeight);

         var lowPass = fifo.Sum(weight => weight == null? 0 : weight.AsG)/fifo.Count;
         var highPass = Math.Abs(rawWeight.AsG - lowPass);
         OnRecognized(new AverageWeight(lowPass, highPass) { TimeStamp = rawWeight.TimeStamp });
      }

      private void SetParametersLimits()
      {
         Params.AveragingWindowMinimum = PARAMS_AVERAGING_WINDOW_MINIMUM;
      }
   }
}
