﻿using System;
using BatLibrary;

namespace Recognition.RecognitionParts
{
   public abstract class RecognitionChainPart<T, U> : IChainableRecognition<T, U>
      where T : Weight
      where U : Weight
   {
      #region Implementation of IRecognition<in T,U>

      public abstract void Add(T rawWeight);

      public virtual event EventHandler<U> Recognized;

      public IWeightProcessingAdministrator<U> Successor { get; set; }

      protected void OnRecognized(U e)
      {
         var handler = Recognized;
         if (handler != null) handler(this, e);
         if (Successor != null)
         {
            Successor.Add(e);
         }
      }

      #endregion
   }
}