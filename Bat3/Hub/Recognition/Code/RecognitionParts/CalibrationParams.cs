using Recognition.Types;

namespace Recognition.RecognitionParts
{
   public class CalibrationParams
   {
      public StableWeight ValueOffset;
      public double ValueMultiplier;
   }
}