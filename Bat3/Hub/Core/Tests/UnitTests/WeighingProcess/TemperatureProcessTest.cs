﻿using System;
using System.Linq;
using Communication.Readers;
using Communication.Samples;
using Core.Sensors;
using Core.WeighingProcess.Temperature;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CoreTests.UnitTests.WeighingProcess
{
   [TestClass]
   public class TemperatureProcessTest
   {
      private Mock<IActualValueOnSensor> mockActualValueOnSensor;
      private MockSensorHistory mockSensorHistory;
      private Mock<ITemperatureReader> mockTemperatureReader;

      private ITemperatureProcess temperatureProcess;

      [TestInitialize]
      public void Init()
      {
         mockActualValueOnSensor = new Mock<IActualValueOnSensor>();
         mockSensorHistory = new MockSensorHistory();
         mockTemperatureReader = new Mock<ITemperatureReader>();

         temperatureProcess = new TemperatureProcess(mockActualValueOnSensor.Object, mockSensorHistory);
         temperatureProcess.ProcessFromSensor(mockTemperatureReader.Object);
      }

      [TestMethod]
      public void SensorHistoryAndActualTemperatureOnSensorChanged_WhenNewSample()
      {
         var sample = new TemperatureSample { TimeStamp = DateTime.Now, Value = 22 };
         Assert.AreEqual(0, mockSensorHistory.TemperatureList.Count);
         mockActualValueOnSensor.VerifySet(s => s.Temperature = It.IsAny<double>(), Times.Never);
         mockTemperatureReader.Raise(s => s.NewSample += null, null, sample);
         Assert.AreEqual(1, mockSensorHistory.TemperatureList.Count);
         Assert.AreEqual(sample, mockSensorHistory.TemperatureList.Last());
         mockActualValueOnSensor.VerifySet(s => s.Temperature = sample.Value, Times.Once());
      }

      [TestMethod]
      public void SensorHistoryAndActualTemperatureOnSensorNotChanged_WhenNewSampleIsNull()
      {
         Assert.AreEqual(0, mockSensorHistory.TemperatureList.Count);
         mockActualValueOnSensor.VerifySet(s => s.Temperature = It.IsAny<double>(), Times.Never);
         mockTemperatureReader.Raise(s => s.NewSample += null, null, null);
         Assert.AreEqual(0, mockSensorHistory.TemperatureList.Count);
         mockActualValueOnSensor.VerifySet(s => s.Temperature = It.IsAny<double>(), Times.Never);
      }

      [TestMethod]
      public void C2SensorReaderChanged_WhenTemperatureReaderChanged()
      {
         var newReader = new Mock<ITemperatureReader>().Object;
         Assert.AreNotEqual(newReader, temperatureProcess.TemperatureSensor.Reader);
         temperatureProcess.ProcessFromSensor(newReader);
         Assert.AreEqual(newReader, temperatureProcess.TemperatureSensor.Reader);
      }
   }
}
