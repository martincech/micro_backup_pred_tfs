﻿using System;
using System.Linq;
using Communication.Readers;
using Communication.Samples;
using Core.Sensors;
using Core.WeighingProcess.Humidity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CoreTests.UnitTests.WeighingProcess
{

   [TestClass]
   public class HumidityProcessTest
   {
      private Mock<IActualValueOnSensor> mockActualValueOnSensor;
      private MockSensorHistory mockSensorHistory;
      private Mock<IHumidityReader> mockHumidityReader;

      private IHumidityProcess humidityProcess;

      [TestInitialize]
      public void Init()
      {
         mockActualValueOnSensor = new Mock<IActualValueOnSensor>();
         mockSensorHistory = new MockSensorHistory();
         mockHumidityReader = new Mock<IHumidityReader>();

         humidityProcess = new HumidityProcess(mockActualValueOnSensor.Object, mockSensorHistory);
         humidityProcess.ProcessFromSensor(mockHumidityReader.Object);
      }

      [TestMethod]
      public void SensorHistoryAndActualHumidityOnSensorChanged_WhenNewSample()
      {
         var sample = new HumiditySample { TimeStamp = DateTime.Now, Value = 25 };
         Assert.AreEqual(0, mockSensorHistory.HumidityList.Count);
         mockActualValueOnSensor.VerifySet(s => s.Humidity = It.IsAny<byte>(), Times.Never);
         mockHumidityReader.Raise(s => s.NewSample += null, null, sample);
         Assert.AreEqual(1, mockSensorHistory.HumidityList.Count);
         Assert.AreEqual(sample, mockSensorHistory.HumidityList.Last());
         mockActualValueOnSensor.VerifySet(s => s.Humidity = sample.Value, Times.Once());
      }

      [TestMethod]
      public void SensorHistoryAndActualHumidityOnSensorNotChanged_WhenNewSampleIsNull()
      {
         Assert.AreEqual(0, mockSensorHistory.HumidityList.Count);
         mockActualValueOnSensor.VerifySet(s => s.Humidity = It.IsAny<byte>(), Times.Never);
         mockHumidityReader.Raise(s => s.NewSample += null, null, null);
         Assert.AreEqual(0, mockSensorHistory.HumidityList.Count);
         mockActualValueOnSensor.VerifySet(s => s.Humidity = It.IsAny<byte>(), Times.Never);
      }


      [TestMethod]
      public void C2SensorReaderChanged_WhenHumidityReaderChanged()
      {
         var newReader = new Mock<IHumidityReader>().Object;
         Assert.AreNotEqual(newReader, humidityProcess.HumiditySensor.Reader);
         humidityProcess.ProcessFromSensor(newReader);
         Assert.AreEqual(newReader, humidityProcess.HumiditySensor.Reader);
      }
   }
}
