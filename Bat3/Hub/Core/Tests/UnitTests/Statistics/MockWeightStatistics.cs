using System;
using BatLibrary;
using Core.Statistics;

namespace CoreTests.UnitTests.Statistics
{
   public class MockWeightStatistics : IWeightStatistics
   {
      #region Implementation of IWeightStatistics

      public Weight Average { get; set; }
      public int Count { get; set; }
      public double Uniformity { get; set; }
      public double Cv { get; set; }
      public double Sigma { get; set; }
      public Weight Gain { get; set; }
      public DateTime Day { get; set; }

      #endregion
   }
}