﻿using System;
using Communication.Readers;
using Communication.Samples;
using Core.Sensors.Humidity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CoreTests.UnitTests.Sensors
{
   [TestClass]
   public class HumiditySensorTests
   {
      #region Private fields

      private Mock<IHumidityReader> mockReader;
      private Mock<IActualHumidityOnSensor> mockHumiditySensor;
      private HumiditySensor sensor;
      private SensorTester<IHumidityReader, HumiditySensor, HumiditySample> tester;

      #endregion

      [TestInitialize]
      public void TestInit()
      {
         mockReader = new Mock<IHumidityReader>();
         mockHumiditySensor = new Mock<IActualHumidityOnSensor>();
         sensor = new HumiditySensor(mockHumiditySensor.Object) { Reader = mockReader.Object };
         tester = new SensorTester<IHumidityReader, HumiditySensor, HumiditySample>(mockReader, sensor);
      }


      [TestMethod]
      public void Values_CollectionChanged_WhenNewSample()
      {
         tester.CollectionChangedTest();
      }

      [TestMethod]
      public void Values_CollectionNotChanged_WhenNewSampleIsNull()
      {
         tester.CollectionNotChangedTest_WhenNewSampleIsNull();
      }

      [TestMethod]
      public void ChangeReader()
      {
         Values_CollectionChanged_WhenNewSample();

         var anotherMockReader = new Mock<IHumidityReader>();
         sensor.Reader = anotherMockReader.Object;
         Assert.AreEqual(anotherMockReader.Object, sensor.Reader);
         tester.ChangeReaderTest(anotherMockReader);
      }

      [TestMethod]
      public void RemoveReader()
      {
         sensor.Reader = null;
         tester.RemoveReaderTest();
      }

      [TestMethod]
      public void LastValue()
      {
         tester.LastValueTest();
      }

      [TestMethod]
      public void LastVale_WhenNewSampleIsNull()
      {
         tester.LastValueTest_WhenNewSampleIsNull();
      }

      [TestMethod]
      public void LastVale_WhenLastNewSampleIsNull()
      {
         tester.LastValueTest_WhenLastNewSampleIsNull();
      }

      [TestMethod]
      public void ActualHumidityOnSensorIsChanged_WhenNewSample()
      {
         var sample = new HumiditySample() { TimeStamp = DateTime.Now, Value = 25 };
         mockHumiditySensor.VerifySet(s => s.Humidity = It.IsAny<byte>(), Times.Never);
         mockReader.Raise(r => r.NewSample += null, null, sample);
         mockHumiditySensor.VerifySet(s => s.Humidity = It.IsAny<byte>(), Times.Once);
         mockHumiditySensor.VerifySet(s => s.Humidity = sample.Value, Times.Once);
      }

      [TestMethod]
      public void ActualHumidityOnSensorIsNotChanged_WhenNewSampleIsNull()
      {
         ActualHumidityOnSensorIsChanged_WhenNewSample();
         mockReader.Raise(r => r.NewSample += null, null, null);
         mockHumiditySensor.VerifySet(s => s.Humidity = It.IsAny<byte>(), Times.Once);
      }

      [TestMethod]
      public void LastValue_WhenActualHumidityOnSensorIsNull()
      {
         CreateHumiditySensorWithoutActualHumidityOnSensor();
         LastValue();
      }

      [TestMethod]
      public void Values_WhenActualHumidityOnSensorIsNull()
      {
         CreateHumiditySensorWithoutActualHumidityOnSensor();
         Values_CollectionChanged_WhenNewSample();
      }

      private void CreateHumiditySensorWithoutActualHumidityOnSensor()
      {
         sensor = new HumiditySensor(null) { Reader = mockReader.Object };
         tester = new SensorTester<IHumidityReader, HumiditySensor, HumiditySample>(mockReader, sensor);
      }

   }
}
