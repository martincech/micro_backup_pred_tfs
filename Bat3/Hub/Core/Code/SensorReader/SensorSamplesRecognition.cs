using Core.WeighingProcess;

namespace Core.SensorReader
{
   internal class SensorSamplesRecognition : ISensorSamplesRecognition
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public SensorSamplesRecognition(WeightSamplesFromSensorRecognition weightSamplesRecognition)
      {
         WeightSamplesRecognition = weightSamplesRecognition;
         TemperatureSamplesRecognition = weightSamplesRecognition;
         Co2SamplesRecognition = weightSamplesRecognition;
         HumiditySamplesRecognition = weightSamplesRecognition;
      }

      #region Implementation of ISensorSamplesRecognition

      public IWeightSamplesRecognition WeightSamplesRecognition { get; private set; }
      public ITemperatureSamplesRecognition TemperatureSamplesRecognition { get; private set; }
      public ICo2SamplesRecognition Co2SamplesRecognition { get; private set; }
      public IHumiditySamplesRecognition HumiditySamplesRecognition { get; private set; }
      
      #endregion


    
   }
}