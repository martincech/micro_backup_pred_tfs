﻿using System.Collections.Generic;
using BatLibrary;
using Recognition.RecognitionParts;

namespace Core.WeighingProcess.Weight
{
   public class WeightParamsSample
   {
      private const double ACCEPTANCE_LOW = 0.7;
      private const double ACCEPTANCE_HIGH = 1.3; 

      public static Dictionary<int, AcceptanceParams> GetParams()
      {
         var acceptanceParams = new Dictionary<int, AcceptanceParams>
         {
            {1,CreateParams(50)},
            {2,CreateParams(100)},
            {3,CreateParams(150)},
            {4,CreateParams(200)},
            {5,CreateParams(250)},
            {6,CreateParams(300)}
         };
         return acceptanceParams;
      }

      private static AcceptanceParams CreateParams(double average)
      {
         return new AcceptanceParams()
         {
            SexMode = Sex.Undefined,
            UndefinedHigh = new BatLibrary.Weight(average * ACCEPTANCE_HIGH, BatLibrary.Weight.WeightUnits.G),
            UndefinedLow = new BatLibrary.Weight(average * ACCEPTANCE_LOW, BatLibrary.Weight.WeightUnits.G),
            StepMode = AcceptanceStepMode.Both,
            AcceptanceSexMode = AcceptanceSexMode.Single
         };
      }
   }
}
