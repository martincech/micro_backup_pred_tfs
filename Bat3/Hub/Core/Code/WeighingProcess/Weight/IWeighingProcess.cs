﻿using Communication.Readers;
using Core.Sensors.Weight;
using Recognition.RecognitionParts;
using Utilities.Annotations;

namespace Core.WeighingProcess.Weight
{
   public interface IWeighingProcess
   {
      /// <summary>
      /// Close weighing day
      /// </summary>
      /// <param name="acceptanceForToday">Acceptance parametrs for next day of weighing</param>
      void CloseDay(AcceptanceParams acceptanceForToday);
      /// <summary>
      /// Weight sensor manager for all weight readers
      /// </summary>
      BirdWeightSensor WeightSensor { get; }
      /// <summary>
      /// Start processing data from weight reader
      /// </summary>
      /// <param name="sensor">Processed weight reader</param>
      void ProcessFromSensor(IWeightReader sensor);
      /// <summary>
      /// Indicates actual weighing day
      /// </summary>
      int WeighingDay { get; set; }
   }
}