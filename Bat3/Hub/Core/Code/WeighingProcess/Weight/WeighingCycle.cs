﻿using System;
using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using Recognition.RecognitionParts;

namespace Core.WeighingProcess.Weight
{
   public class WeighingCycle : IWeighingCycle
   {
      private readonly IWeighingProcess weighingProcess;
      private readonly IDayEndWatcher dayEndWatcher;
      private readonly IWeightForDay weightForDay;
      private Dictionary<int, AcceptanceParams> dayParams;

      public WeighingCycle(IWeighingProcess weighingProcess, IDayEndWatcher dayEndWatcher, IWeightForDay weightForDay)
      {
         this.weighingProcess = weighingProcess;
         this.dayEndWatcher = dayEndWatcher;
         this.weightForDay = weightForDay;

         dayParams = new Dictionary<int, AcceptanceParams>();
         this.dayEndWatcher.DayEnded += dayEndWatcher_DayEnded;
      }

      public void SetDayParams(Dictionary<int, AcceptanceParams> dayParamses)
      {
         if (IsRunning)
         {
            throw new InvalidOperationException("Cannot change weighing parametrs when weighing running!");
         }
         dayParams = new Dictionary<int, AcceptanceParams>(dayParamses);
      }

      public KeyValuePair<int, AcceptanceParams> ActiveDay { get; private set; }

      public bool StartCycle(DateTime dayEndTime)
      {
         if (!dayParams.Any())
         {
            return false;
         }
         dayEndWatcher.DayEndTime = dayEndTime;
         SetActiveDay(dayParams.FirstOrDefault(f => f.Key == dayParams.Keys.Min()));
         weighingProcess.CloseDay(ActiveDay.Value);
         return true;
      }

      public void StopCycle()
      {
         dayEndWatcher.DayEndTime = null;
      }

      public void NextDay()
      {
         dayEndWatcher_DayEnded(null, null);
      }

      public bool IsRunning
      {
         get { return dayEndWatcher.IsWatching; }
      }

      private void dayEndWatcher_DayEnded(object sender, EventArgs e)
      {  

         var day = ActiveDay.Key;
         if(ActiveDay.Equals(new KeyValuePair<int, AcceptanceParams>()))
         {
            day = dayParams.Keys.Min();
         }
         else
         {
            day++;
         }

         var newActiveDay = dayParams.FirstOrDefault(f => f.Key == day);
         if (newActiveDay.Equals(new KeyValuePair<int, AcceptanceParams>()))
         {
            newActiveDay = new KeyValuePair<int, AcceptanceParams>(day, ActiveDay.Value);
         }
         SetActiveDay(newActiveDay);
         weighingProcess.CloseDay(ActiveDay.Value);
      }

      private void SetActiveDay(KeyValuePair<int, AcceptanceParams> newActiveDay)
      {
         ActiveDay = newActiveDay;
         SetExpectedWeight();
         weightForDay.WeighingDay = ActiveDay.Key;
         weighingProcess.WeighingDay = ActiveDay.Key;
      }

      private void SetExpectedWeight()
      {
         BatLibrary.Weight minWeight = new BatLibrary.Weight();
         BatLibrary.Weight maxWeight = new BatLibrary.Weight();

         if (ActiveDay.Value.SexMode == Sex.Undefined && ActiveDay.Value.AcceptanceSexMode != AcceptanceSexMode.Mixed) // sex == Sex.Undefined
         {
            maxWeight = ActiveDay.Value.UndefinedHigh;
            minWeight = ActiveDay.Value.UndefinedLow;
         }
         else
         {
            if (ActiveDay.Value.SexMode != Sex.Male)
            {
               maxWeight = ActiveDay.Value.FemaleHigh;
               minWeight = ActiveDay.Value.FemaleLow;
            }

            if (ActiveDay.Value.SexMode != Sex.Female)
            {
               maxWeight = ActiveDay.Value.MaleHigh;
               minWeight = ActiveDay.Value.MaleLow;
            }
         }

         weightForDay.MinExpectedWeight = minWeight;
         weightForDay.MaxExpectedWeight = maxWeight;
         weightForDay.ExpectedWeight = (maxWeight + minWeight)/2;
      }
   }
}
