﻿using Core.Sensors;
using Core.Sensors.Weight;
using Recognition;
using Recognition.Types;

namespace Core.WeighingProcess.Weight
{
   internal class ReportableCfdaRecognition : FCFDARecognition
   {
      private readonly IActualWeightOnSensor weightReport;

      public ReportableCfdaRecognition(IActualValueOnSensor weightReport)
      {
         this.weightReport = weightReport;
         Calibration.Recognized += CalibrationOnRecognized;
      }

      private void CalibrationOnRecognized(object sender, CalibratedWeight calibratedWeight)
      {
         weightReport.Weight = calibratedWeight;
      }

   }
}