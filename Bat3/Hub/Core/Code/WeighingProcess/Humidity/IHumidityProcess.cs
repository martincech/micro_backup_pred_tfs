﻿using Communication.Readers;
using Core.Sensors.Humidity;

namespace Core.WeighingProcess.Humidity
{
   public interface IHumidityProcess
   {
      HumiditySensor HumiditySensor { get; }
      void ProcessFromSensor(IHumidityReader sensor);
   }
}
