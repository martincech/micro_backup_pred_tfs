﻿using Communication.Readers;
using Core.Sensors.Temperature;

namespace Core.WeighingProcess.Temperature
{
   public interface ITemperatureProcess
   {
      TemperatureSensor TemperatureSensor { get; }
      void ProcessFromSensor(ITemperatureReader sensor);
   }
}