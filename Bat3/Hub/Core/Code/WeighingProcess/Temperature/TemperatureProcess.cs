﻿using Communication.Readers;
using Communication.Samples;
using Core.Sensors;
using Core.Sensors.Temperature;

namespace Core.WeighingProcess.Temperature
{
   internal class TemperatureProcess : ITemperatureProcess
   {
      private readonly ISensorHistory sensorHistory;

      public TemperatureProcess(IActualValueOnSensor temperatureReport, ISensorHistory sensorHistory)
      {
         this.sensorHistory = sensorHistory;
         TemperatureSensor = new TemperatureSensor(temperatureReport);
         TemperatureSensor.Values.CollectionChanged += Temperatures_CollectionChanged;
      }

      void Temperatures_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
      {
         foreach (var newItem in e.NewItems)
         {
            sensorHistory.TemperatureList.Add(newItem as TemperatureSample);
         }
      }

      #region Implementation of ITemperatureProcess
    
      public TemperatureSensor TemperatureSensor { get; private set; }

      public void ProcessFromSensor(ITemperatureReader sensor)
      {  
         TemperatureSensor.Reader = sensor;
      }

      #endregion
   }
}
