﻿using System;
using BatLibrary;
using Recognition.RecognitionParts;

namespace Core.Calibration
{
   /// <summary>
   /// Calibration of weight sensor
   /// </summary>
   internal class CalibrationProcess : ICalibrationProcess
   {
      private readonly ICalibrationProcessMediator processMediator;

      #region Private fiels

      private ICalibration calibration;

      #endregion

      public CalibrationProcess(
         ICalibrationProcessMediator processMediator)
      {
         this.processMediator = processMediator;
         processMediator.StartCalibration += ProcessMediatorOnStartCalibration;
         processMediator.CancelCalibration += ProcessMediatorOnCancelCalibration;
         processMediator.CurrentProcessState = CalibrationProcessState.DefaultInsertCalibrationWeightRequest;
      }

      #region Helpers

      private void CalibrationOnCalibrationWeightStabilized(object sender, Weight weight)
      {
         processMediator.CurrentProcessState = CalibrationProcessState.RemoveCalibrationWeightRequest;
      }

      private void CalibrationOnCalibrated(object sender, EventArgs eventArgs)
      {
         processMediator.CurrentProcessState = CalibrationProcessState.CalibrationComplete;
      }

      private void ProcessMediatorOnCancelCalibration(object sender, EventArgs eventArgs)
      {
         calibration.CancelCalibration();
         processMediator.CurrentProcessState = CalibrationProcessState.Canceled;
      }

      private void ProcessMediatorOnStartCalibration(object sender,
         Weight e)
      {
         if (processMediator.CurrentProcessState != CalibrationProcessState.DefaultInsertCalibrationWeightRequest)
         {
            throw new InvalidOperationException("The cal process allready in progress");
         }         
         calibration.StartCalibration(e);
         processMediator.CurrentProcessState = CalibrationProcessState.Sampling;
      }

      #endregion

      #region Implementation of ICalibrationProcess

      public void InitCalibration(ICalibration cal)
      {
         if (calibration != null)
         {
            calibration.CalibrationWeightStabilized -= CalibrationOnCalibrationWeightStabilized;
            calibration.Calibrated -= CalibrationOnCalibrated;
         }
         calibration = cal;
         calibration.CalibrationWeightStabilized += CalibrationOnCalibrationWeightStabilized;
         calibration.Calibrated += CalibrationOnCalibrated;

         calibration.FilterParams = new FilterParams {StableRange = new Weight(3), StableWindow = 5};


         processMediator.CurrentProcessState = CalibrationProcessState.DefaultInsertCalibrationWeightRequest;
      }

      #endregion
   }
}
