﻿using BatLibrary;

namespace Core.Statistics
{
   public interface IWeightStatistics
   {
      /// <summary>
      /// Number of birds weighted today
      /// </summary>
      int Count { get; set; }
      /// <summary>
      /// Average weight of the birds
      /// </summary>
      Weight Average { get; set; }
      /// <summary>
      /// Uniformity of the birds
      /// </summary>
      double Uniformity { get; set; }
      /// <summary>
      /// Coeficient of variation of the birds
      /// </summary>
      double Cv { get; set; }
      /// <summary>
      /// Standard deviation
      /// </summary>
      double Sigma { get; set; }
      /// <summary>
      /// Gain from yesterday
      /// </summary>
      Weight Gain { get; set; }
   }
}
