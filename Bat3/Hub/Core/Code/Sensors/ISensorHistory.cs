﻿using System.Collections.Generic;
using Communication.Samples;

namespace Core.Sensors
{
   public interface ISensorHistory
   {
      List<KeyValuePair<int, BatLibrary.Weight>> WeightList { get; }
      List<Co2Sample> Co2List { get; }
      List<HumiditySample> HumidityList { get; }
      List<TemperatureSample> TemperatureList { get; }
   }
}