﻿using Core.Sensors.Co2;
using Core.Sensors.Humidity;
using Core.Sensors.Temperature;
using Core.Sensors.Weight;

namespace Core.Sensors
{
   public interface IActualValueOnSensor : IActualWeightOnSensor, IActualTemperatureOnSensor, IActualCo2OnSensor, IActualHumidityOnSensor
   {
      bool TerminalConnected { get; set; }
      bool SensorsConnected { get; set; }
   }
}
