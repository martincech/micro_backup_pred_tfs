﻿namespace Core.Sensors.Temperature
{
   public interface IActualTemperatureOnSensor
   {
      double Temperature { get; set; }
   }
}