﻿using Communication.Readers;
using Communication.Samples;

namespace Core.Sensors.Temperature
{
   public class TemperatureSensor : Sensor<TemperatureSample>
   {
      private ITemperatureReader reader;
      private readonly IActualTemperatureOnSensor temperatureSensor;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public TemperatureSensor(IActualTemperatureOnSensor temperatureSensor)
      {
         this.temperatureSensor = temperatureSensor;
      }

      public ITemperatureReader Reader
      {
         get { return reader; }
         set
         {
            if (reader != null)
            {
               reader.NewSample -= NewSample;
            }
            reader = value;

            if (value == null) return;
            reader.NewSample += NewSample;
         }
      }

      private void NewSample(object sender, TemperatureSample e)
      {
         if (e == null) { return; }

         if (temperatureSensor != null)
         {
            temperatureSensor.Temperature = e.Value;
         }
         Values.Add(e);
      }
   }
}
