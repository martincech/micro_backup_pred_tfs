﻿namespace Core.Sensors.Co2
{
   public interface IActualCo2OnSensor
   {
      long Co2 { get; set; }
   }
}
