﻿using Communication.Readers;
using Communication.Samples;

namespace Core.Sensors.Co2
{
   public class Co2Sensor : Sensor<Co2Sample>
   {
      private ICo2Reader reader;
      private readonly IActualCo2OnSensor actualCo2OnSensor;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public Co2Sensor(IActualCo2OnSensor actualCo2OnSensor)
      {
         this.actualCo2OnSensor = actualCo2OnSensor;
      }

      public ICo2Reader Reader
      {
         get { return reader; }
         set
         {
            if (reader != null)
            {
               reader.NewSample -= NewSample;
            }
            reader = value;

            if (value == null) return;
            value.NewSample += NewSample;
         }
      }

      private void NewSample(object sender, Co2Sample e)
      {
         if (e == null) { return;}

         if (actualCo2OnSensor != null)
         {
            actualCo2OnSensor.Co2 = e.Value;
         }
         Values.Add(e);
      }
   }
}
