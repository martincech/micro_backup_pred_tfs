﻿using System.Collections.Generic;
using System.Linq;
using BatLibrary;
using TechTalk.SpecFlow;

namespace IntegrationTests
{
   [Binding]
   public sealed class InitScenario
   {
      #region StepArgumentTransformation

      [StepArgumentTransformation]
      private IEnumerable<int> IntValues(string values)
      {
         return values.Split(',').Select(int.Parse).AsEnumerable();
      }

      [StepArgumentTransformation]
      private Weight.WeightUnits ToUnit(string unit)
      {
         return unit == "Kg" ? Weight.WeightUnits.KG : Weight.WeightUnits.LB;
      }

      [StepArgumentTransformation]
      private double DoubleValue(string value)
      {
         return double.Parse(value);
      }

      #endregion
   }
}
