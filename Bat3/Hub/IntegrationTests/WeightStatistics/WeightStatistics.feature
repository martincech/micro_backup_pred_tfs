﻿Feature: Show weight statistics for this day
	In order to get weighing statistics of flock for this day
	As a farmer
	I want to see weight statistics for this day

Scenario Outline: Flock statistics are visible on the screen in selected units
   Given Expected weight is between <LowWeight> g and <HighWeight> g	
	And Each of raw signal <Signal> from the weight sensor is generated <N> times
	Then on the screen i see stattistics for this day with <Count> samples
	And with average weight <AverageWeight> g 

Examples: 
| Signal                      | N  | LowWeight | HighWeight | Count | AverageWeight |
| 750,0,990,0,1200,0,1250     | 15 | 70        | 130        | 7     | 101.8571      |
| 1250,400,1220,2230,1285,353 | 15 | 80        | 120        | 5     | 91.1400       |