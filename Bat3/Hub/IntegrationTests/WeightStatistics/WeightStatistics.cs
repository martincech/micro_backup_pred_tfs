﻿using System;
using System.Collections.Generic;
using BatLibrary;
using NUnit.Framework;
using Recognition.RecognitionParts;
using TechTalk.SpecFlow;

namespace IntegrationTests.WeightStatistics
{
   [Binding]
   public sealed class WeightSample : StepDefinitionsBase
   {
      private void Given()
      {
         ConnectSensorPack();
         App.MockMainWindowView.StartReading();
      }


      [Given(@"Expected weight is between (.*) g and (.*) g")]
      public void GivenExpectedWeightIsBetweenGAndG(double lowWeight, double highWeight)
      {
         Given();
         App.MockMainWindowView.Unit = Weight.WeightUnits.G;
         App.WeighingProcess.CloseDay(new AcceptanceParams()
         {
            AcceptanceSexMode = AcceptanceSexMode.Single,
            SexMode = Sex.Undefined,
            UndefinedHigh = new Weight(highWeight),
            UndefinedLow = new Weight(lowWeight),
            StepMode = AcceptanceStepMode.Both
         });
      }

      [Given(@"Each of raw signal (.*) from the weight sensor is generated (.*) times")]
      public void GivenEachOfRawSignalFromTheWeightSensorIsGeneratedTimes(IEnumerable<int> rawsignal, int times)
      {
         foreach (var value in rawsignal)
         {
            for (int j = 0; j < times; j++)
            {
               App.MockSensors.MockWeightReader.Raise(r => r.NewSample += null, null, new Communication.Samples.WeightSample(value, DateTime.Now));
            }
         }
      }

      [Then(@"on the screen i see stattistics for this day with (.*) samples")]
      public void ThenOnTheScreenISeeStattisticsForThisDayWithSamples(int count)
      {
         Assert.AreEqual(count, App.MockMainWindowView.Count);
      }

      [Then(@"with average weight (.*) g")]
      public void ThenWithAverageWeightG(double averageWeight)
      {
         Assert.AreEqual(Math.Round(averageWeight, 2), Math.Round(App.MockMainWindowView.Average, 2));
      }

   }
}
