﻿using Communication.Readers.Interfaces;
using TechTalk.SpecFlow;

namespace IntegrationTests
{
   [Binding]
   public class StepDefinitionsBase
   {
      protected MockApp App { get; private set; }
      protected bool SensorPackConnected { get; private set; }

      [BeforeScenario]
      public void ScenarionInit()
      {
         MockApp mockApp;
         if (ScenarioContext.Current.TryGetValue(out mockApp))
         {
            App = mockApp;
         }
         else
         {
            App = new MockApp();
            ScenarioContext.Current.Set(App);
         }
      }

      protected void ConnectSensorPack(ISensorReaderPack readerPack = null)
      {
         if (SensorPackConnected) return;

         if (readerPack == null)
         {
            App.MockSensors.ConnectedSensorsManager.Raise(r => r.DeviceConnected += null, null,
               App.MockSensors.MockSensorReaderPack.Object);
         }
         else
         {
            App.MockSensors.ConnectedSensorsManager.Raise(r => r.DeviceConnected += null, null,
              readerPack);
         }
         SensorPackConnected = true;
      }
   }
}
