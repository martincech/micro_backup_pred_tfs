﻿using System;
using System.Reflection;
using BatLibrary;
using Communication.Samples;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.UnitTests.Samples
{
   [TestClass]
   public class WeightSampleTests
   {
      private static double _rawWeightConstant;
      private const double KILO = 1;
      private const double GRAMS_IN_KILO = 1000;
      private const double LIBRES_IN_KILO = 2.204623;
      private const double TOLERANCE = 0.0001;

      [ClassInitialize]
      public static void Init(TestContext context)
      {
         //var sample = new WeightSample();
         var po = new PrivateObject(new WeightSample());
         _rawWeightConstant = (double)po.GetField("WEIGHT_RAW_CONSTANT", BindingFlags.NonPublic | BindingFlags.Static);
      }


      [TestMethod]
      public void ConvertToUnit()
      {
         var sample = new WeightSample((int)(GRAMS_IN_KILO * _rawWeightConstant));

         Assert.IsTrue(Math.Abs(KILO - sample.ToUnit(Weight.WeightUnits.KG)) < TOLERANCE );
         Assert.IsTrue(Math.Abs(GRAMS_IN_KILO - sample.ToUnit(Weight.WeightUnits.G)) < TOLERANCE);
         Assert.IsTrue(Math.Abs(LIBRES_IN_KILO - sample.ToUnit(Weight.WeightUnits.LB)) < TOLERANCE);
      }

      [TestMethod]
      public void ConvertFromUnit()
      {
         var sample = new WeightSample();
         var expected = GRAMS_IN_KILO*_rawWeightConstant;

         Assert.AreEqual(0, sample.Value);
         sample.FromUnit(KILO, Weight.WeightUnits.KG);
         Assert.AreEqual(expected, sample.Value);
         sample.FromUnit(GRAMS_IN_KILO, Weight.WeightUnits.G);
         Assert.AreEqual(expected, sample.Value);
         sample.FromUnit(LIBRES_IN_KILO, Weight.WeightUnits.LB);
         Assert.AreEqual(expected, sample.Value);
      }
   }
}
