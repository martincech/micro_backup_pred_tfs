﻿using Communication.Readers;
using Communication.Samples;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.UnitTests.Readers
{
   [TestClass]
   public class HumidityReaderTest
   {
      private HumidityMockReaderTestBase humidityMockTest; 

      [TestInitialize]
      public void Init()
      {
         humidityMockTest = new HumidityMockReaderTestBase();
         var sensor = humidityMockTest.CreateSensorConnection();
         humidityMockTest.SetReader(new HumidityReader(sensor));
      }

      [TestMethod]
      public void NewSampleFired_WhenStartReading()
      {
         humidityMockTest.NewSampleFired_WhenStartReading();
      }

      [TestMethod]
      public void NewSampleFiredMultiple_WhenStartReading()
      {
         humidityMockTest.NewSampleFiredMultiple_WhenStartReading(true);
      }

      [TestMethod]
      public void ChangeReadingParameter()
      {
         humidityMockTest.ReadingParameterChange();
      }

   }

   internal class HumidityMockReaderTestBase : MockReaderTest<HumidityReader, HumiditySample, byte>
   {
      
   }

}
