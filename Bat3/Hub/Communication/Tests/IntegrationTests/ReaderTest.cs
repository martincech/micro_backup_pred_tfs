﻿using System;
using System.Linq;
using System.Threading;
using Communication.Readers;
using Communication.Samples;
using Communication.Tests.UnitTests.Readers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.IntegrationTests
{
   internal abstract class ReaderTest<T, U, V> : MockReaderTest<T, U, V> 
      where T : SampleReader<U>
      where U : SenzorSample<V>
   {  
      private const int BASE_INTERVAL = 150;
      private int SleepInterval { get { return Interval + BASE_INTERVAL; } }
      private const int REPEAT_COUNT = 3;

      public int Interval { get; set; }

      public override void NewSampleFired_WhenStartReading()
      {
         Reader.NewSample += (sender, args) =>
         {
            ReadValues.Add(args);
         };
         Assert.IsFalse(ReadValues.Any());
         Reader.StartReading();
         Thread.Sleep(SleepInterval);
         Assert.IsTrue(ReadValues.Any());
         Assert.AreEqual(ExpectedValue, Convert.ToInt32(ReadValues.Last().Value));
      }

      public void ReadSameSample(bool readOnlyDifferentValue)
      {
         NewSampleFired_WhenStartReading();
         Thread.Sleep(SleepInterval * REPEAT_COUNT);

         var expected = 1;
         if (!readOnlyDifferentValue)
         {
            expected = ExpectedReadItems(REPEAT_COUNT + 1);
         }
         Assert.AreEqual(expected, ReadValues.Count);
      }


      public override void NewSampleFiredMultiple_WhenStartReading(bool readWithSampleRate)
      {
         NewSampleFired_WhenStartReading();
         Assert.IsTrue(Reader.IsReading);

         for (var i = 1; i <= REPEAT_COUNT; i++)
         {
            InitMethodsOutput(i);
            Thread.Sleep(SleepInterval);
         }

         var expected = REPEAT_COUNT + 1;
         if (readWithSampleRate)
         {
            expected = ExpectedReadItems(REPEAT_COUNT + 1);
         }
         Assert.AreEqual(expected, ReadValues.Count);
      }

      public void NewSampleNotFired_WhenStopReading(bool readWithSampleRate)
      {
         NewSampleFiredMultiple_WhenStartReading(readWithSampleRate);
         var expectedValuesCount = ReadValues.Count;
         Assert.IsTrue(Reader.IsReading);
         Reader.StopReading();
         Assert.IsFalse(Reader.IsReading);
         Thread.Sleep(SleepInterval * REPEAT_COUNT);
         Assert.AreEqual(expectedValuesCount, ReadValues.Count);
      }

      public void NewSampleFired_WhenRestartReading(bool readWithSampleRate)
      {
         NewSampleFired_WhenStartReading();
         Assert.IsTrue(Reader.IsReading);
         Reader.StopReading();
         Assert.IsFalse(Reader.IsReading);
         Reader.StartReading();
         Assert.IsTrue(Reader.IsReading);

         const int count = REPEAT_COUNT;
         for (var i = 1; i <= count; i++)
         {
            InitMethodsOutput(i);
            Thread.Sleep(SleepInterval);
         }

         var expected = count + 1;
         if (readWithSampleRate)
         {
            expected = ExpectedReadItems(count + 1) - 1; //-1 for another start reading and read sample rate first
         }
         Assert.AreEqual(expected, ReadValues.Count);
      }

      #region Private helpers

      private int ExpectedReadItems(int times)
      {
         // recompute sample rate frequention to timer rate
         const double sampleRateInterval = 1.0 / SAMPLE_RATE * 1000;
         var ratio = (int)Math.Ceiling(Interval / sampleRateInterval);
         return times * ratio - 1;
      }

      #endregion
   }
}
