﻿using System;
using System.Reflection;
using Communication.Readers;
using Communication.Samples;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Communication.Tests.IntegrationTests
{
   [TestClass]
   public class TemperatureSensorTests
   {
      private TemperatureReaderTestBase temperatureTest;
      private double readInterval;

      [TestInitialize]
      public void Init()
      {
         temperatureTest = new TemperatureReaderTestBase();
         var sensor = temperatureTest.CreateSensorConnection();
         var reader = new TemperatureReader(sensor);
         temperatureTest.SetReader(reader);

         var po = new PrivateObject(reader);
         readInterval = (double)po.GetField("SAMPLE_RATE_READ_PERIOD_MS", BindingFlags.NonPublic | BindingFlags.Static);
         temperatureTest.Interval = (int)Math.Ceiling(readInterval);
      }

      [TestMethod]
      public void Read_OneSample()
      {
         temperatureTest.NewSampleFired_WhenStartReading();
      }

      [TestMethod]
      public void Read_SameSampleMultiply()
      {
         temperatureTest.ReadSameSample(true);
      }

      [TestMethod]
      public void Read_MultipleDifferentSamples()
      {
         temperatureTest.NewSampleFiredMultiple_WhenStartReading(false);
      }

      [TestMethod]
      public void NotRead_WhenStopReading()
      {
         temperatureTest.NewSampleNotFired_WhenStopReading(false);
      }

      [TestMethod]
      public void Read_WhenRestartReading()
      {
         temperatureTest.NewSampleFired_WhenRestartReading(false);
      }
   }

   internal class TemperatureReaderTestBase : ReaderTest<TemperatureReader, TemperatureSample, double>
   {
   }
}
