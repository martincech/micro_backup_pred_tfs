﻿using System;
using BatLibrary;

namespace Communication.Samples
{
   [Serializable]
   public class WeightSample : SenzorSample<int>
   {
      private const double WEIGHT_RAW_CONSTANT = 10.0;
      
      internal WeightSample(int value, DateTime timeStamp)
      {
         Value = value;
         TimeStamp = timeStamp;
      }

      internal WeightSample(int value)
         : this(value, DateTime.Now)
      {
         
      }

      public WeightSample()
      {
      }

      public double ToUnit( Weight.WeightUnits toWeightUnit)
      {
         return ConvertWeight.Convert(Value/WEIGHT_RAW_CONSTANT, Weight.WeightUnits.G, toWeightUnit);
      }

      public void FromUnit(double value, Weight.WeightUnits unit)
      {
         Value = (int) (ConvertWeight.Convert(value, unit, Weight.WeightUnits.G)*WEIGHT_RAW_CONSTANT);
      }
   }
}
