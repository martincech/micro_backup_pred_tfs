﻿using System;
using Communication.Readers;

namespace Communication.SensorManagers
{
   public interface ITemperatureSensorManager
   {
      event EventHandler<ITemperatureReader> DeviceConnected;
      event EventHandler<ITemperatureReader> DeviceDisconnected;
   }
}