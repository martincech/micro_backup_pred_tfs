﻿using System;
using Communication.Readers;

namespace Communication.SensorManagers
{
   public interface IHumiditySensorManager
   {
      event EventHandler<IHumidityReader> DeviceConnected;
      event EventHandler<IHumidityReader> DeviceDisconnected;
   }
}
