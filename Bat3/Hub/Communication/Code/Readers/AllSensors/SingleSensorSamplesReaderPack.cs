using System;
using Communication.Readers.Interfaces;
using Communication.SensorConnection;

namespace Communication.Readers.AllSensors
{
   internal abstract class SingleSensorSamplesReaderPack : ISensorReaderPack
   {

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      internal SingleSensorSamplesReaderPack(ISensorConnection sensorConnection) :
         this(new WeightReader(sensorConnection), new TemperatureReader(sensorConnection),
         new Co2Reader(sensorConnection),new HumidityReader(sensorConnection))
      {
         sensorConnection.ConnectionLost += (sender, args) => { OnConnectionLost(args); };
      }

            /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      protected SingleSensorSamplesReaderPack(IWeightReader weightReader, ITemperatureReader temperatureReader, ICo2Reader co2Reader, IHumidityReader humidityReader)
      {
         WeightReader = weightReader;
         TemperatureReader = temperatureReader;
         Co2Reader = co2Reader;
         HumidityReader = humidityReader;
      }

      public IWeightReader WeightReader { get; private set; }
      public ITemperatureReader TemperatureReader { get; private set; }
      public ICo2Reader Co2Reader { get; private set; }
      public IHumidityReader HumidityReader { get; private set; }

      public event EventHandler<DateTime> ConnectionLost;

      protected virtual void OnConnectionLost(DateTime e)
      {
         var handler = ConnectionLost;
         if (handler != null) handler(this, e);
      }
   }
}