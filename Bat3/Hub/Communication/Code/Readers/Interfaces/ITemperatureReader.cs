using Communication.Samples;

namespace Communication.Readers
{
   public interface ITemperatureReader : ISampleReader<TemperatureSample>
   {
   }
}