﻿using System;

namespace Communication.Readers.Interfaces
{
   public interface ISensorReaderPack
   {
      IWeightReader WeightReader { get; }
      ITemperatureReader TemperatureReader { get; }
      IHumidityReader HumidityReader { get; }
      ICo2Reader Co2Reader { get; }

      event EventHandler<DateTime> ConnectionLost;
   }
}
