﻿using System.Collections.Generic;
using System.Linq;

namespace Saver.Interfaces
{
   /// <summary>
   /// Interface of wrapper for <see cref="Saver"/>
   /// </summary>
   /// <typeparam name="T"></typeparam>
   public interface IPersistentStorage<T>
   {
      void Append(T data);
      void Append(IEnumerable<T> dataList);
      void Update(T data);
      void Update(IEnumerable<T> dataList);
     
      IQueryable<T> Read();
   }
}
