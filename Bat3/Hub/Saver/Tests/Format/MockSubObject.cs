using System;

namespace SaverTests.Format
{
   [Serializable]
   public class MockSubObject
   {
      public string StringValue { get; set; }
   }
}