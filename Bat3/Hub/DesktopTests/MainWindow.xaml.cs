﻿using System;
using System.Windows;
using BatLibrary;

namespace DesktopTests
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      public MainWindow()
      {
         InitializeComponent();
      }

      private void ConvertorTests_Click(object sender, RoutedEventArgs e)
      {
         Converters.ConvertorView converterView = new Converters.ConvertorView()
         {
            Owner = this,
            DataContext = new Converters.ConvertorViewModel()
            {
               WeightOne = new Weight(500),
               WeightTwo = new Weight(1000)
            }
         };
         converterView.ShowDialog();
      }
   }
}
