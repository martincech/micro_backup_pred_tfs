﻿namespace Desktop
{
   public enum SensorType
   {
      Weight,
      CarbonDioxide,
      Temperature,
      Humidity
   }
}