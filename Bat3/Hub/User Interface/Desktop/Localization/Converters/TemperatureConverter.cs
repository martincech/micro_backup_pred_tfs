﻿using System;
using System.Globalization;
using System.Windows.Data;
using BatLibrary;

namespace Desktop.Localization.Converters
{
   public class TemperatureConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         var unit = parameter as Temperature? ?? Temperature.Celsius;
         var temperature = (double)value;
         if (unit == Temperature.Fahrenheit)
         {
            temperature = temperature * 1.8 + 32;
         }

         return temperature;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new InvalidOperationException(string.Format("Invalid use of converter!"));
      }
   }
}
