﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Desktop.Presentation.Converters
{
   /// <summary>
   /// Implements <see cref="IValueConverter"/> interface on <see cref="Localization.Converters.SexConverter"/>.
   /// </summary>
   public class SexConverter : Localization.Converters.SexConverter, IValueConverter
   {
      #region Implementation of IValueConverter

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.Convert(this, value, targetType, parameter, culture);
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         return EnumConverterExtension.ConvertBack(this, value, targetType, parameter, culture);
      }

      #endregion
   }
}