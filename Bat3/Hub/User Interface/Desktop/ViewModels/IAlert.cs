namespace Desktop.ViewModels
{
   public interface IAlert
   {
      string Text { get; set; }
      string Title { get; set; }
   }
}