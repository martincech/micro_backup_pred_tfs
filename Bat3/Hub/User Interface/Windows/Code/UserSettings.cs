﻿using System;
using System.Linq;
using BatLibrary;
using Saver.Interfaces;

namespace Core.Data
{
   [Serializable]
   public class UserSettings
   {
      public Weight.WeightUnits WeightUnit { get; set; }
      public Temperature TemperatureUnit { get; set; }
      public int CalibrationOffset { get; set; }
      public double CalibrationMultiplier { get; set; }

      public Curve MaleGrowthCurve { get; set; }
      public Curve FemaleGrowthCurve { get; set; }
      public Curve UndefinedGrowthCurve { get; set; }

      public int MaleGrowthDeviation { get; set; }
      public int FemaleGrowthDeviation { get; set; }
      public int UndefinedGrowthDeviation { get; set; }

      public void Load(IPersistentStorage<UserSettings> storage)
      {
         var us = storage.Read().FirstOrDefault();
         if (us == null) return;
         WeightUnit = us.WeightUnit;
         TemperatureUnit = us.TemperatureUnit;
         CalibrationOffset = us.CalibrationOffset;
         CalibrationMultiplier = us.CalibrationMultiplier;
         MaleGrowthCurve = us.MaleGrowthCurve;
         FemaleGrowthCurve = us.FemaleGrowthCurve;
         MaleGrowthDeviation = us.MaleGrowthDeviation;
         FemaleGrowthDeviation = us.FemaleGrowthDeviation;
         UndefinedGrowthCurve = us.UndefinedGrowthCurve;
      }

      public void Save(IPersistentStorage<UserSettings> storage)
      {
         storage.Update(this);
      }
   }
}
