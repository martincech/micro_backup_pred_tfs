﻿using System.Windows;

namespace UI.Windows.Views
{
   /// <summary>
   /// Interaction logic for AlertView.xaml
   /// </summary>
   public partial class AlertView : Window
   {
      public AlertView()
      {
         InitializeComponent();
         Owner = Application.Current.MainWindow;
      }

      private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
      {
         Close();
      }
   }
}
