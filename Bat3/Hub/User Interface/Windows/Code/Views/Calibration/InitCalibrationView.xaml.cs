﻿using System.Windows;
using System.Windows.Controls;

namespace UI.Windows.Views.Calibration
{
   /// <summary>
   /// Interaction logic for PutWeightView.xaml
   /// </summary>
   public partial class InitCalibrationView : UserControl
   {
      public InitCalibrationView()
      {
         InitializeComponent();
      }

      public void ButtonBase_OnClick(object sender, RoutedEventArgs e)
      {
         Window parentWindow = Window.GetWindow(this);
         parentWindow.Close();
      }
   }
}
