﻿using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Saver.Format;
using Saver.Storage;

namespace CoreTests.Data
{
   [TestClass]
   public class SensorsHistoryTest
   {
      private const string TEMP_FILE = "SensorsHistory.tmp";
      private SensorsHistory sensorsHistory;
      private BasePersistentStorage<SensorsHistory> storage;

      [TestInitialize]
      public void Init()
      {
         storage = new BasePersistentStorage<SensorsHistory>(new XmlSaver<SensorsHistory>(TEMP_FILE));
         sensorsHistory = new SensorsHistory();
      }

      [TestCleanup]
      public void CleanUp()
      {
         if (File.Exists(TEMP_FILE))
         {
            File.Delete(TEMP_FILE);
         }
      }

      [TestMethod]
      public void Save_SensorsData_WhenNotExist()
      {
         sensorsHistory.Weights.Add(TestData.Weights[0]);
         sensorsHistory.Weights.Add(TestData.Weights[1]);

         sensorsHistory.Temperatures.Add(TestData.Temperatures[0]);
         sensorsHistory.Temperatures.Add(TestData.Temperatures[1]);

         Assert.IsFalse(File.Exists(TEMP_FILE));
         sensorsHistory.Save(storage);
         Assert.IsTrue(File.Exists(TEMP_FILE));
         sensorsHistory.Load(storage);
         CheckSensorsHistoryData(sensorsHistory, TestData.Weights, TestData.Temperatures);
      }

      [TestMethod]
      public void Load_SensorsData_WhenNotExist()
      {
         sensorsHistory.Load(storage);
         Assert.IsFalse(sensorsHistory.Weights.Any());
         Assert.IsFalse(sensorsHistory.Temperatures.Any());
      }

      private void CheckSensorsHistoryData(SensorsHistory sensorsHistory, List<BirdWeight> weights, List<double> temperatures)
      {
         Assert.AreEqual(weights.Count, sensorsHistory.Weights.Count);

         for (int i = 0; i < weights.Count; i++)
         {
            var value = sensorsHistory.Weights[i];
            AreEqual(weights[i], value);
         }

         Assert.AreEqual(temperatures.Count, sensorsHistory.Temperatures.Count);

         for (int i = 0; i < temperatures.Count; i++)
         {
            var value = sensorsHistory.Temperatures[i];
            Assert.AreEqual(temperatures[i], value);
         }
      }

      private void AreEqual(BirdWeight expected, BirdWeight result)
      {
         Assert.AreEqual(expected.Sex, result.Sex);
      }

   }
}
