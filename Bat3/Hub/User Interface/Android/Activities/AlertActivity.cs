using System;
using Android.App;
using Android.OS;
using Android.Widget;

namespace UI.Android.Activities
{
   [Activity(Theme = "@android:style/Theme.Holo.Light.Dialog", Label = "AlertActivity")]
   public class AlertActivity : Activity
   {
      protected override void OnCreate(Bundle bundle)
      {
         base.OnCreate(bundle);
         Title = Resources.GetString(Resource.String.Error);
         SetContentView(Resource.Layout.Alert);

         var closeButton = FindViewById<Button>(Resource.Id.closeButton);
         closeButton.Click += CloseButtonOnClick;
      }

      private void CloseButtonOnClick(object sender, EventArgs eventArgs)
      {
         Finish();
      }
   }
}