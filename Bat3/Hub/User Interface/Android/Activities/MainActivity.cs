﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using BatLibrary;

namespace UI.Android.Activities
{
   [Activity(Label = "UI.Android", MainLauncher = true, ScreenOrientation = ScreenOrientation.Landscape)]
   public class MainActivity : Activity
   {
      #region Private fields

      private TextView avgWeightUnit;
      private TextView actualWeightUnit;
      private TextView temperatureUnit;
      private ImageView warningImage;

      #endregion

      protected override void OnCreate(Bundle bundle)
      {
         base.OnCreate(bundle);
         SetContentView(Resource.Layout.Main);

         var dummyWarningButton = FindViewById<Button>(Resource.Id.DummyWarningButton);
         dummyWarningButton.Click += DummyWarningOnClick;

         //TODO: load user config

         avgWeightUnit = FindViewById<TextView>(Resource.Id.AvgWeightUnitTextView);
         actualWeightUnit = FindViewById<TextView>(Resource.Id.ActualWeightUnitTextView);
         temperatureUnit = FindViewById<TextView>(Resource.Id.ActualTemperatureUnitTextView);
         warningImage = FindViewById<ImageView>(Resource.Id.WarningImageView);
         warningImage.Visibility = ViewStates.Gone;

         var unitsRadioGroup = FindViewById<RadioGroup>(Resource.Id.radioUnits);
         unitsRadioGroup.CheckedChange += UnitsRadioGroupOnCheckedChange;
         var temperatureUnitsRadioGroup = FindViewById<RadioGroup>(Resource.Id.radioTemperature);
         temperatureUnitsRadioGroup.CheckedChange += TemperatureUnitsRadioGroupOnCheckedChange;
      }

      #region Private helpers

      private void TemperatureUnitsRadioGroupOnCheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
      {
         var unit = "C";
         switch (e.CheckedId)
         {
            case Resource.Id.radioTempC:
               unit = "C";
               break;
            case Resource.Id.radioTempF:
               unit = "F";
               break;
         }
         ChangeTemperatureUnits(unit);
      }

      private void UnitsRadioGroupOnCheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
      {
         var unit = Weight.WeightUnits.KG;
         switch (e.CheckedId)
         {
            case Resource.Id.radioUnitsKg:
               unit = Weight.WeightUnits.KG;
               break;
            case Resource.Id.radioUnitsLb:
               unit = Weight.WeightUnits.LB;
               break;
         }
         ChangeUnits(unit);
      }

      private void DummyWarningOnClick(object sender, EventArgs eventArgs)
      {
         var alertIntent = new Intent(this, typeof (AlertActivity));
         StartActivity(alertIntent);

         warningImage.Visibility = ViewStates.Visible;
      }

      private void ChangeUnits(Weight.WeightUnits weightUnit)
      {
         //TODO refactoring - string from resources in Core project
         var strId = Resource.String.KG;
         if (weightUnit == Weight.WeightUnits.LB)
         {
            strId = Resource.String.LB;
         }
         var text = Resources.GetString(strId);

         avgWeightUnit.Text = text;
         actualWeightUnit.Text = text;
         //TODO - convert weight (in Core)
      }

      private void ChangeTemperatureUnits(string unit)
      {
         //TODO refactor as ChangeUnits
         var strId = Resource.String.Celsius;
         if (unit.Equals("F"))
         {
            strId = Resource.String.Fahrenheit;
         }
         var text = Resources.GetString(strId);
         temperatureUnit.Text = text;
      }

      #endregion
   }
}

