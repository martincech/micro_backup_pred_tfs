﻿using System.Collections.Generic;
using System.Linq;
using PSD.Core.Models;
using PSD.Core.Repository.Interfaces;

namespace PSD.Core.Repository
{
   public class CagesCsvRepository : ICagesRepository
   {   
      #region Constructors

      public CagesCsvRepository()
      {      
         Cages = new List<Cage>();      
      }

      #endregion    

      /// <summary>
      /// Parse data and save to <see cref="Cages"/>.
      /// </summary>
      /// <param name="data"></param>
      public void Parse(byte[] data)
      {
         if (data == null)
         {
            ((List<Cage>)Cages).Clear();
            return;
         }

         char delimiter;
         char.TryParse(Properties.Resources.CsvDelimiter,  out delimiter);
         var parser = new CagesCsvParser(data, delimiter);
         Cages = parser.Parse();       
      }

      #region Implementation of ICagesRepository

      public int GetFreeCageNumber()
      {
         var maxNumber = 0;
         if (Cages.Any())
         {
            maxNumber = Cages.Max(i => i.CageNumber);
         }
         return maxNumber + 1;
      }

      public IEnumerable<Cage> Cages { get; private set; }

      public bool Add(Cage cage)
      {
         if (((List<Cage>)Cages).Contains(cage))
         {
            return false;
         }
         ((List<Cage>)Cages).Add(cage);
         OrderList();
         return true;
      }

      public bool Update(Cage cage)
      {
         var cageAt = ((List<Cage>)Cages).IndexOf(cage);
         if (cageAt < 0)
         {
            return false;
         }
         ((List<Cage>)Cages)[cageAt] = cage;
         OrderList();
         return true;
      }

      public bool Delete(Cage cage)
      {
         if (!((List<Cage>)Cages).Remove(cage))
         {
            return false;
         }
         OrderList();
         return true;
      }

      public IQueryable<Cage> All()
      {
         return Cages.AsQueryable();
      }

      #endregion

      private void OrderList()
      {
         Cages = Cages.OrderBy(c => c.CageNumber).ToList(); // order by cage number
      }
   }
}
