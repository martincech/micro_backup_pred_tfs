﻿using System;
using System.Globalization;
using PSD.Core.Enums;

namespace PSD.Core.Helpers
{
   public static class ParameterEToString
   {
      public static string ConvertToString(this ParameterE argument, double weight = -1)
      {
         switch (argument)
         {
            case ParameterE.ParameterOk:
               if (weight > 0)
               {
                  return weight.ToString(new CultureInfo(Settings.CultureName)) + " " + Settings.Unit;
               }
               return ""; // dont want to show any text on OK parameter
            case ParameterE.ParameterTwin:
               return Properties.Resources.ParameterE_TWIN;
            case ParameterE.ParameterCracked:
               return Properties.Resources.ParameterE_CRACKED;
            case ParameterE.ParameterMembrane:
               return Properties.Resources.ParameterE_MEMBRANE;
            case ParameterE.ParameterDead:
               return Properties.Resources.ParameterE_DEAD;
            default:
               return null;
         }
      }

      public static ParameterE ConvertFromString(string argument)
      {
         if (argument == Properties.Resources.ParameterE_TWIN)
            return ParameterE.ParameterTwin;
         if (argument == Properties.Resources.ParameterE_CRACKED)
            return ParameterE.ParameterCracked;
         if (argument == Properties.Resources.ParameterE_MEMBRANE)
            return ParameterE.ParameterMembrane;
         if (argument == Properties.Resources.ParameterE_DEAD)
            return ParameterE.ParameterDead;
         if (argument == "")
            return ParameterE.ParameterOk;

         throw new ArgumentException("Invalid argument: " + argument);
      }

   }
}
