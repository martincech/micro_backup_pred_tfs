﻿namespace PSD.Core.EggScale.Interfaces
{
   public interface IEggScale : IScale
   {
      /// <summary>
      /// Set scale weight to zero.
      /// </summary>
      void Tare();

      /// <summary>
      /// Change backlight - on, of, auto
      /// </summary>
      void Backlight();

      /// <summary>
      /// Calibration
      /// </summary>
      void Calibration();
 
      /// <summary>
      /// Quantify of samples
      /// </summary>
      void Count();

      /// <summary>
      /// Units can be changed between: g, ozt, ct, lb, oz, dwt and GN
      /// </summary>
      void ChangeUnit();

      /// <summary>
      /// Start reading current weight.
      /// </summary>
      void Start();

      /// <summary>
      /// Stop reading current weight.
      /// </summary>
      void Stop();
   }
}
