﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using PSD.Core.EggScale.Interfaces;

namespace PSD.Core.EggScale
{
   public class Protocol : IEggScale
   {
      #region Private Fields

      private Stream _inputStream;
      private Stream _outputStream;
      private const int BufferSize = 14;
      private Timer _timer;
      private const int ReadInterval = 300;              //ms
      private const double Tolerance = 0.01;
      private const double CorrectionError = 0.100001;
      private const double LowLimit = 10;                // minimal distinguishable value

      // variables for reading from scale
      private double _lastValue = -1;        // last receive weight;
      private double _sample;                // last read sample;
      private bool _checkSample;             // test sample
      private bool _reading;                 // read data flag
      private bool _isBallanced;             // test sample and last value

      // indexes for parsing data
      private const int SignIndex = 0;
      private const int DataIndex = 2;
      private const int DataLength = 7;
      private const int UnitIndex = 9;
      private const int UnitLength = 3;

      // unit convert constants
      private const double TransferConstantLb = 453.59;
      private const double TransferConstantCt = 0.2;
      private const double TransferConstantDwt = 1.555;
      private const double TransferConstantOz = 28.35;
      private const double TransferConstantOzt = 31.10;
      private const double TransferConstantGn = 0.0648;

      #endregion

      #region Public interfaces

      public Protocol(Stream inputStream, Stream outputStream)
      {
         _inputStream = inputStream;
         _outputStream = outputStream;
      }

      #region Implementation of IEggScale

      public event EventHandler<CommandArgs> EggWeighted;

      public void Tare()
      {
         Send(Commands.Tare);
      }

      public void Backlight()
      {
         Send(Commands.Backlight);
      }

      public void Calibration()
      {
         Send(Commands.Calibration);
      }

      public void Count()
      {
         Send(Commands.Count);
      }

      public void ChangeUnit()
      {
         Send(Commands.UnitConversion);
      }

      /// <summary>
      /// Start reading weight from scale. 
      /// </summary>
      public void Start()
      {
         if (_reading) return;   // if reading is already started, not run next

         _reading = true;
         _timer = new Timer(Callback, null, ReadInterval, Timeout.Infinite);
      }

      /// <summary>
      /// Stop reading weight from scale.
      /// </summary>
      public void Stop()
      {
         _reading = false;
      }

      #endregion

      /// <summary>
      /// Call direct command.
      /// </summary>
      /// <param name="command"></param>
      /// <returns></returns>
      public void SendRaw(string command)
      {
         var data = new byte[command.Length + 1];
         data[0] = (byte)Commands.DefaulState;
         for (var i = 1; i < data.Length; i++)
         {
            data[i] = Convert.ToByte(command[i - 1]);
         }
         _outputStream.Write(data, 0, data.Length);
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Check if weight is different than previous value.
      /// </summary>
      /// <param name="weight">tested weight</param>
      /// <returns>true - weight is different</returns>
      private bool WeightIsDifferent(double weight)
      {
         return Math.Abs(weight - _lastValue) > CorrectionError ||
                (Math.Abs(_lastValue) < Tolerance && Math.Abs(weight) > Tolerance);
      }

      /// <summary>
      /// Timer service.
      /// </summary>
      /// <param name="state"></param>
      private void Callback(object state)
      {
         if (!_reading) return;

         Print(); // send request for current weight
         StartReading();
         _timer.Change(ReadInterval, Timeout.Infinite);
      }

      /// <summary>
      /// Read data from scale.
      /// </summary>
      private void StartReading()
      {
         var task = new Task(() =>
         {  // read data from scale              
            try
            {
               var data = Read();
               var weight = ConvertWeight(data);
               if (weight < 0) return;

               if (_checkSample && Math.Abs(weight - _sample) < Tolerance)
               {  // 2 samples in row have the same value
                  _isBallanced = true;
                  _checkSample = false;
               }
               else
               {  // sample with different value
                  _isBallanced = false;
                  _sample = weight;
                  _checkSample = true;
               }

               if (!_isBallanced || !WeightIsDifferent(weight) || weight < LowLimit) return;
               
               _lastValue = weight;
               // weight is correct only if 2 samples in row have the same value and
               // previous weight is different.
               InvokeEvent(weight);
            }
            catch (Exception)
            {
               InvokeEventError();
            }         
         });
         task.Start();
      }

      /// <summary>
      /// Convert data to weight value.
      /// </summary>
      /// <param name="data">incoming data</param>
      /// <returns>converted weight in grams; 
      /// return -1 if data is not correct</returns>
      private double ConvertWeight(byte[] data)
      {
         if (data.Length != BufferSize) return -1;
         var sign = Convert.ToChar(data[SignIndex]) == '-' ? -1 : 1;

         double number;
         var value = "";
         for (var i = DataIndex; i < DataLength + DataIndex; i++)
         {
            var c = Convert.ToChar(data[i]);
            value += c;
         }

         //protocol mistake - 3 letters unit
         var letter = "";
         if (char.IsLetter(value[value.Length - 1]))
         {
            letter = value[value.Length - 1].ToString();
            value = value.Remove(value.Length - 1, 1);   // remove last char, which is begining of unit
         }
         if (!double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out number))
         {
            return -1;
         }

         // check units and convert to grams eventually
         var unit = ParseUnit(data);
         unit = letter + unit;
         switch (unit)
         {
            case "g":
               break;
            case "lb":
               number *= TransferConstantLb;
               break;
            case "ct":
               number *= TransferConstantCt;
               break;
            case "dwt":
               number *= TransferConstantDwt;
               break;
            case "oz":
               number *= TransferConstantOz;
               break;
            case "ozt":
               number *= TransferConstantOzt;
               break;
            case "GN":
               number *= TransferConstantGn;
               break;
            default:
               return -1;
         }

         return Math.Round(number * sign, 1);
      }

      /// <summary>
      /// Get unit from data.
      /// </summary>
      /// <param name="data">read data</param>
      /// <returns>unit</returns>
      private string ParseUnit(byte[] data)
      {
         var unit = "";
         for (var j = UnitIndex; j < UnitLength + UnitIndex; j++)
         {
            unit += Convert.ToChar(data[j]);
         }
         unit = unit.Trim();
         return unit;
      }

      private void InvokeEvent(double weight)
      {
         if (EggWeighted != null)
         {
            EggWeighted(this, new CommandArgs(weight));
         }
      }

      private void InvokeEventError()
      {
         if (EggWeighted != null)
         {
            var arg = new CommandArgs(-1) {State = State.Error};
            EggWeighted(this, arg);
         }
      }

      /// <summary>
      /// Send request for current weight.
      /// </summary>
      private void Print()
      {
         Send(Commands.Print);
      }

      /// <summary>
      /// Send command to output stream.
      /// </summary>
      /// <param name="command">command</param>
      private void Send(Commands command)
      {
         var data = new[] { (byte)Commands.DefaulState, (byte)command };      
         _outputStream.Write(data, 0, data.Length);      
      }

      /// <summary>
      /// Read data from input stream.
      /// </summary>
      /// <returns></returns>
      private byte[] Read()
      {
         var buffer = new byte[BufferSize];
         _inputStream.Read(buffer, 0, buffer.Length);
         return buffer;
      }

      #endregion
   }
}
