using PSD.Core.Enums;

namespace PSD.Core.Models
{
   public class CageRecord
   {
      public ParameterE Type;
      public int Count;
      public double Weight;

      public CageRecord()
      {
         Weight = -1;
      }
   }
}