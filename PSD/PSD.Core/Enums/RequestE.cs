﻿namespace PSD.Core.Enums
{
   public enum RequestE
   {
      ConnectedDevice = 10,
      EnableBt,
      NewCampaign,
      PressedKeyCode
   }
}
