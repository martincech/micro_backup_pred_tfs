﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using PSD.Core.Enums;
using PSD.Core.Helpers;
using PSD.Core.Repository.Interfaces;

namespace PSD.Core.Extensions
{
   public static class RepositoryExtensionMethods
   {
      private static readonly string NewLine = Environment.NewLine;

      /// <summary>
      /// It Prepares list to writing to file. 
      /// </summary>
      /// <returns>csv file as string</returns>
      public static string ToWriteString(this ICagesRepository repository, IEnumerable<string> header)
      {
         var csv = new StringBuilder();
         var delimiter = Properties.Resources.CsvDelimiter;
         var culture = new CultureInfo(Settings.CultureName);

         // header
         var enumerable = header as IList<string> ?? header.ToList();
         var lastItem = enumerable.LastOrDefault();
         foreach (var item in enumerable)
         {
            csv.Append(item);
            if (item != lastItem)
            {
               csv.Append(delimiter);
            }
         }
         csv.Append(NewLine);

         // content
         foreach (var cage in repository.Cages)
         {
            foreach (var record in cage.Records)
            {
               csv.Append(cage.CageNumber + delimiter);
               var count = record.Count.ToString();
               var parameter = record.Type.ConvertToString();
               if (record.Type == ParameterE.ParameterDead)
               {
                  count = "";
               }
               // check weight
               var weight = "";
               if (record.Type == ParameterE.ParameterOk && record.Weight > 0)
               {
                  weight = record.Weight.ToString(culture);
               }
               csv.Append(count + delimiter + parameter + delimiter + weight + NewLine);
            }

            //empty cage
            if (!cage.Records.Any())
            {
               csv.Append(cage.CageNumber + delimiter + delimiter + delimiter + NewLine);
            }
         }
         return csv.ToString();
      }
   }
}
