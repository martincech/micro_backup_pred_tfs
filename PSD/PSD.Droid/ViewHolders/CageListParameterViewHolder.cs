using Android.Widget;

namespace PSD.Droid.ViewHolders
{
   public class CageListParameterViewHolder : Java.Lang.Object
   {
      public TextView Parameter { get; set; }
      public TextView Count { get; set; }
   }
}