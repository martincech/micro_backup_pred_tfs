using System;
using System.Text.RegularExpressions;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using PSD.Core;
using PSD.Core.Repository.Interfaces;

namespace PSD.Droid.Activities
{
   [Activity(Label = "PickerActivity", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize,
      ScreenOrientation = ScreenOrientation.Portrait)]
   public class PickerActivity : BaseActivity<ISettings>
   {
      private EditText pickerName;

      protected override void OnCreate(Bundle bundle)
      {
         base.OnCreate(bundle);
         SetContentView(Resource.Layout.Picker);

         pickerName = FindViewById<EditText>(Resource.Id.pickerNameEditText);
         pickerName.Text = ViewModel.PickerName;
         var okButton = FindViewById<Button>(Resource.Id.OkButton);
         var cancelButton = FindViewById<Button>(Resource.Id.cancelButton);

         cancelButton.Click += CancelButtonOnClick;
         okButton.Click += OkButton_Click;
         pickerName.FocusChange += PickerNameOnFocusChange;
         pickerName.KeyPress += PickerNameOnKeyPress;
      }

      #region Private helpers

      private void PickerNameOnKeyPress(object sender, View.KeyEventArgs e)
      {
         if (e.Event.Action == KeyEventActions.Down && (e.KeyCode == Keycode.Enter || e.KeyCode == Keycode.NumpadEnter))
         {
            // cancel inserting enter to edit box
            e.Handled = true;
         }
         else
         {
            e.Handled = false;
         }

         if (e.Event.Action == KeyEventActions.Up && (e.KeyCode == Keycode.Enter || e.KeyCode == Keycode.NumpadEnter))
         {
            // service on key up, because if it was on keydown, the activity with new list was been started 
            // and keyup event was been catch in this new activity.
            e.Handled = true;
            OkButton_Click(this, null);
         }
      }

      private void PickerNameOnFocusChange(object sender, View.FocusChangeEventArgs args)
      {
         if (!args.HasFocus) return;

         ShowSoftwareKeyboard(this, true);
         pickerName.SetSelection(pickerName.Text.Length);
      }

      private void OkButton_Click(object sender, EventArgs e)
      {
         // check name if contains valid characters only
         if (!Regex.IsMatch(pickerName.Text, Settings.ValidFilePattern) &&
             pickerName.Text != string.Empty)
         {
            var alert = new AlertDialog.Builder(this);
            alert.SetTitle(Resources.GetString(Resource.String.Warning));
            alert.SetMessage(Resources.GetString(Resource.String.InvalidName));
            alert.SetPositiveButton("OK", (o, args) => { });
            var dialog = alert.Create();
            dialog.Show();

            pickerName.Text = "";
            return;
         }

         ViewModel.PickerName = pickerName.Text;
         Program.PickerName = pickerName.Text;
         ViewModel.Save(BaseContext);
         Finish();
      }

      private void CancelButtonOnClick(object sender, EventArgs eventArgs)
      {
         Finish();
      }

      #endregion

   }
}