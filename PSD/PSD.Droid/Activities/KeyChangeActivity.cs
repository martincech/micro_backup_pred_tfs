using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using PSD.Core.Repository.Interfaces;
using Android.Content;
using Android.Content.PM;

namespace PSD.Droid.Activities
{
   [Activity(Theme = "@android:style/Theme.Dialog",
             ConfigurationChanges = ConfigChanges.KeyboardHidden | ConfigChanges.Orientation)]
   public class KeyChangeActivity : BaseActivity<ISettings>
   {
      public const string KEY_CODE_PRESSED = "pressed_keycode";
      private LinearLayout layout;

      protected override void OnCreate(Bundle bundle)
      {
         base.OnCreate(bundle);
         Title = Resources.GetString(Resource.String.ChangeKeyTitle);         
         SetContentView(Resource.Layout.KeyChange);

         var backButton = FindViewById<Button>(Resource.Id.backButton);
         backButton.Click += BackButtonOnClick;
         backButton.Focusable = false;

         layout = FindViewById<LinearLayout>(Resource.Id.keyChangeLayout);
         layout.KeyPress += LayoutOnKeyPress;
         layout.Focusable = true;

         ShowSoftwareKeyboardIfHwMiss(this, true);
      }

      #region Private helpers

      /// <summary>
      /// Key press event.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void LayoutOnKeyPress(object sender, View.KeyEventArgs e)
      {
         layout.KeyPress -= LayoutOnKeyPress;
         e.Handled = true;
         SetResult(e.KeyCode);
         ShowSoftwareKeyboardIfHwMiss(this, false);
         BackButtonOnClick(this, null);
      }

      /// <summary>
      /// Close activity and returns pressed keycode.
      /// </summary>
      /// <param name="code"></param>
      private void SetResult(Keycode code)
      {
         // Create the result Intent and include data
         var intent = new Intent();
         intent.PutExtra(KEY_CODE_PRESSED, (int) code);

         // Set result and finish this Activity
         SetResult(Result.Ok, intent);
      }


      private void BackButtonOnClick(object sender, EventArgs eventArgs)
      {
         Finish();
      }

      #endregion
   }
}