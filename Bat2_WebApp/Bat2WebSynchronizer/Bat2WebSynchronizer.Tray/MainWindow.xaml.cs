﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Bat2WebSynchronizer.Tray.Code;
using Bat2WebSynchronizer.Tray.Model;
using Bat2WebSynchronizer.Tray.View;
using Bat2WebSynchronizer.Tray.ViewModel;
using Desktop.Wpf.Applications;
using Desktop.Wpf.NotifyIcon;
using Desktop.Wpf.Presentation;
using Utilities.Annotations;
using Timer = System.Timers.Timer;

namespace Bat2WebSynchronizer.Tray
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : INotifyPropertyChanged
   {
      #region Private properties

      private Timer BallonTimer { get; set; }
      private SyncStatus _lastMessage;
      private UIElement _popupWindow;
      private ICommand _iconLeftClickCommand;

      #endregion

      public UIElement PopupWindow
      {
         get { return _popupWindow; }
         private set
         {
            if (Equals(value, _popupWindow)) return;
            _popupWindow = value;
            OnPropertyChanged();
         }
      }

      public ICommand IconLeftClickCommand
      {
         get
         {  // set settings windows allways as window to popup after left click
            if (_iconLeftClickCommand == null)
            {
               _iconLeftClickCommand = new RelayCommand(() =>
               {
                  Settings_Click(this, null);
               });
            }
            return _iconLeftClickCommand;
         }
      }

      private SettingsViewModel Model { get; set; }
      private CodeApp codeApp;

      public MainWindow()
      {
         InitializeComponent();
         Model = new SettingsViewModel();
         codeApp = new CodeApp(Model);

         //Model.UpdatePorts();
         Model.UserSettings.LogMessage.CollectionChanged += LogMessage_CollectionChanged;
         CodeMessageBox.Instance.StayOpened += ModelOnStayOpened;

         // ballon show/hide timers
         BallonTimer = new Timer(29000);
         BallonTimer.Elapsed += BallonTimerOnElapsed;

         CheckSettings();
         DataContext = Model;

         NotifyIcon.TrayBalloonTipClosed += NotifyIconOnTrayBalloonTipClosed;
         NotifyIcon.TrayBalloonTipClicked += NotifyIconOnTrayBalloonTipClicked;

         Model.PropertyChanged += Model_PropertyChanged;
         NotifyIcon.ShowTrayPopup();
      }

      void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (e.PropertyName == "Logged")
         {
            if (Model.Logged)
            {
               NotifyIcon.Icon = Properties.Resources.ICON;
            }
            else
            {
               NotifyIcon.Icon = Properties.Resources.ICON_ERR;
            }
         }
      }

      #region Event handlers

      private void NotifyIconOnTrayBalloonTipClicked(object sender, RoutedEventArgs routedEventArgs)
      {
         NotifyIcon.CloseBalloon();
         if (_lastMessage == SyncStatus.MessageReceived)
         {
            Log_Click(sender, routedEventArgs);
         }
         else
         {
            Settings_Click(sender, routedEventArgs);
         }
      }

      private void NotifyIconOnTrayBalloonTipClosed(object sender, RoutedEventArgs routedEventArgs)
      {
         BallonTimer.Stop();
      }

      private void BallonTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
      {
         NotifyIcon.CloseBalloon();
      }

      private void LogMessage_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         ShowMessage(SyncStatus.LogMessage);
      }

      private void Log_Click(object sender, RoutedEventArgs e)
      {
         PopupWindow = new LogView { DataContext = Model };
         NotifyIcon.TrayPopupResolved.Closed -= TrayPopupResolvedOnClosed;
         NotifyIcon.ShowTrayPopup();
         Model.StayOpened = true;
      }

      private void Settings_Click(object sender, RoutedEventArgs e)
      {
         PopupWindow = new SettingsView { DataContext = Model };
         NotifyIcon.TrayPopupResolved.Closed -= TrayPopupResolvedOnClosed;
         NotifyIcon.TrayPopupResolved.Closed += TrayPopupResolvedOnClosed;
         NotifyIcon.ShowTrayPopup();
         Model.StayOpened = true;
      }

      private void Elo_Click(object sender, RoutedEventArgs e)
      {
         var menuItem = (MenuItem)sender;
         var device = (Device)menuItem.Tag;

         if (device.Type == DeviceType.Modem)
         {
            return;
         }

         var setting = Model.EloSettings.FirstOrDefault(f => f.SerialNumber == device.SerialNumber);
         PopupWindow = new EloView(Model) { DataContext = setting };
         NotifyIcon.TrayPopupResolved.Closed -= TrayPopupResolvedOnClosed;
         NotifyIcon.TrayPopupResolved.Closed += TrayPopupResolvedOnClosed;
         NotifyIcon.ShowTrayPopup();
         Model.StayOpened = true;
      }

      /// <summary>
      /// Service for closing popup window.
      /// </summary>
      private void TrayPopupResolvedOnClosed(object sender, EventArgs eventArgs)
      {
         // When popup is closed, mvvm not working correctly and view model
         // must be update direct.
         var view = PopupWindow as SettingsView;
         if (view != null)
         {
            Model.UserSettings.Name = view.UserNameTextBox.Text;
            Model.UserSettings.Password = view.PasswordBox.Password;
            Model.UserSettings.ServerUrl = view.ServerUrlTextBox.Text;
            UserSettings.Save(Model.UserSettings);
            CheckSettings();
         }

         var elo = PopupWindow as EloView;
         if (elo != null)
         {
            var setting = (EloSetting)elo.DataContext;
            Model.SaveEloSettings.Execute(setting.PortNumber);
         }
         Model.StayOpened = false;
      }


      private void Exit_Click(object sender, RoutedEventArgs e)
      {
         ThreadPool.QueueUserWorkItem(delegate
         {
            var result = MessageBox.Show(Properties.Resources.EXIT_DIALOG, Properties.Resources.EXIT, MessageBoxButton.YesNo, MessageBoxImage.Warning);
            DispatcherHelper.RunAsync(() =>
            {
               if (result == MessageBoxResult.Yes)
               {
                  Close();
               }
            });
         }, null);
      }

      #endregion

      #region Helper methods

      /// <summary>
      /// Views are presented as popup which is closed when lost focus.
      /// Any view can contain element e.g. OpenFileDialog that remove focus from view 
      /// and then both window will be closed. Therefore is there event for set popup to
      /// not close after it lost focus.
      /// </summary>
      /// <param name="b">if true popup will be open after lost focus</param>
      private void ModelOnStayOpened(object sender, bool b)
      {
         if (NotifyIcon.TrayPopupResolved == null)
         {
            return;
         }

         NotifyIcon.TrayPopupResolved.StaysOpen = b;
      }

      private void CheckSettings()
      {

         if (string.IsNullOrEmpty(Model.UserSettings.Name))
         {
            ShowMessage(SyncStatus.CredentialsMissing);
         }
         else if (string.IsNullOrEmpty(Model.UserSettings.Password))
         {
            ShowMessage(SyncStatus.CredentialsMissing);
         }
         else if (string.IsNullOrEmpty(Model.UserSettings.ServerUrl))
         {
            ShowMessage(SyncStatus.ServerUrlMissing);
         }
      }

      private void ShowMessage(SyncStatus message)
      {
         if (!Model.UserSettings.ShowNotification)
         {
            return;
         }

         if (_lastMessage == message)
         {
            return;
         }
         var tip = "";

         switch (message)
         {
            case SyncStatus.AutosyncDisabled:
               tip = Properties.Resources.MESSAGE_AUTOSYNC_DISABLED;
               NotifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Warning);
               _lastMessage = SyncStatus.AutosyncDisabled;
               break;
            case SyncStatus.CredentialsMissing:
               tip = Properties.Resources.MESSAGE_MISSING_CREDENTIALS;
               NotifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Error);
               _lastMessage = SyncStatus.CredentialsMissing;
               break;
            case SyncStatus.ServerUrlMissing:
               tip = Properties.Resources.MESSAGE_MISSING_SERVERURL;
               NotifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Error);
               _lastMessage = SyncStatus.CredentialsMissing;
               break;
            case SyncStatus.MessageReceived:
               tip = Properties.Resources.MESSAGE_SMS_RECEIVED;
               NotifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Info);
               _lastMessage = SyncStatus.MessageReceived;
               break;
            case SyncStatus.ComPortsMissing:
               tip = Properties.Resources.MESSAGE_MISSING_COMPORT;
               NotifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, tip, BalloonIcon.Error);
               _lastMessage = SyncStatus.MessageReceived;
               break;
            case SyncStatus.LogMessage:
               NotifyIcon.ShowBalloonTip(Properties.Resources.TITLE_BAT2, Model.UserSettings.LogMessage.Last().Value,
                  BalloonIcon.Info);
               _lastMessage = SyncStatus.LogMessage;
               break;
         }
         BallonTimer.Start();
      }

      public event PropertyChangedEventHandler PropertyChanged;

      [NotifyPropertyChangedInvocator]
      protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
      {
         var handler = PropertyChanged;
         if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
      }

      #endregion
   }
}
