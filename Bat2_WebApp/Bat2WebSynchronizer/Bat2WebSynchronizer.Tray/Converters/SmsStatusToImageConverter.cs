﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Bat2Library.Sms;
using Bat2WebSynchronizer.Tray.Helpers;
using Bat2WebSynchronizer.Tray.Model;

namespace Bat2WebSynchronizer.Tray.Converters
{
   public class SmsStatusToImageConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {

         var device = value as Device;
         if (device == null) return null;

         BitmapSource img;
         switch (device.Event)
         {
            case SmsEvent.RESET:
               img = IconHelper.Yellow;
               break;
            case SmsEvent.READY:
               img = IconHelper.Green;
               break;
            default:
               img = IconHelper.Red;
               break;
         }

         if (device.Type == DeviceType.ELO)
         {
            img = IconHelper.Gear;
         }
         return img;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
