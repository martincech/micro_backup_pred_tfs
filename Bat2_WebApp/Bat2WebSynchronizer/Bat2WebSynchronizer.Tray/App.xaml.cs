﻿using System;
using System.Reflection;
using System.Windows;
using Desktop.Wpf;
using Desktop.Wpf.Presentation;
using Utilities;

namespace Bat2WebSynchronizer.Tray
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        static App()
        {
            if (SingleApplication.IsAlreadyRunning())
            {
                Environment.Exit(0);
            }
            DispatcherHelper.Initialize();
        }

       protected override void OnStartup(StartupEventArgs e)
       {
          AppDomain.CurrentDomain.AssemblyLoad += (sender, args) =>
          {
             Logger.Write(args.LoadedAssembly.FullName);
          };

          //var referencedAssemblies = Assembly.GetEntryAssembly().MyGetReferencedAssembliesRecursive();
          //var missingAssemblies = Assembly.GetEntryAssembly().MyGetMissingAssembliesRecursive();
          //foreach (var asm in referencedAssemblies)
          //{
          //   Logger.Write(asm.Key);   
          //}
          //Logger.Write("missingAssemblies " + missingAssemblies.Count);
          //foreach (var m in missingAssemblies)
          //{
          //   Logger.Write(" missing: " + m.MissingAssemblyName);
          //}

          base.OnStartup(e);
          AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
       }

       private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
       {
          var ex = (Exception)e.ExceptionObject;
          Logger.Write("Unhandled Error\n\t\t" + ex.Message + "\n\t\t" + ex.StackTrace + "\n");

          MessageBox.Show(ex.Message + ex.StackTrace,
             "Error", MessageBoxButton.OK, MessageBoxImage.Stop);
       }
    }
}
