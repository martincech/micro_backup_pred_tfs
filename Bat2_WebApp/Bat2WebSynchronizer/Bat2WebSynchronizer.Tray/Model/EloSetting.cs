﻿using System.Collections.ObjectModel;
using System.Xml.Serialization;
using Utilities.Observable;

namespace Bat2WebSynchronizer.Tray.Model
{
   public class EloSetting : ObservableObject
   {
      private string serialNumber;
      private int parity;
      private byte addressFrom;
      private byte addressTo;
      private int mode;
      private int baudRate;
      private int portNumber;

      private int hour;
      private int minute;

      private int address;
      private bool scanning;

      private ObservableCollection<ScaleDevice> scales;

      public EloSetting()
      {
         scales = new ObservableCollection<ScaleDevice>();
      }

      public string SerialNumber
      {

         get { return serialNumber; }
         set { SetProperty(ref serialNumber, value); }
      }

      public int Parity
      {

         get { return parity; }
         set { SetProperty(ref parity, value); }
      }

      public byte AddressFrom
      {

         get { return addressFrom; }
         set { SetProperty(ref addressFrom, value); }
      }

      public byte AddressTo
      {

         get { return addressTo; }
         set { SetProperty(ref addressTo, value); }
      }

      public int Mode
      {

         get { return mode; }
         set { SetProperty(ref mode, value); }
      }

      public int BaudRate
      {

         get { return baudRate; }
         set { SetProperty(ref baudRate, value); }
      }

      public int PortNumber
      {

         get { return portNumber; }
         set { SetProperty(ref portNumber, value); }
      }

      public string PortName
      {
         get { return "COM" + portNumber; }
      }


      public int Hour
      {
         get { return hour; }
         set { SetProperty(ref hour, value); }
      }

      public int Minute
      {
         get { return minute; }
         set { SetProperty(ref minute, value); }
      }

      [XmlIgnore]
      public int Address
      {
         get { return address; }
         set { SetProperty(ref address, value); }
      }

      [XmlIgnore]
      public bool Scanning
      {
         get { return scanning; }
         set { SetProperty(ref scanning, value); }
      }

      [XmlIgnore]
      public ObservableCollection<ScaleDevice> Scales
      {
         get { return scales; }
         set { SetProperty(ref scales, value); }
      }
   }

   public class ScaleDevice
   {
      public int Address { get; set; }
      public string Name { get; set; }
      public bool Connected { get; set; }

      public string Header { get { return string.Format("Scale {0} [Address {1}]", Name, Address); } }
   }
}
