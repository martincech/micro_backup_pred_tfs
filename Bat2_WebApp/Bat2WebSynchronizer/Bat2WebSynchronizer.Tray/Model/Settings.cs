﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Bat2Library.Sms;
using Bat2Library.Utilities.Sms.Models;
using Utilities.Observable;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Bat2WebSynchronizer.Tray.Model
{
   public class Settings : ValidatableObservableObject
   {
      #region Private properties

      private string name;
      private string password;
      private int recievedSmsCount;
      private string serverUrl;
      private bool showNotification;

      private ObservableCollection<KeyValuePair<DateTime, SmsStatus>> toSyncSms;
      private ObservableCollection<SmsData> syncedSms;
      private ObservableCollection<KeyValuePair<DateTime, string>> logMessage;
      private int syncedSmsCount;

      #endregion

      public Settings()
      {
         ToSyncSms = new ObservableCollection<KeyValuePair<DateTime, SmsStatus>>();
         LogMessage = new ObservableCollection<KeyValuePair<DateTime, string>>();
         SyncedSms = new ObservableCollection<SmsData>();
      }

      /// <summary>
      /// User name to for sync data
      /// </summary>
      public string Name
      {
         get { return name; }
         set { SetProperty(ref name, value); }
      }

      /// <summary>
      /// Password used for sync data
      /// </summary>
      public string Password
      {
         get { return password; }
         set { SetProperty(ref password, value); }
      }

      /// <summary>
      /// Url of server to syncronize data
      /// </summary>
      public string ServerUrl
      {
         get { return serverUrl; }
         set { SetPropertyAndValidate(ref serverUrl, value); }
      }

      /// <summary>
      /// Show notification messages in tray area
      /// </summary>
      public bool ShowNotification
      {
         get { return showNotification; }
         set { SetProperty(ref showNotification, value); }
      }

      /// <summary>
      /// Collection SMS to be synced
      /// </summary>
      public ObservableCollection<KeyValuePair<DateTime, SmsStatus>> ToSyncSms
      {
         get { return toSyncSms; }
         private set { SetProperty(ref toSyncSms, value); }
      }

      /// <summary>
      /// Collection of messages showed in log
      /// </summary>
      public ObservableCollection<KeyValuePair<DateTime, string>> LogMessage
      {
         get { return logMessage; }
         set
         {
            if (logMessage != null)
            {
               logMessage.CollectionChanged -= SyncedSmsOnCollectionChanged;
            }
            SetProperty(ref logMessage, value);
            if (logMessage != null)
            {
               logMessage.CollectionChanged += LogMessageOnCollectionChanged;
            }
         }
      }

      /// <summary>
      /// Number of received SMS
      /// </summary>
      public int RecievedSmsCount
      {
         get { return recievedSmsCount; }
         set { SetProperty(ref recievedSmsCount, value); }
      }

      /// <summary>
      /// Collection of SMS sent to server
      /// </summary>
      public ObservableCollection<SmsData> SyncedSms
      {
         get { return syncedSms; }
         private set
         {
            if (syncedSms != null)
            {
               syncedSms.CollectionChanged -= SyncedSmsOnCollectionChanged;
            }
            SetProperty(ref syncedSms, value);
            if (syncedSms != null)
            {
               syncedSms.CollectionChanged += SyncedSmsOnCollectionChanged;
            }
         }
      }

      public int SyncedSmsCount
      {
         get { return syncedSmsCount; }
         private set { SetProperty(ref syncedSmsCount, value); }
      }

      #region Private helpers

      private void LogMessageOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:
               if (e.NewItems.Count == 1)
               {
                  var message = (KeyValuePair<DateTime, string>)e.NewItems[0];
                  if (message.Value.Contains(Properties.Resources.LOG_SMS_RECEIVED.Replace("{0}", "")))
                  {
                     RecievedSmsCount++;
                  }
               }
               break;
            case NotifyCollectionChangedAction.Remove:
               RecievedSmsCount--;
               break;
            case NotifyCollectionChangedAction.Reset:
               RecievedSmsCount = 0;
               break;
            default:
               return;
         }
      }

      private void SyncedSmsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:
               SyncedSmsCount++;
               break;
            case NotifyCollectionChangedAction.Remove:
               SyncedSmsCount--;
               break;
            case NotifyCollectionChangedAction.Reset:
               SyncedSmsCount = 0;
               break;
            default:
               return;
         }
      }



      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         string errorMessage = "";
         IEnumerable<string> objects = new[] { "" };

         if (propertyName.Equals("ServerUrl"))
         {
            var valid = true;

            var url = (string)value;
            if (string.IsNullOrEmpty(url) || !(Regex.IsMatch(url, "^[a-zA-Z0-9]")))
            {
               valid = false;
            }

            if (valid)
            {
               url = url.ToLower();
               if (!url.StartsWith("http://") && !url.StartsWith("https://"))
               {
                  url = "http://" + url;
               }

               Regex urlchk =
                  new Regex(
                     @"((http|https)://)+(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,15})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9\&amp;%_\./-~-]*)?",
                     RegexOptions.Singleline | RegexOptions.IgnoreCase);

              valid =  urlchk.IsMatch(url);
            }

            if (!valid)
            {
               objects = new[] {"ServerUrl"};
               errorMessage = "error"; 
            }
         }



         if (!errorMessage.Equals(""))
         {
            var valRes = new List<ValidationResult>
            {
               new ValidationResult(errorMessage, objects)      
            };
            return valRes;
         }

         return base.AdditionalValidationRules(value, propertyName);
      }

      #endregion
   }

   /// <summary>
   /// Type of event for message logs
   /// </summary>
   public enum SyncStatus
   {
      None,
      CredentialsMissing,
      AutosyncDisabled,
      ServerUrlMissing,
      MessageReceived,
      LogMessage,
      ComPortsMissing
   }
}
