﻿using System.Linq;
using BatApp.Extensions;
using DTOs;

namespace BatApp.Controllers.V1
{
   /// <summary>
   /// Bat2 old configuration controller
   /// </summary>
   public class ConfigurationsV2Controller : BaseApiController
   {
      /// <summary>
      /// Get Bat2's active configuration.
      /// </summary>
      /// <param name="id">scale name</param>
      /// <returns>configuration</returns>
      public ConfigurationV2 Get(string id)
      {
         var scale = Context.Scales.FirstOrDefault(f => f.Name.Equals(id));
         if (scale == null) return null;

         var scaleDto = scale.MapTo();
         return scaleDto.ConfigurationsV2.FirstOrDefault(c => c.Active);
      }
   }
}
