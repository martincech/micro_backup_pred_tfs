﻿using DataModel;

namespace BatApp.Controllers.V1
{
    public class Bat2OldFlocksController : BaseApiController
    {
       /// <summary>
       /// Create a new flock.
       /// </summary>
       /// <param name="flock"></param>
       /// <returns>created flock</returns>
       public Flock Post(Bat2Library.Bat2Old.DB.Helpers.DataFlock flock)
       {
          if (flock == null) return null;

          var newFlock = new Flock
          {
             Name = flock.Name,
             StartDate = flock.StartDate,
             EndDate = flock.EndDate,
             ScaleId = Context.CreateScale(flock.Scale.ToString())
          };
          if (!Context.CanCreateFlock(newFlock)) return null;

          Context.SaveFlock(newFlock);
          return newFlock;
       }
    }
}
