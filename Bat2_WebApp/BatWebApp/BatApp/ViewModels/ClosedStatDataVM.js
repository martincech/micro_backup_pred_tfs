﻿/* OnDOM ready */
$(document).ready(function () {
    JsActions.Flocks.GetClosedFlocks({
        success: function (result) {
            $(result).each(function () {
                GraphVM.flocks.push({ name: this.name, id: this.id });
            });
            if ($.cookie(window.location.pathname) === "graph") {
                $("#selectedGraph a").trigger("click");
                if (!statTable) {
                    createStatTable();
                }
            } else {
                $("#selectedStat a").trigger("click");
            }
        }
    });
});