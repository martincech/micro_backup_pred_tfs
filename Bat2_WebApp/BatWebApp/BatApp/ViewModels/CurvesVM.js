﻿var curvesTable;

var chart;
var chartData = [];
var tableTop = 260;

var curvePoint;
var gl = globals[$.cookie("_culture")];

ko.validation.locale($.cookie("_culture"));
ko.validation.init({});

var CurvesVM = {
   curveName: ko.observable().extend({ required: true }),
   curveIds: ko.observableArray(),
   curveNames: ko.observableArray(),
   curves: ko.observableArray(),
   selectedCurves: ko.observableArray(),
   day: ko.observable().extend({ required: true, digit: true }),
   weight: ko.observable().extend({ required: true, pattern: '^[0-9]+((\.|,)[0-9]{1,8})?$' }),

   selectedDataRow: ko.observableArray()
};


var createCurvesTable = function() {

    if (typeof curvesTable == 'undefined') {
        curvesTable = $('#growthCurvesTable').dataTable({
            "language": {
                "url": baseURL + "Content/DataTables/Internationalisation/" + $.cookie("_culture") + ".txt"
            },            
            "sScrollY": calcDataTableHeight(),
            "sScrollX": "100%",
            "bServerSide": true,
            "sAjaxSource": baseURL + "Curves/CurveHandler",
            "bProcessing": true,
            "sDom": "frtiS",
            "bFilter": false,
            "fnServerParams": function(aoData) {
                aoData.push(
                {
                    "name": "curveId",
                    "value": CurvesVM.selectedCurves()[0]
                });
            },
            "aoColumns": [
                {
                    "sName": "Id",
                    "bVisible": false
                },
                {
                    "bSearchable": false,
                    "bSortable": false,
                    "sName": "Day"
                },
                {
                    "bSearchable": false,
                    "bSortable": false,
                    "sName": "Weight"
                },
                {
                    "bSearchable": false,
                    "bSortable": false,
                    "data": null,
                    "render": function(data, type, row) {
                        return "<a class='table-action-deletelink'><div></div></a>";
                    }
                }
            ]
        });
        curvesTable.makeEditable({
            fnOnAdded: function(status) {
                if (status == 'success') {
                    CurvesVM.day(undefined);
                    CurvesVM.weight(undefined);
                }
            },
            fnOnEditing: function (jInput) {
                var cellIndex = $(jInput).closest("td").index();
                if (cellIndex === 0) {
                    return validateDay(jInput);
                } else if (cellIndex === 1) {
                    return validateValue(jInput);
                }
                return false;
            },
            fnOnDeleting: function (tr, id, fnDeleteRow, delUrl) {
                curvePoint = id;
                $("#deleteCurvePoint").modal("show");            
                return false;
            },
            aoColumns: [{ event: 'taphold dblclick', onblur: "submit", placeholder: "" }, { event: 'taphold dblclick', onblur: "submit", placeholder: "" }, null],
            sDeleteRowButtonId: ""
        });
    } else {
        curvesTable.fnDraw();
    }

}; // inicialization

$(document).ready(function() {
    CurvesVM.refreshBindings();
   $("#curveSelector").change(function() {
       if (CurvesVM.selectedCurves().length == 1) {
           createCurvesTable();
           createChart();
       }
   });
   $('#curveSelector').css('height', calcDataTableHeight() + 30);
});


$('#growthCurvesTable').on('draw.dt', function () {
   chartData = [];
   $("#growthCurvesTable").DataTable().rows().indexes().each(function (idx) {
      var data = $("#growthCurvesTable").DataTable().row(idx).data();
      if (typeof data === 'undefined') {
         return;
      }
      chartData.push({ day: data[1], weight: data[2] });
   });

   if (chartData == undefined || chartData == null || chartData.length == 0) {
      chart.dataProvider = chartData;
      chart.validateData();
      return;
   }
   var curveName = $('#curveSelector option:selected').text();
   chart.graphs[0].balloonText = gl.gcTitle.replace("{0}", gl.weight).replace("{1}", curveName);//"Weight of standard growth curve \"" + curveName + "\" at day [[x]]: [[y]]";

    chartData.unshift({"day":"day", "weight":"weight"});

   chart.dataProvider = chartData;
   chart.validateData();
   chart.write("chartdivCurve");
});

AmCharts.exportPrint = {
   "format": "PRINT",
   "label": gl.print
};

function createChart() {

   var lang = $.cookie("_culture");
   var graphTypeName = gl.weight;
   // SERIAL CHART
   chart = new AmCharts.AmXYChart();
   var fileName = $("#curveSelector :selected").text().replace(/[^a-z0-9\s]/gi, '_');
   chart.colors = ['#AF002A'];
   chart.export = {
      "enabled": true,
      "libs": {
          "path": baseURL + "Scripts/AmCharts/plugins/export/libs/"
      },
      "fileName": fileName,
      "menu": [{
         "class": "export-main",
         "label": gl.export,
         "menu": [
         {
            "label": gl.download,
            "menu": [
                {
                   "label": "PNG",
                   "click": function () {
                      this.capture({}, function () {
                         this.toPNG({}, function (data) {
                            this.download(data, "image/png", chart.export.config.fileName + ".png");
                         });
                      });
                   }
                }, {
                   "label": "JPG",
                   "click": function () {
                      this.capture({}, function () {
                         this.toJPG({}, function (data) {
                            this.download(data, "image/jpg", chart.export.config.fileName + ".jpg");
                         });
                      });
                   }
                }, {
                   "label": "SVG",
                   "click": function () {
                      this.capture({}, function () {
                         this.toSVG({}, function (data) {
                            this.download(data, "text/xml", chart.export.config.fileName + ".svg");
                         });
                      });
                   }
                }, {
                   "format": "PDF",
                   "content": ["Saved from:", window.location.href, {
                      "image": "reference",
                      "fit": [523.28, 769.89] // fit image to A4
                   }], "click": function () {
                      this.capture({}, function () {
                         this.toPDF({}, function (data) {
                            this.download(data, "application/pdf", chart.export.config.fileName + ".pdf");
                         });
                      });
                   }
                }
            ]
         }, {
            "label": gl.save,
            "menu": [{
               "label": "CSV",
               "click": function () {                 
                  this.capture({}, function () {
                     this.toCSV({ withHeader: false }, function (data) {                      
                        this.download(data, "text/plain", chart.export.config.fileName + ".csv");
                     });
                  });
               }
            }, {
               "label": "XLSX",
               "click": function () {               
                  this.capture({}, function () {
                     this.toXLSX({}, function (data) {                      
                        this.download(data, "text/xml", chart.export.config.fileName + ".xlsx");
                     });
                  });
               }
            }]
         }, {
            "label": gl.annotate,
            "action": "draw",
            "menu": [{
               "class": "export-drawing",
               "menu": [gl.undo, gl.redo, {
                  "label": gl.save,
                  "menu": ["PNG", "JPG", "SVG", "PDF"]
               }, AmCharts.exportPrint, gl.cancel]
            }]
         }, AmCharts.exportPrint]
      }]
   };

   chart.pathToImages = baseURL + "Content/images/";
   chart.startDuration = 0;
   if (lang == undefined) {
       chart.language = null;
   } else if (lang.substr(0, 2) == "en") {
       chart.language = null;
   } else {
       chart.language = lang.substr(0, 2);
   }


   // AXES
   // category
   // X
   var xAxis = new AmCharts.ValueAxis();
   xAxis.title = gl.dayTitle;
   xAxis.position = "bottom";
   xAxis.dashLength = 1;
   xAxis.axisAlpha = 0;
   xAxis.autoGridCount = true;
   chart.addValueAxis(xAxis);

   // Y
   var yAxis = new AmCharts.ValueAxis();
   yAxis.position = "left";
   yAxis.title = graphTypeName;
   yAxis.dashLength = 1;
   yAxis.axisAlpha = 0;
   yAxis.autoGridCount = true;
   chart.addValueAxis(yAxis);

   var curveName = $('#curveSelector option:selected').text();
   var graph = new AmCharts.AmGraph();
   graph.title = curveName;
   graph.xField = "day";
   graph.yField = "weight";
   graph.balloonText = gl.gcTitle.replace("{0}", graphTypeName).replace("{1}", curveName);//graphTypeName + " of standard growth curve \"" + curveName + "\" at day [[x]]: [[y]]";
   graph.lineAlpha = 1;
   graph.bullet = "circle";

   graph.bulletSize = 8;
   graph.lineThickness = 3;
   chart.addGraph(graph);

   // SCROLLBAR
   var chartScrollbar = new AmCharts.ChartScrollbar();
   chart.addChartScrollbar(chartScrollbar);
   $("#chartdivCurve").text("");
  
}

CurvesVM.createCurve = function() {
    if (CurvesVM.curveName.isValid()) {
        JsActions.Curves.Create(
            CurvesVM.curveName(),
            {
                success: function(result) {
                    CurvesVM.refreshBindings();
                    $('.modal').modal('hide');
                }
            });
    } else {
        CurvesVM.errorsCurve.showAllMessages();
    }
};

CurvesVM.addCurvePoint = function () {
    if (validateDay($("#day"))) {
        JsActions.Curves.AddData(
            CurvesVM.selectedCurves()[0],
            CurvesVM.weight,
            CurvesVM.day,
            {
                success: function(result) {
                    CurvesVM.refreshBindings();
                    $('.modal').modal('hide');
                    CurvesVM.day(null);
                    CurvesVM.weight(null);
                    createCurvesTable();
                }
            });
    } else {
    CurvesVM.errorsPoints.showAllMessages();
}
};



CurvesVM.refreshBindings = function() {
   JsActions.Curves.GetCurves(
   {
      success: function(result) {
         CurvesVM.curves(result);
         $(result).each(function() {
            CurvesVM.curveNames.push(this.name);
            CurvesVM.curveIds.push(this.id);
         });

      }
   });
};

CurvesVM.deleteCurvePoint = function () {
    JsActions.Curves.DeleteData(
           curvePoint,
           {
               success: function (result) {
                   $(".modal").modal("hide");
                   createCurvesTable();
                   createChart();
               }
           });
};

CurvesVM.deleteCurves = function() {
   JsActions.Curves.DeleteCurves(
      CurvesVM.selectedCurves(),
      {
         success: function() {
            CurvesVM.refreshBindings();
            CurvesVM.selectedCurves.removeAll();
            $('.modal').modal('hide');
         }
      }
   );
};

var validateDay = function (input) {
   var isDuplicated = false;
   var intRegex = /^\d+$/;

   $('#growthCurvesTable tbody tr').each(function () {
       var testedTd = $(this).find("td:first").html();
      if (testedTd == input.val()) {
         isDuplicated = true;
          return false;
      }
      return true;
   });
    
   if (isDuplicated) {
      alert('Given day already exists!');
      return false;
   }

   if (input.val() == "" || !intRegex.test(input.val())) {
      alert("Given value is incorrect!");
      return false;
   }
   return true;
};

var validateValue = function(input) {
   var floatRegex = /^((\d+(\.|\,\d *)?)|((\d*\.|\,)?\d+))$/;
   if (input.val() == "" || !floatRegex.test(input.val())) {
      alert("Given value is incorrect!");
      return false;
   }
   return true;
};

var calcDataTableHeight = function () {

    var height = $(window).height() - 710;
    if (height < 150) {
        height = 150;
    }
    return height;
};

$(window).resize(function() {
    $('.dataTables_scrollBody').css('height', calcDataTableHeight());
    $('#curveSelector').css('height', calcDataTableHeight() + 30);
});

CurvesVM.errorsCurve = ko.validation.group([CurvesVM.curveName]);
CurvesVM.errorsPoints = ko.validation.group([CurvesVM.day, CurvesVM.weight]);
ko.applyBindings(CurvesVM, document.getElementById("curvesForm"));