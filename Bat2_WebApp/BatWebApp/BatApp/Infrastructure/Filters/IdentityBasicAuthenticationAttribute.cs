﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using DataModel;
using Microsoft.AspNet.Identity;
using Utilities.IOC;


namespace BatApp.Infrastructure.Filters
{
   public class IdentityBasicAuthenticationAttribute : BasicAuthenticationAttribute
   {
      protected override IPrincipal Authenticate(string userName, string password)
      {
         UserManager =  IocContainer.Container.Get<UserManager<User, Guid>>();
         var user = UserManager.Find(userName, password);
         if (user == null)
         {
            // No user with userName/password exists.
            return null;
         }
         // Create a ClaimsIdentity with all the claims for this user.
         var identity = UserManager.ClaimsIdentityFactory.CreateAsync(UserManager, user, "Basic").Result;
         return new ClaimsPrincipal(identity);
      }
      private UserManager<User, Guid> UserManager { get; set; }
   }
}