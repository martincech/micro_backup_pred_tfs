﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace JsAction
{
   /// <summary>
   /// Http and Route handler for javascript file generation
   /// </summary>
   public class JsActionHandler : IJsActionHandler
   {
      private const string GET = "GET";
      private const string POST = "POST";

      protected override void GenerateMethodCall(StringBuilder js, MethodInfo method, string controller,
         bool documentate)
      {
         var attributes = method.GetCustomAttributes(true);
         var actionAttr = attributes.Where(attr => attr is ActionNameAttribute).ToList();
         var jsattribute = attributes.First(attr => attr is JsActionAttribute) as JsActionAttribute;

         Debug.Assert(jsattribute != null);
         if (Groups.Count(str => string.IsNullOrEmpty(str) == false) > 0 &&
             !jsattribute.Groups.Split(',').Intersect(Groups).Any())
            return;

         var pars = method.GetParameters();
         
         var parameters = string.Join(",", pars.Select(m => m.Name));
         if (parameters.Length > 0)
            parameters += ',';

         foreach (var m in pars)
         {
            if (m.ParameterType.IsGenericType)
               ComplexTypeList.Value.Add(m.ParameterType.GetGenericArguments().First());
            else if (m.ParameterType != typeof(string) && m.ParameterType.IsPrimitive == false)
               ComplexTypeList.Value.Add(m.ParameterType);
         }

         var methodName = method.Name;

         if (string.IsNullOrEmpty(jsattribute.MethodName) == false)
            methodName = jsattribute.MethodName;
         else if (actionAttr.Any())
            methodName = ((ActionNameAttribute) actionAttr.First()).Name;

         js.AppendFormat("{0}:function({1}options)", methodName, parameters);

         js.Append('{');
         if (documentate)
         {
            //Method can be empty.
            JsActionUtils.DocumentateTheFunction(js, method);
            js.Append("},");
            return;
         }

         var action = (actionAttr.Count() != 0) ? ((ActionNameAttribute) actionAttr.First()).Name : method.Name;
         var url = new UrlHelper(RequestContext).Action(action, controller);

         var post = false;
         var get = false;

         if (jsattribute.Verb != HttpSingleVerb.None)
         {
            if (jsattribute.Verb == HttpSingleVerb.HttpPost)
               post = true;
            else
               get = true;
         }
         else
         {
            if (attributes.Count(attr => attr is AcceptVerbsAttribute) != 0)
            {
               var verbattr = attributes.First(attr => attr is AcceptVerbsAttribute);

               foreach (var verb in ((AcceptVerbsAttribute) verbattr).Verbs)
               {
                  switch (verb.ToUpper())
                  {
                     case GET:
                        get = true;
                        break;
                     case POST:
                        post = true;
                        break;
                  }
               }
            }
            else
            {
               post = attributes.Count(attr => attr is HttpPostAttribute) > 0;
               get = attributes.Count(attr => attr is HttpGetAttribute) > 0;
            }
         }

         if (post && get || (!post && !get))
         {
            if (Debugger.IsAttached)
            {
               Debug.WriteLine("JsAction -- I found two HttpVerb attributes, i will use GET as preferred");
               Alerts.Value.AppendFormat("{2}_{1}_{0},", action, controller,
                  jsattribute.MethodName ?? method.Name);
               post = false;
            }
            else
               throw new Exception("There are two acceptable HttpVerbs but noone has been marked as preferred!");
         }

         var requestmethod = post ? POST : GET;

         var obj = new StringBuilder();

         var hasParams = method.GetParameters().Any();
         if (hasParams)
         {
            foreach (var parameter in method.GetParameters())
            {
               js.AppendFormat("var _{0} = {0};", parameter.Name);
            }
            js.Append("if (ko !== null && ko !== undefined){");
            obj.AppendFormat(
               "{{");
            foreach (var parameter in method.GetParameters())
            {
               js.AppendFormat("_{0} = ko.toJS({0});", parameter.Name);
               obj.AppendFormat("{0}: _{0},", parameter.Name);
            }

            if (obj.Length > 0)
            {
               obj.Remove(obj.Length - 1, 1);
            }
            obj.AppendFormat("}}");
            js.Append("}");
         }
         js.AppendFormat("var _url=\"{0}\"{1};", url,
            requestmethod == GET && hasParams ? "+ \"?\" + " + string.Format("objectToQueryString({0})", obj) : "");
         js.AppendFormat(
            "var opts={{success:trd, dataType:\"json\", contentType: \"application/json\", type:\"{2}\", async:{0}, cache:{1}, url: _url", jsattribute.Async ? "true" : "false",
            jsattribute.CacheRequest ? "true" : "false", requestmethod);
         if (requestmethod == POST)
         {
            if (hasParams)
            {
               js.AppendFormat(
                  "{0}", hasParams ? string.Format(", data:JSON.stringify({0})", obj) : "");   
            }
         }

         js.Append("};jQuery.extend(opts,options);return jQuery.ajax(opts);},");
      }

      protected override IEnumerable<IGrouping<Type, MethodInfo>> GetMethodsWith<TAttribute>(params Assembly[] asm)
      {
         var methods =
            from ass in asm
            from t in ass.GetTypes()
            from m in t.GetMethods()
            where m.IsDefined(typeof (TAttribute), false)
                  && m.IsDefined(typeof (NonActionAttribute), false) == false
            select m;

         return methods.GroupBy(m => m.DeclaringType);
      }

      protected override string CacheKey
      {
         get { return string.Join("_", Qstring.Split(',').OrderBy(w => w)); }
      }

      protected override string InnerObject
      {
         get { return string.Empty; }
      }
   }
}