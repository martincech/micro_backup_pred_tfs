﻿using System.Web.Routing;

namespace JsAction
{
    /// <summary>
    /// Static class with singleton instance
    /// </summary>
    public static class JsActionRouteHandlerInstance
    {
        /// <summary>
        /// Static singleton Route to add
        /// </summary>
        public static readonly Route JsActionRoute = new Route("JsAction", new RouteValueDictionary(new { action = "JsAction", controller = "Foo" }), new JsActionHandler());


    }
}
