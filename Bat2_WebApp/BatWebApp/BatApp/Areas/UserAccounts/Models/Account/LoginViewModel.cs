using System.ComponentModel.DataAnnotations;

namespace BatApp.Areas.UserAccounts.Models.Account
{
   public class LoginViewModel
   {
      [Required]
      [Display(ResourceType = typeof (Resources.Resources), Name = "UserName")]
      public string UserName { get; set; }

      [Required]
      [DataType(DataType.Password)]
      [Display(ResourceType = typeof (Resources.Resources), Name = "Password")]
      public string Password { get; set; }

      [Display(ResourceType = typeof (Resources.Resources), Name = "RememberMe")]
      public bool RememberMe { get; set; }
   }
}