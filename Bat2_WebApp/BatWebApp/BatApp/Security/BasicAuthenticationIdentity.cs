﻿using System.Security.Principal;

namespace BatApp.Security
{
   public class BasicAuthenticationIdentity : GenericIdentity
   {
      public BasicAuthenticationIdentity(string name, string password)
         : base(name, "Basic")
      {
         Password = password;
      }

      /// <summary>
      /// Basic Auth Password for custom authentication
      /// </summary>
      public string Password { get; set; }
   }
}
