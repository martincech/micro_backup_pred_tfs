﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;

namespace DataModel
{
   public partial class BatModelContainer
   {
      /// <summary>
      ///     If true validates that emails are unique
      /// </summary>
      public bool RequireUniqueEmail { get; set; }

      /// <summary>
      ///     Validates that UserNames are unique and case insenstive
      /// </summary>
      /// <param name="entityEntry"></param>
      /// <param name="items"></param>
      /// <returns></returns>
      protected override DbEntityValidationResult ValidateEntity(DbEntityEntry entityEntry,
          IDictionary<object, object> items)
      {
         if (entityEntry == null || entityEntry.State != EntityState.Added)
            return base.ValidateEntity(entityEntry, items);

         var errors = new List<DbValidationError>();
         var user = entityEntry.Entity as User;
         //check for uniqueness of user name and email
         if (user != null)
         {
            if (Users.Any(u => String.Equals(u.UserName, user.UserName) && u.CompanyId == user.CompanyId))
            {
               errors.Add(new DbValidationError("User",
                  String.Format(CultureInfo.CurrentCulture, Resources.Resources.DuplicateUserName, user.UserName, user.Company.Name)));
            }
            if (RequireUniqueEmail && Users.Any(u => String.Equals(u.Email, user.Email)))
            {
               errors.Add(new DbValidationError("User",
                  String.Format(CultureInfo.CurrentCulture, Resources.Resources.DuplicateEmail, user.Email)));
            }
         }
         else
         {
            var role = entityEntry.Entity as Role;
            //check for uniqueness of role name
            if (role != null && Roles.Any(r => String.Equals(r.Name, role.Name)))
            {
               errors.Add(new DbValidationError("Role",
                  String.Format(CultureInfo.CurrentCulture, Resources.Resources.RoleAlreadyExists, role.Name)));
            }
         }
         
         return errors.Any() ? new DbEntityValidationResult(entityEntry, errors) : base.ValidateEntity(entityEntry, items);
      }
   }
}
