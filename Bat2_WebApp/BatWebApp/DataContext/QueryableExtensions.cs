﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Linq.Expressions;

namespace DataContext
{
    internal static class QueryableExtensions
    {
        public static IQueryable<T> WhereContains<T>(this IQueryable<T> items, List<string> properties,
                                                     string searchString)
        {
            if (properties.Count == 0 || string.IsNullOrEmpty(searchString))
            {
                return items;
            }

            var typeOfT = typeof(T);
            var parameter = Expression.Parameter(typeOfT, "x");
            var propertyName = properties.First();

            var props = propertyName.Split('.');
            Expression expr = parameter;
            foreach (var prop in props.Take(props.Length - 1))
            {
                var pi = typeOfT.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                typeOfT = pi.PropertyType;
            }

            //string lastPropName = props.Last();
            var propertyAccess = Expression.PropertyOrField(expr, props.Last());


            var expression = GetSearchExpression(searchString, propertyAccess);


            foreach (var property in properties.Skip(1))
            {
                var propsi = property.Split('.');
                Expression expri = parameter;
                typeOfT = typeof(T);

                foreach (var prop in propsi.Take(propsi.Length - 1))
                {
                    var pi = typeOfT.GetProperty(prop);
                    expri = Expression.Property(expri, pi);
                    typeOfT = pi.PropertyType;
                }

                propertyAccess = Expression.PropertyOrField(expri, propsi.Last());

                var expressionNext = GetSearchExpression(searchString, propertyAccess);
                expression = Expression.OrElse(expression, expressionNext);
            }

            return items.Where(Expression.Lambda<Func<T, bool>>(expression, parameter));
        }

        private static Expression GetSearchExpression(string searchString, MemberExpression propertyAccess)
        {
            Expression exp;

            if ((propertyAccess.Type == typeof(double)) || (propertyAccess.Type == typeof(int)))
            {
                var stringConvert = typeof(SqlFunctions).GetMethod("StringConvert",
                                                                           new[]
                                                                               {
                                                                                   typeof (double?), typeof (Int32?),
                                                                                   typeof (Int32?)
                                                                               });
                exp = Expression.Call(null, stringConvert,
                                      new Expression[]
                                          {
                                              Expression.Convert(propertyAccess, typeof (double?)),
                                              Expression.Constant(5, typeof (Int32?)),
                                              Expression.Constant(3, typeof (Int32?))
                                          });
            }
            else if ((propertyAccess.Type == typeof(string)))
            {
                exp = propertyAccess;
            }
            else
            {
                return Expression.Constant(false);
            }

            Expression expression = Expression.Call(exp, "Contains", null,
                                                    Expression.Constant(searchString));
            return expression;
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> items, string propertyName,
                                                      string sortDir = "asc")
        {
            var typeOfT = typeof(T);
            var method = (sortDir.Equals("asc")) ? "OrderBy" : "OrderByDescending";
            var parameter = Expression.Parameter(typeOfT, "parameter");

            var props = propertyName.Split('.');
            Expression expr = parameter;
            foreach (var prop in props.Take(props.Length - 1))
            {
                var pi = typeOfT.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                typeOfT = pi.PropertyType;
            }

            var lastPropName = props.Last();
            var propertyType = typeOfT.GetProperty(lastPropName).PropertyType;
            var propertyAccess = Expression.PropertyOrField(expr, lastPropName);
            var orderExpression = Expression.Lambda(propertyAccess, parameter);

            var expression = Expression.Call(typeof(Queryable), method,
                                                              new[] { typeof(T), propertyType },
                                                              items.Expression, Expression.Quote(orderExpression));
            return (IOrderedQueryable<T>)items.Provider.CreateQuery<T>(expression);
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IQueryable<T> items, string propertyName,
                                                     string sortDir = "asc")
        {
            var typeOfT = typeof(T);
            var method = (sortDir.Equals("asc")) ? "ThenBy" : "ThenByDescending";
            var parameter = Expression.Parameter(typeOfT, "parameter");

            var props = propertyName.Split('.');
            Expression expr = parameter;
            foreach (var prop in props.Take(props.Length - 1))
            {
                var pi = typeOfT.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                typeOfT = pi.PropertyType;
            }

            var lastPropName = props.Last();
            var propertyType = typeOfT.GetProperty(lastPropName).PropertyType;
            var propertyAccess = Expression.PropertyOrField(expr, lastPropName);
            var orderExpression = Expression.Lambda(propertyAccess, parameter);

            var expression = Expression.Call(typeof(Queryable), method,
                                                              new[] { typeof(T), propertyType },
                                                              items.Expression, Expression.Quote(orderExpression));
            return (IOrderedQueryable<T>)items.Provider.CreateQuery<T>(expression);
        }
    }
}
