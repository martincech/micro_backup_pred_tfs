﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DataModel;
using Microsoft.AspNet.Identity;

namespace DataContext
{
   public class RoleStore : IQueryableRoleStore<Role, Guid>
   {
      private bool disposed;
      private EntityStore<Role> roleStore;

      /// <summary>
      /// Constructor which creates a new instance of <see cref="BatModelContainer"/> context.
      /// </summary>
      public RoleStore()
         : this(new BatModelContainer())
      {
      }

      /// <summary>
      ///     Constructor which takes a db context and wires up the stores with default instances using the context
      /// </summary>
      /// <param name="context"></param>
      public RoleStore(BatModelContainer context)
      {
         if (context == null)
         {
            throw new ArgumentNullException("context");
         }
         Context = context;
         roleStore = new EntityStore<Role>(context);
      }

      /// <summary>
      ///     Context for the store
      /// </summary>
      public BatModelContainer Context { get; private set; }

      /// <summary>
      ///     If true will call dispose on the BatModelContainer during Dipose
      /// </summary>
      public bool DisposeContext { get; set; }

      /// <summary>
      ///     Find a role by id
      /// </summary>
      /// <param name="roleId"></param>
      /// <returns></returns>
      public Task<Role> FindByIdAsync(Guid roleId)
      {
         ThrowIfDisposed();
         return roleStore.GetByIdAsync(roleId);
      }

      /// <summary>
      ///     Find a role by name
      /// </summary>
      /// <param name="roleName"></param>
      /// <returns></returns>
      public Task<Role> FindByNameAsync(string roleName)
      {
         ThrowIfDisposed();
         return
            roleStore.EntitySet.FirstOrDefaultAsync(u => u.Name.ToUpper() == roleName.ToUpper());
      }

      /// <summary>
      ///     Insert an entity
      /// </summary>
      /// <param name="role"></param>
      public virtual async Task CreateAsync(Role role)
      {
         ThrowIfDisposed();
         if (role == null)
         {
            throw new ArgumentNullException("role");
         }
         roleStore.Create(role);
         await Context.SaveChangesAsync();
      }

      /// <summary>
      ///     Mark an entity for deletion
      /// </summary>
      /// <param name="role"></param>
      public virtual async Task DeleteAsync(Role role)
      {
         ThrowIfDisposed();
         if (role == null)
         {
            throw new ArgumentNullException("role");
         }
         roleStore.Delete(role);
         await Context.SaveChangesAsync();
      }

      /// <summary>
      ///     Update an entity
      /// </summary>
      /// <param name="role"></param>
      public virtual async Task UpdateAsync(Role role)
      {
         ThrowIfDisposed();
         if (role == null)
         {
            throw new ArgumentNullException("role");
         }
         roleStore.Update(role);
         await Context.SaveChangesAsync();
      }

      /// <summary>
      ///     Returns an IQueryable of users
      /// </summary>
      public IQueryable<Role> Roles
      {
         get { return roleStore.EntitySet; }
      }

      /// <summary>
      ///     Dispose the store
      /// </summary>
      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }

      private void ThrowIfDisposed()
      {
         if (disposed)
         {
            throw new ObjectDisposedException(GetType().Name);
         }
      }

      /// <summary>
      ///     If disposing, calls dispose on the Context.  Always nulls out the Context
      /// </summary>
      /// <param name="disposing"></param>
      protected virtual void Dispose(bool disposing)
      {
         if (DisposeContext && disposing && Context != null)
         {
            Context.Dispose();
         }
         disposed = true;
         Context = null;
         roleStore = null;
      }
   }
}