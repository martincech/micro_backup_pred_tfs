﻿using System;
using Bat2Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BatApp.Services.DataPublication;

namespace BatApp.Tests.Services.DataPublication
{
    [TestClass]
    public class BinaryDataParserTest
    {
        private byte[] binaryDataLong;
        private byte[] binaryDataShort;

        [TestInitialize]
        public void Init()
        {
            PublishedData data = new PublishedData()
            {

                DateTime = new DateTime(2001, 2, 3, 4, 5, 6, 7),
                Day = 31,
                // Stat data in base balues 1g = 10 of base value
                Male = new GenderStats()
                {
                    Average = 1,
                    Count = 2,
                    Cv = 3,
                    Gain = 4,
                    Sigma = 5,
                    Target = 6,
                    Uniformity = 7
                },
                Female = new GenderStats()
                {
                    Average = 11,
                    Count = 12,
                    Cv = 13,
                    Gain = 14,
                    Sigma = 15,
                    Target = 16,
                    Uniformity = 17
                },
                SerialNumber = 1324

            };
            binaryDataLong = MessageParser.GenerateData(data);
            data.Female = null;
            binaryDataShort = MessageParser.GenerateData(data);
        }

        [TestMethod]
        public void MessageLong_Correct()
        {
            var parser = new BinaryDataParser();
            Assert.IsTrue(parser.ParseData(binaryDataLong));
            //Male
            Assert.AreEqual(new DateTime(2001, 2, 3, 4, 5, 6, 7).ToString(), parser.Male.Date.ToString());
            Assert.AreEqual(31, parser.Male.Day);
            Assert.AreEqual(0.1, parser.Male.Average);
            Assert.AreEqual(2, parser.Male.Count);
            Assert.AreEqual(0.3, parser.Male.Cv);
            Assert.AreEqual(0.4, parser.Male.Gain);
            Assert.AreEqual(0.5, parser.Male.Sigma);
            Assert.AreEqual(0.7, parser.Male.Uni);
            //Female
            Assert.AreEqual(new DateTime(2001, 2, 3, 4, 5, 6, 7).ToString(), parser.Female.Date.ToString());
            Assert.AreEqual(31, parser.Female.Day);
            Assert.AreEqual(1.1, parser.Female.Average);
            Assert.AreEqual(12, parser.Female.Count);
            Assert.AreEqual(1.3, parser.Female.Cv);
            Assert.AreEqual(1.4, parser.Female.Gain);
            Assert.AreEqual(1.5, parser.Female.Sigma);
            Assert.AreEqual(1.7, parser.Female.Uni);
        }

        [TestMethod]
        public void MessageShort_Correct()
        {
            var parser = new BinaryDataParser();
            Assert.IsTrue(parser.ParseData(binaryDataShort));
            //Male
            Assert.AreEqual(new DateTime(2001, 2, 3, 4, 5, 6, 7).ToString(), parser.Male.Date.ToString());
            Assert.AreEqual(31, parser.Male.Day);
            Assert.AreEqual(0.1, parser.Male.Average);
            Assert.AreEqual(2, parser.Male.Count);
            Assert.AreEqual(0.3, parser.Male.Cv);
            Assert.AreEqual(0.4, parser.Male.Gain);
            Assert.AreEqual(0.5, parser.Male.Sigma);
            Assert.AreEqual(0.7, parser.Male.Uni);
            Assert.AreEqual(null, parser.Female);
        }

        [TestMethod]
        public void Message_IncorectType()
        {
            var parser = new BinaryDataParser();
            Assert.IsFalse(parser.ParseData(new object()));
        }

        [TestMethod]
        public void Message_IncorectData()
        {
            var parser = new BinaryDataParser();
            Assert.IsFalse(parser.ParseData(new byte[] {1, 1, 2, 3}));
        }
    }
}
