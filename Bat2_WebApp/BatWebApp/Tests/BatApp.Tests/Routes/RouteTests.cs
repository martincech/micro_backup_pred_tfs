﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using BatApp.Areas.UserAccounts;
using BatApp.Tests.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BatApp.Tests.Routes
{
   [TestClass]
   public class RouteTests
   {
      #region Private helpers



      private static bool TestIncomingRouteResult(RouteData routeResult,
         string controller, string action, object propertySet = null)
      {
         Func<object, object, bool> valCompare =
            (v1, v2) => StringComparer.InvariantCultureIgnoreCase.Compare(v1, v2) == 0;

         var result = valCompare(routeResult.Values["controller"], controller) &&
                      valCompare(routeResult.Values["action"], action);

         if (propertySet == null) return result;
         var propInfo = propertySet.GetType().GetProperties();
         if (propInfo.Any(pi => !(routeResult.Values.ContainsKey(pi.Name))
                                && valCompare(routeResult.Values[pi.Name], pi.GetValue(propertySet, null))))
         {
            result = false;
         }
         return result;
      }
      private static RouteData CreateRouteData(string url, string httpMethod = "GET")
      {
         var routes = new RouteCollection();
         var userAccountsArea = new UserAccountsAreaRegistration();
         Assert.AreEqual("UserAccounts", userAccountsArea.AreaName);
         var areaRegistrationContext = new AreaRegistrationContext(userAccountsArea.AreaName, routes);
         userAccountsArea.RegisterArea(areaRegistrationContext);

         RouteConfig.RegisterRoutes(routes);
         var result = routes.GetRouteData(BaseControllerFake.CreateHttpContext(url, httpMethod));
         return result;
      }

      private void TestRouteMatch(string url, string controller, string action,
         object routeProperties = null, string httpMethod = "GET")
      {
         var result = CreateRouteData(url, httpMethod);

         Assert.IsNotNull(result);
         Assert.IsTrue(TestIncomingRouteResult(result, controller, action, routeProperties));
      }

      private void TestRouteFail(string url)
      {
         var result = CreateRouteData(url);

         Assert.IsTrue(result == null || result.Route == null);
      }

      #endregion

      [TestMethod]
      public void TestIncomingRoutes()
      {
         TestRouteMatch("~/Home/Index", "Home", "Index");
         //TestRouteMatch("~/Home", "Home", "Index");
         //TestRouteMatch("~/", "Home", "Index");

         TestRouteMatch("~/UserAccounts/Contr", "Contr", "Index");
         TestRouteMatch("~/UserAccounts/Contr/Index", "Contr", "Index");
         //TestRouteMatch("~/UserAccounts", "UserAccounts", "Index");
      }
   }
}