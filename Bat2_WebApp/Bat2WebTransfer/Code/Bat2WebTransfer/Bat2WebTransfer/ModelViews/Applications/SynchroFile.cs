﻿using System.Collections.Generic;
using Utilities.Observable;

namespace Bat2WebTransfer.ModelViews.Applications
{
   public class SynchroFile : ObservableObject
   {
      private string _name;
      private bool _isChecked;
      private int _count;                 // number of synchornizated items
      private bool _enabled;
      private IEnumerable<object> _data;  // data read from database

      public SynchroFile(string name, bool isChecked = true)
      {
         Name = name;
         IsChecked = isChecked;
         Count = 0;
         Enabled = true;        
      }

      public string Name { get { return _name; } set { SetProperty(ref _name, value); } }
      public bool IsChecked { get { return _isChecked; } set { SetProperty(ref _isChecked, value); } }
      public int Count { get { return _count; } set { SetProperty(ref _count, value); } }
      public bool Enabled { get { return _enabled; } set { SetProperty(ref _enabled, value); } }
      public IEnumerable<object> Data { get { return _data; } set { SetProperty(ref _data, value); } }
   }
}
