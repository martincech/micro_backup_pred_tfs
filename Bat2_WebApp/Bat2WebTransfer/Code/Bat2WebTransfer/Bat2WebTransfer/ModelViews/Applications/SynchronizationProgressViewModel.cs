﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Bat2Library.Bat2Old.DB;
using Bat2WebTransfer.Map;
using Desktop.Wpf.Applications;
using Services.WebApi;
using Utilities.Extensions;
using Utilities.Observable;

namespace Bat2WebTransfer.ModelViews.Applications
{
   public class SynchronizationProgressViewModel : ObservableObject
   {
      #region Private helpers

      private string _actualFile;   
      private int _totalProgress;
      private int _partialProgress;
      private ICommand _stornoSynchroCommand;
      private ICommand _synchroCommand;
      private List<string> _selectedFiles;
      private List<string> _selectedTypes;

      private readonly MainWindowViewModel _mainVm;      // main view model
      private event EventHandler<string> TaskComplete;   // event for start next task

      #endregion

      #region Public interfaces

      public SynchronizationProgressViewModel(MainWindowViewModel vm)
      {
         _mainVm = vm;
         TaskComplete += OnTaskComplete;
      }

      public string ActualFile { get { return _actualFile; } set { SetProperty(ref _actualFile, value); } }
      public int TotalProgress { get { return _totalProgress; } set { SetProperty(ref _totalProgress, value); } }
      public int PartialProgress { get { return _partialProgress; } set { SetProperty(ref _partialProgress, value); } }

      #region Commands

      /// <summary>
      /// Stop synchronization
      /// </summary>
      public ICommand StornoSynchroCommand
      {
         get
         {
            if (_stornoSynchroCommand == null)
            {
               _stornoSynchroCommand = new RelayCommand<Window>(
                  //execute
                  w =>
                  {  // interrupt synchronization and close view
                     TaskComplete -= OnTaskComplete;
                     _mainVm.Logs.Add(Properties.Resources.SynchroCancel);
                     w.Close();
                  },
                  //can execute
                  w => true
                  );
            }
            return _stornoSynchroCommand;
         }
      }

      /// <summary>
      /// Start synchronization
      /// </summary>
      public ICommand SynchroCommand
      {
         get
         {
            if (_synchroCommand == null)
            {
               _synchroCommand = new RelayCommand(
                  //execute
                  Synchronize,
                  //can execute
                  () => true
                  );
            }
            return _synchroCommand;
         }
      }

      #endregion

      #endregion

      #region Private helpers

      /// <summary>
      /// Synchronize data from Bat2 database to web.
      /// </summary>
      private void Synchronize()
      {
         _mainVm.SynchronizationTypes.ForEach(t =>
         {
            t.Count = 0;   // reset counter before start synchronization
            t.Data = null; // reset data
         });
         CreateSelectedFiles();
         ReadDbFile();

         _selectedTypes = _mainVm.SynchronizationTypes.Where(t => t.IsChecked).Select(s => s.Name).ToList();
         if (_selectedTypes.Count == 0) return;

         ActualFile = _selectedTypes.First();
         CallApi(ActualFile, _mainVm.SynchronizationTypes.First(f => f.Name.Equals(ActualFile)).Data);     
      }

      /// <summary>
      /// Create selected files from selected synchronization types.
      /// </summary>
      private void CreateSelectedFiles()
      {
         _selectedFiles = new List<string>();
         foreach (var type in _mainVm.SynchronizationTypes.Where(x => x.IsChecked))
         {
            if (type.Name.Equals(Properties.Resources.Scales) || 
                type.Name.Equals(Properties.Resources.Data) ||
                type.Name.Equals(Properties.Resources.Flocks))
            {
               AddSelectedFile("Sms");
            }
            else if (type.Name.Equals(Properties.Resources.Curves))
            {
               AddSelectedFile("Flocks");
               AddSelectedFile("FFlocks");
               AddSelectedFile("SetupF");
               AddSelectedFile("Curves");
            }
         }
      }

      /// <summary>
      /// Add file to private list _selectedFiles if file is contained 
      /// at list of Files. File is added only if is not already contains at list.
      /// </summary>
      /// <param name="file">file name</param>
      private void AddSelectedFile(string file)
      {
         if (_mainVm.Files.Contains(file) && !_selectedFiles.Contains(file))
         {
            _selectedFiles.Add(file);
         }
      }

      #region Read database

      /// <summary>
      /// Read paradox database files.
      /// </summary>
      private void ReadDbFile()
      {
         var mapper = new ParadoxMapper(_mainVm.DbFolderPath);
         foreach (var file in _selectedFiles)
         {
            var readNull = false;
            switch (file)
            {
               case "Sms":
                  var tmpSms = mapper.SmsMap(file);
                  var sms = new List<Sms>(tmpSms);
                  readNull = tmpSms == null;
                  if (sms.Any())
                  {
                     // statistics
                     _mainVm.SynchronizationTypes.First(x => x.Name.Equals(Properties.Resources.Data)).Data = sms;
                     // scales
                     var ids = sms.Select(s => (object)s.Id).Distinct();                  
                     _mainVm.SynchronizationTypes.First(x => x.Name.Equals(Properties.Resources.Scales)).Data = ids;
                     // flocks
                     var dataFlocks = mapper.DataFlockMap(sms);
                     _mainVm.SynchronizationTypes.First(x => x.Name.Equals(Properties.Resources.Flocks)).Data =
                        dataFlocks;
                  }
                  break;
               case "Flocks":
               case "FFlocks":
                  var tmpFlocks = mapper.FlockMap(file);
                  var flocks = new List<SetupF>(tmpFlocks);
                  readNull = tmpFlocks == null;
                  AssignCurves(mapper.FlockCurves(flocks));
                  break;
               case "SetupF":
                  var tmpSetupFs = mapper.SetupFMap(file);
                  var setupFs = new List<SetupF>(tmpSetupFs);
                  readNull = tmpSetupFs == null;
                  AssignCurves(mapper.FlockCurves(setupFs));
                  break;
               case "Curves":
                  var curves = mapper.CurveMap(file);
                  readNull = curves == null;
                  AssignCurves(curves);
                  break;
               // skip Bat2 db files which aren't synchrnoized
               case "Cal":
               case "Config":
               case "FCal":
               case "FConfig":
               case "FHeader":
               case "Filters":
               case "FOnline":
               case "FRecords":
               case "FStat":
               case "Header":
               case "ModbusScales":
               case "Numbers":
               case "Online":
               case "Records":
               case "Setup":
               case "SetupS":
               case "Stat":
                  _mainVm.Logs.Add(string.Format(Properties.Resources.FileNotSynchro, file));                
                  return;
            }

            _mainVm.Logs.Add(!readNull
               ? string.Format(Properties.Resources.ReadOK, file)
               : string.Format(Properties.Resources.ReadFail, file));
         }  
      }

      private void AssignCurves(IEnumerable<GrowthCurve> curves)
      {
         if (curves == null) return;
         var curveList = curves.ToList();

         var origCurves = _mainVm.SynchronizationTypes.First(x => x.Name.Equals(Properties.Resources.Curves)).Data;
         _mainVm.SynchronizationTypes.First(x => x.Name.Equals(Properties.Resources.Curves)).Data =
            origCurves != null
               ? origCurves.Concat(curveList)
               : curveList;
      }

      #endregion

      /// <summary>
      /// Call web api post methods.
      /// </summary>
      /// <param name="fileName">database file name</param>
      /// <param name="data">data sending to server</param>
      private void CallApi(string fileName, IEnumerable<object> data)
      {
         var client = new WebApiClient(_mainVm.ServerUrl, _mainVm.UserName, _mainVm.Password);
         var name = fileName;
         var sendingData = data.ToArray();
      
         ThreadPool.QueueUserWorkItem(param =>
         {
            client.ItemSynced += client_ItemSynced;
            var count = client.Synchronize(sendingData, name);
            var dispatcher = Application.Current.Dispatcher;
            dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate
            {
               // text format: file name, count of succesfull synchronized items, total count
               _mainVm.Logs.Add(string.Format(Properties.Resources.SynchronizedFiles, name, count, sendingData.Count()));
               AddCountToSynchroType(name, count);
               InvokeTaskComplete(name);
            });          
         });
      }

      void client_ItemSynced(object sender, int percent)
      {
          var dispatcher = Application.Current.Dispatcher;
         dispatcher.Invoke(DispatcherPriority.Normal, (Action) delegate
         {
            PartialProgress = percent;
         });
      }

      /// <summary>
      /// Add count of synchronizated items to related synchronization type.
      /// </summary>
      /// <param name="fileName">synchronization type name</param>
      /// <param name="count">count of synchornizated items</param>
      private void AddCountToSynchroType(string fileName, int count)
      {
         var type = _mainVm.SynchronizationTypes.FirstOrDefault(f => f.Name.Equals(fileName));
         if (type == null) return;
         type.Count += count;
      }

      /// <summary>
      /// Add total statistic to Log.
      /// </summary>
      private void PrintSynchronizationResult()
      {
         var time = DateTime.Now;
         var delimiter = new StringBuilder().Insert(0, "=", 30).ToString();
         _mainVm.Logs.Add(delimiter);
         _mainVm.Logs.Add("= " + Properties.Resources.Result + " = " + time);
         _mainVm.Logs.Add(delimiter);
         foreach (var type in _mainVm.SynchronizationTypes.Where(x => x.IsChecked))
         {
            _mainVm.Logs.Add(type.Name + ": " + type.Count + " " + Properties.Resources.WereSynchronized);
         }
         _mainVm.Logs.Add(delimiter);
      }

      /// <summary>
      /// Set progress bar value and start next task.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="s">name of finished synchornization file</param>
      private void OnTaskComplete(object sender, string s)
      {        
         var index = _selectedTypes.IndexOf(s);        
         var synchroTypeCount = _selectedTypes.Count;
         var percentage = ((index + 1.0) / synchroTypeCount) * 100;
         TotalProgress = (int)percentage;

         if (index + 1 >= synchroTypeCount)
         {  // taks is complete
            _selectedFiles = null;
            _selectedTypes = null;
            //ActualFile = "";
            TaskComplete -= OnTaskComplete;
            PrintSynchronizationResult();
         }
         else
         {  // start next task
            ActualFile = _selectedTypes.ElementAt(index + 1);
            CallApi(ActualFile, _mainVm.SynchronizationTypes.First(f => f.Name.Equals(ActualFile)).Data);
         }
      }

      private void InvokeTaskComplete(string name)
      {
         if (TaskComplete != null)
         {
            TaskComplete(this, name);
         }
      }

      #endregion
   }
}
