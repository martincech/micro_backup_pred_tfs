﻿using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Bat2WebTransfer.ModelViews.Applications;

namespace Bat2WebTransfer.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow
   {
      private MainWindowViewModel _vm;

      public MainWindow()
      {
         InitializeComponent();
      }

      private void MainWindow_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
      {
         if (_vm != null)
         {  // remove previous event handler
            _vm.Logs.CollectionChanged -= LogsOnCollectionChanged;
         }

         _vm = DataContext as MainWindowViewModel;
         if (_vm != null)
         {
            _vm.Logs.CollectionChanged += LogsOnCollectionChanged;
         }
      }

      /// <summary>
      /// Scroll down when listbox source change.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="notifyCollectionChangedEventArgs"></param>
      private void LogsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
      {
         var border = VisualTreeHelper.GetChild(LogListBox, 0) as Decorator;
         if (border == null) return;
         
         var scroll = border.Child as ScrollViewer;
         if (scroll != null)
         {
            scroll.ScrollToEnd();
         }
      }      
   }
}
