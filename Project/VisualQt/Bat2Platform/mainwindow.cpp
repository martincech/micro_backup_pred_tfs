//******************************************************************************
//
//   MainWindow.cpp  Bat2Platform simulator window
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Bat2Platform/Rcs.h"

#define SOCKET_SAMPLING_TIME 100
#define TIMER_SLOW_TIME      1000

//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   _dump = new CrtDump( ui->crt);      // initialize CrtDump utility
   _dump->setMode( CrtDump::MIXED);    // show mixed dump
   _dump->setEnable( ui->actionDump->isChecked());
   // Simulator
   _PlatformSimulator = new PlatformSimulator();
   _PlatformSimulator->show();
   // activate server :
   _server = new LocalServer( this);
   _server->setLogger( _dump);         // set activity logger
   connect( _server, SIGNAL( dataReceived()), this, SLOT( dataReceived()));
   // create read timer :
   _timer = new QTimer( this);
   connect( _timer, SIGNAL( timeout()), this, SLOT( timerTimeout()));
   _timer->start( SOCKET_SAMPLING_TIME);
   RcsInit( _server);
} // MainWindow

//------------------------------------------------------------------------------
//   Destructor
//------------------------------------------------------------------------------

MainWindow::~MainWindow()
{
   delete ui;
   if( _dump){
      delete _dump;
   }
   if( _timer){
      delete _timer;
   }
   if( _PlatformSimulator){
      delete _PlatformSimulator;
   }
   if( _server){
      delete _server;
   }
} // ~MainWindow

//------------------------------------------------------------------------------
//   Dump
//------------------------------------------------------------------------------

void MainWindow::on_actionDump_triggered()
{
   _dump->setEnable( ui->actionDump->isChecked());
} // on_actionDump_triggered

//------------------------------------------------------------------------------
//  Timer
//------------------------------------------------------------------------------

void MainWindow::timerTimeout()
{
//   RcsExecute();
} // timerTimeout

void MainWindow::dataReceived()
{
   RcsExecute();
} // dataReceived
