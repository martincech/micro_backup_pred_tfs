/****************************************************************************
** Meta object code from reading C++ file 'Bat2Bootloader.h'
**
** Created: Wed Sep 25 13:13:45 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Bat2Bootloader.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Bat2Bootloader.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Bat2Bootloader[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      44,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Bat2Bootloader[] = {
    "Bat2Bootloader\0\0on_LoadFileButton_clicked()\0"
    "on_FlashButton_clicked()\0"
};

void Bat2Bootloader::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Bat2Bootloader *_t = static_cast<Bat2Bootloader *>(_o);
        switch (_id) {
        case 0: _t->on_LoadFileButton_clicked(); break;
        case 1: _t->on_FlashButton_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData Bat2Bootloader::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Bat2Bootloader::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Bat2Bootloader,
      qt_meta_data_Bat2Bootloader, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Bat2Bootloader::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Bat2Bootloader::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Bat2Bootloader::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Bat2Bootloader))
        return static_cast<void*>(const_cast< Bat2Bootloader*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Bat2Bootloader::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
