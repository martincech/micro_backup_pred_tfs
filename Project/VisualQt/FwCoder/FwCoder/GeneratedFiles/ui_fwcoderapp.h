/********************************************************************************
** Form generated from reading UI file 'fwcoderapp.ui'
**
** Created: Thu Sep 19 15:05:14 2013
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FWCODERAPP_H
#define UI_FWCODERAPP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FwCoderAppClass
{
public:
    QWidget *centralWidget;
    QPushButton *OpenConfigPushButton;
    QPushButton *EncryptPushButton;
    QLabel *FileLabel;
    QLabel *ConfigLabel;
    QPushButton *DecryptPushButton;
    QLabel *label;
    QLabel *label_2;
    QPushButton *InterpretPushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *FwCoderAppClass)
    {
        if (FwCoderAppClass->objectName().isEmpty())
            FwCoderAppClass->setObjectName(QString::fromUtf8("FwCoderAppClass"));
        FwCoderAppClass->resize(600, 400);
        centralWidget = new QWidget(FwCoderAppClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        OpenConfigPushButton = new QPushButton(centralWidget);
        OpenConfigPushButton->setObjectName(QString::fromUtf8("OpenConfigPushButton"));
        OpenConfigPushButton->setGeometry(QRect(10, 10, 75, 23));
        EncryptPushButton = new QPushButton(centralWidget);
        EncryptPushButton->setObjectName(QString::fromUtf8("EncryptPushButton"));
        EncryptPushButton->setGeometry(QRect(10, 70, 75, 23));
        FileLabel = new QLabel(centralWidget);
        FileLabel->setObjectName(QString::fromUtf8("FileLabel"));
        FileLabel->setGeometry(QRect(90, 10, 46, 13));
        ConfigLabel = new QLabel(centralWidget);
        ConfigLabel->setObjectName(QString::fromUtf8("ConfigLabel"));
        ConfigLabel->setGeometry(QRect(90, 40, 46, 13));
        DecryptPushButton = new QPushButton(centralWidget);
        DecryptPushButton->setObjectName(QString::fromUtf8("DecryptPushButton"));
        DecryptPushButton->setGeometry(QRect(10, 100, 75, 23));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(90, 70, 251, 16));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(90, 100, 251, 16));
        InterpretPushButton = new QPushButton(centralWidget);
        InterpretPushButton->setObjectName(QString::fromUtf8("InterpretPushButton"));
        InterpretPushButton->setGeometry(QRect(10, 130, 75, 23));
        FwCoderAppClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(FwCoderAppClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        FwCoderAppClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(FwCoderAppClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        FwCoderAppClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(FwCoderAppClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        FwCoderAppClass->setStatusBar(statusBar);

        retranslateUi(FwCoderAppClass);

        QMetaObject::connectSlotsByName(FwCoderAppClass);
    } // setupUi

    void retranslateUi(QMainWindow *FwCoderAppClass)
    {
        FwCoderAppClass->setWindowTitle(QApplication::translate("FwCoderAppClass", "FwCoderApp", 0, QApplication::UnicodeUTF8));
        OpenConfigPushButton->setText(QApplication::translate("FwCoderAppClass", "Open config", 0, QApplication::UnicodeUTF8));
        EncryptPushButton->setText(QApplication::translate("FwCoderAppClass", "Encrypt", 0, QApplication::UnicodeUTF8));
        FileLabel->setText(QString());
        ConfigLabel->setText(QString());
        DecryptPushButton->setText(QApplication::translate("FwCoderAppClass", "Decrypt", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("FwCoderAppClass", "hex -> fwc", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("FwCoderAppClass", "fwc -> fwe", 0, QApplication::UnicodeUTF8));
        InterpretPushButton->setText(QApplication::translate("FwCoderAppClass", "Interpret", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FwCoderAppClass: public Ui_FwCoderAppClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FWCODERAPP_H
