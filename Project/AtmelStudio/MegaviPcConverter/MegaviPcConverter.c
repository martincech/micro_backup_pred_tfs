#include "Hardware.h"       

static byte _Buffer[BUFFER_SIZE];
static byte *RxPtr = _Buffer;
static byte *TxPtr = _Buffer;


#define RxError(s) ((s) & ((1 << FE1) | (1 << DOR1) | (1 << UPE1)))
#define RxEnable() UCSR1B|= (1 << RXCIE1)
#define TxEnable() UCSR0B |= (1 << UDRIE0)
#define TxDisable() UCSR0B &= ~(1 << UDRIE0)

void MegaviSideInit(){
   UBRR1H = (byte)(BAUD_DIVISOR_MEGAVI >> 8);		        // set baud rate divisor
   UBRR1L = (byte) BAUD_DIVISOR_MEGAVI;
   UCSR1A = 0;
   UCSR1B = (1 << RXEN1) | (1 << TXEN1) | (1 << UCSZ12);	// Enable Rx/Tx, 9bits enable RX interrupt
   UCSR1C = (3 << UCSZ10);									// 9 bits, 1 stopbit
   RxEnable();
   MegaviTxInit();
   MegaviTxDisable();
}

void PcSideInit(){
	UBRR0H = (byte)(BAUD_DIVISOR_PC >> 8);					// set baud rate divisor
	UBRR0L = (byte) BAUD_DIVISOR_PC;
	UCSR0A = 0;
	UCSR0B = (1 << TXEN0) | (1 << RXEN0);					// Enable  Rx/Tx
	UCSR0C = (3 << UCSZ00);									// 8 bits, 1 stopbit
}

ISR(USART1_RX_vect){
byte status;	
			
	status = UCSR1A;
	if(RxError(status)){
		UDR1;
		return;
	}
	*RxPtr = UDR1;
	if(RxPtr == TxPtr){
		TxEnable();
		StatusLedROn();
	}
	RxPtr++;
	if( (_Buffer + BUFFER_SIZE) > RxPtr){
		return;
	}		
	RxPtr = _Buffer;
}

ISR(USART0_UDRE_vect){
	if(TxPtr != RxPtr){
		UDR0 = *TxPtr;
		TxPtr++;
		if( (_Buffer + BUFFER_SIZE) <= TxPtr){
			TxPtr = _Buffer;
		}
	} else {
		TxDisable();
		StatusLedROff();
	}		
}	
int main(void)
{
	StatusLedsInit();
	PcSideInit();
	MegaviSideInit();
	EnableInts();
    while(1)
    {
		StatusLedGOn();
		SysDelay( 100);
		StatusLedGOff();
		SysDelay( 100);
    }
}