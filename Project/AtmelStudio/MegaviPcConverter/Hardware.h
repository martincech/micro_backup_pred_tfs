/*
 * Hardware.h
 *
 * Created: 18-Sep-13 2:46:15 PM
 *  Author: Vyvoj2
 */ 


#ifndef __Hardware_H__
#define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "Unisys/Uni.h"

//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//------------------------------------------------------------------------------
// MEGAVI
//------------------------------------------------------------------------------

#define MEGAVI_BAUD          19200      // baud rate
#define BAUD_DIVISOR_MEGAVI  ((FXTAL / 16 / MEGAVI_BAUD) - 1)

// RS485 Tx control :
#define MEGAVI_RS485_DE      4          // RS485 direction control

#define MegaviTxInit()       DDRD  |=  (1 << MEGAVI_RS485_DE)
#define MegaviTxEnable()     PORTD |=  (1 << MEGAVI_RS485_DE)
#define MegaviTxDisable()    PORTD &= ~(1 << MEGAVI_RS485_DE)

//------------------------------------------------------------------------------
// PC
//------------------------------------------------------------------------------

#define PC_BAUD				57600      // baud rate
#define BAUD_DIVISOR_PC		((FXTAL / 16 / PC_BAUD) )

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLEDR   0
#define StatusLEDG   1

#define StatusLedsInit()  DDRA  |=  (1 << StatusLEDR);DDRA  |=  (1 << StatusLEDG)

#define StatusLedROn()    PORTA |=  (1 << StatusLEDR)
#define StatusLedROff()   PORTA &= ~(1 << StatusLEDR)

#define StatusLedGOn()    PORTA |=  (1 << StatusLEDG)
#define StatusLedGOff()   PORTA &= ~(1 << StatusLEDG)

//-----------------------------------------------------------------------------
// Buffer
//-----------------------------------------------------------------------------
#define BUFFER_SIZE 256


#define DELAY_COUNT   (word)(FXTAL / 4000 - 1)

void SysDelay( word ms)
// Milisecond delay
{
	word cnt;

	__asm__ __volatile__( "\n"
	"L_dl1%=:" "\n\t"
	"mov %A0, %A2" "\n\t"
	"mov %B0, %B2" "\n"
	"L_dl2%=:" "\n\t"
	"sbiw %A0, 1" "\n\t"
	"brne L_dl2%=" "\n\t"
	"sbiw %A1, 1" "\n\t"
	"brne L_dl1%=" "\n\t"
	: "=&w" (cnt)
	: "w" (ms), "r" (DELAY_COUNT)
	);
} // SysDelay

#define DisableInts() cli()
#define EnableInts()  sei()

#endif /* __Hardware_H__ */