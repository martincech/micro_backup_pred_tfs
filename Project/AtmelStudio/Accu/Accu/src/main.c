#include "Cpu/Cpu.h"
#include "SmartBattery/SmartBatterySlave.h"
#include "Iic/IicSlave.h"
#include "Sleep/Sleep.h"
#include "Accu/Accu.h"
#include "System/SysTimer.h"
#include "System/Delay.h"
#include "Accu/Accu.h"
#include "Charger/Charger.h"
#include "Rtc/Rtc.h"
#include "Rpc/Rpc.h"

volatile TYesNo FlagRtc = YES;
volatile TYesNo FlagTimerFast = YES;
volatile TYesNo FlagTimerSlow = YES;
volatile TYesNo FlagEvent = YES;
volatile TYesNo FlagIic = YES;
volatile TYesNo FlagCharger = YES;
volatile TYesNo FlagButton = YES;

#define BLINK_PERIOD          700 / TIMER_FAST_PERIOD
#define TIMER_FAST_PERIOD     TIMER_PERIOD
#define TIMER_SLOW_PERIOD     5000

typedef enum {
   K_IDLE,
   K_TIMER_RTC,
   K_TIMER_SLOW,
   K_TIMER_FAST,
   K_IIC,
   K_CHARGER,
   K_BUTTON,
   K_EVENT   
} EKey;

int SysScheduler( void);
// Scheduler

int main (void)
{
byte Status;
byte Capacity = ACCU_CAPACITY_INVALID;
byte Led = 0;
TYesNo RpcStarted = NO;
byte Phase = 0;
byte Key;
   CpuInit();
   SmartBatteryInit();
   AccuInit();
   ChargerInit();
   SleepInit();
   SleepEnable( NO);
   ButtonInit();
   LedInit();
   RtcInit();
   RpcInit();
   SysTimerStart();
   LedSet(LED0 | LED1 | LED2 | LED3);
   SysDelay(1000);
   LedSet(0);
   InterruptEnable();
   
   forever {
      IicSlaveExecute();
      if(RpcStarted) {
         RpcExecute();
      }
      Key = SysScheduler();    
      switch(Key) {
         case K_TIMER_RTC:
            AccuExecute();
            break;
         
         case K_TIMER_SLOW:
            Capacity = AccuCapacityRemaining();
            break;
         
         case K_TIMER_FAST:
            Phase++;
            break;

         default:
            break;
      }
      
      if(Key == K_IDLE || Key == K_IIC || Key == K_TIMER_SLOW || Key == K_TIMER_RTC) {
         continue;
      }
      
      if(Key == K_TIMER_FAST || Key == K_CHARGER) {
         ChargerTemperatureSet(AccuTemperature());
         ChargerExecute();
         Status = ChargerStatus();
         switch(Status) {
            case CHARGER_OFF:
               if(RpcStarted) {
                  RpcStop();
                  RpcStarted = NO;
               }
               break;
               
            default:
               if(!RpcStarted) {
                  RpcStart();
                  RpcStarted = YES;
               }
               break;
         }

         switch (Status) {
            case CHARGER_DONE:
               Led = LED0 | LED1 | LED2 | LED3;
               break;
               
            case CHARGER_PROGRESS:
               if(Capacity > 90) {
                  Led = LED0 | LED1 | LED2;
               } else if(Capacity > 50) {
                  Led = LED0 | LED1;
               } else if(Capacity > 25) {
                  Led = LED0;
               } else {
                  Led = 0;
               }
               if(Phase < BLINK_PERIOD) {
                  Led = (Led << 1) | LED0; // blink the last LED
               } else {
                  if(Phase >= 2 * BLINK_PERIOD) {
                     Phase = 0;
                  }
               }
               break;

            case CHARGER_ERROR:
               if(Phase < BLINK_PERIOD) {
                  Led = LED0;
               } else if(Phase < 2 * BLINK_PERIOD) {
                  Led = LED1;
               } else if(Phase < 3 * BLINK_PERIOD) {
                  Led = LED2;
               } else {
                  Led = LED3;
                  if(Phase >= 4 * BLINK_PERIOD) {
                     Phase = 0;
                  }
               }
               break;
               
            default:
               Led = 0;
               Phase = 0;
               break;
         }
      }

      if((Key == K_TIMER_FAST || Key == K_BUTTON) && ChargerStatus() == CHARGER_OFF) { // Charger has priority over button
         if(ButtonPushed()) {
            if(Capacity <= 10 || Capacity == ACCU_CAPACITY_INVALID) {
               if(Phase < BLINK_PERIOD) {
                  Led = LED0;
               } else {
                  Led = 0;
                  if(Phase >= 2 * BLINK_PERIOD) {
                     Phase = 0;
                  }                     
               }
            } else if(Capacity > 90) {
               Led = LED0 | LED1 | LED2 | LED3;
            } else if(Capacity > 50) {
               Led = LED0 | LED1 | LED2;
            } else if(Capacity > 25) {
               Led = LED0 | LED1;
            } else {
               Led = LED0;
            }
         } else {
            Led = 0;
            Phase = 0;
         }   
      }
      
      LedSet(Led);       
   }  
}


int SysScheduler( void)
// Scheduler
{
int Key = K_IDLE;
   while(Key == K_IDLE) {
      InterruptDisable();
      if(FlagIic) {
         Key = K_IIC;
         FlagIic = NO;
      } else if(FlagCharger) {
         Key = K_CHARGER;
         FlagCharger = NO;
      } else if(FlagButton) {
         Key = K_BUTTON;
         FlagButton = NO;
      } else if(FlagTimerFast) {
         Key = K_TIMER_FAST;
         FlagTimerFast = NO;
      } else if(FlagTimerSlow) {
         Key = K_TIMER_SLOW;
         FlagTimerSlow = NO;
      } else if(FlagRtc) {
         Key = K_TIMER_RTC;
         FlagRtc = NO;
      } else {
         if(!IicSlaveBusy() && !ButtonPushed() && Bq2425xPg()) { // go to sleep
            SleepEnable( YES);
         }
         InterruptEnable();
         SleepScheduler();
      }
      InterruptEnable();
   }
   return Key;
}

RtcHandler() {
   FlagRtc = YES;
   SleepEnable( NO);
}

void SysTimerExecute( void) {
static word Timer = 0;
   FlagTimerFast = YES;
   if(!Timer--) {
      Timer = TIMER_SLOW_PERIOD / TIMER_PERIOD;
      FlagTimerSlow = YES;
   }
   
   SleepEnable( NO);
}

void ChargerInputEvent( void) {
   FlagCharger = YES;
   SleepEnable( NO);
};

ButtonHandler() {
   FlagButton = YES;
   SleepEnable( NO);
}

void IicSlaveEvent( void) {
   FlagIic = YES;
   SleepEnable( NO);
}
