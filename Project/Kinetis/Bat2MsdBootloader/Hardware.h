//*****************************************************************************
//
//    Hardware.h   Bat2 bootloader definitions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Bat2Hardware.h"

#undef PRINTF_STRING

//------------------------------------------------------------------------------
//   System parameters
//------------------------------------------------------------------------------

//#define WATCHDOG_PERIOD   2500         // Watchdog period [ms]
#define TIMER_PERIOD    1

//------------------------------------------------------------------------------
//   Clock
//------------------------------------------------------------------------------

#define F_CPU     48000000ull
#define F_SYSTEM  48000000ull
#define F_BUS     48000000ull

//------------------------------------------------------------------------------
//   USB
//------------------------------------------------------------------------------

#define USB_INTERRUPT_NUMBER     USB0_IRQn
#define UsbIsrHost               USB0_IRQHandler

//------------------------------------------------------------------------------
//   Bootloader
//------------------------------------------------------------------------------

#include "Device/VersionDef.h"

#define DEVICE_CLASS          DEVICE_SCALE_COMPACT
#define DEVICE_MODIFICATION   DEVICE_MODIFICATION_STANDARD
#define VERSION_SW            0x0100
#define VERSION_HW            BAT2_HW_VERSION
#define SERIAL_NUMBER         0x00

#endif