#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "System/Delay.h"

void main(void)
{
   CpuInit();
   SysInit();
   BacklightInit();
   
   forever {
      for(int i = BACKLIGHT_INTENSITY_MIN ; i < BACKLIGHT_INTENSITY_MAX ; i++) {
         BacklightTest( i);
         SysDelay(200);
      }
      for(int i = BACKLIGHT_INTENSITY_MAX ; i > BACKLIGHT_INTENSITY_MIN ; i--) {
         BacklightTest( i);
         SysDelay(200);
      }
   }
}

void SysTimerExecute( void) {}