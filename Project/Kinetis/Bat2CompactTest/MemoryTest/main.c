#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"

void main(void)
{
   CpuInit();
   SysInit();
   BacklightInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);

   forever {
      
   }
}

void SysTimerExecute( void) {}