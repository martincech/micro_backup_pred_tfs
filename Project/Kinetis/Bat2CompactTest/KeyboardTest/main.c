#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"
#include "Kbd/Kbd.h"

void main(void)
{
int Key;
   CpuInit();
   SysInit();
   KbdInit();
   BacklightInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);

   forever {
      Key = SysSchedulerDefault();
      GTextAt(0, 0);
      switch(Key) {
         case K_ESC:
            cputs("ESC");
            break;

         case K_ESC | K_REPEAT:
            cputs("ESC repeat");
            break;

         case K_UP:
            cputs("UP");
            break;

         case K_UP | K_REPEAT:
            cputs("UP repeat");
            break;

         case K_ENTER:
            cputs("ENTER");
            break;

         case K_ENTER | K_REPEAT:
            cputs("ENTER repeat");
            break;

         case K_LEFT:
            cputs("LEFT");
            break;

         case K_LEFT | K_REPEAT:
            cputs("LEFT repeat");
            break;

         case K_DOWN:
            cputs("DOWN");
            break;

         case K_DOWN | K_REPEAT:
            cputs("DOWN repeat");
            break;

         case K_RIGHT:
            cputs("RIGHT");
            break;

         case K_RIGHT | K_REPEAT:
            cputs("RIGHT repeat");
            break;
      }

      if(Key & K_RELEASED) {
         GClear();
      }

      GFlush();
   }
}

void SysTimerExecute( void) {}