//*****************************************************************************
//
//    Fonts.h      project fonts
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Fonts_H__
   #define __Fonts_H__

#ifndef __Font_H__   
   #include "Font/Font.h"
#endif   

// Fonts enumeration :
typedef enum {
   TAHOMA16,
   TAHOMA16J,
   ARIAL_BOLD14,
   ARIAL_BOLD18,
   ARIAL_BOLD21,
   ARIAL_BOLD27,
   ARIAL_BOLD45,
   ARIAL_BOLD55,
   ARIAL_BOLD90,
   _FONT_LAST
} EProjectFonts;

// Font descriptor :

extern TFontDescriptor const *Fonts[];

void SetFont( int FontNumber);
// Set font

#endif
