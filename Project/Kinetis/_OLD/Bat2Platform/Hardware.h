//*****************************************************************************
//
//    Hardware.h   Bat2 wired platform hardware descriptions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Cpu/uMKL02Z32VFM4.h"
#include "Cpu/Io.h"
#include "Unisys/Uni.h"


#define OPTION_SIGMA_DELTA

//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

#define F_CPU      21000000L                      // Main clock by synthesizer [Hz]
#define F_SYSTEM   21000000L
#define F_BUS      21000000L

//-----------------------------------------------------------------------------
// System timer
//-----------------------------------------------------------------------------

#define TIMER_WAKE_UP_PERIOD  1000
#define TIMER_PERIOD      1                      // System timer period [ms]
#define TIMER_FAST_PERIOD 50                     // System timer 'fast' tick period [ms]
#define TIMER_SLOW_PERIOD 500                    // System timer 'slow' tick period [ms]
#define TIMER_FLASH1      300                    // Flash 1 delay [ms]
#define TIMER_FLASH2      200                    // Flash 2 delay [ms]
#define TIMER_TIMEOUT     10                     // Inactivity timeout [s]

//-----------------------------------------------------------------------------
// System
//-----------------------------------------------------------------------------

#define SYSTEM_CLOCK
#define SLEEP_ENABLE
//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define STATUS_LEDR   KINETIS_PIN_PORTB06
#define STATUS_LEDG   KINETIS_PIN_PORTB07
#define STATUS_LEDB   KINETIS_PIN_PORTB10


#define StatusLedInit()    PinFunction(STATUS_LEDR, KINETIS_GPIO_FUNCTION); \
                           PinFunction(STATUS_LEDG, KINETIS_GPIO_FUNCTION); \
                           PinFunction(STATUS_LEDB, KINETIS_GPIO_FUNCTION); \
                           GpioOutput(STATUS_LEDR); \
                           GpioOutput(STATUS_LEDG); \
                           GpioOutput(STATUS_LEDB); \
                           StatusLedsOff()

#define StatusLedsOff()   StatusLedROff(); StatusLedGOff(); StatusLedBOff();

#define StatusLedROn()     GpioClr(STATUS_LEDR)
#define StatusLedROff()    GpioSet(STATUS_LEDR)
#define StatusLedRToggle() GpioToggle(STATUS_LEDR)

#define StatusLedGOn()    GpioClr(STATUS_LEDG)
#define StatusLedGOff()   GpioSet(STATUS_LEDG)
#define StatusLedGToggle() GpioToggle(STATUS_LEDG)

#define StatusLedBOn()    GpioClr(STATUS_LEDB)
#define StatusLedBOff()   GpioSet(STATUS_LEDB)
#define StatusLedBToggle() GpioToggle(STATUS_LEDB)

//-----------------------------------------------------------------------------
// Bargraph
//-----------------------------------------------------------------------------

#define BAR_LED0
#define BAR_LED1
#define BAR_LED2
#define BAR_LED3
#define BAR_LED4

#define BarInit()   

#define BarOff()    

#define Bar0On()
#define Bar0Off()
#define Bar0Toggle()
#define Bar1On()
#define Bar1Off()
#define Bar1Toggle()
#define Bar2On()
#define Bar2Off()
#define Bar2Toggle()
#define Bar3On()
#define Bar3Off()
#define Bar3Toggle()
#define Bar4On()
#define Bar4Off()
#define Bar4Toggle()

//-----------------------------------------------------------------------------
// UART common
//-----------------------------------------------------------------------------

#define UART_PORTS_COUNT   1

#define UART0_PRIORITY       1         // UART0 IRQ

// conditional compilation :
#define UART_NATIVE  1                      // enable native mode

#define _Usart0NativeHandler     UART0_IRQHandler

#define RCS_CHANNEL                 UART_UART0
#define RCS_TIMEOUT                 UART_TIMEOUT_OFF
#define RCS_INTERCHARACTER_TIMEOUT  10
#define RCS_SEND_DELAY              0
#define RCS_BAUD_RATE               19200
#define RCS_FORMAT                  UART_8BIT

//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define UART0_RTS_PIN         KINETIS_PIN_PORTB05

#define Uart0PortInit()       PinFunction( KINETIS_PIN_UART0_TX_PORTB02, KINETIS_PIN_UART0_TX_PORTB02_FUNCTION); \
                              PinFunction( KINETIS_PIN_UART0_RX_PORTB01, KINETIS_PIN_UART0_RX_PORTB01_FUNCTION); \
                              PinPullup(KINETIS_PIN_UART0_RX_PORTB01); \
                              PinFunction( UART0_RTS_PIN, KINETIS_GPIO_FUNCTION); \
                              GpioOutput( UART0_RTS_PIN); \
                              GpioClr( UART0_RTS_PIN)

#define Uart0PortDeinit()     PinFunction( KINETIS_PIN_UART0_TX_PORTB02, KINETIS_GPIO_FUNCTION); \
                              PinFunction( KINETIS_PIN_UART0_RX_PORTB01, KINETIS_GPIO_FUNCTION)

#define UART0_BAUD           9600           // baud rate
#define UART0_FORMAT         UART_8BIT      // default format
#define UART0_TIMEOUT        10             // intercharacter timeout [ms]

#define UART0_TX_ENABLE_REGISTER   &GpioRegisterSet( UART0_RTS_PIN)
#define UART0_TX_DISABLE_REGISTER  &GpioRegisterClr( UART0_RTS_PIN)
#define UART0_TX_ENABLE_MASK       GpioMask( UART0_RTS_PIN)

//-----------------------------------------------------------------------------
// Buttons
//-----------------------------------------------------------------------------

#define BUTTON_RED
#define BUTTON_BLACK

#define ButtonInit()

#define ButtonRedPressed()       0
#define ButtonBlackPressed()     0

//-----------------------------------------------------------------------------
// Ads1232
//-----------------------------------------------------------------------------

#define ADC_INTERRUPT_HANDLER       PORTA_IRQHandler
#define ADC_INTERRUPT_NUMBER        PORTA_IRQn
#define ADC_INTERRUPT_PRIORITY      5

#define AdcInterruptHandler()       void __irq ADC_INTERRUPT_HANDLER( void)
#define AdcInitInt()                CpuIrqAttach(ADC_INTERRUPT_NUMBER, ADC_INTERRUPT_PRIORITY, ADC_INTERRUPT_HANDLER); \
                                    CpuIrqEnable(ADC_INTERRUPT_NUMBER)
#define AdcClearInt()               EintClearFlag( ADC_DOUT_RDY_PIN)
#define AdcEnableInt()              EintEnable( ADC_DOUT_RDY_PIN, EINT_SENSE_FALLING_EDGE)
#define AdcDisableInt()             EintDisable( ADC_DOUT_RDY_PIN)

#define ADC_SCLK_PIN       KINETIS_PIN_PORTA12
#define ADC_DOUT_RDY_PIN   KINETIS_PIN_PORTA13
#define ADC_POWERDOWN_PIN  KINETIS_PIN_PORTA05

#define AdcPortInit()      /*PinFunction(ADC_SCLK_PIN, KINETIS_GPIO_FUNCTION); \
                           PinFunction(ADC_DOUT_RDY_PIN, KINETIS_GPIO_FUNCTION); \
                           PinFunction(ADC_POWERDOWN_PIN, KINETIS_GPIO_FUNCTION); \
                           GpioOutput(ADC_SCLK_PIN); \
                           GpioOutput(ADC_POWERDOWN_PIN); \
                           GpioInput(ADC_DOUT_RDY_PIN);*/

#define AdcSetSCLK()     //GpioSet(ADC_SCLK_PIN)
#define AdcClrSCLK()     //GpioClr(ADC_SCLK_PIN)

#define AdcPowerDownDisable()     //GpioSet(ADC_POWERDOWN_PIN)
#define AdcPowerDownEnable()     //GpioClr(ADC_POWERDOWN_PIN)

#define AdcGetDOUT()     0//GpioGet(ADC_DOUT_RDY_PIN)
#define AdcGetRDY()      1//!GpioGet(ADC_DOUT_RDY_PIN)

//-----------------------------------------------------------------------------
// ADC filtering
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  50       // max. width of averaging window
#define FILTER_SLOW_RESTART    1       // slow stabilisation after restart

// Basic data types :
typedef byte TSamplesCount;            // samples counter

//------------------------------------------------------------------------------
// PWM
//------------------------------------------------------------------------------

#define PwmPortInit()
#define PwmOn()         StatusLedBOn()
#define PwmOff()        StatusLedBOff()

//-----------------------------------------------------------------------------
// Keyboard / Events
//-----------------------------------------------------------------------------

#define _K_SYSTEM_BASE   0

#include "System/SystemKey.h"

#define _K_KEYBOARD_BASE      _K_SYSTEM_LAST

#include "Menu/KbxKey.h"

// timing constants [ms] :
#define KBD_DEBOUNCE           50      // delay after first touch
#define KBD_AUTOREPEAT_START   300     // autorepeat delay
#define KBD_AUTOREPEAT_SPEED   200     // autorepeat repeat speed

#define SYSTEM_KEYBOARD        1       // service keyboard with default scheduler

//-----------------------------------------------------------------------------
// Display
//-----------------------------------------------------------------------------

// chicken icon :
#define DisplayWeighingOn()       Bar0On()
#define DisplayWeighingOff()      Bar0Off()

// accumulator icon :
#define DisplayChargerOn()        Bar1On()
#define DisplayChargerOff()       Bar1Off()

// antenna icon :
#define DisplaySignalOn()         Bar2On()
#define DisplaySignalOff()        Bar2Off()

// load icon :
#define DisplayCalibrationOn()    Bar3On()
#define DisplayCalibrationOff()   Bar3Off()

// all icon off :
#define DisplayModeOff()          BarOff()

// red LED :
#define DisplayErrorOn()          StatusLedROn()
#define DisplayErrorOff()         StatusLedROff()

// green LED :
#define DisplayOkOn()             StatusLedGOn()
#define DisplayOkOff()            StatusLedGOff()

// blue LED :
#define DisplayCommunicationOn()  StatusLedBOn()
#define DisplayCommunicationOff() StatusLedBOff() 

#define DisplayStatusOff()        StatusLedsOff()

//-----------------------------------------------------------------------------
// Control
//-----------------------------------------------------------------------------

// mode button :
#define K_MODE          K_KEY_BLACK

// OK button :
#define K_OK            K_KEY_RED

// matching button (power up) :
#define K_MATCHING      K_KEY_RED

// illumination on :
#define K_ILLUMINATION (K_KEY_BLACK | K_REPEAT)

//-----------------------------------------------------------------------------
// Samples FIFO
//-----------------------------------------------------------------------------

#define FIFO_SIZE       128            // samples FIFO capacity

//-----------------------------------------------------------------------------
// Diagnostic
//-----------------------------------------------------------------------------

#define DIAGNOSTIC_FRAME_COUNT  10    // Frame FIFO capacity

//------------------------------------------------------------------------------
//  Power control
//------------------------------------------------------------------------------

#define PowerAdcOn()
#define PowerAdcOff()

//------------------------------------------------------------------------------
//  Eeprom
//------------------------------------------------------------------------------

#define EEPROM_SIZE          (63)
#define EEPROM_FLASH_SIZE    (3 * 2048)


#endif
