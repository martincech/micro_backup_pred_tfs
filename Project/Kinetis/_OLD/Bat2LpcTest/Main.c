//*****************************************************************************
//
//    main.c       Main
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "System/System.h"
#include "Sleep/Sleep.h"

volatile byte a = 0;
void main( void) {
   CpuInit();
   SysInit();
   UartInit( CONSOLE_CHANNEL);
   UartSetup( CONSOLE_CHANNEL, CONSOLE_BAUD, UART_8BIT);

   LedOrangeInit();
   LedYellowInit();
   LedBlueInit();
   LedGreenInit();

   LedOrangeOn();
   LedYellowOn();
   LedGreenOn();
   LedBlueOn();
   SysDelay(1000);

   LedOrangeOff();
   LedYellowOff();
   LedGreenOff();
   LedBlueOff();

   PinInit();
   ButtonInit();
   SleepEnable(YES);

   printf("START\r\n");

   forever {
      switch( SysEventWait()){
         case K_TIMER_SLOW :
            break;

         default :
            break;
      }
   }
}

//******************************************************************************
// Scheduler
//******************************************************************************

int SysScheduler( void)
// Scheduler cycle, returns any event
{
int Key;

   Key = SysSchedulerDefault();
   switch( Key){
      case K_SLEEP_WAKE_BUTTON:
         SleepEnable(NO);
         break;

      case K_SLEEP_WAKE_TIMER:
         //LedBlueToggle();
         break;

      case K_TIMEOUT:
         SleepEnable(YES);
         break;

      case K_TIMER_FAST :
         break;

      case K_TIMER_SLOW :
         break;

      case K_FLASH1 :
         break;

      case K_IDLE :
         break;

      default :
         break;
   }
   return( Key);
} // SysScheduler

void SysTimerExecute( void) {
}

PinInterruptHandler() {
   //LedOrangeOff();
   //LedOrangeOn();
   PinIntClear();
}