#-------------------------------------------------
#
#  Bat2Compact project
#
#-------------------------------------------------

QT       += core gui network

TARGET    = Bat2CompactEth

#CONFIG   += console
CONFIG   += warn_off
QMAKE_CFLAGS_DEBUG     += -Wall
QMAKE_CFLAGS_RELEASE   += -Wall
QMAKE_CXXFLAGS_DEBUG   += -Wall
QMAKE_CXXFLAGS_RELEASE += -Wall
TEMPLATE  = app

RC_FILE   = Bat2Compact.rc

INCLUDEPATH  = ../../../Library/Bat2/Compact
INCLUDEPATH += ../../../Library/Qt
INCLUDEPATH += ../../../Library/Qt/Bat2Platform
INCLUDEPATH += ../../../Library/Bat2
INCLUDEPATH += ../../../Library/Bat2Platform/Compact
INCLUDEPATH += ../../../Library/Bat2Platform
INCLUDEPATH += ../../../Library/Bat
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Win32
INCLUDEPATH += ../../../Library/VisualC/Bat2

DEFINES += __UDEBUG__
DEFINES += OPTION_BAT2_COMPACT
DEFINES += OPTION_REMOTE
DEFINES += OPTION_SIMULATION
DEFINES += OPTION_SIGMA_DELTA
#DEFINES += OPTION_PICOSTRAIN
#DEFINES += __MENU_WEIGHING__

SOURCES += main.cpp \
    ../../../Library/Bat2Platform/Platform/Acceptance.c \
    ../../../Library/Bat2Platform/Fifo/Fifo.c \
    ../../../Library/Bat/Filter/FilterRelative.c \
    ../../../Library/Bat2/Compact/Config/Config.c \
    ../../../Library/Bat2Platform/Compact/Platform/Platform.c \
    ../../../Library/Bat2Platform/Device/Status.c \
    ../../../Library/Bat2/Compact/Menu/MenuMaintenanceCalibration.c \
    ../../../Library/Qt/Bat2Platform/Samples.cpp \
    ../../../Library/Bat2Platform/Platform/Wepl.c \
    ../../../Library/Bat2/Weighing/Weighing.c \
    ../../../Library/Qt/Bat2Platform/pWeighing.c \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.cpp \
    ../../../Library/Bat2Platform/Calibration/Sigma/Calibration.c \
    ../../../Library/Device/Ad7192/Ad7192fDummy.c \
    ../../../Library/Bat2/Diagnostic/DiagnosticFifo.c \
    ../../../Library/Bat2/Diagnostic/Diagnostic.c \
    ../../../Library/Bat2/Dummy/Usb.c \
    ../../../Library/Qt/uSimulator/Power.cpp \
    ../../../Library/Bat2/Log/Log.c \
    ../../../Library/Bat2/Menu/MenuDateTime.c \
    ../../../Library/Bat2/Dummy/Sleep.c \
    ../../../Library/Bat2/Dummy/Multitasking.cpp \
    ../../../Library/Bat2/Dummy/Accu.c \
    ../../../Library/Bat2/Dummy/MenuPower.c \
    ../../../Library/Bat/Gadget/DGraph.c \
    ../../../Library/Bat2/Graph/MenuGraph.c \
    ../../../Library/Bat2/Megavi/MegaviTask.c \
    ../../../Library/Bat2/Megavi/MessageMegavi.c \
    ../../../Library/Bat2/Megavi/MegaviConfig.c \
    ../../../Library/Qt/Socket/TcpRawClient.cpp \
    ../../../Library/Qt/Socket/tcpclient.cpp \
    ../../../Library/Bat2/Action/ActionRemote.cpp \
    ../../../Library/Bat2/Config/Context.c \
    ../../../Library/Device/Rs485/Rs485Config.c \
    ../../../Library/Bat2/Modbus/ModbusWeighingGroup.c \
    ../../../Library/Bat2/Modbus/ModbusTask.c \
    ../../../Library/Bat2/Modbus/ModbusStatisticsGroup.c \
    ../../../Library/Bat2/Modbus/ModbusRtu.c \
    ../../../Library/Bat2/Modbus/ModbusRegRangeCheck.c \
    ../../../Library/Bat2/Modbus/ModbusReg.c \
    ../../../Library/Bat2/Modbus/ModbusPredefinedWeighingsGroup.c \
    ../../../Library/Bat2/Modbus/ModbusPredefinedWeighingPlansGroup.c \
    ../../../Library/Bat2/Modbus/ModbusPredefinedGrowthCurvesGroup.c \
    ../../../Library/Bat2/Modbus/ModbusPredefinedCorrectionCurvesGroup.c \
    ../../../Library/Bat2/Modbus/ModbusInputRegisters.c \
    ../../../Library/Bat2/Modbus/ModbusHoldingRegisters.c \
    ../../../Library/Bat2/Modbus/ModbusGsmContactsGroup.c \
    ../../../Library/Bat2/Modbus/ModbusDiscreteInputs.c \
    ../../../Library/Bat2/Modbus/ModbusConfigurationGroup.c \
    ../../../Library/Bat2/Modbus/ModbusConfig.c \
    ../../../Library/Bat2/Modbus/ModbusCoils.c \
    ../../../Library/Bat2/Modbus/ModbusCharacterBufferGroup.c \
    ../../../Library/Bat2/Modbus/ModbusCalibrationGroup.c \
    ../../../Library/Bat2/Modbus/ModbusAscii.c \
    ../../../Library/Bat2/Ethernet/MenuEthernetOptions.c \
    ../../../Library/Bat2/Ethernet/MenuEthernetInfo.c \
    ../../../Library/Bat2/Ethernet/MenuEthernet.c \
    ../../../Library/Bat2/Ethernet/EthernetConfig.c \
    ../../../Library/Qt/System/SysClock.c \
    ../../../Library/Bat/Menu/MenuExit.c \
    ../../../Library/Bat2/Dummy/Megavi.cpp \
    ../../../Library/Bat2/Dummy/EthernetLibrary.cpp \
    ../../../Library/Bat2/Dummy/Modbus.cpp \
    ../../../Library/Bat2/Scheduler/WeighingPlanList.c \
    ../../../Library/Bat2/Scheduler/WeighingPlan.c \
    ../../../Library/Qt/Modbus/TimerSlot.cpp \
    ../../../Library/Qt/Bat2/Compact/Tasks.c \
    ../../../Library/Bat2/Menu/MenuRs485Interface.c \
    ../../../Library/Bat2/Menu/MenuRs485.c \
    ../../../Library/Bat2/Message/MGsm.c \
    ../../../Library/Bat2/Scheduler/MenuWeighingPlans.c \
    ../../../Library/Qt/Socket/SocketParameters.cpp \
    MainClass.cpp \
    ../../../Library/Device/Rs485/Rs485.c \
    ../../../Library/Device/Remote/Frame.c \
    ../../../Library/Bat2/SmsGate/SmsGate.c \
    ../../../Library/Bat2/SmsGate/SmsGateConfig.c \
    ../../../Library/Micro/Crc/Crc.c \
    ../../../Library/Bat2/Dacs/DacsTask.c \
    ../../../Library/Bat2/Dacs/DacsConfig.c \
    ../../../Library/Bat2/Dummy/Dacs.cpp \
    ../../../Library/Bat2/Modbus/ModbusSmsGateGroup.c \
    ../../../Library/Bat2/Dummy/SocketIfUsb.c \
    ../../../Library/Bat2/Dummy/SocketIfUart.c \
    ../../../Library/Bat2/Dummy/SocketIfEthernet.c \
    ../../../Library/Qt/uSimulator/Nvm.cpp \
    ../../../Library/Device/Memory/File.c \
    ../../../Library/Bat2/Scheduler/WeighingSchedulerRemote.c \
    ../../../Library/Bat2/Usb/MenuUsbDummy.c \
    ../../../Library/Qt/Socket/tcpserver.cpp \
    ../../../Library/Bat2/Menu/MenuWeighingTargetAdjust.c \
    ../../../Library/Device/Memory/FileNvm.c \
    ../../../Library/Bat2/Message/MenuGsmRemoteControl.c \
    ../../../Library/Bat2/Message/MenuGsmPowerOptionsTimes.c \
    ../../../Library/Bat/Gadget/DTimeRange.c \
    ../../../Library/Bat2/Remote/Cmd.c \
    ../../../Library/Qt/Remote/Socket.cpp \
    ../../../Library/Qt/Remote/SocketIfEthernetQ.cpp \
    ../../../Library/Bat2/Config/State.c \
    ../../../Library/Device/Memory/Ram.c \
    ../../../Library/Bat2/Platform/ZoneConfig.c \
    ../../../Library/Bat2/Memory/FileDef.c \
    ../../../Library/Bat2/Menu/MenuConfig.c \
    ../../../Library/Device/Memory/FileRemote.c \
    ../../../Library/Bat2/Compact/Sys.c \
    ../../../Library/Bat2/Compact/Gui.c

SOURCES += ../../../Library/Bat2/Compact/AppMain.c
SOURCES += ../../../Library/Bat2/Compact/Str.c
SOURCES += ../../../Library/Bat2/Compact/Bitmap.c
SOURCES += ../../../Library/Bat2/Compact/Fonts.c
SOURCES +=
#SOURCES += ../../../Library/Bat2/Dummy/Power.c
SOURCES += ../../../Library/Bat2/Archive/Archive.c
SOURCES += ../../../Library/Bat2/Archive/MenuArchive.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Curve/GrowthCurve.c
SOURCES += ../../../Library/Bat2/Curve/CurveList.c
SOURCES += ../../../Library/Bat2/Curve/MenuGrowthCurve.c
SOURCES += ../../../Library/Bat2/Curve/CorrectionCurve.c
SOURCES += ../../../Library/Bat2/Curve/CorrectionList.c
SOURCES += ../../../Library/Bat2/Curve/MenuCorrectionCurve.c
SOURCES += ../../../Library/Bat2/Display/DisplayConfiguration.c
SOURCES += ../../../Library/Bat2/Display/MenuContrast.c
SOURCES += ../../../Library/Bat2/Display/MenuBacklightIntensity.c
SOURCES += ../../../Library/Bat2/Display/MenuDisplay.c
SOURCES += ../../../Library/Bat2/Display/MenuBacklight.c
SOURCES += ../../../Library/Bat2/Menu/Menu.c
SOURCES += ../../../Library/Bat2/Menu/MenuMain.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighing.c
SOURCES += ../../../Library/Bat2/Menu/MenuStatistics.c
SOURCES += ../../../Library/Bat2/Menu/MenuUserSettings.c
SOURCES += ../../../Library/Bat2/Menu/MenuConfiguration.c
SOURCES += ../../../Library/Bat2/Menu/MenuMaintenance.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingStart.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingStartLater.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingStop.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingSuspend.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingRelease.c
SOURCES += ../../../Library/Bat2/Menu/MenuConfigurationWeighing.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingConfiguration.c
SOURCES += ../../../Library/Bat2/Menu/MenuInitialWeights.c
SOURCES += ../../../Library/Bat2/Menu/MenuGrowthCurves.c
SOURCES += ../../../Library/Bat2/Menu/MenuStandardCurves.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingMenuSelection.c
SOURCES += ../../../Library/Bat2/Menu/MenuTargetWeights.c
SOURCES += ../../../Library/Bat2/Menu/MenuDetection.c
SOURCES += ../../../Library/Bat2/Menu/MenuAcceptance.c
SOURCES += ../../../Library/Bat2/Menu/MenuAcceptanceData.c
SOURCES += ../../../Library/Bat2/Menu/MenuPlatformInfo.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Menu/MenuMaintenanceSimulation.c
SOURCES += ../../../Library/Bat2/Menu/Password.c
SOURCES += ../../../Library/Bat2/Menu/Screen.c
SOURCES += ../../../Library/Bat2/Menu/ScreenStatistic.c
SOURCES += ../../../Library/Bat2/Message/MenuGsm.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmContacts.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmContact.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmStatistics.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmCommands.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmEvents.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmPowerOptions.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmInfo.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmPinCode.c
SOURCES += ../../../Library/Bat2/Message/GsmMessage.c
SOURCES += ../../../Library/Bat2/Message/ContactList.c
SOURCES += ../../../Library/Bat2/Message/Message.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Platform/Zone.c
SOURCES += ../../../Library/Bat2/Predefined/PredefinedList.c
SOURCES += ../../../Library/Bat2/Predefined/MenuPredefinedWeighing.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Scheduler/MenuWeighingPlan.c
SOURCES += ../../../Library/Bat2/Scheduler/MenuWeighingDays.c
SOURCES += ../../../Library/Bat2/Scheduler/MenuWeighingTimes.c
SOURCES += ../../../Library/Bat2/Scheduler/MenuWeekDays.c
SOURCES += ../../../Library/Bat2/Statistic/Calculate.c
SOURCES += ../../../Library/Bat2/Statistic/DStatistic.c
SOURCES += ../../../Library/Bat2/Statistic/MenuStatisticParameters.c
SOURCES += ../../../Library/Bat2/Storage/Sample.c
SOURCES += ../../../Library/Bat2/Storage/MenuSample.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Weighing/WeighingConfiguration.c
SOURCES += ../../../Library/Bat2/Weighing/Statistics.c
SOURCES += ../../../Library/Bat2/Weighing/Prediction.c
SOURCES += ../../../Library/Bat2/Weighing/Memory.c
SOURCES += ../../../Library/Bat2/Weighing/Curve.c
SOURCES += ../../../Library/Bat2/Weighing/Correction.c
SOURCES += ../../../Library/Bat2/Dummy/Contrast.c
SOURCES += ../../../Library/Bat2/Dummy/Backlight.c
SOURCES += ../../../Library/Bat2/Dummy/Beep.c
SOURCES += ../../../Library/Bat2/Dummy/Gsm.cpp
SOURCES += ../../../Library/Bat2/Dummy/Iadc.c
SOURCES += ../../../Library/Bat/Console/conio.c
SOURCES += ../../../Library/Bat/Console/xprint.c
SOURCES += ../../../Library/Bat/Console/xprintf.c
SOURCES += ../../../Library/Bat/Console/sputchar.c
SOURCES += ../../../Library/Bat/Country/Country.c
SOURCES += ../../../Library/Bat/Country/xTime.c
SOURCES += ../../../Library/Bat/Country/bTime.c
SOURCES += ../../../Library/Bat/Country/MenuCountry.c
SOURCES += ../../../Library/Bat/Graphic/Graphic.c
SOURCES += ../../../Library/Bat/Graphic/Text.c
SOURCES += ../../../Library/Bat/Gadget/DCursor.c
SOURCES += ../../../Library/Bat/Gadget/DList.c
SOURCES += ../../../Library/Bat/Gadget/DDirectory.c
SOURCES += ../../../Library/Bat/Gadget/DNamedList.c
SOURCES += ../../../Library/Bat/Gadget/DMsg.c
SOURCES += ../../../Library/Bat/Gadget/DMenu.c
SOURCES += ../../../Library/Bat/Gadget/DCheckList.c
SOURCES += ../../../Library/Bat/Gadget/DLayout.c
SOURCES += ../../../Library/Bat/Gadget/DLabel.c
SOURCES += ../../../Library/Bat/Gadget/DInputText.c
SOURCES += ../../../Library/Bat/Gadget/DInput.c
SOURCES += ../../../Library/Bat/Gadget/DFormat.c
SOURCES += ../../../Library/Bat/Gadget/DEvent.c
SOURCES += ../../../Library/Bat/Gadget/DEnterText.c
SOURCES += ../../../Library/Bat/Gadget/DEnterList.c
SOURCES += ../../../Library/Bat/Gadget/DEnter.c
SOURCES += ../../../Library/Bat/Gadget/DEdit.c
SOURCES += ../../../Library/Bat/Gadget/DEnterDate.c
SOURCES += ../../../Library/Bat/Gadget/DEnterTime.c
SOURCES += ../../../Library/Bat/Gadget/DProgress.c
SOURCES += ../../../Library/Bat/Gadget/DTime.c
SOURCES += ../../../Library/Bat/Statistic/Statistic.c
SOURCES += ../../../Library/Bat/Statistic/Histogram.c
SOURCES += ../../../Library/Bat/Statistic/DHistogram.c
SOURCES += ../../../Library/Bat/String/StrUtil.c
SOURCES += ../../../Library/Bat/Weight/Weight.c
SOURCES += ../../../Library/Bat/Weight/xWeight.c
SOURCES += ../../../Library/Bat/Weight/DWeight.c
SOURCES += ../../../Library/Bat/Weight/MenuWeightUnits.c
SOURCES += ../../../Library/Micro/Convert/uBcd.c
SOURCES += ../../../Library/Micro/Data/uStorage.c
SOURCES += ../../../Library/Micro/Data/uFifo.c
SOURCES += ../../../Library/Micro/Data/uDirectory.c
SOURCES += ../../../Library/Micro/Data/uNamedList.c
SOURCES += ../../../Library/Micro/Data/uList.c
SOURCES += ../../../Library/Micro/Time/uClock.c
SOURCES += ../../../Library/Micro/Time/uDate.c
SOURCES += ../../../Library/Micro/Time/uTime.c
SOURCES += ../../../Library/Micro/Time/uDateTime.c
SOURCES += ../../../Library/Micro/Math/uGeometry.c
SOURCES += ../../../Library/Micro/Parse/uParse.c
SOURCES += ../../../Library/Qt/uSimulator/mainwindow.cpp
SOURCES += ../../../Library/Qt/uSimulator/devicepanel.cpp
SOURCES += ../../../Library/Qt/uSimulator/lcddisplay.cpp
SOURCES +=
SOURCES += ../../../Library/Qt/uSimulator/uDebug.cpp
SOURCES += ../../../Library/Qt/Crt/crt.cpp
SOURCES += ../../../Library/Qt/Crt/crtdump.cpp
SOURCES += ../../../Library/Qt/File/Efs.cpp
SOURCES += ../../../Library/Win32/Utility/Performance.c
#SOURCES += ../../../Library/Win32/Uart/WinUart.cpp
#SOURCES += ../../../Library/Win32/Uart/UartModem.cpp
#SOURCES += ../../../Library/Device/Gsm/Gsm.c
#--- connectivity :
SOURCES +=

HEADERS += ../uBat2Compact/Hardware.h \
    ../../../Library/Bat2/Compact/Config/ConfigDef.h \
    ../../../Library/Bat2/Compact/Config/Config.h \
    Hardware.h \
    ../../../Library/Bat2Platform/Compact/Weighing/pWeighing.h \
    ../../../Library/Qt/Bat2Platform/Samples.h \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.h \
    ../../../Library/Bat/Gadget/DGraph.h \
    ../../../Library/Bat2/Graph/MenuGraph.h \
    ../../../Library/Bat2/Graph/GraphDef.h \
    ../../../Library/Bat2/Graph/Graph.h \
    ../../../Library/Bat2/Megavi/MegaviTask.h \
    ../../../Library/Device/Megavi/MegaviDef.h \
    ../../../Library/Device/Megavi/Megavi.h \
    ../../../Library/Bat2/Megavi/MessageMegavi.h \
    ../../../Library/Bat2/Megavi/MegaviConfig.h \
    ../../../Library/Bat2/Megavi/MegaviConfigDef.h \
    ../../../Library/Bat2/Multitasking/pt-sem.h \
    ../../../Library/Bat2/Multitasking/pt.h \
    ../../../Library/Bat2/Multitasking/lc-switch.h \
    ../../../Library/Bat2/Multitasking/lc.h \
    ../../../Library/Qt/Socket/TcpRawClient.h \
    ../../../Library/Qt/Socket/tcpclient.h \
    ../../../Library/Bat2/Action/ActionDef.h \
    ../../../Library/Bat2/Action/Action.h \
    ../../../Library/Qt/Bat2/Remote/Bat2RemoteInterface.h \
    ../../../Library/Bat2/Config/Context.h \
    ../../../Library/Device/Rs485/Rs485ConfigDef.h \
    ../../../Library/Device/Rs485/Rs485Config.h \
    ../../../Library/Bat2/Modbus/ModbusWeighingGroup.h \
    ../../../Library/Bat2/Modbus/ModbusTask.h \
    ../../../Library/Bat2/Modbus/ModbusStatisticsGroup.h \
    ../../../Library/Bat2/Modbus/ModbusRtu.h \
    ../../../Library/Bat2/Modbus/ModbusRegRangeCheck.h \
    ../../../Library/Bat2/Modbus/ModbusReg.h \
    ../../../Library/Bat2/Modbus/ModbusPredefinedWeighingsGroup.h \
    ../../../Library/Bat2/Modbus/ModbusPredefinedWeighingPlansGroup.h \
    ../../../Library/Bat2/Modbus/ModbusPredefinedGrowthCurvesGroup.h \
    ../../../Library/Bat2/Modbus/ModbusPredefinedCorrectionCurvesGroup.h \
    ../../../Library/Bat2/Modbus/ModbusInputRegisters.h \
    ../../../Library/Bat2/Modbus/ModbusHoldingRegisters.h \
    ../../../Library/Bat2/Modbus/ModbusGsmContactsGroup.h \
    ../../../Library/Bat2/Modbus/ModbusDiscreteInputs.h \
    ../../../Library/Bat2/Modbus/ModbusDef.h \
    ../../../Library/Bat2/Modbus/ModbusConfigurationGroup.h \
    ../../../Library/Bat2/Modbus/ModbusConfigDef.h \
    ../../../Library/Bat2/Modbus/ModbusConfig.h \
    ../../../Library/Bat2/Modbus/ModbusCoils.h \
    ../../../Library/Bat2/Modbus/ModbusCharacterBufferGroup.h \
    ../../../Library/Bat2/Modbus/ModbusCalibrationGroup.h \
    ../../../Library/Bat2/Modbus/ModbusAscii.h \
    ../../../Library/Bat2/Ethernet/MenuEthernetOptions.h \
    ../../../Library/Bat2/Ethernet/MenuEthernetInfo.h \
    ../../../Library/Bat2/Ethernet/MenuEthernet.h \
    ../../../Library/Bat2/Ethernet/EthernetLibrary.h \
    ../../../Library/Bat2/Ethernet/EthernetConfigDef.h \
    ../../../Library/Bat2/Ethernet/EthernetConfig.h \
    ../../../Library/Bat/Menu/MenuExit.h \
    ../../../Library/Bat2/Scheduler/WeighingPlanList.h \
    ../../../Library/Bat2/Scheduler/WeighingPlan.h \
    ../../../Library/Bat2/Scheduler/MenuWeighingTimes.h \
    ../../../Library/Bat2/Scheduler/MenuWeighingTime.h \
    ../../../Library/Bat2/Scheduler/MenuWeighingPlans.h \
    ../../../Library/Bat2/Scheduler/MenuWeighingPlan.h \
    ../../../Library/Qt/Modbus/TimerSlot.h \
    ../../../Library/Qt/Multitasking/MultitaskingDef.h \
    ../../../Library/Bat2/Menu/MenuRs485Interface.h \
    ../../../Library/Bat2/Menu/MenuRs485Def.h \
    ../../../Library/Bat2/Menu/MenuRs485.h \
    ../../../Library/Bat2/Scheduler/MenuWeighingTimes.h \
    ../../../Library/Bat2/Scheduler/MenuWeighingTime.h \
    ../../../Library/Bat2/Scheduler/WeighingPlanList.h \
    ../../../Library/Bat2/Scheduler/WeighingPlan.h \
    ../../../Library/Bat2/Scheduler/MenuWeighingTimes.h \
    ../../../Library/Bat2/Scheduler/MenuWeighingTime.h \
HEADERS += ../uBat2Compact/Storage.h \
    ../../../Library/Qt/Socket/SocketParameters.h \
    MainClass.h \
    ../../../Library/Device/Rs485/Rs485.h \
    ../../../Library/Device/Ethernet/fnet_user_config.h \
    ../../../Library/Bat2/Memory/FileDef.h \
    ../../../Library/Bat2/SmsGate/SmsGateDef.h \
    ../../../Library/Bat2/SmsGate/SmsGate.h \
    ../../../Library/Bat2/SmsGate/SmsGateConfigDef.h \
    ../../../Library/Bat2/SmsGate/SmsGateConfig.h \
    ../../../Library/Micro/Crc/Crc.h \
    ../../../Library/Bat2/Dacs/DacsTask.h \
    ../../../Library/Bat2/Dacs/DacsConfigDef.h \
    ../../../Library/Bat2/Dacs/DacsConfig.h \
    ../../../Library/Bat2/Dacs/Dacs5Addr.h \
    ../../../Library/Bat2/Modbus/ModbusSmsGateGroup.h \
    ../../../Library/Qt/Socket/tcpserver.h \
    ../../../Library/Bat2/Menu/MenuWeighingTargetAdjust.h \
    ../../../Library/Bat2/Message/MenuGsmRemoteControl.h \
    ../../../Library/Bat2/Message/MenuGsmPowerOptionsTimes.h \
    ../../../Library/Bat2/Remote/Cmd.h \
    ../../../Library/Qt/Remote/SocketIf.h \
    ../../../Library/Qt/Remote/SocketDef.h \
    ../../../Library/Qt/Remote/SocketIfEthernetQ.h

HEADERS += ../uBat2Compact/Persistent.h
HEADERS += ../uBat2Compact/Bitmap.h
HEADERS += ../uBat2Compact/Str.h
HEADERS += ../../../Library/Qt/uSimulator/mainwindow.h
HEADERS += ../../../Library/Qt/uSimulator/devicepanel.h
HEADERS += ../../../Library/Qt/uSimulator/lcddisplay.h
HEADERS += ../../../Library/Qt/Crt/crt.h
HEADERS += ../../../Library/Qt/Crt/crtdump.h
HEADERS += ../../../Library/Bat2/Archive/ArchiveDef.h
HEADERS += ../../../Library/Bat2/Archive/Archive.h
HEADERS += ../../../Library/Bat2/Archive/MenuArchive.h
HEADERS += ../../../Library/Bat2/Compact/Power.h
HEADERS += ../../../Library/Bat2/Config/Bat2Def.h
HEADERS += ../../../Library/Bat2/Config/ConfigDef.h
HEADERS += ../../../Library/Bat2/Config/Config.h
HEADERS += ../../../Library/Bat2/Config/DeviceDef.h
HEADERS += ../../../Library/Bat2/Curve/GrowthCurveDef.h
HEADERS += ../../../Library/Bat2/Curve/GrowthCurve.h
HEADERS += ../../../Library/Bat2/Curve/CurveList.h
HEADERS += ../../../Library/Bat2/Curve/MenuGrowthCurve.h
HEADERS += ../../../Library/Bat2/Curve/CorrectionCurveDef.h
HEADERS += ../../../Library/Bat2/Curve/CorrectionCurve.h
HEADERS += ../../../Library/Bat2/Curve/CorrectionList.h
HEADERS += ../../../Library/Bat2/Curve/MenuCorrectionCurve.h
HEADERS += ../../../Library/Bat2/Display/DisplayConfigurationDef.h
HEADERS += ../../../Library/Bat2/Display/DisplayConfiguration.h
HEADERS += ../../../Library/Bat2/Display/MenuBacklightIntensity.h
HEADERS += ../../../Library/Bat2/Display/MenuContrast.h
HEADERS += ../../../Library/Bat2/Display/MenuDisplay.h
HEADERS += ../../../Library/Bat2/Display/MenuBacklight.h
HEADERS += ../../../Library/Bat2/Memory/NvmLayout.h
HEADERS += ../../../Library/Bat2/Menu/Menu.h
HEADERS += ../../../Library/Bat2/Menu/MenuMain.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighing.h
HEADERS += ../../../Library/Bat2/Menu/MenuStatistics.h
HEADERS += ../../../Library/Bat2/Menu/MenuUserSettings.h
HEADERS += ../../../Library/Bat2/Menu/MenuConfiguration.h
HEADERS += ../../../Library/Bat2/Menu/MenuMaintenance.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingStart.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingStartLater.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingStop.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingSuspend.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingRelease.h
HEADERS += ../../../Library/Bat2/Menu/MenuConfigurationWeighing.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingConfiguration.h
HEADERS += ../../../Library/Bat2/Menu/MenuInitialWeights.h
HEADERS += ../../../Library/Bat2/Menu/MenuGrowthCurves.h
HEADERS += ../../../Library/Bat2/Menu/MenuStandardCurves.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingMenuSelection.h
HEADERS += ../../../Library/Bat2/Menu/MenuTargetWeights.h
HEADERS += ../../../Library/Bat2/Menu/MenuDetection.h
HEADERS += ../../../Library/Bat2/Menu/MenuAcceptance.h
HEADERS += ../../../Library/Bat2/Menu/MenuAcceptanceData.h
HEADERS += ../../../Library/Bat2/Menu/MenuPlatformInfo.h
HEADERS += ../../../Library/Bat2/Menu/MenuMaintenanceCalibration.h
HEADERS += ../../../Library/Bat2/Menu/MenuMaintenanceSimulation.h
HEADERS += ../../../Library/Bat2/Menu/Password.h
HEADERS += ../../../Library/Bat2/Menu/Screen.h
HEADERS += ../../../Library/Bat2/Menu/ScreenStatistic.h
HEADERS += ../../../Library/Bat2/Message/GsmMessageDef.h
HEADERS += ../../../Library/Bat2/Message/GsmMessage.h
HEADERS += ../../../Library/Bat2/Message/ContactList.h
HEADERS += ../../../Library/Bat2/Message/MenuGsm.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmContacts.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmContact.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmStatistics.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmCommands.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmPowerOptions.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmInfo.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmPinCode.h
HEADERS += ../../../Library/Bat2/Message/Message.h
HEADERS += ../../../Library/Bat2/Message/MGsm.h
HEADERS += ../../../Library/Bat2Platform/Platform/PlatformDef.h
HEADERS += ../../../Library/Bat2Platform/Platform/Wepl.h
HEADERS += ../../../Library/Bat2/Platform/Zone.h
HEADERS += ../../../Library/Bat2/Predefined/PredefinedWeighingDef.h
HEADERS += ../../../Library/Bat2/Predefined/PredefinedList.h
HEADERS += ../../../Library/Bat2/Predefined/MenuPredefinedWeighing.h
HEADERS += ../../../Library/Bat2/Scheduler/WeighingSchedulerDef.h
HEADERS += ../../../Library/Bat2/Scheduler/WeighingScheduler.h
HEADERS += ../../../Library/Bat2/Scheduler/WeighingPlan.h
HEADERS += ../../../Library/Bat2/Scheduler/MenuWeighingPlan.h
HEADERS += ../../../Library/Bat2/Scheduler/MenuWeighingDays.h
HEADERS += ../../../Library/Bat2/Scheduler/MenuWeighingTime.h
HEADERS += ../../../Library/Bat2/Scheduler/MenuWeighingTimes.h
HEADERS += ../../../Library/Bat2/Scheduler/MenuWeekDays.h
HEADERS += ../../../Library/Bat2/Statistic/StatisticDef.h
HEADERS += ../../../Library/Bat2/Statistic/Calculate.h
HEADERS += ../../../Library/Bat2/Statistic/DStatistic.h
HEADERS += ../../../Library/Bat2/Statistic/MenuStatisticParameters.h
HEADERS += ../../../Library/Bat2/Storage/Sample.h
HEADERS += ../../../Library/Bat2/Storage/SampleDef.h
HEADERS += ../../../Library/Bat2/Storage/MenuSample.h
HEADERS += ../../../Library/Bat2/Weighing/Weighing.h
HEADERS += ../../../Library/Bat2/Weighing/WeighingConfigurationDef.h
HEADERS += ../../../Library/Bat2/Weighing/WeighingConfiguration.h
HEADERS += ../../../Library/Bat2/Weighing/Statistics.h
HEADERS += ../../../Library/Bat2/Weighing/Prediction.h
HEADERS += ../../../Library/Bat2/Weighing/Memory.h
HEADERS += ../../../Library/Bat2/Weighing/Curve.h
HEADERS += ../../../Library/Bat2/Weighing/Correction.h
HEADERS += ../../../Library/Bat/Country/CountryDef.h
HEADERS += ../../../Library/Bat/Country/Country.h
HEADERS += ../../../Library/Bat/Country/CountrySet.h
HEADERS += ../../../Library/Bat/Country/xTime.h
HEADERS += ../../../Library/Bat/Country/bTime.h
HEADERS += ../../../Library/Bat/Country/MenuCountry.h
HEADERS += ../../../Library/Bat/Device/VersionDef.h
HEADERS += ../../../Library/Bat/Gadget/DCursor.h
HEADERS += ../../../Library/Bat/Gadget/DList.h
HEADERS += ../../../Library/Bat/Gadget/DDirectory.h
HEADERS += ../../../Library/Bat/Gadget/DNamedList.h
HEADERS += ../../../Library/Bat/Gadget/DMsg.h
HEADERS += ../../../Library/Bat/Gadget/DMenu.h
HEADERS += ../../../Library/Bat/Gadget/DCheckList.h
HEADERS += ../../../Library/Bat/Gadget/DLayout.h
HEADERS += ../../../Library/Bat/Gadget/DLabel.h
HEADERS += ../../../Library/Bat/Gadget/DInput.h
HEADERS += ../../../Library/Bat/Gadget/DFormat.h
HEADERS += ../../../Library/Bat/Gadget/DEvent.h
HEADERS += ../../../Library/Bat/Gadget/DEnterList.h
HEADERS += ../../../Library/Bat/Gadget/DEnter.h
HEADERS += ../../../Library/Bat/Gadget/DEdit.h
HEADERS += ../../../Library/Bat/Gadget/DProgress.h
HEADERS += ../../../Library/Bat/Gadget/DTime.h
HEADERS += ../../../Library/Bat/Sound/Beep.h
HEADERS += ../../../Library/Bat/Statistic/StatisticGaugeDef.h
HEADERS += ../../../Library/Bat/Statistic/HistogramDef.h
HEADERS += ../../../Library/Bat/Statistic/Statistic.h
HEADERS += ../../../Library/Bat/Statistic/Histogram.h
HEADERS += ../../../Library/Bat/Statistic/DHistogram.h
HEADERS += ../../../Library/Bat/System/System.h
HEADERS += ../../../Library/Bat/Weight/Weight.h
HEADERS += ../../../Library/Bat/Weight/xWeight.h
HEADERS += ../../../Library/Bat/Weight/DWeight.h
HEADERS += ../../../Library/Bat/Weight/WeightDef.h
HEADERS += ../../../Library/Bat/Weight/SexDef.h
HEADERS += ../../../Library/Bat/Weight/WeightFlagDef.h
HEADERS += ../../../Library/Bat/Weight/MenuWeightUnits.h
HEADERS += ../../../Library/Device/File/Efs.h
HEADERS += ../../../Library/Device/Memory/Nvm.h
HEADERS += ../../../Library/Device/System/System.h
HEADERS += ../../../Library/Device/Gsm/GsmDef.h
HEADERS += ../../../Library/Device/Gsm/Gsm.h
HEADERS += ../../../Library/Micro/Convert/uBcd.h
HEADERS += ../../../Library/Micro/Data/uDirectory.h
HEADERS += ../../../Library/Micro/Data/uDirectoryDef.h
HEADERS += ../../../Library/Micro/Data/uNamedList.h
HEADERS += ../../../Library/Micro/Data/uNamedListDef.h
HEADERS += ../../../Library/Micro/Data/uStorage.h
HEADERS += ../../../Library/Micro/Data/uStorageDef.h
HEADERS += ../../../Library/Micro/Data/uFifo.h
HEADERS += ../../../Library/Micro/Data/uFifoDef.h
HEADERS += ../../../Library/Micro/Data/uListDef.h
HEADERS += ../../../Library/Micro/Data/uList.h
HEADERS += ../../../Library/Micro/Time/uTime.h
HEADERS += ../../../Library/Micro/Time/uDate.h
HEADERS += ../../../Library/Micro/Time/uDateTime.h
HEADERS += ../../../Library/Micro/Time/uClock.h
HEADERS += ../../../Library/Micro/Math/uGeometry.h
HEADERS += ../../../Library/Micro/Parse/uParse.h
#--- connectivity :
HEADERS +=
HEADERS += ../../../Library/Qt/Bat2/WeplClient.h

FORMS   += ../../../Library/Qt/uSimulator/mainwindow.ui \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.ui \
    ../../../Library/Qt/Socket/SocketParameters.ui
FORMS   += ../../../Library/Qt/uSimulator/devicepanel.ui

RESOURCES += Bat2Compact.qrc \
    ../Bat2Platform/Bat2Platform.qrc \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.qrc

OTHER_FILES +=
OTHER_FILES +=
