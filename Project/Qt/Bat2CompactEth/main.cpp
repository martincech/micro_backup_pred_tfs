//******************************************************************************
//
//    main.cpp     Bat2compact simulator main
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include <QtGui/QApplication>
#include <QDir>
#include "File/Efs.h"
#include "Socket/SocketParameters.h"
#include "MainClass.h"

#define EFS_NAME        "Root"

int main(int argc, char *argv[])
{
   QApplication a(argc, argv);
   MainClass mc;
   SocketParameters socketWindow;
   socketWindow.setWindowTitle( "Bat2 TCP connection");
   socketWindow.show();

   // external file system :
   QString efsPath;
   efsPath  = QDir::currentPath();         // get working directory
   efsPath += QString( "/" EFS_NAME);
   EfsRoot( efsPath.toAscii());


   QObject::connect( &socketWindow, SIGNAL(connected(TcpClient &)), &socketWindow, SLOT(close()));
   a.exec();         // Qt main loop
   return( 0);
} // main
