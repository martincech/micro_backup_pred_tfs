#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QProgressBar>
#include <QModelIndex>
#include <QTimer>
#include "Uart/WinUart.h"
#include "Crt/crtdump.h"
#include "Bat2/Remote/Threads.h"


namespace Ui {
   class MainWindow;
}

class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();

private slots:
   void UartConnected();
   void UartDisconnected();

   void on_buttonReset_clicked();

   void on_actionStop_triggered();

   void initDone();
   void sendDone();
   void readDone();
   void statusReadDone();

   void on_lipsumButton_clicked();

   void on_readButton_clicked();

   void on_registerStatus_clicked();


   void on_smsReadyStatus_clicked();


   void on_sendButton_clicked();

   void on_radioButtonSmsSend_clicked();

   void on_radioButtonDataSend_clicked();

   void on_radioButtonSmsCommand_clicked();

   void on_smsText_textChanged();

   void on_smsMemory_clicked(const QModelIndex &index);

   void on_readStatusButton_clicked();


   void on_smsChannelReadyStatus_clicked();
   void updateModemStatistics();
signals:
   void doWork();
private:
   Ui::MainWindow *ui;
   WinUart *_uart;
   CrtDump *_log;
   QLabel  _status;
   QProgressBar _progress;
   Thread *currentThread;
   QThread workerThread;

   QThread smsChannelWorker;
   Thread  *smsChannelThread;

   QTimer statisticsTimer;

   void enableItems();
   void disableItems();

   void startThread(Thread *th, const char* doneSlot);
};




#endif // MAINWINDOW_H
