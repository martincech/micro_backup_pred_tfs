#include "Eeprom/Eeprom.h"
#include "Flash/IFlash.h"
#include <stdio.h>
#include <windows.h>               // Sleep only

int main(int argc, char *argv[])
{
   Sleep(100);
   IFlashInit();
   EepromInit();

   byte Data[] = "ABCD";
   byte DataRead[1024];

   for(int i = 0 ; i < 12 ; i++) {
      EepromWrite(0, "ABCD", 4);
      EepromRead(0, DataRead, 4);
      if(memcmp(Data, DataRead, 4)) {
         return 1;
      }
   }
   return 0;
}
