//*****************************************************************************
//
//    Hardware.h
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Unisys/Uni.h"

#define MCU_FLASH_SIZE           (32 * 1024)
#define MCU_FLASH_SECTOR_SIZE     1024

extern byte IFlash[MCU_FLASH_SIZE];

#define MCU_FLASH_OFFSET         (int)IFlash


#endif
