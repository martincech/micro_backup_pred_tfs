#-------------------------------------------------
#
# StringGlue project
#
#-------------------------------------------------

QT       += core gui

TARGET = StringGlue
TEMPLATE = app

INCLUDEPATH  = ../../../Library/Qt
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Win32

RC_FILE   = StringGlue.rc

SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../Library/Qt/Parse/TextFile.cpp \
    ../../../Library/Qt/Parse/Lexer.cpp \
    StringParser.cpp

HEADERS  += mainwindow.h \
    ../../../Library/Qt/Parse/Lexer.h \
    ../../../Library/Qt/Parse/TextFile.h \
    StringParser.h

FORMS    += mainwindow.ui

RESOURCES += \
    StringGlue.qrc
