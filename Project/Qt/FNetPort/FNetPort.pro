#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T12:25:44
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FNetPort
TEMPLATE = app

INCLUDEPATH  = ../../../Library/Bat2/Compact
INCLUDEPATH += ../../../Library/Qt
INCLUDEPATH += ../../../Library/Qt/Bat2Platform
INCLUDEPATH += ../../../Library/Qt/fnet
INCLUDEPATH += ../../../Library/Bat2
INCLUDEPATH += ../../../Library/Bat2Platform/Compact
INCLUDEPATH += ../../../Library/Bat2Platform
INCLUDEPATH += ../../../Library/Bat
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Device/Ethernet
INCLUDEPATH += ../../../Library/Device/Ethernet/dhcp
INCLUDEPATH += ../../../Library/Device/Ethernet/dns
INCLUDEPATH += ../../../Library/Device/Ethernet/poll
INCLUDEPATH += ../../../Library/Device/Ethernet/serial
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Win32
INCLUDEPATH += ../../../Library/Kinetis/Ethernet
INCLUDEPATH += ../Bat2Compact/
INCLUDEPATH += ../../../Library/Win32/WinPcap/Include

LIBS += -lws2_32
LIBS += -L../../../Library/Win32/WinPcap/Lib -lwpcap


SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../Library/Device/Ethernet/fnet_arp.c \
    ../../../Library/Device/Ethernet/fnet_error.c \
    ../../../Library/Device/Ethernet/fnet_eth.c \
    ../../../Library/Device/Ethernet/fnet_checksum.c \
    ../../../Library/Device/Ethernet/fnet_icmp.c \
    ../../../Library/Device/Ethernet/fnet_icmp6.c \
    ../../../Library/Device/Ethernet/fnet_igmp.c \
    ../../../Library/Device/Ethernet/fnet_inet.c \
    ../../../Library/Device/Ethernet/fnet_ip.c \
    ../../../Library/Device/Ethernet/fnet_ip6.c \
    ../../../Library/Device/Ethernet/fnet_isr.c \
    ../../../Library/Device/Ethernet/fnet_loop.c \
    ../../../Library/Device/Ethernet/fnet_mempool.c \
    ../../../Library/Device/Ethernet/fnet_mld.c \
    ../../../Library/Device/Ethernet/fnet_nd6.c \
    ../../../Library/Device/Ethernet/fnet_netbuf.c \
    ../../../Library/Device/Ethernet/fnet_netif.c \
    ../../../Library/Device/Ethernet/fnet_prot.c \
    ../../../Library/Device/Ethernet/fnet_raw.c \
    ../../../Library/Device/Ethernet/fnet_socket.c \
    ../../../Library/Device/Ethernet/fnet_stack.c \
    ../../../Library/Device/Ethernet/fnet_stdlib.c \
    ../../../Library/Device/Ethernet/fnet_tcp.c \
    ../../../Library/Device/Ethernet/fnet_tcp_gsm.c \
    ../../../Library/Device/Ethernet/fnet_timer.c \
    ../../../Library/Device/Ethernet/fnet_udp.c \
    ../../../Library/Device/Ethernet/fnet_udp_gsm.c \
    ../../../Library/Device/Ethernet/dhcp/fnet_dhcp.c \
    ../../../Library/Device/Ethernet/dns/fnet_dns.c \
    ../../../Library/Device/Ethernet/poll/fnet_poll.c \
    ../../../Library/Device/Ethernet/serial/fnet_serial.c \
    ../../../Library/Kinetis/Ethernet/fnet_cpu.c \
    ../../../Library/Qt/fnet/fnet_win32.cpp \
    ../../../Library/Qt/fnet/fnet_win32_prv.cpp \
    ../../../Library/Qt/fnet/cputimer.cpp \
    ../../../Library/Qt/fnet/FNetLogWindow.cpp

HEADERS  += mainwindow.h \
    ../../../Library/Device/Ethernet/fnet.h \
    ../../../Library/Device/Ethernet/fnet_arp.h \
    ../../../Library/Device/Ethernet/fnet_config.h \
    ../../../Library/Device/Ethernet/fnet_debug.h \
    ../../../Library/Device/Ethernet/fnet_error.h \
    ../../../Library/Device/Ethernet/fnet_eth.h \
    ../../../Library/Device/Ethernet/fnet_eth_prv.h \
    ../../../Library/Device/Ethernet/fnet_checksum.h \
    ../../../Library/Device/Ethernet/fnet_icmp.h \
    ../../../Library/Device/Ethernet/fnet_icmp6.h \
    ../../../Library/Device/Ethernet/fnet_igmp.h \
    ../../../Library/Device/Ethernet/fnet_inet.h \
    ../../../Library/Device/Ethernet/fnet_ip.h \
    ../../../Library/Device/Ethernet/fnet_ip_prv.h \
    ../../../Library/Device/Ethernet/fnet_ip6.h \
    ../../../Library/Device/Ethernet/fnet_ip6_prv.h \
    ../../../Library/Device/Ethernet/fnet_isr.h \
    ../../../Library/Device/Ethernet/fnet_loop.h \
    ../../../Library/Device/Ethernet/fnet_mempool.h \
    ../../../Library/Device/Ethernet/fnet_mld.h \
    ../../../Library/Device/Ethernet/fnet_nd6.h \
    ../../../Library/Device/Ethernet/fnet_netbuf.h \
    ../../../Library/Device/Ethernet/fnet_netif.h \
    ../../../Library/Device/Ethernet/fnet_netif_prv.h \
    ../../../Library/Device/Ethernet/fnet_prot.h \
    ../../../Library/Device/Ethernet/fnet_raw.h \
    ../../../Library/Device/Ethernet/fnet_services.h \
    ../../../Library/Device/Ethernet/fnet_services_config.h \
    ../../../Library/Device/Ethernet/fnet_socket.h \
    ../../../Library/Device/Ethernet/fnet_socket_prv.h \
    ../../../Library/Device/Ethernet/fnet_stack.h \
    ../../../Library/Device/Ethernet/fnet_stack_config.h \
    ../../../Library/Device/Ethernet/fnet_stdlib.h \
    ../../../Library/Device/Ethernet/fnet_tcp.h \
    ../../../Library/Device/Ethernet/fnet_tcp_gsm.h \
    ../../../Library/Device/Ethernet/fnet_timer.h \
    ../../../Library/Device/Ethernet/fnet_timer_prv.h \
    ../../../Library/Device/Ethernet/fnet_udp.h \
    ../../../Library/Device/Ethernet/fnet_udp_gsm.h \
    ../../../Library/Device/Ethernet/fnet_user_config.h \
    ../../../Library/Device/Ethernet/dhcp/fnet_dhcp.h \
    ../../../Library/Device/Ethernet/dhcp/fnet_dhcp_config.h \
    ../../../Library/Device/Ethernet/dns/fnet_dns.h \
    ../../../Library/Device/Ethernet/dns/fnet_dns_config.h \
    ../../../Library/Device/Ethernet/dns/fnet_dns_prv.h \
    ../../../Library/Device/Ethernet/poll/fnet_poll.h \
    ../../../Library/Device/Ethernet/serial/fnet_serial.h \
    ../../../Library/Device/Ethernet/serial/fnet_serial_config.h \
    ../../../Library/Kinetis/Ethernet/EthPhy.h \
    ../../../Library/Kinetis/Ethernet/fnet_comp.h \
    ../../../Library/Kinetis/Ethernet/fnet_comp_config.h \
    ../../../Library/Kinetis/Ethernet/fnet_cpu.h \
    ../../../Library/Kinetis/Ethernet/fnet_cpu_config.h \
    ../../../Library/Kinetis/Ethernet/fnet_os.h \
    ../../../Library/Kinetis/Ethernet/fnet_os_config.h \
    ../../../Library/Win32/WinPcap/Include/bittypes.h \
    ../../../Library/Win32/WinPcap/Include/ip6_misc.h \
    ../../../Library/Win32/WinPcap/Include/Packet32.h \
    ../../../Library/Win32/WinPcap/Include/pcap.h \
    ../../../Library/Win32/WinPcap/Include/pcap-bpf.h \
    ../../../Library/Win32/WinPcap/Include/pcap-namedb.h \
    ../../../Library/Win32/WinPcap/Include/pcap-stdinc.h \
    ../../../Library/Win32/WinPcap/Include/remote-ext.h \
    ../../../Library/Win32/WinPcap/Include/Win32-Extensions.h \
    ../../../Library/Win32/WinPcap/Include/pcap/bluetooth.h \
    ../../../Library/Win32/WinPcap/Include/pcap/bpf.h \
    ../../../Library/Win32/WinPcap/Include/pcap/namedb.h \
    ../../../Library/Win32/WinPcap/Include/pcap/pcap.h \
    ../../../Library/Win32/WinPcap/Include/pcap/sll.h \
    ../../../Library/Win32/WinPcap/Include/pcap/usb.h \
    ../../../Library/Win32/WinPcap/Include/pcap/vlan.h \
    ../../../Library/Qt/fnet/fnet_win32_prv.h \
HEADERS  += ../Bat2Compact/Hardware.h \
    ../../../Library/Qt/fnet/cputimer.h \
    ../../../Library/Qt/fnet/fnet_win32.h \
    ../../../Library/Qt/fnet/FNetLogWindow.h
FORMS    += mainwindow.ui \
    ../../../Library/Qt/fnet/FNetLogWindow.ui
