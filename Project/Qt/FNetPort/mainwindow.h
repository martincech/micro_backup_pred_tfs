#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QList>
#include <QWaitCondition>
#include <QMutex>
#include <QString>
#include <QQueue>
#include <QStringListModel>

extern "C"{
#include "fnet.h"
}

namespace Ui {
   class MainWindow;
}

class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();

   void connectionCreated(SOCKET s);

private slots:
   void on_ipAddress_textChanged(const QString &arg1);
   void on_port_textChanged(const QString &arg1);
   void on_connectButton_clicked();
   void on_connectedSocket_selectionChanged(const QItemSelection & selected, const QItemSelection &);
   void on_disconnectButton_clicked();

   void on_sendButton_clicked();

private:
   QHash<QString, SOCKET> connections;
   QStringListModel connectionIps;
   SOCKET activeSocket;
   bool breakup;
   QMutex readMutex;
   QWaitCondition waitRead;
   QMutex endMutex;
   QWaitCondition waitEnd;
   Ui::MainWindow *ui;
   QLabel status;

   void showStatus(QString);
   void polling();
   void readSocketData();

   void initFnet();

};

#endif // MAINWINDOW_H
