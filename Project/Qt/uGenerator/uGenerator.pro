#-------------------------------------------------
#
# uGenerator
#
#-------------------------------------------------

QT       += core gui

TARGET = uGenerator
TEMPLATE = app

INCLUDEPATH  = ../../../Library/Qt           # zakladni vyvojove prostredi
INCLUDEPATH += ../../../Library/Micro        # obecne knihovny pro mikrokontrolery
INCLUDEPATH += ../../../Library/Win32        # utility pro OS nezavisle na vyvojovem prostredi

RC_FILE   = uGenerator.rc

SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../Library/Qt/Model/arraymodel.cpp \
    ../../../Library/Qt/Parse/csv.cpp \
    ../../../Library/Qt/Parse/nameTransformation.cpp \
    ../../../Library/Qt/uGenerator/uGenerator.cpp \
    ../../../Library/Qt/Parse/TextFile.cpp \
    ../../../Library/Qt/uGenerator/SourceTemplate.cpp \
    ../../../Library/Win32/Parse/Enum.cpp \
    ../../../Library/Qt/uGenerator/DictionaryParser.cpp \
    ../../../Library/Qt/uGenerator/DataParser.cpp \
    ../../../Library/Qt/uGenerator/CodeSeparator.cpp \
    ../../../Library/Qt/uGenerator/CodeStrings.cpp \
    ../../../Library/Qt/uGenerator/CodeHeader.cpp \
    ../../../Library/Qt/uGenerator/CodeDefaults.cpp \
    ../../../Library/Qt/uGenerator/CodeMenu.cpp \
    ../../../Library/Qt/uGenerator/CodeNumber.cpp \
    ../../../Library/Qt/uGenerator/EmbeddedParser.cpp

HEADERS  += mainwindow.h \
    ../../../Library/Qt/Model/arraymodel.h \
    ../../../Library/Qt/Parse/csv.h \
    ../../../Library/Qt/Parse/nameTransformation.h \
    ../../../Library/Qt/uGenerator/uGenerator.h \
    ../../../Library/Qt/Parse/TextFile.h \
    ../../../Library/Qt/uGenerator/SourceTemplate.h \
    ../../../Library/Win32/Parse/Enum.h \
    ../../../Library/Qt/uGenerator/DictionaryParser.h \
    ../../../Library/Qt/uGenerator/GeneratorDef.h \
    ../../../Library/Qt/uGenerator/DataParser.h \
    ../../../Library/Qt/uGenerator/CodeSeparator.h \
    ../../../Library/Qt/uGenerator/CodeStrings.h \
    ../../../Library/Qt/uGenerator/CodeHeader.h \
    ../../../Library/Qt/uGenerator/CodeDefaults.h \
    ../../../Library/Qt/uGenerator/CodeMenu.h \
    ../../../Library/Qt/uGenerator/CodeNumber.h \
    ../../../Library/Qt/uGenerator/EmbeddedParser.h

FORMS    += mainwindow.ui

RESOURCES += \
    uGenerator.qrc
