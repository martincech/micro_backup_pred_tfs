//******************************************************************************
//
//   MenuMaintenanceCalibration.h  Maintenance calibration menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenanceCalibration_H__
   #define __MenuMaintenanceCalibration_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuMaintenanceCalibration( void);
// Menu maintenance calibration

#endif
