//******************************************************************************
//
//   MenuMaintenancePicostrain.c  Maintenance picostrain menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenancePicostrain.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config.h"               // Project configuration

#include "Platform.h"


static DefMenu( MaintenancePicostrainMenu)
   STR_FILTER,
   STR_ACCURACY,
   STR_RATE,
   STR_PREFILTER,
EndMenu()

typedef enum {
   MI_FILTER,
   MI_ACCURACY,
   MI_RATE,
   MI_PREFILTER
} EMaintenancePicostrainMenu;

// Local functions :

static void PlatformPicostrainParameters( int Index, int y, TPlatformPicostrain *Parameters);
// Draw maintenance picostrain parameters

//------------------------------------------------------------------------------
//  Menu MaintenancePicostrain
//------------------------------------------------------------------------------

void MenuMaintenancePicostrain( void)
// Edit maintenance picostrain parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_PICOSTRAIN, MaintenancePicostrainMenu, (TMenuItemCb *)PlatformPicostrainParameters, &PlatformPicostrain, &MData)){
         ConfigPlatformPicostrainSave();
         return;
      }
      switch( MData.Item){
         case MI_FILTER :
            i = PlatformPicostrain.Filter;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PICOSTRAIN_FILTER, _PICOSTRAIN_FILTER_LAST)){
               break;
            }
            PlatformPicostrain.Filter = (byte)i;
            break;

         case MI_ACCURACY :
            i = PlatformPicostrain.Accuracy;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, PLATFORM_PICOSTRAIN_ACCURACY_MIN, PLATFORM_PICOSTRAIN_ACCURACY_MAX, "%")){
               break;
            }
            PlatformPicostrain.Accuracy = (byte)i;
            break;

         case MI_RATE :
            i = PlatformPicostrain.Rate;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_PICOSTRAIN_RATE_MIN, PLATFORM_PICOSTRAIN_RATE_MAX, "Hz")){
               break;
            }
            PlatformPicostrain.Rate = (word)i;
            break;

         case MI_PREFILTER :
            i = PlatformPicostrain.Prefilter;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_PICOSTRAIN_PREFILTER_MIN, PLATFORM_PICOSTRAIN_PREFILTER_MAX, 0)){
               break;
            }
            PlatformPicostrain.Prefilter = (word)i;
            break;

      }
   }
} // MenuMaintenancePicostrain

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformPicostrainParameters( int Index, int y, TPlatformPicostrain *Parameters)
// Draw maintenance picostrain parameters
{
   switch( Index){
      case MI_FILTER :
         DLabelEnum( Parameters->Filter, ENUM_PICOSTRAIN_FILTER, DMENU_PARAMETERS_X, y);
         break;

      case MI_ACCURACY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%f5.1", Parameters->Accuracy, "%");
         break;

      case MI_RATE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Rate, "Hz");
         break;

      case MI_PREFILTER :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Prefilter, 0);
         break;

   }
} // PlatformPicostrainParameters
