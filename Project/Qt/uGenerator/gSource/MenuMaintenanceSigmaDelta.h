//******************************************************************************
//
//   MenuMaintenanceSigmaDelta.h  Maintenance sigma delta menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenanceSigmaDelta_H__
   #define __MenuMaintenanceSigmaDelta_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuMaintenanceSigmaDelta( void);
// Menu maintenance sigma delta

#endif
