//******************************************************************************
//
//   MenuMaintenanceDetection.c  Maintenance detection menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenanceDetection.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config.h"               // Project configuration

#include "Platform.h"


static DefMenu( MaintenanceDetectionMenu)
   STR_MODE,
   STR_AVERAGING_WINDOW,
   STR_ABSOLUTE_RANGE,
   STR_RELATIVE_RANGE,
   STR_STABLE_WINDOW,
   STR_THRESHOLD_WEIGHT,
EndMenu()

typedef enum {
   MI_MODE,
   MI_AVERAGING_WINDOW,
   MI_ABSOLUTE_RANGE,
   MI_RELATIVE_RANGE,
   MI_STABLE_WINDOW,
   MI_THRESHOLD_WEIGHT
} EMaintenanceDetectionMenu;

// Local functions :

static void PlatformDetectionParameters( int Index, int y, TPlatformDetection *Parameters);
// Draw maintenance detection parameters

//------------------------------------------------------------------------------
//  Menu MaintenanceDetection
//------------------------------------------------------------------------------

void MenuMaintenanceDetection( void)
// Edit maintenance detection parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DETECTION, MaintenanceDetectionMenu, (TMenuItemCb *)PlatformDetectionParameters, &PlatformDetection, &MData)){
         ConfigPlatformDetectionSave();
         return;
      }
      switch( MData.Item){
         case MI_MODE :
            i = PlatformDetection.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_DETECTION_MODE, _DETECTION_MODE_LAST)){
               break;
            }
            PlatformDetection.Mode = (byte)i;
            break;

         case MI_AVERAGING_WINDOW :
            i = PlatformDetection.AveragingWindow;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, PLATFORM_DETECTION_AVERAGING_WINDOW_MIN, PLATFORM_DETECTION_AVERAGING_WINDOW_MAX, "s")){
               break;
            }
            PlatformDetection.AveragingWindow = (byte)i;
            break;

         case MI_ABSOLUTE_RANGE :
            i = PlatformDetection.AbsoluteRange;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformDetection.AbsoluteRange = (TWeight)i;
            break;

         case MI_RELATIVE_RANGE :
            i = PlatformDetection.RelativeRange;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, PLATFORM_DETECTION_RELATIVE_RANGE_MIN, PLATFORM_DETECTION_RELATIVE_RANGE_MAX, "%")){
               break;
            }
            PlatformDetection.RelativeRange = (word)i;
            break;

         case MI_STABLE_WINDOW :
            i = PlatformDetection.StableWindow;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, PLATFORM_DETECTION_STABLE_WINDOW_MIN, PLATFORM_DETECTION_STABLE_WINDOW_MAX, "s")){
               break;
            }
            PlatformDetection.StableWindow = (byte)i;
            break;

         case MI_THRESHOLD_WEIGHT :
            i = PlatformDetection.ThresholdWeight;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformDetection.ThresholdWeight = (TWeight)i;
            break;

      }
   }
} // MenuMaintenanceDetection

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformDetectionParameters( int Index, int y, TPlatformDetection *Parameters)
// Draw maintenance detection parameters
{
   switch( Index){
      case MI_MODE :
         DLabelEnum( Parameters->Mode, ENUM_DETECTION_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_AVERAGING_WINDOW :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%f5.1", Parameters->AveragingWindow, "s");
         break;

      case MI_ABSOLUTE_RANGE :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->AbsoluteRange);
         break;

      case MI_RELATIVE_RANGE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%f5.1", Parameters->RelativeRange, "%");
         break;

      case MI_STABLE_WINDOW :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%f5.1", Parameters->StableWindow, "s");
         break;

      case MI_THRESHOLD_WEIGHT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->ThresholdWeight);
         break;

   }
} // PlatformDetectionParameters
