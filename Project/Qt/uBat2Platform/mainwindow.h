//******************************************************************************
//
//   MainWindow.h    Bat2Platform simulator window
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "Crt/CrtDump.h"
#include "Uart/UartParameters.h"
#include "Bat2Platform/PlatformSimulator/PlatformSimulator.h"

namespace Ui {
   class MainWindow;
}

class MainWindow : public QMainWindow
{
   Q_OBJECT
   
public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();
   
private slots:
   void on_actionDump_triggered();
   void on_actionCOM_settings_triggered();
   void timerTimeout();   

private:
   Ui::MainWindow *ui;
   CrtDump        *_dump;
   QTimer         *_timer;
   UartParameters *_parameters;
   PlatformSimulator    *_PlatformSimulator;
};

#endif // MAINWINDOW_H
