/********************************************************************************
** Form generated from reading UI file 'UartParameters.ui'
**
** Created: Fri 12. Jul 13:32:50 2013
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UARTPARAMETERS_H
#define UI_UARTPARAMETERS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UartParameters
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *cbxPort;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *cbxBaudRate;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QComboBox *cbxDataBits;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QComboBox *cbxParity;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QComboBox *cbxStopBits;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *UartParameters)
    {
        if (UartParameters->objectName().isEmpty())
            UartParameters->setObjectName(QString::fromUtf8("UartParameters"));
        UartParameters->resize(191, 202);
        layoutWidget = new QWidget(UartParameters);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 21, 170, 167));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setEnabled(true);
        label->setMinimumSize(QSize(60, 0));

        horizontalLayout->addWidget(label);

        cbxPort = new QComboBox(layoutWidget);
        cbxPort->setObjectName(QString::fromUtf8("cbxPort"));
        cbxPort->setMinimumSize(QSize(100, 0));

        horizontalLayout->addWidget(cbxPort);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(60, 0));

        horizontalLayout_2->addWidget(label_2);

        cbxBaudRate = new QComboBox(layoutWidget);
        cbxBaudRate->setObjectName(QString::fromUtf8("cbxBaudRate"));
        cbxBaudRate->setMinimumSize(QSize(100, 0));

        horizontalLayout_2->addWidget(cbxBaudRate);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(60, 0));

        horizontalLayout_3->addWidget(label_3);

        cbxDataBits = new QComboBox(layoutWidget);
        cbxDataBits->setObjectName(QString::fromUtf8("cbxDataBits"));
        cbxDataBits->setMinimumSize(QSize(100, 0));

        horizontalLayout_3->addWidget(cbxDataBits);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(60, 0));

        horizontalLayout_4->addWidget(label_4);

        cbxParity = new QComboBox(layoutWidget);
        cbxParity->setObjectName(QString::fromUtf8("cbxParity"));
        cbxParity->setMinimumSize(QSize(100, 0));

        horizontalLayout_4->addWidget(cbxParity);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(60, 0));

        horizontalLayout_5->addWidget(label_5);

        cbxStopBits = new QComboBox(layoutWidget);
        cbxStopBits->setObjectName(QString::fromUtf8("cbxStopBits"));
        cbxStopBits->setMinimumSize(QSize(100, 0));

        horizontalLayout_5->addWidget(cbxStopBits);


        verticalLayout->addLayout(horizontalLayout_5);

        buttonBox = new QDialogButtonBox(layoutWidget);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(false);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(UartParameters);
        QObject::connect(buttonBox, SIGNAL(accepted()), UartParameters, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), UartParameters, SLOT(reject()));

        QMetaObject::connectSlotsByName(UartParameters);
    } // setupUi

    void retranslateUi(QDialog *UartParameters)
    {
        UartParameters->setWindowTitle(QApplication::translate("UartParameters", "Uart Parameters", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("UartParameters", "Port :", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("UartParameters", "Baud Rate :", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("UartParameters", "Data Bits :", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("UartParameters", "Parity :", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("UartParameters", "Stop Bits :", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class UartParameters: public Ui_UartParameters {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UARTPARAMETERS_H
