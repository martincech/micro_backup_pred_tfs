//******************************************************************************
//
//   Samples.cpp   Weighing samples loader
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Samples.h"
#include "Bat2Platform/Weighing.h"
#include "Platform/Wepl.h"

#define CSV_COLUMNS_MAX      2
#define CSV_WEIGHT_COLUMN    0

//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

Samples::Samples()
{
   _csv          = new Csv( CSV_COLUMNS_MAX);
   _samples      = 0;
   _samplesCount = 0;
} // MainWindow

//------------------------------------------------------------------------------
//   Destructor
//------------------------------------------------------------------------------

Samples::~Samples()
{
   if( _csv){
      delete _csv;
   }
   if( _samples){
      delete [] _samples;
   }
} // ~MainWindow

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

bool Samples::load( QString fileName)
{
   WeplStop();                                   // stop weighing
   // update samples :
   _samplesCount = 0;                            // clear samples count
   if( _samples){
      delete [] _samples;
      _samples = 0;
   }
   // load file :
   _csv->array()->setRowsCount( 0);              // clear contents
   _csv->setUnicode( false);
   if( !_csv->load( fileName)){
      return( false);
   }
   // convert to samples :
   _samplesCount = _csv->array()->getRowsCount();
   if( _samplesCount == 0){
      return( false);
   }
   _samples = new TWeightGauge[ _samplesCount];
   for( int i = 0; i < _samplesCount; i++){
      _samples[ i] = _csv->array()->getItem( i, CSV_WEIGHT_COLUMN).toInt();
   }
   // set samples :
   WeighingSamplesSet( _samples, _samplesCount); // set weighing samples
   return( true);
} // load

//------------------------------------------------------------------------------
//   Count
//------------------------------------------------------------------------------

int Samples::count()
{
   return( _samplesCount);
} // count
