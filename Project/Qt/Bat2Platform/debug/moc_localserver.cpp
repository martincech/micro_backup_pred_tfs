/****************************************************************************
** Meta object code from reading C++ file 'localserver.h'
**
** Created: Wed 17. Apr 09:22:56 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../Library/Qt/Socket/localserver.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'localserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LocalServer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      28,   12,   12,   12, 0x08,
      44,   12,   12,   12, 0x08,
      63,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_LocalServer[] = {
    "LocalServer\0\0dataReceived()\0connectClient()\0"
    "disconnectClient()\0dataReady()\0"
};

void LocalServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LocalServer *_t = static_cast<LocalServer *>(_o);
        switch (_id) {
        case 0: _t->dataReceived(); break;
        case 1: _t->connectClient(); break;
        case 2: _t->disconnectClient(); break;
        case 3: _t->dataReady(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData LocalServer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LocalServer::staticMetaObject = {
    { &QLocalServer::staticMetaObject, qt_meta_stringdata_LocalServer,
      qt_meta_data_LocalServer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LocalServer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LocalServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LocalServer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LocalServer))
        return static_cast<void*>(const_cast< LocalServer*>(this));
    return QLocalServer::qt_metacast(_clname);
}

int LocalServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLocalServer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void LocalServer::dataReceived()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
