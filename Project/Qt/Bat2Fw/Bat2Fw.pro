#-------------------------------------------------
#
# Project created by QtCreator 2015-03-04T09:41:58
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Bat2Fw
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH  = ../../../Library/Bat2/Compact
INCLUDEPATH += ../../../Library/Qt
INCLUDEPATH += ../../../Library/Qt/Bat2Platform
INCLUDEPATH += ../../../Library/Bat2
INCLUDEPATH += ../../../Library/Bat2Platform/Compact
INCLUDEPATH += ../../../Library/Bat2Platform
INCLUDEPATH += ../../../Library/Bat
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Win32
INCLUDEPATH += ../../../Library/VisualC

SOURCES += main.cpp \
    ../../../Library/Device/Crypt/Aes.c \
    ../../../Library/Device/Crypt/AesCbc.c \
    ../../../Library/VisualC/FwCoder/FwCoder.cpp \
    ../../../Library/VisualC/Hex/HexFile.cpp \
    ../../../Library/Qt/File/Efs.cpp \
    ../../../Library/Device/Memory/File.c \
    ../../../Library/Device/Memory/FileRemote.c \
    ../../../Library/Bat2/Memory/FileDef.c \
    ../../../Library/Bat2/Remote/Cmd.c \
    ../../../Library/Qt/Remote/Socket.cpp \
    ../../../Library/Device/Memory/FileNvm.c \
    ../../../Library/Qt/uSimulator/Nvm.cpp \
    ../../../Library/Device/Remote/Frame.c \
    ../../../Library/Qt/Remote/SocketIfMsdQ.cpp \
    ../../../Library/Device/Fw/FwStorage.c \
    ../../../Library/Device/Remote/SocketIfBootloaderMsd.c

HEADERS += \
    Hardware.h \
    ../../../Library/Device/Remote/SocketIfBootloaderMsd.h
