//******************************************************************************
//
//    main.cpp     Bat2compact simulator main
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include <QtGui/QApplication>
#include <QDir>
#include "File/Efs.h"
#include "uSimulator/mainwindow.h"
#include "Remote/SocketIfMsdQ.h"
#include "Memory/FileRemote.h"
#include "Action/ActionRemote.h"

#define EFS_NAME        "Root"

int main(int argc, char *argv[])
{
SocketIfMsd *Socket;
   QApplication a(argc, argv);

   // external file system :
   QString efsPath;
   efsPath  = QDir::currentPath();         // get working directory
   efsPath += QString( "/" EFS_NAME);
   EfsRoot( efsPath.toAscii());


   Socket = new SocketIfMsd();

   FileRemoteSetup( Socket);
   ActionRemoteSetup( Socket);



   MainWindow *w = new MainWindow();
   w->setWindowTitle( "Bat2 Simulator");
   w->setAttribute(Qt::WA_QuitOnClose);
   // nonvolatile memory initialization :
   w->show();



   a.exec();         // Qt main loop

   return( 0);
} // main
