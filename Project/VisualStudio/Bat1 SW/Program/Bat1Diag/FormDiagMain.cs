﻿using System;
using System.Windows.Forms;
using Bat;
using Bat1Library;
using DataContext;
using Veit.Bat1;
using Bat1;

namespace Bat1Diag {
    public partial class FormDiagMain : Form {
        /// <summary>
        /// Waiting form
        /// </summary>
        private FormScaleWait formScaleWait;

        public FormDiagMain() {
            InitializeComponent();

            labelVersion.Text = DatabaseVersion.ToString();
        }

        private void ShowWaitingForm() {
            formScaleWait = new FormScaleWait(this);
            formScaleWait.Show();
            formScaleWait.Refresh();
        }

        private void UploadLogo(string fileName) {
            // Zkonvertuju data
            byte[] data;
            if (!Bat1Logo.ConvertImage(fileName, out data)) {
                MessageBox.Show("Logo must be bitmap with size 240x160", "Error");
                return;
            }
            
            // Poslu do vahy
            Bat1Version7 bat1 = new Bat1Version7();
            Cursor.Current = Cursors.WaitCursor;
            Refresh();      // Jinak tam zustane cast dialogu Open file
            ShowWaitingForm();
            try {
                if (!bat1.UploadLogo(data)) {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Scale is not connected", "Error");
                    return;
                }
                buttonUploadLogoAgain.Enabled = true;       // Povolim opakovani
            } finally {
                formScaleWait.Close();
                Cursor.Current = Cursors.Default;
            }
        }

        private bool LoadDevice() {
            // Switch off power :
            bool powerOn;
            if (!Bat1Library.Dll.DeviceIsOn(out powerOn)) {
                return false;
            }
            if (powerOn) {
                if (!Bat1Library.Dll.DevicePowerOff()) {
                    return false;
                }
            }
            // Load device :
            if (!Bat1Library.Dll.LoadDevice()) {
                return false;
            }

            return true;
        }

        private bool WriteSampleList(string fileName, SampleList sampleList) {
            // create file :
            int FIndex = Bat1Library.Dll.FileCreate();
            Bat1Library.Dll.SetFileName( FIndex, fileName );       // directory name
            Bat1Library.Dll.SetFileNote( FIndex, "" );  // directory note
            Bat1Library.Dll.SetCurrentFile( FIndex );              // set as working file

            // fill with samples :
            Bat1Library.Dll.FileAllocSamples( FIndex, sampleList.Count);         // alloc samples list
            sampleList.First();
            int i = 0;
            while (sampleList.Read()) {
                int dateTime = Bat1Library.Dll.EncodeTime(sampleList.Sample.DateTime.Day,
                                                        sampleList.Sample.DateTime.Month,
                                                        sampleList.Sample.DateTime.Year,
                                                        sampleList.Sample.DateTime.Hour,
                                                        sampleList.Sample.DateTime.Minute,
                                                        sampleList.Sample.DateTime.Second);
                Bat1Library.Dll.SetSampleTimestamp( FIndex, i, dateTime);
                Bat1Library.Dll.SetSampleWeight( FIndex, i, (int)(sampleList.Sample.Weight * 1000.0F));
                Bat1Library.Dll.SetSampleFlag( FIndex, i, (int)sampleList.Sample.Flag);
                i++;
            }

            return true;
        }

        private bool WriteDemoData(string fileName, float averageWeight, int count, Random randomNumber) {
            SampleList sampleList = TestDemoData.CreateHistogramSampleList(DateTime.Now, averageWeight, 28.0F, count, Flag.NONE, randomNumber);
            return WriteSampleList(fileName, sampleList);
        }

        private bool WriteDemoData(int count) {
            // Load device :
            if (!LoadDevice()) {
                return false;
            }
            
            // remove all files :
            Bat1Library.Dll.FilesDeleteAll();

            Random randomNumber = new Random();
            
            if (!WriteDemoData("HOUSE 1", 1.23F, count, randomNumber)) {
                return false;
            }
            if (!WriteDemoData("HOUSE 2", 1.42F, count, randomNumber)) {
                return false;
            }
            if (!WriteDemoData("HOUSE 3", 1.26F, count, randomNumber)) {
                return false;
            }

            // Save data to device :
            if (!Bat1Library.Dll.SaveDevice()) {
                return false;
            }

            return true;
        }

        private void WriteDemoDataToScale(int count) {
            Cursor.Current = Cursors.WaitCursor;
            FormScaleWait form = new FormScaleWait(this);
            try {
                form.Show();
                form.Refresh();

                if (!WriteDemoData(count)) {
                    MessageBox.Show(Bat1.Properties.Resources.SCALE_NOT_CONNECTED, "Error");
                }
            } finally {
                form.Close();
                Cursor.Current = Cursors.Default;
            }
        }


        private bool WriteManyFiles() {
            // Load device :
            if (!LoadDevice()) {
                return false;
            }
            
            // remove all files :
            Bat1Library.Dll.FilesDeleteAll();
            Bat1Library.Dll.GroupsDeleteAll();

            Random randomNumber = new Random();
        
            const int FILE_COUNT = BatLibrary.Const.DIRECTORY_SIZE - 10;

            // Vytvorim soubory
            for (int i = 1; i <= FILE_COUNT; i++) {
                if (!WriteDemoData("HOUSE " + i.ToString("000"), 1.23F, 12000 / FILE_COUNT, randomNumber)) {
                    return false;
                }
            }

            // Vytvorim skupiny
            for (int i = 0; i < 10; i++) {
                int index = Bat1Library.Dll.GroupCreate();

                // Jmeno a poznamka
                Bat1Library.Dll.SetGroupName(index, "GROUP " + i.ToString("00"));
                Bat1Library.Dll.SetGroupNote(index, "NOTE");

                // Seznam souboru
                Bat1Library.Dll.GroupClearFiles(index);
                for (int file = 0; file < FILE_COUNT; file++) {
                    if (i > 0 && file % i != 0) {
                        continue;
                    }
                    Bat1Library.Dll.AddGroupFile(index, file);
                }
            }

            // Save data to device :
            if (!Bat1Library.Dll.SaveDevice()) {
                return false;
            }

            return true;
        }

        private void WriteManyFilesToScale() {
            Cursor.Current = Cursors.WaitCursor;
            FormScaleWait form = new FormScaleWait(this);
            try {
                form.Show();
                form.Refresh();

                if (!WriteManyFiles()) {
                    MessageBox.Show(Bat1.Properties.Resources.SCALE_NOT_CONNECTED, "Error");
                }
            } finally {
                form.Close();
                Cursor.Current = Cursors.Default;
            }
        }

        private bool WriteOneBigFile() {
            // Load device :
            if (!LoadDevice()) {
                return false;
            }
            
            // remove all files :
            Bat1Library.Dll.FilesDeleteAll();

            Random randomNumber = new Random();
            
            if (!WriteDemoData("HOUSE 1", 1.23F, 11000, randomNumber)) {
                return false;
            }

            // Save data to device :
            if (!Bat1Library.Dll.SaveDevice()) {
                return false;
            }

            return true;
        }

        private void WriteOneBigFileToScale() {
            Cursor.Current = Cursors.WaitCursor;
            FormScaleWait form = new FormScaleWait(this);
            try {
                form.Show();
                form.Refresh();

                if (!WriteOneBigFile()) {
                    MessageBox.Show(Bat1.Properties.Resources.SCALE_NOT_CONNECTED, "Error");
                }
            } finally {
                form.Close();
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonUploadLogo_Click(object sender, EventArgs e) {
            // Zeptam se na soubor s logem
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            UploadLogo(openFileDialog.FileName);
        }

        private void buttonUploadLogoAgain_Click(object sender, EventArgs e) {
            if (!System.IO.File.Exists(openFileDialog.FileName)) {
                MessageBox.Show("No file selected");
                return;
            }

            UploadLogo(openFileDialog.FileName);
        }

        private void buttonWriteDiagnostics_Click(object sender, EventArgs e) {
            // Zeptam se na jmeno souboru
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter   = "*." + FileExtension.HARDCOPY + "|*." + FileExtension.HARDCOPY;
            openFileDialog.FileName = "";
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            Refresh();
            Cursor.Current = Cursors.WaitCursor;
            FormScaleWait form = new FormScaleWait(this);
            try {
                form.Show();
                form.Refresh();
                Bat1Version7 bat1 = null;
                try {
                    bat1 = new Bat1Version7();
                } catch {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(Bat1.Properties.Resources.DRIVER_NOT_INSTALLED, "Error");
                    return;
                }
                if (!bat1.WriteEeprom(openFileDialog.FileName)) {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(Bat1.Properties.Resources.SCALE_NOT_CONNECTED, "Error");
                }
            } finally {
                form.Close();
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonWriteDemoData_Click(object sender, EventArgs e) {
            WriteDemoDataToScale(300);
        }

        private void buttonWriteLargeDemoData_Click(object sender, EventArgs e) {
            WriteDemoDataToScale(4500);
        }

        private void buttonManyFiles_Click(object sender, EventArgs e) {
            WriteManyFilesToScale();
        }

        private void buttonOneBigFile_Click(object sender, EventArgs e) {
            WriteOneBigFileToScale();
        }
    }
}
