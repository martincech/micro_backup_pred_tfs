﻿using System;
using Bat2Library.Connection.Interface.Contract;

namespace Bat1.Services
{
   internal class Bat2ActionEventsContractMessage
   {

      public Bat2ActionEventsContractMessage(Action<IBat2ActionEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2ActionEventsContract> ActionToInvoke { get; private set; }
   }
}
