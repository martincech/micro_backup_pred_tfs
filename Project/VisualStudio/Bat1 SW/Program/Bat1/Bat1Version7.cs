﻿using System;
using System.Collections.Generic;
using System.Text;
using Bat1Library;
using Bat1Library.Connection.Native;
using BatLibrary;
using DataContext;
using Utilities;

namespace Bat1 {
    /// <summary>
    /// Bat1 scale version 7 connection
    /// </summary>
    public class Bat1Version7 {

        /// <summary>
        /// Deafult scale config, initialized in a static constructor
        /// </summary>
        public static Bat1Scale DefaultConfig { get { return defaultConfig; } }
        private static Bat1Scale defaultConfig;

        /// <summary>
        /// Static constructor
        /// </summary>
        static Bat1Version7() {
            // Zakazu logger - staci jednou staticky
            Dll.EnableLogger(false);

            // Vytvorim default config, aby byl dale k dispozici (nemuzu ho vytvaret kdykoliv, protoze Bat1Library.Dll
            // je staticka trida, tj. mohly by se narusit nactena data z vahy)
            Dll.NewDevice();
            defaultConfig = ReadGlobalConfig();
        }

       /// <summary>
        /// Check if device is connected
        /// </summary>
        /// <returns>True if connected</returns>
        private bool CheckDevice() {
            // Kontrolovat musim aspon 2x. Pokud je vaha nepripojena, tak se nastavi nejaky flag, ktery se vynuluje pouze
            // dalsim volanim Bat1Library.Dll.CheckDevice(). Az nasledujicim volanim Bat1Library.Dll.CheckDevice() se opravdu
            // zkontroluje pripojeni.
            for (var i = 0; i < 3; i++) {
                if (Dll.CheckDevice()) {
                    return true;
                }
            }
            return false;
        }
        
        /// <summary>
        /// Check if the scale is connected and ready to communicate
        /// </summary>
        /// <returns></returns>
        public bool Check() {
            // Kontrola pripojeni
            if (!CheckDevice()) {
                return false;
            }

            // Pokud je vaha zapnuta, vypnu ji - behem komunikace musi byt vypnuta (sice je to nutne jen pokud se zapisuje
            // do EEPROM, ale radeji to beru jako pravidlo pri kazde komunikaci. I uzivatel si lepe zvykne, kdyz se vypne vzdy)
            return PowerOff();
        }
        
        /// <summary>
        /// Switch the scale off
        /// </summary>
        /// <returns>True if successful</returns>
        private bool PowerOff() {
            bool powerOn;
            if (!Dll.DeviceIsOn(out powerOn)) {
                return false;
            }
            if (powerOn) {
                if (!Dll.DevicePowerOff()) {
                    return false;
                }
            }
            return true;
        }
        
        /// <summary>
        /// Decode date and time from internal format
        /// </summary>
        /// <param name="value">Date and time in internal format</param>
        /// <returns>DateTime instance</returns>
        private DateTime DecodeDateTime(int value) {
            int day, month, year, hour, min, sec;
            Dll.DecodeTime(value, out day, out month, out year, out hour, out min, out sec);
            return new DateTime(year, month, day, hour, min, sec);
        }
        
        /// <summary>
        /// Read scale name
        /// </summary>
        /// <returns>Scale name</returns>
        public static string ReadScaleName() {
            var stringBuilder = new StringBuilder();
            Dll.GetScaleName(stringBuilder);
            return stringBuilder.ToString();
        }
        
        /// <summary>
        /// Check if password is null
        /// </summary>
        /// <param name="password">Password</param>
        /// <returns>True if null</returns>
        private static bool IsPasswordNull(byte [] password) {
            // Heslo je zakazane, pokud jsou vsechny klavesy K_NULL
            foreach (var key in password) {
                if (key != (byte)Keys.K_NULL) {
                    return false;
                }
            }
            return true;
        }
        
        /// <summary>
        /// Read global scale config, scale doesn't need to be connected
        /// </summary>
        /// <returns>Global config</returns>
        public static Bat1Scale ReadGlobalConfig() {
            var scaleConfig = new Bat1Scale();

            // Verze
            var version = Bcd.ToInt32((int)Dll.GetDeviceVersion());
            scaleConfig.SwVersion.Major = version / 100;
            scaleConfig.SwVersion.Minor = version % 100;
            scaleConfig.SwVersion.Build = (int)Dll.GetBuild();
            scaleConfig.HwVersion    = (byte)Dll.GetHwVersion();

            // Jmeno vahy
            scaleConfig.ScaleName = ReadScaleName();

            // Country - jazyk prevedu podle verze vahy na univerzalni kod jazyka pouzity v DB
            scaleConfig.Country            = (Country)           Dll.GetCountry();
            scaleConfig.LanguageInDatabase = ScaleLanguage.ConvertToDatabase(Dll.GetLanguage(), scaleConfig.SwVersion.Minor);     // Konvertuju na univerzalni kod jazyka pouzivany v DB
            scaleConfig.DateFormat         = (DateFormat)        Dll.GetDeviceDateFormat();
            scaleConfig.DateSeparator1     =                     Dll.GetDateSeparator1();
            scaleConfig.DateSeparator2     =                     Dll.GetDateSeparator2();
            scaleConfig.TimeFormat         = (TimeFormat)        Dll.GetDeviceTimeFormat();
            scaleConfig.TimeSeparator      =                     Dll.GetTimeSeparator();
            scaleConfig.DaylightSavingMode = (DaylightSavingMode)Dll.GetDaylightSavingType();

            // Format hmotnosti
            scaleConfig.Units.Units        = (Units)Dll.GetWeighingUnits();
            scaleConfig.Units.Range        = Dll.GetWeighingRange();
            scaleConfig.Units.Decimals     =        Dll.GetWeighingDecimals();
            scaleConfig.Units.MaxDivision  = Dll.GetWeighingMaxDivision();
            scaleConfig.Units.Division = BatLibrary.ConvertWeight.IntToWeight(Dll.GetWeighingDivision(), scaleConfig.Units.Units);
            scaleConfig.Units.WeighingCapacity = (WeighingCapacity)Dll.GetWeighingCapacity();

            // Zvuky
            scaleConfig.Sounds.ToneDefault    = (Tone)Dll.GetToneDefault();
            scaleConfig.Sounds.ToneLight      = (Tone)Dll.GetToneLight();
            scaleConfig.Sounds.ToneOk         = (Tone)Dll.GetToneOk();
            scaleConfig.Sounds.ToneHeavy      = (Tone)Dll.GetToneHeavy();
            scaleConfig.Sounds.ToneKeyboard   = (Tone)Dll.GetToneKeyboard();
            scaleConfig.Sounds.EnableSpecial  =       Dll.GetEnableSpecialSounds();
            scaleConfig.Sounds.VolumeSaving   =       Dll.GetVolumeSaving();
            scaleConfig.Sounds.VolumeKeyboard =       Dll.GetVolumeKeyboard();

            // Zobrazeni
            scaleConfig.Display.Mode                = (DisplayMode)  Dll.GetDisplayMode();
            scaleConfig.Display.Contrast            =                Dll.GetDisplayContrast();
            scaleConfig.Display.Backlight.Mode      = (BacklightMode)Dll.GetBacklightMode();
            scaleConfig.Display.Backlight.Intensity =                Dll.GetBacklightIntensity();
            scaleConfig.Display.Backlight.Duration  =                Dll.GetBacklightDuration();

            // Tiskarna
            scaleConfig.Printer.PaperWidth          =            Dll.GetPrinterPaperWidth();
            scaleConfig.Printer.CommunicationFormat = (ComFormat)Dll.GetPrinterCommunicationFormat();
            scaleConfig.Printer.CommunicationSpeed  =            Dll.GetPrinterCommunicationSpeed();

            // Globalni nastaveni
            scaleConfig.PowerOffTimeout      = Dll.GetPowerOffTimeout();
            scaleConfig.EnableFileParameters = Dll.GetEnableFileParameters();

            // Heslo
            Dll.GetPassword(scaleConfig.PasswordConfig.Password);
            scaleConfig.PasswordConfig.Enable = !IsPasswordNull(scaleConfig.PasswordConfig.Password);

            // Vazeni
            scaleConfig.WeighingConfig.Saving.EnableMoreBirds    = Dll.GetEnableMoreBirds();
            scaleConfig.WeighingConfig.Saving.NumberOfBirds      = 1;     // Pouziva se u kazdeho souboru zvlast, default necham 1
            scaleConfig.WeighingConfig.WeightSorting.Mode        = (WeightSorting)Dll.GetWeightSortingMode();
            scaleConfig.WeighingConfig.WeightSorting.LowLimit    = 1;     // Pouziva se u kazdeho souboru zvlast
            scaleConfig.WeighingConfig.WeightSorting.HighLimit   = 2;     // Pouziva se u kazdeho souboru zvlast
            scaleConfig.WeighingConfig.Saving.Mode               = (SavingMode)   Dll.GetSavingMode();
            scaleConfig.WeighingConfig.Saving.Filter             = Dll.GetFilter() / 10.0;
            scaleConfig.WeighingConfig.Saving.StabilisationTime  = Dll.GetStabilisationTime() / 10.0;
            scaleConfig.WeighingConfig.Saving.MinimumWeight = BatLibrary.ConvertWeight.IntToWeight(Dll.GetMinimumWeight(), scaleConfig.Units.Units);
            scaleConfig.WeighingConfig.Saving.StabilisationRange = Dll.GetStabilisationRange() / 10.0;
        
            // Statistika
            scaleConfig.StatisticConfig.UniformityRange =                Dll.GetUniformityRange();
            scaleConfig.StatisticConfig.Histogram.Mode  = (HistogramMode)Dll.GetHistogramMode();
            scaleConfig.StatisticConfig.Histogram.Range =                Dll.GetHistogramRange();
            scaleConfig.StatisticConfig.Histogram.Step = BatLibrary.ConvertWeight.IntToWeight(Dll.GetHistogramStep(), scaleConfig.Units.Units);

            // Seznam souboru vcetne nastaveni souboru, zaroven nactu aktivni soubor
            scaleConfig.FileList = new FileList();
            var filesCount = Dll.GetFilesCount();
            for (var index = 0; index < filesCount; index++) {
                var stringBuilder = new StringBuilder();
                // Nactu jmeno
                Dll.GetFileName(index, stringBuilder);
                var name = stringBuilder.ToString();

                // Nactu poznamku
                Dll.GetFileNote(index, stringBuilder);
                var note = stringBuilder.ToString();
                
                // Vytvorim novy soubor, zaroven nactu jeho config a pridam ho do seznamu
                scaleConfig.FileList.Add(new File(name, note, ReadFileConfig(index)));

                // Zkontroluju, zda jde o aktivni soubor
                if (Dll.IsCurrentFile(index)) {
                    scaleConfig.ActiveFileIndex = index;
                }
            }

            // Skupiny souboru
            scaleConfig.FileGroupList = new FileGroupList();
            var groupsCount = Dll.GetGroupsCount();
            for (var index = 0; index < groupsCount; index++) {
                var stringBuilder = new StringBuilder();
                // Nactu jmeno
                Dll.GetGroupName(index, stringBuilder);
                var name = stringBuilder.ToString();

                // Nactu poznamku
                Dll.GetGroupNote(index, stringBuilder);
                var note = stringBuilder.ToString();

                // Vytvorim novou skupinu
                var group = new FileGroup(name);
                group.SetNote(note);

                // Pridam do skupiny vsechny soubory
                filesCount = Dll.GetGroupFilesCount(index);
                for (var fileIndex = 0; fileIndex < filesCount; fileIndex++) {
                    group.AddFile(Dll.GetGroupFile(index, fileIndex));
                }
                
                // Pridam vytvorenou skupinu do seznamu
                scaleConfig.FileGroupList.Add(group);
            }

            return scaleConfig;
        }

        /// <summary>
        /// Write country config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        /// <param name="version">Version of connected scale</param>
        private void WriteCountryConfig(ScaleConfig scaleConfig, ScaleConfigVersion version) {
            Dll.SetCountry(           (int)scaleConfig.Country);
            Dll.SetLanguage(ScaleLanguage.ConvertToScale(scaleConfig.LanguageInDatabase, version.Minor));    // Kod jazyka pro tuto verzi vah
            Dll.SetCodePage(Locales.GetCodePage(ScaleLanguage.ConvertToScale(scaleConfig.LanguageInDatabase, SoftwareVersion.MINOR)));  // Od verze 7.02.x musim nastavovat kodovou stranku rucne podle jazyka
            Dll.SetDeviceDateFormat(  (int)scaleConfig.DateFormat);
            Dll.SetDateSeparator1(         scaleConfig.DateSeparator1);
            Dll.SetDateSeparator2(         scaleConfig.DateSeparator2);
            Dll.SetDeviceTimeFormat(  (int)scaleConfig.TimeFormat);
            Dll.SetTimeSeparator(          scaleConfig.TimeSeparator);
            Dll.SetDaylightSavingType((int)scaleConfig.DaylightSavingMode);
        }

        /// <summary>
        /// Write scale config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteWeighingConfig(ScaleConfig scaleConfig) {
            Dll.SetWeighingUnits((int)scaleConfig.Units.Units);
            Dll.SetWeighingDivision(BatLibrary.ConvertWeight.WeightToInt(scaleConfig.Units.Division, scaleConfig.Units.Units));
            Dll.SetWeighingCapacity((int)scaleConfig.Units.WeighingCapacity);     // Musim volat az po SetWeighingUnits()
        }

        /// <summary>
        /// Write sounds config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteSoundsConfig(Bat1Scale scaleConfig) 
        {
           Dll.SetToneDefault(    (int)scaleConfig.Sounds.ToneDefault);
            Dll.SetToneLight(     (int)scaleConfig.Sounds.ToneLight);
            Dll.SetToneOk(        (int)scaleConfig.Sounds.ToneOk);
            Dll.SetToneHeavy(     (int)scaleConfig.Sounds.ToneHeavy);
            Dll.SetToneKeyboard(  (int)scaleConfig.Sounds.ToneKeyboard);
            Dll.SetEnableSpecialSounds(scaleConfig.Sounds.EnableSpecial);
            Dll.SetVolumeSaving(       scaleConfig.Sounds.VolumeSaving);
            Dll.SetVolumeKeyboard(     scaleConfig.Sounds.VolumeKeyboard);
        }

        /// <summary>
        /// Write display config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteDisplayConfig(ScaleConfig scaleConfig) {
            // Kontrast zde nenastavuju, nastavuje se extra
            Dll.SetDisplayMode(       (int)scaleConfig.Display.Mode);
            Dll.SetBacklightMode(     (int)scaleConfig.Display.Backlight.Mode);
            Dll.SetBacklightIntensity(     scaleConfig.Display.Backlight.Intensity);
            Dll.SetBacklightDuration(      scaleConfig.Display.Backlight.Duration);
        }

        /// <summary>
        /// Write printer config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WritePrinterConfig(Bat1Scale scaleConfig) {
            Dll.SetPrinterPaperWidth(              scaleConfig.Printer.PaperWidth);
            Dll.SetPrinterCommunicationFormat((int)scaleConfig.Printer.CommunicationFormat);
            Dll.SetPrinterCommunicationSpeed(      scaleConfig.Printer.CommunicationSpeed);
        }

        /// <summary>
        /// Write auto power off config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteAutoPowerOffConfig(ScaleConfig scaleConfig) {
            Dll.SetPowerOffTimeout(scaleConfig.PowerOffTimeout);
        }

        /// <summary>
        /// Write Password config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WritePasswordConfig(Bat1Scale scaleConfig) {
            if (scaleConfig.PasswordConfig.Enable) {
                Dll.SetPassword(scaleConfig.PasswordConfig.Password);
            } else {
                byte [] password = {1, 2};
                Dll.SetPassword(scaleConfig.PasswordConfig.Password);
            }
        }

        /// <summary>
        /// Write weighing config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteSavingConfig(Bat1Scale scaleConfig) {
            Dll.SetWeightSortingMode(         (int)scaleConfig.WeighingConfig.WeightSorting.Mode);
            Dll.SetSavingMode(                (int)scaleConfig.WeighingConfig.Saving.Mode);
            Dll.SetFilter(            (int)(10.0 * scaleConfig.WeighingConfig.Saving.Filter));
            Dll.SetStabilisationTime( (int)(10.0 * scaleConfig.WeighingConfig.Saving.StabilisationTime));
            Dll.SetMinimumWeight(BatLibrary.ConvertWeight.WeightToInt(scaleConfig.WeighingConfig.Saving.MinimumWeight, scaleConfig.Units.Units));
            Dll.SetStabilisationRange((int)(10.0 * scaleConfig.WeighingConfig.Saving.StabilisationRange));
            Dll.SetEnableMoreBirds(                scaleConfig.WeighingConfig.Saving.EnableMoreBirds);
            Dll.SetEnableFileParameters(           scaleConfig.EnableFileParameters);
        }

        /// <summary>
        /// Write statistics config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteStatisticsConfig(ScaleConfig scaleConfig) {
            Dll.SetUniformityRange(scaleConfig.StatisticConfig.UniformityRange);
            if (scaleConfig.StatisticConfig.Histogram.Mode == HistogramMode.RANGE) {
                Dll.SetHistogramRange(scaleConfig.StatisticConfig.Histogram.Range);
            } else {
               Dll.SetHistogramStep(BatLibrary.ConvertWeight.WeightToInt(scaleConfig.StatisticConfig.Histogram.Step, scaleConfig.Units.Units));
            }
        }

        /// <summary>
        /// Check there is enough space to create new file or group
        /// </summary>
        /// <returns>True if can be created</returns>
        private bool CanCreateFileOrGroup() {
            // V souborovem systemu je misto jen na DIRECTORY_SIZE souboru a skupin dohromady
            return Dll.GetFilesCount() + Dll.GetGroupsCount() < Const.DIRECTORY_SIZE; 
        }
        
        /// <summary>
        /// Write file list config into the device and set acrive file
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteFilesConfig(Bat1Scale scaleConfig) {
            // Vytvorim vsechny soubory
            foreach (var file in scaleConfig.FileList.List) {
                if (!CanCreateFileOrGroup()) {
                    break;      // Na dalsi soubor uz neni misto
                }

                var index = Dll.FileCreate();

                // Jmeno a poznamka
                Dll.SetFileName(index, file.Name);
                Dll.SetFileNote(index, file.Note);

                // Nastaveni souboru
                var weighingConfig = file.FileConfig.WeighingConfig;     // Pro zkraceni textu

                // Pocet kusu a meze nastavuji u kazdeho souboru zvlast i kdyz pouziva jen globalni parametry
                Dll.SetFileNumberOfBirds(index, weighingConfig.Saving.NumberOfBirds);
                Dll.SetFileLowLimit(index, BatLibrary.ConvertWeight.WeightToInt(weighingConfig.WeightSorting.LowLimit, scaleConfig.Units.Units));
                Dll.SetFileHighLimit(index, BatLibrary.ConvertWeight.WeightToInt(weighingConfig.WeightSorting.HighLimit, scaleConfig.Units.Units));

                if (!scaleConfig.EnableFileParameters) {
                    // Parametry souboru nejsou povoleny
                    continue;       
                }

                // Ostatni parametry souboru, ktere se nastavuji pouze pri povolenem file-specific settings
                Dll.SetFileWeightSortingMode(index,  (int)       weighingConfig.WeightSorting.Mode);
                Dll.SetFileEnableMoreBirds(index,                weighingConfig.Saving.EnableMoreBirds);
                Dll.SetFileSavingMode(index,         (int)        weighingConfig.Saving.Mode);
                Dll.SetFileFilter(index,             (int)(10.0 * weighingConfig.Saving.Filter));
                Dll.SetFileStabilisationTime(index,  (int)(10.0 * weighingConfig.Saving.StabilisationTime));
                Dll.SetFileMinimumWeight(index, BatLibrary.ConvertWeight.WeightToInt(weighingConfig.Saving.MinimumWeight, scaleConfig.Units.Units));
                Dll.SetFileStabilisationRange(index, (int)(10.0 * weighingConfig.Saving.StabilisationRange));
            }

            // Nastavim aktivni soubor
            if (scaleConfig.FileList.List.Count > 0 && scaleConfig.ActiveFileIndex >= 0) {
                Dll.SetCurrentFile(scaleConfig.ActiveFileIndex);
            }
        }

        /// <summary>
        /// Write group list config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteGroupsConfig(ScaleConfig scaleConfig) {
            foreach (var group in scaleConfig.FileGroupList.List) {
                if (!CanCreateFileOrGroup()) {
                    break;      // Na dalsi skupinu uz neni misto
                }

                var index = Dll.GroupCreate();

                // Jmeno a poznamka
                Dll.SetGroupName(index, group.Name);
                Dll.SetGroupNote(index, group.Note);

                // Seznam souboru
                Dll.GroupClearFiles(index);
                foreach (var fileIndex in group.FileIndexList) {
                    Dll.AddGroupFile(index, fileIndex);
                }
            }
        }

        /// <summary>
        /// Delete all files and groups from the device
        /// </summary>
        private void DeleteAllFilesAndGroups() {
            Dll.GroupsDeleteAll();
            Dll.FilesDeleteAll();
        }

        /// <summary>
        /// Write whole config into the device. The device must be already read!
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        public void WriteConfig(Bat1Scale scaleConfig) {
            // Nactu cislo verze prave pripojene vahy
            var version = ReadGlobalConfig().SwVersion;          

            // Updatuju cely config
            // Smazu vsechny soubory a skupiny a pridam nove definovane. Vzorky se smazou automaticky.
            DeleteAllFilesAndGroups();
            WriteFilesConfig(scaleConfig);
            WriteGroupsConfig(scaleConfig);
            WriteCountryConfig(scaleConfig, version);
            WritePasswordConfig(scaleConfig);
            WriteDisplayConfig(scaleConfig);
            WriteSoundsConfig(scaleConfig);
            WriteSavingConfig(scaleConfig);
            WriteStatisticsConfig(scaleConfig);
            WriteWeighingConfig(scaleConfig);
            WritePrinterConfig(scaleConfig);
            WriteAutoPowerOffConfig(scaleConfig);
        }

        /// <summary>
        /// Save device
        /// </summary>
        /// <returns>True if successful</returns>
        public bool SaveDevice() {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }

            return Dll.SaveDevice();
        }

        /// <summary>
        /// Read file-specific config
        /// </summary>
        /// <param name="index">File index</param>
        /// <returns>File-specific config</returns>
        private static FileConfig ReadFileConfig(int index) {
            var fileConfig = new FileConfig();
            var units = (Units)Dll.GetWeighingUnits();      // Globalni config zde nemam k dispozici

            // Pokud pouziva pouze globalni parametry, funkce GetFileXXX() nelze pouzit, musim hodnoty rucne nacist z globalniho configu
            // Pocet kurat a meze se ukladaji ke kazdemu souboru zvlast vzdy, i pokud ma nastavene pouziti globalniho configu
            fileConfig.WeighingConfig.Saving.NumberOfBirds    = Dll.GetFileNumberOfBirds(index);
            fileConfig.WeighingConfig.WeightSorting.LowLimit = BatLibrary.ConvertWeight.IntToWeight(Dll.GetFileLowLimit(index), units);
            fileConfig.WeighingConfig.WeightSorting.HighLimit = BatLibrary.ConvertWeight.IntToWeight(Dll.GetFileHighLimit(index), units);

            if (Dll.GetEnableFileParameters()) {
                // Pouziva nastaveni pro kazdy soubor zvlast
                fileConfig.WeighingConfig.WeightSorting.Mode        = (WeightSorting)Dll.GetFileWeightSortingMode(index);
                fileConfig.WeighingConfig.Saving.EnableMoreBirds    =                Dll.GetFileEnableMoreBirds(index);
                fileConfig.WeighingConfig.Saving.Mode               = (SavingMode)   Dll.GetFileSavingMode(index);
                fileConfig.WeighingConfig.Saving.Filter             = Dll.GetFileFilter(index) / 10.0;
                fileConfig.WeighingConfig.Saving.StabilisationTime  = Dll.GetFileStabilisationTime(index) / 10.0;
                fileConfig.WeighingConfig.Saving.MinimumWeight = BatLibrary.ConvertWeight.IntToWeight(Dll.GetFileMinimumWeight(index), units);
                fileConfig.WeighingConfig.Saving.StabilisationRange = Dll.GetFileStabilisationRange(index) / 10.0;
            } else {
                // Pouziva pouze globalni parametry, pouze preberu
                fileConfig.WeighingConfig.WeightSorting.Mode        = (WeightSorting)Dll.GetWeightSortingMode();
                fileConfig.WeighingConfig.Saving.EnableMoreBirds    =                Dll.GetEnableMoreBirds();
                fileConfig.WeighingConfig.Saving.Mode               = (SavingMode)   Dll.GetSavingMode();
                fileConfig.WeighingConfig.Saving.Filter             = Dll.GetFilter() / 10.0;
                fileConfig.WeighingConfig.Saving.StabilisationTime  = Dll.GetStabilisationTime() / 10.0;
                fileConfig.WeighingConfig.Saving.MinimumWeight = BatLibrary.ConvertWeight.IntToWeight(Dll.GetMinimumWeight(), units);
                fileConfig.WeighingConfig.Saving.StabilisationRange = Dll.GetStabilisationRange() / 10.0;
            }

            return fileConfig;
        }

        /// <summary>
        /// Read samples saved in a file
        /// </summary>
        /// <param name="fileIndex">File index</param>
        /// <returns>List of samples</returns>
        private SampleList ReadFileSamples(int fileIndex) {
            var samplesCount = Dll.GetFileSamplesCount(fileIndex);

            // Nactu vsechny vzorky
            var list = new SampleList();
            for (var sampleIndex = 0; sampleIndex < samplesCount; sampleIndex++) {
                var sample = new Sample();
                sample.DateTime = DecodeDateTime(Dll.GetSampleTimestamp(fileIndex, sampleIndex));
                sample.Weight   = (float)Dll.DecodeWeight(Dll.GetSampleWeight(fileIndex, sampleIndex));
                sample.Flag     = (Flag)Dll.GetSampleFlag(fileIndex, sampleIndex);
                list.Add(sample);
            }
            list.SortByTime();  // Radsi setridim, kdyby napr. behem vazeni zmenil datum ve vaze

            return list;
        }
        
        private WeighingData ReadFile(int fileIndex, Bat1Scale scaleConfig) {
            var stringBuilder = new StringBuilder();

            // Jmeno souboru
            Dll.GetFileName(fileIndex, stringBuilder);
            var name = stringBuilder.ToString();

            // Definici souboru vcetne nastaveni beru ze seznamu souboru v globalnim configu
            var file = scaleConfig.GetFile(name);

/*            // Poznamka
            Bat1Library.Dll.GetFileNote(fileIndex, stringBuilder);
            string note = stringBuilder.ToString();

            // Soubor vcetne nastaveni
            File file = new File(name, note, ReadFileConfig(fileIndex));*/

            // Vzorky
            var sampleList = ReadFileSamples(fileIndex);

            return new WeighingData(-1, ResultType.NEW_SCALE, RecordSource.SAVE, file, sampleList, scaleConfig, "");
        }
        
        private List<WeighingData> ReadFiles(Bat1Scale scaleConfig) {
            var fileList = new List<WeighingData>();

            // Zjistim pocet souboru ve vaze
            var filesCount = Dll.GetFilesCount();

            // Postupne nacitam jednotlive soubory
            for (var fileIndex = 0; fileIndex < filesCount; fileIndex++) {
                fileList.Add(ReadFile(fileIndex, scaleConfig));
            }

            return fileList;
        }
        
        /// <summary>
        /// Load device
        /// </summary>
        /// <returns>True if successful</returns>
        public bool LoadDevice() {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }
            
            // Nahraju vsechna data z vahy
            if (!Dll.LoadDevice()) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Load complete scale config only
        /// </summary>
        /// <returns>True if successful</returns>
        public bool LoadConfig() {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }
            
            // Nahraju z vahy nastaveni
            if (!Dll.LoadConfiguration()) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check if the scale is supported by the SW. Scales with higher version numbers than the SW was designed to are not supported.
        /// </summary>
        /// <returns></returns>
        public bool IsSupported() {
            // Pozor, device uz musi byt nacteny
            
            // Nactu globalni config
            ScaleConfig scaleConfig = ReadGlobalConfig();

            // Vahy 7.00.x nepodporuju, jsou binarne nekompatibilni s 7.01 (7.00.x mela jen 64 souboru)
            if (scaleConfig.SwVersion.Major == 7 && scaleConfig.SwVersion.Minor == 0) {
                return false;
            }
            
            // Cislo verze vahy musi byt mensi nebo rovno cislu verze, ktere podporuje SW.
            // Build neni treba kontrolovat
            if (scaleConfig.SwVersion.Major > SoftwareVersion.MAJOR) {
                return false;
            }
            if (scaleConfig.SwVersion.Major == SoftwareVersion.MAJOR && scaleConfig.SwVersion.Minor > SoftwareVersion.MINOR)
            {
                return false;
            }

            // Podporovana vaha
            return true;
        }

        public bool ReadAll(out Bat1Scale scaleConfig, out List<WeighingData> weighingDataList) {
            scaleConfig      = null;
            weighingDataList = null;
            
            // Kontrola pripojeni
            if (!LoadDevice()) {
                return false;
            }

            // Globalni nastaveni vahy
            scaleConfig = ReadGlobalConfig();

            // Soubory
            weighingDataList = ReadFiles(scaleConfig);
            
            return true;
        }

        /// <summary>
        /// Clear samples in all files
        /// </summary>
        /// <returns>True if successful</returns>
        public bool ClearAllFiles() {
            // Nactu
            if (!LoadConfig()) {
                return false;
            }

            // Smazu vsechny vzorky
            var filesCount = Dll.GetFilesCount();
            for (var index = 0; index < filesCount; index++) {
                Dll.FileClearSamples(index);
            }

            // Ulozim zpet do vahy, kontrolovat pripojeni znovu nemusim
            return Dll.SaveDevice();
        }

        /// <summary>
        /// Upload startup logo
        /// </summary>
        /// <param name="data">Logo data</param>
        /// <returns>True if successful</returns>
        public bool UploadLogo(byte[] data) {
            // Kontrola pripojeni, nic z vahy neni treba nacitat
            if (!Check()) {
                return false;
            }

            // Ulozim logo do vahy
            return Dll.WriteEeprom(Dll.GetLogoAddress(), data, Dll.GetLogoSize());
        }

        public bool LoadEstimation(out float percent) {
            int promile;
            if (!Dll.LoadEstimation(out promile)) {
                percent = 0;
                return false;
            }
            percent = promile / 10.0f;
            return true;
        }

        /// <summary>
        /// Set computer date and time
        /// </summary>
        /// <returns>True if successful</returns>
        public bool SetDateTime() {
            // Pred zapisem casu musim nacist config, aby spravne fungovala fce EncodeTime(), ktera pouziva
            // konfiguraci pro nastaveni letni/zimni cas
            if (!LoadConfig()) {
                return false;
            }

            var now = DateTime.Now;
            return Dll.SetTime(Dll.EncodeTime(now.Day, now.Month, now.Year, now.Hour, now.Minute, now.Second));
        }

        /// <summary>
        /// Reads date and time from the scale
        /// </summary>
        /// <param name="dateTime">Date and time</param>
        /// <returns>True if successful</returns>
        public bool GetDateTime(out DateTime dateTime) {
            int timestamp;
            if (!Dll.GetTime(out timestamp)) {
                dateTime = DateTime.MinValue;
                return false;
            }

            int day, month, year, hour, min, sec;
            Dll.DecodeTime(timestamp, out day, out month, out year, out hour, out min, out sec);
            dateTime = new DateTime(year, month, day, hour, min, sec);
            return true;
        }

        /// <summary>
        /// Set scale name
        /// </summary>
        /// <param name="name">New name</param>
        /// <returns>True if successful</returns>
        public bool SetName(string name) {
            // Pozor, obsah vahy uz musi byt nacteny, zde uz LoadDevice() nevolam. Predpokladane pouziti je:
            // 1. LoadDevice()
            // 2. Uzivatel edituje jmeno
            // 3. Zavolani teto fce

            // Nastavim nove jmeno
            Dll.SetScaleName(name);

            // Ulozim zpet do vahy, kontrolovat pripojeni znovu nemusim
            return Dll.SaveDevice();
        }

        /// <summary>
        /// Read whole EEPROM and save it as a file
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>True if successful</returns>
        public bool ReadEeprom(string fileName) {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }

            var size = Dll.GetEepromSize();
            var buffer = new byte[size];
            if (!Dll.ReadEeprom(0, buffer, size)) {
                return false;
            }
            var fileStream = System.IO.File.Create(fileName);
            fileStream.Write(buffer, 0, size);
            fileStream.Close();

            return true;
        }

        /// <summary>
        /// Write whole EEPROM from a file
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>True if successful</returns>
        public bool WriteEeprom(string fileName) {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }

            var size = Dll.GetEepromSize();
            var buffer = new byte[size];
            if (!System.IO.File.Exists(fileName)) {
                return false;
            }
            var fileStream = System.IO.File.OpenRead(fileName);
            fileStream.Read(buffer, 0, size);
            fileStream.Close();
            if (!Dll.WriteEeprom(0, buffer, size)) {
                return false;
            }
/*            if (!Bat1Library.Dll.DeviceByEeprom(buffer)) {
                return false;
            }*/

            return true;
        }

    
    }
}
