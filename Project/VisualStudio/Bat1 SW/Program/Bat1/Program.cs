﻿using System.Drawing.Printing;
using System.IO;
using System.Windows;
using Bat1.Properties;
using Bat2Library.Connection;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager;
using DataContext;
using Desktop.WinForms;
using Ninject;
using Utilities.IOC;
using SingleApplication = Utilities.SingleApplication;
using System;

namespace Bat1
{
   public static class Program
   {
      /// <summary>
      /// Database instance
      /// </summary>
      public static Database.Database Database;

      /// <summary>
      /// Application setup
      /// </summary>
      public static Setup Setup;

      /// <summary>
      /// Data folder name in user's profile
      /// </summary>
      public static string DataFolder { get; private set; }

      /// <summary>
      /// Folder for temporary files in user's profile
      /// </summary>
      public static string TempFolder { get; private set; }

      /// <summary>
      /// Database file name without path
      /// </summary>
      public static readonly string DatabaseFileName = AppConfig.ApplicationNameSmall + "." + FileExtension.DATABASE;

      /// <summary>
      /// Full database file name including path
      /// </summary>
      public static string FullDatabaseFileName
      {
         get { return DataFolder + @"\" + DatabaseFileName; }
      }

      /// <summary>
      /// Application name
      /// </summary>
      public static readonly string ApplicationName = AppConfig.ApplicationName;

      /// <summary>
      /// Global printer settings used for all printing jobs (so the user doesn't have to select
      /// a printer every time)
      /// </summary>
      public static PrinterSettings PrinterSettings = new PrinterSettings();


      /// <summary>
      /// Prepare working directories
      /// </summary>
      public static void PrepareDirectories()
      {
         // Nactu nazev pracovniho adresare (struktura "Veit\Bat1\V7")
         var sFolders =
            new SFolders(AppConfig.CompanyDirectory + @"\" + AppConfig.ApplicationNameSmall + @"\V" +
                         DatabaseVersion.MAJOR);
         sFolders.PrepareData(null); // Pokud neexistuje, vytvorim pracovni adresar
         DataFolder = sFolders.DataFolder; // Zapamatuju si pracovni adresar

         // Adresar pro docasne soubory
         TempFolder = DataFolder + @"\Temp"; // Zapamatuju si adresar pro docasne soubory
         if (!Directory.Exists(TempFolder))
         {
            // Pokud neexistuje, vytvorim
            Directory.CreateDirectory(TempFolder);
         }
      }

      /// <summary>
      /// Database initialization
      /// </summary>
      private static bool InitDatabase(System.Windows.Forms.Control splash)
      {
         // Zalozim objekt databaze
         Database = new Database.Database(FullDatabaseFileName);

         // Zkontroluju databazi
         if (Database.Exists())
         {
            // Zkontroluju integritu dat v databazi
            if (!Database.CheckIntegrity())
            {
               // Korupce dat, musi obnovit ze zalohy
               if (splash != null)
               {
                  splash.Hide(); // Schovam splash screen
               }
               try
               {
                  MessageBox.Show(Resources.DATABASE_CORRUPTED, ApplicationName);
                  var form = new FormRestoreBackup();
                  if (form.ShowDialog() != System.Windows.Forms.DialogResult.OK)               
                  {
                     return false;
                  }
               }
               finally
               {
                  if (splash != null)
                  {
                     splash.Show(); // Na zaver opet zobrazim
                  }
               }
            }

            // Zkontroluji platnost nastaveni
            Database.CheckSetup();

            // Zkontroluji verzi databaze (az po pripadne obnove ze zalohy)
            if (!Database.Update())
            {
               // Databaze je novejsi nez SW, musi upgradovat SW
               if (splash != null)
               {
                  splash.Hide(); // Schovam splash screen
               }
               MessageBox.Show(Resources.DATABASE_NOT_SUPPORTED, ApplicationName);
               return false;
            }

            // Provedu automatickou zalohu databaze po startu programu
            StartupBackup.Backup(true); // Integritu jsem uz otestoval, neni treba znovu

            // Periodicka zaloha kazdych 7 dni
            PeriodicBackup.Execute();
         }
         else
         {
            // Database neexistuje
            Database.CreateDatabase(); // Vytvorim prazdnou databazi
            Database.CreateTables(); // Vytvorim vsechny tabulky
         }

         return true;
      }

      /// <summary>
      /// Init ninject kernel
      /// </summary>
      private static void InitIoc()
      {
         IKernel ninjectKernel = new StandardKernel();
         // Load modules
         ninjectKernel.Load(new[] {new ConnectedDevicesModule()});
         // Specify types
         ninjectKernel.Bind<IBat1ScaleControl>().To<UserControlScaleBat1>();
         ninjectKernel.Bind<IBat2ScaleControl>().To<UserControlScaleBat2>();
         ninjectKernel.Bind<IScaleControl>().To<UserControlScale>();
         ninjectKernel.Bind<IWeighingsControl>().To<UserControlWeighings>();
         ninjectKernel.Bind<IStatisticsControl>().To<UserControlStatisticsTabs>();
         ninjectKernel.Bind<IFlocksControl>().To<UserControlFlocks>();
         ninjectKernel.Bind<IMaintenanceControl>().To<UserControlMaintenance>();
         ninjectKernel.Bind<Window>().To<MainWindow>();

         IocContainer.Container = new NinjectIocContainer(ninjectKernel);
      }

      /// <summary>
      /// Data directory and database initialization
      /// </summary>
      public static bool InitData(FormSplash splash)
      {
         // Priprava adresarove struktury - trva kratce, text ve splashi ani nezobrazuju
         PrepareDirectories();

         // Inicializuji databazi
         if (!InitDatabase(splash))
         {
            return false;
         }

         return true;
      }


      /// <summary>
      /// The main entry point for the application.
      /// </summary>    
      public static Window Main()
      {
         // Pokud uz program jednou bezi, nespoustim ho podruhe, ale prepnu na jiz spusteny
         if (SingleApplication.IsAlreadyRunning())
         {
            SingleApplication.SwitchToCurrentInstance();
            return null;
         }
         // Zobrazim splash

         var splash = new FormSplash();
         splash.Show();
         splash.Refresh(); // Aby se hned vykreslilo

         // init ninject, should be called at the very begining       
         InitIoc();

         try
         {
            // Inicializuji adresarovou strukturu a databazi
            if (!InitData(splash))
            {
               return null;
            }

            // Nactu nastaveni programu
            Setup = Database.LoadSetup();
         }       
         finally
         {
            splash.Close();
            splash.Dispose();
         }

         // Pokud zatim neni zvoleny zadny jazyk, necham uzivatele vybrat
         if (Setup.Language == SwLanguage.UNDEFINED)
         {
            new FormLanguage().ShowDialog(); // Bud vybere, nebo zustane UNDEFINED (vykresli se anglictina)
         }

         // Nastavim jazyk
         SwLanguageClass.SetLanguage(Setup.Language);
         System.Windows.Forms.Application.EnableVisualStyles();
         //Application.Run();       
         return IocContainer.Container.Get<Window>();              
      }
   }
}