﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Bat1 {
    public partial class CustomControlHistogramDataGridView : DataGridView {
        public CustomControlHistogramDataGridView() {
            InitializeComponent();
        }

        /// <summary>
        /// Increase bitmap brightness
        /// </summary>
        /// <param name="bitmap">Original bitmap</param>
        /// <returns>Newly created bitmap with higher brightness</returns>
        private Bitmap IncreaseBrightness(Bitmap bitmap) {
            var imageAttributes = new ImageAttributes();
            imageAttributes.SetGamma(0.1f, ColorAdjustType.Bitmap);
            var newBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            var g = Graphics.FromImage(newBitmap);
            g.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height,
                        GraphicsUnit.Pixel, imageAttributes);
            return newBitmap;
        }

        protected override void OnCellPainting(DataGridViewCellPaintingEventArgs e) {
            if (e.RowIndex < 0) {
                return;     // Hlavicku kreslim normalne
            }
            if (!Columns.Contains("ColumnHistogram")) {
                return;     // V tabulce neni sloupec s histogramem
            }
            if (e.ColumnIndex != Columns["ColumnHistogram"].Index) {
                return;     // Ostatni sloupce vykreslim normalne
            }
            if ((e.State & DataGridViewElementStates.Selected) != DataGridViewElementStates.Selected) {
                return;     // Vykresluje se histogram, ale bunka neni vybrana. Vykreslim histogram normalne.
            }

            // Pozadi bunky vykreslim normalne, vcetne zvyrazneni vybrane bunky
            e.PaintBackground(e.CellBounds, true);

            // U rucne zadanych vysledku histogram neni
            if (e.Value == null) {
                // Oznacim, ze jsem zpracoval event
                e.Handled = true;
                return;
            }

            // Zvysim jas histogramu
            var bitmap = IncreaseBrightness((Bitmap)e.Value);

            // Vykreslim zesvetleny histogram
            var y = e.CellBounds.Y + (e.CellBounds.Height - bitmap.Height) / 2;     // Umistim doprostred vysky radku
            if (bitmap.Width <= e.CellBounds.Width) {
                // Histogram se vleze cely, vykreslim jednoduse na stred bunky
                e.Graphics.DrawImage(bitmap, e.CellBounds.X + e.CellBounds.Width / 2 - bitmap.Width / 2, y);
            } else {
                // Histogram se nevejde cely, je treba ho oriznout (jinak se vykresli vlevo do sousedni bunky)
                var rectangle = new Rectangle(bitmap.Width / 2 - e.CellBounds.Width / 2, 0, e.CellBounds.Width, bitmap.Height);
                e.Graphics.DrawImage(bitmap, e.CellBounds.X, y, rectangle, GraphicsUnit.Pixel);
            }
            
            // Uvolnim kopii bitmapy
            bitmap.Dispose();
            
            // Oznacim, ze jsem zpracoval event
            e.Handled = true;
        }
    }
}
