﻿using Bat1Library;
using Bat2Library.Connection.Interface.Domain;
using BatLibrary;
using DataContext;

namespace Bat1.Forms.Bat2.Extensions
{
   public static class Bat2ScaleExtension
   {
      public static void MapScale(this Bat2Scale config, Bat2DeviceData device)
      {
         if (config == null)
         {
            return;
         }
         config.MapFromCountry(device);
         config.MapFromBat2Identification(device);
         config.MapFromDisplayConfiguration(device);
         config.MapFromEthernet(device);
         config.MapFromDacsOptions(device);
         config.MapFromMegaviOptions(device);
         config.MapFromModbusOptions(device);
         config.MapFromCellularData(device);
         config.MapFromWeightUnits(device);
         config.MapFromRs485Options(device);
         config.MapFromPlatformCalibration(device);
         config.MapFromDataPublication(device);
         config.MapFromWeighingConfiguration(device);
         config.GetListNames(device);
         config.MapFromGsmMessage(device);   
      }

      public static void MapFromCountry(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.Country = (Bat1Library.Country)dev.Configuration.Country.CountryCode;
         config.LanguageInDatabase = (ScaleLanguagesInDatabase)dev.Configuration.Country.Language;
         config.CodePage = dev.Configuration.Country.CodePage;

         config.DateFormat = (DateFormat)dev.Configuration.Country.DateFormat;
         config.DateSeparator1 = dev.Configuration.Country.DateSeparator1;
         config.DateSeparator2 = dev.Configuration.Country.DateSeparator2;

         config.TimeFormat = (TimeFormat)dev.Configuration.Country.TimeFormat;
         config.TimeSeparator = dev.Configuration.Country.TimeSeparator;
         config.DaylightSavingMode = (DaylightSavingMode)dev.Configuration.Country.DaylightSavingType;
      }

      public static void MapFromBat2Identification(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.SerialNumber = (int)dev.Configuration.VersionInfo.SerialNumber;
         config.ScaleName = dev.Configuration.DeviceInfo.Name;
         config.HwVersion.Major = dev.Configuration.VersionInfo.HardwareMajor;
         config.HwVersion.Minor = dev.Configuration.VersionInfo.HardwareMinor;
         config.HwVersion.Build = dev.Configuration.VersionInfo.HardwareBuild;
         config.SwVersion.Major = dev.Configuration.VersionInfo.SoftwareMajor;
         config.SwVersion.Minor = dev.Configuration.VersionInfo.SoftwareMinor;
         config.SwVersion.Build = dev.Configuration.VersionInfo.SoftwareBuild;
         config.Modification = dev.Configuration.VersionInfo.Modification;
         config.Class = dev.Configuration.VersionInfo.Class;
      }

      public static void MapFromDisplayConfiguration(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.Display.Contrast = dev.Configuration.DisplayConfiguration.Contrast;
         config.Display.Mode = (DisplayMode)dev.Configuration.DisplayConfiguration.Mode;
         config.Display.Backlight.Duration = dev.Configuration.DisplayConfiguration.BacklightDuration;
         config.Display.Backlight.Intensity = dev.Configuration.DisplayConfiguration.BacklightIntensity;
         config.Display.Backlight.Mode = (BacklightMode)dev.Configuration.DisplayConfiguration.BacklightMode;
         config.SavePower = dev.Configuration.DisplayConfiguration.SavePower;
      }

      public static void MapFromEthernet(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.Ethernet.Dhcp = dev.Configuration.Ethernet.Dhcp;
         config.Ethernet.Gateway = dev.Configuration.Ethernet.Gateway;
         config.Ethernet.Ip = dev.Configuration.Ethernet.Ip;
         config.Ethernet.PrimaryDns = dev.Configuration.Ethernet.PrimaryDns;
         config.Ethernet.SubnetMask = dev.Configuration.Ethernet.SubnetMask;
      }

      public static void MapFromDacsOptions(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.DacsOption.Address = dev.Configuration.DacsOptions.Address;
         config.DacsOption.DeviceAddress = dev.Configuration.DacsOptions.DeviceAddress;
         config.DacsOption.Version = dev.Configuration.DacsOptions.Version;
      }

      public static void MapFromMegaviOptions(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.MegaviOptions.Address = dev.Configuration.MegaviOptions.Address;
         config.MegaviOptions.DeviceAddress = dev.Configuration.MegaviOptions.DeviceAddress;
         config.MegaviOptions.Code = dev.Configuration.MegaviOptions.Code;
      }

      public static void MapFromModbusOptions(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.ModbusOptions.Address = dev.Configuration.ModbusOptions.Address;
         config.ModbusOptions.BaudRate = dev.Configuration.ModbusOptions.BaudRate;
         config.ModbusOptions.DataBits = dev.Configuration.ModbusOptions.DataBits;
         config.ModbusOptions.DeviceAddress = dev.Configuration.ModbusOptions.DeviceAddress;
         config.ModbusOptions.Mode = dev.Configuration.ModbusOptions.Mode;
         config.ModbusOptions.Parity = dev.Configuration.ModbusOptions.Parity;
         config.ModbusOptions.ReplyDelay = dev.Configuration.ModbusOptions.ReplyDelay;
      }

      public static void MapFromCellularData(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.CellularData.Apn = dev.Configuration.CellularData.Apn;
         config.CellularData.Password = dev.Configuration.CellularData.Password;
         config.CellularData.Username = dev.Configuration.CellularData.Username;
      }

      public static void MapFromWeightUnits(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.Units.Decimals = dev.Configuration.WeightUnits.Decimals;
         config.Units.Division = dev.Configuration.WeightUnits.Division;
         config.Units.MaxDivision = dev.Configuration.WeightUnits.DivisionMax;
         config.Units.Range = dev.Configuration.WeightUnits.Range;
         config.Units.Units = (Units)dev.Configuration.WeightUnits.Units;
      }

      public static void MapFromRs485Options(this Bat2Scale config, Bat2DeviceData dev)
      {
         //config.Rs485Optionses.Clear();
         //config.Rs485Optionses.Add(MapFromRs485(dev.Configuration.Rs485Options[0]));
         //config.Rs485Optionses.Add(MapFromRs485(dev.Configuration.Rs485Options[1]));
      }

      public static Rs485Options MapFromRs485(Rs485Options src)
      {
         var dst = new Rs485Options
         {
            Enabled = src.Enabled,
            Mode = src.Mode,
            PhysicalDeviceAddress = src.PhysicalDeviceAddress
         };
         return dst;
      }

      public static void MapFromPlatformCalibration(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.PlatformCalibration.Delay = dev.Configuration.PlatformCalibration.Delay;
         config.PlatformCalibration.Duration = dev.Configuration.PlatformCalibration.Duration;
         config.PlatformCalibration.Points = dev.Configuration.PlatformCalibration.Points;
      }

      public static void MapFromDataPublication(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.DataPublication.AccelerateFromDay = dev.Configuration.DataPublication.AccelerateFromDay;
         config.DataPublication.AcceleratedPeriod = dev.Configuration.DataPublication.AcceleratedPeriod;
         config.DataPublication.SendAt = dev.Configuration.DataPublication.SendAt;
         config.DataPublication.Period = dev.Configuration.DataPublication.Period;
         config.DataPublication.StartFromDay = dev.Configuration.DataPublication.StartFromDay;
         config.DataPublication.Interface = dev.Configuration.DataPublication.Interface;
         config.DataPublication.Password = dev.Configuration.DataPublication.Password;
         config.DataPublication.Url = dev.Configuration.DataPublication.Url;
         config.DataPublication.Username = dev.Configuration.DataPublication.Username;
      }

      public static void MapFromWeighingConfiguration(this Bat2Scale config, Bat2DeviceData dev)
      {
         config.WeighingConfiguration.Name = dev.Configuration.WeighingConfiguration.Name;
         config.WeighingConfiguration.Flock = dev.Configuration.WeighingConfiguration.Flock;
         config.WeighingConfiguration.MenuMask = dev.Configuration.WeighingConfiguration.MenuMask;
         config.WeighingConfiguration.InitialDay = dev.Configuration.WeighingConfiguration.InitialDay;
         config.WeighingConfiguration.CorrectionCurveIndex = dev.Configuration.WeighingConfiguration.CorrectionCurveIndex;
         config.WeighingConfiguration.PredefinedIndex = dev.Configuration.WeighingConfiguration.PredefinedIndex;
         config.WeighingConfiguration.DayStart = dev.Configuration.WeighingConfiguration.DayStart;
         config.WeighingConfiguration.MaleInitialWeight = dev.Configuration.WeighingConfiguration.MaleInitialWeight;
         config.WeighingConfiguration.MaleGrowthCurveIndex = dev.Configuration.WeighingConfiguration.MaleGrowthCurveIndex;        
         config.WeighingConfiguration.FemaleInitialWeight = dev.Configuration.WeighingConfiguration.FemaleInitialWeight;
         config.WeighingConfiguration.FemaleGrowthCurveIndex = dev.Configuration.WeighingConfiguration.FemaleGrowthCurveIndex;        
         config.WeighingConfiguration.AdjustTargetWeights = dev.Configuration.WeighingConfiguration.AdjustTargetWeights;
         config.WeighingConfiguration.Sex = dev.Configuration.WeighingConfiguration.Sex;
         config.WeighingConfiguration.SexDifferentiation = dev.Configuration.WeighingConfiguration.SexDifferentiation;
         config.WeighingConfiguration.Growth = dev.Configuration.WeighingConfiguration.Growth;
         config.WeighingConfiguration.Mode = dev.Configuration.WeighingConfiguration.Mode;
         config.WeighingConfiguration.Filter = dev.Configuration.WeighingConfiguration.Filter;
         config.WeighingConfiguration.StabilizationTime = dev.Configuration.WeighingConfiguration.StabilizationTime;
         config.WeighingConfiguration.StabilizationRange = dev.Configuration.WeighingConfiguration.StabilizationRange;
         config.WeighingConfiguration.Step = dev.Configuration.WeighingConfiguration.Step;
         config.WeighingConfiguration.MaleMarginAbove = dev.Configuration.WeighingConfiguration.MaleMarginAbove;
         config.WeighingConfiguration.MaleMarginBelow = dev.Configuration.WeighingConfiguration.MaleMarginBelow;
         config.WeighingConfiguration.FemaleMarginAbove = dev.Configuration.WeighingConfiguration.FemaleMarginAbove;
         config.WeighingConfiguration.FemaleMarginBelow = dev.Configuration.WeighingConfiguration.FemaleMarginBelow;
         config.WeighingConfiguration.ShortPeriod = dev.Configuration.WeighingConfiguration.ShortPeriod;
         config.WeighingConfiguration.ShortType = dev.Configuration.WeighingConfiguration.ShortType;
         config.WeighingConfiguration.UniformityRange = dev.Configuration.WeighingConfiguration.UniformityRange;
         config.WeighingConfiguration.HistogramStep = dev.Configuration.WeighingConfiguration.HistogramStep;
         config.WeighingConfiguration.HistogramRange = dev.Configuration.WeighingConfiguration.HistogramRange;
         config.WeighingConfiguration.HistogramMode = dev.Configuration.WeighingConfiguration.HistogramMode;
         config.WeighingConfiguration.Planning = dev.Configuration.WeighingConfiguration.Planning;
         config.WeighingConfiguration.WeighingPlanIndex = dev.Configuration.WeighingConfiguration.WeighingPlanIndex;
      }

      public static void GetListNames(this Bat2Scale config, Bat2DeviceData dev)
      {
         //// curves names and weighingPlans names
         //foreach (var curve in dev.CorrectionCurves)
         //{
         //   config.CorrectionCurvesNames.Add(curve.Name);
         //}
         //foreach (var curve in dev.GrowthCurves)
         //{
         //   config.GrowthCurvesNames.Add(curve.Name);
         //}
         //foreach (var plan in dev.WeighingPlans)
         //{
         //   config.WeighingPlansNames.Add(plan.Name);
         //}
      }

      public static void MapFromGsmMessage(this Bat2Scale config, Bat2DeviceData dev)
      {
         //config.GsmMessage.CommandsEnabled = dev.Configuration.GsmMessage.CommandsEnabled;
         //config.GsmMessage.CommandsCheckPhoneNumber = dev.Configuration.GsmMessage.CommandsCheckPhoneNumber;
         //config.GsmMessage.CommandsExpiration = dev.Configuration.GsmMessage.CommandsExpiration;
         //config.GsmMessage.EventMask = dev.Configuration.GsmMessage.EventMask;
         //config.GsmMessage.Mode = dev.Configuration.GsmMessage.Mode;
         //config.GsmMessage.SwitchOnPeriod = dev.Configuration.GsmMessage.SwitchOnPeriod;
         //config.GsmMessage.SwitchOnDuration = dev.Configuration.GsmMessage.SwitchOnDuration;
         //config.GsmMessage.SwitchOnTimes = dev.Configuration.GsmMessage.SwitchOnTimes;
      }
   }
}
