﻿using System;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using BatLibrary;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class StatisticsViewModel : ObservableObject
   {
      #region Private fields

      private const double DECIMAL_HIGH = 1000.0;
      private const double DECIMAL_LOW = 0.001;
      private byte uniformityRange;
      private byte shortPeriod;
      private StatisticShortTypeE shortType;
      private int histogramStep;
      private double histogramStepFormatted;
      private byte histogramRange;
      private HistogramModeE histogramMode;
      private WeightUnitsE units;
      private int stepMinimum;
      private double stepMaximum;
      private WeighingCapacity weighingCapacity;
       
      #endregion

      #region Public interface

      #region Constructors

      public StatisticsViewModel(IView view, WeighingConfiguration conf, Bat1Library.UnitsConfig unitsConfig)
      {       
         ShortPeriod = conf.ShortPeriod;
         ShortType = conf.ShortType;
         UniformityRange = conf.UniformityRange;
         HistogramStepFormatted = conf.HistogramStep;
         HistogramRange = conf.HistogramRange;
         HistogramMode = conf.HistogramMode;        
         Initialize(view, unitsConfig);
      }

      public StatisticsViewModel(IView view, Bat1Library.StatisticConfig conf, Bat1Library.UnitsConfig unitsConfig)
      {        
         UniformityRange = (byte)conf.UniformityRange;
         HistogramStepFormatted = conf.Histogram.Step;
         HistogramRange = (byte)conf.Histogram.Range;
         HistogramMode = (HistogramModeE)conf.Histogram.Mode;
         Initialize(view, unitsConfig);
      }

      private void Initialize(IView view, Bat1Library.UnitsConfig unitsConfig)
      {
         Units = (WeightUnitsE)unitsConfig.Units;
         WeighingCapacity = unitsConfig.WeighingCapacity;
         StepMinimum = 0;
         view.DataContext = this;
      }

      #endregion

      #region Properties

      public byte ShortPeriod { get { return shortPeriod; } set { SetProperty(ref shortPeriod, value); } }
      public StatisticShortTypeE ShortType { get { return shortType; } set { SetProperty(ref shortType, value); } }
      public byte UniformityRange { get { return uniformityRange; } set { SetProperty(ref uniformityRange, value); } }
      public int HistogramStep { get { return histogramStep; } set { SetProperty(ref histogramStep, value); } }
      public byte HistogramRange { get { return histogramRange; } set { SetProperty(ref histogramRange, value); } }
      public HistogramModeE HistogramMode { get { return histogramMode; } set { SetProperty(ref histogramMode, value); } }
      public int StepMinimum { get { return stepMinimum; } set { SetProperty(ref stepMinimum, value); } }
      public double StepMaximum { get { return stepMaximum; } set { SetProperty(ref stepMaximum, value); } }

      public double HistogramStepFormatted
      {
         get
         {
            return histogramStepFormatted;
         }
         set
         {          
            SetProperty(ref histogramStepFormatted, value);
            HistogramStep = (int)ConvertWeight.Convert(value, (Units)Units, BatLibrary.Units.G);
         }
      }

      public WeightUnitsE Units
      {
         get
         {
            return units;
         }
         set
         {
            var step = ConvertWeight.Convert(HistogramStepFormatted, (Units)units, (Units)value);
            SetProperty(ref units, value);

            if (Units == WeightUnitsE.WEIGHT_UNITS_G && step % 1 != 0)
            {
               step = Math.Round(step, 0);
            }

            HistogramStepFormatted = step;
            UpdateStepMaximum();
         }
      }

      public WeighingCapacity WeighingCapacity
      {
         get
         {
            return weighingCapacity;
         }
         set
         {
            SetProperty(ref weighingCapacity, value);
            UpdateStepMaximum();
         }
      }

      #endregion 

      #endregion

      #region Private helpers

      private void UpdateStepMaximum()
      {
         var max = 0.0;
         if (WeighingCapacity == WeighingCapacity.NORMAL)
         {
            switch (Units)
            {
               case WeightUnitsE.WEIGHT_UNITS_G:
                  max = WeightUnitsC.G_RANGE;
                  break;
               case WeightUnitsE.WEIGHT_UNITS_KG:
                  max = WeightUnitsC.KG_RANGE / DECIMAL_HIGH;
                  break;
               case WeightUnitsE.WEIGHT_UNITS_LB:
                  max = WeightUnitsC.LB_RANGE / DECIMAL_HIGH;
                  break;
            }
         }
         else if (WeighingCapacity == WeighingCapacity.EXTENDED)
         {
            switch (Units)
            {
               case WeightUnitsE.WEIGHT_UNITS_G:
                  max = WeightUnitsC.G_EXT_RANGE;
                  break;
               case WeightUnitsE.WEIGHT_UNITS_KG:
                  max = WeightUnitsC.KG_EXT_RANGE / DECIMAL_HIGH;
                  break;
               case WeightUnitsE.WEIGHT_UNITS_LB:
                  max = WeightUnitsC.LB_EXT_RANGE / DECIMAL_HIGH;
                  break;
            }
         }
         StepMaximum = max;
         
         // Step histogram validation
         if (HistogramStepFormatted > StepMaximum)
         {
            HistogramStepFormatted = StepMaximum;
         }
         if (HistogramStepFormatted < DECIMAL_LOW)
         {
            HistogramStepFormatted = DECIMAL_LOW;
         }
      }

      #endregion
   }
}
