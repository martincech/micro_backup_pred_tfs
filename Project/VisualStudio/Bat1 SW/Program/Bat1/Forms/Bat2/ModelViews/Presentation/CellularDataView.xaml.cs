﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat1.Forms.Bat2.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for CellularDataView.xaml
   /// </summary>
   public partial class CellularDataView : ICellularDataView
   {    
      public CellularDataView()
      {
         InitializeComponent();        
      }

      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface ICellularDataView : IView
   {
   }
}
