﻿namespace Bat1
{
   partial class UserControlScaleBat2
   {
      /// <summary> 
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary> 
      /// Required method for Designer support - do not modify 
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleBat2));
         this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
         this.flowLayoutPanelScaleCmd = new System.Windows.Forms.FlowLayoutPanel();
         this.buttonRead = new System.Windows.Forms.Button();
         this.buttonSetName = new System.Windows.Forms.Button();
         this.buttonSetDateTime = new System.Windows.Forms.Button();
         this.buttonSetParameters = new System.Windows.Forms.Button();
         this.flashButton = new System.Windows.Forms.Button();
         this.userControlScaleResults2 = new Bat1.UserControlScaleResults2();
         this.tableLayoutPanel.SuspendLayout();
         this.flowLayoutPanelScaleCmd.SuspendLayout();
         this.SuspendLayout();
         // 
         // tableLayoutPanel
         // 
         resources.ApplyResources(this.tableLayoutPanel, "tableLayoutPanel");
         this.tableLayoutPanel.Controls.Add(this.flowLayoutPanelScaleCmd, 0, 0);
         this.tableLayoutPanel.Controls.Add(this.userControlScaleResults2, 0, 1);
         this.tableLayoutPanel.Name = "tableLayoutPanel";
         // 
         // flowLayoutPanelScaleCmd
         // 
         resources.ApplyResources(this.flowLayoutPanelScaleCmd, "flowLayoutPanelScaleCmd");
         this.flowLayoutPanelScaleCmd.Controls.Add(this.buttonRead);
         this.flowLayoutPanelScaleCmd.Controls.Add(this.buttonSetName);
         this.flowLayoutPanelScaleCmd.Controls.Add(this.buttonSetDateTime);
         this.flowLayoutPanelScaleCmd.Controls.Add(this.buttonSetParameters);
         this.flowLayoutPanelScaleCmd.Controls.Add(this.flashButton);
         this.flowLayoutPanelScaleCmd.Name = "flowLayoutPanelScaleCmd";
         // 
         // buttonRead
         // 
         resources.ApplyResources(this.buttonRead, "buttonRead");
         this.buttonRead.Name = "buttonRead";
         this.buttonRead.UseVisualStyleBackColor = true;
         this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
         // 
         // buttonSetName
         // 
         resources.ApplyResources(this.buttonSetName, "buttonSetName");
         this.buttonSetName.Name = "buttonSetName";
         this.buttonSetName.UseVisualStyleBackColor = true;
         this.buttonSetName.Click += new System.EventHandler(this.buttonSetName_Click);
         // 
         // buttonSetDateTime
         // 
         resources.ApplyResources(this.buttonSetDateTime, "buttonSetDateTime");
         this.buttonSetDateTime.Name = "buttonSetDateTime";
         this.buttonSetDateTime.UseVisualStyleBackColor = true;
         this.buttonSetDateTime.Click += new System.EventHandler(this.buttonSetDateTime_Click);
         // 
         // buttonSetParameters
         // 
         resources.ApplyResources(this.buttonSetParameters, "buttonSetParameters");
         this.buttonSetParameters.Name = "buttonSetParameters";
         this.buttonSetParameters.UseVisualStyleBackColor = true;
         this.buttonSetParameters.Click += new System.EventHandler(this.buttonSetParameters_Click);
         // 
         // flashButton
         // 
         resources.ApplyResources(this.flashButton, "flashButton");
         this.flashButton.Name = "flashButton";
         this.flashButton.UseVisualStyleBackColor = true;
         this.flashButton.Click += new System.EventHandler(this.flashButton_Click);
         // 
         // userControlScaleResults2
         // 
         resources.ApplyResources(this.userControlScaleResults2, "userControlScaleResults2");
         this.userControlScaleResults2.Name = "userControlScaleResults2";
         // 
         // UserControlScaleBat2
         // 
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
         resources.ApplyResources(this, "$this");
         this.Controls.Add(this.tableLayoutPanel);
         this.Name = "UserControlScaleBat2";
         this.tableLayoutPanel.ResumeLayout(false);
         this.tableLayoutPanel.PerformLayout();
         this.flowLayoutPanelScaleCmd.ResumeLayout(false);
         this.flowLayoutPanelScaleCmd.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
      private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelScaleCmd;
      private System.Windows.Forms.Button buttonRead;
      private System.Windows.Forms.Button buttonSetDateTime;
      private System.Windows.Forms.Button buttonSetParameters;
      private UserControlScaleResults2 userControlScaleResults2;
      private System.Windows.Forms.Button flashButton;
      private System.Windows.Forms.Button buttonSetName;

   }
}
