﻿using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using Desktop.Wpf.Presentation;

namespace Bat1
{
   public partial class UserControlScale : UserControl, IScaleControl
   {
      public UserControlScale(
         IBat1ScaleControl userControlScaleBat1,
         IBat2ScaleControl userControlScaleBat2
         )
      {
         InitializeComponent();
         AddPage(userControlScaleBat1, scaleBat1);
         AddPage(userControlScaleBat2, scaleBat2);         
      }

      private static void AddPage(IView control, Control toControl)
      {
         Control pageControl;
         if (control is UserControl)
         {
            pageControl = control as UserControl;
         }
         else if (control is UIElement)
         {
            pageControl = new ElementHost {Child = control as UIElement};
         }
         else
         {
            return;
         }
         pageControl.Dock = DockStyle.Fill;
         toControl.Controls.Add(pageControl);
      }

      #region Implementation of IView

      /// <summary>
      /// Gets or sets the data context of the view.
      /// This will be set to ViewModel of the view.
      /// </summary>
      public object DataContext { get; set; }

      #endregion
   }

   public interface IScaleControl : IView
   {
   }
}
