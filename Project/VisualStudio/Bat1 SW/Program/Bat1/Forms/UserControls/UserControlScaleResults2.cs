using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Bat1.Exporting;
using Bat1.Properties;
using Bat1.Utilities;
using Bat1Library;
using Bat1Library.Statistics;
using Bat2Library.Connection.Interface.Domain;
using BatLibrary;
using DataContext;

namespace Bat1 
{
   public partial class UserControlScaleResults2 : UserControlResultsBase
   {
      #region Fields and properties

       public BindingList<Bat2Device> Devices { get; private set; }
       public BindingList<string> DevNames;  // name list of all devices
       
       /// <summary>
       /// Index of actual selected scale.
       /// </summary>
       public int ActualDevice
       {
          get{ return comboBoxScaleNames.SelectedIndex - 1; }        
       }

       private const string CONNECT = "Connected";
       private const string DISCONNECT = "Disconnected";
       private const int DAY_FROM = 0;
       private const int DAY_TO = 23;
       private const float TOLERANCE = 0.000001f;     

      private List<StatisticResult> statisticResults = new List<StatisticResult>();
      private List<Weighing> weighings = new List<Weighing>(); 

      private delegate void UpdateCallback(bool redrawArchive);

      #endregion

      #region Public interface

      #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public UserControlScaleResults2() 
        {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) 
            {
                return;         // V designeru z databaze nenahravam
            }
           
            // Zacinam plnit data do controlu
            isLoading = true;
                  
            ViewControls(false);
            comboBoxScaleNames.DataSource = null;
            Devices = new BindingList<Bat2Device>();
            Devices.ListChanged += Devices_ListChanged;
            comboBoxScaleNames.Items.Clear();  
            DevNames = new BindingList<string> { Properties.Resources.SELECT_SCALES_NONE };
            comboBoxScaleNames.DataSource = new BindingSource(DevNames, null);
            comboBoxScaleNames.SelectedIndex = 0;
         
            try
            {  // Nahraju z databaze naposledy pouzite jednotky - staci jednou pri startu programu               
               LoadUnits();
            } 
            finally 
            {
                isLoading = false;
            }        
        }

        #endregion
         
      /// <summary>
      /// Redraw actual device's properties.
      /// </summary>
      /// <param name="redrawArchive"></param>
      public void Update(bool redrawArchive = false)
      {
         if (this.InvokeRequired)   // Check if running thread is different than comboBoxScaleNames was created
         {
            UpdateCallback callback = Update;
            Invoke(callback, new object[] { redrawArchive });
         }
         else
         {
            if (comboBoxScaleNames.SelectedIndex <= 0)
            {
               return;
            }

            labelConnected.Text = GetConnectedStringFromBool(Devices[ActualDevice].IsConnected);
            if (redrawArchive)
            {
               RedrawArchive();
            }
         }
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Add new scale name to list.
      /// </summary>
      private void Devices_ListChanged(object sender, ListChangedEventArgs e)
      {
         if (e.ListChangedType == ListChangedType.ItemAdded)
         {
            var name = ((BindingList<Bat2Device>)sender).Last().Name;
            DevNames.Add(name);
         }
      }

        /// <summary>
        /// Redraw archive for actual selected scale.
        /// </summary>
        private void RedrawArchive()
        {
           isLoading = true;
           try
           {
              if (Devices[ActualDevice].Archive == null)
              {
                 return;
              }

              customControlHistogramDataGridViewWeighings.Rows.Clear();

              // Load weighings for actual scale             
              LoadStatistics();
              foreach (var item in Devices[ActualDevice].Archive)
              {
                 AddArchiveToDataGrid(item);
              }

              UpdateUnsaveItemsIndex();
           }
           finally
           {
              isLoading = false;
           }
        }

        /// <summary>
        /// Save unsave items' indexes and update button visibility for save these items.
        /// </summary>
        private void UpdateUnsaveItemsIndex()
        {
           if (customControlHistogramDataGridViewWeighings.Rows.Count == 0)
           {
              return;
           }

           var cellCount = customControlHistogramDataGridViewWeighings.Rows[0].Cells.Count;
           var unsaveIndex = (from DataGridViewRow row in customControlHistogramDataGridViewWeighings.Rows
                              where (bool)row.Cells[cellCount - 1].Value == false
                              select row.Index).ToList();

           buttonSaveUnsaved.Enabled = true;
           if (unsaveIndex.Count == 0)
           {
              buttonSaveUnsaved.Enabled = false;
           }
        }

        private void LoadStatistics()
        {
           var serialNumber = Devices[ActualDevice].SerialNumber.ToString();
           statisticResults.Clear();
           foreach (var info in Program.Database.ReadWeighingSearchInfo())
           {
              if (serialNumber.Equals(GetSerialNumberFromFileName(info.FileName)))
              {
                 statisticResults.Add(Program.Database.LoadScaleManualResults(info.Id));

                 //var weighingResult = new WeighingResults()
                 //weighings.Add(new Weighing());
              }
           }
        }

       private bool IsArchiveAndWeighingSame(int count, float average, float sigma, float uniformity, DateTime timeStamp)
       {
          return statisticResults.Any(stat =>
             stat.Count == count &&
             Math.Abs(stat.Average - average) < TOLERANCE &&
             Math.Abs(stat.Sigma - sigma) < TOLERANCE &&
             Math.Abs(stat.Uniformity - uniformity) < TOLERANCE &&
             DateTime.Compare(stat.WeighingStarted, timeStamp) == 0);       
       }

      private void AddArchiveToDataGrid(ArchiveItem archive)
      {
         // Only daily record is view.
         if (archive.HourFrom != DAY_FROM && archive.HourTo != DAY_TO)
         {
            return;
         }

         var avg = ConvertWeight.Convert(archive.Average, Units.G, (Units)comboBoxUnits.SelectedIndex);
         var sigma = ConvertWeight.Convert(archive.Sigma, Units.G, (Units)comboBoxUnits.SelectedIndex);
         var gain = ConvertWeight.Convert(archive.Gain, Units.G, (Units)comboBoxUnits.SelectedIndex);
         var targetWeight = ConvertWeight.Convert(archive.TargetWeight, Units.G, (Units)comboBoxUnits.SelectedIndex);
         var targetWeightFemale = ConvertWeight.Convert(archive.TargetWeightFemale, Units.G, (Units)comboBoxUnits.SelectedIndex);

         // if archive is already saved, checked column in table
         var isChecked = IsArchiveAndWeighingSame(archive.Count, archive.Average, archive.Sigma, archive.Uniformity, archive.Timestamp);        

         // Vytvorim nahled histogramu
         var arr = archive.Histogram.Select(b => (int) b).ToArray();
         var bitmap = new Bitmap(HistogramMiniGraph.GetTotalWidth(archive.Histogram.Count()), customControlHistogramDataGridViewWeighings.RowTemplate.Height - 3);
         HistogramMiniGraph.Draw(arr, Graphics.FromImage(bitmap), bitmap.Height - 1, 1, true);

         // Pridam radek do tabulky               
         var index = customControlHistogramDataGridViewWeighings.Rows.Add(archive.Timestamp.ToString("G"),                                   // Cas vcetne sekund
                                                                           archive.Count.ToString("N0"),                                     // N0 oddeli tisicovky
                                                                           archive.Sex,                                                      // Pohlavi
                                                                           DisplayFormat.RoundWeight(avg, (Units)comboBoxUnits.SelectedIndex) + " " + ((Units)comboBoxUnits.SelectedIndex).ToLocalizedString(),   // Prumer
                                                                           DisplayFormat.Uniformity(Statistics.ConvertUniformity(archive.Uniformity)) + " %",      // Uniformita
                                                                           DisplayFormat.RoundWeight(sigma, (Units)comboBoxUnits.SelectedIndex) + " " + ((Units)comboBoxUnits.SelectedIndex).ToLocalizedString(),
                                                                           DisplayFormat.RoundWeight(gain, (Units)comboBoxUnits.SelectedIndex) + " " + ((Units)comboBoxUnits.SelectedIndex).ToLocalizedString(),
                                                                           DisplayFormat.RoundWeight(targetWeight, (Units)comboBoxUnits.SelectedIndex) + " " + ((Units)comboBoxUnits.SelectedIndex).ToLocalizedString(),
                                                                           DisplayFormat.RoundWeight(targetWeightFemale, (Units)comboBoxUnits.SelectedIndex) + " " + ((Units)comboBoxUnits.SelectedIndex).ToLocalizedString(),
                                                                           bitmap,                                                           // Histogram
                                                                           isChecked
                                                                           );
      }

        /// <summary>
        /// Hide or visible controls when choose scale.
        /// </summary>
        /// <param name="option">true - visible controls, false - hide</param>
        private void ViewControls(bool option)
        {
           var conn = "";
           if (option)
           {
              var selDevConnected = Devices[ActualDevice].IsConnected;
              conn = GetConnectedStringFromBool(selDevConnected);
           }
           labelConnected.Text = conn;
           customControlHistogramDataGridViewWeighings.Visible = option;
           label2.Visible = option;          
           flowLayoutPanel1.Visible = option;
           labelUnits.Visible = option;
           comboBoxUnits.Visible = option;
        }

        /// <summary>
        /// Load last used units from database
        /// </summary>
        private void LoadUnits() 
        {
            comboBoxUnits.SelectedIndex = (int)Program.Setup.ScaleVersion6Units;
        }

        /// <summary>
        /// Save selected units to database
        /// </summary>
        private void SaveUnits() 
        {
            Program.Setup.ScaleVersion6Units = (Units)comboBoxUnits.SelectedIndex;
            Program.Database.SaveSetup();
        }
    
        private string GetConnectedStringFromBool(bool flag)
        {
           return flag ? CONNECT : DISCONNECT;
        }

       #region Events

        private void comboBoxUnits_SelectionChangeCommitted(object sender, EventArgs e)
        {   // Ulozim si vybrane jednotky
            SaveUnits();         
        }
     
        private void UserControlScaleResults2_Load(object sender, EventArgs e)
        {   // Prehodim focus na tabulku
            customControlHistogramDataGridViewWeighings.Focus();
        }

        private void buttonSaveUnsaved_Click(object sender, EventArgs e)
        {
           var cellCount = customControlHistogramDataGridViewWeighings.Rows[0].Cells.Count;
           var unsaveIndex = (from DataGridViewRow row in customControlHistogramDataGridViewWeighings.Rows
                              where (bool)row.Cells[cellCount - 1].Value == false
                              select row.Index).ToList();

           var tmpArchive = new List<ArchiveItem>(Devices[ActualDevice].Archive);
           var archive = unsaveIndex.Select(index => tmpArchive[index]).ToList();
           SaveArchive(archive);

           // edit checkboxes
           foreach (var index in unsaveIndex)
           {
              customControlHistogramDataGridViewWeighings.Rows[index]
                 .Cells[cellCount - 1].Value = true;
           }

           buttonSaveUnsaved.Enabled = false;
        }

        private void buttonSaveAll_Click(object sender, EventArgs e) 
        {
           SaveArchive(Devices[ActualDevice].Archive);

           // edit checkboxes
           var cellCount = customControlHistogramDataGridViewWeighings.Rows[0].Cells.Count;
           foreach (DataGridViewRow row in customControlHistogramDataGridViewWeighings.Rows)
           {
              row.Cells[cellCount - 1].Value = true;
           }

           buttonSaveUnsaved.Enabled = false;
        }

        private void buttonSaveSelected_Click(object sender, EventArgs e)
        {          
           var tmpArchive = new List<ArchiveItem>(Devices[ActualDevice].Archive);
           var archive = (from DataGridViewRow row in customControlHistogramDataGridViewWeighings.SelectedRows 
                                        select tmpArchive[row.Index]).ToList();

           SaveArchive(archive);

           // edit checkboxes
           var cellCount = customControlHistogramDataGridViewWeighings.Rows[0].Cells.Count;
           foreach (DataGridViewRow row in customControlHistogramDataGridViewWeighings.SelectedRows)
           {
              row.Cells[cellCount - 1].Value = true;
           }

           UpdateUnsaveItemsIndex();
        }           

       private string GetSerialNumberFromFileName(string name)
       {
          var str = name.Split('_');
          return str.First();
       }

       private void SaveArchive(IEnumerable<ArchiveItem> archive)
       {
          Cursor.Current = Cursors.WaitCursor;
          var anySave = false;
          try
          {
             if (comboBoxScaleNames.SelectedIndex == 0)
             { //none scale is selected
                return;
             }

             anySave = SaveArchiveToDatabase(archive);        
          }
          finally
          {
             Cursor.Current = DefaultCursor;
             if (anySave)
             {
                MessageBox.Show(Resources.WEIGHINGS_SAVED, Program.ApplicationName);
             }
          }
       }

       private bool SaveArchiveToDatabase(IEnumerable<ArchiveItem> archive)
       {
          var anySave = false;
          var scaleName = Devices[ActualDevice].Name;
          var units = (Units)comboBoxUnits.SelectedIndex;
          var sn = Devices[ActualDevice].SerialNumber.ToString();
          var file = new NameNote(sn, "");
          var resultList = new List<StatisticResult>();

          foreach (var archiveItem in archive)
          {
             var write = true;
             var startDateTime = archiveItem.Timestamp;
             var cv = Statistics.GetCv(archiveItem.Average, archiveItem.Sigma);
             var flag = Flag.MALE;

             if (archiveItem.Sex == Bat2Library.SexE.SEX_FEMALE)
             {
                flag = Flag.FEMALE;
             }

             Histogram histogram = null;
             if (archiveItem.Histogram != null)
             {
                histogram = new Histogram(archiveItem.Histogram.Count())
                {
                   Counts = archiveItem.Histogram.Select(h => (int) h).ToArray()
                };
             }

             var result = new StatisticResult(flag, archiveItem.Count, 
                       (float)ConvertWeight.Convert(archiveItem.Average, Units.G, units),
                       (float)ConvertWeight.Convert(archiveItem.Sigma, Units.G, units), cv, 
                       Statistics.ConvertUniformity(archiveItem.Uniformity), archiveItem.Timestamp, 
                       (float)ConvertWeight.Convert(archiveItem.Gain, Units.G, units), histogram);

             foreach (var info in Program.Database.ReadWeighingSearchInfo()
                                          .Where(info => sn.Equals(GetSerialNumberFromFileName(info.FileName)) &&
                                                         DateTime.Compare(archiveItem.Timestamp, info.MinDateTime) == 0))
             {
                var dialogResult = CanSaveArchive(info.FileName, info.MinDateTime);
                switch (dialogResult)
                {
                   case DialogResult.No:     // user won't to rewrite weighing, but he wants to continue in saving others files
                      write = false;
                      continue;              
                   case DialogResult.Cancel: // weighing exist, but user want cancel all saving
                      return anySave;        
                   case DialogResult.OK:     // weighing exist -> update                     
                      Program.Database.UpdateManualResult(info.Id, result, Devices[ActualDevice].HistogramStep);
                      anySave = true;
                      write = false;
                      continue;
                }
             }

             if (write)
             {
                resultList.Add(result);
                Program.Database.SaveWeighing(scaleName, file, startDateTime, units, resultList, null, Devices[ActualDevice].HistogramStep, ScaleType.SCALE_BAT2);
                anySave = true;
             }
          }
          return anySave;
       }

        private void buttonExportSelected_Click(object sender, EventArgs e) 
        {
           var archive = new List<ArchiveItem>(Devices[ActualDevice].Archive);
           if (archive.Count == 0)
           {
              return;
           }

           // List of archive which I want to export
           var selectedArchiveList = 
               archive.Where((t, i) => customControlHistogramDataGridViewWeighings.Rows[i].Selected).ToList();

           if (selectedArchiveList.Count == 0)
           {
              return;
           }

           var name = Devices[ActualDevice].Name;
           new Export(this).ExportArchiveList(selectedArchiveList, true, name, (Units)comboBoxUnits.SelectedIndex);
        }

        private void buttonExportAll_Click(object sender, EventArgs e) 
        {
           var archive = new List<ArchiveItem>(Devices[ActualDevice].Archive);
           if (archive.Count == 0)
           {
              return;
           }

           var name = Devices[ActualDevice].Name;
           new Export(this).ExportArchiveList(archive, true, name, (Units)comboBoxUnits.SelectedIndex);          
        }

        /// <summary>
        /// Selected scale changed.
        /// </summary>
        private void comboBoxScaleNames_SelectedIndexChanged(object sender, EventArgs e)
        {
           var option = false;
           if (comboBoxScaleNames.SelectedIndex != 0)
           {
              option = true;
              labelConnected.Text = GetConnectedStringFromBool(
                           Devices[ActualDevice].IsConnected);
              RedrawArchive();             
           }

           ViewControls(option);      
        }           

        /// <summary>
        /// Change units in comboBox. Redraw archive items with new selected units.
        /// </summary>
        private void comboBoxUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
           if (comboBoxScaleNames.SelectedIndex <= 0) return;
           if (Devices[ActualDevice].Archive != null)
           {
              RedrawArchive();
           }              
        }

       /// <summary>
       /// Update checkbox for save in database.
       /// </summary>    
       private void tableLayoutPanel_VisibleChanged(object sender, EventArgs e)
       {
          if (ActualDevice >= 0)
          {
             RedrawArchive();
          }
       }

      #endregion

      #endregion
   }
}
