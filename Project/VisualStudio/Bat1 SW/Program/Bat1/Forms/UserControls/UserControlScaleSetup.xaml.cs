﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Bat1.Exporting;
using Bat1.Forms.Bat2.Extensions;
using Bat2Library.Connection.Interface.Domain;
using DataContext;
using Cursors = System.Windows.Input.Cursors;
using MessageBox = System.Windows.MessageBox;
using UserControl = System.Windows.Controls.UserControl;
using Desktop.Wpf.Presentation;

namespace Bat1.Forms.UserControls
{
   /// <summary>
   /// Interaction logic for UserControlScaleSetup.xaml
   /// </summary>
   public partial class UserControlScaleSetup : UserControl
   {
      #region Private fields

      private bool isBat1;

      /// <summary>
      /// Name of currently open predefined config
      /// </summary>
      private string openConfigName = "";

      /// <summary>
      /// Waiting form
      /// </summary>
      private FormScaleWait formScaleWait;

      /// <summary>
      /// User control with scale config
      /// </summary>
      private UserControlScaleConfig userControlScaleConfig;

      /// <summary>
      /// Title of the parent form
      /// </summary>
      private readonly string formTitle;

      private Bat2DeviceData device;
      private Bat1Scale scaleBat1;
      private Bat2Scale scaleBat2;

      #endregion

      #region Public interface

      #region Constructors

      public UserControlScaleSetup(string formTitle)
      {  // Bat1
         InitializeComponent();
         isBat1 = true;

         // Vytvorim user control s configem      
         const bool readOnly = false;
         SetBat1Config();

         var setup = new ScaleSetup(scaleBat1, readOnly);
         SetConfig(setup.Configuration, setup.Settings);
         // Ulozim si nazev okna (nazev ojan budu nasledne menit podle otevreneho configu)
         this.formTitle = formTitle;
      }

      public UserControlScaleSetup(string formTitle, Bat2DeviceData device)
      {  // Bat2
         InitializeComponent();
         isBat1 = false;

         this.device = device;
         SetBat2Config();

         var setup = new ScaleSetup(scaleBat2, device);
         SetConfig(setup.Configuration, setup.Settings);
         // Ulozim si nazev okna (nazev okna budu nasledne menit podle otevreneho configu)
         this.formTitle = formTitle;
      }

      #endregion

      public void SetBat2Config()
      {
         // Nastaveni vah
         if (scaleBat2 == null)
         {
            scaleBat2 = new Bat2Scale();
         }

         scaleBat2.MapScale(device);                      
      }

      public void SetBat1Config()
      {
         var config = new Bat1Scale(Bat1Version7.DefaultConfig);
         // Soubor FILE000 po volbe Restore factory defaults, zaroven ho nastavim jako aktivni
         config.FileList.Add("FILE000", "", config);
         config.ActiveFileIndex = 0;

         this.scaleBat1 = Program.Database.LoadBat1Config(config);
      }

      #endregion

      #region Private Methods

      private void ButtonNew_OnClick(object sender, RoutedEventArgs e)
      {
         if (!isBat1)
         {
            throw new System.NotImplementedException();
         }

         // Zeptam se na ulozeni zmen
         if (!IsChangesSaved())
         {
            return;
         }

         // Udelam kopii default nastaveni
         SetScaleDefaultConfig();
         SetOpenConfigName("");
      }

      private void ButtonOpenTemplate_OnClick(object sender, RoutedEventArgs e)
      {
         if (!isBat1)
         {
            throw new System.NotImplementedException();
         }

         // Zeptam se na ulozeni zmen
         if (!IsChangesSaved())
         {
            return;
         }

         // Zeptam se na config, ktery chce otevrit
         FormOpenScaleConfig form = new FormOpenScaleConfig();
         if (form.ShowDialog() != DialogResult.OK)
         {
            return;
         }

         // Otevru vybrany config
         userControlScaleConfig.SetScaleConfig((Bat1Scale)Program.Database.LoadScaleConfig(Program.Database.GetPredefinedScaleConfigId(form.SelectedConfigName)));

         // Zapamatuju si jmeno otevreneho configu
         SetOpenConfigName(form.SelectedConfigName);
      }

      private void ButtonSaveTemplate_OnClick(object sender, RoutedEventArgs e)
      {
         if (!isBat1)
         {
            throw new System.NotImplementedException();
         }

         // Zeptam se na jmeno
         FormName form = new FormName(ButtonSaveTemplate.Content.ToString(), openConfigName, false);
         if (form.ShowDialog() != DialogResult.OK)
         {
            return;
         }
         string name = form.EnteredName;

         // Zjistim, zda uz jmeno neexistuje
         if (Program.Database.ScaleConfigExists(name))
         {
            if (MessageBox.Show(String.Format(Properties.Resources.TEMPLATE_ALREADY_EXISTS, name), ButtonSaveTemplate.Content.ToString(), MessageBoxButton.YesNo) != MessageBoxResult.Yes)
            {
               return;         // Nechce prepsat
            }

            // Smazu existujici sablonu
            Program.Database.DeleteScaleConfig(name);
         }

         // Ulozim do DB
         Program.Database.SaveScaleConfig(userControlScaleConfig.ScaleBat1Config, name);

         // Zapamatuju si jmeno
         SetOpenConfigName(form.EnteredName);
      }

      private void ButtonLoadFromScale_OnClick(object sender, RoutedEventArgs e)
      {
         if (!isBat1)
         {
            throw new System.NotImplementedException();
         }

         // Zeptam se na ulozeni zmen
         if (!IsChangesSaved())
         {
            return;
         }

         Bat1Scale scaleConfig;
         if (!LoadScaleConfig(out scaleConfig))
         {
            return;
         }
         userControlScaleConfig.SetScaleConfig(scaleConfig);
         SetOpenConfigName("");
      }

      private void ButtonSaveToScale_OnClick(object sender, RoutedEventArgs e)
      {
         if (!isBat1)
         {
            throw new System.NotImplementedException();
         }

         // Zapsanim configu se smazou vsechny vzorky. Zeptam se, zda chce pkracovat
         if (MessageBox.Show(Properties.Resources.ALL_DATA_WILL_BE_DELETED, ButtonSaveToScale.Content.ToString(), MessageBoxButton.YesNo) != MessageBoxResult.Yes)
         {
            return;
         }

         Mouse.OverrideCursor = Cursors.Wait;
         //Refresh();
         ShowWaitingForm();

         try
         {
            // Nactu aktualni nastaveni z vahy
            Bat1Version7 bat1;
            if (!LoadConfig(out bat1))
            {
               return;
            }

            // Zmenim v nastaveni veskera data (ponecham cislo verze atd)
            bat1.WriteConfig(userControlScaleConfig.ScaleBat1Config);

            // Ulozim zpet do vahy
            SaveDevice(bat1);
         }
         finally
         {
            formScaleWait.Close();
            Mouse.OverrideCursor = null;
         }
      }

      private void ButtonImport_OnClick(object sender, RoutedEventArgs e)
      {
         if (!isBat1)
         {
            throw new System.NotImplementedException();
         }

         // Zeptam se na ulozeni zmen
         if (!IsChangesSaved())
         {
            return;
         }

         // Importuju
         Bat1Scale scaleConfig = new Export(null).ImportScaleConfig();
         if (scaleConfig == null)
         {
            return;
         }

         // Nastavim jako aktivni
         userControlScaleConfig.SetScaleConfig(scaleConfig);
         SetOpenConfigName("");
      }

      private void ButtonExport_OnClick(object sender, RoutedEventArgs e)
      {
         if (!isBat1)
         {
            throw new System.NotImplementedException();
         }

         // Exportuju, pokud ma otevreny preddefinovany config, ulozim i jeho nazev
         //new Export(this).ExportScaleConfig(userControlScaleConfig.ScaleConfig, openConfigName);
         new Export(null).ExportScaleConfig(userControlScaleConfig.ScaleBat1Config, openConfigName);
      }

      #endregion

      #region Bat1 methods

      /// <summary>
      /// Sets name of openned config
      /// </summary>
      /// <param name="configName">New name</param>
      private void SetOpenConfigName(string configName)
      {
         //openConfigName = configName;
         //if (configName == "")
         //{
         //   ParentForm.Text = formTitle;
         //}
         //else
         //{
         //   ParentForm.Text = formTitle + " (" + configName + ")";
         //}
      }

      public void SetScaleDefaultConfig()
      {
         // Nastaveni vah
         Bat1Scale config = new Bat1Scale(Bat1Version7.DefaultConfig);

         // Soubor FILE000 po volbe Restore factory defaults, zaroven ho nastavim jako aktivni
         config.FileList.Add("FILE000", "", config);
         config.ActiveFileIndex = 0;

         userControlScaleConfig.SetScaleConfig(config);
      }

      /// <summary>
      /// Load config from the scale
      /// </summary>
      /// <param name="bat1">Bat1Version7 instance to read to</param>
      /// <returns>True if successful</returns>
      public static bool LoadConfig(out Bat1Version7 bat1)
      {
         try
         {
            bat1 = new Bat1Version7();
         }
         catch
         {
            // Driver vahy neni nainstalovan
            Mouse.OverrideCursor = null;
            MessageBox.Show(Properties.Resources.DRIVER_NOT_INSTALLED, Program.ApplicationName);
            bat1 = null;
            return false;
         }

         // Nactu nastaveni z vahy
         if (!bat1.LoadConfig())
         {
            Mouse.OverrideCursor = null;
            MessageBox.Show(Properties.Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
            return false;
         }

         // Zkontroluju, zda vaha neni novejsi nez SW
         if (!bat1.IsSupported())
         {
            MessageBox.Show(Properties.Resources.SCALE_NOT_SUPPORTED, Program.ApplicationName);
            return false;
         }

         return true;
      }

      private void ShowWaitingForm()
      {
         //TODO
         //formScaleWait = new FormScaleWait(ParentForm);
         formScaleWait = new FormScaleWait(null);
         formScaleWait.Show();
         formScaleWait.Refresh();
      }

      private bool LoadScaleConfig(out Bat1Scale scaleConfig)
      {
         Mouse.OverrideCursor = Cursors.Wait;
         ShowWaitingForm();

         scaleConfig = null;
         try
         {
            Bat1Version7 bat1;

            if (!LoadConfig(out bat1))
            {
               return false;
            }

            scaleConfig = Bat1Version7.ReadGlobalConfig();

            return true;
         }
         finally
         {
            formScaleWait.Close();
            Mouse.OverrideCursor = null;
         }
      }


      /// <summary>
      /// Save all data to the scale
      /// </summary>
      /// <param name="bat1">Bat1 to use</param>
      /// <returns>True if successful</returns>
      private bool SaveDevice(Bat1Version7 bat1)
      {
         // Ulozim do vahy, pokud se delaly zmeny v souborech a skupinach, smazou se vsechny vzorky ve vaze
         if (!bat1.SaveDevice())
         {
            Mouse.OverrideCursor = null;
            MessageBox.Show(Properties.Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
            return false;
         }

         return true;
      }

      private bool IsChangesSaved()
      {
         switch (MessageBox.Show(Properties.Resources.SAVE_CHANGES_QUESTION, Program.ApplicationName, MessageBoxButton.YesNoCancel))
         {
            case MessageBoxResult.Yes:
               ButtonSaveTemplate_OnClick(null, null);
               return true;

            case MessageBoxResult.No:
               return true;

            default:
               return false;
         }
      }

      /// <summary>
      /// Initialize scale configuration.
      /// </summary>
      /// <param name="configuration">list of configuration</param>
      /// <param name="settings">list of services settings</param>
      private void SetConfig(IEnumerable<IView> configuration, IEnumerable<IView> settings)
      {
         userControlScaleConfig = new UserControlScaleConfig(configuration, settings)
         {          
            TabIndex = 0
         };
         GridContent.Children.Add(userControlScaleConfig);

         if (!isBat1)
         {
            userControlScaleConfig.SetScaleConfig(scaleBat2);
         }
      }

      #endregion
   }
}
