﻿// Spusteni prohlizece

using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using DataContext;

namespace Bat1 {
    public partial class UserControlAbout : UserControl {
        public UserControlAbout() {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            // Zobrazim jmeno aplikace a verzi
            labelApplicationName.Text = Program.ApplicationName;
            labelVersion.Text         = DatabaseVersion.ToString();

            // Zobrazim informace o firme
            labelCompanyName.Text    = AppConfig.CompanyName;
            labelCompanyAddress.Text = AppConfig.CompanyAddress;

            // Zobrazim mail a web
            linkLabelMail.Text = AppConfig.CompanyMail;
            linkLabelWeb.Text  = AppConfig.CompanyWeb;

            // Nahraju logo
            try {
                //pictureBoxLogo.Load("Logo.png");
               var logo = Properties.Resources.ResourceManager.GetObject("Logo");
               pictureBoxLogo.Image = (System.Drawing.Image)logo;
            } catch {
                // Soubor neexistuje, nic se nedeje
            }
        }

        private void linkLabelMail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            // Otevru mailoveho klienta
            Process.Start("mailto:" + AppConfig.CompanyMail + "?subject=" + AppConfig.CompanyMailSubject + " " + DatabaseVersion.ToString());        
        }

        private void linkLabelWeb_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            // Spustim webovy prohlizec
            var startInfo = new ProcessStartInfo(AppConfig.CompanyWebAddress);
            try {
                // Pomoci try-catch osetrim pripad, kdy spusteni nepovoli napr. firewall
                Process.Start(startInfo);
            } catch {
            }
        }
    }
}
