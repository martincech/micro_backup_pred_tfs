﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Bat1.Exporting;
using Bat1.Properties;
using Bat1Library;
using DataContext;
using Desktop.Wpf.Presentation;
using MessageBox = System.Windows.Forms.MessageBox;

namespace Bat1
{
   public partial class UserControlWeighings : UserControl, IWeighingsControl
   {
      /// <summary>
      /// Filtering of available weighings
      /// </summary>
      private WeighingFilter weighingFilter;

      /// <summary>
      /// Selected weighing
      /// </summary>
      public Weighing SelectedWeighing
      {
         get { return selectedWeighing; }
      }

      private Weighing selectedWeighing;

      private readonly bool selectOneWeighing;

      public UserControlWeighings(bool selectOneWeighing = false)
      {
         InitializeComponent();

         buttonAdd.Visible = !selectOneWeighing;
         buttonEdit.Visible = !selectOneWeighing;
         buttonDelete.Visible = !selectOneWeighing;
         buttonImport.Visible = !selectOneWeighing;
         buttonExportAll.Visible = !selectOneWeighing;
         buttonExportSelected.Visible = !selectOneWeighing;
         buttonOk.Visible = selectOneWeighing;
         buttonCancel.Visible = selectOneWeighing;
         dataGridViewWeighings.MultiSelect = !selectOneWeighing;

         this.selectOneWeighing = selectOneWeighing;        
      }

      private void SelectLastRow()
      {
         if (dataGridViewWeighings.Rows.Count <= 1)
         {
            return; // Bud zadny nebo 1 radek, nedelam nic
         }
         var index = dataGridViewWeighings.Rows.Count - 1;
         dataGridViewWeighings.FirstDisplayedScrollingRowIndex = index;
         dataGridViewWeighings.Refresh(); // Prekreslim s vybranym radkem na prvni pozici
         dataGridViewWeighings.CurrentCell = dataGridViewWeighings.Rows[index].Cells[0];
         dataGridViewWeighings.Rows[index].Selected = true;
      }

      /// <summary>
      /// Available list changed event handler
      /// </summary>
      /// <param name="sender"></param>
      private void AvailableChangedEventHandler(object sender)
      {      
         // Pocet vazeni v seznamu
         labelAvailableWeighings.Text = string.Format(Resources.SELECT_WEIGHINGS_AVAILABLE + " ({0:N0}):",
            weighingFilter.WeighingInfoFilter.FilesList.Count);

         // Prejdu na posledni (nejnovejsi) zaznam
         SelectLastRow();
      }

      /// <summary>
      /// Return currently selected weighing
      /// </summary>
      /// <returns>Weighing</returns>
      private Weighing GetFirstSelectedWeighing()
      {
         if (dataGridViewWeighings.Rows.Count == 0)
         {
            return null; // Tabulka je prazdna
         }

         if (dataGridViewWeighings.SelectedRows.Count == 0)
         {
            return null;
         }

         return
            Program.Database.LoadWeighing(
               weighingFilter.WeighingInfoFilter.FilteredInfoList[dataGridViewWeighings.SelectedRows[0].Index].Id);
      }

      private void Edit()
      {
         var weighing = GetFirstSelectedWeighing();
         if (weighing == null)
         {
            return; // Tabulka je prazdna
         }

         if (weighing.WeighingData.SampleList == null)
         {            
            // Rucni vazeni
            var formManual = new FormManualResults(weighing, weighing.ScaleType);
            formManual.ShowDialog();
         }
         else
         {
            // Vazeni z vahy

            // Predani 12tis vzorku trva, dam presypaci hodiny
            FormEditWeighing form;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
               // Nahraju z databaze seznam souboru
               var fileList = new NameNoteUniqueList();
               foreach (var file in Program.Database.LoadFileNameNoteList())
               {
                  fileList.Add(file);
               }

               // Predam data oknu
               form = new FormEditWeighing(weighing.WeighingData, fileList, weighing.WeighingData.ScaleConfig, false,
                  true);
            }
            finally
            {
               Cursor.Current = Cursors.Default;
            }
            if (form.ShowDialog() != DialogResult.OK)
            {
               return;
            }

            // Updatuju vazeni. Teoreticky tak muze vzniknout duplicitni vazeni, ale to neresim.
            Program.Database.UpdateWeighing(weighing.WeighingData.Id,
               weighing.WeighingData.File.Name,
               weighing.WeighingData.File.Note,
               weighing.WeighingData.Note,
               weighing.WeighingData.SampleList,
               weighing.WeighingData.ScaleConfig);
         }

         // Ulozim si vybrany radek
         var selectedRow = dataGridViewWeighings.SelectedRows[0].Index;

         // Zapamatuju si prave vybrany soubor a vahu. U rucne zadaneho vazeni muze editovat i jmeno vahy,
         // takze musim oboje. Datumy nemusim, ty zustanou.
         var selectedFileName = "";
         if (comboBoxFile.SelectedIndex > 0)
         {
            selectedFileName = comboBoxFile.Text;
         }
         var selectedScaleName = "";
         if (comboBoxScale.SelectedIndex > 0)
         {
            selectedScaleName = comboBoxScale.Text;
         }

         // Nactu novy seznam souboru a vah do comboboxu (stacil by jen seznam souboru)
         weighingFilter.LoadWeighings();

         // Obnovim puvodne vybrany soubor a vahu
         var fileIndex = comboBoxFile.Items.IndexOf(selectedFileName);
         if (fileIndex < 0)
         {
            comboBoxFile.SelectedIndex = 0; // Vyberu all
         }
         else
         {
            comboBoxFile.SelectedIndex = fileIndex;
         }
         var scaleIndex = comboBoxScale.Items.IndexOf(selectedScaleName);
         if (scaleIndex < 0)
         {
            comboBoxScale.SelectedIndex = 0; // Vyberu all
         }
         else
         {
            comboBoxScale.SelectedIndex = scaleIndex;
         }

         // Nastavim filtr souboru a vahy, datumy zustavaji beze zmeny
         weighingFilter.WeighingInfoFilter.FileFilter = comboBoxFile.SelectedIndex == 0 ? null : comboBoxFile.Text;
         weighingFilter.WeighingInfoFilter.ScaleFilter = comboBoxScale.SelectedIndex == 0 ? null : comboBoxScale.Text;

         // Prekreslim seznam vazeni
         weighingFilter.ShowAvailableWeighings();

         // Vratim se zpet na puvodni radek - musim osetrit i pripad, kdy je v seznamu moc vazeni a je odscrolovany dolu
         if (selectedRow < dataGridViewWeighings.Rows.Count)
         {
            dataGridViewWeighings.FirstDisplayedScrollingRowIndex = dataGridViewWeighings.Rows[selectedRow].Index;
            dataGridViewWeighings.Refresh(); // Prekreslim s vybranym radkem na prvni pozici
            dataGridViewWeighings.CurrentCell = dataGridViewWeighings.Rows[selectedRow].Cells[0];
            dataGridViewWeighings.Rows[selectedRow].Selected = true;
         }
      }

      /// <summary>
      /// Reload weighing list from the database and try to keep currently selected filter.
      /// Call after a weighing has been deleted or imported.
      /// </summary>
      public void ReloadWeighingList()
      {
         // Zapamatuju si aktualni nastaveny filtr
         var fromDateTime = dateTimePickerFrom.Value;
         var fromChecked = dateTimePickerFrom.Checked;
         var toDateTime = dateTimePickerTo.Value;
         var toChecked = dateTimePickerTo.Checked;
         var selectedFile = comboBoxFile.Text;
         var selectedScale = comboBoxScale.Text;

         // Nahraju vazeni z databaze a naplnim seznamy. Controly filtru se nastavi na default hodnoty.
         weighingFilter = new WeighingFilter(this, dateTimePickerFrom, dateTimePickerTo, comboBoxFile, comboBoxScale,
               dataGridViewWeighings)
         {
            ShowSeconds = true
         };
         weighingFilter.Load();
         weighingFilter.BeginUpdate();

         // Pokud v seznamu souboru zustal puvodne vybrany soubor, vyberu ho. Pokud se vybrany soubor smazal,
         // vyberu vsechny soubory. Zaroven nastavim i filtr.
         if (weighingFilter.WeighingInfoFilter.FilesList.Contains(selectedFile))
         {
            comboBoxFile.Text = selectedFile;
            weighingFilter.WeighingInfoFilter.FileFilter = selectedFile;
         }
         else
         {
            comboBoxFile.SelectedIndex = 0;
            weighingFilter.WeighingInfoFilter.FileFilter = null;
         }

         // Stejne tak vahu
         if (weighingFilter.WeighingInfoFilter.ScalesList.Contains(selectedScale))
         {
            comboBoxScale.Text = selectedScale;
            weighingFilter.WeighingInfoFilter.ScaleFilter = selectedScale;
         }
         else
         {
            comboBoxScale.SelectedIndex = 0;
            weighingFilter.WeighingInfoFilter.ScaleFilter = null;
         }

         // Obnovim casovy filtr
         dateTimePickerFrom.Value = fromDateTime;
         dateTimePickerFrom.Checked = false; // Musim rucne, aby se vyvolal event. Jinak checkbox blbne.
         dateTimePickerFrom.Checked = true;
         dateTimePickerFrom.Checked = fromChecked;
         dateTimePickerTo.Value = toDateTime;
         dateTimePickerTo.Checked = false;
         dateTimePickerTo.Checked = true;
         dateTimePickerTo.Checked = toChecked;

         weighingFilter.EndUpdate();

         // Zobrazim seznam vazeni
         weighingFilter.ShowAvailableWeighings();
      }

      private void buttonDelete_Click(object sender, EventArgs e)
      {
         if (dataGridViewWeighings.Rows.Count == 0)
         {
            return;
         }

         if (dataGridViewWeighings.SelectedRows.Count == 0)
         {
            return;
         }

         if (MessageBox.Show(Resources.DELETE_WEIGHINGS, buttonDelete.Text, MessageBoxButtons.YesNo) == DialogResult.No)
         {
            return;
         }

         Cursor.Current = Cursors.WaitCursor;
         try
         {
            // Smazu vsechny vybrane polozky z databaze
            var weighingIdList =
               (from DataGridViewRow row in dataGridViewWeighings.SelectedRows
                  select weighingFilter.WeighingInfoFilter.FilteredInfoList[row.Index].Id).ToList();

            Program.Database.DeleteWeighings(weighingIdList);

            // Nactu znovu vsechna vazeni
            ReloadWeighingList();
         }
         finally
         {
            Cursor.Current = Cursors.Default;
         }
      }

      private void buttonEdit_Click(object sender, EventArgs e)
      {
         Edit();
      }

      private void dataGridViewWeighings_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
      {
         if (selectOneWeighing)
         {
            // Pouze vybira jedno vazeni bez moznosti jakekoliv editace, doubleclick = OK
            buttonOk_Click(this, null);
            return;
         }
         if (e.RowIndex < 0)
         {
            return; // Header
         }
         Edit();
      }

      private void buttonOk_Click(object sender, EventArgs e)
      {
         if (dataGridViewWeighings.SelectedRows.Count == 0)
         {
            return; // Nema vybrane zadne vazeni
         }

         // Nactu prvni vybrane vazeni
         selectedWeighing =
            Program.Database.LoadWeighing(
               weighingFilter.WeighingInfoFilter.FilteredInfoList[dataGridViewWeighings.SelectedRows[0].Index].Id);

         if (ParentForm != null) ParentForm.DialogResult = DialogResult.OK;
      }

      private void buttonImport_Click(object sender, EventArgs e)
      {
         // Zeptam se na jmeno souboru
         var openFileDialog = new OpenFileDialog { DefaultExt = FileExtension.EXPORT_BAT1, Filter = Export.FilterBat1 + "|"+ Export.FilterBat2 +"|All files (*.*)|*.*" };
         if (openFileDialog.ShowDialog() != DialogResult.OK)
         {
            return;
         }

         Refresh();

         // Nactu seznam vazeni, ktere soubor exportu obsahuje
         Cursor.Current = Cursors.WaitCursor;
         List<Weighing> weighingList = new List<Weighing>();

         //TODO any test on type of file (Bat1 format, Bat2 old, ...)
         try
         {
            var exportBat1 = new ExportBat1(openFileDialog.FileName);
            weighingList = exportBat1.ImportWeighingList();  
         }
         catch
         {
            // Import file is not Bat1 format, check if is xml file (Bat2 old)
            var importBat2Old = new ImportBat2Old(openFileDialog.FileName);
            weighingList = importBat2Old.ImportWeighingList();
         }
         finally
         {
            Cursor.Current = Cursors.Default;
         }

         if (weighingList == null)
         {
            return; // Neplatny soubor exportu
         }

         // Zeptam se uzivatele, ktera vazeni chce importovat
         var form = new FormImportWeighings(weighingList);
         if (form.ShowDialog() != DialogResult.OK)
         {
            return;
         }

         Refresh();

         // Sestavim seznam vazeni, ktere chce ulozit
         var saveWeighingList = new List<Weighing>();
         foreach (var weighing in form.SelectedWeighingList)
         {
            DialogResult dialogResult;
            if (weighing.ScaleType == ScaleType.SCALE_BAT2OLD)
            {
               dialogResult = UserControlResultsBase.CanSaveWeighing(weighing);
            }
            else
            {  // Zkontroluju, zda uz vazeni v databazi neexistuje (kombinace soubor - start vazeni)
               dialogResult = UserControlResultsBase.CanSaveWeighing(weighing.WeighingData.File.Name,
                  weighing.GetMinDateTime());
            }
            
            if (dialogResult == DialogResult.No)
            {
               continue; // Vazeni uz existuje, nechce ho prepsat, ale chce pokracovat v ukladani
            }
            if (dialogResult == DialogResult.Cancel)
            {
               return; // Vazeni uz existuje, chce ukoncit cele ukladani
            }

            // Vazeni je mozne ulozit, zmenim zdroj na IMPORT
            weighing.WeighingData.RecordSource = RecordSource.IMPORT;

            // Pridam ho do seznamu
            saveWeighingList.Add(weighing);
         }

         // Pokud je seznam vazeni pro ulozeni prazdny, koncim
         if (saveWeighingList.Count == 0)
         {
            return;
         }

         Cursor.Current = Cursors.WaitCursor;
         try
         {
            // Ulozim seznam vazeni do DB (kazde vazeni s vlastnim configem)
            foreach (var weighing in saveWeighingList)
            {
               Program.Database.SaveWeighing(weighing);
            }

            // Zobrazim info, ze se ulozilo
            Cursor.Current = Cursors.Default;
            MessageBox.Show(Resources.WEIGHINGS_SAVED, Program.ApplicationName);

            // Nactu znovu vsechna vazeni
            ReloadWeighingList();
         }
         finally
         {
            Cursor.Current = Cursors.Default;
         }
      }

      private void buttonAdd_Click(object sender, EventArgs e)
      {
         new FormManualResults().ShowDialog();

         // Nactu znovu vsechna vazeni
         ReloadWeighingList();
      }

      private void UserControlWeighings_Load(object sender, EventArgs e)
      {
         // Nahraju a zobrazim vazeni
         Cursor.Current = Cursors.WaitCursor;
         try
         {
            // Dostupna vazeni nahraju z DB  zobrazim
            weighingFilter = new WeighingFilter(this, dateTimePickerFrom, dateTimePickerTo, comboBoxFile, comboBoxScale,
               dataGridViewWeighings)
            {
               ShowSeconds = true
            };

            // Zaregistruju event handler po zmene seznamu dostupnych vazeni
            weighingFilter.ChangedEvent += AvailableChangedEventHandler;

            // Nahraju seznam (musim az pote, co mam zaregistrovan event)
            weighingFilter.Load();
         }
         finally
         {
            Cursor.Current = Cursors.Default;
         }
      }

      private void buttonExportAll_Click(object sender, EventArgs e)
      {
         if (dataGridViewWeighings.Rows.Count == 0)
         {
            return; // Tabulka je prazdna
         }

         // Nactu seznam vsech vazeni v tabulce - muze to trvat
         Cursor.Current = Cursors.WaitCursor;
         var weighingList = new List<Weighing>();
         try
         {
            weighingList.AddRange(
               weighingFilter.WeighingInfoFilter.FilteredInfoList.Select(info => Program.Database.LoadWeighing(info.Id)));
         }
         finally
         {
            Cursor.Current = Cursors.Default;
         }

         // Exportuju
         new Export(this).ExportWeighingList(weighingList, Flag.ALL, true);
      }

      private void buttonExportSelected_Click(object sender, EventArgs e)
      {
         if (dataGridViewWeighings.SelectedRows.Count == 0)
         {
            return; // Nema vybrane zadne vazeni
         }

         // Nactu seznam vybranych vazeni - muze to trvat
         Cursor.Current = Cursors.WaitCursor;
         var weighingList = new List<Weighing>();
         try
         {
            foreach (DataGridViewRow row in dataGridViewWeighings.SelectedRows)
            {
               weighingList.Add(
                  Program.Database.LoadWeighing(weighingFilter.WeighingInfoFilter.FilteredInfoList[row.Index].Id));
            }
         }
         finally
         {
            Cursor.Current = Cursors.Default;
         }

         // Exportuju
         new Export(this).ExportWeighingList(weighingList, Flag.ALL, true);
      }

      #region Implementation of IView

      /// <summary>
      /// Gets or sets the data context of the view.
      /// This will be set to ViewModel of the view.
      /// </summary>
      public object DataContext { get; set; }

      #endregion
   }

   public interface IWeighingsControl : IView
   {
      /// <summary>
      /// Reload list of visible weighings from database
      /// </summary>
      void ReloadWeighingList();
   }
}
