using System;

namespace Bat1
{
   /// <summary>
   /// One point in a time graph
   /// </summary>
   public struct TimeDoublePoint {
      public DateTime dateTime;
      public double value;
   }
}