﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using DataContext;
using Desktop.Wpf.Presentation;
using UserControl = System.Windows.Controls.UserControl;

namespace Bat1.Forms.UserControls
{
   /// <summary>
   /// Interaction logic for UserControlScaleConfig.xaml
   /// </summary>
   public partial class UserControlScaleConfig : UserControl
   {
      #region Fields and properties

      /// <summary>
      /// Scale config that is edited
      /// </summary>
      public Bat2Scale ScaleBat2Config { get { return scaleBat2Config; } }
      private Bat2Scale scaleBat2Config;

      public Bat1Scale ScaleBat1Config { get { return scaleBat1Config; } }
      private Bat1Scale scaleBat1Config;

      /// <summary>
      /// List of created details
      /// </summary>  
      private List<IView> listDetails = new List<IView>();   

      #endregion

      #region Public interface

      #region Constructors

      public UserControlScaleConfig(IEnumerable<IView> configuration, IEnumerable<IView> settings)
      {
         InitializeComponent();
      
         AddListToTreeView(Properties.Resources.Configuration, configuration);
         AddListToTreeView(Properties.Resources.ServiceSettings, settings);
      }

      #endregion

      /// <summary>
      /// Set scale config to edit
      /// </summary>
      /// <param name="scaleConfig">Scale config</param>
      public void SetScaleConfig(Bat2Scale scaleConfig)
      {
         scaleBat2Config = scaleConfig;
      }

      /// <summary>
      /// Set scale config to edit
      /// </summary>
      /// <param name="scaleConfig">Scale config</param>
      public void SetScaleConfig(ScaleConfig scaleConfig)
      {
         scaleBat1Config = Program.Database.LoadBat1Config(scaleConfig);
         // Nastavim ve vsech controlech
         foreach (var control in listDetails)
         {
            var c = control as UserControlScaleConfigBase;
            if (c != null)
            {
               c.SetScaleConfig(scaleBat1Config);
            }
         }
      }    

      #endregion

      #region Private helpers

      private void AddListToTreeView(string headerName, IEnumerable<IView> list)
      {       
         var treeItem = new TreeViewItem { Header = headerName, IsExpanded = true };
         foreach (var c in list)
         {
            var name = c.GetType().Name;
            treeItem.Items.Add(new TreeViewItem { Header = Properties.Resources.ResourceManager.GetString(name), Name = name });
            System.Windows.Forms.Control pageControl;
            listDetails.Add(c);

            if (c is System.Windows.Forms.UserControl)
            {
               pageControl = c as System.Windows.Forms.UserControl;
            }
            else if (c is UIElement)
            {
               pageControl = new ElementHost {Child = c as UIElement};
               pageControl.Hide();
            }
            else
            {
               continue;
            }
            pageControl.Dock = DockStyle.Fill;
            PanelPages.Controls.Add(pageControl);
         }
         TreeViewMenu.Items.Add(treeItem);
      }

      /// <summary>
      /// Show specified details
      /// </summary>
      /// <param name="visibleControl"></param> 
      private void ShowDetails(IView visibleControl)
      {
         // Schovam vsechny detaily krome tech, ktere mam zobrazit    
         var visibleControlIndex = 0;
         for (var i = 0; i < PanelPages.Controls.Count; i++)
         {
            PanelPages.Controls[i].Hide();

            var host = PanelPages.Controls[i] as ElementHost;
            if ((host != null && host.Child.Equals(visibleControl)) ||  //wpf control
                PanelPages.Controls[i].Equals(visibleControl))          // winform control
            {
               visibleControlIndex = i;
            }          
         }          

         if (visibleControl == null)
         {
            // Chce pouze schovat vsechny detaily, nic nezobrazuju
            return;
         }

         // Zobrazim zvolene detaily       
         PanelPages.Controls[visibleControlIndex].Show();
      }

      private void TreeViewMenu_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
      {
         var item = e.NewValue as TreeViewItem;
         if (item == null) { return; }

         if (item.Items.Count > 0) // parent node
         {  //switch to first child
            ((TreeViewItem) item.Items[0]).IsSelected = true;
            return;
         }

         GroupBoxDetails.Header = item.Header;
         var control = listDetails.FirstOrDefault(d => d.GetType().Name.Equals(item.Name));       
         ShowDetails(control);
      }

      private void UserControlScaleConfig_OnLoaded(object sender, RoutedEventArgs e)
      {  // View first view in first menu
         var firstMenu = TreeViewMenu.Items[0] as TreeViewItem;
         if (firstMenu == null) return;
         var firstSubItem = firstMenu.Items[0] as TreeViewItem;
         if (firstSubItem == null) return;
         firstSubItem.IsSelected = true;
      }

      #endregion    
  
      #region Bat1 methods

      /// <summary>
      /// Read-only mode
      /// </summary>
      private bool readOnly;

      #endregion
   }
}
