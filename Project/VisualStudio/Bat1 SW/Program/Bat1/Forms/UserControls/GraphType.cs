namespace Bat1
{
   /// <summary>
   /// Displayed graph type
   /// </summary>
   enum GraphType {
      NONE,
      GROWTH_CURVE,
      GAIN,
      COUNT,
      SIGMA,
      CV,
      UNIFORMITY,
      SPEED
   }
}