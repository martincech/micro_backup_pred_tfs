﻿using System.Windows.Forms;

namespace Bat1 {
    public partial class FormSplash : Form {
        public FormSplash() {
            InitializeComponent();

            // Nahraju splash screen
            try 
            {             
               object splash = Properties.Resources.Splash;              
               pictureBoxSplash.Image = (System.Drawing.Image)splash;
            } 
            catch 
            {
                // Soubor neexistuje, nic se nedeje
            }

        }
    }
}
