﻿namespace Bat1 {
    partial class FormSplash {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSplash));
         this.pictureBoxSplash = new System.Windows.Forms.PictureBox();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSplash)).BeginInit();
         this.SuspendLayout();
         // 
         // pictureBoxSplash
         // 
         resources.ApplyResources(this.pictureBoxSplash, "pictureBoxSplash");
         this.pictureBoxSplash.Name = "pictureBoxSplash";
         this.pictureBoxSplash.TabStop = false;
         // 
         // FormSplash
         // 
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.White;
         this.Controls.Add(this.pictureBoxSplash);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.Name = "FormSplash";
         this.ShowInTaskbar = false;
         this.TopMost = true;
         ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSplash)).EndInit();
         this.ResumeLayout(false);
         this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxSplash;

    }
}