using System.Reflection;

namespace Bat1
{
   public class SoftwareVersion
   {
// ReSharper disable once InconsistentNaming
      public static int MINOR
      {
         get { return Assembly.GetExecutingAssembly().GetName().Version.Minor; }
      }

// ReSharper disable once InconsistentNaming
      public static int MAJOR
      {
         get { return Assembly.GetExecutingAssembly().GetName().Version.Major; }
      }
   }
}