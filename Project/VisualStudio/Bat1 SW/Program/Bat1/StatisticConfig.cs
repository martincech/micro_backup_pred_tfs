using Bat1Library;

namespace Bat1
{
   /// <summary>
   /// Parameters for statistic calculations
   /// </summary>
   public struct StatisticConfig 
   {
      public int             UniformityRange;// Uniformity range in +/- percents of average weight
      public HistogramConfig Histogram;      // Histogram parameters
   }
}