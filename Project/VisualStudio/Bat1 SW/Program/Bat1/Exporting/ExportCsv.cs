﻿// Decimal separator

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Bat1.Properties;
using Bat1.Utilities;
using Bat1Library;
using Bat2Library.Connection.Interface.Domain;
using BatLibrary;
using DataContext;
using Utilities;

namespace Bat1.Exporting 
{
    /// <summary>
    /// Export data into CSV file
    /// </summary>
    public class ExportCsv 
    {
       /// <summary>
       /// Transfer number for data from bat2 device.
       /// </summary>
       private const float TRANSFER = 0.1f;

        /// <summary>
        /// File name to save to
        /// </summary>
        private string fileName;

        /// <summary>
        /// CSV instance
        /// </summary>
        private Csv csv;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileName">File name to save to</param>
        public ExportCsv(string fileName) {
            // Preberu nazev souboru
            this.fileName = fileName;

            // Vytvorim instanci CSV s parametry
            string delimiter;
            switch (Program.Setup.CsvConfig.Delimiter) {
                case CsvDelimiter.COMMA:
                    delimiter = ",";
                    break;
                case CsvDelimiter.SEMICOLON:
                    delimiter = ";";
                    break;
                default: // CsvDelimiter.TAB:
                    delimiter = "\t";
                    break;
            }

            string decimalSeparator;
            switch (Program.Setup.CsvConfig.DecimalSeparator) {
                case CsvDecimalSeparator.COMMA:
                    decimalSeparator = ",";
                    break;
                case CsvDecimalSeparator.DOT:
                    decimalSeparator = ".";
                    break;
                default: // CsvDecimalSeparator.DEFAULT:
                    decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    break;
            }

            Encoding encoding;
            switch (Program.Setup.CsvConfig.Encoding) {
                case CsvEncoding.DEFAULT:
                    encoding = Encoding.Default;
                    break;
                case CsvEncoding.UTF16:
                    encoding = Encoding.Unicode;
                    break;
                default: // CsvEncoding.UTF8:
                    encoding = Encoding.UTF8;
                    break;
            }

            csv = new Csv(delimiter, decimalSeparator, encoding);
        }

        /// <summary>
        /// Adds flag to the file name
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="flag">Used flag</param>
        /// <returns></returns>
        private string AddFlag(string fileName, Flag flag) {
            if (flag != Flag.ALL) {
                fileName += " (" + flag.ToLocalizedString()+ ")";     // Pridam flag
            }
            return fileName;
        }

        private void AddArchive(ArchiveItem archive, string scaleName, Units units)
        {
           var avg = BatLibrary.ConvertWeight.Convert(archive.Average * TRANSFER, Units.G, units);
           var sigma = BatLibrary.ConvertWeight.Convert(archive.Sigma * TRANSFER, Units.G, units);
           var gain = BatLibrary.ConvertWeight.Convert(archive.Gain * TRANSFER, Units.G, units);
           var targetWeight = BatLibrary.ConvertWeight.Convert(archive.TargetWeight * TRANSFER, Units.G, units);
           var targetWeightFemale = BatLibrary.ConvertWeight.Convert(archive.TargetWeightFemale * TRANSFER, Units.G, units);

           // Hlavicka
           csv.ClearLine();
           csv.AddString(0, Resources.DATE_AND_TIME + ":");
           csv.AddDateTime(1, archive.Timestamp);
           csv.AddString(3, Resources.COUNT + ":");
           csv.AddInteger(4, archive.Count);
           csv.AddString(6, Resources.GAIN + " [" + units.ToLocalizedString() + "]:");
           csv.AddDouble(7, double.Parse(DisplayFormat.RoundWeight(gain, units)));
           csv.SaveLine();

           csv.ClearLine();
           csv.AddString(0, Resources.SCALE + ":");
           csv.AddString(1, scaleName);
           csv.AddString(3, Resources.AVERAGE + " [" + units.ToLocalizedString() + "]:");
           csv.AddDouble(4, double.Parse(DisplayFormat.RoundWeight(avg, units)));
           csv.AddString(6, Resources.UNIFORMITY + " [%]:");
           csv.AddDouble(7, double.Parse(DisplayFormat.Uniformity(archive.Uniformity * TRANSFER)));
           csv.SaveLine();

           csv.ClearLine();
           csv.AddString(0, Resources.SEX + ":");
           csv.AddString(1, (archive.Sex).ToString());
           csv.AddString(3, Resources.ST_DEVIATION + " [" + units.ToLocalizedString() + "]:");
           csv.AddDouble(4, double.Parse(DisplayFormat.RoundWeight(sigma, units)));
           csv.AddString(6, Resources.TARGET_WEIGHT + " [" + units.ToLocalizedString() + "]:");
           csv.AddDouble(7, double.Parse(DisplayFormat.RoundWeight(targetWeight, units)));
           csv.SaveLine();

           csv.ClearLine();
           csv.AddString(6, Resources.TARGET_WEIGHT_FEMALE + " [" + units.ToLocalizedString() + "]:");
           csv.AddDouble(7, double.Parse(DisplayFormat.RoundWeight(targetWeightFemale, units)));
           csv.SaveLine();
        }

        /// <summary>
        /// Add weighing to the CSV
        /// </summary>
        /// <param name="weighing">Weighing</param>
        /// <param name="flag">Flag</param>
        private void AddWeighing(Weighing weighing, Flag flag) {
            // Nactu vysledek
            var result = weighing.WeighingResults.GetResult(flag);
            if (result == null) {
                return;     // Tento flag ve vysledku neni
            }

            var units = weighing.WeighingData.ScaleConfig.Units.Units;
            var unitsString = units.ToLocalizedString();
            
            // Hlavicka
            csv.ClearLine();
            csv.AddString( 0, Resources.FILE + ":");
            csv.AddString( 1, AddFlag(weighing.WeighingData.File.Name, flag));       
            csv.AddString( 3, Resources.COUNT + ":");
            csv.AddInteger(4, result.Count);
            csv.AddString( 6, Resources.CV + " [%]:");
            csv.AddDouble( 7, double.Parse(DisplayFormat.Cv(result.Cv)));
            csv.SaveLine();

            csv.ClearLine();
            csv.AddString( 0, Resources.SCALE + ":");
            csv.AddString( 1, weighing.WeighingData.ScaleConfig.ScaleName);
            csv.AddString( 3, Resources.AVERAGE + " [" + unitsString + "]:");
            csv.AddDouble( 4, double.Parse(DisplayFormat.RoundWeight(result.Average, units)));
            csv.AddString( 6, Resources.UNIFORMITY + " [%]:");
            csv.AddDouble( 7, double.Parse(DisplayFormat.Uniformity(result.Uniformity)));
            csv.SaveLine();

            csv.ClearLine();
            csv.AddString( 0, Resources.NOTE + ":");
            csv.AddString( 1, weighing.WeighingData.Note);
            csv.AddString( 3, Resources.ST_DEVIATION + " [" + unitsString + "]:");
            csv.AddDouble( 4, double.Parse(DisplayFormat.RoundWeight(result.Sigma, units)));
            csv.AddString( 6, Resources.SPEED + " [1/" + Resources.HOUR + "]:");
            csv.AddInteger(7, (int)Math.Round(result.WeighingSpeed));
            csv.SaveLine();

            csv.ClearLine();
            csv.SaveLine();

            // Hlavicka tabulky vzorku
            csv.ClearLine();
            csv.AddString(0, Resources.FILE);
            csv.AddString(1, Resources.NUMBER);
            csv.AddString(2, Resources.DATE_AND_TIME);
            csv.AddString(3, Resources.WEIGHT + " [" + unitsString + "]");
            csv.AddString(4, Resources.SEX_LIMIT);
            csv.SaveLine();

            // Vzorky pouze u vysledku stazenych z vahy (u rucne zadanych vzorky nejsou)
            //if (weighing.WeighingData.ResultType != ResultType.MANUAL)
            if (weighing.WeighingData.SampleList != null)
            {
                var sampleIndex = 1;
                weighing.WeighingData.SampleList.First(flag);
                while (weighing.WeighingData.SampleList.Read(flag)) {
                    csv.ClearLine();
                    csv.AddString(  0, weighing.WeighingData.File.Name);
                    csv.AddInteger( 1, sampleIndex);
                    csv.AddDateTime(2, weighing.WeighingData.SampleList.Sample.DateTime);
                    csv.AddDouble(  3, double.Parse(DisplayFormat.RoundWeight(weighing.WeighingData.SampleList.Sample.Weight, units)));
                    csv.AddString(  4, weighing.WeighingData.SampleList.Sample.Flag.ToLocalizedString());
                    csv.SaveLine();
                    sampleIndex++;
                }
            }
        }

        /// <summary>
        /// Add header of statistics table to the CSV (layout corresponding to icon Statistics)
        /// </summary>
        /// <param name="units">Units used</param>
        private void AddStatisticsHeader(Units units) {
            var unitsString = units.ToLocalizedString();
            
            csv.ClearLine();
            csv.AddString(0, Resources.DATE_AND_TIME);
            csv.AddString(1, Resources.FILE);
            csv.AddString(2, Resources.COUNT);
            csv.AddString(3, Resources.AVERAGE + " [" + unitsString + "]");
            csv.AddString(4, Resources.ST_DEVIATION + " [" + unitsString + "]");
            csv.AddString(5, Resources.CV + " [%]");
            csv.AddString(6, Resources.UNIFORMITY + " [%]");
            csv.AddString(7, Resources.SPEED + " [1/" + Resources.HOUR + "]");
            csv.SaveLine();
        }

        /// <summary>
        /// Fills columns of statistics
        /// </summary>
        /// <param name="result">Statistic result</param>
        private void AddStatisticsColumns(StatisticResult result, Units units, ResultType resultType) {
            csv.AddInteger(2, result.Count);
            csv.AddDouble( 3, double.Parse(DisplayFormat.RoundWeight(result.Average, units)));
            csv.AddDouble( 4, double.Parse(DisplayFormat.RoundWeight(result.Sigma, units)));
            csv.AddDouble( 5, double.Parse(DisplayFormat.Cv(result.Cv)));
            csv.AddDouble( 6, double.Parse(DisplayFormat.Uniformity(result.Uniformity)));
            csv.AddInteger(7, (int)Math.Round(result.WeighingSpeed));
        }

        /// <summary>
        /// Add weighing statistics to the CSV (layout corresponding to icon Statistics)
        /// </summary>
        /// <param name="weighing">Weighing</param>
        /// <param name="flag">Flag</param>
        private void AddStatistics(Weighing weighing, Flag flag) {
            // Nactu vysledek
            var result = weighing.WeighingResults.GetResult(flag);

            var units = weighing.WeighingData.ScaleConfig.Units.Units;
            var unitsString = units.ToLocalizedString();
            
            csv.ClearLine();
            if (result == null) {
                // Tento flag ve vysledku neni, ulozim jen nazev souboru
                csv.AddString(  1, AddFlag(weighing.WeighingData.File.Name, flag));       
            } else {
                csv.AddDateTime(0, weighing.GetMinDateTime());
                csv.AddString(  1, AddFlag(weighing.WeighingData.File.Name, flag));
                AddStatisticsColumns(result, units, weighing.WeighingData.ResultType);
            }
            csv.SaveLine();
        }

        /// <summary>
        /// Add statistics total to the CSV (layout corresponding to icon Statistics)
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Flag</param>
        private void AddStatisticsTotal(WeighingList weighingList, Flag flag) {
            // Nactu vysledek
            var result = weighingList.TotalstatisticResults.GetResult(flag);
            var units = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;
            
            csv.ClearLine();
            csv.AddString(0, Resources.TOTAL + " (" + weighingList.List.Count.ToString() + ")");
            if (result != null) {
                // Tento flag je ve vysledku (pokud ne, ulozim pouze nazev souboru)
                AddStatisticsColumns(result, units, ResultType.MANUAL);     // Celkove vysledky vzdy zaokrouhluju
            }
            csv.SaveLine();
        }

        /// <summary>
        /// Add header of flock statistics table to the CSV (layout corresponding to icon Flocks)
        /// </summary>
        /// <param name="units">Units used</param>
        private void AddFlocksHeader(Units units) {
            var unitsString = units.ToLocalizedString();
            
            csv.ClearLine();
            csv.AddString(0, Resources.DATE_AND_TIME);
            csv.AddString(1, Resources.DAY);
            csv.AddString(2, Resources.Flock);
            csv.AddString(3, Resources.COUNT);
            csv.AddString(4, Resources.AVERAGE      + " [" + unitsString + "]");
            csv.AddString(5, Resources.TARGET       + " [" + unitsString + "]");
            csv.AddString(6, Resources.DIFFERENCE   + " [" + unitsString + "]");
            csv.AddString(7, Resources.ABOVE);
            csv.AddString(8, Resources.BELOW);
            csv.AddString(9, Resources.GAIN         + " [" + unitsString + "]");
            csv.AddString(10,Resources.ST_DEVIATION + " [" + unitsString + "]");
            csv.AddString(11,Resources.CV + " [%]");
            csv.AddString(12,Resources.UNIFORMITY + " [%]");
            csv.AddString(13,Resources.SPEED + " [1/" + Resources.HOUR + "]");
            csv.SaveLine();
        }

        /// <summary>
        /// Fills columns of flock
        /// </summary>
        /// <param name="result">Statistic result</param>
        private void AddFlockColumns(StatisticResult result, Units units, ResultType resultType) {
            csv.AddInteger( 3, result.Count);
            csv.AddDouble(  4, double.Parse(DisplayFormat.RoundWeight(result.Average, units)));
            csv.AddDouble( 10, double.Parse(DisplayFormat.RoundWeight(result.Sigma, units)));
            csv.AddDouble( 11, double.Parse(DisplayFormat.Cv(result.Cv)));
            csv.AddDouble( 12, double.Parse(DisplayFormat.Uniformity(result.Uniformity)));
            csv.AddInteger(13, (int)Math.Round(result.WeighingSpeed));
        }

        /// <summary>
        /// Add flock statistics to the CSV (layout corresponding to icon Flocks)
        /// </summary>
        /// <param name="weighing">Weighing</param>
        /// <param name="flag">Flag</param>
        private void AddFlocks(Weighing weighing, Flag flag) {
            // Nactu vysledek
            var result = weighing.WeighingResults.GetResult(flag);

            var units = weighing.WeighingData.ScaleConfig.Units.Units;
            var unitsString = units.ToLocalizedString();
            
            csv.ClearLine();
            if (result == null) {
                // Tento flag ve vysledku neni, ulozim jen nazev hejna
                csv.AddString(  2, AddFlag(weighing.WeighingData.File.Name, flag));       
            } else {
                csv.AddDateTime(0, weighing.GetMinDateTime());
                csv.AddInteger( 1, weighing.FlockData.Day);
                csv.AddString(  2, AddFlag(weighing.WeighingData.File.Name, flag));       
                AddFlockColumns(result, units, weighing.WeighingData.ResultType);
                csv.AddDouble(  5, double.Parse(DisplayFormat.RoundWeight(weighing.FlockData.Target, units)));
                csv.AddDouble(  6, double.Parse(DisplayFormat.RoundWeight(weighing.FlockData.Difference, units)));
                csv.AddInteger( 7, weighing.FlockData.CountAbove);
                csv.AddInteger( 8, weighing.FlockData.CountBelow);
                csv.AddDouble(  9, double.Parse(DisplayFormat.RoundWeight(weighing.FlockData.Gain, units)));
            }
            csv.SaveLine();
        }

        /// <summary>
        /// Add flocks total to the CSV (layout corresponding to icon Flocks)
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Flag</param>
        private void AddFlocksTotal(WeighingList weighingList, Flag flag) {
            // Nactu vysledek
            var result = weighingList.TotalstatisticResults.GetResult(flag);
            var units = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;
            
            csv.ClearLine();
            csv.AddString(0, Resources.TOTAL + " (" + weighingList.List.Count.ToString() + ")");
            if (result != null) {
                // Tento flag je ve vysledku (pokud ne, ulozim pouze nazev souboru)
                AddFlockColumns(result, units, ResultType.MANUAL);     // Celkove vysledky vzdy zaokrouhluju
            }
            csv.SaveLine();
        }

        /// <summary>
        /// Save current CSV content to the file
        /// </summary>
        private void Save() {
            try {
                // Pokud soubor existuje, prepisu
                if (System.IO.File.Exists(fileName)) {
                    System.IO.File.Delete(fileName);
                }
                
                // Ulozim do souboru
                csv.SaveToFile(fileName);
            } catch {
                // Soubor jiz ma otevreny v Excelu
            }
        }

        /// <summary>
        /// Export one weighing
        /// </summary>
        /// <param name="weighing">Weighing</param>
        /// <param name="flag">Flag</param>
        public void ExportWeighing(Weighing weighing, Flag flag) {
            // Nactu vysledek
            var result = weighing.WeighingResults.GetResult(flag);
            if (result == null) {
                return;     // Tento flag ve vysledku neni
            }
            
            // Smazu cele CSV
            csv.Clear();

            // Pridam vazeni
            AddWeighing(weighing, flag);

            // Ulozim do souboru
            csv.SaveToFile(fileName);
        }


        public void ExportArchiveList(List<ArchiveItem> archiveList, string scaleName, Units units)
        {
           // Smazu cele CSV
           csv.Clear();

           foreach (var archive in archiveList)
           {
              AddArchive(archive, scaleName, units);

              // Prazdny radek
              csv.ClearLine();
              csv.SaveLine();
              csv.ClearLine();
              csv.SaveLine();
           }

           // Ulozim do souboru
           Save();
        }

        /// <summary>
        /// Export a list of weighings
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Flag</param>
        public void ExportWeighingList(List<Weighing> weighingList, Flag flag) {
            // Smazu cele CSV
            csv.Clear();

            foreach (var weighing in weighingList) {
                // Nactu vysledek
                var result = weighing.WeighingResults.GetResult(flag);
                if (result == null) {
                    continue;   // Tento flag ve vysledku neni
                }

                // Pridam vazeni
                AddWeighing(weighing, flag);

                // Prazdny radek
                csv.ClearLine();
                csv.SaveLine();
                csv.SaveLine();
                csv.SaveLine();
            }

            // Ulozim do souboru
            Save();
        }

        /// <summary>
        /// Export table with statistics (layout corresponding to icon Statistics)
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Flag</param>
        /// <param name="isFlocks">True if exporting flocks table</param>
        public void ExportStatisticsTable(WeighingList weighingList, Flag flag, bool isFlocks) {
            if (weighingList.List.Count == 0) {
                return;     // Seznam je prazdny
            }
            
            // Smazu cele CSV
            csv.Clear();

            var units = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;

            if (isFlocks) {
                AddFlocksHeader(units);
            } else {
                AddStatisticsHeader(units);
            }
            
            // Pridam vsechna vazeni
            foreach (var weighing in weighingList.List) {
                if (isFlocks) {
                    AddFlocks(weighing, flag);
                } else {
                    AddStatistics(weighing, flag);
                }
            }

            // Pridam radek TOTAL, pouze pokud je v tabulce vic jak 1 radek
            if (weighingList.List.Count > 1) {
                if (isFlocks) {
                    AddFlocksTotal(weighingList, flag);
                } else {
                    AddStatisticsTotal(weighingList, flag);
                }
            }

            // Ulozim do souboru
            Save();
        }

    }
}
