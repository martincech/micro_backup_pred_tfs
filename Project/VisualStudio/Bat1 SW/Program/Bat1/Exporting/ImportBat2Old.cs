﻿using System.Collections.Generic;
using System.Xml;
using Bat1Library;
using Bat2Library;
using Bat2Library.Utilities;
using DataContext;

namespace Bat1.Exporting
{
   public class ImportBat2Old
   {
      private readonly string fileName;

      public ImportBat2Old(string fileName)
      {
         this.fileName = fileName;
      }

      public List<Weighing> ImportWeighingList()
      {
         var doc = new XmlDocument();
         doc.Load(fileName);        
         var xmlParser = new XmlFileParser(doc);
         xmlParser.Parse();
  
         var weighings = new List<Weighing>();
         foreach (var report in xmlParser.Reports)
         {
            var config = new ScaleConfig {ScaleName = report.ScaleName};
            var file = report.ScaleName + "_" + report.Day + "_" + report.Date.ToShortDateString();
            var weighingData = new WeighingData(-1, new File(file, "", null), null, config, "");           

            var sex = new Flag();
            switch (report.Sex)
            {
               case SexE.SEX_FEMALE:
                  sex = Flag.FEMALE;
                  break;
               case SexE.SEX_MALE:
                  sex = Flag.MALE;
                  break;
            }

            var stat = new StatisticResult(sex, report.Count, (float)report.Average, (float)report.Sigma, (float)report.Cv, (float)report.Uniformity,
                  report.Date, (float)report.Gain);
            var weighingResults = new WeighingResults(new List<StatisticResult> {stat});

            var flockData = new FlockData
            {
               Gain = (float) report.Gain, 
               Day = report.Day
            };
            var weighing = new Weighing(weighingData, weighingResults)
            {
               ScaleType = ScaleType.SCALE_BAT2OLD,
               FlockData = flockData
            };       

            weighings.Add(weighing);
         }

         return weighings;
      }
   }
}
