﻿using System;
using Utilities.Observable;

namespace Bat1Wpf.ModelViews.Models
{
   public class Archive : ObservableObject
   {
      private string filename;
      private DateTime dateAndTime;
      private int count;
      private double average;
      private int divide;
      private double uniformity;
      private string note;
      private bool isSaved;
      private int[] histogram;

      public string Filename { get { return filename; } set { SetProperty(ref filename, value); } }
      public DateTime DateAndTime { get { return dateAndTime; } set { SetProperty(ref dateAndTime, value); } }
      public int Count { get { return count; } set { SetProperty(ref count, value); } }
      public double Average { get { return average; } set { SetProperty(ref average, value); } }
      public int Divide { get { return divide; } set { SetProperty(ref divide, value); } }
      public double Uniformity { get { return uniformity; } set { SetProperty(ref uniformity, value); } }
      public string Note { get { return note; } set { SetProperty(ref note, value); } }
      public bool IsSaved { get { return isSaved; } set { SetProperty(ref isSaved, value); } }
      public int[] Histogram { get { return histogram; } set { SetProperty(ref histogram, value); } }
   }
}
