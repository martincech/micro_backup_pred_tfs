﻿using System.Windows.Input;
using Utilities.Observable;

namespace Bat1Wpf.ModelViews.Models
{
   public class CommandObject : ObservableObject
   {
      private System.Drawing.Bitmap picture;
      private string text;
      private string toolTip;
      private ICommand command;
      private object content;

      public System.Drawing.Bitmap Picture { get { return picture; } set { SetProperty(ref picture, value); } }
      public string Text { get { return text; } set { SetProperty(ref text, value); } }
      public string ToolTip { get { return toolTip; } set { SetProperty(ref toolTip, value); } }
      public ICommand Command { get { return command; } set { SetProperty(ref command, value); } }
      public object Content { get { return content; } set { SetProperty(ref content, value); } }
   }
}
