﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Bat1Wpf.ModelViews.Applications;
using Bat1Wpf.ModelViews.Models;
using Bat1Wpf.Utilities;
using Bat2Library;

namespace Bat1Wpf.SampleData
{
   public class SampleScaleBat1ViewModel : ScaleBat1ViewModel
   {
      private static ObservableCollection<CommandObject> scaleItems = new CreateCommandObjectItems().CreateScaleBat1ScaleItems();
      private static ObservableCollection<CommandObject> archiveItems = new CreateCommandObjectItems().CreateScaleBat1ArchiveItems();
      private static ObservableCollection<CommandObject> items = new CreateCommandObjectItems().CreateScaleBat1Items();

      public SampleScaleBat1ViewModel() : base(scaleItems, archiveItems, items)
      {
         DivideColl = new ObservableCollection<int>();
         for (int i = 1; i <= 10; i++)
         {
            DivideColl.Add(i);
         }

         Scale = new Scale
         {
            Name = "BAT1",
            Archives = new ObservableCollection<Archive>()
         };

         // create histogram
         var hist = new int[39];
         hist[5] = 2;
         hist[6] = 5;
         hist[7] = 7;
         hist[8] = 10;
         hist[9] = 9;
         hist[10] = 8;
         hist[15] = 2;
         hist[16] = 1;
         
         var a1 = new Archive
         {
            Count = 10,
            Average = 1,
            DateAndTime = DateTime.Now,
            Divide = 1,
            Filename = "File000",
            IsSaved = false,
            Uniformity = 100,
            Histogram = hist
         };
         Scale.Archives.Add(a1);


         Unit = WeightUnitsE.WEIGHT_UNITS_KG;
      }
   }
}
