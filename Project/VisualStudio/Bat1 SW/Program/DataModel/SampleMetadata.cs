//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
   using System;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   
   [MetadataType(typeof(SampleMetadata))]
   public partial class Sample
   {
      internal sealed class SampleMetadata
   	{
      
        [Display(Name = "Weight")]
      	public double Weight { get; set; }
   
        [Display(Name = "Flag")]
      	public byte Flag { get; set; }
   
        [DataType(DataType.DateTime)]
        [Display(Name = "Saved Date Time")]
      	public System.DateTime SavedDateTime { get; set; }
   
        [Key, Editable(false)]
        [Display(Name = "Sample")]
      	public long SampleId { get; set; }
   
        [Required(ErrorMessage=null, ErrorMessageResourceName ="SampleResultRequired", ErrorMessageResourceType = typeof(DataModel.Properties.Resources))]
        public SampleResult SampleResult { get; set; }
   
   	}
   }
}
