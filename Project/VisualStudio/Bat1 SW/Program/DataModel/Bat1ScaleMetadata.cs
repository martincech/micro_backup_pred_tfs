//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
   using System;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   
   [MetadataType(typeof(Bat1ScaleMetadata))]
   [Table("ScaleConfigs_Bat1Scale")]
   public partial class Bat1Scale : ScaleConfig
   {
      internal sealed class Bat1ScaleMetadata
   	{
      
        [Display(Name = "Sounds Tone Default")]
      	public byte SoundsToneDefault { get; set; }
   
        [Display(Name = "Sounds Tone Light")]
      	public byte SoundsToneLight { get; set; }
   
        [Display(Name = "Sounds Tone Ok")]
      	public byte SoundsToneOk { get; set; }
   
        [Display(Name = "Sounds Tone Heavy")]
      	public byte SoundsToneHeavy { get; set; }
   
        [Display(Name = "Sounds Tone Keyboard")]
      	public byte SoundsToneKeyboard { get; set; }
   
        [Display(Name = "Sounds Enable Special")]
      	public bool SoundsEnableSpecial { get; set; }
   
        [Display(Name = "Sounds Volume Keyboard")]
      	public byte SoundsVolumeKeyboard { get; set; }
   
        [Display(Name = "Sounds Volume Saving")]
      	public byte SoundsVolumeSaving { get; set; }
   
        [Display(Name = "Printer Paper Width")]
      	public byte PrinterPaperWidth { get; set; }
   
        [Display(Name = "Printer Comm Format")]
      	public byte PrinterCommFormat { get; set; }
   
        [Display(Name = "Printer Comm Speed")]
      	public int PrinterCommSpeed { get; set; }
   
        [Display(Name = "Enable More Birds")]
      	public bool EnableMoreBirds { get; set; }
   
        [Display(Name = "Enable File Parameters")]
      	public bool EnableFileParameters { get; set; }
   
        [DataType(DataType.Password)]
        [Display(Name = "Enable Password")]
      	public bool EnablePassword { get; set; }
   
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
      	public int Password { get; set; }
   
        [Display(Name = "Minimum Weight")]
      	public double MinimumWeight { get; set; }
   
        [Display(Name = "Active File Index")]
      	public short ActiveFileIndex { get; set; }
   
        [Display(Name = "Weighing Capacity")]
      	public byte WeighingCapacity { get; set; }
   
        [Display(Name = "Hw Version")]
      	public byte HwVersion { get; set; }
   
   	}
   }
}
