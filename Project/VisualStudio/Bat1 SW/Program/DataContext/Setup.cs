﻿// Decimal separator

using BatLibrary;

namespace DataContext
{
   public class Setup
   {
      /// <summary>
      /// Selected SW language
      /// </summary>
      public SwLanguage Language;

      /// <summary>
      /// Units last used during entering manual results
      /// </summary>
      public Units ManualUnits;

      /// <summary>
      /// COM port for old scales
      /// </summary>
      public int ScaleVersion6ComPort;

      /// <summary>
      /// Manually selected units for old scales
      /// </summary>
      public Units ScaleVersion6Units;

      /// <summary>
      /// Units last used during entering a growth curve
      /// </summary>
      public Units CurveUnits;

      /// <summary>
      /// Export format used last time
      /// </summary>
      public ExportFormat ExportFormat;

      /// <summary>
      /// CSV export config
      /// </summary>
      public CsvConfig CsvConfig;

      /// <summary>
      /// Excel CSV template
      /// </summary>
      public readonly CsvConfig CsvConfigExcelTemplate;

      /// <summary>
      /// Open Office CSV template
      /// </summary>
      public readonly CsvConfig CsvConfigOpenOfficeTemplate;

      /// <summary>
      /// Constructor
      /// </summary>
      public Setup()
      {
         // Vytvorim sablonu CSV pro Excel
         CsvConfigExcelTemplate.Template = CsvTemplate.EXCEL;
         CsvConfigExcelTemplate.DecimalSeparator = CsvDecimalSeparator.DOT; // Excel chce jen tecku
         CsvConfigExcelTemplate.Delimiter = CsvDelimiter.TAB; // Excel chce jen tabulator
         CsvConfigExcelTemplate.Encoding = CsvEncoding.UTF16; // UTF8 nefunguje

         // Vytvorim sablonu CSV pro Open Office
         CsvConfigOpenOfficeTemplate.Template = CsvTemplate.OPEN_OFFICE;
         CsvConfigOpenOfficeTemplate.DecimalSeparator = CsvDecimalSeparator.DEFAULT;
         CsvConfigOpenOfficeTemplate.Delimiter = CsvDelimiter.COMMA; // V OpenOffice je default carka
         CsvConfigOpenOfficeTemplate.Encoding = CsvEncoding.UTF16; // Funguje i UTF8, necham radsi UTF16

         // Nastavim defaulty
         SetDefaults();
      }

      /// <summary>
      /// Set default values
      /// </summary>
      public void SetDefaults()
      {
         Language = SwLanguage.UNDEFINED;

         ManualUnits = Units.KG;

         ScaleVersion6ComPort = 1;
         ScaleVersion6Units = Units.KG;

         CurveUnits = Units.KG;

         ExportFormat = ExportFormat.BAT1;

         CsvConfig = CsvConfigExcelTemplate; // Struktury muzu kopirovat primo
      }

      internal static Setup FromDbSetup(DataModel.Setup dbSetup)
      {
         var setup = new Setup();
         // Jazyk
         if (dbSetup.Language == null)
         {
            setup.Language = SwLanguage.UNDEFINED; // Muze byt null, tento sloupec jsem pridaval v upgradu
         }
         else
         {
            setup.Language = (SwLanguage) (short) dbSetup.Language;
            if (setup.Language < 0 || setup.Language >= SwLanguage._COUNT)
            {
               setup.Language = SwLanguage.UNDEFINED; // Neplatna hodnota
            }
         }

         // Dalsi parametry
         setup.ManualUnits = (Units) dbSetup.ManualUnits;
         setup.ScaleVersion6ComPort = dbSetup.ScaleVersion6ComPort;
         setup.ScaleVersion6Units = (Units) dbSetup.ScaleVersion6Units;
         setup.CurveUnits = (Units) dbSetup.CurveUnits;
         setup.ExportFormat = (ExportFormat) dbSetup.ExportFormat;
         setup.CsvConfig.Template = (CsvTemplate) dbSetup.CsvTemplate;
         setup.CsvConfig.Delimiter = (CsvDelimiter) dbSetup.CsvDelimiter;
         setup.CsvConfig.DecimalSeparator = (CsvDecimalSeparator) dbSetup.CsvDecimalSeparator;
         setup.CsvConfig.Encoding = (CsvEncoding) dbSetup.CsvEncoding;
         return setup;
      }

      internal void ToDbSetup(ref DataModel.Setup dbSetup)
      {
         dbSetup.MajorVersion = DatabaseVersion.MAJOR;
         dbSetup.MinorVersion = DatabaseVersion.MINOR;
         dbSetup.DatabaseVersion = DatabaseVersion.DATABASE;
         dbSetup.BuildVersion = DatabaseVersion.BUILD;
         dbSetup.Language = (short) Language;
         dbSetup.ManualUnits = (byte) ManualUnits;
         dbSetup.ScaleVersion6ComPort = (byte) ScaleVersion6ComPort;
         dbSetup.ScaleVersion6Units = (byte) ScaleVersion6Units;
         dbSetup.CurveUnits = (byte) CurveUnits;
         dbSetup.ExportFormat = (byte) ExportFormat;
         dbSetup.CsvTemplate = (byte) CsvConfig.Template;
         dbSetup.CsvDelimiter = (byte) CsvConfig.Delimiter;
         dbSetup.CsvDecimalSeparator = (byte) CsvConfig.DecimalSeparator;
         dbSetup.CsvEncoding = (byte) CsvConfig.Encoding;
      }
   }
}