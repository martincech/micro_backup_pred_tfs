﻿namespace DataContext {
    /// <summary>
    /// Codes for supported languages
    /// </summary>
    public enum SwLanguage {
        UNDEFINED,      // Poradi se nesmi menit
        ENGLISH,        
        CZECH,
        PORTUGUESE,
        SPANISH,
        GERMAN,
        FRENCH,
        FINNISH,
        RUSSIAN,
        TURKISH,
        JAPANESE,
        POLISH,
        ITALIAN,
        _COUNT
    }
}
