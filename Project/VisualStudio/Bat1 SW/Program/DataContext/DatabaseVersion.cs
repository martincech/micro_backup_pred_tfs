﻿using System.Globalization;
using System.Reflection;

namespace DataContext
{
   /// <summary>
   /// Current SW version - static part
   /// Holder for version of SW that used the database last time.
   /// </summary>
   public struct DatabaseVersion
   {
      #region Database version as readed from Db

      /// <summary>
      /// Major version number
      /// </summary>
      public int Major;

      /// <summary>
      /// Minor version number
      /// </summary>
      public int Minor;

      /// <summary>
      /// Database version number
      /// </summary>
      public int Database;

      /// <summary>
      /// Build number
      /// </summary>
      public int Build;

      #endregion

      #region Static constants - current version of database supported by this code

      /// <summary>
      /// Major version number, corresponding to scale major version
      /// </summary>
      // ReSharper disable once InconsistentNaming
      public static short MINOR
      {
         get { return (short) Assembly.GetExecutingAssembly().GetName().Version.Minor; }
      }


      /// <summary>
      /// Minor version number, corresponding to scale minor version
      /// </summary>
      // ReSharper disable once InconsistentNaming
      public static short MAJOR
      {
         get { return (short) Assembly.GetExecutingAssembly().GetName().Version.Major; }
      }

      /// <summary>
      /// Database version number, changed when the database structure changes
      /// </summary>
      // ReSharper disable once InconsistentNaming
      public static short DATABASE
      {
         get { return (short) Assembly.GetExecutingAssembly().GetName().Version.Build; }
      }

      /// <summary>
      /// Build number
      /// </summary>
      // ReSharper disable once InconsistentNaming
      public static short BUILD
      {
         get { return (short) Assembly.GetExecutingAssembly().GetName().Version.Revision; }
      }

      /// <summary>
      /// Get version string, for example "7.0.0.0"
      /// </summary>
      /// <returns>Version string</returns>
      public new static string ToString()
      {
         return MAJOR.ToString(CultureInfo.InvariantCulture) + "." + MINOR.ToString(CultureInfo.InvariantCulture) + "." +
                DATABASE.ToString(CultureInfo.InvariantCulture) + "." + BUILD.ToString(CultureInfo.InvariantCulture);
      }

      #endregion
   }
}
