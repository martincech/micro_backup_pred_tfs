namespace DataContext
{
   /// <summary>
   /// Record source in the database
   /// </summary>
   public enum RecordSource {
      SAVE,               // Normalne ulozene vazeni v programu
      IMPORT              // Importovane vazeni
   }
}