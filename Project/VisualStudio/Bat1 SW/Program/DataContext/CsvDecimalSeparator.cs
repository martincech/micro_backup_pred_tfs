namespace DataContext
{
   /// <summary>
   /// CSV decimal separator
   /// </summary>
   public enum CsvDecimalSeparator {
      DEFAULT,    // Default nastavene ve Windows
      DOT,
      COMMA
   }
}