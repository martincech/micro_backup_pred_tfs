﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using Bat1Library;
using Bat1Library.Statistics;
using BatLibrary;
using DataModel;
using Utilities.Extensions;
using Histogram = DataModel.Histogram;

namespace DataContext
{
   public class BatContext
   {
      #region Private & protected members

      private readonly DateTime DATETIME_MAX = new DateTime(9999, 12, 31, 23, 59, 59); // SqlServerCe has problem with DateTime.MaxValue
      private readonly DateTime DATETIME_MIN = new DateTime(1753, 1, 1, 0, 0, 0);      // min value for sql compact is year 1753

      private BatModelContainer tmpContainer;

      private BatModelContainer Container
      {
         get
         {
            Debug.Assert(tmpContainer != null, "Container not set!");
            return tmpContainer;
         }
         set { tmpContainer = value; }
      }

      private BatModelContainer CreateContainer()
      {  
         Database.SetInitializer(new MigrateDatabaseToLatestVersion<BatModelContainer, DataModel.Migrations.Configuration>(true));
         Container = string.IsNullOrEmpty(ConnectionString)
            ? new BatModelContainer()
            : new BatModelContainer(ConnectionString);
         return Container;
      }

      protected string ConnectionString { get; set; }
      public static string connectionString; // connection string is required by Flock, that haven't acces to ConnectionString

      #endregion

      #region Constructors

      public BatContext()
         : this(null)
      {
      }

      public BatContext(string connectionString)
      {
         ConnectionString = connectionString;
      }

      #endregion

      #region public interface

      /// <summary>
      /// Check if database exists
      /// </summary>
      /// <returns>True if exists</returns>
      public bool Exists()
      {      
         using (CreateContainer())
         {
            return Container.Database.Exists();
         }                
      }

      /// <summary>
      /// Create database
      /// </summary>
      public void CreateDatabase()
      {
         using (CreateContainer())
         {
            if (Container.Database.Exists())
            {
               return;
            }
            Container.Database.CreateIfNotExists();
            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Create all tables, actually here all tables are created by <see cref="CreateDatabase"/> now.
      /// </summary>
      public void CreateTables()
      {
         //CreateDatabase();
      }

      /// <summary>
      /// Check if setup is valid
      /// </summary>
      public void CheckSetup()
      {
         // Vytvorim tabulku znovu a vyplnim default hodnoty
         using (CreateContainer())
         {
            // Zkontroluju, zda tabulka existuje
            // Tabulka musi obsahovat prave 1 radek
            if (Container.Setups.Count() == 1) return;

            foreach (var setup in Container.Setups.ToList())
            {
               Container.Setups.Remove(setup);
            }
            // Vytvorim tabulku znovu a vyplnim default hodnoty
            Container.SaveChanges();
         }
         SaveSetup(new Setup());
      }

      /// <summary>
      /// Update table definition
      /// </summary>
      public bool Update()
      {
         using (CreateContainer())
         {
            // Nactu verzi programu, ktery pracoval s DB naposledy
            var databaseVersion = LoadVersion();

            // Pokud s DB uz pracoval nejaky novejsi SW, odmitnu DB spustit pod touto verzi SW.
            // DB by mohla obsahovat novejsi jazyky, novejsi polozky configu nebo muze mit DB zmenenou strukturu,
            // kterou tento SW nepodporuje. Uzivatel musi nejdriv upgradovat SW.
            if (databaseVersion.Minor > DatabaseVersion.MINOR || databaseVersion.Database > DatabaseVersion.DATABASE)
            {
               return false; // DB je novejsi nez SW, nelze ji pouzit
            }
            UpdateDb();
            SaveVersion();
            Container.SaveChanges();
            return true;
         }
      }

      /// <summary>
      /// Read info of all weighings
      /// </summary>
      /// <returns>List of WeighingSearchInfo</returns>
      public List<WeighingSearchInfo> ReadWeighingSearchInfo()
      {
         var weighingInfoList = new List<WeighingSearchInfo>();
         using (CreateContainer())
         {
            weighingInfoList.AddRange(Container.WeighingFiles.OfType<SampleResult>().ToList().Select(
               weighing => new WeighingSearchInfo
               {
                  Id = weighing.Id,
                  MinDateTime =
                     weighing.SamplesMinDateTime ?? DATETIME_MIN,
                  MaxDateTime =
                     weighing.SamplesMaxDateTime ?? DATETIME_MAX,
                  ScaleName = weighing.ScaleConfig.ScaleName,
                  FileName = weighing.Name,
                  Note = weighing.Note
               }));

            weighingInfoList.AddRange(Container.WeighingFiles.OfType<ManualResult>().ToList().Select(
               weighing => new WeighingSearchInfo
               {
                  Id = weighing.Id,
                  MinDateTime = weighing.OriginDateTime,
                  MaxDateTime = DATETIME_MAX,
                  ScaleName = weighing.ScaleConfig.ScaleName,
                  FileName = weighing.Name,
                  Note = weighing.Note
               }));
         }
         return weighingInfoList;
      }

      /// <summary>
      /// Read list of all weighings sorted by weighing date/time
      /// </summary>
      /// <returns>List of weighings</returns>
      public List<Weighing> ReadWeighingList()
      {
         // Import weighings from old version

         throw new Exception("TODO");
         //var list = new List<Weighing>();
         //using (CreateContainer())
         //{
         //   list.AddRange(container.Weighings.Select(w => w.WeighingId).Select(id => LoadWeighing(id)));
         //}
         //return list;
      }

      /// <summary>
      /// Read list of weighing Ids that belong to a specified flock and falls within specified time filter. The list is sorted by date.
      /// </summary>
      /// <param name="flockDefinition">Flock definition</param>
      /// <param name="fromDate">Additional time filter (or DateTime.MinValue when NA)</param>
      /// <param name="toDate">Additional time filter (or DateTime.MaxValue when NA)</param>
      /// <returns>List of Ids</returns>
      public List<long> ReadWeighingList(FlockDefinition flockDefinition, DateTime fromDate = default(DateTime),
         DateTime toDate = default(DateTime))
      {
         if (fromDate == default(DateTime))
         {
            fromDate = DATETIME_MIN;
         }
         if (toDate == default(DateTime))
         {
            toDate = DATETIME_MAX;
         }
         var idList = new List<long>();

         // Pokud hejno neobsahuje zadne soubory, vratim rovnou prazdny seznam
         if (flockDefinition.FlockFileList.Count == 0)
         {
            return idList;
         }

         using (CreateContainer())
         {
            var weighings = Container.WeighingFiles.OfType<SampleResult>().ToList()
               .Where(w => flockDefinition.FlockFileList.Any(ff => ff.FileName.Equals(w.Name) &&
                                                                   w.Id >= ff.Id))
               .OrderBy(w => w.SamplesMinDateTime)
               .ThenBy(w => w.Name)
               .Select(w => (long) w.Id).ToList();

            var manualWeighings = Container.WeighingFiles.OfType<ManualResult>().ToList()
               .Where(w => flockDefinition.FlockFileList.Any(ff => ff.FileName.Equals(w.Name) && ff.Id == w.Id))
               .OrderBy(w => w.Name)
               .Select(w => (long) w.Id).ToList();

            weighings.AddRange(manualWeighings);
            return weighings;
         }
      }

      /// <summary>
      /// Delete weighing
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <returns>True is successful</returns>
      public bool DeleteWeighing(long weighingId)
      {
         var list = new List<long> {weighingId};
         return DeleteWeighings(list);
      }

      /// <summary>
      /// Delete list of weighings in one transaction
      /// </summary>
      /// <param name="weighingIdList">List of weighing Ids to delete</param>
      public bool DeleteWeighings(List<long> weighingIdList)
      {
         using (CreateContainer())
         {
            foreach (var weighingId in weighingIdList)
            {
               var weighing = Container.WeighingFiles.Find(weighingId);
               if (weighing == null)
               {
                  continue;
               }
               // Globalni config vahy, vcetne seznamu souboru a skupin muzu smazat pouze pokud uz config
               // neni pouzity u zadneho jineho vazeni. Pokud je config pouzity jeste jinde, musim ho
               // v databazi ponechat. Smaze se az pri smazani posledniho vazeni, ktere tento config pouziva.
               if (weighing.ScaleConfig.Files.Count == 1)
               {
                  DeleteScaleConfig(weighing.ScaleConfigScaleConfigId);
               }
               else
               {
                  // delete just weighingFile                       
                  var file = Container.WeighingFiles.FirstOrDefault(f => f.Id == weighing.Id);
                  if (file != null)
                  {
                     DeleteFileFromGroup(file);
                     file.Flocks.Clear();
                     DeleteSamples(file.Id);
                     DeleteStatistics(file.Id);
                     Container.WeighingFiles.Remove(file);
                  }
               }
            }
            return Container.SaveChanges() != 0;
         }
      }

      /// <summary>
      /// Remove file from list of flocks.
      /// </summary>
      /// <param name="file">removing file</param>
      private void RemoveFileFromFlock(WeighingFile file)
      {
         // load flocks is required when delete file belonging some flock
         var flocks = Container.Flocks.Select(f => file.Flocks.Any(m => m.FlockId == f.FlockId));
         file.Flocks.Clear();
      }

      /// <summary>
      /// Remove file from list of groups.
      /// </summary>
      /// <param name="file">removing file</param>
      private void RemoveFileFromGroup(WeighingFile file)
      {
         // load groups is required when delete file belonging some group
         var groups = Container.Groups.Select(f => file.Groups.Any(g => g.GroupId == f.GroupId));
         file.Groups.Clear();
      }

      /// <summary>
      /// Update existing manual result (Bat2 archive)
      /// </summary>
      /// <param name="id">manual reasult id</param>
      /// <param name="result">statistic result</param>
      public void UpdateManualResult(long id, StatisticResult result, double histogramStep)
      {
         using (CreateContainer())
         {
            var scale = Container.ScaleConfigs
                  .OfType<DataModel.Bat2Scale>().First(s => s.Files.Any(f => f.Id == id));
            
            scale.HistogramStep = histogramStep;
            UpdateManualResult(id, result.WeighingStarted);
            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Update file name, file note, weighing note and sample list of a specified weighing
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <param name="fileName">New file name</param>
      /// <param name="fileNote">New file note</param>
      /// <param name="weighingNote">New weighing note</param>
      /// <param name="sampleList">New samples</param>
      /// <param name="scaleConfig">ScaleConfig with new units</param>
      public void UpdateWeighing(long weighingId, string fileName, string fileNote, string weighingNote,
         SampleList sampleList, ScaleConfig scaleConfig)
      {
         using (CreateContainer())
         {
            // Nactu Id souboru
            var fileId = ReadFileId(weighingId);
            if (fileId < 0)
            {
               return; // Nenasel jsem
            }

            // Updatuju jmeno a poznamku souboru
            Update(fileId, fileName, fileNote);

            // Updatuju poznamku a rozsah datumu vazeni
            UpdateWeighing(weighingId, weighingNote, sampleList.MinDateTime, sampleList.MaxDateTime);

            // Smazu vsechny vzorky
            DeleteSamples(weighingId);

            // Ulozim nove vzorky 
            var weighing = Container.WeighingFiles.OfType<SampleResult>().FirstOrDefault(f => f.Id == weighingId);
            if (weighing == null)
            {
               return;
            }
            SaveSamples(sampleList, weighing, (Units)weighing.ScaleConfig.Units);

            // Pokud zmenil jednotky, musim je zmenit v ScaleConfig. Zaroven musim prepocitat vsechny vzorky, ktere
            // sdili tento ScaleConfig
            long scaleConfigId;
            Units oldUnits;
            ChangeUnitsInConfigRaw(weighingId, scaleConfig, out oldUnits, out scaleConfigId);

            if (oldUnits != scaleConfig.Units.Units)
            {
               // Doslo ke zmene jednotek v configu, musim prepocitat vzorky ve vsech vazenich, ktere sdili tento config
               // Pozor, vazeni s weighingId uz je v DB prepoctene, tj. musim ho vynechat
               ChangeUnitsInSamplesRaw(scaleConfigId, weighingId, oldUnits, scaleConfig.Units.Units);
            }

            Container.SaveChanges();
         }
      }


      /// <summary>
      /// Load complete weighing from database
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <returns>Weighing instance</returns>
      public Weighing LoadWeighing(long weighingId)
      {
         using (CreateContainer())
         {
            var weighingData = LoadWeighingData(weighingId);

            if (weighingData == null)
            {
               return null;
            }

            ScaleType scaleType = ScaleType.SCALE_BAT1;// GetScaleType(weighingId);
            if (weighingData.ScaleConfig is DataContext.Bat2Scale)
            {
               scaleType = ScaleType.SCALE_BAT2;
            }

            if (weighingData.ScaleConfig is DataContext.Bat2OldScale)
            {
               scaleType = ScaleType.SCALE_BAT2OLD;
            }
         
            if (weighingData.SampleList == null)
            {
               // Nactu jeste rucne zadane vysledky
               var weighingResults = new WeighingResults(LoadManualResults(weighingId));
               return new Weighing(weighingData, weighingResults) { ScaleType = scaleType };
            }
            // U vysledku z vahy se vysledky vypocitaji automaticky ze vzorku
            return new Weighing(weighingData) { ScaleType = scaleType };
         }
      }

      /// <summary>
      /// Load sorted list of all file names used in the database
      /// </summary>
      /// <returns>Sorted list of file names</returns>
      public List<FlockFile> LoadFileNameList()
      {
         using (CreateContainer())
         {
            return LoadNameList();
         }
      }

      /// <summary>
      /// Load sorted list of all scale names used in the database
      /// </summary>
      /// <returns>Sorted list of scale names</returns>
      public List<string> LoadScaleNameList()
      {
         using (CreateContainer())
         {
            var list = Container.ScaleConfigs.Select(sc => sc.ScaleName).Where(n => n != null).ToList();
            list.Sort();
            return list;
         }
      }

      /// <summary>
      /// Save manually entered results.
      /// </summary>
      /// <param name="scaleName">Scale name</param>
      /// <param name="file">File name and note</param>      
      /// <param name="startDateTime">Date and time when weighing started</param>   
      /// <param name="units">Units</param>
      /// <param name="resultList">List of results</param>   
      /// <param name="scaleType">scale type</param>
      /// <param name="day">day</param>
      public void SaveWeighing(string scaleName, NameNote file, DateTime startDateTime,
         Units units, List<StatisticResult> resultList, int? day, double histogramStep, ScaleType scaleType = ScaleType.SCALE_BAT1, int serialNumber = -1)
      {
         using (CreateContainer())
         {
            var newFile = Container.WeighingFiles.OfType<ManualResult>()
                  .FirstOrDefault(m => m.Name.Equals(file.Name) &&
                                       DateTime.Compare(m.OriginDateTime, startDateTime) == 0);

            if (newFile == null)
            {
               // Ulozim vysledky              
               newFile = Container.Set<ManualResult>().Create();
               newFile.Name = file.Name;


               if (scaleType != ScaleType.SCALE_BAT1)
               {
                  if (serialNumber == -1)
                  {
                     if (!Int32.TryParse(file.Name, out serialNumber))
                     {
                        throw new Exception("Cannot parse serial number!");
                     }
                     newFile.Name = serialNumber + "_" + startDateTime.ToShortDateString() + "_" +
                                    startDateTime.ToShortTimeString();
                  }
               }

               // Ulozim nastaveni vahy
               SaveScaleConfig(scaleName, scaleType, units, startDateTime, serialNumber, newFile);
            }
            else
            {
               //if file exist, must load scale config for correct update file in database
               var scaleConfig =
                  Container.ScaleConfigs.FirstOrDefault(s => s.ScaleConfigId == newFile.ScaleConfigScaleConfigId);
            }

            newFile.Note = file.Note;
            newFile.ScaleConfig.HistogramStep = histogramStep;

            foreach (var result in resultList.Where(result => result.Flag != Flag.ALL))
            {
               newFile.OriginDateTime = startDateTime;
               newFile.Day = day;
      
               var stat = newFile.Statistics.FirstOrDefault(s => s.Flag == (byte)result.Flag);
               if (stat == null)
               {
                  stat = new Statistic();
                  newFile.Statistics.Add(stat);
               }              

               stat.Flag = (byte)result.Flag;
               stat.Count = (short)result.Count;         
               stat.Average = ConvertWeight.Convert(result.Average, units, Units.G);
               stat.Sigma = ConvertWeight.Convert(result.Sigma, units, Units.G);
               stat.Cv = result.Cv;
               stat.Uniformity = StatisticResult.ReCalcUniformity(result.Uniformity);

               if (result.Gain != null)
               {
                  stat.Gain = ConvertWeight.Convert(result.Gain.Value, units, Units.G);
               }                        
               if (result.Histogram != null)
               {
                  stat.Histograms = ConvertHistogramToCollectionCounts(result.Histogram);                 
               }
            }
            Container.SaveChanges();
         }
      }

      private void SaveScaleConfig(string scaleName, ScaleType scaleType, Units units, DateTime startDateTime, int serialNumber,
         ManualResult file)
      {
         object dbConfig;
         if (scaleType == ScaleType.SCALE_BAT2)
         {
            var config2 = new Bat2Scale(units)
            {
               ScaleName = scaleName
            };
        
            dbConfig = GetDefaultScaleConfig<Bat2Scale, DataModel.Bat2Scale>(config2);
            ((DataModel.Bat2Scale) dbConfig).SerialNumber = serialNumber;
           // file.Name = serialNumber+"_" + startDateTime.ToShortDateString() + "_" + startDateTime.ToShortTimeString();
            ((DataModel.Bat2Scale) dbConfig).Files.Add(file);
         }
         else if (scaleType == ScaleType.SCALE_BAT2OLD)
         {
            var config2Old = new Bat2OldScale(units)
            {
               ScaleName = scaleName
            };

            dbConfig = GetDefaultScaleConfig<Bat2OldScale, DataModel.Bat2OldScale>(config2Old);
            //file.Name += "_" + startDateTime.ToShortDateString();
            ((DataModel.Bat2OldScale) dbConfig).Files.Add(file);
         }
         else if (scaleType == ScaleType.SCALE_BAT1)
         {
            var config1 = new Bat1Scale(units)
            {
               SwVersion = {Major = 0, Minor = 0, Build = 0},
               ScaleName = scaleName,
               HwVersion = 0
            };
            dbConfig = GetDefaultScaleConfig<Bat1Scale, DataModel.Bat1Scale>(config1);
            ((DataModel.Bat1Scale) dbConfig).Files.Add(file);
         }
      }

      public void UpdateWeighing(Weighing weighing)
      {
         using (CreateContainer())
         {
            Bat1Scale config = (Bat1Scale) weighing.WeighingData.ScaleConfig;
            var dbConfig = GetScaleConfig(config);

            SaveWeighing(weighing, dbConfig);
            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Save list of weighings read from the scale. All weighings share the same ScaleConfig obtained
      /// form the first weighing.
      /// </summary>
      /// <param name="weighingList">Weighing list</param>
      public void SaveWeighingList(List<Weighing> weighingList)
      {
         // Vazeni nactena z vahy sdili jedno nastaveni vahy, proto je nutne ukladat seznam souboru najednou.
         // Nastaveni vahy se vytvori jen jedno (vcetne seznamu souboru a skupin), jednotliva vazeni se pak
         // odkazuji na config a prislusny soubor v ramci configu.   

         if (weighingList.Count == 0)
         {
            return;
         }

         using (CreateContainer())
         {
            //create scale config
            Bat1Scale config = (Bat1Scale) weighingList.First().WeighingData.ScaleConfig;
            var dbConfig = GetScaleConfig(config);

            // Weighings
            List<DataModel.File> insertFiles = SaveWeighings(weighingList, dbConfig);

            //Groups         
            AddGroups(config.FileGroupList.List, dbConfig, insertFiles, config.FileList.List);

            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Save groups to database.
      /// </summary>
      /// <param name="groups">loaded groups from scale</param>
      /// <param name="dbConfig">scale config</param>
      /// <param name="insertFiles">scale's files</param>
      /// <param name="files">list of all files</param>
      private void AddGroups(List<FileGroup> groups, DataModel.Bat1Scale dbConfig, List<DataModel.File> insertFiles,
         List<File> files)
      {
         foreach (var group in groups)
         {
            if (group.Name == "")
            {
               throw new Exception("Group name cannot be empty");
            }

            var dbGroup = Container.Groups.FirstOrDefault(g =>
               g.ScaleConfigId == dbConfig.ScaleConfigId && g.Name.Equals(group.Name));
            if (dbGroup == null)
            {
               dbGroup = Container.Groups.Create();
               dbGroup.Name = group.Name;
               dbConfig.Groups.Add(dbGroup);
            }
            dbGroup.Note = group.Note;

            var fileIdInGroupList = group.FileIndexList.Select(fileIndex => files[fileIndex]).ToList();
            foreach (
               var file in
                  fileIdInGroupList.Select(item => insertFiles.FirstOrDefault(f => f.Name.Equals(item.Name)))
                     .Where(file => file != null))
            {
               dbGroup.WeighingFiles.Add(file);
            }
         }
      }

      /// <summary>
      /// Add predefined config on PC to the table.
      /// </summary>
      /// <param name="config">Scale config</param>
      /// <param name="name">Name of predefined config on PC</param>
      /// <returns>Config Id</returns>
      private TU GetDefaultScaleConfig<T, TU>(T config, string name = "")
         where T : ScaleConfig
         where TU : DataModel.ScaleConfig
      {
         var dbConfig = Container.ScaleConfigs.OfType<TU>().FirstOrDefault(c => c.Name.Equals(null) &&
                                                                                c.ScaleName.Equals(config.ScaleName));
         if (dbConfig != null)
         {
            return dbConfig;
         }
         dbConfig = Container.Set<TU>().Create();
         dbConfig = (TU) config.ToDbScaleConfig(dbConfig);
         dbConfig.Name = null;
         Container.ScaleConfigs.Add(dbConfig);
         return dbConfig;
      }

      /// <summary>
      /// Load from database or create new scale config.
      /// </summary>
      /// <param name="config">configuration from scale</param>
      /// <returns>database scale configuration</returns>
      private DataModel.Bat1Scale GetScaleConfig(Bat1Scale config)
      {
         var dbConfig = Container.ScaleConfigs.OfType<DataModel.Bat1Scale>().FirstOrDefault(
            c => c.Name.Equals(null) && c.ScaleName.Equals(config.ScaleName));
            //TODO: misto scaleName pouzit serialNumber
         if (dbConfig == null)
         {
            dbConfig = Container.Set<DataModel.Bat1Scale>().Create();
            dbConfig = config.ToDbScaleConfig(dbConfig);
            dbConfig.Name = null;
            Container.ScaleConfigs.Add(dbConfig);
         }
         return dbConfig;
      }


      /// <summary>
      /// Save weighings to database.
      /// </summary>
      /// <param name="weighingList">list of weighings</param>
      /// <param name="dbConfig">database scale configuration</param>
      private List<DataModel.File> SaveWeighings(List<Weighing> weighingList, DataModel.ScaleConfig dbConfig)
      {
         return weighingList.Select(weighing => SaveWeighing(weighing, dbConfig)).ToList();
      }

      private DataModel.File SaveWeighing(Weighing weighing, DataModel.ScaleConfig dbConfig)
      {
         var weighingData = weighing.WeighingData;
         var file =
            Container.WeighingFiles.OfType<DataModel.File>().FirstOrDefault(f => f.Name == weighingData.File.Name &&
                                                                                 f.ScaleConfigScaleConfigId ==
                                                                                 dbConfig.ScaleConfigId);
         if (file == null)
         {
            file = Container.Set<DataModel.File>().Create();
            var f = weighingData.ScaleConfig.FileList.List.FirstOrDefault(x => x.Name.Equals(weighingData.File.Name));
            if (f != null)
            {
               f.ToDbFile(ref file);
            }
            dbConfig.Files.Add(file);
            // Soubor jsem nenasel (nemelo by nastat, soubor jsem pred chvili ulozil)          
         }

         file.Note = weighingData.Note;
         file.SwMajorVersion = DatabaseVersion.MAJOR;
         file.SwMinorVersion = DatabaseVersion.MINOR;
         file.SwBuildVersion = DatabaseVersion.BUILD;
         file.SamplesMinDateTime = weighingData.SampleList.MinDateTime;
         file.SamplesMaxDateTime = weighingData.SampleList.MaxDateTime;

         //Samples
         SaveSamples(weighingData.SampleList, file, (Units)dbConfig.Units);

         return file;
      }

      private void SaveImportedWeighing(Weighing weighing, DataModel.ScaleConfig dbConfig)
      {
         var weighingData = weighing.WeighingData;
         var file = Container.WeighingFiles.OfType<SampleResult>().FirstOrDefault(f =>
            f.Name == weighingData.File.Name &&
            f.ScaleConfigScaleConfigId == dbConfig.ScaleConfigId &&
            DateTime.Compare(f.SamplesMinDateTime.Value, weighingData.SampleList.MinDateTime) == 0);
         if (file == null)
         {
            file = Container.Set<SampleResult>().Create();
            var f = weighingData.ScaleConfig.FileList.List.FirstOrDefault(x => x.Name.Equals(weighingData.File.Name));
            if (f != null)
            {
               f.ToDbFile(ref file);
            }
            dbConfig.Files.Add(file);
         }

         // Copy scale config from weighing
         var scale = dbConfig as DataModel.Bat1Scale;
         if (scale != null)
         {
            Bat1Scale.FromWeighing(ref scale, weighing);
         }
         
         file.Note = weighingData.Note;
         file.SamplesMinDateTime = weighingData.SampleList.MinDateTime;
         file.SamplesMaxDateTime = weighingData.SampleList.MaxDateTime;

         //Samples        
         SaveSamples(weighingData.SampleList, file, (Units)dbConfig.Units);
      }

      /// <summary>
      /// Save samples to database.
      /// </summary>
      /// <param name="sampleList">list of samples</param>
      /// <param name="dbWeighing">Samples owner</param>
      private void SaveSamples(SampleList sampleList, SampleResult dbWeighing, Units units)
      {
         dbWeighing.Samples.Clear();
         Container.Samples.RemoveRange(Container.Samples.Where(w => w.SampleResult.Id == dbWeighing.Id));

         sampleList.First();
         while (sampleList.Read())
         {
            var sample = Container.Samples.Create();
            sample.Weight = Math.Round(ConvertWeight.Convert(sampleList.Sample.Weight, units, Units.G), 4);
            sample.Flag = (byte) sampleList.Sample.Flag;
            sample.SavedDateTime = sampleList.Sample.DateTime;

            dbWeighing.Samples.Add(sample);
         }
      }

      /// <summary>
      /// Save imported weighing.
      /// </summary>
      /// <param name="weighing">Weighing to save</param>
      public void SaveWeighing(Weighing weighing)
      {
         using (CreateContainer())
         {
            if (weighing.WeighingData.SampleList == null)
            {
               // Manual result data
               SaveWeighing(weighing.WeighingData.ScaleConfig.ScaleName,
                  new NameNote(weighing.WeighingData.File.Name, weighing.WeighingData.File.Note),
                  weighing.GetMinDateTime(),
                  weighing.WeighingData.ScaleConfig.Units.Units,
                  weighing.WeighingResults.StatisticResultList,
                  weighing.FlockData.Day,
                  weighing.WeighingData.ScaleConfig.StatisticConfig.Histogram.Step,
                  weighing.ScaleType);
               return;
            }

            // Only Bat1 imported data can be non manual
            // Save scale's config including list of files and groups
            var config = new Bat1Scale {ScaleName = weighing.WeighingData.ScaleConfig.ScaleName};
            var dbConfig = GetScaleConfig(config);
            // set dbConfig from weighing
            //TODO
            SaveImportedWeighing(weighing, dbConfig);

            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Load sorted list of all file names and last used notes from the database
      /// </summary>
      /// <returns>List of file names and notes</returns>
      public List<NameNote> LoadFileNameNoteList()
      {
         using (CreateContainer())
         {
            return LoadNameNoteList();
         }
      }

      /// <summary>
      /// Check if weighing with specified file name and starting datetime exists
      /// </summary>
      /// <param name="fileName">File name</param>
      /// <param name="weighingStarted">Datetime when the weighing started</param>
      /// <param name="weighingId">Weighing ID of existing weighing</param>
      /// <returns>True if exists</returns>
      public bool WeighingExists(string fileName, DateTime weighingStarted, out long weighingId)
      {
         using (CreateContainer())
         {
            var weighing = Container.WeighingFiles.OfType<SampleResult>().FirstOrDefault(
               w => DateTime.Compare(w.SamplesMinDateTime.Value, weighingStarted) == 0 && w.Name.Equals(fileName));
            if (weighing != null)
            {
               weighingId = weighing.Id;
               return true;
            }

            //check if weiging is manual result
            var manual = Container.WeighingFiles.OfType<ManualResult>().FirstOrDefault(
                  m => DateTime.Compare(m.OriginDateTime, weighingStarted) == 0 && m.Name.Equals(fileName));
            if (manual != null)
            {
               weighingId = manual.Id;
               return true;
            }        
         }
         weighingId = -1;
         return false;
      }


      /// <summary>
      /// Check if bat2 old weighing with specified file name, starting date time, 
      /// day number and sex's flag exists.
      /// </summary>    
      /// <param name="weighing">Weighing</param>
      /// <returns>True if exists</returns>
      public bool WeighingExists(Weighing weighing)
      {
         using (CreateContainer())
         {
            //check if weiging is manual result
            var weighingStarted = weighing.GetMinDateTime();
            var manual = Container.WeighingFiles.OfType<ManualResult>().FirstOrDefault(
                  m => DateTime.Compare(m.OriginDateTime, weighingStarted) == 0 &&
                       m.Name.Equals(weighing.WeighingData.File.Name)
                  );

            if (manual != null)
            {  //check day and flag                     
               if (manual.Day != weighing.FlockData.Day)
               {
                  return true;
               }

               foreach (var stat in from flag in weighing.WeighingResults.FlagList 
                                    where flag != Flag.ALL 
                                    select manual.Statistics.FirstOrDefault(s => s.Flag == (byte)flag))
               {
                  return stat != null;
               }            
            }
         }
         return false;
      }


      /// <summary>
      /// Load setup from database
      /// </summary>
      /// <returns>Setup instance</returns>
      public Setup LoadSetup()
      {
         using (CreateContainer())
         {
            return Setup.FromDbSetup(GetSetup());
         }
      }

      /// <summary>
      /// Save predefined scale config, including files and groups into the database
      /// </summary>
      /// <param name="scaleConfig">Scale config</param>
      /// <param name="name">Name of predefined config</param>
      /// <returns>Config ID</returns>
      public long SaveScaleConfig<T>(T scaleConfig, string name = null)
         where T : ScaleConfig
      {
         using (CreateContainer())
         {
            long configId;
            if (typeof (T) == typeof (Bat1Scale))
            {
               configId = SaveScaleConfigRaw<T, DataModel.Bat1Scale>(scaleConfig, name);
            }
            else
            {
               configId = SaveScaleConfigRaw<T, DataModel.Bat2Scale>(scaleConfig, name);
            }

            Container.SaveChanges();
            return configId;
         }
      }

      /// <summary>
      /// Check if a predefined scale config exists
      /// </summary>
      /// <param name="name">Config name</param>
      /// <returns>True if exists</returns>
      public bool ScaleConfigExists(string name)
      {
         using (CreateContainer())
         {
            return Container.ScaleConfigs.Any(sc => sc.Name.Equals(name));
         }
      }

      /// <summary>
      /// Delete predefined scale config
      /// </summary>
      /// <param name="name">Config name</param>
      public void DeleteScaleConfig(string name)
      {
         using (CreateContainer())
         {
            var id = GetScaleConfigId(name);
            // Dale se zahaji nova transakce
            DeleteScaleConfig(id);
            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Load predefined config names
      /// </summary>
      /// <returns>List of config names</returns>
      public List<string> LoadPredefinedScaleConfigNameList()
      {
         using (CreateContainer())
         {
            return LoadScaleConfigNameList();
         }
      }

      /// <summary>
      /// Load complete scale config
      /// </summary>
      /// <param name="scaleConfigId">Scale config ID</param>
      public ScaleConfig LoadScaleConfig(long scaleConfigId)
      {
         using (CreateContainer())
         {
            return LoadScaleConfigRaw(scaleConfigId);
         }
      }

      /// <summary>
      /// Get ID of predefined config
      /// </summary>
      /// <param name="configName">Config name</param>
      /// <returns>Config ID</returns>
      public long GetPredefinedScaleConfigId(string configName)
      {
         using (CreateContainer())
         {
            return GetScaleConfigId(configName);
         }
      }

      /// <summary>
      /// Load a list of all growth curves saved in the database
      /// </summary>
      /// <returns>CurveList instance</returns>
      public CurveList LoadCurveList()
      {
         using (CreateContainer())
         {
            var curveList = new CurveList();
            foreach (var curve in Container.Curves)
            {
               curveList.Add(Curve.FromDbCurve(curve));
            }
            LoadCurvePointsToList(ref curveList);

            return curveList;
         }
      }

      /// <summary>
      /// Add a growth curve including its points into the database. Curve ID will be automatically updated in the curve instance.
      /// </summary>
      /// <param name="curve">Curve to add</param>
      public void SaveCurve(ref Curve curve)
      {
         using (CreateContainer())
         {
            // Definice krivky, zaroven vyplnim ID ulozene krivky
            curve.Id = Add(curve);
            // Body krivky
            AddCurvePoints(curve);
            Container.SaveChanges();
            
            // After save curve to database, id of curve must be update
            var curveName = curve.Name;
            var dbCurve = Container.Curves.First(c => c.Name.Equals(curveName));
            curve.Id = dbCurve.CurveId;
         }
      }

      /// <summary>
      /// Update growth curve including its points in the database
      /// </summary>
      /// <param name="curve">Curve to update</param>
      public void UpdateCurve(Curve curve)
      {
         using (CreateContainer())
         {
            // Definice krivky
            Update(curve);

            // Smazu puvodni body krivky
            DeleteCurvePoints(curve.Id);

            // Ulozim nove body krivky
            AddCurvePoints(curve);
            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Delete growth curve including its points from the database. The curve will be deleted from all flocks as well.
      /// </summary>
      /// <param name="curveId">Curve ID to delete</param>
      public void DeleteCurve(long curveId)
      {
         using (CreateContainer())
         {
            // Vymazu krivku ze vsech hejn, kde je zminovana
            DeleteCurveFromFlock(curveId);

            // Definice krivky
            var curve = Container.Curves.FirstOrDefault(c => c.CurveId == curveId);
            if (curve == null)
            {
               return;
            }
            Container.Curves.Remove(curve);
            // Body krivky
            DeleteCurvePoints(curveId);
            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Read last weighings of all files
      /// </summary>
      /// <returns>List of WeighingSearchInfo</returns>
      public List<WeighingSearchInfo> ReadLastWeighings()
      {
         using (CreateContainer())
         {
            // Nactu seznam jmen souboru, neni treba tridit, na zaver se tridi podle casu
            return Container.WeighingFiles.ToList()
               .Select(weighing =>
               {
                  if (weighing != null)
                  {
                     return new WeighingSearchInfo {Id = weighing.Id};
                  }
                  return new WeighingSearchInfo();
               }).ToList();
         }
      }

      /// <summary>
      /// Delete flock including its files from the database
      /// </summary>
      /// <param name="flockId">Flock ID to delete</param>
      public void DeleteFlock(long flockId)
      {
         using (CreateContainer())
         {
            // Definice hejna
            var flock = Container.Flocks.FirstOrDefault(f => f.FlockId == flockId);
            if (flock == null)
            {
               return;
            }
            flock.WeighingFiles.Clear();
            Container.Flocks.Remove(flock);
            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Load a list of all flocks saved in the database
      /// </summary>
      /// <returns>CurveList instance</returns>
      public FlockDefinitionList LoadFlockList()
      {
         using (CreateContainer())
         {
            var flockDefinitionList = new FlockDefinitionList();
            foreach (var flock in Container.Flocks.ToList())
            {
               flockDefinitionList.Add(FlockDefinition.FromDbFlock(flock));
            }
            // Soubory hejna
            LoadFlockFilesList(ref flockDefinitionList);
            // Teoreticka rustova krivka (pokud je definovana)
            foreach (var flockDefinition in flockDefinitionList.List)
            {
               LoadCurve(ref flockDefinition.CurveDefault);
               LoadCurve(ref flockDefinition.CurveFemales);
            }
            return flockDefinitionList;
         }
      }

      /// <summary>
      /// Add a flock including its files into the database. Flock ID will be automatically updated in the flock instance.
      /// </summary>
      /// <param name="flockDefinition">Flock to add</param>
      public void SaveFlock(ref FlockDefinition flockDefinition)
      {
         using (CreateContainer())
         {
            var flock = AddFlock(flockDefinition);
            flock.ToDateTime = DATETIME_MAX;
            flock.FromDateTime = flockDefinition.Started;
            AddFlockFile(flockDefinition, flock);

            Container.SaveChanges();           
         }
      }

      /// <summary>
      /// Update flock including its files in the database
      /// </summary>
      /// <param name="flockDefinition">Flock to update</param>
      public void UpdateFlock(FlockDefinition flockDefinition)
      {
         using (CreateContainer())
         {
            // Definice hejna
            var flock = Update(flockDefinition);

            // Smazu puvodni soubory hejna
            DeleteFlockFiles(ref flock);

            // Ulozim nove soubory hejna
            flock.FromDateTime = flockDefinition.Started;
            AddFlockFile(flockDefinition, flock);
            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Check if flock is active.
      /// </summary>
      /// <param name="flockName">name of flock</param>
      /// <returns>true - flock is current</returns>
      public bool CheckIsFlockActive(string flockName)
      {
         using (CreateContainer())
         {
            var flock = Container.Flocks.FirstOrDefault(f => f.Name.Equals(flockName));
            if (flock != null && DateTime.Compare(flock.ToDateTime, DATETIME_MAX.Date) >= 0)
            {
               return true;
            }
            return false;
         }
      }

      /// <summary>
      /// Set flock on state current.
      /// </summary>
      /// <param name="name">name of flock</param>
      public void SetFlockCurrent(string name)
      {
         SetFlockDate(name, DATETIME_MAX.Date);
      }

      /// <summary>
      /// Set flock on state closed.
      /// </summary>
      /// <param name="name">name of flock</param>
      /// <param name="closedDateTime">closed dateTime</param>
      public void SetFlockClosed(string name, DateTime closedDateTime)
      {
         SetFlockDate(name, closedDateTime);
      }

      /// <summary>
      /// Set flock's date and time.
      /// </summary>
      /// <param name="name">name of flock</param>
      /// <param name="setDateTime">new dateTime</param>
      private void SetFlockDate(string name, DateTime setDateTime)
      {
         using (CreateContainer())
         {
            var flock = Container.Flocks.FirstOrDefault(f => f.Name.Equals(name));
            if (flock != null)
            {
               flock.ToDateTime = setDateTime;
               Container.SaveChanges();
            }
         }
      }

      /// <summary>
      /// Load number of flocks where specified growth curve is used
      /// </summary>
      /// <param name="curveId">Curve ID</param>
      /// <returns>Number of rows</returns>
      public int FlocksWithCurve(long curveId)
      {
         using (CreateContainer())
         {
            return Container.Flocks.Count(f => f.CurveDefault.CurveId == curveId || f.CurveFemale.CurveId == curveId);
         }
      }

      /// <summary>
      /// Load list of flocks that are curently being weighed
      /// </summary>
      /// <returns>List flock definitions</returns>
      public FlockDefinitionList LoadCurrentFlockList()
      {
         using (CreateContainer())
         {
            // Nactu hejna, ktera se prave vazi
            return LoadFlockDefinitionList(LoadCurrentFlockIds());
         }
      }

      /// <summary>
      /// Return total number of flocks in the database
      /// </summary>
      /// <returns>Number of flocks</returns>
      public int FlockCount()
      {
         using (CreateContainer())
         {
            return Container.Flocks.Count();
         }
      }

      /// <summary>
      /// Save list of selected flocks
      /// </summary>
      /// <param name="flockDefinitionList">Flock list</param>
      public void SaveSelectedFlocks(FlockDefinitionList flockDefinitionList)
      {
         using (CreateContainer())
         {
            var setup = GetSetup();
            setup.SelectedFlocks.Clear();
            foreach (
               var flock in
                  flockDefinitionList.List.Select(flockDefinition => Container.Flocks.Find(flockDefinition.Id)))
            {
               setup.SelectedFlocks.Add(flock);
            }

            Container.SaveChanges();
         }
      }

      /// <summary>
      /// Load list of selected flocks
      /// </summary>
      /// <returns>List of flock definitions</returns>
      public FlockDefinitionList LoadSelectedFlocks()
      {
         using (CreateContainer())
         {
            var setup = GetSetup();
            return LoadFlockDefinitionList(setup.SelectedFlocks.Select(f => f.FlockId).ToList());
         }
      }

      #endregion

      #region CRUD operations

      #region CRUD for Files

      /// <summary>
      /// Add new file
      /// </summary>
      /// <param name="file">File to add</param>
      /// <param name="scaleConfigId">ScaleConfig Id this file belongs to</param>
      /// <returns>Id of new file</returns>
      private long Add(File file, long scaleConfigId)
      {
         return AddFile(file, scaleConfigId);
      }

      /// <summary>
      /// Add file list
      /// </summary>
      /// <param name="fileList">File list to be added</param>
      /// <param name="scaleConfigId">ScaleConfig Id this file belongs to</param>
      /// <returns>List of IDs saved to the database</returns>
      private List<long> Add(FileList fileList, long scaleConfigId)
      {
         var list = fileList.List.Select(file => AddFile(file, scaleConfigId)).ToList();
         return list;
      }


      /// <summary>
      /// Delete all files that belong to specified config
      /// </summary>
      /// <param name="scaleConfigId">Scale config ID</param>
      /// <returns>Number of rows affected</returns>
      private void DeleteFiles(long scaleConfigId)
      {
         throw new Exception("TODO");
         //var filesToDelete = Container.Files.Where(f => f.ScaleConfigId == scaleConfigId);
         //foreach (var file in filesToDelete)
         //{
         //   Container.Files.Remove(file);
         //}
      }

      /// <summary>
      /// Update file name and note
      /// </summary>
      /// <param name="fileId">File Id</param>
      /// <param name="name">New name</param>
      /// <param name="note">New note</param>
      private void Update(long fileId, string name, string note)
      {
         // Jmeno musi byt zadane
         if (name == "")
         {
            throw new Exception("File name cannot be empty");
         }

         var file = Container.WeighingFiles.FirstOrDefault(f => f.Id == fileId);
         if (file == null)
         {
            return;
         }
         if (!file.Name.Equals(name))
         {
            file.Name = name;
         }
         if (!file.Note.Equals(note))
         {
            file.Note = note;
         }
      }

      /// <summary>
      /// Change units in config of all files that belong to specified config
      /// </summary>
      /// <param name="scaleConfigId">specific <see cref="DataModel.ScaleConfig.ScaleConfigId"/> of config to be updated</param>
      /// <param name="scaleConfig"><see cref="ScaleConfig"/> parameters of update</param>
      /// <param name="oldUnits"> old units </param>
      private void ChangeUnits(long scaleConfigId, ScaleConfig scaleConfig, Units oldUnits)
      {
         // V configu musi zustat vsechny puvodni soubory, zmenily se pouze jednotky a nasledne 
         // se prepocetly vsechny hodnoty v globalnim configu a configu jednotlivych souboru

         // Musim updatovat postupne kazdy soubor, u kazdeho souboru muze byt jiny config. Updatuju pouze
         // polozky, na ktere ma vliv zmena jednotek (odpovida fci ScaleConfig.SetNewUnits())
         foreach (var file in scaleConfig.FileList.List)
         {
            var fileCopy = file;
            Container.WeighingFiles.OfType<DataModel.File>()
               .Where(f => f.ScaleConfigScaleConfigId == scaleConfigId && f.Name.Equals(fileCopy.Name))
               .ForEach(
                  f =>
                  {
                     f.LowLimit = fileCopy.FileConfig.WeighingConfig.WeightSorting.LowLimit;
                     f.HighLimit = fileCopy.FileConfig.WeighingConfig.WeightSorting.HighLimit;
                     f.MinimumWeight = fileCopy.FileConfig.WeighingConfig.Saving.MinimumWeight;
                  });
         }
      }

      #endregion

      #region CRUD for Curves

      /// <summary>
      /// Add new curve
      /// </summary>
      /// <param name="curve">Curve</param>
      /// <returns>Id of new curve</returns>
      private long Add(Curve curve)
      {
         // Jmeno musi byt zadane
         if (curve.Name == "")
         {
            throw new Exception("Curve name cannot be empty");
         }

         // Krivka nesmi mit zadany ID. Pokud ma ID vyplneny, krivka uz byla do DB ulozena a musim pouzit Update.
         if (curve.Id > 0)
         {
            throw new Exception("Curve ID > 0");
         }

         var newCurve = Container.Curves.Create();
         newCurve.Name = curve.Name;
         newCurve.Note = curve.Note;
         newCurve.Units = (byte) curve.Units;
         Container.Curves.Add(newCurve);
         return newCurve.CurveId;
      }

      /// <summary>
      /// Update curve data
      /// </summary>
      /// <param name="curve">Curve</param>
      private void Update(Curve curve)
      {
         // Jmeno musi byt zadane
         if (curve.Name == "")
         {
            throw new Exception("Curve name cannot be empty");
         }

         // Krivka uz musi mit zadany ID, tj. uz je v databazi ulozena
         if (curve.Id <= 0)
         {
            throw new Exception("Curve ID <= 0");
         }

         var uCurve = Container.Curves.FirstOrDefault(c => c.CurveId == curve.Id);
         if (uCurve == null)
         {
            return;
         }
         uCurve.Name = curve.Name;
         uCurve.Note = curve.Note;
         uCurve.Units = (byte) curve.Units;
      }

      #endregion

      #region CRUD for CurvePoints

      /// <summary>
      /// Add points of new curve
      /// </summary>
      /// <param name="curve">Curve with valid ID</param>
      private void AddCurvePoints(Curve curve)
      {
         // Ulozim body nove krivky
         if (curve.CurvePoints.Count == 0)
         {
            return; // Krivka neobsahuje zadne body
         }

         foreach (var point in curve.CurvePoints)
         {
            var newPoint = Container.CurvePoints.Create();
            newPoint.CurveId = curve.Id;
            newPoint.DayNumber = (short) point.Day;
            newPoint.Weight = point.Weight;
            Container.CurvePoints.Add(newPoint);
         }
      }

      /// <summary>
      /// Delete points of a specified curve
      /// </summary>
      /// <param name="curveId">Curve Id</param>
      /// <returns>Number of rows affected</returns>
      private void DeleteCurvePoints(long curveId)
      {
         var curve = Container.Curves.FirstOrDefault(c => c.CurveId == curveId);
         if (curve == null)
         {
            return;
         }
         Container.CurvePoints.RemoveRange(curve.CurvePoints);
         curve.CurvePoints.Clear();
      }

      #endregion

      #region CRUD for Flocks

      /// <summary>
      /// Add new flock
      /// </summary>
      /// <param name="flockDefinition">Flock</param>
      /// <returns>Id of new flock</returns>
      private DataModel.Flock AddFlock(FlockDefinition flockDefinition)
      {
         // Jmeno musi byt zadane
         if (flockDefinition.Name == "")
         {
            throw new Exception("Flock name cannot be empty");
         }

         // Hejno nesmi mit zadany ID. Pokud ma ID vyplneny, hejno uz bylo do DB ulozene a musim pouzit Update.
         if (flockDefinition.Id > 0)
         {
            throw new Exception("Flock ID > 0");
         }

         // Ulozim nove hejno
         var flock = Container.Flocks.Create();
         SetFlock(ref flock, flockDefinition);
         Container.Flocks.Add(flock);
         return flock;
      }

      /// <summary>
      /// Update flock data
      /// </summary>
      /// <param name="flockDefinition">Flock</param>
      /// <returns>Number of rows affected</returns>
      private DataModel.Flock Update(FlockDefinition flockDefinition)
      {
         // Jmeno musi byt zadane
         if (flockDefinition.Name == "")
         {
            throw new Exception("Flock name cannot be empty");
         }

         // Hejno uz musi mit zadany ID, tj. uz je v databazi ulozene
         if (flockDefinition.Id <= 0)
         {
            throw new Exception("Flock ID <= 0");
         }

         var flock = Container.Flocks.FirstOrDefault(f => f.FlockId == flockDefinition.Id);
         if (flock == null)
         {
            return null;
         }
         // Updatuju hejno
         SetFlock(ref flock, flockDefinition);
         return flock;
      }

      /// <summary>
      /// Delete specified growth curve from all flocks
      /// </summary>
      /// <param name="curveId">Curve Id</param>
      /// <returns>Number of rows affected</returns>
      private void DeleteCurveFromFlock(long curveId)
      {
         var delFlocks =
            Container.Flocks.Where(f => f.CurveDefault.CurveId == curveId || f.CurveFemale.CurveId == curveId);
         foreach (var flock in delFlocks.ToList())
         {
            if (flock.CurveDefault != null && flock.CurveDefault.CurveId == curveId)
            {
               flock.CurveDefault = null;
            }
            if (flock.CurveFemale != null && flock.CurveFemale.CurveId == curveId)
            {
               flock.CurveFemale = null;
            }
         }
      }

      #endregion

      #region CRUD for FlockFiles

      /// <summary>
      /// Add files of new flock
      /// </summary>
      /// <param name="flockDefinition">Flock with valid ID</param>
      /// <param name="dbFlock">flock</param>
      private void AddFlockFile(FlockDefinition flockDefinition, DataModel.Flock dbFlock)
      {
         if (flockDefinition.FlockFileList.Count == 0)
         {
            return; // Hejno neobsahuje zadne soubory
         }

         foreach (var flockFile in flockDefinition.FlockFileList)
         {
            var file = Container.WeighingFiles
               .First(f =>
                  f.Name.Equals(flockFile.FileName) && f.Id == flockFile.Id);

            dbFlock.WeighingFiles.Add(file);
         }
      }

      /// <summary>
      /// Delete files of a specified flock
      /// </summary>
      /// <param name="flock">Flock</param>
      private void DeleteFlockFiles(ref DataModel.Flock flock)
      {
         if (flock == null)
         {
            return;
         }
         flock.WeighingFiles.Clear();
      }

      #endregion

      #region CRUD for Groups

      /// <summary>
      /// Add new group
      /// </summary>
      /// <param name="name">Group name</param>
      /// <param name="note">Group note</param>
      /// <param name="scaleConfigId">ScaleConfig Id this group belongs to</param>
      /// <returns>Id of new group</returns>
      private long AddGroup(string name, string note, long scaleConfigId)
      {
         // Jmeno musi byt zadane
         if (name == "")
         {
            throw new Exception("Group name cannot be empty");
         }

         if (!Container.ScaleConfigs.Any(sc => sc.ScaleConfigId == scaleConfigId))
         {
            throw new Exception("Unknown scale config ID");
         }
         var group = Container.Groups.Create();
         group.Name = name;
         group.Note = note;
         group.ScaleConfigId = scaleConfigId;
         Container.Groups.Add(group);
         return group.GroupId;
      }

      /// <summary>
      /// Delete all groups that belong to specified config
      /// </summary>
      /// <param name="scaleConfigId">Scale config ID</param>
      /// <returns>Number of rows affected</returns>
      private void DeleteGroups(long scaleConfigId)
      {
         var scaleConfig = Container.ScaleConfigs.FirstOrDefault(sc => sc.ScaleConfigId == scaleConfigId);
         if (scaleConfig == null)
         {
            return;
         }
         scaleConfig.Groups.Clear();
      }

      /// <summary>
      /// Add files to the group
      /// </summary>
      /// <param name="groupId">Group ID</param>
      /// <param name="fileIdList">List of File IDs</param>
      private void AddFilesToGroup(long groupId, List<long> fileIdList)
      {
         throw new Exception("TODO");
         //var group = Container.Groups.FirstOrDefault(g => g.GroupId == groupId);
         //if (group == null)
         //{
         //   return;
         //}

         //foreach (
         //   var file in
         //      fileIdList.Select(fileId => Container.Files.FirstOrDefault(f => f.FileId == fileId))
         //         .Where(file => file != null))
         //{
         //   @group.Files.Add(file);
         //}
      }

      /// <summary>
      /// Delete file that belong to specified group.
      /// If group contains only this file, delete whole group.
      /// </summary>
      /// <param name="file">file</param>
      private void DeleteFileFromGroup(WeighingFile file)
      {
         var group = file.Groups.FirstOrDefault(
            f => f.ScaleConfigId == file.ScaleConfigScaleConfigId && f.WeighingFiles.Contains(file));
         if (group != null)
         {
            if (group.WeighingFiles.Count == 1)
            {
               Container.Groups.Remove(group);
            }
            else
            {
               group.WeighingFiles.Remove(file);
            }
         }
      }

      #endregion

      #region CRUD for ManualResults

      /// <summary>
      /// Update existing manual result
      /// </summary>
      /// <param name="weighingId">weighing ID</param>
      /// <param name="startedDateTime"></param>
      private void UpdateManualResult(long weighingId, DateTime startedDateTime)
      {        
         var file = Container.WeighingFiles.OfType<ManualResult>().First(f => f.Id == weighingId);
         if (file == null)
         {
            throw new Exception("Weighing doesn't exist.");
         }
         file.OriginDateTime = startedDateTime;          
      }

      /// <summary>
      /// Add new row
      /// </summary>
      /// <param name="weighingId">Weighing id</param>
      /// <param name="startedDateTime"></param>
      /// <param name="flag"></param>
      /// <param name="count"></param>
      /// <param name="average"></param>
      /// <param name="sigma"></param>
      /// <param name="cv"></param>
      /// <param name="uniformity"></param>
      private void AddManualResult(long weighingId, DateTime startedDateTime, Flag flag, int count,
         double average, double sigma, double cv, int uniformity)
      {
         throw new Exception("TODO");
         //var weighing = Container.Weighings.Find(weighingId);
         //if (weighing == null)
         //{
         //   return;
         //}
         //var manualResult = Container.ManualResults.Create();
         //manualResult.StartedDateTime = startedDateTime;
         //manualResult.Flag = (byte) flag;
         //manualResult.Count = (short) count;
         //manualResult.Average = average;
         //manualResult.Sigma = sigma;
         //manualResult.Cv = cv;
         //manualResult.Uniformity = (byte) uniformity;
         //weighing.ManualResults.Add(manualResult);
      }

      private void AddManualResult(ref WeighingFile weighing, DateTime startedDateTime, Flag flag, int count,
         double average, double sigma, double cv, int uniformity)
      {
         throw new Exception("TODO");
         //if (weighing == null)
         //{
         //   return;
         //}
         //var manualResult = Container.ManualResults.Create();
         //manualResult.StartedDateTime = startedDateTime;
         //manualResult.Flag = (byte)flag;
         //manualResult.Count = (short)count;
         //manualResult.Average = average;
         //manualResult.Sigma = sigma;
         //manualResult.Cv = cv;
         //manualResult.Uniformity = (byte)uniformity;
         //weighing.ManualResults.Add(manualResult);
      }

      /// <summary>
      /// Delete results
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      private void DeleteManualResults(long weighingId)
      {
         throw new Exception("TODO");
         //var weighing = Container.WeighingFiles.OfType<ManualResult>().FirstOrDefault(f => f.Id == weighingId);
         //if (weighing != null)
         //{
         //   //weighing.ManualResults.Clear();
         //   DeleteStatistics(weighingId);
         //}
      }

      #endregion

      #region CRUD for Samples

      /// <summary>
      /// Save sample list of a specified weighing within an existing transaction
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <param name="sampleList">Sample list to save</param>
      private void SaveSampleListRaw(long weighingId, SampleList sampleList)
      {
         // Transakce uz musi byt zahajena a obslouzena volajici fci
         sampleList.First();
         while (sampleList.Read())
         {
            AddSample(weighingId, sampleList.Sample.Weight,
               (short) sampleList.Sample.Flag, sampleList.Sample.DateTime);
         }
      }

      private void ChangeUnitsInSamplesRaw(long scaleConfigId, long excludeWeighingId, Units oldUnits, Units newUnits)
      {
         // Nactu seznam vazeni se zadanym configem
         var weighingIdList = ReadWeighingIdList(scaleConfigId);

         // Smazu ze seznamu pozadovane vazeni
         weighingIdList.Remove(excludeWeighingId);

         // U vsech vazeni prepoctu jednotky
         foreach (var weighingId in weighingIdList)
         {
            // Nactu vzorky z DB
            var sampleList = LoadSamples(weighingId);

            // Prepoctu jednotky
            sampleList.Convert(oldUnits, newUnits);

            // Smazu vsechny vzorky
            DeleteSamples(weighingId);

            // Ulozim nove vzorky
            SaveSampleListRaw(weighingId, sampleList);
         }
      }

      /// <summary>
      /// Add new row
      /// </summary>
      /// <param name="weighingId">Weighing id</param>
      /// <param name="weight">Weight</param>
      /// <param name="flag">Flag</param>
      /// <param name="savedDateTime">Saved timestamp</param>
      private void AddSample(long weighingId, double weight, short flag, DateTime savedDateTime)
      {
         var weighing = Container.WeighingFiles.OfType<SampleResult>().FirstOrDefault(f => f.Id == weighingId);
            //may File instead of SampleResult
         if (weighing == null)
         {
            return;
         }
         var sample = Container.Samples.Create();
         sample.Weight = weight;
         sample.Flag = (byte) flag;
         sample.SavedDateTime = savedDateTime;
         weighing.Samples.Add(sample);
      }

      /// <summary>
      /// Delete samples
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      private void DeleteSamples(long weighingId)
      {
         var weighing = Container.WeighingFiles.OfType<SampleResult>().FirstOrDefault(f => f.Id == weighingId);
         if (weighing == null)
         {
            return;
         }
         weighing.Samples.Clear();
         Container.Samples.RemoveRange(Container.Samples.Where(w => w.SampleResult.Id == weighingId));
      }

      /// <summary>
      /// Delete statistics
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      private void DeleteStatistics(long weighingId)
      {
         var weighing = Container.WeighingFiles.OfType<ManualResult>().FirstOrDefault(f => f.Id == weighingId);
         if (weighing == null)
         {
            return;
         }
         weighing.Statistics.ForEach(s => s.Histograms.Clear());
         Container.Histograms.RemoveRange(Container.Histograms.Where(h => h.Statistic.ManualResult.Id == weighingId));
         weighing.Statistics.Clear();
         Container.Statistics.RemoveRange(Container.Statistics.Where(w => w.ManualResult.Id == weighingId));
      }

      #endregion

      #region CRUD for ScaleConfigs

      /// <summary>
      /// Save predefined scale config, including files and groups into the database within existing transaction
      /// </summary>
      /// <param name="scaleConfig">Scale config</param>
      /// <param name="name">Name of predefined config</param>
      /// <returns>Config ID</returns>
      private long SaveScaleConfigRaw<T, TU>(T scaleConfig, string name = null)
         where T : ScaleConfig
         where TU : DataModel.ScaleConfig
      {
         // Transakce uz musi byt zahajena a obslouzena volajici fci

         // Globalni nastaveni
         var scaleConfigId = AddScaleConfig<T, TU>(scaleConfig, name);

         // Seznam souboru vcetne nastaveni souboru
         var fileIdList = Add(scaleConfig.FileList, scaleConfigId);

         // Skupiny
         foreach (var group in scaleConfig.FileGroupList.List)
         {
            var groupId = AddGroup(group.Name, group.Note, scaleConfigId);

            // Vytvorim seznam ID souboru, ktere do skupiny patri
            var fileIdInGroupList = @group.FileIndexList.Select(fileIndex => fileIdList[fileIndex]).ToList();

            // Pridam do skupiny jednotlive soubory
            AddFilesToGroup(groupId, fileIdInGroupList);
         }

         return scaleConfigId;
      }

      /// <summary>
      /// Change units in ScaleConfig with specified Weighing ID
      /// </summary>
      /// <param name="weighingId">Weighing ID</param>
      /// <param name="scaleConfig">ScaleConfig with new units</param>
      /// <param name="oldUnits"></param>
      /// <param name="scaleConfigId"></param>
      private void ChangeUnitsInConfigRaw(long weighingId, ScaleConfig scaleConfig, out Units oldUnits,
         out long scaleConfigId)
      {
         // Podle zadaneho weighing ID najdu ID configu
         long fileId;
         if (!ReadWeighingIds(weighingId, out fileId, out scaleConfigId))
         {
            throw new Exception("Cannot find weighing");
         }

         // Nactu ulozene jednotky z DB a zjistim, zda je vubec treba zmena
         var oldScaleConfig = LoadScaleConfigPrivate(scaleConfigId);
         if (oldScaleConfig == null)
         {
            throw new Exception("Cannot find scale config");
         }
         oldUnits = oldScaleConfig.Units.Units;
         if (oldUnits == scaleConfig.Units.Units)
         {
            return; // Jednotky jsou stejne, neni treba zmena
         }

         // Zmenim jednotky v configu
         UpdateScaleConfig(scaleConfigId, scaleConfig);

         // Zmenim jednotky v configu u vsech souboru (i pokud nepouziva config pro kazdy soubor zvlast)
         ChangeUnits(scaleConfigId, scaleConfig, oldUnits);
      }

      /// <summary>
      /// Add predefined config on PC to the table
      /// </summary>
      /// <param name="config">Scale config</param>
      /// <param name="name">Name of predefined config on PC</param>
      /// <returns>Config Id</returns>
      private long AddScaleConfig<T, TU>(T config, string name)
         where T : ScaleConfig
         where TU : DataModel.ScaleConfig
      {
         var dbConfig = Container.Set<TU>().Create();
         dbConfig = (TU) config.ToDbScaleConfig(dbConfig);
         dbConfig.Name = name;
         Container.ScaleConfigs.Add(dbConfig);

         return dbConfig.ScaleConfigId;
      }

      /// <summary>
      /// Add config donwloaded from the scale
      /// </summary>
      /// <param name="config">Scale config</param>
      /// <returns>Config Id</returns>
      private long AddScaleConfig<T, TU>(T config)
         where T : ScaleConfig
         where TU : DataModel.ScaleConfig
      {
         return AddScaleConfig<T, TU>(config, null); // Nastaveni nactena z vahy musi mit jmeno NULL
      }

      /// <summary>
      /// Update config parameters
      /// </summary>
      /// <param name="scaleConfigId">Scale config ID</param>
      /// <param name="config">Scale config</param>
      private void UpdateScaleConfig(long scaleConfigId, ScaleConfig config)
      {
         var dbConfig =
            Container.ScaleConfigs.FirstOrDefault(c => c.ScaleConfigId == scaleConfigId);
         if (dbConfig == null)
         {
            return;
         }
         dbConfig = config.ToDbScaleConfig(dbConfig);
      }

      /// <summary>
      /// Delete config
      /// </summary>
      /// <param name="scaleConfigId">Config Id</param>
      /// <returns>Number of rows affected</returns>
      private void DeleteScaleConfig(long scaleConfigId)
      {
         var config = Container.ScaleConfigs.Find(scaleConfigId);
         if (config == null)
         {
            return;
         }
         DeleteInheritEntities(config.ScaleConfigId);
         Container.ScaleConfigs.Remove(config);
      }

      private void DeleteInheritEntities(long scaleConfigId)
      {
         foreach (var file in Container.WeighingFiles.Where(f => f.ScaleConfigScaleConfigId == scaleConfigId).ToList())
         {
            RemoveFileFromFlock(file);
            RemoveFileFromGroup(file);
            DeleteSamples(file.Id);
            DeleteStatistics(file.Id);
            Container.WeighingFiles.Remove(file);
         }
      }

      #endregion

      #region CRUD for Setup

      /// <summary>
      /// Delete selected flock from list of selected flocks
      /// </summary>
      /// <param name="flockId">Flock Id</param>
      /// <returns>Number of rows affected</returns>
      private void RemoveSelectedFlock(long flockId)
      {
         var setup = Container.Setups.FirstOrDefault();
         if (setup == null)
         {
            return;
         }
         var flock = Container.Flocks.Find(flockId);
         if (flock == null)
         {
            return;
         }
         setup.SelectedFlocks.Remove(flock);
      }

      /// <summary>
      /// Save version data to database
      /// </summary>
      private void SaveVersion()
      {
         var dbSetup = GetSetup();
         dbSetup.MajorVersion = DatabaseVersion.MAJOR;
         dbSetup.MinorVersion = DatabaseVersion.MINOR;
         dbSetup.DatabaseVersion = DatabaseVersion.DATABASE;
         dbSetup.BuildVersion = DatabaseVersion.BUILD;
      }

      /// <summary>
      /// Save setup to database including version info
      /// </summary>
      /// <param name="setup">Setup to save</param>
      protected void SaveSetup(Setup setup)
      {
         using (CreateContainer())
         {
            var dbSetup = GetSetup();
            setup.ToDbSetup(ref dbSetup);
            Container.SaveChanges();
         }
      }

      #endregion

      #region CRUD for Weighings

      /// <summary>
      /// Add new row
      /// </summary>
      /// <param name="fileId">File ID</param>
      /// <param name="resultType"></param>
      /// <param name="recordSource"></param>
      /// <param name="note"></param>
      /// <param name="minDateTime"></param>
      /// <param name="maxDateTime"></param>
      /// <returns>Id of new item</returns>
      private long AddWeighing(long fileId, ResultType resultType, RecordSource recordSource, string note,
         DateTime minDateTime, DateTime maxDateTime)
      {
         throw new Exception("TODO");
         //var weighing = Container.Weighings.Create();
         //weighing.FileId = fileId;
         //weighing.ResultType = (byte) resultType;
         //weighing.RecordSource = (byte) recordSource;
         //weighing.DownloadedDateTime = DateTime.Now;
         //weighing.Note = note;
         //weighing.SwMajorVersion = DatabaseVersion.MAJOR;
         //weighing.SwMinorVersion = DatabaseVersion.MINOR;
         //weighing.SwBuildVersion = DatabaseVersion.BUILD;
         //weighing.SamplesMinDateTime = minDateTime;
         //weighing.SamplesMaxDateTime = maxDateTime;
         //Container.Weighings.Add(weighing);
         //return weighing.WeighingId;
      }

      /// <summary>
      /// Update weighing data
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <param name="note">New note</param>
      /// <param name="samplesMinDateTime">DateTime of first sample</param>
      /// <param name="samplesMaxDateTime">DateTime of last sample</param>
      private void UpdateWeighing(long weighingId, string note, DateTime samplesMinDateTime, DateTime samplesMaxDateTime)
      {
         var weighing = Container.WeighingFiles.OfType<SampleResult>().FirstOrDefault(f => f.Id == weighingId);
         if (weighing == null)
         {
            return;
         }

         if (DateTime.Compare(weighing.SamplesMinDateTime.GetValueOrDefault(), samplesMinDateTime) != 0)
         {
            weighing.SamplesMinDateTime = samplesMinDateTime;
         }
         if (DateTime.Compare(weighing.SamplesMaxDateTime.GetValueOrDefault(), samplesMaxDateTime) != 0)
         {
            weighing.SamplesMaxDateTime = samplesMaxDateTime;
         }
         if (!weighing.Note.Equals(note))
         {
            weighing.Note = note;
         }
      }

      #endregion

      #endregion

      #region Queries

      #region Queries for File

      /// <summary>
      /// Load list of files that belong to specified scale config
      /// </summary>
      /// <param name="scaleConfigId">ScaleConfig ID</param>
      /// <param name="fileIdList">List of file IDs in the database</param>
      /// <returns>List of files</returns>
      private FileList LoadFileList(long scaleConfigId, out List<long> fileIdList)
      {
         var fileList = new FileList();
         fileIdList = new List<long>();
         //foreach (var file in Container.WeighingFiles.OfType<DataModel.File>().Where(f => f.ScaleConfigScaleConfigId == scaleConfigId))
         //{
         //   fileIdList.Add(file.Id);
         //   fileList.Add(File.FromDbFile(file));
         //}
         //foreach (var manualResult in Container.WeighingFiles.OfType<DataModel.ManualResult>().Where(f => f.ScaleConfigScaleConfigId == scaleConfigId))
         //{
         //   fileIdList.Add(manualResult.Id);
         //   fileList.Add(File.FromDbFile(manualResult));
         //}

         foreach (var file in Container.WeighingFiles.Where(f => f.ScaleConfigScaleConfigId == scaleConfigId))
         {
            fileIdList.Add(file.Id);
            fileList.Add(File.FromDbFile(file));
         }

         return fileList;
      }

      /// <summary>
      /// Load file from the database
      /// </summary>
      /// <param name="fileId">Item Id</param>
      /// <returns>File instance</returns>
      private File LoadFile(long fileId)
      {
         throw new Exception("TODO");
         //return File.FromDbFile(Container.Files.FirstOrDefault(f => f.FileId == fileId));
      }

      /// <summary>
      /// Load file with a specified name that belongs to a specified scale config
      /// </summary>
      /// <param name="scaleConfigId">Scale config ID that the file belongs to</param>
      /// <param name="name">File name</param>
      /// <returns>File ID</returns>
      private long LoadId(long scaleConfigId, string name)
      {
         throw new Exception("TODO");
         //var file = Container.Files.FirstOrDefault(f => f.ScaleConfigId == scaleConfigId && f.Name.Equals(name)) ??
         //           Container.Files.Local.FirstOrDefault(f => f.ScaleConfigId == scaleConfigId && f.Name.Equals(name));
         //if (file == null) return -1;
         //return file.FileId;
      }

      /// <summary>
      /// Load file name and note from the database
      /// </summary>
      /// <param name="fileId">Item Id</param>
      /// <returns><see cref="NameNote"/> instance</returns>
      private NameNote LoadNameNote(long fileId)
      {
         var file = Container.WeighingFiles.FirstOrDefault(f => f.Id == fileId);
         if (file == null)
         {
            throw new Exception(String.Format("File Id {0} doesn't exist", fileId));
         }
         return new NameNote(file.Name, file.Note);
      }

      /// <summary>
      /// Load file name from the database
      /// </summary>
      /// <param name="fileId">Item Id</param>
      /// <returns>File name</returns>
      private string LoadFileName(long fileId)
      {
         return LoadNameNote(fileId).Name;
      }

      /// <summary>
      /// Load sorted list of all file names from the database
      /// </summary>
      /// <returns>Sorted list of scale names</returns>
      private List<FlockFile> LoadNameList()
      {
         var list = new List<FlockFile>();
         foreach (var file in Container.WeighingFiles.OfType<SampleResult>())
         {
            list.Add(new FlockFile
            {
               Id = file.Id,
               FileName = file.Name,
               From = file.SamplesMinDateTime.GetValueOrDefault(),
               To = file.SamplesMaxDateTime.GetValueOrDefault()
            });
         }

         foreach (var file in Container.WeighingFiles.OfType<ManualResult>())
         {
            list.Add(new FlockFile
            {
               Id = file.Id,
               FileName = file.Name,
               From = file.OriginDateTime,
               To = file.OriginDateTime.AddHours(1)
            });
         }

         list.Sort((a, b) => String.Compare(a.FileName, b.FileName, StringComparison.Ordinal));
         return list;
      }

      /// <summary>
      /// Load sorted list of all file names and last used notes from the database
      /// </summary>
      /// <returns>List of file names and notes</returns>
      private List<NameNote> LoadNameNoteList()
      {
         var list = Container.WeighingFiles.ToList().Select(f => new NameNote(f.Name, f.Note)).ToList();
         list.Sort(NameNote.CompareByName);
         return list;
      }

      #endregion

      #region Queries for Curves

      private Curve LoadCurve(long curveId)
      {
         var curve = Container.Curves.FirstOrDefault(c => c.CurveId == curveId);
         if (curve == null)
         {
            throw new Exception(String.Format("Curve Id {0} don't exist", curveId));
         }
         return Curve.FromDbCurve(curve);
      }

      private void LoadCurve(ref Curve curve)
      {
         if (curve == null)
         {
            return; // Krivka neni v tomto hejnu definovana
         }

         // Definice krivky
         curve = LoadCurve(curve.Id);

         // Body krivky
         LoadCurvePoints(ref curve);
      }

      #endregion

      #region Queries for CurvePoints

      /// <summary>
      /// Load curve points from database
      /// </summary>
      /// <param name="curve">Curve to load the points to</param>
      private void LoadCurvePoints(ref Curve curve)
      {
         var id = curve.Id;
         var selCurve = Container.Curves.FirstOrDefault(c => c.CurveId == id);
         if (selCurve == null)
         {
            return;
         }
         foreach (var point in selCurve.CurvePoints)
         {
            curve.CurvePoints.Add(new CurvePoint(point.DayNumber, (float) point.Weight));
         }
      }

      /// <summary>
      /// Load curve list from database
      /// </summary>
      /// <param name="curveList">Curve list to load the points to</param>
      private void LoadCurvePointsToList(ref CurveList curveList)
      {
         foreach (var c in curveList.List)
         {
            var curve = c;
            LoadCurvePoints(ref curve);
         }
      }

      #endregion

      #region Queries for Flocks

      /// <summary>
      /// Load flock from database
      /// </summary>
      /// <param name="flockId">Flock Id</param>
      /// <returns>Flock instance</returns>
      private FlockDefinition LoadFLock(long flockId)
      {
         var flock = Container.Flocks.FirstOrDefault(f => f.FlockId == flockId);
         if (flock == null)
         {
            throw new Exception(String.Format("Flock Id {0} doesn't exist", flockId));
         }
         return FlockDefinition.FromDbFlock(flock);
      }

      /// <summary>
      /// Read flock definitions according to specified ID list
      /// </summary>
      /// <param name="idList">Flock ID list</param>
      /// <returns>FlockDefinitionList instance</returns>
      private FlockDefinitionList LoadFlockDefinitionList(List<long> idList)
      {
         var flockDefinitionList = new FlockDefinitionList();
         foreach (var id in idList)
         {
            var flockDefinition = LoadFLock(id); // Definice
            LoadFlockFiles(ref flockDefinition); // Prihraju jendotlivce soubory
            LoadCurve(ref flockDefinition.CurveDefault); // Teoreticka rustova krivka (pokud je definovana)
            LoadCurve(ref flockDefinition.CurveFemales);
            flockDefinitionList.Add(flockDefinition);
         }
         return flockDefinitionList;
      }

      #endregion

      #region Queries for FlockFiles

      /// <summary>
      /// Load flock files from database
      /// </summary>
      /// <param name="flockDefinition"><see cref="FlockDefinition"/> to load the files to</param>
      private void LoadFlockFiles(ref FlockDefinition flockDefinition)
      {
         flockDefinition.FlockFileList.Clear();
         var id = flockDefinition.Id;

         //load files
         foreach (
            var flockFile in
               Container.WeighingFiles.OfType<SampleResult>().Where(ff => ff.Flocks.Any(f => f.FlockId == id)))
         {
            flockDefinition.FlockFileList.Add(FlockFile.FromDbFlockFile(flockFile));
         }

         //load manual results
         foreach (
            var flockFile in
               Container.WeighingFiles.OfType<ManualResult>().Where(ff => ff.Flocks.Any(f => f.FlockId == id)))
         {
            flockDefinition.FlockFileList.Add(FlockFile.FromDbFlockFile(flockFile));
         }
      }

      /// <summary>
      /// Load flock list from database
      /// </summary>
      /// <param name="flockDefinitionList">Flock list to load the points to</param>
      private void LoadFlockFilesList(ref FlockDefinitionList flockDefinitionList)
      {
         foreach (var fd in flockDefinitionList.List)
         {
            var flockDefinition = fd;
            LoadFlockFiles(ref flockDefinition);
         }
      }

      /// <summary>
      /// Load ID list of flocks that are curently being weighed
      /// </summary>
      /// <returns>List of flock IDs</returns>
      private List<long> LoadCurrentFlockIds()
      {
         var now = DateTime.Now;
         var from = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59); // Jakykoliv cas tohoto dne vyhovuje
         return Container.Flocks.Where(f => DateTime.Compare(f.FromDateTime, from) <= 0 &&
                                            DateTime.Compare(f.ToDateTime, DATETIME_MAX.Date) >= 0)
            .Select(g => g.FlockId)
            .ToList();
      }

      #endregion

      #region Queries for Groups

      /// <summary>
      /// Load names and notes of groups that belong to specified config
      /// </summary>
      /// <param name="scaleConfigId">ScaleConfig Id the groups belong to</param>
      /// <param name="idList">List of group IDs corresponding to the returned group list</param>
      /// <returns>Group list</returns>
      private FileGroupList LoadGroups(long scaleConfigId, out List<long> idList)
      {
         var groupList = new FileGroupList();
         idList = new List<long>();
         var scaleConfig = Container.ScaleConfigs.FirstOrDefault(sc => sc.ScaleConfigId == scaleConfigId);
         if (scaleConfig == null)
         {
            return groupList;
         }

         foreach (var @group in scaleConfig.Groups)
         {
            groupList.Add(@group.Name, @group.Note);
            idList.Add(@group.GroupId);
         }
         return groupList;
      }

      /// <summary>
      /// Load list of group IDs that belong to specified config
      /// </summary>
      /// <param name="scaleConfigId">Scale config ID</param>
      /// <returns>List of group IDs</returns>
      private List<long> LoadGroupIds(long scaleConfigId)
      {
         var idList = new List<long>();
         var scaleConfig = Container.ScaleConfigs.FirstOrDefault(sc => sc.ScaleConfigId == scaleConfigId);
         if (scaleConfig == null)
         {
            return idList;
         }
         idList.AddRange(scaleConfig.Groups.Select(@group => @group.GroupId));
         return idList;
      }

      /// <summary>
      /// Load file indexes
      /// </summary>
      /// <param name="groupId">Group ID</param>
      /// <returns>List of file indexes</returns>
      private List<int> LoadFileIndexes(long groupId)
      {
         var list = new List<int>();
         var group = Container.Groups.FirstOrDefault(g => g.GroupId == groupId);
         return group == null ? list : group.WeighingFiles.Select(f => f.Id).ToList();
      }

      #endregion

      #region Queries for ManualResults    

      public StatisticResult LoadScaleManualResults(long weighingId)
      {
         using (CreateContainer())
         {
            var weighing = Container.WeighingFiles.OfType<ManualResult>().FirstOrDefault(f => f.Id == weighingId);
            if (weighing == null)
            {
               throw new Exception(String.Format("Manual results with weighing Id {0} don't exist", weighingId));
            }
                  
            return new StatisticResult((Flag)weighing.Statistics.First().Flag, weighing.Statistics.First().Count, (float)weighing.Statistics.First().Average,
               (float)weighing.Statistics.First().Sigma, (float)weighing.Statistics.First().Cv, weighing.Statistics.First().Uniformity, weighing.OriginDateTime);
         }
      }

      /// <summary>
      /// Load results form database
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <returns>List of StatisticResult</returns>
      private List<StatisticResult> LoadManualResults(long weighingId)
      {
         var weighing = Container.WeighingFiles.OfType<ManualResult>().FirstOrDefault(f => f.Id == weighingId);
         if (weighing == null)
         {
            throw new Exception(String.Format("Manual results with weighing Id {0} don't exist", weighingId));
         }         

         var statisticResults = new List<StatisticResult>();
         foreach (var stat in weighing.Statistics)
         {         
            var result = new StatisticResult((Flag)stat.Flag, stat.Count, (float)stat.Average, (float)stat.Sigma,
                        (float)stat.Cv, stat.Uniformity, weighing.OriginDateTime, (float?)stat.Gain);

            if (stat.Histograms != null && stat.Histograms.Count > 0)
            {
               result.Histogram = ConvertCollectionCountsToHistogram(stat.Histograms, stat.Average,
                                                                  weighing.ScaleConfig.HistogramStep);
            }
            statisticResults.Add(result);
         }        
         return statisticResults;
      }

      #endregion

      #region Queries for Samples

      /// <summary>
      /// Load samples form database
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <returns>SampleList instance</returns>
      private SampleList LoadSamples(long weighingId)
      {
         var weighing = Container.WeighingFiles.OfType<SampleResult>().FirstOrDefault(f => f.Id == weighingId);
         if (weighing == null)
         {
            throw new Exception(String.Format("Samples with weighing Id {0} don't exist", weighingId));
         }
         var list = new SampleList();
         foreach (var s in weighing.Samples)
         {
            list.Add(new Sample
            {
               Weight = (float) s.Weight,
               Flag = (Flag) s.Flag,
               DateTime = s.SavedDateTime
            });
         }
         return list;
      }

      #endregion

      #region Queries for ScaleConfigs

      /// <summary>
      /// Get Id of the specified predefined config. If the name doesn't exist, an exception is thrown.
      /// </summary>
      /// <param name="name">Predefined config name</param>
      /// <returns>Id</returns>
      private long GetScaleConfigId(string name)
      {
         var config = Container.ScaleConfigs.FirstOrDefault(sc => sc.Name.Equals(name));
         if (config == null)
         {
            return -1;
         }
         return config.ScaleConfigId;
      }

      //private ScaleType GetScaleType(long weighingId) 
      //{
      //   long fileId, scaleConfigId;
      //   string note;
      //  //  Najdu pozadovane vazeni v databazi
      //   if (!ReadWeighingInfo(weighingId, out fileId, out scaleConfigId, out note))
      //   {
      //      return ScaleType.SCALE_BAT1;
      //   }
      //   var scaleConfig = Container.ScaleConfigs.FirstOrDefault(c => c.ScaleConfigId == scaleConfigId);

      //   if (scaleConfig == null)
      //   {
      //      return ScaleType.SCALE_BAT1;
      //   }

      //   var type = System.Data.Entity.Core.Objects.ObjectContext.GetObjectType(scaleConfig.GetType());

      //   if (type == typeof(DataModel.Bat1Scale))
      //   {  
      //      return ScaleType.SCALE_BAT1;
      //   }

      //   if (type == typeof(DataModel.Bat2Scale))
      //   {
      //      return ScaleType.SCALE_BAT2;
      //   }

      //   if (type == typeof(DataModel.Bat2OldScale))
      //   {
      //      return ScaleType.SCALE_BAT2OLD;
      //   }

      //   return ScaleType.SCALE_BAT1;
      //}


      /// <summary>
      /// Load scale config from database
      /// </summary>
      /// <param name="scaleConfigId">Scale config Id</param>
      /// <returns>ScaleConfig instance</returns>
      private ScaleConfig LoadScaleConfigPrivate(long scaleConfigId)
      {
         DataModel.ScaleConfig dbConfig = Container.ScaleConfigs.FirstOrDefault(c => c.ScaleConfigId == scaleConfigId);

         if (dbConfig == null)
         {
            return null;
         }

         var scale = ScaleConfig.FromDbScaleConfig(dbConfig);   

         if (dbConfig is DataModel.Bat1Scale)
         {
            return new Bat1Scale(scale);
         }

         if (dbConfig is DataModel.Bat2Scale)
         {
            return new Bat2Scale(scale) { SerialNumber = (dbConfig as DataModel.Bat2Scale).SerialNumber };
         }

         if (dbConfig is DataModel.Bat2OldScale)
         {
            return new Bat2OldScale(scale);
         }

         return ScaleConfig.FromDbScaleConfig(dbConfig);
      }

      /// <summary>
      /// Load complete scale config within an existing connection
      /// </summary>
      /// <param name="scaleConfigId">Scale config ID</param>
      private ScaleConfig LoadScaleConfigRaw(long scaleConfigId)
      {
         // Globalni nastaveni
         var scaleConfig = LoadScaleConfigPrivate(scaleConfigId);

         // Soubory prihraju do configu vahy
         List<long> fileIdList;
         scaleConfig.FileList = LoadFileList(scaleConfigId, out fileIdList);

         // Seznam skupin
         List<long> groupIdList;
         scaleConfig.FileGroupList = LoadGroups(scaleConfigId, out groupIdList);

         // Soubory v jednotlivych skupinach
         var index = 0;
         foreach (var groupId in groupIdList)
         {
            scaleConfig.FileGroupList.List[index].FileIndexList.AddRange(LoadFileIndexes(groupId));
            index++;
         }

         return scaleConfig;
      }

      /// <summary>
      /// Load predefined config names
      /// </summary>
      /// <returns>List of config names</returns>
      private List<string> LoadScaleConfigNameList()
      {
         return Container.ScaleConfigs.Select(sc => sc.Name).Where(n => n != null).ToList();
      }

      #endregion

      #region Queries for Setup

      /// <summary>
      /// Load version of SW that used the database last time
      /// </summary>
      /// <returns>SW version</returns>
      private DatabaseVersion LoadVersion()
      {
         if (!Container.Setups.Any())
         {
            throw new Exception("Setup doesn't exist");
         }
         var setup = GetSetup();
         var version = new DatabaseVersion
         {
            Major = setup.MajorVersion,
            Minor = setup.MinorVersion,
            Database = setup.DatabaseVersion,
            Build = setup.BuildVersion
         };
         return version;
      }

      #endregion

      #region Queries for Weighing

      /// <summary>
      /// Load weighing data from database
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <returns>WeighingData instance</returns>
      private WeighingData LoadWeighingData(long weighingId)
      {
         long fileId, scaleConfigId;
         string note;
         // Najdu pozadovane vazeni v databazi
         if (!ReadWeighingInfo(weighingId, out fileId, out scaleConfigId, out note))
         {
            return null;
         }
         // Nactu vsechny potrebne udaje z databaze

         // Config vahy
         var scaleConfig = LoadScaleConfigRaw(scaleConfigId); // Komplet config vcetne souboru a skupin        

         // Soubor, ktery se vazil nastavim jako ukazatel do configu vahy (tam uz mam vsechny soubory nactene)
         var file = scaleConfig.GetFile(LoadFileName(fileId));

         // Vzorky jen u vazeni stahnuteho z vahy (u rucne zadanych vysledku vzorky nejsou)
         SampleList sampleList = null;
         var sampleResult = Container.WeighingFiles.OfType<SampleResult>().FirstOrDefault(f => f.Id == weighingId);
         if (sampleResult != null)
         {
            sampleList = LoadSamples(weighingId);
         }

         // Vytvorim a vratim vazeni
         return new WeighingData(weighingId, file, sampleList, scaleConfig, note);
      }

      /// <summary>
      /// Read Ids of file and scale config, result type and note
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <param name="fileId"></param>
      /// <param name="scaleConfigId"></param>      
      /// <param name="note"></param>
      /// <returns>True when successful</returns>
      private bool ReadWeighingInfo(long weighingId, out long fileId, out long scaleConfigId,
         out string note)
      {
         var weighing = Container.WeighingFiles.Find(weighingId);
         if (weighing == null)
         {
            // Vazeni neexistuje
            fileId = scaleConfigId = 0;
            note = "";
            return false;
         }
         fileId = weighing.Id;
         scaleConfigId = weighing.ScaleConfigScaleConfigId;
         note = weighing.Note;
         return true;
      }

      /// <summary>
      /// Read Ids of file and scale config
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <param name="fileId"></param>
      /// <param name="scaleConfigId"></param>
      /// <returns>True when successful</returns>
      private bool ReadWeighingIds(long weighingId, out long fileId, out long scaleConfigId)
      {
         string note;
         return ReadWeighingInfo(weighingId, out fileId, out scaleConfigId, out note);
      }

      /// <summary>
      /// Read File ID
      /// </summary>
      /// <param name="weighingId">Weighing Id</param>
      /// <returns>File Id or -1</returns>
      private long ReadFileId(long weighingId)
      {
         long fileId;
         long scaleConfigId;

         if (!ReadWeighingIds(weighingId, out fileId, out scaleConfigId))
         {
            return -1;
         }

         return fileId;
      }

      /// <summary>
      /// Read list of all weighing IDs sorted by date/time of weighing start
      /// </summary>
      /// <returns>List of IDs</returns>
      private List<long> ReadWeighingIdList()
      {
         throw new Exception("TODO");
         //return Container.Weighings.OrderBy(w => w.SamplesMinDateTime).Select(w => w.WeighingId).ToList();
      }

      /// <summary>
      /// Read list of all weighing IDs that share the specified config
      /// </summary>
      /// <param name="scaleConfigId"></param>
      /// <returns>List of IDs</returns>
      private List<long> ReadWeighingIdList(long scaleConfigId)
      {
         return
            Container.WeighingFiles.OfType<SampleResult>().Where(w => w.ScaleConfigScaleConfigId == scaleConfigId)
               .OrderBy(w => w.SamplesMinDateTime)
               .Select(w => (long) w.Id)
               .ToList();
      }

      /// <summary>
      /// Check if specified file exists in any weighing
      /// </summary>
      /// <param name="fileId">File Id</param>
      /// <returns>True if exists</returns>
      public bool FileExists(long fileId)
      {
         throw new Exception("TODO");
         //return Container.Weighings.Any(w => w.FileId == fileId);
      }

      /// <summary>
      /// Check if specified scale config exists in any weighing
      /// </summary>
      /// <param name="scaleConfigId">Scale config Id</param>
      /// <returns>True if exists</returns>
      public bool ScaleConfigExists(long scaleConfigId)
      {
         throw new Exception("TODO");
         //return Container.Weighings.Any(w => w.File.ScaleConfigId == scaleConfigId);
      }


      /// <summary>
      /// Load scale config for Bat2 device from general config.
      /// </summary>
      /// <param name="config">general config</param>
      /// <returns>Bat2 scale config</returns>
      public Bat2Scale LoadBat2Config(Bat2Scale config)
      {
         using (CreateContainer())
         {
            var dbConfig =
               Container.ScaleConfigs.OfType<DataModel.Bat2Scale>()
                  .FirstOrDefault(c => c.ScaleName.Equals(config.ScaleName));
            return dbConfig == null ? new Bat2Scale(config) : Bat2Scale.FromDbScaleConfig(dbConfig);
         }
      }

      //public T LoadBatConfig<T>(ScaleConfig config) where T : ScaleConfig, new()
      //{
      //   using (CreateContainer())
      //   {
      //      if (typeof(T) == typeof(Bat2Scale))
      //      {

      //         var dbConfig = container.ScaleConfigs.OfType<DataModel.Bat2Scale>().FirstOrDefault(c => c.ScaleName.Equals(config.ScaleName));
      //         if (dbConfig == null)
      //         {
      //            return (T)Activator.CreateInstance(typeof(T), config);
      //         }

      //         return T.FromDbScaleConfig(dbConfig);
      //      }
      //      else
      //      {  //Bat1Scale

      //      }
      //   }
      //}

      /// <summary>
      /// Load scale config for Bat1 device from general config.
      /// </summary>
      /// <param name="config">general config</param>
      /// <returns>Bat1 scale config</returns>
      public Bat1Scale LoadBat1Config(ScaleConfig config)
      {
         using (CreateContainer())
         {
            var dbConfig =
               Container.ScaleConfigs.OfType<DataModel.Bat1Scale>()
                  .FirstOrDefault(c => c.ScaleName.Equals(config.ScaleName));
            return dbConfig == null ? new Bat1Scale(config) : Bat1Scale.FromDbScaleConfig(dbConfig);
         }
      }

      public void SaveBat1Config(Bat1Scale config)
      {
         using (CreateContainer())
         {
            var dbConfig =
               Container.ScaleConfigs.OfType<DataModel.Bat1Scale>()
                  .FirstOrDefault(c => c.ScaleName.Equals(config.ScaleName));
            if (dbConfig == null)
            {
               return;
            }

            dbConfig = config.ToDbScaleConfig(dbConfig);
            Container.SaveChanges();
         }
      }

      #endregion

      #endregion

      #region Private helpers

      /// <summary>
      /// Convert Histogram structure to simple collection
      /// </summary>
      /// <param name="histohram">Bat1Library.Statistics.Histogram</param>
      /// <returns>Collection of histograms values</returns>
      public ICollection<Histogram> ConvertHistogramToCollectionCounts(Bat1Library.Statistics.Histogram histohram)
      {
         return histohram.Counts.Select(item => new Histogram {Value = (byte) item}).ToList();
      }

      public Bat1Library.Statistics.Histogram ConvertCollectionCountsToHistogram(ICollection<Histogram> collection, double average, double step)
      {
         var stat = new Statistics();
         return stat.Histogram(collection.Select(item => item.Value).Select(dummy => (float)dummy).ToList(), (float)average, step);
      }

      /// <summary>
      /// Add new file and optionaly save
      /// </summary>
      /// <param name="file">File to add</param>
      /// <param name="scaleConfigId">ScaleConfig Id this file belongs to</param>
      /// <returns>Id of new file</returns>
      private long AddFile(File file, long scaleConfigId)
      {
         throw new Exception("TODO AddFile");
         //// Jmeno musi byt zadane
         //if (file.Name == "")
         //{
         //   throw new Exception("File name cannot be empty");
         //}

         //var newFile = Container.Files.Create();
         //file.ToDbFile(ref newFile);
         //newFile.ScaleConfigId = scaleConfigId;

         //var scaleConfig = Container.ScaleConfigs.Local.FirstOrDefault(c => c.ScaleConfigId == scaleConfigId);
         //newFile.ScaleConfig = scaleConfig;

         //newFile = Container.Files.Add(newFile);
         //return newFile.FileId;
      }

      private void SetFlock(ref DataModel.Flock flock, FlockDefinition flockDefinition)
      {
         flock.Name = flockDefinition.Name;
         flock.Note = flockDefinition.Note;
         flock.StartedDay = (short?) flockDefinition.StartedDay;

         flock.CurveDefault = null;
         if (flockDefinition.CurveDefault != null)
         {
            var curveDefault = Container.Curves.FirstOrDefault(c => c.CurveId == flockDefinition.CurveDefault.Id);
            flock.CurveDefault = curveDefault;
         }

         flock.CurveFemale = null;
         if (flockDefinition.CurveFemales != null)
         {
            var curveFemale = Container.Curves.FirstOrDefault(c => c.CurveId == flockDefinition.CurveFemales.Id);
            flock.CurveFemale = curveFemale;
         }
      }

      private DataModel.Setup GetSetup()
      {
         var setup = Container.Setups.FirstOrDefault();
         if (setup != null) return setup;

         setup = Container.Setups.Create();
         Container.Setups.Add(setup);
         Container.SaveChanges();
         return setup;
      }

      private void UpdateDb()
      {
         // TODO do update of previous database
      }

      #endregion
   }
}