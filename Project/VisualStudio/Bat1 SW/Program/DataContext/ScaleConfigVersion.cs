namespace DataContext
{
   public struct ScaleConfigVersion {
      /// <summary>
      /// Major version number (1.xx.xx)
      /// </summary>
      public int Major;
        
      /// <summary>
      /// Minor version number (x.01.xx)
      /// </summary>
      public int Minor;

      /// <summary>
      /// Build number (x.xx.01)
      /// </summary>
      public int Build;

      //public ScaleConfigVersion(int minorVersion) {
      //   // Default vyplnim verzi vahy, kterou podporuje tato verze SW
      //   Major = ScaleVersion.MAJOR;
      //   Minor = minorVersion;
      //   Build = 0;       
      //}
   }
}