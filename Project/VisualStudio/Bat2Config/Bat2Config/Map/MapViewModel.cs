﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Bat2Config.ViewModel.Configuration;
using Bat2Library.Connection.Interface.Domain;

namespace Bat2Config.Map
{
   public static class MapViewModel
   {
      public static CellularData Map(this CellularDataViewModel viewModel)
      {
         return new CellularData()
         {
            Username = viewModel.Username,
            Apn = viewModel.Apn,
            Password = viewModel.Password
         };
      }

      public static VersionInfo Map(this Bat2IdentificationViewModel viewModel)
      {
         return new VersionInfo()
         {
            Class = (short)viewModel.Class,
            HardwareBuild = (byte)viewModel.HardwareBuild,
            HardwareMajor = (byte)viewModel.HardwareMajor,
            HardwareMinor = (byte)viewModel.HardwareMinor,
            Modification = (short)viewModel.Modification,
            SerialNumber = (uint)viewModel.SerialNumber,
            SoftwareBuild = (byte)viewModel.SoftwareBuild,
            SoftwareMajor = (byte)viewModel.SoftwareMajor,
            SoftwareMinor = (byte)viewModel.SoftwareMinor
         };
      }

      public static Country Map(this CountryViewModel viewModel)
      {
         return new Country()
         {
            CountryCode = viewModel.CountryCode,
            Language = viewModel.Language,
            CodePage = viewModel.CodePage,
            DateFormat = viewModel.DateFormat,
            DateSeparator1 = viewModel.DateSeparator1,
            DateSeparator2 = viewModel.DateSeparator2,
            TimeFormat = viewModel.TimeFormat,
            TimeSeparator = viewModel.TimeSeparator,
            DaylightSavingType = viewModel.DaylightSaving
         };
      }

      public static DisplayConfiguration Map(this DisplayConfigurationViewModel viewModel)
      {
         return new DisplayConfiguration()
         {
            Contrast = viewModel.Contrast,
            Mode = viewModel.Mode,
            BacklightDuration = viewModel.BacklightDuration,
            BacklightIntensity = viewModel.BacklightIntensity,
            BacklightMode = viewModel.BacklightMode,
            SavePower = viewModel.SavePower
         };
      }


      public static Ethernet Map(this EthernetViewModel viewModel)
      {
         return new Ethernet()
         {
            Dhcp = viewModel.Dhcp,
            Gateway = IPAddress.Parse(viewModel.Gateway),
            Ip = IPAddress.Parse(viewModel.Ip),
            PrimaryDns = IPAddress.Parse(viewModel.PrimaryDns),
            SubnetMask = IPAddress.Parse(viewModel.SubnetMask)
         };
      }

      public static DacsOptions Map(this DacsOptionsViewModel viewModel)
      {
         return new DacsOptions()
         {
            Address = viewModel.Address,
            DeviceAddress = viewModel.DeviceAddress,
            Version = viewModel.Version
         };
      }

      public static MegaviOptions Map(this MegaviOptionsViewModel viewModel)
      {
         return new MegaviOptions()
         {
            Address = viewModel.Address,
            DeviceAddress = viewModel.DeviceAddress,
            Code = viewModel.Code
         };
      }

      public static ModbusOptions Map(this ModbusOptionsViewModel viewModel)
      {
         return new ModbusOptions()
         {
            Address = viewModel.Address,
            BaudRate = viewModel.BaudRate,
            Mode = viewModel.Mode,
            Parity = viewModel.Parity
         };
      }

      public static WeightUnits Map(this WeightUnitViewModel viewModel)
      {
         return new WeightUnits()
         {
            Decimals = viewModel.Decimals,
            Division = viewModel.Division,
            DivisionMax = viewModel.DivisionMax,
            Range = viewModel.Range,
            Units = viewModel.Units
         };
      }

      public static GsmMessage Map(this GsmMessageViewModel viewModel)
      {
         return new GsmMessage()
         {
            CommandsEnabled = viewModel.CommandsEnabled,
            CommandsCheckPhoneNumber = viewModel.CommandsCheckPhoneNumber,
            CommandsExpiration = viewModel.CommandsExpiration,
            EventMask = viewModel.EventMask,
            Mode = viewModel.Mode,
            SwitchOnPeriod = viewModel.SwitchOnPeriod,
            SwitchOnDuration = viewModel.SwitchOnDuration,
            SwitchOnTimes = viewModel.SwitchOnTimes.ToList()
         };
      }

      public static DataPublication Map(this DataPublicationViewModel viewModel)
      {
         return new DataPublication()
         {
            AccelerateFromDay = viewModel.AccelerateFromDay,
            AcceleratedPeriod = viewModel.AcceleratedPeriod,
            Interface = viewModel.Interface,
            Password = viewModel.Password,
            Period = viewModel.Period,
            SendAt = viewModel.SendAt,
            StartFromDay = viewModel.StartFromDay,
            Url = viewModel.Url,
            Username = viewModel.Username,
         };
      }

      public static IEnumerable<Rs485Options> Map(this Rs485OptionsListViewModel viewModel)
      {
         return
            viewModel.Models.Select(
               x =>
                  new Rs485Options()
                  {
                     Enabled = x.Enabled,
                     Mode = x.Mode,
                  });

      }

      public static WeighingConfiguration Map(this WeighingConfigurationViewModel viewModel)
      {
         return new WeighingConfiguration()
         {
            Name = viewModel.Name,
            Flock = viewModel.Flock,
            MenuMask = viewModel.MenuMask,
            InitialDay = viewModel.InitialDay,
            CorrectionCurveIndex = viewModel.CorrectionCurveIndex,
            PredefinedIndex = viewModel.PredefinedIndex,
            DayStart = viewModel.DayStart,
            MaleInitialWeight = viewModel.MaleInitialWeight,
            MaleGrowthCurveIndex = viewModel.MaleGrowthCurveIndex,
            FemaleInitialWeight = viewModel.FemaleInitialWeight,
            FemaleGrowthCurveIndex = viewModel.FemaleGrowthCurveIndex,
            AdjustTargetWeights = viewModel.AdjustTargetWeights,
            Sex = viewModel.Sex,
            SexDifferentiation = viewModel.SexDifferentiation,
            Growth = viewModel.Growth,
            Mode = (byte)viewModel.Mode,
            Filter = viewModel.Filter,
            StabilizationTime = viewModel.StabilizationTime,
            StabilizationRange = viewModel.StabilizationRange,
            Step = viewModel.Step,
            MaleMarginAbove = viewModel.MaleMarginAbove,
            MaleMarginBelow = viewModel.MaleMarginBelow,
            FemaleMarginAbove = viewModel.FemaleMarginAbove,
            FemaleMarginBelow = viewModel.FemaleMarginBelow,
            ShortPeriod = viewModel.StatisticsVm.ShortPeriod,
            ShortType = viewModel.StatisticsVm.ShortType,
            UniformityRange = viewModel.StatisticsVm.UniformityRange,
            HistogramStep = (short)viewModel.StatisticsVm.HistogramStep,
            HistogramRange = viewModel.StatisticsVm.HistogramRange,
            HistogramMode = viewModel.StatisticsVm.HistogramMode,
            Planning = viewModel.Planning,
            WeighingPlanIndex = viewModel.WeighingPlanIndex
         };
      }
   }
}
