﻿using System;
using System.Net;
using System.Windows.Data;

namespace Bat2Config.Convertors
{
   public class IpAddressToStringConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (value is IPAddress)
            return value.ToString();

         return new IPAddress(0).ToString();
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         IPAddress addr;
         return IPAddress.TryParse(value.ToString(), out addr) ? addr : new IPAddress(0);
      }
   }
}
