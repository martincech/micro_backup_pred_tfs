﻿using System.Windows;
using System.Windows.Controls;
using Bat1.Forms.Bat2.ModelViews.Applications;
using DataContext;
using Desktop.Wpf.Presentation;

namespace Bat1.Forms.Bat2.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for DataPublicationView.xaml
   /// </summary>
   public partial class DataPublicationView : IDataPublicationView
   {
      private DataPublicationViewModel vm;

      public DataPublicationView()
      {
         InitializeComponent();
      }

      private void SetPasswordToView()
      {
         DataPublicationPasswordBox.Password = vm.Password;
      }   

      private void DataPublicationPasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
      {         
         vm.Password = DataPublicationPasswordBox.Password;
      }

      private void DataPublicationView_OnLoaded(object sender, RoutedEventArgs e)
      {
         vm = DataContext as DataPublicationViewModel;
         SetPasswordToView();
      }

      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface IDataPublicationView : IView
   {
   }
}
