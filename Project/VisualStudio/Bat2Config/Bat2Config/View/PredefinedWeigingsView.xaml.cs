﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View
{
   /// <summary>
   /// Interaction logic for PredefinedWeigingsView.xaml
   /// </summary>
   public partial class PredefinedWeigingsView : IView
   {
      public PredefinedWeigingsView()
      {
         InitializeComponent();
      }

      #region Implementation of IView

      /// <summary>
      /// Show this view
      /// </summary>
      public void Show()
      {
         throw new NotImplementedException();
      }

      /// <summary>
      /// Hide this view
      /// </summary>
      public void Hide()
      {
         throw new NotImplementedException();
      }

      #endregion
   }
}
