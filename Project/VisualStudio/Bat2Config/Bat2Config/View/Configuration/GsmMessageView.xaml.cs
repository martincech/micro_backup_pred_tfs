﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for GsmMessageView.xaml
   /// </summary>
   public partial class GsmMessageView : IGsmMessageView
   {
      //private GsmMessageViewModel vm;

      public GsmMessageView()
      {
         InitializeComponent();          
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }

      private void GsmMessageView_OnLoaded(object sender, RoutedEventArgs e)
      {
         //vm = DataContext as GsmMessageViewModel;
         //if (vm == null) return;

         //var time = new TimeView { DataContext = vm.SwitchOnTimes };
         //TimeStackPanel.Children.Add(time);
      }
   }

   public interface IGsmMessageView : IView
   {
   }
}
