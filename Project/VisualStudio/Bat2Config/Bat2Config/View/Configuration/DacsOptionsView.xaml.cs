﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for DacsConfigurationView.xaml
   /// </summary>
   public partial class DacsOptionsView : IView
   {   
      public DacsOptionsView()
      {
         InitializeComponent();
      }


      public void Show()
      {
         dacsControl.Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         dacsControl.Visibility = Visibility.Collapsed;
      }
   }
}
