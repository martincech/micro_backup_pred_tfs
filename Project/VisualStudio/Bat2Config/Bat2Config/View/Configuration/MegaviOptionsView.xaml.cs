﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for MegaviOptionsView.xaml
   /// </summary>
   public partial class MegaviOptionsView : IView
   {   
      public MegaviOptionsView()
      {
         InitializeComponent();       
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }
}
