﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for CountryView.xaml
   /// </summary>
   public partial class CountryView : ICountryView
   {
      public CountryView()
      {  //Bat2
         InitializeComponent();       
         CodePageStackPanel.Visibility = Visibility.Visible;        
      }

      public CountryView(bool readOnly)
      {  //Bat1
         InitializeComponent();      
         CodePageStackPanel.Visibility = Visibility.Collapsed;
         Layout.IsEnabled = !readOnly;
      }


      public void Show()
      {
         CountryControl.Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         CountryControl.Visibility = Visibility.Collapsed;
      }
   }

   public interface ICountryView : IView
   {
   }
}
