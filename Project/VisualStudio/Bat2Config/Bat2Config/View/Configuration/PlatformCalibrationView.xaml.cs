﻿using System.Windows;
using System.Windows.Controls;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for PlatformCalibrationView.xaml
   /// </summary>
   public partial class PlatformCalibrationView : IPlatformCalibrationView
   {
      public PlatformCalibrationView()
      {
         InitializeComponent();
      }

      private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         dataGrid.UnselectAllCells();
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface IPlatformCalibrationView : IView
   {
   }
}
