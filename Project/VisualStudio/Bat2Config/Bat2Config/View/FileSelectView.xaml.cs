﻿using System.Windows;
using Desktop.Wpf.Presentation;
using Microsoft.Win32;

namespace Bat2Config.View
{
   /// <summary>
   /// Interaction logic for FileSelectView.xaml
   /// </summary>
   public partial class FileSelectView : IView
   {
      public FileSelectView()
      {
         InitializeComponent();
      }

      public void Show()
      {
        
      }

      public void Hide()
      {
      
      }
   }
}
