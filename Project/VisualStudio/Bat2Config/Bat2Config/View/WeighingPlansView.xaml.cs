﻿using System;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View
{
   /// <summary>
   /// Interaction logic for WeighingPlansView.xaml
   /// </summary>
   public partial class WeighingPlansView : IView
   {
      public WeighingPlansView()
      {
         InitializeComponent();
      }

      #region Implementation of IView

      /// <summary>
      /// Show this view
      /// </summary>
      public void Show()
      {
         throw new NotImplementedException();
      }

      /// <summary>
      /// Hide this view
      /// </summary>
      public void Hide()
      {
         throw new NotImplementedException();
      }

      #endregion
   }
}
