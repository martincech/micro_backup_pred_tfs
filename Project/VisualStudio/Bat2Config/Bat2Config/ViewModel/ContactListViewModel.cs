﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel
{
   public class ContactListViewModel : ObservableObject
   {
      private ObservableCollection<Contact> contatList;
      private Contact activeItem;


      private ICommand addCommand;
      private ICommand deleteCommand;

      public ContactListViewModel(IView view)
      {
         contatList = new ObservableCollection<Contact>();
         view.DataContext = this;
      }


      public ObservableCollection<Contact> ContatList
      {
         get { return contatList; }
         set { SetProperty(ref contatList, value); }
      }

      public Contact ActiveItem
      {
         get { return activeItem; }
         set
         {
            SetProperty(ref activeItem, value);
            UpdateCommands();
         }
      }

      public ICommand AddCommand
      {
         get
         {
            return addCommand ?? (addCommand = new RelayCommand(() =>
               {
                  ContatList.Add(new Contact() { Name = "New contact", PhoneNumber = "0"});
                  ActiveItem = ContatList.Last();
               }));
         }
      }

      public ICommand DeleteCommand
      {
         get
         {
            return deleteCommand ?? (deleteCommand = new RelayCommand(() =>
            {
               ContatList.Remove(ActiveItem);
            }, () => ActiveItem != null));
         }
      }

      private void UpdateCommands()
      {
        ((RelayCommand)DeleteCommand).RaiseCanExecuteChanged();
      }
   }
}
