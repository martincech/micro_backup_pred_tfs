﻿using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Configuration
{
   public class WeightUnitViewModel : ObservableObject
   {
      #region Private fields

      private byte decimals;
      private short division;
      private short divisionMax;
      private int range;
      private WeightUnitsE units;
      private WeightCapacityE capacity;

      #endregion

      #region Public interface

      #region Constructors

      public WeightUnitViewModel(IView view, WeightUnits units)
      {
         Decimals = units.Decimals;
         Division = units.Division;
         DivisionMax = units.DivisionMax;
         Range = units.Range;
         Units = units.Units;
         Capacity = ConvertRange(Units, Range);
         view.DataContext = this;
      }

      #endregion

      #region Properties

      public byte Decimals
      {
         get
         {
            return decimals;           
         }
         set
         {          
            SetProperty(ref decimals, value);
         }
      }

      public short Division 
      { 
         get { return division; } 
         set { SetProperty(ref division, value); } 
      }

      public short DivisionMax
      {
         get
         {
            return divisionMax;         
         }
         set
         {
            SetProperty(ref divisionMax, value);
         }
      }

      public int Range { get { return range; } set { SetProperty(ref range, value); Capacity = ConvertRange(Units, Range); } }

      public WeightUnitsE Units
      {
         get
         {
            return units;
         }
         set
         {
            SetProperty(ref units, value);
            foreach (var conf in StoreVM.Instance.PredefinedWeigingsVM.PredefinedWeighings)
            {
               conf.Units = value;
            }
            StoreVM.Instance.ConfigurationVM.WeighingConfigurationVM.Units = value;
            switch (Units)
            {
               case WeightUnitsE.WEIGHT_UNITS_G:
                  Decimals = WeightUnitsC.G_DECIMALS;
                  break;
               case WeightUnitsE.WEIGHT_UNITS_KG:
                  Decimals = WeightUnitsC.KG_DECIMALS;
                  break;
               case WeightUnitsE.WEIGHT_UNITS_LB:
                  Decimals = WeightUnitsC.LB_DECIMALS;
                  break;
               default:
                  Decimals = 1;
                  break;
            }                    
         }
      }

      public WeightCapacityE Capacity
      {
         //get { return ConvertRange(Units, Range); }
         get { return capacity; }
         set { SetProperty(ref capacity, value); }
      }

      /// <summary>
      /// Minimum value of <see cref="WeightUnitViewModel.Division"/> property
      /// </summary>
      public int MinDivision
      {
         get
         {
            switch (Units)
            {
               case WeightUnitsE.WEIGHT_UNITS_G:
                  return WeightUnitsC.G_DIVISION;
               case WeightUnitsE.WEIGHT_UNITS_KG:
                  return WeightUnitsC.KG_DIVISION;
               case WeightUnitsE.WEIGHT_UNITS_LB:
                  return WeightUnitsC.LB_DIVISION;
               default:
                  return 1;
            }
         }
      }

      #endregion 

      #endregion

      #region Private helpers

      private static WeightCapacityE ConvertRange(WeightUnitsE units, int rangeNum)
      {
         switch (units)
         {
            case WeightUnitsE.WEIGHT_UNITS_G:
               if (rangeNum == WeightUnitsC.G_EXT_RANGE)
               {
                  return (WeightCapacityE.WEIGHT_CAPACITY_EXTENDED);
               }
               break;
            case WeightUnitsE.WEIGHT_UNITS_LB:
               if (rangeNum == WeightUnitsC.LB_EXT_RANGE)
               {
                  return (WeightCapacityE.WEIGHT_CAPACITY_EXTENDED);
               }
               break;
            case WeightUnitsE.WEIGHT_UNITS_KG:
               if (rangeNum == WeightUnitsC.KG_EXT_RANGE)
               {
                  return (WeightCapacityE.WEIGHT_CAPACITY_EXTENDED);
               }
               break;
         }
         return WeightCapacityE.WEIGHT_CAPACITY_NORMAL;
      }

      #endregion
   }
}
