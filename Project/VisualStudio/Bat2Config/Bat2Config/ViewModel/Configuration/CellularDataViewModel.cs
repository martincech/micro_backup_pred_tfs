﻿using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Configuration
{
   public class CellularDataViewModel : ObservableObject
   {
      #region Private fields

      private string apn;
      private string password;
      private string username;

      #endregion

      #region Public interface

      #region Constructors

      public CellularDataViewModel(IView view, CellularData data)
      {
         Apn = data.Apn;
         Password = data.Password;
         Username = data.Username;

         view.DataContext = this;
      }

      #endregion

      #region Properties

      public string Apn { get { return apn; } set { SetProperty(ref apn, value); } }
      public string Password { get { return password; } set { SetProperty(ref password, value); } }
      public string Username { get { return username; } set { SetProperty(ref username, value); } }   

      #endregion 

      #endregion
   }
}
