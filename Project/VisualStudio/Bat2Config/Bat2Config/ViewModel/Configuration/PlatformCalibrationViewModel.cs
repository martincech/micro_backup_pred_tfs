﻿using System.Collections.ObjectModel;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Configuration
{
   public class PlatformCalibrationViewModel : ObservableObject
   {
      #region Private fields

      private short delay;
      private short duration;
      private ObservableCollection<double> points;
      private WeightUnitsE units;
   
      #endregion

      #region Public interface

      #region Constructors

      public PlatformCalibrationViewModel(IView view, PlatformCalibration calibration/*, Units units*/)
      {
         Delay = calibration.Delay;
         Duration = calibration.Duration;
         Points = new ObservableCollection<double>();
         foreach (var point in calibration.Points)
         {
            Points.Add(point);
         }
         Units = (WeightUnitsE)units;
         ConvertPointsUnits(WeightUnitsE.WEIGHT_UNITS_G, Units);

         view.DataContext = this;
      }

      #endregion

      #region Properties

      public short Delay { get { return delay; } set { SetProperty(ref delay, value); } }
      public short Duration { get { return duration; } set { SetProperty(ref duration, value); } }

      public ObservableCollection<double> Points
      {
         get
         {
            return points;
         }
         set
         {
            SetProperty(ref points, value);
         }
      }

      public WeightUnitsE Units
      {
         get
         {
            return units;
         }
         set
         {
            ConvertPointsUnits(units, value);
            SetProperty(ref units, value);
         }
      }

      #endregion 

      #endregion

      #region Private helpers

      private void ConvertPointsUnits(WeightUnitsE oldUnits, WeightUnitsE newUnits)
      {
         for(var i = 0; i < Points.Count; i++)
         {
            //Points[i] = Math.Round(ConvertWeight.Convert(Points[i], (Units)oldUnits, (Units)newUnits), 3, MidpointRounding.AwayFromZero);
         }
      }

      #endregion
   }
}
