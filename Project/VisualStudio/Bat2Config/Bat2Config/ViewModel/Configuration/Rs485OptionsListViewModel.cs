﻿using System.Collections.ObjectModel;
using System.Linq;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Configuration
{
   public class Rs485OptionsListViewModel : ObservableObject
   {
      #region Private fields

      private Rs485OptionsViewModel model;
      private ObservableCollection<Rs485OptionsViewModel> models;

      private ModbusOptionsViewModel modbusOptions;
      private MegaviOptionsViewModel megaviOptions;
      private DacsOptionsViewModel dacsOptions;

      #endregion

      #region Public interface

      #region Constructors

      public Rs485OptionsListViewModel(IView view,
                                       Rs485Options rs1,
                                       Rs485Options rs2,
                                       ModbusOptions modbus,
                                       MegaviOptions megavi,
                                       DacsOptions dacs)
      {
         var m1 = new Rs485OptionsViewModel(rs1);
         var m2 = new Rs485OptionsViewModel(rs2);

         Models = new ObservableCollection<Rs485OptionsViewModel> { m1, m2 };
         Model = Models.First();

         ModbusOptions = new ModbusOptionsViewModel(modbus, rs1.Mode == Rs485ModeE.RS485_MODE_SENSOR_PACK);
         MegaviOptions = new MegaviOptionsViewModel(megavi);
         DacsOptions = new DacsOptionsViewModel(dacs);

         m1.PropertyChanged += m_PropertyChanged;
         m2.PropertyChanged += m_PropertyChanged;

         view.DataContext = this;
      }

      void m_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
      {
         if (e.PropertyName == "Mode")
         {
            var vm = (Rs485OptionsViewModel)sender;
            if (Models[0].Enabled && Models[1].Enabled &&
                ((Models[0].Mode == Rs485ModeE.RS485_MODE_SENSOR_PACK && Models[1].Mode == Rs485ModeE.RS485_MODE_MODBUS)
                 ||
                 (Models[0].Mode == Rs485ModeE.RS485_MODE_MODBUS && Models[1].Mode == Rs485ModeE.RS485_MODE_SENSOR_PACK) 
                 ||
                 (Models[0].Mode == Models[1].Mode)))
            {
               return;
            }

            if (vm.Mode == Rs485ModeE.RS485_MODE_SENSOR_PACK)
            {
               ModbusOptions.IsSensorPack = true;
            }

            if (vm.Mode == Rs485ModeE.RS485_MODE_MODBUS)
            {
               ModbusOptions.IsSensorPack = false;
            }
         }
      }

      #endregion

      #region Properties

      public Rs485OptionsViewModel Model { get { return model; } set { SetProperty(ref model, value); } }
      public ObservableCollection<Rs485OptionsViewModel> Models { get { return models; } set { SetProperty(ref models, value); } }

      public ModbusOptionsViewModel ModbusOptions { get { return modbusOptions; } set { SetProperty(ref modbusOptions, value); } }
      public MegaviOptionsViewModel MegaviOptions { get { return megaviOptions; } set { SetProperty(ref megaviOptions, value); } }
      public DacsOptionsViewModel DacsOptions { get { return dacsOptions; } set { SetProperty(ref dacsOptions, value); } }

      #endregion

      #endregion
   }
}
