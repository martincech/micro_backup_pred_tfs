﻿using System;
using Common.Library.Observable;
using Desktop.Client.ToolBarService.Interface;

namespace Desktop.Client.ToolBarService.Aplications
{
   public class ToolBarNode : ObservableObject, IToolBarNode
   {
      #region Private fields

      private object toolTip;
      private Action selectAction;
      private bool canSelect;
      private object viewDelegate;

      #endregion

      public ToolBarNode(string text, Action selectAction, string toolTip = null, bool canSelect = true)
      {
         this.ViewDelegate = text;
         this.toolTip = toolTip;
         this.selectAction = selectAction;
         this.canSelect = canSelect;
      }

      #region Implementation of IToolBarNode

      /// <summary>
      /// Gets the name of the node.
      /// </summary>
      public object ViewDelegate
      {
         get { return viewDelegate; }
         private set { SetProperty(ref viewDelegate, value); }
      }

      /// <summary>
      /// Gets the toolbar tip
      /// </summary>
      public object ToolTip
      {
         get { return toolTip; }
         set { SetProperty(ref toolTip, value); }
      }


      /// <summary>
      /// Action to be executed when toolbar node selected
      /// </summary>
      public Action SelectAction
      {
         get { return selectAction; }
         private set { SetProperty(ref selectAction, value); }
      }

      /// <summary>
      /// Can be this node selected?
      /// </summary>
      public bool CanSelect
      {
         get { return canSelect; }
         set { SetProperty(ref canSelect, value); }
      }

      #endregion
   }

}
