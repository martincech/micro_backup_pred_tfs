﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for SmsGateConfigurationView.xaml
   /// </summary>
   public partial class SmsGateConfigurationView : ISmsGateConfigurationsDetailView
   {
      public SmsGateConfigurationView()
      {
         InitializeComponent();
      }
   }
}
