﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for WeighingPlanView.xaml
   /// </summary>
   public partial class WeighingPlanView : IWeighingPlanDetailView
   {
      public WeighingPlanView()
      {
         InitializeComponent();
      }
   }
}
