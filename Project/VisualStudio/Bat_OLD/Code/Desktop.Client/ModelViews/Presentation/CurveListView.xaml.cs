﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for CurveListView.xaml
   /// </summary>
   public partial class CurveListView : ICurveListView
   {
      public CurveListView()
      {
         InitializeComponent();
      }
   }
}
