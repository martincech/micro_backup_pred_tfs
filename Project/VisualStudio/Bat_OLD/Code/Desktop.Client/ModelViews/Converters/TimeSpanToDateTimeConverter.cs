﻿using System;
using System.Windows.Data;

namespace Desktop.Client.ModelViews.Converters
{
   [ValueConversion(typeof(TimeSpan), typeof(DateTime))]
   public class TimeSpanToDateTimeConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (targetType == typeof(TimeSpan?) && value != null)
         {
            DateTime date = (DateTime)value;
            return new TimeSpan(date.Hour, date.Minute, date.Second);
         }
         return null;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (targetType == typeof(DateTime) && value != null)
         {
            TimeSpan time = (TimeSpan)value;
            return new DateTime(1, 1, 1, time.Hours, time.Minutes, time.Seconds);
         }
         return null;
      }
   }
}
