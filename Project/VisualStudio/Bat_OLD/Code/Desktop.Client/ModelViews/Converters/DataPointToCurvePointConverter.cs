﻿using System;
using System.Windows.Data;
using OxyPlot;
using ViewModels;

namespace Desktop.Client.ModelViews.Converters
{
   [ValueConversion(typeof(CurvePoint), typeof(DataPoint))]
   public class DataPointToCurvePointConverter : IValueConverter
   {
      public double transfer;

      public DataPointToCurvePointConverter()
      {
         transfer = 1.0;
      }
  
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (targetType == typeof(CurvePoint))
         {
            //DataPoint is non-nullable type, therefore isn't use cast by "as"
            DataPoint dPoint = (DataPoint)value;
            CurvePoint cPoint = new CurvePoint();
            cPoint.ValueX = (int)(dPoint.X);
            cPoint.ValueY = (int)(dPoint.Y / transfer);
            return cPoint;
         }      
         return null;  
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         CurvePoint cPoint = value as CurvePoint;
         if (targetType == typeof(DataPoint))
         {
            return new DataPoint(cPoint.ValueX, cPoint.ValueY * transfer);
         }  
         return null;
      }
   }
}
