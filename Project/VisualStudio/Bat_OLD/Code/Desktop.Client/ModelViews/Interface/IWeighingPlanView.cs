﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IWeighingPlanView : IView
   {
   }

   [InheritedExport]
   public interface IWeighingPlanDetailView : IWeighingPlanView, IDetailView
   {
   }

   [InheritedExport]
   public interface IWeighingPlanListView : IWeighingPlanView, IListView
   {
   }
}
