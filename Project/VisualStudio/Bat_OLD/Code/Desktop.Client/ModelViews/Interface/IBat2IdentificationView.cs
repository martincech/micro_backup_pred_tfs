﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IBat2IdentificationView : IView
   {
   }

   [InheritedExport]
   public interface IBat2IdentificationDetailView : IBat2IdentificationView, IDetailView
   {
   }
}
