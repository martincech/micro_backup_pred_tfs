﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IDacsConfigurationView : IView
   {
   }

   [InheritedExport]
   public interface IDacsConfigurationDetailView : IDacsConfigurationView, IDetailView
   {
   }
}
