﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface ISmsGateConfigurationsView : IView
   {
   }

   [InheritedExport]
   public interface ISmsGateConfigurationsDetailView : ISmsGateConfigurationsView, IDetailView
   {
   }
}
