﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.ComponentModel.Composition;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<DisplayConfiguration>))]
   public class DisplayConfigurationViewModel : ViewModel<DisplayConfiguration>
   {
      #region Private fields
      #endregion

      #region Public interface

      #region Constructors

      [ImportingConstructor]
      public DisplayConfigurationViewModel(IDisplayConfigurationView view)
         : base(view)
      {
      }

      #endregion      

      #endregion
   }
}
