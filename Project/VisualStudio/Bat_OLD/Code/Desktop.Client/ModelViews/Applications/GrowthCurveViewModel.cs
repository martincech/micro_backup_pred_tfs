﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<GrowthCurve>))]
   public class GrowthCurveViewModel : CurveViewModel<GrowthCurve>
   {
      #region Constructors

      [ImportingConstructor]
      public GrowthCurveViewModel(ICurveView view)
         : base(view)
      {
         //TODO: pro rustovou krivku konstanty zatim chybi
         //YMax = Bat2Library.GrowthCurveC.
         YMin = 0;
         //XMax = 99;  //TODO: use constant from Bat2Library

         Transfer = 1;
         DescriptionY = "Weight";
         UnitY = "g";
      }

      #endregion

      #region Additional validation

      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         string errorMsg = "";

         if (propertyName == "Model")
         {
            return base.AdditionalValidationRules(value, propertyName);
         }

         if (propertyName == "Name")
         {
            string name = value as string;
            if (name.Length <= Bat2Library.GrowthCurveC.CURVE_NAME_SIZE_MAX_LENGTH)
            {
               return base.AdditionalValidationRules(value, propertyName);
            }
            //errorMsg = Resources.GrowthCurve_Name_Size;
            errorMsg = "Growth curve - name size";
         }

         if (propertyName == "LastEditPoint" && value != null)
         {
            CurvePoint cPoint = value as CurvePoint;
            if (Model.Points.Count > Bat2Library.GrowthCurveC.POINT_COUNT_MAX)
            {
               //errorMsg = Resources.GrowthCurve_Points_Count;
               errorMsg = "Growth curve - too much point";
            }
            //TODO: zatim chybi konstanty pro rustovou krivku
            else if (cPoint.ValueY >= 0 &&
               /*cPoint.ValueY <= //maxY constant && */
                     cPoint.ValueX >= 0 /*&&
                     cPoint.ValueX <= //maxX constant */)
            {
               return base.AdditionalValidationRules(value, propertyName);
            }
            else
            {
               //errorMsg = Resources.GrowthCurve_InvalidPoint;
               errorMsg = "Growth curve - invalid point";
            }
         }

         var valRes = new List<ValidationResult>
         {
            new ValidationResult(errorMsg, new []{propertyName})    
         };
         return valRes;
      }

      #endregion
   }
}
