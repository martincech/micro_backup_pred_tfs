﻿using System.ComponentModel.Composition;
using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Applications
{
   [PartCreationPolicy(CreationPolicy.Shared)]
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<EthernetConfiguration>))]
   public class EthernetConfigurationViewModel : ViewModel<EthernetConfiguration>
   {
      #region Private fields
      #endregion

      #region Public interface

      #region Constructors

      [ImportingConstructor]
      public EthernetConfigurationViewModel(IEthernetConfigurationView view)
         : base(view)
      {
      }

      #endregion     

      #endregion
   }
}
