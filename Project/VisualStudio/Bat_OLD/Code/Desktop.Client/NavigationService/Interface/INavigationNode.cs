﻿using System.ComponentModel;
using Common.Desktop.Presentation;

namespace Desktop.Client.NavigationService.Interface
{
   /// <summary>
   /// Represents a navigation node.
   /// </summary>
   public interface INavigationNode : INotifyPropertyChanged
   {
      /// <summary>
      /// Gets the name of the node.
      /// </summary>
      string Name { get; }

      /// <summary>
      /// Gets the order of the node.
      /// </summary>
      double Order { get; set; }

      /// <summary>
      /// Current view of this node
      /// </summary>
      IView View { get; set; }
   }
}
