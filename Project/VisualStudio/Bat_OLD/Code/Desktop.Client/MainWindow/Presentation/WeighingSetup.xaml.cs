﻿using Common.Desktop.Presentation;
using Common.Library.Annotations;
using Desktop.Client.NavigationService.Aplications;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Client.MainWindow.Presentation
{
    /// <summary>
    /// Interaction logic for WeighingSetup.xaml
    /// </summary>
   public partial class WeighingSetup : IView, INotifyPropertyChanged
    {
        #region Fields

        private DateTime? setTime;
        private DateTime? startTime;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public interface

        #region Constructor

        public WeighingSetup()
        {
            SetTime = DateTime.Now;
            StartTime = DateTime.Now;

            InitializeComponent();
        }

        #endregion

        #region Properties

        public DateTime? SetTime
        {
            get
            {
                return
                   pcTimeCheckBox == null ||
                   (pcTimeCheckBox.IsChecked.HasValue && pcTimeCheckBox.IsChecked.Value && setTime.HasValue) ? DateTime.Now : setTime.Value;
            }
            set
            {
                if (value.Equals(setTime)) return;
                setTime = value;
                OnPropertyChanged();
            }
        }

        public DateTime? StartTime
        {
            get
            {
                return
                   startNowCheckBox == null ||
                   (startNowCheckBox.IsChecked.HasValue && startNowCheckBox.IsChecked.Value) ? null : startTime;
            }
            set
            {
                if (value.Equals(startTime)) return;
                startTime = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #endregion

        #region Private helpers

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UpdateBindings()
        {
            OnPropertyChanged("SetTime");
            OnPropertyChanged("StartTime");
        }

        private void startNowCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            UpdateBindings();
        }

        private void startNowCheckBox_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            UpdateBindings();
        }

        #endregion
    }
}
