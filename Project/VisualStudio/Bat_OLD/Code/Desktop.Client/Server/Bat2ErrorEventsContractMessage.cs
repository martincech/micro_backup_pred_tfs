﻿using Connection.Interface.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Client.Server
{
    internal class Bat2ErrorEventsContractMessage
    {
        public Bat2ErrorEventsContractMessage(Action<IBat2ErrorEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2ErrorEventsContract> ActionToInvoke { get; private set; }
    }
}
