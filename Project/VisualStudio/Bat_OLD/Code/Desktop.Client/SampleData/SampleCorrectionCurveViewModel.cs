﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
   public class SampleCorrectionCurveViewModel : CorrectionCurveViewModel
   {
      public SampleCorrectionCurveViewModel()
         : base(new MockICorrectionCurveView())
      {
         var curves = SampleDataProvider.CreateCorrectionCurves();
         var curve = curves.First();

         curve.Name = "Curve #1";
         curve.Points.Clear();
         for (int i = 1; i < 5; i++)
         {
            CurvePoint point = new CurvePoint();
            point.ValueX = i;
            point.ValueY = 10 * i;
            curve.Points.Add(point);
         }

         Model = curve;

         foreach (var c in curves)
         {
            Models.Add(c);
         }

         Models[1].Name = "Curve #2";
         Models[1].Points.Clear();
         Random random = new Random();
         for (int i = 0; i < 5; i++)
         {
            CurvePoint point = new CurvePoint();
            point.ValueX = 4 * i;
            point.ValueY = random.Next(1, 100);
            Models[1].Points.Add(point);
         }

         SelectedModels.Clear();
         SelectedModels.Add(Model);
      }
   }

   [PartNotDiscoverable]
   internal class MockICorrectionCurveView : MockView, ICurveDetailView
   {
   }
}
