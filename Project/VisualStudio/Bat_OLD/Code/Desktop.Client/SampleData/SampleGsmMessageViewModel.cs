﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
   public class SampleGsmMessageViewModel : GsmMessageViewModel
   {
      public SampleGsmMessageViewModel()
         : base(new MockIGsmMessageView())
      {
         var message = SampleDataProvider.CreateGsmMessages().First();

         message.Statistics.SendAt = new DateTime(Bat2Library.GsmMessageC.STATISTICS_SEND_AT_DEFAULT.Ticks);
         message.Statistics.AcceleratedPeriod = Bat2Library.GsmMessageC.STATISTICS_ACCELERATED_PERIOD_DEFAULT;
         message.Statistics.AccelerateFromDay = Bat2Library.GsmMessageC.STATISTICS_ACCELERATE_FROM_DAY_DEFAULT;
         message.Statistics.Period = Bat2Library.GsmMessageC.STATISTICS_PERIOD_DEFAULT;
         message.Statistics.StartFromDay = Bat2Library.GsmMessageC.STATISTICS_START_FROM_DAY_DEFAULT;
         message.Commands.Expiration = Bat2Library.GsmMessageC.COMMANDS_EXPIRATION_DEFAULT;
         message.Commands.CheckPhoneNumber = false;
         message.Commands.Enabled = true;
         message.RemoteControl.CheckPhoneNumber = true;
         message.RemoteControl.Enabled = false;
         message.Events.EventMask = Bat2Library.GsmEventMaskE.GSM_EVENT_STATUS_CHANGE;

         message.PowerConfiguration.SwitchOnDuration = Bat2Library.GsmMessageC.POWER_OPTIONS_SWITCH_ON_DURATION_DEFAULT;
         message.PowerConfiguration.SwitchOnPeriod = Bat2Library.GsmMessageC.POWER_OPTIONS_SWITCH_ON_PERIOD_DEFAULT;
         message.PowerConfiguration.Mode = Bat2Library.GsmPowerModeE.GSM_POWER_MODE_ON;

         GsmPowerOptionTime time1 = new GsmPowerOptionTime();
         time1.From = new DateTime(2014, 12, 1, 10, 10, 0);
         time1.To = new DateTime(2014, 12, 1, 14, 0, 0);
         GsmPowerOptionTime time2 = new GsmPowerOptionTime();
         time2.From = new DateTime(2014, 12, 1, 18, 30, 0);
         time2.To = new DateTime(2014, 12, 1, 20, 5, 0);
         message.GsmPowerOptionTimes.Clear();
         message.GsmPowerOptionTimes.Add(time1);
         message.GsmPowerOptionTimes.Add(time2);

         //GsmPowerOptiontimeViewColl.Add(time1);
         //GsmPowerOptiontimeViewColl.Add(time2);
       
         Model = message;
      }
   }

   [PartNotDiscoverable]
   internal class MockIGsmMessageView : MockView, IGsmMessageDetailView
   {
   }
}
