﻿using System.Linq;
using ViewModels;
using Desktop.Client.ModelViews.Applications;

namespace Desktop.Client.SampleData
{
   public class SampleRs485ConfigurationViewModel : Rs485ConfigurationViewModel
   {
      public SampleRs485ConfigurationViewModel()
      {
         Model = SampleDataProvider.CreateRs485Configurations().First();
         DacsConfiguration = SampleDataProvider.CreateDacsConfigurations().First();
         MegaviConfiguration = SampleDataProvider.CreateMegaviConfigurations().First();
         ModbusConfiguration = SampleDataProvider.CreateModbusConfigurations().First();
         SmsGateConfiguration = SampleDataProvider.CreateSmsGateConfigurations().First();
      }
   }
}
