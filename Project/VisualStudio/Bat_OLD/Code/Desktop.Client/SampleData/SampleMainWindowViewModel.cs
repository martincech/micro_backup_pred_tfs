﻿using Common.Library.Observable;
using Desktop.Client.MainWindow.Aplications;
using Desktop.Client.ToolBarService.Interface;
using System;
using Common.Desktop.Presentation;

namespace Desktop.Client.SampleData
{
   public class SampleMainWindowViewModel : MainWindowViewModel
   {
      public SampleMainWindowViewModel()
      {  
         DispatcherHelper.Initialize();
         ConnectedDevices = new SampleConnectedDevicesViewModel();
         NavigationNodes = new SampleNavigationNodeViewModel();
         
         ToolBarNodes.Add(new SampleToolBarNode { CanSelect = true, ViewDelegate = "Butoon1Text", ToolTip = "tooltip1" });
         ToolBarNodes.Add(new SampleToolBarNode { CanSelect = false, ViewDelegate = "Butoon2Text", ToolTip = "tooltip2" });
         ToolBarNodes.Add(new SampleToolBarNode { CanSelect = true, ViewDelegate = "Butoon3Text", ToolTip = "tooltip3" });
         ToolBarNodes.Add(new SampleToolBarNode { CanSelect = false, ViewDelegate = "Butoon4Text", ToolTip = "tooltip4" });
      }
   }

   public class SampleToolBarNode : ObservableObject, IToolBarNode
   {
      #region Private fields

      private object viewDelegate;
      private object toolTip;
      private Action selectAction;
      private bool canSelect;

      #endregion


      #region Implementation of IToolBarNode

      /// <summary>
      /// Gets the name of the node.
      /// </summary>
      public object ViewDelegate
      {
         get { return viewDelegate; }
         set { SetProperty(ref viewDelegate, value); }
      }

      /// <summary>
      /// Gets the toolbar tip
      /// </summary>
      public object ToolTip
      {
         get { return toolTip; }
         set { SetProperty(ref toolTip, value); }
      }


      /// <summary>
      /// Action to be executed when toolbar node selected
      /// </summary>
      public Action SelectAction
      {
         get { return selectAction; }
         set { SetProperty(ref selectAction, value); }
      }

      /// <summary>
      /// Can be this node selected?
      /// </summary>
      public bool CanSelect
      {
         get { return canSelect; }
         set { SetProperty(ref canSelect, value); }
      }

      #endregion

   }
}