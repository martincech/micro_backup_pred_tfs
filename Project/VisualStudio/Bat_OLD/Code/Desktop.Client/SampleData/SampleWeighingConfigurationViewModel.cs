﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
   public class SampleWeighingConfigurationViewModel : WeighingConfigurationViewModel
   {
      public SampleWeighingConfigurationViewModel()
         : base(new MockIWeighingConfigurationView())
      {
         var confs = SampleDataProvider.CreateWeighingConfigurations();
         var conf = confs.First();

         conf.FlockName = "Slepice special";
         conf.InitialDayNumber = Bat2Library.WeighingConfigurationC.CONFIGURATION_INITIAL_DAY_MIN;
         conf.Name = "Konfig007";
         conf.CorrectionCurveIndex = 23;
         conf.PredefinedIndex = 18;
         conf.DayStart = new TimeSpan(14, 36, 0);
         conf.WeighingPlanIndex = 128;
         conf.Planning = true;
         conf.MenuMask = Bat2Library.WeighingConfigurationMenuMaskE.WCMM_FLOCK;
         conf.Detection.Step = Bat2Library.PlatformStepModeE.PLATFORM_STEP_BOTH;
         conf.Detection.StabilizationRange = Bat2Library.WeighingConfigurationC.DETECTION_STABILIZATION_RANGE_DEFAULT;
         conf.Detection.StabilizationTime = Bat2Library.WeighingConfigurationC.DETECTION_STABILIZATION_TIME_DEFAULT;
         conf.Detection.Filter = Bat2Library.WeighingConfigurationC.DETECTION_FILTER_DEFAULT;
         conf.TargetWeight.Mode = Bat2Library.PredictionModeE.PREDICTION_MODE_AUTOMATIC;
         conf.TargetWeight.Growth = Bat2Library.PredictionGrowthE.PREDICTION_GROWTH_FAST;
         conf.TargetWeight.SexDifferentiation = Bat2Library.SexDifferentiationE.SEX_DIFFERENTIATION_NO;
         conf.TargetWeight.OnlineAdjustment = Bat2Library.OnlineAdjustmentE.TARGET_ADJUSTMENT_RECOMPUTE;

         conf.MaleGender.InitialWeight = 15;
         conf.MaleGender.GrowthCurveIndex = 28;
         conf.MaleGender.StandardCurveIndex = 64;
         conf.MaleGender.Acceptance.MarginAbove = Bat2Library.WeighingConfigurationC.ACCEPTANCE_MARGIN_ABOVE_DEFAULT;
         conf.MaleGender.Acceptance.MarginBelow = Bat2Library.WeighingConfigurationC.ACCEPTANCE_MARGIN_BELOW_DEFAULT;

         conf.FemaleGender.InitialWeight = 8;
         conf.FemaleGender.GrowthCurveIndex = 256;
         conf.FemaleGender.StandardCurveIndex = 512;
         conf.FemaleGender.Acceptance.MarginAbove = Bat2Library.WeighingConfigurationC.ACCEPTANCE_MARGIN_ABOVE_MAX;
         conf.FemaleGender.Acceptance.MarginBelow = Bat2Library.WeighingConfigurationC.ACCEPTANCE_MARGIN_BELOW_MIN;

         conf.Statistics.HourlyType = Bat2Library.StatisticShortTypeE.STATISTIC_SHORT_TYPE_CUMMULATIVE;
         conf.Statistics.HourlyPeriod = 8;
         conf.Statistics.UniformityRange = 32;
         conf.Statistics.HistogramConfiguration.Mode = Bat2Library.HistogramModeE.HISTOGRAM_MODE_RANGE;
         conf.Statistics.HistogramConfiguration.Step = 4;
         conf.Statistics.HistogramConfiguration.Range = 16;

         Model = conf;

         foreach (var c in confs)
         {
            Models.Add(c);
         }

         Models[1].FlockName = "Slepice normal";
         Models[1].InitialDayNumber = Bat2Library.WeighingConfigurationC.CONFIGURATION_INITIAL_DAY_MIN;
         Models[1].Name = "Konfig29";
         Models[1].CorrectionCurveIndex = 13;
         Models[1].PredefinedIndex = 5;
         Models[1].DayStart = new TimeSpan(8, 3, 0);
         Models[1].WeighingPlanIndex = 64;
         Models[1].Planning = true;
         Models[1].MenuMask = Bat2Library.WeighingConfigurationMenuMaskE.WCMM_PLANNING;
         Models[1].Detection.Step = Bat2Library.PlatformStepModeE.PLATFORM_STEP_ENTER;
         Models[1].Detection.StabilizationRange = Bat2Library.WeighingConfigurationC.DETECTION_STABILIZATION_RANGE_DEFAULT;
         Models[1].Detection.StabilizationTime = Bat2Library.WeighingConfigurationC.DETECTION_STABILIZATION_TIME_DEFAULT;
         Models[1].Detection.Filter = Bat2Library.WeighingConfigurationC.DETECTION_FILTER_DEFAULT;
         Models[1].TargetWeight.Mode = Bat2Library.PredictionModeE.PREDICTION_MODE_GROWTH_CURVE;
         Models[1].TargetWeight.Growth = Bat2Library.PredictionGrowthE.PREDICTION_GROWTH_SLOW;
         Models[1].TargetWeight.SexDifferentiation = Bat2Library.SexDifferentiationE.SEX_DIFFERENTIATION_NO;
         Models[1].TargetWeight.OnlineAdjustment = Bat2Library.OnlineAdjustmentE.TARGET_ADJUSTMENT_SINGLE;


         SelectedModels.Clear();
         SelectedModels.Add(Model);
      }
   }

   [PartNotDiscoverable]
   internal class MockIWeighingConfigurationView : MockView, IWeighingConfigurationDetailView
   {
   }
}
