//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
   using System;
   using System.ComponentModel.DataAnnotations;
   
   [MetadataType(typeof(GsmEventMetadata))]
   public partial class GsmEvent
   {
      internal sealed class GsmEventMetadata
   	{
      
        [Display(Name = "Event Mask")]
      	public Bat2Library.GsmEventMaskE EventMask { get; set; }
   
   	}
   }
}
