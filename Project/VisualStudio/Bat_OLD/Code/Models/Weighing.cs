//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
    
   
   using System;
   using System.Collections.Generic;
   using System.ComponentModel;
   using System.ComponentModel.DataAnnotations;
   using Common.Library.Observable;
   
   public partial class Weighing : RecursiveValidatableObservableObject
   {
      #region Private fields
      
      private Bat2Library.WeighingStatusE status;
      private Bat2Library.WeighingStatusE prevStatus;
      private bool dayActive;
      private short day;
      private int targetWeightMale;
      private int targetWeightFemale;
      private short correction;
      private System.DateTime dayCloseAt;
      private System.DateTime startAt;
      private short id;
      private System.TimeSpan dayDuration;
      private CorrectionCurve correctionCurve;
      private GrowthCurve growthCurveMale;
      private WeighingPlan weighingPlan;
      private GrowthCurve growthCurveFemale;
      
      #endregion
      
      #region Constructors
   
      static Weighing()
      {
         TypeDescriptor.AddProviderTransparent(
            new AssociatedMetadataTypeTypeDescriptionProvider(
               typeof(Weighing), typeof(WeighingMetadata)), typeof(Weighing));
      }
      
      public Weighing()
      {
         this.CorrectionCurve = new CorrectionCurve();
         this.GrowthCurveMale = new GrowthCurve();
         this.WeighingPlan = new WeighingPlan();
         this.GrowthCurveFemale = new GrowthCurve();
         AditionalConstructor();
      }
   
      partial void AditionalConstructor();
   
      #endregion
      public virtual Bat2Library.WeighingStatusE Status { get{ return status; } set{ SetPropertyAndValidate(ref status, value); } }
      public virtual Bat2Library.WeighingStatusE PrevStatus { get{ return prevStatus; } set{ SetPropertyAndValidate(ref prevStatus, value); } }
      public virtual bool DayActive { get{ return dayActive; } set{ SetPropertyAndValidate(ref dayActive, value); } }
      public virtual short Day { get{ return day; } set{ SetPropertyAndValidate(ref day, value); } }
      public virtual int TargetWeightMale { get{ return targetWeightMale; } set{ SetPropertyAndValidate(ref targetWeightMale, value); } }
      public virtual int TargetWeightFemale { get{ return targetWeightFemale; } set{ SetPropertyAndValidate(ref targetWeightFemale, value); } }
      public virtual short Correction { get{ return correction; } set{ SetPropertyAndValidate(ref correction, value); } }
      public virtual System.DateTime DayCloseAt { get{ return dayCloseAt; } set{ SetPropertyAndValidate(ref dayCloseAt, value); } }
      public virtual System.DateTime StartAt { get{ return startAt; } set{ SetPropertyAndValidate(ref startAt, value); } }
      public virtual short Id { get{ return id; } set{ SetPropertyAndValidate(ref id, value); } }
      public virtual System.TimeSpan DayDuration { get{ return dayDuration; } set{ SetPropertyAndValidate(ref dayDuration, value); } }
   
      public virtual CorrectionCurve CorrectionCurve { get{ return correctionCurve; } set{ SetPropertyAndValidate(ref correctionCurve, value); } }
      public virtual GrowthCurve GrowthCurveMale { get{ return growthCurveMale; } set{ SetPropertyAndValidate(ref growthCurveMale, value); } }
      public virtual WeighingPlan WeighingPlan { get{ return weighingPlan; } set{ SetPropertyAndValidate(ref weighingPlan, value); } }
      public virtual GrowthCurve GrowthCurveFemale { get{ return growthCurveFemale; } set{ SetPropertyAndValidate(ref growthCurveFemale, value); } }
   }
}
