﻿using System;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class MegaviConfiguration
   {
      public void Map(Connection.Interface.Domain.MegaviOptions configuration)
      {
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.MegaviOptions_can_t_be_null);
         }

         ModuleCode = configuration.Code;
         ModuleAddress = configuration.Address;
      }
   }
}
