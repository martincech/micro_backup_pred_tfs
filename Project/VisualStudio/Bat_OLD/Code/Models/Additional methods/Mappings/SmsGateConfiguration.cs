﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class SmsGateConfiguration
   {
      public void Map(Connection.Interface.Domain.SmsGateOptions configuration)
      {
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.SmsGateOptions_can_t_be_null);
         }

         Parity = (SmsGateParityE)configuration.Parity;
         DataBits = (SmsGateDataBitsE)configuration.DataBits;
         BaudRate = configuration.BaudRate;
      }
   }
}
