﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class WeighingConfiguration
   {
      public void Map(Connection.Interface.Domain.WeighingConfiguration configuration)
      {
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.WeighingConfiguration_can_t_be_null);
         }

         InitialDayNumber = configuration.InitialDay;
         MenuMask = (WeighingConfigurationMenuMaskE)configuration.MenuMask;
         FlockName = configuration.Flock;
         Name = configuration.Name;
         CorrectionCurveIndex = configuration.CorrectionCurveIndex;
         PredefinedIndex = configuration.PredefinedIndex;
         DayStart = TimeSpan.FromTicks(configuration.DayStart.Ticks);
         TargetWeight.OnlineAdjustment = (OnlineAdjustmentE)configuration.OnlineAdjustment;
         TargetWeight.Sex = (SexE)configuration.Sex;
         TargetWeight.SexDifferentiation = (SexDifferentiationE)configuration.SexDifferentiation;
         TargetWeight.Growth = (PredictionGrowthE)configuration.Growth;
         TargetWeight.Mode = (PredictionModeE)configuration.Mode;
         Detection.Step = (PlatformStepModeE)configuration.Step;
         Detection.StabilizationRange = configuration.StabilizationRange;
         Detection.StabilizationTime = configuration.StabilizationTime;
         Detection.Filter = configuration.Filter;
         MaleGender.InitialWeight = configuration.MaleInitialWeight;
         MaleGender.GrowthCurveIndex = configuration.MaleGrowthCurveIndex;
         MaleGender.StandardCurveIndex = configuration.MaleStandardCurveIndex;
         MaleGender.Acceptance.MarginBelow = configuration.MaleMarginBelow;
         MaleGender.Acceptance.MarginAbove = configuration.MaleMarginAbove;
         FemaleGender.InitialWeight = configuration.FemaleInitialWeight;
         FemaleGender.GrowthCurveIndex = configuration.FemaleGrowthCurveIndex;
         FemaleGender.StandardCurveIndex = configuration.FemaleStandardCurveIndex;
         FemaleGender.Acceptance.MarginBelow = configuration.FemaleMarginBelow;
         FemaleGender.Acceptance.MarginAbove = configuration.FemaleMarginAbove;
         Statistics.HourlyType = (StatisticShortTypeE)configuration.ShortType;
         Statistics.HourlyPeriod = configuration.ShortPeriod;
         Statistics.UniformityRange = configuration.UniformityRange;
         Statistics.HistogramConfiguration.Step = configuration.HistogramStep;
         Statistics.HistogramConfiguration.Range = configuration.HistogramRange;
         Statistics.HistogramConfiguration.Mode = (HistogramModeE)configuration.HistogramMode;
         WeighingPlanIndex = configuration.WeighingPlanIndex;
         Planning = configuration.Planning;
      }
   }
}
