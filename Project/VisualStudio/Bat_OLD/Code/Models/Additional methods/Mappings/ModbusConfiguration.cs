﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class ModbusConfiguration
   {
      public void Map(Connection.Interface.Domain.ModbusOptions configuration)
      {
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.ModbusConfiguration_can_t_be_null);
         }

         ReplyDelay = configuration.ReplyDelay;
         Parity = (ModbusParityE)configuration.Parity;
         DataBits = (ModbusDataBitsE)configuration.DataBits;
         BaudRate = configuration.BaudRate;
         Address = configuration.Address;
         Mode = (ModbusTransmissionModeE)configuration.Mode;
      }
   }
}
