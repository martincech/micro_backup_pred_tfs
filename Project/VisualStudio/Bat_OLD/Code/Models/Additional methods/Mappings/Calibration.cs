﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class Calibration
   {
      public void Map(Connection.Interface.Domain.PlatformCalibration calibration)
      {
         if (calibration == null)
         {
            throw new ArgumentNullException("calibration", Resources.PlatformCalibration_can_t_be_null);
         }
         // Calibration
         Delay = calibration.Delay;
         Duration = calibration.Duration;

         // CalibrationPoint
         CalibrationPoints = new ObservableCollection<CalibrationPoint>();

         if (calibration.Points == null) return;

         foreach (var point in calibration.Points.Select(weigh => new CalibrationPoint {Weight = weigh}))
         {
            CalibrationPoints.Add(point);
         }
      }
   }
}