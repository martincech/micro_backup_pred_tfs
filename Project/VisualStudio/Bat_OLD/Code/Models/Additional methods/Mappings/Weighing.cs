﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class Weighing
   {
      public void Map(Connection.Interface.Domain.WeighingContext weighing)
      {
         if (weighing == null)
         {
            throw new ArgumentNullException("weighing", Resources.WeighingContext_can_t_be_null);
         }
         // Weighing
         Status = (WeighingStatusE)weighing.Status;
         PrevStatus = (WeighingStatusE)weighing.LastStatus;
         DayActive = Convert.ToBoolean((byte) weighing.DayActive);
         Day = weighing.Day;
         TargetWeightFemale = weighing.TargetWeightFemale;
         TargetWeightMale = weighing.TargetWeight;
         Correction = weighing.Correction;
         DayCloseAt = weighing.DayCloseAt;
         StartAt = weighing.StartAt;
         Id = weighing.Id;
         DayDuration = weighing.DayDuration;

         if (weighing.WeighingPlan != null)
         {
            // WeighingPlan
            WeighingPlan.Map(weighing.WeighingPlan);              
         }
         if (weighing.CorrectionCurve != null)
         {
            // CorrectionCurve
            CorrectionCurve.Map(weighing.CorrectionCurve);
         }

         // GrowthCurve
         if (weighing.GrowthCurveFemale != null)
         {
            GrowthCurveFemale.Map(weighing.GrowthCurveFemale);
         }
         if (weighing.GrowthCurveMale != null)
         {
            GrowthCurveMale.Map(weighing.GrowthCurveMale);
         }
      }
   }
}
