﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class DacsConfiguration
   {
      public void Map(Connection.Interface.Domain.DacsOptions configuration)
      {
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.DacsOptions_can_t_be_null);
         }

         Address = configuration.Address;
         Version = (DacsVersionE)configuration.Version;
      }
   }
}
