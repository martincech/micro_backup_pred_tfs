﻿using System;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class Rs485Configuration
   {
      public void Map(Connection.Interface.Domain.Rs485Options configuration)
      {
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.Rs485Options_can_t_be_null);
         }

         Mode = (Rs485ModeE)configuration.Mode;
         Enabled = configuration.Enabled;
         PhysicalInterfaceAddress = configuration.PhysicalDeviceAddress;
      }
   }
}
