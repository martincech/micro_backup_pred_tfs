﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class GsmMessage
   {
      public void Map(Connection.Interface.Domain.GsmMessage message)
      {
         if (message == null)
         {
            throw new ArgumentNullException("message", Resources.GsmMessage_can_t_be_null);
         }

         //gsm message
         Statistics.SendAt = message.SendAt;
         Statistics.AcceleratedPeriod = message.AcceleratedPeriod;
         Statistics.AccelerateFromDay = message.AccelerateFromDay;
         Statistics.Period = message.Period;
         Statistics.StartFromDay = message.StartFromDay;
         Commands.Expiration = message.CommandsExpiration;
         Commands.CheckPhoneNumber = message.CommandsCheckPhoneNumber;
         Commands.Enabled = message.CommandsEnabled;
         RemoteControl.CheckPhoneNumber = message.RemoteControlCheckPhoneNumber;
         RemoteControl.Enabled = message.RemoteControlEnabled;
         Events.EventMask = (GsmEventMaskE)message.EventMask;

         //gsm power configuration
         PowerConfiguration.SwitchOnDuration = message.SwitchOnDuration;
         PowerConfiguration.SwitchOnPeriod = message.SwitchOnPeriod;
         PowerConfiguration.Mode = (GsmPowerModeE)message.Mode;

         //gsm power option time
         GsmPowerOptionTimes = new ObservableCollection<GsmPowerOptionTime>();
         if (message.SwitchOnTimes == null) return;
         foreach (var powerTime in message.SwitchOnTimes.Select(time => new GsmPowerOptionTime {From = time.From, To = time.To}))
         {
            GsmPowerOptionTimes.Add(powerTime);
         }
      }
   }
}
