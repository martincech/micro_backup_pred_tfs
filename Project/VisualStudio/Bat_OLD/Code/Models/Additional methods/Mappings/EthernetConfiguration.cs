﻿using System;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class EthernetConfiguration
   {
      public void Map(Connection.Interface.Domain.EthernetOptions configuration)
      {
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.EthernetOptions_can_t_be_null);
         }

         Port = configuration.Port;
         Address = configuration.Address;
         Enabled = configuration.Enabled;
      }
   }
}
