﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Bat2Library;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class ArchiveItem
   {
      public void Map(Connection.Interface.Domain.ArchiveItem item)
      {
         if (item == null)
         {
            throw new ArgumentNullException("item", Resources.ArchiveItem_can_t_be_null);
         }

         MaleTargetWeight = item.TargetWeight;
         FemaleTargetWeight = item.TargetWeightFemale;
         ZoneId = item.ZoneIdentifier;
         StatisticOriginator = item.StatisticCalculate;
         Day = item.Day;
         Timestamp = item.Timestamp;
         Average = item.Average;
         Count = item.Count;
         Gain = item.Gain;
         Sigma = item.Sigma;
         Uniformity = item.Uniformity;
         From = new DateTime(1, 1, 1, item.HourFrom, 0, 0);
         To = new DateTime(1, 1, 1, item.HourTo, 0, 0);
         Sex = (SexE)item.Sex;

         //HistogramSlot
         Histogram = new ObservableCollection<HistogramSlot>();
         if (item.Histogram == null) return;
         foreach (var slot in item.Histogram.Select(val => new HistogramSlot {Value = val}))
         {
            Histogram.Add(slot);
         }
      }
   }
}
