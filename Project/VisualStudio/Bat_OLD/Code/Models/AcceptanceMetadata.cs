//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
   using System;
   using System.ComponentModel.DataAnnotations;
   
   [MetadataType(typeof(AcceptanceMetadata))]
   public partial class Acceptance
   {
      internal sealed class AcceptanceMetadata
   	{
      
   	   
        [Range(Bat2Library.WeighingConfigurationC.ACCEPTANCE_MARGIN_BELOW_MIN, Bat2Library.WeighingConfigurationC.ACCEPTANCE_MARGIN_BELOW_MAX, 
          ErrorMessage=null, ErrorMessageResourceName ="MarginBelowRange", ErrorMessageResourceType = typeof(ViewModels.Properties.Resources))]
        [Display(Name = "Margin Below")]
      	public byte MarginBelow { get; set; }
   
   	   
        [Range(Bat2Library.WeighingConfigurationC.ACCEPTANCE_MARGIN_ABOVE_MIN, Bat2Library.WeighingConfigurationC.ACCEPTANCE_MARGIN_ABOVE_MAX, 
          ErrorMessage=null, ErrorMessageResourceName ="MarginAboveRange", ErrorMessageResourceType = typeof(ViewModels.Properties.Resources))]
        [Display(Name = "Margin Above")]
      	public byte MarginAbove { get; set; }
   
   	}
   }
}
