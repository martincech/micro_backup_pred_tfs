﻿using System.Collections.Generic;
using System.Windows.Controls;
using ViewModels;
using Expression.Blend.SampleData.ConnectedDevices;

namespace SketchScreens
{
	/// <summary>
	/// Interaction logic for ArchiveItemListView.xaml
	/// </summary>
   public partial class ArchiveItemListView : UserControl
   {
      #region Properties and fields

      private bool recordType;    
   
      #endregion

      #region Public interfaces

      #region Constructors

      public ArchiveItemListView()
      {
	     this.InitializeComponent();

         recordType = false;
      }

      #endregion

      #endregion

      #region Private Helpers

      private void PreviousArchiveItem(object sender, System.Windows.RoutedEventArgs e)
      {       
      }

      private void NextArchiveItem(object sender, System.Windows.RoutedEventArgs e)
      {
      }

      private void ChangeRecordType(object sender, System.Windows.RoutedEventArgs e)
      {
         if (recordType == false)
         {
            recordTypeButton.Content = SketchScreens.Properties.Resources.Daily;
            recordTypeTextBlock.Text = SketchScreens.Properties.Resources.Hourly;
            recordType = true;
         }
         else
         {
            recordTypeButton.Content = SketchScreens.Properties.Resources.Hourly;
            recordTypeTextBlock.Text = SketchScreens.Properties.Resources.Daily;
            recordType = false;
         }
      }

      private void archiveList_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (archiveList.SelectedIndex == -1)
         {
            archiveList.SelectedIndex = 0;
         }
      }

      #endregion
   }
}