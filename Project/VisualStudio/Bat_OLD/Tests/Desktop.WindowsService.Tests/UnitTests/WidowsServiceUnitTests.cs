﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common.Library.Messenger;
using Desktop.WindowsService.Tests.FunctionalTest;
using System.ServiceModel;
using System.Threading;
using Connection.Interface.Contract;
using Connection.Interface.Domain;
using Connection.Server;
using Bat2Library;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Globalization;

namespace Desktop.WindowsService.Tests.UnitTests
{
    [TestClass]
    public class WidowsServiceUnitTests_Connected : ServiceTestBase
    {
        [TestInitialize()]
        public void Init()
        {
            base.InitTests();
        }               
        
        [TestCleanup()]
        public void Cleanup()
        {
            base.CleanUpTests(); 
        }

        #region Tests when device connected

        [TestMethod]
        public void ReadData_WhenDeviceConnected()
        {
            
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            LoadConnectedDevices(contract);

            replyRecived = false;
            expectedEventType = Bat2EventType.DataRead;
            contract.ReadData(actualData);
            waitHandle.WaitOne(5000);
            //Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void SaveData_WhenDeviceConnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            LoadConnectedDevices(contract);

            replyRecived = false;
            expectedEventType = Bat2EventType.DataRead;
            contract.ReadContactList(actualData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);

            List<Contact> oldContacts = actualData.Contacts.ToList();
            oldContacts.Add(new Contact() { Name = "Test", PhoneNumber = "+420888999444" });
            Assert.AreEqual(actualData.Contacts.Count() + 1,oldContacts.Count);
            actualData.Contacts = oldContacts;
            replyRecived = false;
            expectedEventType = Bat2EventType.DataWriten;
            contract.WriteContactList(actualData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);

            actualData.Contacts = null;
            Assert.IsNull(actualData.Contacts);

            replyRecived = false;
            expectedEventType = Bat2EventType.DataRead;
            contract.ReadContactList(actualData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);

            Assert.AreEqual(oldContacts.Count, actualData.Contacts.Count());
            Assert.AreEqual(oldContacts.LastOrDefault().Name, actualData.Contacts.LastOrDefault().Name);
            Assert.AreEqual(oldContacts.LastOrDefault().PhoneNumber, actualData.Contacts.LastOrDefault().PhoneNumber);

            oldContacts.Remove(oldContacts.Last());
            actualData.Contacts = oldContacts;
            replyRecived = false;
            expectedEventType = Bat2EventType.DataWriten;
            contract.WriteContactList(actualData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void StartStopWeighingNow_WhenDeviceConnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            LoadConnectedDevices(contract);
            LoadContextData(contract, actualData);

            replyRecived = false;
            switch (actualData.Context.WeighingContext.Status)
            {
                case (byte)WeighingStatusE.WEIGHING_STATUS_WEIGHING:
                case (byte)WeighingStatusE.WEIGHING_STATUS_RELEASED:
                case (byte)WeighingStatusE.WEIGHING_STATUS_SLEEP:
                    expectedEventType = Bat2EventType.WeighingStopped;
                    contract.WeighingStop(actualData);
                    break;
                case (byte)WeighingStatusE.WEIGHING_STATUS_UNDEFINED:
                case (byte)WeighingStatusE.WEIGHING_STATUS_CALIBRATION:
                case (byte)WeighingStatusE.WEIGHING_STATUS_DIAGNOSTICS:
                    Assert.Fail("Device is not in stopped or weighing status.");
                    break;
                case (byte)WeighingStatusE.WEIGHING_STATUS_STOPPED:
                case (byte)WeighingStatusE.WEIGHING_STATUS_SUSPENDED:
                    expectedEventType = Bat2EventType.WeighingStarted;
                    contract.WeighingStart(actualData, null);
                    break;
                   
            }

            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void TimeGet_WhenDeviceConnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            LoadConnectedDevices(contract);

            replyRecived = false;
            expectedEventType = Bat2EventType.TimeGet;
            contract.TimeGet(actualData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void TimeSet_WhenDeviceConnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            LoadConnectedDevices(contract);

            replyRecived = false;
            expectedEventType = Bat2EventType.TimeSet;
            contract.TimeSet(actualData, DateTime.Now);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void SuspendReleaseWeighing_WhenDeviceConnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            LoadConnectedDevices(contract);
            LoadContextData(contract, actualData);

            if (actualData.Context.WeighingContext.Status == (byte)WeighingStatusE.WEIGHING_STATUS_STOPPED) 
            {
                replyRecived = false;
                expectedEventType = Bat2EventType.WeighingStarted;
                contract.WeighingStart(actualData, null);
                waitHandle.WaitOne(2000);
                Assert.IsTrue(replyRecived);
            }

            replyRecived = false;
            switch (actualData.Context.WeighingContext.Status)
            {
                case (byte)WeighingStatusE.WEIGHING_STATUS_SUSPENDED:
                case (byte)WeighingStatusE.WEIGHING_STATUS_SLEEP:
                case (byte)WeighingStatusE.WEIGHING_STATUS_WAIT:
                    expectedEventType = Bat2EventType.WeighingReleased;
                    contract.WeighingRelease(actualData);
                    break;
                case (byte)WeighingStatusE.WEIGHING_STATUS_UNDEFINED:
                case (byte)WeighingStatusE.WEIGHING_STATUS_CALIBRATION:
                case (byte)WeighingStatusE.WEIGHING_STATUS_DIAGNOSTICS:
                    Assert.Fail("Device is not in stopped or weighing status.");
                    break;
                case (byte)WeighingStatusE.WEIGHING_STATUS_RELEASED:
                case (byte)WeighingStatusE.WEIGHING_STATUS_WEIGHING:
                    expectedEventType = Bat2EventType.WeighingSuspended;
                    contract.WeighingSuspend(actualData);
                    break;
            }

            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }
       


        [TestMethod]
        public void SendArchiveData_WhenDeviceConnected()
        {  
            IPAddress localAdd = IPAddress.Parse(SERVER_IP);
            TcpListener listener = new TcpListener(localAdd, PORT_NO);
            listener.Server.ReceiveTimeout = 5000;
            listener.Server.SendTimeout = 5000;
            
            listener.Start();
            TcpClient client = listener.AcceptTcpClient();
            NetworkStream nwStream = client.GetStream();
            byte[] buffer = new byte[client.ReceiveBufferSize];
            int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);
            string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);
            Assert.IsTrue(dataReceived.Contains(actualData.Configuration.VersionInfo.SerialNumber.ToString("X")));
            client.Close();
            listener.Stop();
        }

        

        #region Private methods
        private void Receive(Socket socket, byte[] buffer, int offset, int size, int timeout)
        {
            int startTickCount = Environment.TickCount;
            int received = 0;  // how many bytes is already received
            do
            {
                if (Environment.TickCount > startTickCount + timeout)
                    throw new Exception("Timeout.");
                try
                {
                    received += socket.Receive(buffer, offset + received, size - received, SocketFlags.None);
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode == SocketError.WouldBlock ||
                        ex.SocketErrorCode == SocketError.IOPending ||
                        ex.SocketErrorCode == SocketError.NoBufferSpaceAvailable)
                    {
                        // socket buffer is probably empty, wait and try again
                        Thread.Sleep(30);
                    }
                    else
                        throw ex;  // any serious error occurr
                }
            } while (received < size);
        }

        #endregion

        #endregion
    }

    [TestClass]
    public class WidowsServiceUnitTests_Disconnected : ServiceTestBase
    {
        [TestInitialize()]
        public void Init()
        {
            base.InitTests();
        }

        [TestCleanup()]
        public void Cleanup()
        {
            base.CleanUpTests();
        }

        [TestMethod]
        public void CanRestartService()
        {
            eventHandler.EventReceived -= EventRecived;
            Assert.IsTrue(commandHost.IsOpened);
            commandHost.Stop();
            Assert.IsFalse(commandHost.IsOpened);
            commandHost.Start();
            Assert.IsTrue(commandHost.IsOpened);
            eventHandler.EventReceived += EventRecived;
        }


        #region Tests when device disconnected

        [TestMethod]
        public void ReadData_WhenDeviceDisconnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            replyRecived = false;
            expectedEventType = Bat2EventType.ErrorRised;
            contract.ReadData(dummyData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void SaveData_WhenDeviceDisconnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);

            expectedEventType = Bat2EventType.ErrorRised;
            replyRecived = false;
            contract.ConnectedDevicesGet();
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
            if (actualData.Configuration.VersionInfo.SerialNumber > 0) 
            {
                return;
            }

            dummyData.Contacts = new List<Contact>();
            List<Contact> oldContacts = new List<Contact>();
            oldContacts.Add(new Contact() { Name = "Test", PhoneNumber = "+420888999444" });
            Assert.AreEqual(dummyData.Contacts.Count() + 1, oldContacts.Count);         
            replyRecived = false;
            expectedEventType = Bat2EventType.ErrorRised;
            contract.WriteContactList(dummyData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void StartStop_WhenDeviceDisconnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            replyRecived = false;
            expectedEventType = Bat2EventType.ErrorRised;
            contract.WeighingStart(dummyData, null);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);

            replyRecived = false;
            expectedEventType = Bat2EventType.ErrorRised;
            contract.WeighingStop(dummyData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void SuspendRelease_WhenDeviceDisconnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            replyRecived = false;
            expectedEventType = Bat2EventType.ErrorRised;
            contract.WeighingRelease(dummyData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);

            replyRecived = false;
            expectedEventType = Bat2EventType.ErrorRised;
            contract.WeighingSuspend(dummyData);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void TimeGet_WhenDeviceDisconnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            replyRecived = false;
            expectedEventType = Bat2EventType.ErrorRised;
            contract.ConnectedDevicesGet();
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void TimeSet_WhenDeviceDisconnected()
        {
            IBat2CommandContract contract = commandContract.CreateChannel();
            Assert.IsTrue(commandHost.IsOpened);
            replyRecived = false;
            expectedEventType = Bat2EventType.ErrorRised;
            contract.ConnectedDevicesGet();
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        [TestMethod]
        public void FileTransfer_Test()
        {
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(@"D:\Bat zaloha\ahoj.txt");
            ChannelFactory<IBat2FileCommandContract> fileFactory = new ChannelFactory<IBat2FileCommandContract>("IBat2FileCommandContract");
            RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
            using (System.IO.FileStream stream =
                   new System.IO.FileStream(fileInfo.FullName,
                   System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                uploadRequestInfo.FileName = "ahoj.txt";
                uploadRequestInfo.Length = fileInfo.Length;
                uploadRequestInfo.FileByteStream = stream;
                fileFactory.CreateChannel().FlashDevice(uploadRequestInfo);
            }
        }

      

        #endregion
    }

    [TestClass]
    public class WindosServiceUnitTests_TcpMessageOnly : ServiceTestBase
    {
        private TcpListener listener;
        private Socket socket;

        [TestInitialize()]
        public void Init() 
        {
            IPAddress localAdd = IPAddress.Parse(SERVER_IP);
            listener = new TcpListener(localAdd, PORT_NO);
            listener.Server.ReceiveTimeout = 5000;
            listener.Server.SendTimeout = 5000;
            listener.Start();

            TcpClient tcpClient = new TcpClient(SERVER_IP, PORT_NO);
            socket = tcpClient.Client;
            dummyData = GetDummyData();
        }

        [TestCleanup()]
        public void Cleanup() 
        {
           
            listener.Stop();
        }

        [TestMethod]
        public void SendArchiveData_MessageShortOnly()
        {   
            string shortMessage = SocketSendArchive.GenerateShortMessage(
                dummyData.Archive.ToList()[0], dummyData.Configuration.VersionInfo.SerialNumber, 
                (Bat2Library.WeightUnitsE)dummyData.Configuration.WeightUnits.Units);
            
            SocketSendArchive.Send(socket, System.Text.Encoding.UTF8.GetBytes(shortMessage), 0, shortMessage.Length, 5000);


            TcpClient client = listener.AcceptTcpClient();
            NetworkStream nwStream = client.GetStream();
            byte[] buffer = new byte[client.ReceiveBufferSize];
            int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);
            string dataReceived = Encoding.UTF8.GetString(buffer, 0, bytesRead);
            Assert.AreEqual(shortMessage, dataReceived);
            client.Close();
         
        }

        [TestMethod]
        public void SendArchiveData_MessageLongOnly()
        {
      
            string longMessage = SocketSendArchive.GenerateLongMessage(
                dummyData.Archive.ToList()[0], dummyData.Archive.ToList()[1], dummyData.Configuration.VersionInfo.SerialNumber, 
                (Bat2Library.WeightUnitsE)dummyData.Configuration.WeightUnits.Units);
            SocketSendArchive.Send(socket, System.Text.Encoding.UTF8.GetBytes(longMessage), 0, longMessage.Length, 5000);


            TcpClient client = listener.AcceptTcpClient();
            NetworkStream nwStream = client.GetStream();
            byte[] buffer = new byte[client.ReceiveBufferSize];
            int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);
            string dataReceived = Encoding.UTF8.GetString(buffer, 0, bytesRead);
            Assert.AreEqual(longMessage, dataReceived);
            client.Close();
          
        }

        [TestMethod]
        public void CreateArchiveData_MessageLong()
        {
            ArchiveItem fDummy = dummyData.Archive.Where(p => p.Sex == (byte)Bat2Library.SexE.SEX_FEMALE).FirstOrDefault();
            ArchiveItem mDummy = dummyData.Archive.Where(p => p.Sex == (byte)Bat2Library.SexE.SEX_MALE).FirstOrDefault();

            string longMessage = SocketSendArchive.GenerateLongMessage(fDummy, mDummy,
                dummyData.Configuration.VersionInfo.SerialNumber, (Bat2Library.WeightUnitsE)dummyData.Configuration.WeightUnits.Units);
            Bat2DeviceData data = ParseMessage(longMessage,(Bat2Library.WeightUnitsE)dummyData.Configuration.WeightUnits.Units);

            Assert.IsNotNull(data);
            Assert.AreEqual(dummyData.Configuration.VersionInfo.SerialNumber, data.Configuration.VersionInfo.SerialNumber);
            ArchiveItem m = data.Archive.Where(p => p.Sex == (byte)Bat2Library.SexE.SEX_MALE).FirstOrDefault();
            ArchiveItem f = data.Archive.Where(p => p.Sex == (byte)Bat2Library.SexE.SEX_FEMALE).FirstOrDefault();
            Assert.IsNotNull(m);
            Assert.IsNotNull(f);

            Assert.AreEqual(mDummy.Day, m.Day);
            Assert.AreEqual(fDummy.Day ,f.Day);

            Assert.AreEqual(mDummy.Timestamp.ToString("dd/MM/yyyy"), m.Timestamp.ToString("dd/MM/yyyy"));
            Assert.AreEqual(fDummy.Timestamp.ToString("dd/MM/yyyy"), f.Timestamp.ToString("dd/MM/yyyy"));

            Assert.AreEqual(mDummy.Count, m.Count);
            Assert.AreEqual(fDummy.Count, f.Count);

            Assert.AreEqual(mDummy.Average, m.Average);
            Assert.AreEqual(fDummy.Average, f.Average);

            Assert.AreEqual(mDummy.Gain, m.Gain);
            Assert.AreEqual(fDummy.Gain, f.Gain);

            Assert.AreEqual(mDummy.Sigma, m.Sigma);
            Assert.AreEqual(fDummy.Sigma, f.Sigma);

            Assert.AreEqual(mDummy.Uniformity, m.Uniformity);
            Assert.AreEqual(fDummy.Uniformity, f.Uniformity);

        }

        [TestMethod]
        public void CreateArchiveData_MessageShort()
        {

            ArchiveItem mDummy = dummyData.Archive.Where(p => p.Sex == (byte)Bat2Library.SexE.SEX_UNDEFINED).FirstOrDefault();

            string shortMessage = SocketSendArchive.GenerateShortMessage(mDummy,
              dummyData.Configuration.VersionInfo.SerialNumber, (Bat2Library.WeightUnitsE)dummyData.Configuration.WeightUnits.Units);
            Bat2DeviceData data = ParseMessage(shortMessage, (Bat2Library.WeightUnitsE)dummyData.Configuration.WeightUnits.Units);

            Assert.IsNotNull(data);
            Assert.AreEqual(dummyData.Configuration.VersionInfo.SerialNumber, data.Configuration.VersionInfo.SerialNumber);
            ArchiveItem m = data.Archive.Where(p => p.Sex == (byte)Bat2Library.SexE.SEX_UNDEFINED).FirstOrDefault();

            Assert.IsNotNull(m);       
            Assert.AreEqual(mDummy.Day, m.Day);
            Assert.AreEqual(mDummy.Timestamp.ToString("dd/MM/yyyy"), m.Timestamp.ToString("dd/MM/yyyy"));
            Assert.AreEqual(mDummy.Count, m.Count);
            Assert.AreEqual(mDummy.Average, m.Average);
            Assert.AreEqual(mDummy.Gain, m.Gain);
            Assert.AreEqual(mDummy.Sigma, m.Sigma);
            Assert.AreEqual(mDummy.Uniformity, m.Uniformity);

        }

        private Bat2DeviceData ParseMessage(string message, Bat2Library.WeightUnitsE unit) 
        {           
            List<string> list = message.ToLower().Split(' ').ToList();
            bool sexdiff = false;
            Bat2DeviceData  data = new Bat2DeviceData();

            ArchiveItem m = new ArchiveItem() { Sex = (byte)Bat2Library.SexE.SEX_UNDEFINED };
            ArchiveItem f = new ArchiveItem();
            if(list.Contains("females:"))
            {
                m.Sex = (byte)Bat2Library.SexE.SEX_MALE;
                f.Sex = (byte)Bat2Library.SexE.SEX_FEMALE;
                sexdiff = true;
            }
            

            bool isMale = true;

            for (int i = 0; i < list.Count; i++) 
            {
                switch (list[i]) 
                {
                    case "scale":
                        data.Configuration.VersionInfo.SerialNumber = uint.Parse(list[i + 1], NumberStyles.HexNumber);
                        break;
                    case "day":
                        m.Day = short.Parse(list[i + 1]);
                        m.Timestamp = DateTime.Parse(list[i + 2]);
                        f.Day = short.Parse(list[i + 1]);
                        f.Timestamp = DateTime.Parse(list[i + 2]);
                        break;
                    case "females:":
                        isMale = false;
                        break;
                    case "males:":
                        isMale = true;
                        break;
                    case "cnt":
                        int cnt = int.Parse(list[i+1]);
                        if (isMale) { m.Count = cnt; } else { f.Count = cnt; } 
                        break;
                    case "avg":
                        int avg = ConvertWeight(double.Parse(list[i+1]),unit);
                        if (isMale) { m.Average = avg; } else { f.Average = avg; } 
                        break;
                    case "gain":
                         int gain = ConvertWeight(double.Parse(list[i+1]),unit);
                        if (isMale) { m.Gain = gain; } else { f.Gain = gain; } 
                        break;
                    case "sig":
                          int sig = ConvertWeight(double.Parse(list[i+1]),unit);
                        if (isMale) { m.Sigma = sig; } else { f.Sigma = sig; } 
                        break;
                    case "cv":
                        break;
                    case "uni":
                        short uni = short.Parse(list[i+1]);
                        if (isMale) { m.Uniformity = uni; } else { f.Uniformity = uni; } 
                        break;
                    default:
                        break;
                }     
            }
            if (sexdiff) 
            {
                data.Archive = new List<ArchiveItem>() { m, f };
            }
            else
            {
                data.Archive = new List<ArchiveItem>() { m };
            }

            return data;
        }

        private int ConvertWeight(double number, Bat2Library.WeightUnitsE unit)
        {
            int cNumber = 0;
            switch (unit) 
            {
                case WeightUnitsE.WEIGHT_UNITS_G:
                    cNumber = (int)(number * 10);
                    break;
                case WeightUnitsE.WEIGHT_UNITS_KG:
                    cNumber = (int)(number * 10 * 1000);
                    break;
                case WeightUnitsE.WEIGHT_UNITS_LB:
                    cNumber = (int)((number / 2.204624420183777) * 10 * 1000);
                    break;
            }
            return cNumber;
        }

    }

   


    public class ServiceTestBase 
    {
        public const int PORT_NO = 8800;
        public const string SERVER_IP = "127.0.0.1";

        public ServiceHost eventsHost;
        internal Bat2CommandServiceSample commandHost;
        internal Bat2EventHandler eventHandler;
        public ChannelFactory<IBat2CommandContract> commandContract;
        internal Bat2EventType expectedEventType;
        public Bat2DeviceData actualData;
        public ManualResetEvent waitHandle;
        public bool replyRecived;
        public Bat2DeviceData dummyData;

        public void InitTests() 
        {
            waitHandle = new ManualResetEvent(false);
            eventHandler = new Bat2EventHandler();
            eventHandler.EventReceived += EventRecived;
            commandHost = new Bat2CommandServiceSample();
            Messenger.Default.Register<Bat2DataWriteEventsContractMessage>(
                this, msg => msg.ActionToInvoke(eventHandler));
            Messenger.Default.Register<Bat2DataReadEventsContractMessage>(
               this, msg => msg.ActionToInvoke(eventHandler));
            Messenger.Default.Register<Bat2ConnectionEventsContractMessage>(
               this, msg => msg.ActionToInvoke(eventHandler));
            Messenger.Default.Register<Bat2ActionEventsContractMessage>(
               this, msg => msg.ActionToInvoke(eventHandler));
            Messenger.Default.Register<Bat2ErrorEventsContractMessage>(
              this, msg => msg.ActionToInvoke(eventHandler));
            commandContract = new ChannelFactory<IBat2CommandContract>("IBat2CommandContract_netPipe");
            eventsHost = new ServiceHost(typeof(Bat2EventsContractService));
            eventsHost.Open();
            commandHost.Start();
            expectedEventType = Bat2EventType.DeviceConnected;
            waitHandle.WaitOne(2000);
            dummyData = GetDummyData();
            //Thread.Sleep(3000);
        }



        public void CleanUpTests() 
        {
            eventsHost.Close();
            commandHost.Stop();
        }

        #region Private helpers

        internal void EventRecived(object sender, EventReceivedArgs e)
        {
            Assert.AreEqual(expectedEventType, e.Operation);
            actualData = e.DeviceData;
            switch (e.Operation)
            {
                case Bat2EventType.WeighingPlanned:
                case Bat2EventType.TimeSet:
                case Bat2EventType.TimeGet:
                case Bat2EventType.ErrorRised:
                    Assert.AreEqual(1, e.Args.Length);
                    break;
                default:
                    Assert.AreEqual(0, e.Args.Length);
                    break;
            }

            replyRecived = true;
            waitHandle.Set();
            waitHandle.Reset();
        }

        public void LoadConnectedDevices(IBat2CommandContract contract)
        {
            if (actualData != null)
            {
                return;
            }
            Assert.IsTrue(commandHost.IsOpened);

            expectedEventType = Bat2EventType.DeviceConnected;
            replyRecived = false;
            contract.ConnectedDevicesGet();
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        public void LoadContextData(IBat2CommandContract contract, Bat2DeviceData data)
        {
            if (actualData.Context != null)
            {
                return;
            }

            expectedEventType = Bat2EventType.DataRead;
            replyRecived = false;
            contract.ReadContext(data);
            waitHandle.WaitOne(2000);
            Assert.IsTrue(replyRecived);
        }

        public Bat2DeviceData GetDummyData()
        {
            Bat2DeviceData dummyData = new Bat2DeviceData();
            dummyData.Configuration = new Configuration()
            {
                EthernetOptions = new EthernetOptions() { Address = SERVER_IP, Port = PORT_NO, Enabled = true },
                VersionInfo = new VersionInfo() { SerialNumber = 123456789 },
                WeightUnits = new WeightUnits() { Units = (byte)Bat2Library.WeightUnitsE.WEIGHT_UNITS_G }
            };
            dummyData.Archive = new List<ArchiveItem>() 
            {
                new ArchiveItem(){ Day = 1, Timestamp = DateTime.Now, Count = 4, Average = 630, Gain = 230, Sigma = 530, Uniformity = 3, Sex = (byte)Bat2Library.SexE.SEX_FEMALE, HourFrom = 0, HourTo = 23},
                new ArchiveItem(){ Day = 1, Timestamp = DateTime.Now, Count = 17, Average = 730, Gain = 330, Sigma = 430, Uniformity = 4, Sex = (byte)Bat2Library.SexE.SEX_MALE, HourFrom = 0, HourTo = 23},
                new ArchiveItem(){ Day = 2, Timestamp = DateTime.Now, Count = 22, Average = 830, Gain = 230, Sigma = 630, Uniformity = 2, Sex = (byte)Bat2Library.SexE.SEX_UNDEFINED, HourFrom = 0, HourTo = 23},
                new ArchiveItem(){ Day = 2, Timestamp = DateTime.Now, Count = 22, Average = 830, Gain = 230, Sigma = 630, Uniformity = 2, Sex = (byte)Bat2Library.SexE.SEX_MALE, HourFrom = 0, HourTo = 1},
                new ArchiveItem(){ Day = 3, Timestamp = DateTime.Now, Count = 55, Average = 930, Gain = 430, Sigma = 730, Uniformity = 5, Sex = (byte)Bat2Library.SexE.SEX_MALE, HourFrom = 7, HourTo = 8},
                new ArchiveItem(){ Day = 3, Timestamp = DateTime.Now, Count = 55, Average = 930, Gain = 430, Sigma = 730, Uniformity = 5, Sex = (byte)Bat2Library.SexE.SEX_MALE, HourFrom = 0, HourTo = 23},
                new ArchiveItem(){ Day = 3, Timestamp = DateTime.Now, Count = 70, Average = 530, Gain = 530, Sigma = 830, Uniformity = 3, Sex = (byte)Bat2Library.SexE.SEX_FEMALE, HourFrom = 0, HourTo = 23}
            };
            return dummyData;
        }

        #endregion
    }
}