﻿using System.Threading;
using System.Windows;
using Bat2Tester.ModelViews.Interface;
using Desktop.Wpf.Presentation;
using GUI;

namespace Bat2Tester.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for Progress.xaml
   /// </summary>
   public partial class Progress : IProgress
   {
      private CancellationTokenSource token;
      private Window window;

      public Progress()
      {  
         InitializeComponent();
         //window = new Window();
      }

      private void Button_Click(object sender, RoutedEventArgs e)
      {
         token.Cancel(); 
      }

      public void SetText(string text)
      {
         LabelText.Content = text;
      }

      public bool? ShowDialog(IProgressBar progressBar, CancellationTokenSource tokenSource)
      {
         token = tokenSource;
         ContentControl.Content = progressBar as ProgressBar;
         Application.Current.MainWindow.Opacity = 0.5;
         window = new Window()
         {
            Owner = Application.Current.MainWindow,
            WindowStyle = WindowStyle.None,
            Content = this,
            ResizeMode = ResizeMode.NoResize,
            WindowStartupLocation = WindowStartupLocation.CenterOwner,
            Width = 250,
            Height = 100
         };

         var result = window.ShowDialog();
         Application.Current.MainWindow.Opacity = 1;
         return result;
      }

      public void Close()
      {
         window.Close();
      }
   }
}
