﻿using System.Windows;
using System.Windows.Controls;
using Bat2Tester.ModelViews.Aplications;

namespace Bat2Tester.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      public MainWindow()
      {
         InitializeComponent();
      }

      private void UIElement_OnIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
      {
         var control = (sender as Control);
         if (control == null)
         {  
            return;
         }
         control.Opacity = control.IsEnabled ? 1 : 0;
      }

      private void MenuItem_OnClick(object sender, RoutedEventArgs e)
      {
         var w = new HexToBinWindow
         {
            DataContext = new HexToBinViewModel(),
            Owner = this
         };
         w.ShowDialog();
      }
   }
}
