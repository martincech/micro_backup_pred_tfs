﻿namespace Bat2Tester.Settings
{
   public class Header
   {
      public const string HEADER_SN = "Serial number";
      public const string HEADER_MODIFICATION = "Modification";

      public const int SERIAL_NUMBER_INDEX = 0;
      public const int MODIFICATION_INDEX = 1;
   }
}
