#pragma once

#using "Connection.Manager.Native.dll" as_friend

namespace Connection
{
   namespace Routines
   {
      using namespace System::IO;
      using namespace Connection::Usb;

      public ref class ConfigLoadTest
      {

      public:
         ConfigLoadTest(HidDevice ^device);
         property HidDevice ^Device{ HidDevice ^get(){ return device; } }
         bool LoadConfig();

      private:
         HidDevice ^device;
      };
   }
}
