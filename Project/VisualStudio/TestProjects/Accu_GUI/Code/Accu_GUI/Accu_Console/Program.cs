﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accu_GUI;

namespace Accu_Console
{
   /// <summary>
   /// Test Console application for view all registers data.
   /// </summary>
   class Program
   {    

      static void Main(string[] args)
      {       
         ModelView mv = new ModelView();
         mv.UpdateData();        

         try
         {
            Console.WriteLine("Fuel Gauge\n----------------");
            Console.WriteLine("Temperature: {0}", mv.FGAccuTemperature);
            Console.WriteLine("\t{0:x} {1:x}", mv.FGAccuTemperatureRaw[0], mv.FGAccuTemperatureRaw[1]);
            
            Console.WriteLine("Voltage: {0}mA", mv.FGAccuVoltage);
            Console.WriteLine("\t{0:x} {1:x}", mv.FGAccuVoltageRaw[0], mv.FGAccuVoltageRaw[1]);
            
            Console.WriteLine("Current: {0}mA", mv.FGAccuCurrent);
            Console.WriteLine("\t{0:x} {1:x}", mv.FGAccuCurrentRaw[0], mv.FGAccuCurrentRaw[1]);


            Console.WriteLine("\nCharger\n----------------");
            Console.WriteLine("Charge Type: {0}\n", mv.ChType);
            Console.WriteLine("\t{0:x}",mv.ChRegisters[2]);

            Console.WriteLine("Charge current: {0}mA", mv.ChChargerCurrent);
            Console.WriteLine("\t{0:x}", mv.ChRegisters[1]);

            Console.WriteLine("Charge current limit: {0}mA", mv.ChCurrentLimit);
            Console.WriteLine("\t{0:x}", mv.ChRegisters[3]);        
         }
         catch (Exception e)
         {
            Console.WriteLine(e.Message);
         }
         finally
         {
            mv.Close();
         }


         Console.ReadKey();
      }
   }
}
