﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Connection.Usb;

namespace Accu_GUI
{
   public class FuelGauge
   {
      private Max17047 max17047;
      public byte[] RawData { get; private set; }
      

      // register address
      enum TMax17047Address
      {
         Temperature = 0x08,
         Current = 0x0a,
         Voltage = 0x09
      };

      public struct TUnits
      {
         public const double Vcell = 0.625;
         public const double TemperatureLSB = 0.0039;
         public const double Current = 0.0015625;
         public const double RSense = 20;
      }

      /// <summary>
      /// FuelGauge class initialize.
      /// </summary>
      /// <param name="vid">vendor id</param>
      /// <param name="pid">product id</param>
      public FuelGauge(HidDevice usb)
      {
         max17047 = new Max17047(usb);
         RawData = new byte[2];
      }

      /// <summary>
      /// Convert bit array to int.
      /// </summary>
      /// <param name="bitArray">converting bit array</param>
      /// <returns>integer from bit array</returns>
      private int GetIntFromBitArray(BitArray bitArray)
      {
         if (bitArray.Length > 32)
            throw new ArgumentException("Argument length havn't to bigger then 32 bits");

         int[] array = new int[1];
         bitArray.CopyTo(array, 0);
         return array[0];
      }

      /// <summary>
      /// Shift bit array to right
      /// </summary>
      /// <param name="bitArr">bit array</param>
      /// <param name="shift">size of shift</param>
      /// <returns>shifted bit array</returns>
      private BitArray ShiftBitArray(BitArray bitArr, int shift)
      {
         if (shift >= bitArr.Count)
            throw new ArgumentException("Argument shift is greater then bit array");
       
         for (int i = shift; i < bitArr.Count; i++)
         {
            bitArr[i - shift] = bitArr[i];
         }

         //clear higher bits
         for (int i = 1; i <= shift; i++)
         {
            bitArr[bitArr.Count - i] = false;
         }

         return bitArr;
      }


      /// <summary>
      /// Get voltage from Accumulator. Unit: mV
      /// </summary>
      /// <returns>voltage</returns>
      public double GetAccuVoltage()
      {
         max17047.RegisterRead((int)TMax17047Address.Voltage);       

         byte[] array = new byte[2] { max17047.RawData[0], max17047.RawData[1] };
         //little endian
         Array.Reverse(array);
         BitArray bitArray = ShiftBitArray(new BitArray(array), 3);

         int result = GetIntFromBitArray(bitArray);
         double voltage = (double)TUnits.Vcell * result; //unit: mV

         RawData = max17047.RawData;
         return voltage;
      }

  
      /// <summary>
      /// Get Accumulator current. Unit: mA
      /// </summary>
      /// <returns>current</returns>
      public double GetAccuCurrent()
      {
         max17047.RegisterRead((int)TMax17047Address.Current);       

         byte[] array = new byte[2] { max17047.RawData[0], max17047.RawData[1] };
         //little endian
         Array.Reverse(array);
         BitArray bitArray = new BitArray(array);
         double current = GetIntFromBitArray(bitArray) * (double)TUnits.Current / (double)TUnits.RSense;

         RawData = max17047.RawData;
         return current;
      }


      /// <summary>
      /// Get Accu temperature. Unit: degree celsium
      /// </summary>
      /// <returns>temperature</returns>
      public double GetAccuTemperature()
      {
         max17047.RegisterRead((int)TMax17047Address.Temperature);        

         double msb = Convert.ToInt32(max17047.RawData[0]);
         double lsb = Convert.ToInt32(max17047.RawData[1]) * (double)TUnits.TemperatureLSB;

         RawData = max17047.RawData;
         return msb + lsb;
      }

   }
}
