﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connection.Usb;

namespace Accu_GUI
{  

   public class Rpc
   {
      private HidDevice usb;                             // HID device
      private static int answer = 0x80;                  // answer code
      private int actualCommmand;                        // actual command

      public event EventHandler<byte[]> SuccesfullyReaded;

      /// <summary>
      /// Rpc class initialize.
      /// </summary>
      /// <param name="usb">HID device</param>   
      public Rpc(HidDevice usb)
      {
         this.usb = usb;                 
         usb.DataArrived += usb_DataArrived;
      }
    
     
      /// <summary>
      /// Test answer packet.
      /// </summary>
      /// <param name="cmd">type of command</param>
      public void CheckAnswer(int cmd, byte cmdType, byte success)
      {
         int command = Convert.ToInt32(cmdType);
         //System.Windows.MessageBox.Show(String.Format("test answer: {0}", Response[0]));     

         if (command != (cmd | answer))
         {
            throw new Exception("Wrong answer data format");
         }

         if (!Convert.ToBoolean(success))
         {
            throw new Exception("Answer data is not correct");
         }
      }

      /// <summary>
      /// Request to remote device to read data.
      /// </summary>
      /// <param name="module">required module</param>
      /// <param name="address">data address / register number</param>
      public void Read(ModelView.TModuleType module, int address)
      {
         PacketAssembly packetAssembly = new PacketAssembly(module);
         byte[] packet = packetAssembly.CreatePacket(address);

         if (module == ModelView.TModuleType.Max17047)
         {
            actualCommmand = (int)ModelView.TEAccuRpcCmd.CMD_MAX17047_READ;
         }
         else if (module == ModelView.TModuleType.Bq2425x)
         {
            actualCommmand = (int)ModelView.TEAccuRpcCmd.CMD_BQ2425X_READ;
         }

         usb.Write(packet, 0, packet.Length);

         //test console output
         Console.WriteLine("write packet:");
         for (int i = 0; i < packet.Length; i++)
            Console.Write("{0:x} ", packet[i]);

         Console.WriteLine("\n");        
      }

      /// <summary>
      /// Write data to remote device.
      /// </summary>
      /// <param name="module">required module</param>
      /// <param name="address">data address / register number</param>
      /// <param name="data">data</param>
      public void Write(ModelView.TModuleType module, int address, byte[] data)
      {
         PacketAssembly packetAssembly = new PacketAssembly(module);
         byte[] packet = packetAssembly.CreatePacket(address, data);

         usb.Write(packet, 0, packet.Length);
      }

      /// <summary>
      /// Event for read data from device.
      /// </summary>    
      void usb_DataArrived(object sender, byte[] e)
      {               
         //Response.Clear();

         /*int receiveDataSize = 0;
         if (actualCommmand == (int)ModelView.TEAccuRpcCmd.CMD_MAX17047_READ)
         {
            receiveDataSize = 4;
         }
         else if (actualCommmand == (int)ModelView.TEAccuRpcCmd.CMD_BQ2425X_READ)
         {
            receiveDataSize = 9;
         }*/

         //byte[] receiveData = new byte[64]; //64 | receiveDataSize
         byte[] receiveData = e;

         //TODO: check count of load data
         //Data are in event parameter e
         //int num = usb.Read(receiveData, 0, receiveData.Length);  //num - size of read data
         int num = e.Length;

         //delete
         //Response = receiveData.ToList();
         
         
         // if data aren't a null, process answer
         if (num >= 4 &&
            (receiveData[0] != 0 && receiveData[1] != 0 && receiveData[2] != 0 && receiveData[3] != 0))
         {
            //check answer
            CheckAnswer(actualCommmand, e[0], e[1]);   
            //System.Windows.MessageBox.Show(String.Format("test: {0}\nContent: {1}", num, BitConverter.ToString(receiveData)));
            System.Console.WriteLine("test: {0}\nContent: {1}", num, BitConverter.ToString(receiveData));

            InvokeEvent(receiveData);
         }
      }

      private void InvokeEvent(byte[] data)
      {         
         if(SuccesfullyReaded != null)
         {           
            SuccesfullyReaded(this, data);
         }
      }
      
   }
}
