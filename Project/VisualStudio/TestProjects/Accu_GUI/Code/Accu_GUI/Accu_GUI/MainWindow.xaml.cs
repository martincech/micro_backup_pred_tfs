﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Connection.Usb;

namespace Accu_GUI
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      //private HidDevice usb;
      private FuelGauge fuelGauge;
      private ModelView mv;
      private const int vid = 0xffff;
      private const int pid = 0xffff;

      public MainWindow()
      {
         InitializeComponent();    

         try
         {
            mv = new ModelView();
            mv.UpdateData();
            MessageBox.Show(String.Format("Temperature: {0}, Voltage {1}, Current {2}", mv.FGAccuTemperature, mv.FGAccuVoltage, mv.FGAccuCurrent));

            MessageBox.Show(String.Format("Charger type: {0}, Charge current: {1}mA, Charge current limit: {2}mA", mv.ChType, mv.ChChargerCurrent, mv.ChCurrentLimit));
         }
         catch (Exception e)
         {
            MessageBox.Show(e.Message);
         }
         finally
         {
            mv.Close();
         }
      }

   }
}
