﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Connection.Usb;

namespace Accu_GUI
{
   public class Max17047
   {                 
      private Rpc rpc;                               // device service
      public byte[] RawData { get; private set; }    // raw data from answer


      /// <summary>
      /// Class initialize.
      /// </summary>
      /// <param name="vid">vendor id</param>
      /// <param name="pid">product id</param>
      public Max17047(HidDevice usb)
      {                                   
         rpc = new Rpc(usb);
         RawData = new byte[2];
         rpc.SuccesfullyReaded += rpc_SuccesfullyReaded;
      }

      void rpc_SuccesfullyReaded(object sender, byte[] e)
      {
         //throw new NotImplementedException();
         Console.WriteLine("Read Max17047!!!");
      }    


      /// <summary>
      /// Read from Max17047 register. Answer save to RawData
      /// </summary>
      /// <param name="address">register address</param>
      public void RegisterRead(int address)
      {        
         rpc.Read(ModelView.TModuleType.Max17047, address);         
         Thread.Sleep(1500);
        
         //data
         Array.Clear(RawData, 0, RawData.Length);
         /*RawData[0] = rpc.Response[2];
         RawData[1] = rpc.Response[3];*/
      }

      /// <summary>
      /// Write to Max17047 register.
      /// </summary>
      /// <param name="address">register address</param>
      /// <param name="data">writing data</param>
      public void RegisterWrite(int address, byte[] data)
      {         
         rpc.Write(ModelView.TModuleType.Max17047, address, data);
      }
   }
}
