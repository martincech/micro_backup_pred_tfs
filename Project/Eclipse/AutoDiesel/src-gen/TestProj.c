
#include <stdlib.h>
#include <string.h>
#include "sc_types.h"
#include "TestProj.h"
#include "TestProjRequired.h"
/*! \file Implementation of the state machine 'TestProj'
*/

// prototypes of all internal functions

static sc_boolean testProj_check_lr0(TestProj* handle);
static void testProj_effect_lr0(TestProj* handle);
static void testProj_enact_SequenceImpl(TestProj* handle);
static void testProj_exact_SequenceImpl(TestProj* handle);
static void testProj_react_Start_logic_Start_logic_r1_Init(TestProj* handle);
static void testProj_react_Start_logic_Start_logic_r1_User_start_requested(TestProj* handle);
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart(TestProj* handle);
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing(TestProj* handle);
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start(TestProj* handle);
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm(TestProj* handle);
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start(TestProj* handle);
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Starting(TestProj* handle);
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Running_check(TestProj* handle);
static void testProj_react_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics(TestProj* handle);
static void testProj_react_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown(TestProj* handle);
static void testProj_react_Motor_control_Motor_priority_logic(TestProj* handle);
static void testProj_react_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run(TestProj* handle);
static void testProj_react_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot(TestProj* handle);
static void testProj_react_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run(TestProj* handle);
static void testProj_react_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro(TestProj* handle);
static void testProj_react_Motor_control_Cooling_logic_r1_Coutner(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE(TestProj* handle);
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE(TestProj* handle);
static void testProj_clearInEvents(TestProj* handle);
static void testProj_clearOutEvents(TestProj* handle);


void testProj_init(TestProj* handle)
{
	int i;

	for (i = 0; i < TESTPROJ_MAX_ORTHOGONAL_STATES; ++i)
		handle->stateConfVector[i] = TestProj_last_state;
	
	
	handle->stateConfVectorPosition = 0;

	testProj_clearInEvents(handle);
	testProj_clearOutEvents(handle);

	/* Default init sequence for statechart TestProj */
	handle->internal.ActiveMotor = 0;
	handle->internal.DieselStartTries = 0;
	handle->internal.StartedBy = 0;
	handle->internal.RunMotor = bool_false;
	handle->internal.CoolPause = 0;
	handle->internal.CoolPauseLength = 600;
	handle->internal.StopDiesel = bool_false;
	handle->internal.EXT_ON = bool_false;
	handle->internal.TEMP_HI = bool_false;
	handle->internal.OIL_LOW = bool_false;
	handle->internal.AIR_ERR = bool_false;
	handle->internal.AUTOTYPE = "";

}

void testProj_enter(TestProj* handle)
{
	/* Default enter sequence for statechart TestProj */
	testProj_enact_SequenceImpl(handle);
	/* 'default' enter sequence for region Start logic */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state Start logic */
	/* 'default' enter sequence for region r1 */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state Init */
	/* Entry action for state 'Init'. */
	handle->internal.Init_raised = bool_true;
	handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
	handle->stateConfVectorPosition = 0;
	/* 'default' enter sequence for region Diesel events to internal events */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state Event watch */
	/* 'default' enter sequence for region Outputs */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state Output watch */
	/* 'default' enter sequence for region r1 */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state Start/Stop watch */
	/* 'default' enter sequence for region STOP Watch */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state STOP */
	handle->stateConfVector[2] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP;
	handle->stateConfVectorPosition = 2;
	/* 'default' enter sequence for region START watch */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state START */
	handle->stateConfVector[3] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START;
	handle->stateConfVectorPosition = 3;
	/* 'default' enter sequence for region r2 */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state RUN/GLOW watch */
	/* 'default' enter sequence for region RUN Watch */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state RUN */
	handle->stateConfVector[4] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN;
	handle->stateConfVectorPosition = 4;
	/* 'default' enter sequence for region GLOW watch */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state GLOW */
	handle->stateConfVector[5] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW;
	handle->stateConfVectorPosition = 5;
	/* 'default' enter sequence for region Inputs */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state Input watch */
	/* 'default' enter sequence for region Diesel */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state Motor signals watch */
	/* 'default' enter sequence for region EXT Watch */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state EXT_FALSE */
	/* Entry action for state 'EXT_FALSE'. */
	handle->internal.EXT_ON = bool_false;
	handle->stateConfVector[6] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE;
	handle->stateConfVectorPosition = 6;
	/* 'default' enter sequence for region AIR Watch */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state AIR_FALSE */
	/* Entry action for state 'AIR_FALSE'. */
	handle->internal.AIR_ERR = bool_false;
	handle->stateConfVector[7] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE;
	handle->stateConfVectorPosition = 7;
	/* 'default' enter sequence for region TEMP Watch */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state TEMP_FALSE */
	/* Entry action for state 'TEMP_FALSE'. */
	handle->internal.TEMP_HI = bool_false;
	handle->stateConfVector[8] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE;
	handle->stateConfVectorPosition = 8;
	/* 'default' enter sequence for region OIL Watch */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state OIL_TRUE */
	/* Entry action for state 'OIL_TRUE'. */
	handle->internal.OIL_LOW = bool_true;
	handle->stateConfVector[9] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE;
	handle->stateConfVectorPosition = 9;
	/* 'default' enter sequence for region Keyboard */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state Keyboard watch */
	/* 'default' enter sequence for region AutoType solder Switch Watch */
	/* Default react sequence for initial entry  */
	/* 'default' enter sequence for state SWITCH_AUTOTYPE_TRUE */
	/* Entry action for state 'SWITCH_AUTOTYPE_TRUE'. */
	handle->internal.AUTOTYPE = "VAN";
	handle->stateConfVector[10] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE;
	handle->stateConfVectorPosition = 10;
}

void testProj_exit(TestProj* handle)
{
	/* Default exit sequence for statechart TestProj */
	/* Default exit sequence for region Start logic */
	/* Handle exit of all possible states (of Start logic) at position 0... */
	switch(handle->stateConfVector[ 0 ]) {
		case TestProj_Start_logic_Start_logic_r1_Init : {
			/* Default exit sequence for state Init */
			handle->stateConfVector[0] = TestProj_last_state;
			handle->stateConfVectorPosition = 0;
			break;
		}
		case TestProj_Start_logic_Start_logic_r1_User_start_requested : {
			/* Default exit sequence for state User start requested */
			handle->stateConfVector[0] = TestProj_last_state;
			handle->stateConfVectorPosition = 0;
			break;
		}
		default: break;
	}
	/* Default exit sequence for region Motor control */
	/* Handle exit of all possible states (of Motor control) at position 1... */
	switch(handle->stateConfVector[ 1 ]) {
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
			/* Default exit sequence for state Restart */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Restart'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
			handle->internal.DieselStartTries -= 1;
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
			/* Default exit sequence for state Glowing */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Glowing'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
			/* Default exit sequence for state Diesel 2 start */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Diesel 2 start'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
			/* Default exit sequence for state Alarm */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Alarm'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
			/* Default exit sequence for state Diesel1 start */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Diesel1 start'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
			/* Default exit sequence for state Starting */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Starting'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
			handle->internal.START_value = bool_true;
			handle->internal.START_raised = bool_true;
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
			/* Default exit sequence for state Running check */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			break;
		}
		case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics : {
			/* Default exit sequence for state Turn off electronics */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			break;
		}
		case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown : {
			/* Default exit sequence for state Shutdown */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Shutdown'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown_tev0_raised) );		
			break;
		}
		case TestProj_Motor_control_Motor_priority_logic : {
			/* Default exit sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			break;
		}
		case TestProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run : {
			/* Default exit sequence for state Elmot wait run */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Elmot wait run'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run_tev0_raised) );		
			break;
		}
		case TestProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot : {
			/* Default exit sequence for state Shut elmot */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Shut elmot'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot_tev0_raised) );		
			break;
		}
		case TestProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run : {
			/* Default exit sequence for state hydro wait run */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'hydro wait run'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run_tev0_raised) );		
			break;
		}
		case TestProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro : {
			/* Default exit sequence for state Shut hydro */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Shut hydro'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro_tev0_raised) );		
			break;
		}
		case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
			/* Default exit sequence for state Coutner */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Coutner'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
			break;
		}
		default: break;
	}
	/* Default exit sequence for region Diesel events to internal events */
	/* Handle exit of all possible states (of Diesel events to internal events) at position 2... */
	switch(handle->stateConfVector[ 2 ]) {
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP : {
			/* Default exit sequence for state STOP */
			handle->stateConfVector[2] = TestProj_last_state;
			handle->stateConfVectorPosition = 2;
			break;
		}
		default: break;
	}
	/* Handle exit of all possible states (of Diesel events to internal events) at position 3... */
	switch(handle->stateConfVector[ 3 ]) {
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START : {
			/* Default exit sequence for state START */
			handle->stateConfVector[3] = TestProj_last_state;
			handle->stateConfVectorPosition = 3;
			break;
		}
		default: break;
	}
	/* Handle exit of all possible states (of Diesel events to internal events) at position 4... */
	switch(handle->stateConfVector[ 4 ]) {
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN : {
			/* Default exit sequence for state RUN */
			handle->stateConfVector[4] = TestProj_last_state;
			handle->stateConfVectorPosition = 4;
			break;
		}
		default: break;
	}
	/* Handle exit of all possible states (of Diesel events to internal events) at position 5... */
	switch(handle->stateConfVector[ 5 ]) {
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW : {
			/* Default exit sequence for state GLOW */
			handle->stateConfVector[5] = TestProj_last_state;
			handle->stateConfVectorPosition = 5;
			break;
		}
		default: break;
	}
	/* Handle exit of all possible states (of Diesel events to internal events) at position 6... */
	switch(handle->stateConfVector[ 6 ]) {
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE : {
			/* Default exit sequence for state EXT_TRUE */
			handle->stateConfVector[6] = TestProj_last_state;
			handle->stateConfVectorPosition = 6;
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE : {
			/* Default exit sequence for state EXT_FALSE */
			handle->stateConfVector[6] = TestProj_last_state;
			handle->stateConfVectorPosition = 6;
			break;
		}
		default: break;
	}
	/* Handle exit of all possible states (of Diesel events to internal events) at position 7... */
	switch(handle->stateConfVector[ 7 ]) {
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE : {
			/* Default exit sequence for state AIR_TRUE */
			handle->stateConfVector[7] = TestProj_last_state;
			handle->stateConfVectorPosition = 7;
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE : {
			/* Default exit sequence for state AIR_FALSE */
			handle->stateConfVector[7] = TestProj_last_state;
			handle->stateConfVectorPosition = 7;
			break;
		}
		default: break;
	}
	/* Handle exit of all possible states (of Diesel events to internal events) at position 8... */
	switch(handle->stateConfVector[ 8 ]) {
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE : {
			/* Default exit sequence for state TEMP_TRUE */
			handle->stateConfVector[8] = TestProj_last_state;
			handle->stateConfVectorPosition = 8;
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE : {
			/* Default exit sequence for state TEMP_FALSE */
			handle->stateConfVector[8] = TestProj_last_state;
			handle->stateConfVectorPosition = 8;
			break;
		}
		default: break;
	}
	/* Handle exit of all possible states (of Diesel events to internal events) at position 9... */
	switch(handle->stateConfVector[ 9 ]) {
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE : {
			/* Default exit sequence for state OIL_TRUE */
			handle->stateConfVector[9] = TestProj_last_state;
			handle->stateConfVectorPosition = 9;
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE : {
			/* Default exit sequence for state OIL_FALSE */
			handle->stateConfVector[9] = TestProj_last_state;
			handle->stateConfVectorPosition = 9;
			break;
		}
		default: break;
	}
	/* Handle exit of all possible states (of Diesel events to internal events) at position 10... */
	switch(handle->stateConfVector[ 10 ]) {
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE : {
			/* Default exit sequence for state SWITCH_AUTOTYPE_TRUE */
			handle->stateConfVector[10] = TestProj_last_state;
			handle->stateConfVectorPosition = 10;
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE : {
			/* Default exit sequence for state SWITCH_AUTOTYPE_FALSE */
			handle->stateConfVector[10] = TestProj_last_state;
			handle->stateConfVectorPosition = 10;
			break;
		}
		default: break;
	}
	testProj_exact_SequenceImpl(handle);
}

static void testProj_clearInEvents(TestProj* handle) {
	handle->ifaceDiesel1.EXT_ON_raised = bool_false;
	handle->ifaceDiesel1.TEMP_HI_raised = bool_false;
	handle->ifaceDiesel1.OIL_LOW_raised = bool_false;
	handle->ifaceDiesel1.AIR_ERR_raised = bool_false;
	handle->ifaceDiesel2.EXT_ON_raised = bool_false;
	handle->ifaceDiesel2.TEMP_HI_raised = bool_false;
	handle->ifaceDiesel2.OIL_LOW_raised = bool_false;
	handle->ifaceDiesel2.AIR_ERR_raised = bool_false;
	handle->ifaceElmot.WALL_AVAILABLE_raised = bool_false;
	handle->ifaceElmot.RUNNING_raised = bool_false;
	handle->ifaceAxima.RUNNING_raised = bool_false;
	handle->ifaceHydro.HYDRO_AVAILABLE_raised = bool_false;
	handle->ifaceHydro.RUNNING_raised = bool_false;
	handle->ifaceKeyboard.START_STOP_HIGH_raised = bool_false;
	handle->ifaceKeyboard.START_STOP_LOW_raised = bool_false;
	handle->ifaceKeyboard.SWITCH_DIESEL_raised = bool_false;
	handle->ifaceKeyboard.SWITCH_HYDRO_raised = bool_false;
	handle->ifaceKeyboard.SWITCH_ELMOT_raised = bool_false;
	handle->ifaceKeyboard.SWITCH_AUTOTYPE_raised = bool_false;
	handle->ifaceCooling.COOL_TMC_raised = bool_false;
	handle->ifaceVentilation.ON_raised = bool_false;
	handle->ifaceVentilation.U_LOW_raised = bool_false;
	handle->ifaceVentilation.BATTERY_FULL_raised = bool_false;
	handle->internal.RUN_raised = bool_false; 
	handle->internal.START_raised = bool_false; 
	handle->internal.GLOW_raised = bool_false; 
	handle->internal.STOP_raised = bool_false; 
	handle->internal.Init_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro_tev0_raised = bool_false; 
	handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised = bool_false; 
}

static void testProj_clearOutEvents(TestProj* handle) {
	handle->ifaceDiesel1.RUN_raised = bool_false;
	handle->ifaceDiesel1.START_raised = bool_false;
	handle->ifaceDiesel1.GLOW_raised = bool_false;
	handle->ifaceDiesel1.STOP_raised = bool_false;
	handle->ifaceDiesel2.RUN_raised = bool_false;
	handle->ifaceDiesel2.START_raised = bool_false;
	handle->ifaceDiesel2.GLOW_raised = bool_false;
	handle->ifaceDiesel2.STOP_raised = bool_false;
	handle->ifaceElmot.RUN_raised = bool_false;
	handle->ifaceHydro.RUN_raised = bool_false;
	handle->ifaceSensors.SIRENA_EN_raised = bool_false;
	handle->ifaceSensors.LED_GLOW_raised = bool_false;
	handle->ifaceSensors.LED_EXT_raised = bool_false;
	handle->ifaceSensors.LED_OIL_raised = bool_false;
	handle->ifaceSensors.LED_TEMP_raised = bool_false;
	handle->ifaceSensors.LED_AIR_raised = bool_false;
	handle->ifaceCooling.COOL_EN_raised = bool_false;
}

void testProj_runCycle(TestProj* handle) {
	
	testProj_clearOutEvents(handle);
	
	for (handle->stateConfVectorPosition = 0;
		handle->stateConfVectorPosition < TESTPROJ_MAX_ORTHOGONAL_STATES;
		handle->stateConfVectorPosition++) {
			
		switch (handle->stateConfVector[handle->stateConfVectorPosition]) {
		case TestProj_Start_logic_Start_logic_r1_Init : {
			testProj_react_Start_logic_Start_logic_r1_Init(handle);
			break;
		}
		case TestProj_Start_logic_Start_logic_r1_User_start_requested : {
			testProj_react_Start_logic_Start_logic_r1_User_start_requested(handle);
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
			testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart(handle);
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
			testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing(handle);
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
			testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start(handle);
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
			testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm(handle);
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
			testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start(handle);
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
			testProj_react_Motor_control_Diesel_start_sequence_inner_region_Starting(handle);
			break;
		}
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
			testProj_react_Motor_control_Diesel_start_sequence_inner_region_Running_check(handle);
			break;
		}
		case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics : {
			testProj_react_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics(handle);
			break;
		}
		case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown : {
			testProj_react_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown(handle);
			break;
		}
		case TestProj_Motor_control_Motor_priority_logic : {
			testProj_react_Motor_control_Motor_priority_logic(handle);
			break;
		}
		case TestProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run : {
			testProj_react_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run(handle);
			break;
		}
		case TestProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot : {
			testProj_react_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot(handle);
			break;
		}
		case TestProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run : {
			testProj_react_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run(handle);
			break;
		}
		case TestProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro : {
			testProj_react_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro(handle);
			break;
		}
		case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
			testProj_react_Motor_control_Cooling_logic_r1_Coutner(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE(handle);
			break;
		}
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE : {
			testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE(handle);
			break;
		}
		default:
			break;
		}
	}
	
	testProj_clearInEvents(handle);
}

void testProj_raiseTimeEvent(TestProj* handle, sc_eventid evid) {
	if ( ((intptr_t)evid) >= ((intptr_t)&(handle->timeEvents))
		&&  ((intptr_t)evid) < ((intptr_t)&(handle->timeEvents)) + sizeof(TestProjTimeEvents)) {
		*(sc_boolean*)evid = bool_true;
	}		
}

sc_boolean testProj_isActive(TestProj* handle, TestProjStates state) {
	switch (state) {
		case TestProj_Start_logic_Start_logic : 
			return (sc_boolean) (handle->stateConfVector[0] >= TestProj_Start_logic_Start_logic
				&& handle->stateConfVector[0] <= TestProj_Start_logic_Start_logic_r1_User_start_requested);
		case TestProj_Start_logic_Start_logic_r1_Init : 
			return (sc_boolean) (handle->stateConfVector[0] == TestProj_Start_logic_Start_logic_r1_Init
			);
		case TestProj_Start_logic_Start_logic_r1_User_start_requested : 
			return (sc_boolean) (handle->stateConfVector[0] == TestProj_Start_logic_Start_logic_r1_User_start_requested
			);
		case TestProj_Motor_control_Diesel_start_sequence : 
			return (sc_boolean) (handle->stateConfVector[1] >= TestProj_Motor_control_Diesel_start_sequence
				&& handle->stateConfVector[1] <= TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check);
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence : 
			return (sc_boolean) (handle->stateConfVector[1] >= TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence
				&& handle->stateConfVector[1] <= TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start);
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart
			);
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing
			);
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start
			);
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm
			);
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start
			);
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting
			);
		case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check
			);
		case TestProj_Motor_control_Diesel_shutdown_sequence : 
			return (sc_boolean) (handle->stateConfVector[1] >= TestProj_Motor_control_Diesel_shutdown_sequence
				&& handle->stateConfVector[1] <= TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown);
		case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics
			);
		case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown
			);
		case TestProj_Motor_control_Motor_priority_logic : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Motor_priority_logic
			);
		case TestProj_Motor_control_Elmot_start_sequence : 
			return (sc_boolean) (handle->stateConfVector[1] >= TestProj_Motor_control_Elmot_start_sequence
				&& handle->stateConfVector[1] <= TestProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot);
		case TestProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run
			);
		case TestProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot
			);
		case TestProj_Motor_control_Hydro_start_sequence : 
			return (sc_boolean) (handle->stateConfVector[1] >= TestProj_Motor_control_Hydro_start_sequence
				&& handle->stateConfVector[1] <= TestProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro);
		case TestProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run
			);
		case TestProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro
			);
		case TestProj_Motor_control_Cooling_logic : 
			return (sc_boolean) (handle->stateConfVector[1] >= TestProj_Motor_control_Cooling_logic
				&& handle->stateConfVector[1] <= TestProj_Motor_control_Cooling_logic_r1_Coutner);
		case TestProj_Motor_control_Cooling_logic_r1_Coutner : 
			return (sc_boolean) (handle->stateConfVector[1] == TestProj_Motor_control_Cooling_logic_r1_Coutner
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch : 
			return (sc_boolean) (handle->stateConfVector[2] >= TestProj_Diesel_events_to_internal_events_Event_watch
				&& handle->stateConfVector[2] <= TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch : 
			return (sc_boolean) (handle->stateConfVector[2] >= TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch
				&& handle->stateConfVector[2] <= TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch : 
			return (sc_boolean) (handle->stateConfVector[2] >= TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch
				&& handle->stateConfVector[2] <= TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP : 
			return (sc_boolean) (handle->stateConfVector[2] == TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START : 
			return (sc_boolean) (handle->stateConfVector[3] == TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch : 
			return (sc_boolean) (handle->stateConfVector[4] >= TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch
				&& handle->stateConfVector[4] <= TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN : 
			return (sc_boolean) (handle->stateConfVector[4] == TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW : 
			return (sc_boolean) (handle->stateConfVector[5] == TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch : 
			return (sc_boolean) (handle->stateConfVector[6] >= TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch
				&& handle->stateConfVector[6] <= TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch : 
			return (sc_boolean) (handle->stateConfVector[6] >= TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch
				&& handle->stateConfVector[6] <= TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE : 
			return (sc_boolean) (handle->stateConfVector[6] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE : 
			return (sc_boolean) (handle->stateConfVector[6] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE : 
			return (sc_boolean) (handle->stateConfVector[7] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE : 
			return (sc_boolean) (handle->stateConfVector[7] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE : 
			return (sc_boolean) (handle->stateConfVector[8] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE : 
			return (sc_boolean) (handle->stateConfVector[8] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE : 
			return (sc_boolean) (handle->stateConfVector[9] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE : 
			return (sc_boolean) (handle->stateConfVector[9] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch : 
			return (sc_boolean) (handle->stateConfVector[10] >= TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch
				&& handle->stateConfVector[10] <= TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE : 
			return (sc_boolean) (handle->stateConfVector[10] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE
			);
		case TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE : 
			return (sc_boolean) (handle->stateConfVector[10] == TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE
			);
		default: return bool_false;
	}
}

void testProjIfaceDiesel1_raise_eXT_ON(TestProj* handle) {
	handle->ifaceDiesel1.EXT_ON_raised = bool_true;
}
void testProjIfaceDiesel1_raise_tEMP_HI(TestProj* handle) {
	handle->ifaceDiesel1.TEMP_HI_raised = bool_true;
}
void testProjIfaceDiesel1_raise_oIL_LOW(TestProj* handle) {
	handle->ifaceDiesel1.OIL_LOW_raised = bool_true;
}
void testProjIfaceDiesel1_raise_aIR_ERR(TestProj* handle) {
	handle->ifaceDiesel1.AIR_ERR_raised = bool_true;
}

sc_boolean testProjIfaceDiesel1_israised_rUN(TestProj* handle) {
	return handle->ifaceDiesel1.RUN_raised;
}
sc_boolean testProjIfaceDiesel1_get_rUN_value(TestProj* handle) {
	return handle->ifaceDiesel1.RUN_value;
}
sc_boolean testProjIfaceDiesel1_israised_sTART(TestProj* handle) {
	return handle->ifaceDiesel1.START_raised;
}
sc_boolean testProjIfaceDiesel1_get_sTART_value(TestProj* handle) {
	return handle->ifaceDiesel1.START_value;
}
sc_boolean testProjIfaceDiesel1_israised_gLOW(TestProj* handle) {
	return handle->ifaceDiesel1.GLOW_raised;
}
sc_boolean testProjIfaceDiesel1_get_gLOW_value(TestProj* handle) {
	return handle->ifaceDiesel1.GLOW_value;
}
sc_boolean testProjIfaceDiesel1_israised_sTOP(TestProj* handle) {
	return handle->ifaceDiesel1.STOP_raised;
}
sc_boolean testProjIfaceDiesel1_get_sTOP_value(TestProj* handle) {
	return handle->ifaceDiesel1.STOP_value;
}

void testProjIfaceDiesel2_raise_eXT_ON(TestProj* handle) {
	handle->ifaceDiesel2.EXT_ON_raised = bool_true;
}
void testProjIfaceDiesel2_raise_tEMP_HI(TestProj* handle) {
	handle->ifaceDiesel2.TEMP_HI_raised = bool_true;
}
void testProjIfaceDiesel2_raise_oIL_LOW(TestProj* handle) {
	handle->ifaceDiesel2.OIL_LOW_raised = bool_true;
}
void testProjIfaceDiesel2_raise_aIR_ERR(TestProj* handle) {
	handle->ifaceDiesel2.AIR_ERR_raised = bool_true;
}

sc_boolean testProjIfaceDiesel2_israised_rUN(TestProj* handle) {
	return handle->ifaceDiesel2.RUN_raised;
}
sc_boolean testProjIfaceDiesel2_get_rUN_value(TestProj* handle) {
	return handle->ifaceDiesel2.RUN_value;
}
sc_boolean testProjIfaceDiesel2_israised_sTART(TestProj* handle) {
	return handle->ifaceDiesel2.START_raised;
}
sc_boolean testProjIfaceDiesel2_get_sTART_value(TestProj* handle) {
	return handle->ifaceDiesel2.START_value;
}
sc_boolean testProjIfaceDiesel2_israised_gLOW(TestProj* handle) {
	return handle->ifaceDiesel2.GLOW_raised;
}
sc_boolean testProjIfaceDiesel2_get_gLOW_value(TestProj* handle) {
	return handle->ifaceDiesel2.GLOW_value;
}
sc_boolean testProjIfaceDiesel2_israised_sTOP(TestProj* handle) {
	return handle->ifaceDiesel2.STOP_raised;
}
sc_boolean testProjIfaceDiesel2_get_sTOP_value(TestProj* handle) {
	return handle->ifaceDiesel2.STOP_value;
}

void testProjIfaceElmot_raise_wALL_AVAILABLE(TestProj* handle, sc_boolean value) {
	handle->ifaceElmot.WALL_AVAILABLE_value = value;
	handle->ifaceElmot.WALL_AVAILABLE_raised = bool_true;
}
void testProjIfaceElmot_raise_rUNNING(TestProj* handle, sc_boolean value) {
	handle->ifaceElmot.RUNNING_value = value;
	handle->ifaceElmot.RUNNING_raised = bool_true;
}

sc_boolean testProjIfaceElmot_israised_rUN(TestProj* handle) {
	return handle->ifaceElmot.RUN_raised;
}
sc_boolean testProjIfaceElmot_get_rUN_value(TestProj* handle) {
	return handle->ifaceElmot.RUN_value;
}

void testProjIfaceAxima_raise_rUNNING(TestProj* handle, sc_boolean value) {
	handle->ifaceAxima.RUNNING_value = value;
	handle->ifaceAxima.RUNNING_raised = bool_true;
}


void testProjIfaceHydro_raise_hYDRO_AVAILABLE(TestProj* handle, sc_boolean value) {
	handle->ifaceHydro.HYDRO_AVAILABLE_value = value;
	handle->ifaceHydro.HYDRO_AVAILABLE_raised = bool_true;
}
void testProjIfaceHydro_raise_rUNNING(TestProj* handle, sc_boolean value) {
	handle->ifaceHydro.RUNNING_value = value;
	handle->ifaceHydro.RUNNING_raised = bool_true;
}

sc_boolean testProjIfaceHydro_israised_rUN(TestProj* handle) {
	return handle->ifaceHydro.RUN_raised;
}
sc_boolean testProjIfaceHydro_get_rUN_value(TestProj* handle) {
	return handle->ifaceHydro.RUN_value;
}

void testProjIfaceKeyboard_raise_sTART_STOP_HIGH(TestProj* handle) {
	handle->ifaceKeyboard.START_STOP_HIGH_raised = bool_true;
}
void testProjIfaceKeyboard_raise_sTART_STOP_LOW(TestProj* handle) {
	handle->ifaceKeyboard.START_STOP_LOW_raised = bool_true;
}
void testProjIfaceKeyboard_raise_sWITCH_DIESEL(TestProj* handle, sc_boolean value) {
	handle->ifaceKeyboard.SWITCH_DIESEL_value = value;
	handle->ifaceKeyboard.SWITCH_DIESEL_raised = bool_true;
}
void testProjIfaceKeyboard_raise_sWITCH_HYDRO(TestProj* handle, sc_boolean value) {
	handle->ifaceKeyboard.SWITCH_HYDRO_value = value;
	handle->ifaceKeyboard.SWITCH_HYDRO_raised = bool_true;
}
void testProjIfaceKeyboard_raise_sWITCH_ELMOT(TestProj* handle, sc_boolean value) {
	handle->ifaceKeyboard.SWITCH_ELMOT_value = value;
	handle->ifaceKeyboard.SWITCH_ELMOT_raised = bool_true;
}
void testProjIfaceKeyboard_raise_sWITCH_AUTOTYPE(TestProj* handle, sc_boolean value) {
	handle->ifaceKeyboard.SWITCH_AUTOTYPE_value = value;
	handle->ifaceKeyboard.SWITCH_AUTOTYPE_raised = bool_true;
}



sc_boolean testProjIfaceSensors_israised_sIRENA_EN(TestProj* handle) {
	return handle->ifaceSensors.SIRENA_EN_raised;
}
sc_boolean testProjIfaceSensors_get_sIRENA_EN_value(TestProj* handle) {
	return handle->ifaceSensors.SIRENA_EN_value;
}
sc_boolean testProjIfaceSensors_israised_lED_GLOW(TestProj* handle) {
	return handle->ifaceSensors.LED_GLOW_raised;
}
sc_boolean testProjIfaceSensors_get_lED_GLOW_value(TestProj* handle) {
	return handle->ifaceSensors.LED_GLOW_value;
}
sc_boolean testProjIfaceSensors_israised_lED_EXT(TestProj* handle) {
	return handle->ifaceSensors.LED_EXT_raised;
}
sc_boolean testProjIfaceSensors_get_lED_EXT_value(TestProj* handle) {
	return handle->ifaceSensors.LED_EXT_value;
}
sc_boolean testProjIfaceSensors_israised_lED_OIL(TestProj* handle) {
	return handle->ifaceSensors.LED_OIL_raised;
}
sc_boolean testProjIfaceSensors_get_lED_OIL_value(TestProj* handle) {
	return handle->ifaceSensors.LED_OIL_value;
}
sc_boolean testProjIfaceSensors_israised_lED_TEMP(TestProj* handle) {
	return handle->ifaceSensors.LED_TEMP_raised;
}
sc_boolean testProjIfaceSensors_get_lED_TEMP_value(TestProj* handle) {
	return handle->ifaceSensors.LED_TEMP_value;
}
sc_boolean testProjIfaceSensors_israised_lED_AIR(TestProj* handle) {
	return handle->ifaceSensors.LED_AIR_raised;
}
sc_boolean testProjIfaceSensors_get_lED_AIR_value(TestProj* handle) {
	return handle->ifaceSensors.LED_AIR_value;
}

void testProjIfaceCooling_raise_cOOL_TMC(TestProj* handle, sc_boolean value) {
	handle->ifaceCooling.COOL_TMC_value = value;
	handle->ifaceCooling.COOL_TMC_raised = bool_true;
}

sc_boolean testProjIfaceCooling_israised_cOOL_EN(TestProj* handle) {
	return handle->ifaceCooling.COOL_EN_raised;
}
sc_boolean testProjIfaceCooling_get_cOOL_EN_value(TestProj* handle) {
	return handle->ifaceCooling.COOL_EN_value;
}

void testProjIfaceVentilation_raise_oN(TestProj* handle, sc_boolean value) {
	handle->ifaceVentilation.ON_value = value;
	handle->ifaceVentilation.ON_raised = bool_true;
}
void testProjIfaceVentilation_raise_u_LOW(TestProj* handle, sc_boolean value) {
	handle->ifaceVentilation.U_LOW_value = value;
	handle->ifaceVentilation.U_LOW_raised = bool_true;
}
void testProjIfaceVentilation_raise_bATTERY_FULL(TestProj* handle, sc_boolean value) {
	handle->ifaceVentilation.BATTERY_FULL_value = value;
	handle->ifaceVentilation.BATTERY_FULL_raised = bool_true;
}



// implementations of all internal functions

static sc_boolean testProj_check_lr0(TestProj* handle) {
	return handle->internal.Init_raised;
}

static void testProj_effect_lr0(TestProj* handle) {
	handle->internal.RUN_value = bool_false;
	handle->internal.RUN_raised = bool_true;
	handle->internal.START_value = bool_false;
	handle->internal.START_raised = bool_true;
	handle->internal.STOP_value = bool_false;
	handle->internal.STOP_raised = bool_true;
	handle->internal.GLOW_value = bool_false;
	handle->internal.GLOW_raised = bool_true;
	handle->ifaceDiesel1.RUN_value = bool_false;
	handle->ifaceDiesel1.RUN_raised = bool_true;
	handle->ifaceDiesel1.START_value = bool_false;
	handle->ifaceDiesel1.START_raised = bool_true;
	handle->ifaceDiesel1.STOP_value = bool_false;
	handle->ifaceDiesel1.STOP_raised = bool_true;
	handle->ifaceDiesel1.GLOW_value = bool_false;
	handle->ifaceDiesel1.GLOW_raised = bool_true;
	handle->ifaceDiesel2.RUN_value = bool_false;
	handle->ifaceDiesel2.RUN_raised = bool_true;
	handle->ifaceDiesel2.START_value = bool_false;
	handle->ifaceDiesel2.START_raised = bool_true;
	handle->ifaceDiesel2.STOP_value = bool_false;
	handle->ifaceDiesel2.STOP_raised = bool_true;
	handle->ifaceDiesel2.GLOW_value = bool_false;
	handle->ifaceDiesel2.GLOW_raised = bool_true;
	handle->ifaceSensors.SIRENA_EN_value = bool_false;
	handle->ifaceSensors.SIRENA_EN_raised = bool_true;
	handle->ifaceSensors.LED_GLOW_value = bool_false;
	handle->ifaceSensors.LED_GLOW_raised = bool_true;
	handle->ifaceSensors.LED_AIR_value = bool_false;
	handle->ifaceSensors.LED_AIR_raised = bool_true;
	handle->ifaceSensors.LED_EXT_value = bool_false;
	handle->ifaceSensors.LED_EXT_raised = bool_true;
	handle->ifaceSensors.LED_OIL_value = bool_false;
	handle->ifaceSensors.LED_OIL_raised = bool_true;
	handle->ifaceSensors.LED_TEMP_value = bool_false;
	handle->ifaceSensors.LED_TEMP_raised = bool_true;
	handle->ifaceCooling.COOL_EN_value = bool_false;
	handle->ifaceCooling.COOL_EN_raised = bool_true;
	handle->ifaceElmot.RUN_value = bool_false;
	handle->ifaceElmot.RUN_raised = bool_true;
	handle->ifaceHydro.RUN_value = bool_false;
	handle->ifaceHydro.RUN_raised = bool_true;
}

/* Entry action for statechart 'TestProj'. */
static void testProj_enact_SequenceImpl(TestProj* handle) {
}

/* Exit action for state 'TestProj'. */
static void testProj_exact_SequenceImpl(TestProj* handle) {
}

/* The reactions of state Init. */
static void testProj_react_Start_logic_Start_logic_r1_Init(TestProj* handle) {
	/* The reactions of state Init. */
	if (testProj_check_lr0(handle)) { 
		testProj_effect_lr0(handle);
	} 
	if (handle->ifaceKeyboard.START_STOP_HIGH_raised) { 
		/* Default exit sequence for state Init */
		handle->stateConfVector[0] = TestProj_last_state;
		handle->stateConfVectorPosition = 0;
		/* 'default' enter sequence for state User start requested */
		handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_User_start_requested;
		handle->stateConfVectorPosition = 0;
	}  else {
		if (handle->ifaceCooling.COOL_TMC_raised) { 
			/* Default exit sequence for state Init */
			handle->stateConfVector[0] = TestProj_last_state;
			handle->stateConfVectorPosition = 0;
			handle->internal.StartedBy = 2;
			/* The reactions of exit exit_start. */
			/* Default exit sequence for state Start logic */
			/* Default exit sequence for region r1 */
			/* Handle exit of all possible states (of r1) at position 0... */
			switch(handle->stateConfVector[ 0 ]) {
				case TestProj_Start_logic_Start_logic_r1_Init : {
					/* Default exit sequence for state Init */
					handle->stateConfVector[0] = TestProj_last_state;
					handle->stateConfVectorPosition = 0;
					break;
				}
				case TestProj_Start_logic_Start_logic_r1_User_start_requested : {
					/* Default exit sequence for state User start requested */
					handle->stateConfVector[0] = TestProj_last_state;
					handle->stateConfVectorPosition = 0;
					break;
				}
				default: break;
			}
			/* 'default' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		}  else {
			if (handle->internal.StartedBy == 0) { 
				/* Default exit sequence for state Init */
				handle->stateConfVector[0] = TestProj_last_state;
				handle->stateConfVectorPosition = 0;
				handle->ifaceCooling.COOL_EN_value = bool_false;
				handle->ifaceCooling.COOL_EN_raised = bool_true;
				handle->internal.RunMotor = bool_false;
				handle->internal.StopDiesel = bool_true;
				/* The reactions of exit exit_start. */
				/* Default exit sequence for state Start logic */
				/* Default exit sequence for region r1 */
				/* Handle exit of all possible states (of r1) at position 0... */
				switch(handle->stateConfVector[ 0 ]) {
					case TestProj_Start_logic_Start_logic_r1_Init : {
						/* Default exit sequence for state Init */
						handle->stateConfVector[0] = TestProj_last_state;
						handle->stateConfVectorPosition = 0;
						break;
					}
					case TestProj_Start_logic_Start_logic_r1_User_start_requested : {
						/* Default exit sequence for state User start requested */
						handle->stateConfVector[0] = TestProj_last_state;
						handle->stateConfVectorPosition = 0;
						break;
					}
					default: break;
				}
				/* 'default' enter sequence for state Motor priority logic */
				handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
				handle->stateConfVectorPosition = 1;
			}  else {
				if (handle->ifaceVentilation.ON_raised && handle->ifaceVentilation.U_LOW_raised) { 
					/* Default exit sequence for state Init */
					handle->stateConfVector[0] = TestProj_last_state;
					handle->stateConfVectorPosition = 0;
					handle->internal.StartedBy = 3;
					/* The reactions of exit exit_start. */
					/* Default exit sequence for state Start logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 0... */
					switch(handle->stateConfVector[ 0 ]) {
						case TestProj_Start_logic_Start_logic_r1_Init : {
							/* Default exit sequence for state Init */
							handle->stateConfVector[0] = TestProj_last_state;
							handle->stateConfVectorPosition = 0;
							break;
						}
						case TestProj_Start_logic_Start_logic_r1_User_start_requested : {
							/* Default exit sequence for state User start requested */
							handle->stateConfVector[0] = TestProj_last_state;
							handle->stateConfVectorPosition = 0;
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Motor priority logic */
					handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
					handle->stateConfVectorPosition = 1;
				} 
			}
		}
	}
}

/* The reactions of state User start requested. */
static void testProj_react_Start_logic_Start_logic_r1_User_start_requested(TestProj* handle) {
	/* The reactions of state User start requested. */
	if (testProj_check_lr0(handle)) { 
		testProj_effect_lr0(handle);
	} 
	if (handle->ifaceKeyboard.START_STOP_LOW_raised) { 
		/* Default exit sequence for state User start requested */
		handle->stateConfVector[0] = TestProj_last_state;
		handle->stateConfVectorPosition = 0;
		/* The reactions of state null. */
		if (handle->internal.StartedBy >= 1) { 
			handle->internal.StartedBy = 0;
			/* 'default' enter sequence for state Init */
			/* Entry action for state 'Init'. */
			handle->internal.Init_raised = bool_true;
			handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
			handle->stateConfVectorPosition = 0;
		}  else {
			handle->internal.StartedBy = 1;
			/* The reactions of exit exit_start. */
			/* Default exit sequence for state Start logic */
			/* Default exit sequence for region r1 */
			/* Handle exit of all possible states (of r1) at position 0... */
			switch(handle->stateConfVector[ 0 ]) {
				case TestProj_Start_logic_Start_logic_r1_Init : {
					/* Default exit sequence for state Init */
					handle->stateConfVector[0] = TestProj_last_state;
					handle->stateConfVectorPosition = 0;
					break;
				}
				case TestProj_Start_logic_Start_logic_r1_User_start_requested : {
					/* Default exit sequence for state User start requested */
					handle->stateConfVector[0] = TestProj_last_state;
					handle->stateConfVectorPosition = 0;
					break;
				}
				default: break;
			}
			/* 'default' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		}
	} 
}

/* The reactions of state Restart. */
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart(TestProj* handle) {
	/* The reactions of state Restart. */
	if (handle->internal.TEMP_HI) { 
		/* Default exit sequence for state Start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			default: break;
		}
		/* The reactions of exit exit_error. */
		/* Default exit sequence for state Diesel start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
				/* Default exit sequence for state Starting */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Starting'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
				handle->internal.START_value = bool_true;
				handle->internal.START_raised = bool_true;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
				/* Default exit sequence for state Running check */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				break;
			}
			default: break;
		}
		/* 'entry_diesel_error' enter sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
		handle->stateConfVectorPosition = 1;
	}  else {
		if (! handle->internal.OIL_LOW) { 
			/* Default exit sequence for state Start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				default: break;
			}
			/* The reactions of exit exit_error. */
			/* Default exit sequence for state Diesel start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
					/* Default exit sequence for state Starting */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Starting'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
					handle->internal.START_value = bool_true;
					handle->internal.START_raised = bool_true;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
					/* Default exit sequence for state Running check */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					break;
				}
				default: break;
			}
			/* 'entry_diesel_error' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		}  else {
			if (handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) { 
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				/* 'default' enter sequence for state Glowing */
				/* Entry action for state 'Glowing'. */
				testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) , 5 * 1000, bool_false);
				handle->internal.GLOW_value = bool_true;
				handle->internal.GLOW_raised = bool_true;
				handle->stateConfVector[1] = TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing;
				handle->stateConfVectorPosition = 1;
			} 
		}
	}
}

/* The reactions of state Glowing. */
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing(TestProj* handle) {
	/* The reactions of state Glowing. */
	if (handle->internal.TEMP_HI) { 
		/* Default exit sequence for state Start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			default: break;
		}
		/* The reactions of exit exit_error. */
		/* Default exit sequence for state Diesel start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
				/* Default exit sequence for state Starting */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Starting'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
				handle->internal.START_value = bool_true;
				handle->internal.START_raised = bool_true;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
				/* Default exit sequence for state Running check */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				break;
			}
			default: break;
		}
		/* 'entry_diesel_error' enter sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
		handle->stateConfVectorPosition = 1;
	}  else {
		if (! handle->internal.OIL_LOW) { 
			/* Default exit sequence for state Start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				default: break;
			}
			/* The reactions of exit exit_error. */
			/* Default exit sequence for state Diesel start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
					/* Default exit sequence for state Starting */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Starting'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
					handle->internal.START_value = bool_true;
					handle->internal.START_raised = bool_true;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
					/* Default exit sequence for state Running check */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					break;
				}
				default: break;
			}
			/* 'entry_diesel_error' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		}  else {
			if (handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) { 
				/* Default exit sequence for state Start sequence */
				/* Default exit sequence for region inner region */
				/* Handle exit of all possible states (of inner region) at position 1... */
				switch(handle->stateConfVector[ 1 ]) {
					case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
						/* Default exit sequence for state Restart */
						handle->stateConfVector[1] = TestProj_last_state;
						handle->stateConfVectorPosition = 1;
						/* Exit action for state 'Restart'. */
						testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
						handle->internal.DieselStartTries -= 1;
						break;
					}
					case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
						/* Default exit sequence for state Glowing */
						handle->stateConfVector[1] = TestProj_last_state;
						handle->stateConfVectorPosition = 1;
						/* Exit action for state 'Glowing'. */
						testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
						break;
					}
					case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
						/* Default exit sequence for state Diesel 2 start */
						handle->stateConfVector[1] = TestProj_last_state;
						handle->stateConfVectorPosition = 1;
						/* Exit action for state 'Diesel 2 start'. */
						testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
						break;
					}
					case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
						/* Default exit sequence for state Alarm */
						handle->stateConfVector[1] = TestProj_last_state;
						handle->stateConfVectorPosition = 1;
						/* Exit action for state 'Alarm'. */
						testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
						break;
					}
					case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
						/* Default exit sequence for state Diesel1 start */
						handle->stateConfVector[1] = TestProj_last_state;
						handle->stateConfVectorPosition = 1;
						/* Exit action for state 'Diesel1 start'. */
						testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
						break;
					}
					default: break;
				}
				/* 'default' enter sequence for state Starting */
				/* Entry action for state 'Starting'. */
				testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) , 5 * 1000, bool_false);
				handle->stateConfVector[1] = TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting;
				handle->stateConfVectorPosition = 1;
			} 
		}
	}
}

/* The reactions of state Diesel 2 start. */
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start(TestProj* handle) {
	/* The reactions of state Diesel 2 start. */
	if (handle->internal.TEMP_HI) { 
		/* Default exit sequence for state Start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			default: break;
		}
		/* The reactions of exit exit_error. */
		/* Default exit sequence for state Diesel start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
				/* Default exit sequence for state Starting */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Starting'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
				handle->internal.START_value = bool_true;
				handle->internal.START_raised = bool_true;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
				/* Default exit sequence for state Running check */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				break;
			}
			default: break;
		}
		/* 'entry_diesel_error' enter sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
		handle->stateConfVectorPosition = 1;
	}  else {
		if (! handle->internal.OIL_LOW) { 
			/* Default exit sequence for state Start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				default: break;
			}
			/* The reactions of exit exit_error. */
			/* Default exit sequence for state Diesel start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
					/* Default exit sequence for state Starting */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Starting'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
					handle->internal.START_value = bool_true;
					handle->internal.START_raised = bool_true;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
					/* Default exit sequence for state Running check */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					break;
				}
				default: break;
			}
			/* 'entry_diesel_error' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		}  else {
			if (handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) { 
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				/* 'default' enter sequence for state Alarm */
				/* Entry action for state 'Alarm'. */
				testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) , 5 * 1000, bool_false);
				handle->ifaceSensors.SIRENA_EN_value = bool_true;
				handle->ifaceSensors.SIRENA_EN_raised = bool_true;
				handle->internal.DieselStartTries = 3;
				handle->stateConfVector[1] = TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm;
				handle->stateConfVectorPosition = 1;
			} 
		}
	}
}

/* The reactions of state Alarm. */
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm(TestProj* handle) {
	/* The reactions of state Alarm. */
	if (handle->internal.TEMP_HI) { 
		/* Default exit sequence for state Start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			default: break;
		}
		/* The reactions of exit exit_error. */
		/* Default exit sequence for state Diesel start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
				/* Default exit sequence for state Starting */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Starting'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
				handle->internal.START_value = bool_true;
				handle->internal.START_raised = bool_true;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
				/* Default exit sequence for state Running check */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				break;
			}
			default: break;
		}
		/* 'entry_diesel_error' enter sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
		handle->stateConfVectorPosition = 1;
	}  else {
		if (! handle->internal.OIL_LOW) { 
			/* Default exit sequence for state Start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				default: break;
			}
			/* The reactions of exit exit_error. */
			/* Default exit sequence for state Diesel start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
					/* Default exit sequence for state Starting */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Starting'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
					handle->internal.START_value = bool_true;
					handle->internal.START_raised = bool_true;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
					/* Default exit sequence for state Running check */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					break;
				}
				default: break;
			}
			/* 'entry_diesel_error' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		}  else {
			if (handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) { 
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				/* 'default' enter sequence for state Glowing */
				/* Entry action for state 'Glowing'. */
				testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) , 5 * 1000, bool_false);
				handle->internal.GLOW_value = bool_true;
				handle->internal.GLOW_raised = bool_true;
				handle->stateConfVector[1] = TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing;
				handle->stateConfVectorPosition = 1;
			} 
		}
	}
}

/* The reactions of state Diesel1 start. */
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start(TestProj* handle) {
	/* The reactions of state Diesel1 start. */
	if (handle->internal.TEMP_HI) { 
		/* Default exit sequence for state Start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			default: break;
		}
		/* The reactions of exit exit_error. */
		/* Default exit sequence for state Diesel start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
				/* Default exit sequence for state Starting */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Starting'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
				handle->internal.START_value = bool_true;
				handle->internal.START_raised = bool_true;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
				/* Default exit sequence for state Running check */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				break;
			}
			default: break;
		}
		/* 'entry_diesel_error' enter sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
		handle->stateConfVectorPosition = 1;
	}  else {
		if (! handle->internal.OIL_LOW) { 
			/* Default exit sequence for state Start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				default: break;
			}
			/* The reactions of exit exit_error. */
			/* Default exit sequence for state Diesel start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
					/* Default exit sequence for state Restart */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Restart'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
					handle->internal.DieselStartTries -= 1;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
					/* Default exit sequence for state Glowing */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Glowing'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
					/* Default exit sequence for state Diesel 2 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel 2 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
					/* Default exit sequence for state Alarm */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Alarm'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
					/* Default exit sequence for state Diesel1 start */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Diesel1 start'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
					/* Default exit sequence for state Starting */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Starting'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
					handle->internal.START_value = bool_true;
					handle->internal.START_raised = bool_true;
					break;
				}
				case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
					/* Default exit sequence for state Running check */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					break;
				}
				default: break;
			}
			/* 'entry_diesel_error' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		}  else {
			if (handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) { 
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				/* 'default' enter sequence for state Alarm */
				/* Entry action for state 'Alarm'. */
				testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) , 5 * 1000, bool_false);
				handle->ifaceSensors.SIRENA_EN_value = bool_true;
				handle->ifaceSensors.SIRENA_EN_raised = bool_true;
				handle->internal.DieselStartTries = 3;
				handle->stateConfVector[1] = TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm;
				handle->stateConfVectorPosition = 1;
			} 
		}
	}
}

/* The reactions of state Starting. */
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Starting(TestProj* handle) {
	/* The reactions of state Starting. */
	if (handle->internal.TEMP_HI || handle->internal.DieselStartTries == 0) { 
		/* Default exit sequence for state Starting */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* Exit action for state 'Starting'. */
		testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
		handle->internal.START_value = bool_true;
		handle->internal.START_raised = bool_true;
		/* The reactions of exit exit_error. */
		/* Default exit sequence for state Diesel start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
				/* Default exit sequence for state Starting */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Starting'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
				handle->internal.START_value = bool_true;
				handle->internal.START_raised = bool_true;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
				/* Default exit sequence for state Running check */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				break;
			}
			default: break;
		}
		/* 'entry_diesel_error' enter sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
		handle->stateConfVectorPosition = 1;
	}  else {
		if (handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) { 
			/* Default exit sequence for state Starting */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Starting'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
			handle->internal.START_value = bool_true;
			handle->internal.START_raised = bool_true;
			/* 'default' enter sequence for state Running check */
			/* Entry action for state 'Running check'. */
			handle->ifaceSensors.SIRENA_EN_value = bool_false;
			handle->ifaceSensors.SIRENA_EN_raised = bool_true;
			handle->internal.START_value = bool_false;
			handle->internal.START_raised = bool_true;
			handle->stateConfVector[1] = TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check;
			handle->stateConfVectorPosition = 1;
		} 
	}
}

/* The reactions of state Running check. */
static void testProj_react_Motor_control_Diesel_start_sequence_inner_region_Running_check(TestProj* handle) {
	/* The reactions of state Running check. */
	if (! handle->internal.OIL_LOW) { 
		/* Default exit sequence for state Running check */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* The reactions of exit default. */
		/* Default exit sequence for state Diesel start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart : {
				/* Default exit sequence for state Restart */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Restart'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) );		
				handle->internal.DieselStartTries -= 1;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing : {
				/* Default exit sequence for state Glowing */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Glowing'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start : {
				/* Default exit sequence for state Diesel 2 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel 2 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm : {
				/* Default exit sequence for state Alarm */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Alarm'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start : {
				/* Default exit sequence for state Diesel1 start */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Diesel1 start'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting : {
				/* Default exit sequence for state Starting */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Starting'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised) );		
				handle->internal.START_value = bool_true;
				handle->internal.START_raised = bool_true;
				break;
			}
			case TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check : {
				/* Default exit sequence for state Running check */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				break;
			}
			default: break;
		}
		/* 'entry_running' enter sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
		handle->stateConfVectorPosition = 1;
	}  else {
		if (handle->internal.OIL_LOW) { 
			/* Default exit sequence for state Running check */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* 'default' enter sequence for state Restart */
			/* Entry action for state 'Restart'. */
			testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised) , 3 * 1000, bool_false);
			handle->internal.STOP_value = bool_false;
			handle->internal.STOP_raised = bool_true;
			handle->internal.GLOW_value = bool_false;
			handle->internal.GLOW_raised = bool_true;
			handle->stateConfVector[1] = TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart;
			handle->stateConfVectorPosition = 1;
		} 
	}
}

/* The reactions of state Turn off electronics. */
static void testProj_react_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics(TestProj* handle) {
	/* The reactions of state Turn off electronics. */
	if (handle->internal.ActiveMotor == 1) { 
		/* Default exit sequence for state Turn off electronics */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* The reactions of exit exit_start_diesel_2. */
		/* Default exit sequence for state Diesel shutdown sequence */
		/* Default exit sequence for region Shutdown sequence */
		/* Handle exit of all possible states (of Shutdown sequence) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics : {
				/* Default exit sequence for state Turn off electronics */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				break;
			}
			case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown : {
				/* Default exit sequence for state Shutdown */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Shutdown'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown_tev0_raised) );		
				break;
			}
			default: break;
		}
		/* 'entry_start_diesel_2' enter sequence for state Diesel start sequence */
		/* 'entry_start_diesel_2' enter sequence for region inner region */
		/* Default react sequence for initial entry entry_start_diesel_2 */
		/* 'default' enter sequence for state Diesel 2 start */
		/* Entry action for state 'Diesel 2 start'. */
		testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised) , 1 * 1000, bool_false);
		handle->internal.ActiveMotor = 2;
		handle->internal.RUN_value = bool_true;
		handle->internal.RUN_raised = bool_true;
		handle->internal.STOP_value = bool_true;
		handle->internal.STOP_raised = bool_true;
		handle->stateConfVector[1] = TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start;
		handle->stateConfVectorPosition = 1;
	}  else {
		if (handle->internal.ActiveMotor != 1) { 
			/* Default exit sequence for state Turn off electronics */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			handle->internal.ActiveMotor = 0;
			/* The reactions of exit exit_not_running. */
			/* Default exit sequence for state Diesel shutdown sequence */
			/* Default exit sequence for region Shutdown sequence */
			/* Handle exit of all possible states (of Shutdown sequence) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics : {
					/* Default exit sequence for state Turn off electronics */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					break;
				}
				case TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown : {
					/* Default exit sequence for state Shutdown */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Shutdown'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown_tev0_raised) );		
					break;
				}
				default: break;
			}
			/* 'entry_diesel_error' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		} 
	}
}

/* The reactions of state Shutdown. */
static void testProj_react_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown(TestProj* handle) {
	/* The reactions of state Shutdown. */
	if (handle->timeEvents.testProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown_tev0_raised) { 
		/* Default exit sequence for state Shutdown */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* Exit action for state 'Shutdown'. */
		testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown_tev0_raised) );		
		/* 'default' enter sequence for state Turn off electronics */
		/* Entry action for state 'Turn off electronics'. */
		handle->internal.RUN_value = bool_false;
		handle->internal.RUN_raised = bool_true;
		handle->stateConfVector[1] = TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics;
		handle->stateConfVectorPosition = 1;
	} 
}

/* The reactions of state Motor priority logic. */
static void testProj_react_Motor_control_Motor_priority_logic(TestProj* handle) {
	/* The reactions of state Motor priority logic. */
	if (handle->internal.StartedBy >= 1 && handle->ifaceKeyboard.START_STOP_HIGH_raised) { 
		/* Default exit sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* 'default' enter sequence for state Cooling logic */
		/* 'default' enter sequence for region r1 */
		/* Default react sequence for initial entry  */
		/* 'default' enter sequence for state Coutner */
		/* Entry action for state 'Coutner'. */
		testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) , 1 * 1000, bool_true);
		handle->stateConfVector[1] = TestProj_Motor_control_Cooling_logic_r1_Coutner;
		handle->stateConfVectorPosition = 1;
	} 
}

/* The reactions of state Elmot wait run. */
static void testProj_react_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run(TestProj* handle) {
	/* The reactions of state Elmot wait run. */
	if (handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run_tev0_raised) { 
		/* Default exit sequence for state Elmot wait run */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* Exit action for state 'Elmot wait run'. */
		testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run_tev0_raised) );		
		/* The reactions of state null. */
		if (! handle->ifaceElmot.RUNNING_raised) { 
			/* 'default' enter sequence for state Shut elmot */
			/* Entry action for state 'Shut elmot'. */
			testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot_tev0_raised) , 3 * 1000, bool_false);
			handle->ifaceElmot.RUN_value = bool_false;
			handle->ifaceElmot.RUN_raised = bool_true;
			handle->internal.ActiveMotor -= 3;
			handle->stateConfVector[1] = TestProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot;
			handle->stateConfVectorPosition = 1;
		}  else {
			/* The reactions of exit default. */
			/* Default exit sequence for state Elmot start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run : {
					/* Default exit sequence for state Elmot wait run */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Elmot wait run'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot : {
					/* Default exit sequence for state Shut elmot */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Shut elmot'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot_tev0_raised) );		
					break;
				}
				default: break;
			}
			/* 'entry_running' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		}
	} 
}

/* The reactions of state Shut elmot. */
static void testProj_react_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot(TestProj* handle) {
	/* The reactions of state Shut elmot. */
	if (handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot_tev0_raised) { 
		/* Default exit sequence for state Shut elmot */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* Exit action for state 'Shut elmot'. */
		testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot_tev0_raised) );		
		/* The reactions of exit exit_error. */
		/* Default exit sequence for state Elmot start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run : {
				/* Default exit sequence for state Elmot wait run */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Elmot wait run'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot : {
				/* Default exit sequence for state Shut elmot */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Shut elmot'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot_tev0_raised) );		
				break;
			}
			default: break;
		}
		/* 'entry_elmot_error' enter sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
		handle->stateConfVectorPosition = 1;
	} 
}

/* The reactions of state hydro wait run. */
static void testProj_react_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run(TestProj* handle) {
	/* The reactions of state hydro wait run. */
	if (handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run_tev0_raised) { 
		/* Default exit sequence for state hydro wait run */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* Exit action for state 'hydro wait run'. */
		testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run_tev0_raised) );		
		/* The reactions of state null. */
		if (! handle->ifaceHydro.RUNNING_raised) { 
			/* 'default' enter sequence for state Shut hydro */
			/* Entry action for state 'Shut hydro'. */
			testProj_setTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro_tev0_raised) , 3 * 1000, bool_false);
			handle->ifaceHydro.RUN_value = bool_false;
			handle->ifaceHydro.RUN_raised = bool_true;
			handle->internal.ActiveMotor = 0;
			handle->stateConfVector[1] = TestProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro;
			handle->stateConfVectorPosition = 1;
		}  else {
			/* The reactions of exit default. */
			/* Default exit sequence for state Hydro start sequence */
			/* Default exit sequence for region inner region */
			/* Handle exit of all possible states (of inner region) at position 1... */
			switch(handle->stateConfVector[ 1 ]) {
				case TestProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run : {
					/* Default exit sequence for state hydro wait run */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'hydro wait run'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run_tev0_raised) );		
					break;
				}
				case TestProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro : {
					/* Default exit sequence for state Shut hydro */
					handle->stateConfVector[1] = TestProj_last_state;
					handle->stateConfVectorPosition = 1;
					/* Exit action for state 'Shut hydro'. */
					testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro_tev0_raised) );		
					break;
				}
				default: break;
			}
			/* 'entry_running' enter sequence for state Motor priority logic */
			handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
			handle->stateConfVectorPosition = 1;
		}
	} 
}

/* The reactions of state Shut hydro. */
static void testProj_react_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro(TestProj* handle) {
	/* The reactions of state Shut hydro. */
	if (handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro_tev0_raised) { 
		/* Default exit sequence for state Shut hydro */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* Exit action for state 'Shut hydro'. */
		testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro_tev0_raised) );		
		/* The reactions of exit exit_error. */
		/* Default exit sequence for state Hydro start sequence */
		/* Default exit sequence for region inner region */
		/* Handle exit of all possible states (of inner region) at position 1... */
		switch(handle->stateConfVector[ 1 ]) {
			case TestProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run : {
				/* Default exit sequence for state hydro wait run */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'hydro wait run'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run_tev0_raised) );		
				break;
			}
			case TestProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro : {
				/* Default exit sequence for state Shut hydro */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Shut hydro'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro_tev0_raised) );		
				break;
			}
			default: break;
		}
		/* 'entry_hydro_error' enter sequence for state Motor priority logic */
		handle->stateConfVector[1] = TestProj_Motor_control_Motor_priority_logic;
		handle->stateConfVectorPosition = 1;
	} 
}

/* The reactions of state Coutner. */
static void testProj_react_Motor_control_Cooling_logic_r1_Coutner(TestProj* handle) {
	/* The reactions of state Coutner. */
	if (handle->internal.StartedBy == 2) { 
		/* Default exit sequence for state Coutner */
		handle->stateConfVector[1] = TestProj_last_state;
		handle->stateConfVectorPosition = 1;
		/* Exit action for state 'Coutner'. */
		testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
		/* The reactions of state null. */
		if (handle->ifaceCooling.COOL_TMC_raised) { 
			handle->ifaceCooling.COOL_EN_value = bool_true;
			handle->ifaceCooling.COOL_EN_raised = bool_true;
			/* The reactions of state null. */
			if ((handle->internal.ActiveMotor == 1 || handle->internal.ActiveMotor == 2) && (handle->internal.CoolPause >= handle->internal.CoolPauseLength)) { 
				handle->internal.CoolPause = 0;
				handle->internal.StopDiesel = bool_true;
				handle->internal.StartedBy = 0;
				/* The reactions of state null. */
				if (handle->internal.CoolPause >= handle->internal.CoolPauseLength) { 
					handle->internal.CoolPause = 0;
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}  else {
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}
			}  else {
				handle->internal.StopDiesel = bool_false;
				/* The reactions of state null. */
				if (handle->internal.CoolPause >= handle->internal.CoolPauseLength) { 
					handle->internal.CoolPause = 0;
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}  else {
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}
			}
		}  else {
			handle->internal.RunMotor = bool_false;
			handle->ifaceCooling.COOL_EN_value = bool_false;
			handle->ifaceCooling.COOL_EN_raised = bool_true;
			/* The reactions of state null. */
			if ((handle->internal.ActiveMotor == 1 || handle->internal.ActiveMotor == 2) && (handle->internal.CoolPause >= handle->internal.CoolPauseLength)) { 
				handle->internal.CoolPause = 0;
				handle->internal.StopDiesel = bool_true;
				handle->internal.StartedBy = 0;
				/* The reactions of state null. */
				if (handle->internal.CoolPause >= handle->internal.CoolPauseLength) { 
					handle->internal.CoolPause = 0;
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}  else {
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}
			}  else {
				handle->internal.StopDiesel = bool_false;
				/* The reactions of state null. */
				if (handle->internal.CoolPause >= handle->internal.CoolPauseLength) { 
					handle->internal.CoolPause = 0;
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}  else {
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}
			}
		}
	}  else {
		if (handle->internal.StartedBy == 1 || handle->internal.StartedBy == 3) { 
			/* Default exit sequence for state Coutner */
			handle->stateConfVector[1] = TestProj_last_state;
			handle->stateConfVectorPosition = 1;
			/* Exit action for state 'Coutner'. */
			testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
			handle->internal.RunMotor = bool_true;
			handle->internal.StopDiesel = bool_false;
			/* The reactions of state null. */
			if (handle->internal.CoolPause >= handle->internal.CoolPauseLength) { 
				handle->internal.CoolPause = 0;
				/* The reactions of exit exit_start_init. */
				/* Default exit sequence for state Cooling logic */
				/* Default exit sequence for region r1 */
				/* Handle exit of all possible states (of r1) at position 1... */
				switch(handle->stateConfVector[ 1 ]) {
					case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
						/* Default exit sequence for state Coutner */
						handle->stateConfVector[1] = TestProj_last_state;
						handle->stateConfVectorPosition = 1;
						/* Exit action for state 'Coutner'. */
						testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
						break;
					}
					default: break;
				}
				/* 'default' enter sequence for state Start logic */
				/* 'default' enter sequence for region r1 */
				/* Default react sequence for initial entry  */
				/* 'default' enter sequence for state Init */
				/* Entry action for state 'Init'. */
				handle->internal.Init_raised = bool_true;
				handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
				handle->stateConfVectorPosition = 0;
			}  else {
				/* The reactions of exit exit_start_init. */
				/* Default exit sequence for state Cooling logic */
				/* Default exit sequence for region r1 */
				/* Handle exit of all possible states (of r1) at position 1... */
				switch(handle->stateConfVector[ 1 ]) {
					case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
						/* Default exit sequence for state Coutner */
						handle->stateConfVector[1] = TestProj_last_state;
						handle->stateConfVectorPosition = 1;
						/* Exit action for state 'Coutner'. */
						testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
						break;
					}
					default: break;
				}
				/* 'default' enter sequence for state Start logic */
				/* 'default' enter sequence for region r1 */
				/* Default react sequence for initial entry  */
				/* 'default' enter sequence for state Init */
				/* Entry action for state 'Init'. */
				handle->internal.Init_raised = bool_true;
				handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
				handle->stateConfVectorPosition = 0;
			}
		}  else {
			if (handle->internal.StartedBy == 0) { 
				/* Default exit sequence for state Coutner */
				handle->stateConfVector[1] = TestProj_last_state;
				handle->stateConfVectorPosition = 1;
				/* Exit action for state 'Coutner'. */
				testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
				/* The reactions of state null. */
				if (handle->internal.CoolPause >= handle->internal.CoolPauseLength) { 
					handle->internal.CoolPause = 0;
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}  else {
					/* The reactions of exit exit_start_init. */
					/* Default exit sequence for state Cooling logic */
					/* Default exit sequence for region r1 */
					/* Handle exit of all possible states (of r1) at position 1... */
					switch(handle->stateConfVector[ 1 ]) {
						case TestProj_Motor_control_Cooling_logic_r1_Coutner : {
							/* Default exit sequence for state Coutner */
							handle->stateConfVector[1] = TestProj_last_state;
							handle->stateConfVectorPosition = 1;
							/* Exit action for state 'Coutner'. */
							testProj_unsetTimer(handle, (sc_eventid) &(handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) );		
							break;
						}
						default: break;
					}
					/* 'default' enter sequence for state Start logic */
					/* 'default' enter sequence for region r1 */
					/* Default react sequence for initial entry  */
					/* 'default' enter sequence for state Init */
					/* Entry action for state 'Init'. */
					handle->internal.Init_raised = bool_true;
					handle->stateConfVector[0] = TestProj_Start_logic_Start_logic_r1_Init;
					handle->stateConfVectorPosition = 0;
				}
			}  else {
				if (handle->timeEvents.testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised) { 
					handle->internal.CoolPause += 1;
				} 
			}
		}
	}
}

/* The reactions of state STOP. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP(TestProj* handle) {
	/* The reactions of state STOP. */
	if (handle->internal.STOP_raised) { 
		/* Default exit sequence for state STOP */
		handle->stateConfVector[2] = TestProj_last_state;
		handle->stateConfVectorPosition = 2;
		/* The reactions of state null. */
		if (handle->internal.ActiveMotor == 1) { 
			handle->ifaceDiesel1.STOP_value = handle->internal.STOP_value;
			handle->ifaceDiesel1.STOP_raised = bool_true;
			/* 'default' enter sequence for state STOP */
			handle->stateConfVector[2] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP;
			handle->stateConfVectorPosition = 2;
		}  else {
			handle->ifaceDiesel2.STOP_value = handle->internal.STOP_value;
			handle->ifaceDiesel2.STOP_raised = bool_true;
			/* 'default' enter sequence for state STOP */
			handle->stateConfVector[2] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP;
			handle->stateConfVectorPosition = 2;
		}
	} 
}

/* The reactions of state START. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START(TestProj* handle) {
	/* The reactions of state START. */
	if (handle->internal.START_raised) { 
		/* Default exit sequence for state START */
		handle->stateConfVector[3] = TestProj_last_state;
		handle->stateConfVectorPosition = 3;
		/* The reactions of state null. */
		if (handle->internal.ActiveMotor == 1) { 
			handle->ifaceDiesel1.START_value = handle->internal.START_value;
			handle->ifaceDiesel1.START_raised = bool_true;
			/* 'default' enter sequence for state START */
			handle->stateConfVector[3] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START;
			handle->stateConfVectorPosition = 3;
		}  else {
			handle->ifaceDiesel2.START_value = handle->internal.START_value;
			handle->ifaceDiesel2.START_raised = bool_true;
			/* 'default' enter sequence for state START */
			handle->stateConfVector[3] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START;
			handle->stateConfVectorPosition = 3;
		}
	} 
}

/* The reactions of state RUN. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN(TestProj* handle) {
	/* The reactions of state RUN. */
	if (handle->internal.RUN_raised) { 
		/* Default exit sequence for state RUN */
		handle->stateConfVector[4] = TestProj_last_state;
		handle->stateConfVectorPosition = 4;
		/* The reactions of state null. */
		if (handle->internal.ActiveMotor == 1) { 
			handle->ifaceDiesel1.RUN_value = handle->internal.RUN_value;
			handle->ifaceDiesel1.RUN_raised = bool_true;
			/* 'default' enter sequence for state RUN */
			handle->stateConfVector[4] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN;
			handle->stateConfVectorPosition = 4;
		}  else {
			handle->ifaceDiesel2.RUN_value = handle->internal.RUN_value;
			handle->ifaceDiesel2.RUN_raised = bool_true;
			/* 'default' enter sequence for state RUN */
			handle->stateConfVector[4] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN;
			handle->stateConfVectorPosition = 4;
		}
	} 
}

/* The reactions of state GLOW. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW(TestProj* handle) {
	/* The reactions of state GLOW. */
	if (handle->internal.GLOW_raised) { 
		/* Default exit sequence for state GLOW */
		handle->stateConfVector[5] = TestProj_last_state;
		handle->stateConfVectorPosition = 5;
		/* The reactions of state null. */
		if (handle->internal.ActiveMotor == 1) { 
			handle->ifaceDiesel1.GLOW_value = handle->internal.GLOW_value;
			handle->ifaceDiesel1.GLOW_raised = bool_true;
			/* 'default' enter sequence for state GLOW */
			handle->stateConfVector[5] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW;
			handle->stateConfVectorPosition = 5;
		}  else {
			handle->ifaceDiesel2.GLOW_value = handle->internal.GLOW_value;
			handle->ifaceDiesel2.GLOW_raised = bool_true;
			/* 'default' enter sequence for state GLOW */
			handle->stateConfVector[5] = TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW;
			handle->stateConfVectorPosition = 5;
		}
	} 
}

/* The reactions of state EXT_TRUE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE(TestProj* handle) {
	/* The reactions of state EXT_TRUE. */
	if (handle->ifaceDiesel1.EXT_ON_raised) { 
		/* Default exit sequence for state EXT_TRUE */
		handle->stateConfVector[6] = TestProj_last_state;
		handle->stateConfVectorPosition = 6;
		/* 'default' enter sequence for state EXT_FALSE */
		/* Entry action for state 'EXT_FALSE'. */
		handle->internal.EXT_ON = bool_false;
		handle->stateConfVector[6] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE;
		handle->stateConfVectorPosition = 6;
	}  else {
		if (handle->ifaceDiesel2.EXT_ON_raised) { 
			/* Default exit sequence for state EXT_TRUE */
			handle->stateConfVector[6] = TestProj_last_state;
			handle->stateConfVectorPosition = 6;
			/* 'default' enter sequence for state EXT_FALSE */
			/* Entry action for state 'EXT_FALSE'. */
			handle->internal.EXT_ON = bool_false;
			handle->stateConfVector[6] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE;
			handle->stateConfVectorPosition = 6;
		} 
	}
}

/* The reactions of state EXT_FALSE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE(TestProj* handle) {
	/* The reactions of state EXT_FALSE. */
	if (handle->ifaceDiesel1.EXT_ON_raised) { 
		/* Default exit sequence for state EXT_FALSE */
		handle->stateConfVector[6] = TestProj_last_state;
		handle->stateConfVectorPosition = 6;
		/* 'default' enter sequence for state EXT_TRUE */
		/* Entry action for state 'EXT_TRUE'. */
		handle->internal.EXT_ON = bool_true;
		handle->stateConfVector[6] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE;
		handle->stateConfVectorPosition = 6;
	}  else {
		if (handle->ifaceDiesel2.EXT_ON_raised) { 
			/* Default exit sequence for state EXT_FALSE */
			handle->stateConfVector[6] = TestProj_last_state;
			handle->stateConfVectorPosition = 6;
			/* 'default' enter sequence for state EXT_TRUE */
			/* Entry action for state 'EXT_TRUE'. */
			handle->internal.EXT_ON = bool_true;
			handle->stateConfVector[6] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE;
			handle->stateConfVectorPosition = 6;
		} 
	}
}

/* The reactions of state AIR_TRUE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE(TestProj* handle) {
	/* The reactions of state AIR_TRUE. */
	if (handle->ifaceDiesel1.AIR_ERR_raised) { 
		/* Default exit sequence for state AIR_TRUE */
		handle->stateConfVector[7] = TestProj_last_state;
		handle->stateConfVectorPosition = 7;
		/* 'default' enter sequence for state AIR_FALSE */
		/* Entry action for state 'AIR_FALSE'. */
		handle->internal.AIR_ERR = bool_false;
		handle->stateConfVector[7] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE;
		handle->stateConfVectorPosition = 7;
	}  else {
		if (handle->ifaceDiesel2.AIR_ERR_raised) { 
			/* Default exit sequence for state AIR_TRUE */
			handle->stateConfVector[7] = TestProj_last_state;
			handle->stateConfVectorPosition = 7;
			/* 'default' enter sequence for state AIR_FALSE */
			/* Entry action for state 'AIR_FALSE'. */
			handle->internal.AIR_ERR = bool_false;
			handle->stateConfVector[7] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE;
			handle->stateConfVectorPosition = 7;
		} 
	}
}

/* The reactions of state AIR_FALSE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE(TestProj* handle) {
	/* The reactions of state AIR_FALSE. */
	if (handle->ifaceDiesel1.AIR_ERR_raised) { 
		/* Default exit sequence for state AIR_FALSE */
		handle->stateConfVector[7] = TestProj_last_state;
		handle->stateConfVectorPosition = 7;
		/* 'default' enter sequence for state AIR_TRUE */
		/* Entry action for state 'AIR_TRUE'. */
		handle->internal.AIR_ERR = bool_true;
		handle->stateConfVector[7] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE;
		handle->stateConfVectorPosition = 7;
	}  else {
		if (handle->ifaceDiesel2.AIR_ERR_raised) { 
			/* Default exit sequence for state AIR_FALSE */
			handle->stateConfVector[7] = TestProj_last_state;
			handle->stateConfVectorPosition = 7;
			/* 'default' enter sequence for state AIR_TRUE */
			/* Entry action for state 'AIR_TRUE'. */
			handle->internal.AIR_ERR = bool_true;
			handle->stateConfVector[7] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE;
			handle->stateConfVectorPosition = 7;
		} 
	}
}

/* The reactions of state TEMP_TRUE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE(TestProj* handle) {
	/* The reactions of state TEMP_TRUE. */
	if (handle->ifaceDiesel1.TEMP_HI_raised) { 
		/* Default exit sequence for state TEMP_TRUE */
		handle->stateConfVector[8] = TestProj_last_state;
		handle->stateConfVectorPosition = 8;
		/* 'default' enter sequence for state TEMP_FALSE */
		/* Entry action for state 'TEMP_FALSE'. */
		handle->internal.TEMP_HI = bool_false;
		handle->stateConfVector[8] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE;
		handle->stateConfVectorPosition = 8;
	}  else {
		if (handle->ifaceDiesel2.TEMP_HI_raised) { 
			/* Default exit sequence for state TEMP_TRUE */
			handle->stateConfVector[8] = TestProj_last_state;
			handle->stateConfVectorPosition = 8;
			/* 'default' enter sequence for state TEMP_FALSE */
			/* Entry action for state 'TEMP_FALSE'. */
			handle->internal.TEMP_HI = bool_false;
			handle->stateConfVector[8] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE;
			handle->stateConfVectorPosition = 8;
		} 
	}
}

/* The reactions of state TEMP_FALSE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE(TestProj* handle) {
	/* The reactions of state TEMP_FALSE. */
	if (handle->ifaceDiesel1.TEMP_HI_raised) { 
		/* Default exit sequence for state TEMP_FALSE */
		handle->stateConfVector[8] = TestProj_last_state;
		handle->stateConfVectorPosition = 8;
		/* 'default' enter sequence for state TEMP_TRUE */
		/* Entry action for state 'TEMP_TRUE'. */
		handle->internal.TEMP_HI = bool_true;
		handle->stateConfVector[8] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE;
		handle->stateConfVectorPosition = 8;
	}  else {
		if (handle->ifaceDiesel2.TEMP_HI_raised) { 
			/* Default exit sequence for state TEMP_FALSE */
			handle->stateConfVector[8] = TestProj_last_state;
			handle->stateConfVectorPosition = 8;
			/* 'default' enter sequence for state TEMP_TRUE */
			/* Entry action for state 'TEMP_TRUE'. */
			handle->internal.TEMP_HI = bool_true;
			handle->stateConfVector[8] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE;
			handle->stateConfVectorPosition = 8;
		} 
	}
}

/* The reactions of state OIL_TRUE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE(TestProj* handle) {
	/* The reactions of state OIL_TRUE. */
	if (handle->ifaceDiesel1.OIL_LOW_raised) { 
		/* Default exit sequence for state OIL_TRUE */
		handle->stateConfVector[9] = TestProj_last_state;
		handle->stateConfVectorPosition = 9;
		/* 'default' enter sequence for state OIL_FALSE */
		/* Entry action for state 'OIL_FALSE'. */
		handle->internal.OIL_LOW = bool_false;
		handle->stateConfVector[9] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE;
		handle->stateConfVectorPosition = 9;
	}  else {
		if (handle->ifaceDiesel2.OIL_LOW_raised) { 
			/* Default exit sequence for state OIL_TRUE */
			handle->stateConfVector[9] = TestProj_last_state;
			handle->stateConfVectorPosition = 9;
			/* 'default' enter sequence for state OIL_FALSE */
			/* Entry action for state 'OIL_FALSE'. */
			handle->internal.OIL_LOW = bool_false;
			handle->stateConfVector[9] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE;
			handle->stateConfVectorPosition = 9;
		} 
	}
}

/* The reactions of state OIL_FALSE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE(TestProj* handle) {
	/* The reactions of state OIL_FALSE. */
	if (handle->ifaceDiesel1.OIL_LOW_raised) { 
		/* Default exit sequence for state OIL_FALSE */
		handle->stateConfVector[9] = TestProj_last_state;
		handle->stateConfVectorPosition = 9;
		/* 'default' enter sequence for state OIL_TRUE */
		/* Entry action for state 'OIL_TRUE'. */
		handle->internal.OIL_LOW = bool_true;
		handle->stateConfVector[9] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE;
		handle->stateConfVectorPosition = 9;
	}  else {
		if (handle->ifaceDiesel2.OIL_LOW_raised) { 
			/* Default exit sequence for state OIL_FALSE */
			handle->stateConfVector[9] = TestProj_last_state;
			handle->stateConfVectorPosition = 9;
			/* 'default' enter sequence for state OIL_TRUE */
			/* Entry action for state 'OIL_TRUE'. */
			handle->internal.OIL_LOW = bool_true;
			handle->stateConfVector[9] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE;
			handle->stateConfVectorPosition = 9;
		} 
	}
}

/* The reactions of state SWITCH_AUTOTYPE_TRUE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE(TestProj* handle) {
	/* The reactions of state SWITCH_AUTOTYPE_TRUE. */
	if (handle->ifaceKeyboard.SWITCH_AUTOTYPE_raised) { 
		/* Default exit sequence for state SWITCH_AUTOTYPE_TRUE */
		handle->stateConfVector[10] = TestProj_last_state;
		handle->stateConfVectorPosition = 10;
		/* 'default' enter sequence for state SWITCH_AUTOTYPE_FALSE */
		/* Entry action for state 'SWITCH_AUTOTYPE_FALSE'. */
		handle->internal.AUTOTYPE = "TRUCK";
		handle->stateConfVector[10] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE;
		handle->stateConfVectorPosition = 10;
	} 
}

/* The reactions of state SWITCH_AUTOTYPE_FALSE. */
static void testProj_react_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE(TestProj* handle) {
	/* The reactions of state SWITCH_AUTOTYPE_FALSE. */
	if (handle->ifaceKeyboard.SWITCH_AUTOTYPE_raised) { 
		/* Default exit sequence for state SWITCH_AUTOTYPE_FALSE */
		handle->stateConfVector[10] = TestProj_last_state;
		handle->stateConfVectorPosition = 10;
		/* 'default' enter sequence for state SWITCH_AUTOTYPE_TRUE */
		/* Entry action for state 'SWITCH_AUTOTYPE_TRUE'. */
		handle->internal.AUTOTYPE = "VAN";
		handle->stateConfVector[10] = TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE;
		handle->stateConfVectorPosition = 10;
	} 
}


